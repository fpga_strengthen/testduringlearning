close all;
clear;
clc;
% --------------------------------------------------------
%将ZCP_detection_for_syncpoint.m转变为verilog相似的语句
%对将ZCP_detection_for_syncpoint_verilog_20170418.m进行整理
%对相位折线进行29点最小二乘法，求斜率曲线，根据其过零点修正中心频率，进而求同步点

%%%%产生相位连续的FSK调制信号进行ADC采样后作为输入%%%%%%%%%%%%%
% sample_rate = 27.095;%采样速率，单位MHz
% data_bit = 100;
% src_data([1:2:data_bit-1])=1;
% src_data([2:2:data_bit])=0;
% % src_data = [1 1 1 1 1 1 1 1 1 1 0 1 0 1 0 1 0 1 1 1 1 1 0 0 1 1 1 1 0 1 1 1 1 1 0 0 0 0 0 1 1 0 0 1 1 1 1 1 1 1 1 0 0 0 0 0 1 1 1 1 1 1 0 0 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 0 0   ];
% % src_data = ones(1,1000);
% BPS_data = 0.56448 ;  %数据传输速率，单位MHz
% freq_0 = 4.234 - 0.282  ; %
% freq_1 = 4.234 + 0.282  ; %比特0和比特1的频点，单位MHz
% i_data_ADC_0  = FSK_mod(src_data,freq_0,freq_1,sample_rate,BPS_data);
% % 频偏变化
% freq_0 = 4.234 - 0.282  + 0.175 ;
% freq_1 = 4.234 + 0.282  + 0.175 ; %比特0和比特1的频点，单位MHz
% i_data_ADC_1  = FSK_mod(src_data,freq_0,freq_1,sample_rate,BPS_data);
% % 频偏变化
% freq_0 = 4.234 - 0.282  -0.175 ;
% freq_1 = 4.234 + 0.282  -0.175 ; %比特0和比特1的频点，单位MHz
% i_data_ADC_2  = FSK_mod(src_data,freq_0,freq_1,sample_rate,BPS_data);
% 
% % i_data_ADC = [i_data_ADC_0,i_data_ADC_1,i_data_ADC_2]-2048;
% i_data_ADC = i_data_ADC_2 - 2048;
% % %%%%%%%%%%%%%%%%%%%%带内干扰%%%%%%%%%%%%%%%%%%%%%
% src_data_noise = [1,1,1,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
% f1_noise = 4.234 - 0.01 ;
% f0_noise = 4.234 + 0.15 ;
% FSK_data_noise  = FSK_mod(src_data_noise,f0_noise,f1_noise,sample_rate,BPS_data)./10;
% FSK_data_noise = FSK_data_noise(1:end);
% i_data_ADC (475:475+length(FSK_data_noise)-1) = i_data_ADC (475:475+length(FSK_data_noise)-1)+ FSK_data_noise;
% i_data_ADC (2000:2000+length(FSK_data_noise)-1) = i_data_ADC (2000:2000+length(FSK_data_noise)-1)+ FSK_data_noise;
% %%%%%%%%%%%%%%%%%%%%加入数字滤波器产生相移%%%%%%%%%%%%%%%%%%%%%
% Hd = LP_8M_phase10degree;
% i_data_ADC = filter(Hd,i_data_ADC);


% %%%%%%%%%%%%%%%%%%%%%%读取ADC采样报文作为输入%%%%%%%%%%%%%%%%%%%%%
% ADC_data_hex = textread('C:\Users\dn\Desktop\BTM_matlab\lixiao\24点求和抵消算法求同步点matlab仿真\思诺应答器采集数据\思诺应答器丢数据ADC采样数据.txt','%s');
% i_data_ADC   = hex2dec(ADC_data_hex)' -2048;

% %%%%%%%%%%%%%%%%%%%%%%读取ADC采样报文作为输入%%%%%%%%%%%%%%%%%%%%%
% ADC_data_hex = textread('C:\Users\dn\Desktop\BTM_matlab\lixiao\不同算法求同步点matlab仿真\西安丢报文采集数据\3c25_signaltap_data\stp3_auto_signaltap_3.txt','%s');
% i_data_ADC   = hex2dec(ADC_data_hex)' - 2048;

% %%%%%%%%%%%%%%%%%%%%%%读取ADC采样报文作为输入%%%%%%%%%%%%%%%%%%%%%
% ADC_data_hex = textread('C:\Users\lenovo\Desktop\adc.txt','%s');
% i_data_ADC   = hex2dec(ADC_data_hex)' - 2048;

% %%%%%%%%%%%%%%%%%%%%%%读取ADC采样报文作为输入%%%%%%%%%%%%%%%%%%%%%
 ADC_data_hex = textread('C:\Users\lenovo\Desktop\data\新版接收板程序数据\0629新版程序数据\1_adc.txt','%s');
 i_data_ADC =  str2num(char(ADC_data_hex))' - 2048;

%% 转换为对应于verilog的算法
% i_data_ADC = FixPointNum(i_data_ADC, 12, 0)-2048;%字长12bit，精度0位小数  (13000:16300)
%导出数据
% i_data_ADC = fix(i_data_ADC);
% i_data_ADC   = dec2hex(i_data_ADC);

%% 窗函数功率谱
sample_rate = 27.095e6;
nfft = length(i_data_ADC);
window = ones(1,length(i_data_ADC))
[Pxx1,f1] = periodogram(i_data_ADC,window,nfft,sample_rate); %直接法求功率谱 h_n(num_windows+1,:)
Pxx_windows  = 10*log10(Pxx1);   %num_windows+1,:
figure(4),
plot(f1,Pxx_windows);
title('ADC数据功率谱');

%% ------------------计算同步点-------------------------------
% Hilbert变换 并将数据对齐
im_datain = hilbert_trans(i_data_ADC);
Length_data = length(im_datain);
im_datain = im_datain(11:Length_data-1);
i_data_ADC = i_data_ADC(6:Length_data-6);

figure(1),
subplot(2,1,1);
plot(i_data_ADC);
hold on,
plot(im_datain,'r');
legend('实部','虚部');

% 相位归一化后求arctan值
angle_tan = Phase_normalization_to_Calculate_phase( i_data_ADC , im_datain );

%%
phase_sum = 0;               %相位累计修正量
angle_fit_reg = zeros(1,30); %最小二乘法移位寄存器
flag_data_error = 0;         %数据错误指示
% slope =3*10^5;                    %斜率初值
slope =0;
bn = 0;                      %截距初值
cnt = 0;                     %修正计数初值
an = 0;                      %加权求和初值
slope_reg = zeros(1,51);     %斜率移位寄存器
slope_for_sync = zeros(1,2); %斜率和中心频率偏移处理后寄存，用于判断同步点
slope_right = 1;             %斜率范围正确指示
slope_correction = 0;        %斜率修正量

for ii = 2:length(angle_tan)
    %% 输入数据 打拍用于判断是否跨2pi
    angle_tan_temp = angle_tan(ii);
    angle_tan_delay = angle_tan(ii-1);
    %% 对2pi，中心频率进行修正,对频偏进行修正 并对连续的1或0的相位累积值进行上溢处理
    %上下溢修正的间隔至少54个点，故新一次上溢时cnt_correct必为0(已记完31点)，否则数据错误
    if( (phase_sum>=2*8192) && (cnt<=10) &&(cnt>=1) )%上溢处理
        flag_data_error = 1;%数据错误
    elseif(phase_sum>=2*8192)%上溢处理
        flag_data_error = 0;%数据正确
    elseif( (phase_sum<=-2*8192) && (cnt<=10) &&(cnt>=1) )%下溢处理
        flag_data_error = 1;%数据错误
    elseif(phase_sum<=-2*8192)%下溢处理
        flag_data_error = 0;%数据正确
    elseif(cnt == 0)
        flag_data_error = 0;
    else
        flag_data_error = flag_data_error;
    end
    
    %绘图用
    flag_data_error_plot(ii) = flag_data_error;
    
    %上下溢修正指示
    if(phase_sum>=2*8192)
        cnt = 1;
        flag_p = 1;
        flag_n = 0;
    elseif(phase_sum<=-2*8192)
        cnt = 1;
        flag_p = 0;
        flag_n = 1;
    elseif(cnt>=1 && cnt<=28)
        cnt = cnt + 1;
        flag_p = flag_p;
        flag_n = flag_n;
    else
        cnt = 0;
        flag_p = 0;
        flag_n = 0;
    end
    
    %绘图用
    cnt_plot(ii) = cnt;
    flag_p_plot(ii) = flag_p;
    flag_n_plot(ii) = flag_n;
    flag_n_or_p_plot(ii) = flag_n_plot(ii) ^ flag_p_plot(ii);
    
    if(phase_sum<=-2*8192)%相位累积超过-2*2pi 下溢处理
        if( angle_tan_temp + 4096 <= angle_tan_delay ) % 过2pi时做2*pi模糊度处理 以及中心频率的修正
            phase_sum = phase_sum + 3*8192 -1280;
        else
            phase_sum = phase_sum + 2*8192 - 1280;
        end
    elseif(phase_sum<2*8192) % 相位累积没有超过2*2pi
        if( angle_tan_temp + 4096 <= angle_tan_delay ) % 过2pi时做2*pi模糊度处理 以及中心频率的修正
            phase_sum = phase_sum + 8192 -1280;
        else
            phase_sum = phase_sum - 1280;
        end
    else% 相位累积超过2*2pi 上溢处理(phase_sum>=2*8192)
        if( angle_tan_temp+4096<=angle_tan_delay )
            phase_sum = phase_sum - 8192 - 1280; % 过2*pi处，相位直接下拉2*pi
        else
            phase_sum =  phase_sum - 2*8192 - 1280;  % 非2*pi处，相位也直接下拉2*pi
        end
    end
    
    %绘图用
    phase_sum_plot(ii) = phase_sum;
    
    if(flag_data_error==1)
        angle_fit = 0;
    else
        angle_fit = angle_tan_temp + phase_sum ;%将累积量叠加到寄存器中的原始相位上
    end
    
    %绘图用
    angle_fit_plot(ii) = angle_fit;
    
    %% 将修改相位后的数存入移位寄存器 30位移位寄存器，用于最小二乘法计算
    angle_fit_reg = [angle_fit_reg(2:end),angle_fit];
    
    %% -----------------最小二乘法---------------------------------------
    %取N=14个点用于之后进行29点最小二乘法
    %按照最小二乘法公式先进行对应位相乘
    %法1
    if(flag_data_error)
        slope = 0;
    elseif(cnt && flag_p)
        slope = slope - bn + 15*angle_fit_reg(1)+14*angle_fit_reg(30) +2*8192*(15-cnt);
    elseif(cnt && flag_n)
        slope = slope - bn + 15*angle_fit_reg(1)+14*angle_fit_reg(30) -2*8192*(15-cnt);
    else
        slope = slope - bn + 15*angle_fit_reg(1)+14*angle_fit_reg(30);
    end
    
    bn = bn + angle_fit_reg(30) - angle_fit_reg(1);
    
    bn_plot(ii+1) = bn;%绘图用
    yn_15_plot(ii)=15*angle_fit_reg(1);
    yn_14_plot(ii)=14*angle_fit_reg(30);
    
    slope_reg = [slope_reg(2:end),slope];%斜率移位寄存器，寄存51个点的斜率值。用于中心频率修正，同步点计算
    
    %% ---------------------中心频率修正--------------------------------------
    % 取平均,求w0，w1
    % 寄存的51个点的斜率值中，第26点用来判断是否过0。第26点过0时，根据斜率寄存器第1到3，第49到51点可以比较准确地估计w0，w1的值
    % 1:8:估计水平斜率；9：38：估计过零点，26点为过零点；39：46估计水平斜率
    slope_average_correction_forward = sum(slope_reg(44:51));
    slope_average_correction_after = sum(slope_reg(1:8));
    mid_freq = ( slope_average_correction_after + slope_average_correction_forward )/16;
    
    %对频率进行限制 给出正确指示
    extension = 1.5;
    f1_L = 53389/extension;   %具体参数见文档
    f1_H = 292726*extension;
    f0_L = -268163*extension;
    f0_H = -77952/extension;
    if( ( (slope_average_correction_forward/8<=f1_H)&&(slope_average_correction_forward/8>=f1_L) ) && ( (slope_average_correction_after/8<=f0_H)&&(slope_average_correction_after/8>=f0_L) ) )  
        slope_right = 1;
    elseif( ((slope_average_correction_forward/8<=f0_H)&&(slope_average_correction_forward/8>=f0_L)) && ((slope_average_correction_after/8<=f1_H)&&(slope_average_correction_after/8>=f1_L)) )
        slope_right = 1;
    else
        slope_right = 0;
    end %if
    
    % 根据估算的w0和w1的值求中心频率偏移量，用来后续同步点的判决
    if( (slope_reg(26)<=0) && (slope_reg(27)>=0) )  % + -  过零判决
        slope_correction = mid_freq;
    elseif( (slope_reg(26)>=0) && (slope_reg(27)<=0) ) % - +  过零判决
        slope_correction = mid_freq;
    else
        slope_correction = slope_correction;
    end %if
    
    slope_for_sync = [slope_for_sync(2),slope_reg(26)-slope_correction];%将修正后的斜率移入移位寄存器
    
    slope_uncorrect(ii) = slope_reg(26);%绘图用
    slope_corrected(ii) = slope_reg(26)-slope_correction;%绘图用
    %斜率限制图 绘图用
    slope_right_plot(ii) = slope_right;
    slope_average_correction_forward_plot(ii) = slope_average_correction_forward/8;
    slope_average_correction_after_plot(ii) = slope_average_correction_after/8;
    f1_L_plot(ii) = f1_L;
    f1_H_plot(ii) = f1_H;
    f0_L_plot(ii) = f0_L;
    f0_H_plot(ii) = f0_H;
    
    % 判决是否是拐点
    if( (slope_for_sync(1)<=0) && (slope_for_sync(2)>=0) && (slope_right==1) )  % + -  过零判决
        sync_point(ii) = 1;%判决的拐点
    elseif( (slope_for_sync(1)>=0) && (slope_for_sync(2)<=0) && (slope_right==1) ) % - +  过零判决
        sync_point(ii) = 1;%判决的拐点
    else
        sync_point(ii) = 0;
    end %if
    
end %for ii

%test
% figure(100),
% plot(flag_data_error_plot*10e4,'LineWidth',2)
% hold on;
% plot(cnt_plot*10e3,'r');
% plot(phase_sum_plot,'g');
% legend('数据错误指示','修正计数','相位累积量');
% plot(slope_right_plot.*5*10^5,'c');
% hold on,
% plot(slope_average_correction_forward_plot,'r');
% plot(slope_average_correction_after_plot,'g');
% plot(f1_L_plot);
% plot(f1_H_plot);
% plot(f0_L_plot);
% plot(f0_H_plot);
% plot(slope_corrected,'k','LineWidth',2);
% plot(sync_point*6*10^5,'k');
% legend('频率范围正确指示','前8点斜率平均','后八点斜率平均','f1下限','f1上限','f0下限','f0上限','修正后的斜率','同步点');
% 
% figure(101),
% plot(slope_uncorrect(25:end),'k');
% hold on,
% plot(-bn_plot,'b');
% plot(yn_15_plot,'r');
% plot(yn_14_plot,'g');
% plot(-bn_plot+[yn_15_plot,0]+[yn_14_plot,0],'c')
% plot(flag_n_or_p_plot*10e5,'k');
% legend('slope','-bn','+15倍','+14倍','-bn_plot+yn_15_plot+yn_14_plot','上下溢处理标志');

% 画出相位折线
figure(1),
subplot(2,1,2);
plot(angle_fit_plot);
title('相位折线');
grid on;

% 画出29点最小二乘法求出的斜率曲线以及拐点
figure(2);
subplot(2,1,1);
plot(slope_uncorrect./2030,'r');
hold on;
plot(slope_corrected./2030,'g');
plot(sync_point*100,'b');
legend('未修正的斜率','修正后的斜率','同步点');
title('过零点判决得到的同步结果');
grid on;

%% -----------------------------对比实验室现用算法----------------------------------
%对中心频率和2pi进行修正
angle_fit_lab = Phase_Correction(angle_tan);
%最小二乘法
[sync_p_8,slope_forward_lab,slope_after_lab] =  Least_Square_Method( angle_fit_lab );

figure(3);
subplot(2,2,1);
plot(angle_fit_plot);
legend('29点相位折线');
subplot(2,2,2);
plot(slope_corrected,'r');
hold on;
plot(sync_point*5*10^5,'g');
legend('29点斜率','29点同步点');
subplot(2,2,3);
plot(angle_fit_lab,'r');
legend('8点相位折线');
subplot(2,2,4);
plot(slope_forward_lab,'r');
hold on;
plot(slope_after_lab,'c');
plot(sync_p_8*5*10^4,'g');
plot(2100.*ones(1,length(sync_point)));%上下限
plot(-2100.*ones(1,length(sync_point)));
plot(12700.*ones(1,length(sync_point)));
plot(-12700.*ones(1,length(sync_point)));
legend('前8点斜率','后8点斜率','8点同步点');

%% ---------------------------------------相干解调-------------------------------------------------
[demodulation_result] = demodulation(i_data_ADC)
demodulation_result = [zeros(1,9),demodulation_result];

%% -----------------------------将同步点导入到同步算法中得到判决时刻----------------------------------
% 29点最小二乘法的判决时刻
[decision_time_29,sync_plot,flag_47] =  SyncFsk( sync_point );

% 观察判决时刻和斜率曲线对应关系
figure(2);
subplot(2,1,2);
plot(decision_time_29(1:end)*1.5,'r');%判决时刻*3*10^9
hold on;
plot(demodulation_result,'LineWidth',2);%相干解调结果
% plot(sync_plot,'k');%同步状态
% for i=1:length(slope_corrected)
%     if(slope_corrected(i)>0)
%         demodulation_result_29(i) = 1;
%     else
%         demodulation_result_29(i) = 0;
%     end
% end
% plot(~demodulation_result_29(49:end),'g','LineWidth',2);%'斜率做解调得出的解调结果'
legend('判决时刻','相干解调结果');
title('判决时刻和斜率曲线对应关系');

%% -----------------------------统计同步点的偏差量----------------------------------
%标准的48点
standard_48 = zeros(1,length(sync_point));
standard_48([48:48:length(sync_point)])=1;

figure(5),
plot(flag_47(6:end).*1.75,'r','LineWidth',2);
hold on;
plot(sync_point(7:end).*1.5,'g');
plot([ones(1,24),standard_48(14:end)].*2,'b');
plot(demodulation_result,'k');
legend('cnt_48=47','同步点','标准的48点','解调结果');
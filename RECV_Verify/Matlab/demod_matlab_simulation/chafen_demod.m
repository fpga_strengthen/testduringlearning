clc;
clear all;
close all;

%% 延时滤波器设计
fs = 63.221;
Ts = 1/fs;
fc = 4.234;
delta_f = 0.282;

D = 1/(4*fc*Ts);

for i = 1:10
   if ((i/2-1<=D)&&(D<= i/2))
       N_place(i) = 1;
   else
       N_place(i) = 0;
   end
end

N = find(N_place==1);

%N=8
h_0 = (1-D)/(1) * (2-D)/(2) * (3-D)/(3) * (4-D)/(4) * (5-D)/(5) * (6-D)/(6) * (7-D)/(7);
h_1 = (D-0)/(1-0) * (D-2)/(1-2) * (D-3)/(1-3) * (D-4)/(1-4) * (D-5)/(1-5) * (D-6)/(1-6) * (D-7)/(1-7);
h_2 = (D-0)/(2-0) * (D-1)/(2-1) * (D-3)/(2-3) * (D-4)/(2-4) * (D-5)/(2-5) * (D-6)/(2-6) * (D-7)/(2-7);
h_3 = (D-0)/(3-0) * (D-1)/(3-1) * (D-2)/(3-2) * (D-4)/(3-4) * (D-5)/(3-5) * (D-6)/(3-6) * (D-7)/(3-7);
h_4 = (D-0)/(4-0) * (D-1)/(4-1) * (D-2)/(4-2) * (D-3)/(4-3) * (D-5)/(4-5) * (D-6)/(4-6) * (D-7)/(4-7);
h_5 = (D-0)/(5-0) * (D-1)/(5-1) * (D-2)/(5-2) * (D-3)/(5-3) * (D-4)/(5-4) * (D-6)/(5-6) * (D-7)/(5-7);
h_6 = (D-0)/(6-0) * (D-1)/(6-1) * (D-2)/(6-2) * (D-3)/(6-3) * (D-4)/(6-4) * (D-5)/(6-5) * (D-7)/(6-7);
h_7 = (D-0)/(7-0) * (D-1)/(7-1) * (D-2)/(7-2) * (D-3)/(7-3) * (D-4)/(7-4) * (D-5)/(7-5) * (D-6)/(7-6);

h = [h_0,h_1,h_2,h_3,h_4,h_5,h_6,h_7];
%fvtool(h,1);

%% 差分解调仿真
sample_rate = 63.221;%采样速率，单位MHz
data_bit = 10;
src_data([1:2:data_bit-1])=1;
src_data([2:2:data_bit])=0;
BPS_data = 0.56448 ;  %数据传输速率，单位MHz
freq_0 = 4.234 - 0.282  ; %
freq_1 = 4.234 + 0.282  ; %比特0和比特1的频点，单位MHz
i_data_ADC  = FSK_mod_14bit_signed(src_data,freq_0,freq_1,sample_rate,BPS_data);

i_data_ADC_reg = zeros(1,8);
for i = 1:length(i_data_ADC)-7
    i_data_ADC_reg = i_data_ADC(i:i+7);
    data_delay(i) = i_data_ADC_reg*h';
end

figure(1),
plot(i_data_ADC);
hold on,
plot(data_delay,'r');
legend('ADC数据','延时后数据');

ADC_mult_delay = i_data_ADC(1:length(data_delay)).* data_delay;

figure(2),
plot(ADC_mult_delay);
hold on,
title('乘法器输出');

for i = 1:length(ADC_mult_delay)-7
    data(i) = mean(ADC_mult_delay(i:i+6));
end
figure(2),
plot(data,'r');

% low_pass = [0.0311552631495954,-0.213983650186376,-0.0802838980110086,-0.0275366273876570,0.0405706338993823,0.122115923239374,0.195730457001522,0.239480569824666,0.239480569824666,0.195730457001522,0.122115923239374,0.0405706338993823,-0.0275366273876570,-0.0802838980110086,-0.213983650186376,0.0311552631495954];
% low_pass_length = length(low_pass);
% for i = 1:length(ADC_mult_delay)-low_pass_length
%     low_pass_data(i) = i_data_ADC(i:i+160)*low_pass';
% end
% 
% figure(2),
% plot(low_pass_data.*500,'r');

module DAROM_bit0(    
    input   wire        clk,
    output  wire [11:0] sin_rom_bit0
);

    parameter   COUNTER_LOAD_8k_INIT = 9'd500;

    wire        cout_sig;
    wire [8:0]  addr;

    //调用计数控制IP
    //该计数器装载初值，会实现向下计数
    counter_ctrl U_Counetr_ctrl(
	    .clock  (clk),
	    .data   (COUNTER_LOAD_8k_INIT - 1),
	    .sload  (cout_sig),

	    .cout   (cout_sig),
	    .q      (addr)
    );

    //调用ROM，根据地址查表输出正弦信号
    rom_12x512 U_rom_12x512(
	    .address(addr),
	    .clock  (clk),

	    .q      (sin_rom_bit)
    );
    
endmodule
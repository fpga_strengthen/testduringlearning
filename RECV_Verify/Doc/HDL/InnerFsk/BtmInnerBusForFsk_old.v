
//-------------------------------------------------------------------------------------------------                                                                             
//                                                                                                                                                                              
//--- ALL RIGHTS RESERVED
//--- File      : BtmInnerBusForFsk.v 
//--- Auther    : JInyuan Chen  Jinyuan_whw@hotmail.com
//--- Date      : 2009.5.12
//--- Version   : v2.0
//--- Abstract  : Inner Bus Control Module of BTM
//
//-------------------------------------------------------------------------------------------------                                                                             
//                                                                                                                                                                              
// Description :                                                                                                                                                                
//                                                                                                                                                                              
//-------------------------------------------------------------------------------------------------                                                                             
//--- Modification History:
//--- Date          By          Version         Change Description 
//-------------------------------------------------------------------
//--- 2009.5.12    Jinyuan Chen      v1.0       
//--- 2009.6.15    Jinyuan Chen      v2.0        ctrl once to send per blaise 
//--- 2009.6.20    Jinyuan Chen      v3.0        (FFFF || FFFFD)-->to throw   
//--- 2009.7.07    Jinyuan Chen      v4.0        460800    Baud 
//--- 2009.8.02    Jinyuan Chen      v5.0        NoFIFO   27095K/2/16= 864.7k Baud   >  564/8*11=77.5K
//--- 2009.8.16    Jinyuan Chen      v6.0         FSK
//--- 2011.8.8     Dan Cui       v6.1    ???FSK??????????3ms??????????????????????
//                         FSK????FFFD???????????FSK???
//--------------------------------------------------------------------
module BtmInnerBusForFsk(
            i_clk_27M,             //--14.7456M clk 
            i_rst_n,               //--reset
            
            iv_defsk_data8bit,     //--defsk data input 8bits
            i_defsk_wen,             //--defsk data by using this clk  
            
            o_uart_data,             //--UART output             
            o_uart_fsk_wen
            
//            defsk_done,
//            defsk_start
            );
    
input i_clk_27M;
input i_rst_n;
input [7:0]iv_defsk_data8bit;
input i_defsk_wen;
output o_uart_data;
output o_uart_fsk_wen;//输出使能信号  
//output defsk_done;
//output defsk_start;

//--------------------------------------信号声明----------------------------------------------//
parameter 
  IDLE      = 3'd0,
  WATI      = 3'd1,
  HEAD1     = 3'd2,
  HEAD2     = 3'd3,
  DATA      = 3'd4,
  END1      = 3'd5,
  END2      = 3'd6;
 
parameter cntTx_max=12800;//接收FSK上限长度
 
wire   defsk_wen_p; 
reg  defsk_wen_temp1 ;
reg  defsk_wen_temp2 ;

reg  defsk_wen_sort ;
reg  [7:0]defsk_data_sort;    
reg  [15:0]defsk_data_temp; 

reg [16:0]cnt_T;
reg [13:0]cntTx;//12800 byte

reg [2:0]state;
reg  defsk_wen_fifo ;
reg  [7:0]defsk_data_fifo;

reg defsk_wen_fifo_d;

wire uart_tx_done;  
wire uart_tx_done_p; 
reg  uart_tx_done1 ;
reg  uart_tx_done2 ; 
    
wire o_uart_data;
reg o_uart_fsk_wen;
  
//------------------------ defsk_wen_p  ---------------   
always @ ( negedge i_rst_n or posedge i_clk_27M)
  begin
    if ( !i_rst_n )
      begin
        defsk_wen_temp1 <= 1'b1;  
        defsk_wen_temp2 <= 1'b1;
      end
    else
      begin
        defsk_wen_temp1 <= i_defsk_wen;   
        defsk_wen_temp2 <= defsk_wen_temp1;
      end
  end

assign defsk_wen_p=(~defsk_wen_temp2)& (defsk_wen_temp1);

//------------------------ defsk_wen_sort filter the FFFF and FFFD ---------------  
//将解调出的结果中包含FFFF和FFFD的数据转换成FC
always @ ( negedge i_rst_n or posedge i_clk_27M)
  begin
    if ( !i_rst_n )
      begin
        defsk_wen_sort <= 1'b0; 
        defsk_data_sort <= 8'b0;
        defsk_data_temp <= 16'hff;
      end
    else if(defsk_wen_p)
      if((defsk_data_temp[15:8]==8'hFF) &&(iv_defsk_data8bit==8'hFF ||iv_defsk_data8bit==8'hFD) )
        begin
          defsk_wen_sort <= 1'b1; 
          defsk_data_sort <= 8'hFC;
          defsk_data_temp <= {iv_defsk_data8bit,defsk_data_temp[15:8]};
        end
      else 
        begin
        defsk_wen_sort <= 1'b1; 
        defsk_data_sort <= iv_defsk_data8bit;
        defsk_data_temp <= {iv_defsk_data8bit,defsk_data_temp[15:8]};
        end
    else 
      begin
      defsk_wen_sort <= 1'b0; 
      defsk_data_sort <= defsk_data_sort;
      defsk_data_temp <= defsk_data_temp;
      end
  end

//----------------------------- cnt_T -------------------------
//计数两个8bit使能之间的时间间隔
always @ ( negedge i_rst_n or posedge i_clk_27M) 
  begin
    if ( !i_rst_n )
      cnt_T <= 17'd0;
    else if(defsk_wen_sort)
      cnt_T <= 17'd0; 
    else if(cnt_T==17'd81285)   //3ms 27.095*3
      cnt_T <= cnt_T; 
    else 
      cnt_T <= cnt_T+1'b1;   
  end
 
//----------------------------- cntTx -------------------------
//计数有多少个连续的8bit数据
always @ ( negedge i_rst_n or posedge i_clk_27M) 
  begin
    if ( !i_rst_n )
      cntTx <= 14'd0;
  //  else if(cnt_T>=16'd44235 )//--  100kbts
    else if(cnt_T>=17'd81285 )//--  100kbts 超时
      cntTx <= 14'd0;
    else if (cntTx>=cntTx_max)   //12800 接收FSK上限长度
      cntTx <= cntTx;        //-------
    else if(defsk_wen_sort)
      cntTx <= cntTx+1'b1;
    else 
      cntTx <= cntTx;   
  end       
    
//  ------------------------ defsk_wen_fifo   defsk_data_fifo---------------
always @ ( negedge i_rst_n or posedge i_clk_27M) 
  begin
    if ( !i_rst_n )
      begin
        state <= IDLE;
        defsk_wen_fifo<=1'd0;
        defsk_data_fifo <= 8'hff;             
      end 
    else begin
      case(state)
          IDLE: 
            if( defsk_wen_sort ) 
              begin
                state <= WATI;
                defsk_wen_fifo<=1'd0;
                defsk_data_fifo <= defsk_data_fifo;             
              end
            else 
              begin
                state <= state;
                defsk_wen_fifo<=1'd0;
                defsk_data_fifo <= defsk_data_fifo;             
              end
          WATI: 
//            if( cnt_T>=16'd44234) //3ms
            if( cnt_T>=17'd81284) //3ms
              begin
                state <= IDLE;
                defsk_wen_fifo<=1'd0;
                defsk_data_fifo <= defsk_data_fifo;             
              end
            else if ( defsk_wen_sort && cntTx<14'd100)
//            else if ( defsk_wen_sort )
              begin
                state <= HEAD1;
                defsk_wen_fifo<=1'd1;
                defsk_data_fifo <= 8'hff;             
              end  
            else 
              begin
                state <= state;
                defsk_wen_fifo<=1'd0;
                defsk_data_fifo <= defsk_data_fifo;             
              end                        
                
          HEAD1: 
            if(uart_tx_done_p) 
              begin
                state <= HEAD2;
                defsk_wen_fifo<=1'd1;
                defsk_data_fifo <= 8'hff;             
              end
            else
              begin
                state <= state;
                defsk_wen_fifo<=1'd0;
                defsk_data_fifo <= defsk_data_fifo;             
              end   
          HEAD2: 
            if(uart_tx_done_p) 
              begin
                state <= DATA;
                defsk_wen_fifo<=1'd0;
                defsk_data_fifo <= defsk_data_fifo;             
              end
            else
              begin
                state <= state;
                defsk_wen_fifo<=1'd0;
                defsk_data_fifo <= defsk_data_fifo;             
              end   
          DATA: 
//            if( cntTx==(cntTx_max-2) || (cnt_T>=16'd44234) )  // cnt_T-1
            if( cnt_T>=17'd81284 )  // cnt_T-1
              begin
                state <= END1;  //??FSK??????????????FFFD
                defsk_wen_fifo<=1'd0;
                defsk_data_fifo <= 8'hff;             
              end
            else
              begin
                state <= state;
                defsk_wen_fifo<=defsk_wen_sort;
                defsk_data_fifo <= defsk_data_sort;             
              end
          END1: 
            begin
              state <= END2;
              defsk_wen_fifo<=1'd0;
              defsk_data_fifo <= 8'hff;             
            end
          END2: 
              begin
                state <= IDLE;
                defsk_wen_fifo<=1'd0;
                defsk_data_fifo <= 8'hff;             
              end
          default :   
            begin
              state <= IDLE;
              defsk_wen_fifo<=1'd0;
              defsk_data_fifo <= 8'hff;             
            end         
      endcase
    end//if
  end//always


//----------------------------------------o_uart_fsk_wen---------------------------------------//
always @ ( negedge i_rst_n or posedge i_clk_27M) 
  begin
    if ( !i_rst_n )
      o_uart_fsk_wen <= 1'b0;
    else if( state == IDLE )
      o_uart_fsk_wen <= 1'b0;
    else
      o_uart_fsk_wen <= 1'b1; 
  end

////----------------------------------------defsk_done--fsk????---------------------------------------//
//reg defsk_done;
//always @ ( negedge i_rst_n or posedge i_clk_27M) 
//  if ( !i_rst_n )
//    defsk_done <= 1'b0;
//  else if( state == END2 )
//    defsk_done <= 1'b1;
//  else if( state == IDLE )  //2011.9.14
//    defsk_done <= defsk_done;
//  else
//    defsk_done <= 1'b0; 

////----------------------------------------defsk_start---------------------------------------//
//reg defsk_start;
//always @ ( negedge i_rst_n or posedge i_clk_27M) 
//  if ( !i_rst_n )
//    defsk_start <= 1'b0;
//  else if( state == HEAD1 )
//    defsk_start <= 1'b1;
//  else
//    defsk_start <= 1'b0;  

//----------------------------- defsk_wen_fifo_d -------------------------
always @ ( negedge i_rst_n or posedge i_clk_27M) 
  begin
    if ( !i_rst_n )
      defsk_wen_fifo_d <= 1'b0;
    else 
      defsk_wen_fifo_d <= defsk_wen_fifo; 
  end

//------------------------ uart_tx_done_p  ---------------   
always @ ( negedge i_rst_n or posedge i_clk_27M)
  begin
    if ( !i_rst_n )
      begin
        uart_tx_done1 <= 1'b1;  
        uart_tx_done2 <= 1'b1;
      end
    else
      begin
        uart_tx_done1 <= uart_tx_done;  
        uart_tx_done2 <= uart_tx_done1;
      end 
  end

assign uart_tx_done_p=(~uart_tx_done2)& (uart_tx_done1);

//---------------------------   UartTx    ------------------------------------
UartTx uu_UartTx
  (
  .CLK ( i_clk_27M ), //need change?    
  .RESETn(i_rst_n),
  .start(defsk_wen_fifo_d && uart_tx_done ),    
  .data(defsk_data_fifo),
  .TIN(o_uart_data), 
  .done(uart_tx_done)
  ); 
         
endmodule                        




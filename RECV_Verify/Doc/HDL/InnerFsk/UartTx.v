//-------------------------------------------------------------------------------------------------                                                                             
//                                                                                                                                                                              
//--- ALL RIGHTS RESERVED
//--- File      : UartTx.v
//--- Auther    : JInyuan Chen  Jinyuan_whw@hotmail.com
//--- Date      : 2009.3.27
//--- Version   : v1.0
//--- Abstract  : Universal Asynchronous Transmitter
//
//-------------------------------------------------------------------------------------------------                                                                             
//                                                                                                                                                                              
// Description :                                                                                                                                                                
//                                                                                                                                                                              
//-------------------------------------------------------------------------------------------------                                                                             
//--- Modification History:
//--- Date          By          Version         Change Description 
//-------------------------------------------------------------------
//--- 2009.3.27    Jinyuan Chen      v1.0       
//---2014.05.30     neng deng       v2.0       修改时钟，改用系统时钟27.095M
//---
//---
//--------------------------------------------------------------------
module UartTx
  (
  CLK,                   //--时钟输入为波特率的 16倍    27.095/29 = 934310
  RESETn,                //--复位 
  start,                 //--开始传输控制，上升沿有效   
  data,                  //--8bits数字输入
  TIN,                   //--UART输出
  done                   //--在转时为0 传完后空闲时为1 
  );
  
  input CLK;
  input RESETn;
  input start;
  input [7:0]data;
  output TIN;
  output done;
  
  parameter   
    IDLE  = 4'h0,
    START = 4'h1,
    BIT0  = 4'h2,
    BIT1  = 4'h3,
    BIT2  = 4'h4,
    BIT3  = 4'h5,
    BIT4  = 4'h6,
    BIT5  = 4'h7,
    BIT6  = 4'h8,
    BIT7  = 4'h9,
    STOP1 = 4'ha;
  reg TIN;   
  reg done;
  
//  reg [3:0] counter;
  reg [4:0] counter;
  reg [3:0] status;
  
  always @( posedge CLK or negedge RESETn )
    if ( !RESETn )
      counter <= 5'd0;
    else 
      begin
        if( status == IDLE )
          counter <= 5'd0;
        else if( status == START && counter == 5'd0 )   
          counter <= 5'd1;
        else if(counter == 5'd28)//计数周期为29
          counter <= 5'd0;
        else
          counter <= counter + 1'b1;
      end
  
  reg start_reg1;
  reg start_reg2;
  always @ ( posedge CLK or negedge RESETn )
    if ( !RESETn )
      begin
        start_reg1 <= 1'b0;
        start_reg2 <= 1'b0;
      end
    else
      begin
        start_reg1 <= start;
        start_reg2 <= start_reg1;
      end
  
  wire start_posedge;
  assign start_posedge = ( !start_reg2 ) && ( start_reg1 );
  
  reg [7:0] buffer;
  always @( posedge CLK or negedge RESETn )
    if ( !RESETn )
      buffer <= 8'hff;
    else
      begin 
        if( start_posedge )
          buffer <= data;
        else
          buffer <= buffer;
      end
  
  always @ ( posedge CLK or negedge RESETn )
    if ( !RESETn )
      status <= IDLE;
    else
      begin
        case ( status )
          IDLE:
            if ( start_posedge )
              status <= START;
            else
              status <= IDLE;
          START:
            if ( counter == 5'd28 )
              status <= BIT0;
            else
              status <= START;
          BIT0:
            if ( counter == 5'd28 )
              status <= BIT1;
            else
              status <= BIT0;
          BIT1:
            if ( counter == 5'd28 )
              status <= BIT2;
            else
              status <= BIT1;
          BIT2:
            if ( counter == 5'd28 )
              status <= BIT3;
            else
              status <= BIT2;
          BIT3:
            if ( counter == 5'd28 )
              status <= BIT4;
            else
              status <= BIT3;
          BIT4:
            if ( counter == 5'd28 )
              status <= BIT5;
            else
              status <= BIT4;
          BIT5:
            if ( counter == 5'd28 )
              status <= BIT6;
            else
              status <= BIT5;
          BIT6:
            if ( counter == 5'd28 )
              status <= BIT7;
            else
              status <= BIT6;
          BIT7:
            if ( counter == 5'd28 )
              status <= STOP1;              
            else
              status <= BIT7;
          STOP1:
            if ( counter == 5'd28 && start_posedge )        
              status <= START;
            else if ( counter == 5'd28 && !start_posedge )
              status <= IDLE;           
            else
              status <= STOP1;  
          default:
            status <= IDLE;   
        endcase
      end
  
  
  always @ ( status or buffer )
    case ( status )
      IDLE :  TIN = 1'b1;
      START:  TIN = 1'b0;  
      BIT0 :  TIN = buffer[0]; 
      BIT1 :  TIN = buffer[1]; 
      BIT2 :  TIN = buffer[2]; 
      BIT3 :  TIN = buffer[3]; 
      BIT4 :  TIN = buffer[4]; 
      BIT5 :  TIN = buffer[5]; 
      BIT6 :  TIN = buffer[6]; 
      BIT7 :  TIN = buffer[7]; 
      STOP1:  TIN = 1'b1;         
      default:  TIN = 1'b1;
    endcase
  
  always @ ( status or counter )
    case ( status )
      IDLE :  done = 1'b1;
      default:  done = 1'b0;
    endcase

endmodule
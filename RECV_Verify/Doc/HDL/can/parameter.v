///////////////////////////////////////////////
//  mcp2515  具体配置策略参见Doc目录下文件
////////////////////////////////////////////
///REGISTER ADDRESS  
  `define TX0CTRL_ADD     8'h30
  `define TX1CTRL_ADD     8'h40
  `define TX2CTRL_ADD     8'h50
  `define TXRTSCTRL_ADD   8'h0D
  `define RXB0CTRL_ADD    8'h60
  `define RXB1CTRL_ADD    8'h70
  `define BFPCTRL_ADD     8'h0c
  `define CNF1_ADD        8'h2A
  `define CNF2_ADD        8'h29
  `define CNF3_ADD        8'h28
  `define CANINTE_ADD     8'h2B
  `define CANINTF_ADD     8'h2C
  `define CANCTRL_ADD     8'h0F
  `define CANSTAT_ADD     8'h0E
  `define TXB0SIDH_ADD    8'h31
  `define TXB1SIDH_ADD    8'h41
  `define TXB2SIDH_ADD    8'h51
  `define TXB0SIDL_ADD    8'h32
  `define TXB1SIDL_ADD    8'h42
  `define TXB2SIDL_ADD    8'h52
  `define TXB0DLC_ADD     8'h35
  `define TXB1DLC_ADD     8'h45
  `define TXB2DLC_ADD     8'h55
  `define TXB0D0_ADD      8'h36
  `define TXB0D1_ADD      8'h37
  `define TXB0D2_ADD      8'h38
  `define TXB0D3_ADD      8'h39
  `define TXB0D4_ADD      8'h3A
  `define TXB0D5_ADD      8'h3B
  `define TXB0D6_ADD      8'h3C
  `define TXB0D7_ADD      8'h3D  
  `define TXB1D0_ADD      8'h46
  `define TXB1D1_ADD      8'h47
  `define TXB1D2_ADD      8'h48
  `define TXB1D3_ADD      8'h49
  `define TXB1D4_ADD      8'h4A
  `define TXB1D5_ADD      8'h4B
  `define TXB1D6_ADD      8'h4C
  `define TXB1D7_ADD      8'h4D  
  `define TXB2D0_ADD      8'h56
  `define TXB2D1_ADD      8'h57
  `define TXB2D2_ADD      8'h58
  `define TXB2D3_ADD      8'h59
  `define TXB2D4_ADD      8'h5A
  `define TXB2D5_ADD      8'h5B
  `define TXB2D6_ADD      8'h5C
  `define TXB2D7_ADD      8'h5D 
  `define RXB0SIDH_ADD    8'h61
  `define RXB1SIDH_ADD    8'h71
  `define RXB0SIDL_ADD    8'h62
  `define RXB0EID8_ADD    8'h63
  `define RXB0EID0_ADD    8'h64
  `define RXB0DLC_ADD     8'h65
  `define RXB0D0_ADD   	  8'h66
  `define RXB0D1_ADD   	  8'h67
  `define RXB0D2_ADD   	  8'h68
  `define RXB0D3_ADD   	  8'h69
  `define RXB0D4_ADD   	  8'h6A
  `define RXB0D5_ADD   	  8'h6B
  `define RXB0D6_ADD   	  8'h6C
  `define RXB0D7_ADD   	  8'h6D
 //////////////////////////////////////////////////// 
 //collect data
  `define TXB0SIDH    8'h00
  `define TXB0SIDL    8'h40
  `define TXB0SIDL_1  8'h20
  `define TXB0SIDL_2  8'h60
  `define TXB0DLC     8'h08
  `define TXB0D0      8'h01
  `define TXB0D1      8'hA8
  `define TXB0D2      8'hff  //num
  `define TXB0D3      8'h00
  `define TXB0D4      8'h00
  `define TXB0D5      8'h00
  `define TXB0D6      8'h00   //crc
  `define TXB0D7      8'h00   //crc
  `define CAN_SEND    8'h08
 //inital data
  `define TX0CTRL      8'b00000000
  `define TX1CTRL      8'b00000001
  `define TX2CTRL      8'b00000011
  `define TXRTSCTRL    8'b00000000
  `define RXB0CTRL     8'b01100100
  `define RXB1CTRL     8'b01100000
  // `define BFPCTRL      8'b00000011
  `define BFPCTRL      8'b00001111
  `define CNF1_125k         8'b00000101     // 8'b00001001（原）
  `define CNF2_125k         8'b10010010     // 8'b10010001（原），8'b10010010(改)计算得波特率为135k，误差为10k;修正后为123k，误差为2k
  `define CNF3_125k         8'b00000011 
  `define CNF1_250k         8'b00000100
  `define CNF2_250k         8'b10010001
  `define CNF3_250k         8'b00000011
  `define CNF1_500k         8'b00000000
  `define CNF2_500k         8'b10111111
  `define CNF3_500k         8'b00000111
  `define CNF1_1M         8'b00000001
  `define CNF2_1M         8'b10001000
  `define CNF3_1M         8'b00000001
  `define CANINTE      8'b00011111
  `define CANCTRL      8'b10000000
  `define CANCTRL_UAL  8'b00000000  
  `define CANINTF_C    8'b00000000 
 //command
  `define MODY_BIT            8'b00000101 
  `define SEND_REG_CMD        8'b00000010
  `define READ_REG_CMD        8'b00000011
  `define READ_STATE_CMD      8'b10100000
  `define RESET_CMD           8'b11000000 
  //board ID
  `define DEFAULTS_ACTIVE 		8'hC1
  `define DEFAULTS_STANDBY		8'hC3
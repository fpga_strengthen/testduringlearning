--------------------------------------------------------------
-- 1 Byte RNG 
--
-- Hamish Rawsley June 2007
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_signed.all;

entity RNG_8Bits is
	port (	clock		: in std_logic;
			reset		: in std_logic;
						
			RNG_Byte	: out std_logic_vector(7 downto 0)
			);
end RNG_8Bits;

architecture arch of RNG_8Bits is

	component lcell
	port(A_IN : in std_logic;
		 A_OUT : out std_logic);
	end component;

    type reg_type is record
		MetaStable_reg		: std_logic_vector(1 downto 0);
		Shift_reg			: std_logic_vector(7 downto 0);
    end record;
    
    signal 	r,
    		rIn         : reg_type;

	signal  lcell1_out,
			lcell2_out,
			XOR_sig		: std_logic;
	
begin
	
	RNG_Byte			<= r.Shift_reg;
	
			
	LCELL1 : lcell
	port map (
			A_IN 		=> NOT(lcell1_out),
			A_OUT		=> lcell1_out);
	
	LCELL2 : lcell
	port map (
			A_IN 		=> NOT(lcell2_out),
			A_OUT		=> lcell2_out);
	
	
	main : process (r, XOR_sig, lcell1_out, lcell2_out)
	begin
		
		rIn.MetaStable_reg	<= r.MetaStable_reg(0) & XOR_sig;
		rIn.Shift_reg		<= r.Shift_reg(6 downto 0) & r.MetaStable_reg(1);
		XOR_sig				<= (((NOT(lcell1_out) XOR NOT(lcell2_out)) XOR r.Shift_reg(0)) XOR r.Shift_reg(7));
						
	end process main;	
	
	
	regs : process (reset, clock, r, rIn)
    begin
		if (reset = '1') then
		
				r.MetaStable_reg	<= (others => '0');
				r.Shift_reg			<= (others => '0');
		
        elsif (clock'event and clock = '1') then

				r.MetaStable_reg	<= rIn.MetaStable_reg;
				r.Shift_reg			<= rIn.Shift_reg;
	        			
        end if;
    end process regs;
end arch;
--------------------------------------------------------------
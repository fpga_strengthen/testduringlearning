// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Wed Aug 21 20:43:53 2024
// Host        : J_GMson running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
//               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ data_delay20_sim_netlist.v
// Design      : data_delay20
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7vx485tffg1157-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "data_delay20,c_shift_ram_v12_0_13,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_13,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix
   (A,
    D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef" *) input [5:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [7:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [7:0]Q;

  wire [5:0]A;
  wire CLK;
  wire [7:0]D;
  wire [7:0]Q;

  (* c_addr_width = "6" *) 
  (* c_ainit_val = "00000000" *) 
  (* c_default_data = "00000000" *) 
  (* c_depth = "36" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "1" *) 
  (* c_has_ce = "0" *) 
  (* c_has_sclr = "0" *) 
  (* c_has_sinit = "0" *) 
  (* c_has_sset = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "0" *) 
  (* c_shift_type = "1" *) 
  (* c_sinit_val = "00000000" *) 
  (* c_sync_enable = "0" *) 
  (* c_sync_priority = "1" *) 
  (* c_verbosity = "0" *) 
  (* c_width = "8" *) 
  (* c_xdevicefamily = "virtex7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_13 U0
       (.A(A),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "6" *) (* C_AINIT_VAL = "00000000" *) (* C_DEFAULT_DATA = "00000000" *) 
(* C_DEPTH = "36" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "1" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "0" *) 
(* C_SHIFT_TYPE = "1" *) (* C_SINIT_VAL = "00000000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "8" *) 
(* C_XDEVICEFAMILY = "virtex7" *) (* downgradeipidentifiedwarnings = "yes" *) 
module decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_13
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [5:0]A;
  input [7:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [7:0]Q;

  wire [5:0]A;
  wire CLK;
  wire [7:0]D;
  wire [7:0]Q;

  (* c_addr_width = "6" *) 
  (* c_ainit_val = "00000000" *) 
  (* c_default_data = "00000000" *) 
  (* c_depth = "36" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "1" *) 
  (* c_has_ce = "0" *) 
  (* c_has_sclr = "0" *) 
  (* c_has_sinit = "0" *) 
  (* c_has_sset = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "0" *) 
  (* c_shift_type = "1" *) 
  (* c_sinit_val = "00000000" *) 
  (* c_sync_enable = "0" *) 
  (* c_sync_priority = "1" *) 
  (* c_verbosity = "0" *) 
  (* c_width = "8" *) 
  (* c_xdevicefamily = "virtex7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_c_shift_ram_v12_0_13_viv i_synth
       (.A(A),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
nn2y8yvGEAU+oxfqIjNJMT0yc3TadO2A5cZHoK3dC6l8eNK9HFYMskFicvVj1pqkc9mDJyUKrSId
CCL/HetNvg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
tNkKvQED4vWNv+vYe3nI8o8VVd25+7OXvcQGN/bznOgwwQ9zOLtv8RpNciTxGp//tcY2jCAsQOQR
SAwBc00y7wnNCTMtZFvxXOqSHEUPYTYxcXYrIMSElXg1AeD03zwd+qdTjbS69InbpBZ40pWQBX71
ctP6YeotRwK8SefTdE8=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
aw7jhd7UZTrADfLTZ5YsRTYTdEzrJ+N4enHI0pDja6no68W2U8xeZSGR45xVkKPWy4UGH3zDqIyg
YAP5FGIc5LcSqiGDfNdrdIvThTT5xBjpM9zvSxq6836B2yVmJhQLgvziF962Xaqbe7uQRnXQp17I
kjzsA6zhURm/05lRx/day4Cz2CbJD4D4RVpd36+ytOucw4q+sxb/dm0j/Zj3I3aD0G+lVv+6bECF
ISSRuJv19uehdbvwf+qrzqTMPcPX7L9lxubWjJXjm+4496NlRuEv4eng6rowNLkC1RQXoUtW+UVo
PyhgdGfb+nJPLDiP90hHMCg0AlZBGnoQ4bZYlw==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
biYqlehfUy2MPR5IeETLexHTdJ7q51nWkAsSzWCNbkr2c3/I9AYuwIkh2pcsUMgsCkL2Lcb26gJW
D9+BY0gYxVOzcZx9DtuxM/DqEvzsBG5XW/R6eymYeJJXtW18hS5aOcO2RDf30eieTCZwOyx3cnis
herNQHaIc5PrMK7dsl2V0mRQ4Lex1+r46DNrReU4z+3VNRVfL0mX4NhB+QV2zDspLTAYf1l7nA5u
EbLi12YBXczbhvawRV6TjHxA4Ml/QzRmYMg1mfxJ+3FZmfoR+uU07NrBc1LOhonCg2DPch0TTDa/
+umVNvlER66pUotjBuU9vgjw7Dkkp+/MdqAajg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
VR2RqLmiSvIq90JPqVnSqgmKE5SicXJKPyTAunTpAaj1DuEXlVsRD04gzJadbWfPqE9xAoUiq9wU
PHjRKiQbrR4JC+wy8bkLpY5iW3oxOSRDZ65wXxQUvMxgGE2eFRVI/k/ZQRRYqr9mm8YdVU01dvtU
egqfPLie7vxuQB/aiN7a0E7FJ3PMFpnNM/lmZU8kUoAycwEcAu9Lvc3isAys48OIVQPaQHkMn2eB
HIwPBmpuermPsgmSnBIbbJxQ+dBWkPL0EzxPuDn7PUp+ojaG9ZnKwIfZL1me0oBJdzhp2dyAcfIo
QfkynG/j81yZHbXXctAPpAfTUjbbnye2spH36A==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
spURjF4NoZ4yjaDCJd2V8NW+Oj5ZMaUYbemUjajnsue2uyV5q0a5IBxkOcXgNdlJ2vnd44QwYWa4
lylHJkTIRWpMiAc6oKbTc0UpP1dy19psYVCAqRk/1+Ql6FdGPO88bH+fSSVGZp3FT1qBOx/xZdlI
oCEDIIhFzto+GF3j648=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
eMFwa46SXA6HVoFlviJtJvyKfnw7oJgLT1JrVR0eN4c/X8/spoO5r8yshZA3q3KquxGdP0kyqeP9
P1R/bA4WmHDfXN2hXErIJk8CzVEKi7D17DVfKTkvxwdjjBmDO5EYn24/k+RSr/VKfX4X0GFHpb5X
2fgSZVIPaY/ztx/tIF1U+TG7NvoQlBKjqBJyQNgDmciPzSPkSPzhpcEFL95PVLp2IwwcbtuM5PnK
SYBqbLz3Y6FcLScZwLiOUD+aFacR+bbjaJnOQ+Aap6Jy6mwXic0Vpf3KziYI6760f5HtSIaKYnEf
2emYl3n7jq3Nqev5AAxpFg7eUMHAnHMWb375Iw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
O1ijMDCqOOh/X+I66D6nqW8xYDmAyuR7Uc6JOqqFURykdOys+G1C7oFY4rvTp7LLT+huvIro897X
Wam1y7rJ9kAFYC96gvPeoOGLESQ8ILVyYOFURfss4xyDtd7Zn7cH4snMCNYKJwnkd9azpZQghKb3
p7/9PYMD8I9ndxqkXNtsa+CCL47aCppmn9R4J8tH6zlGbZ5XR2mE6ZiCB44pvpfzTieyPKYXyghp
4Cp+YAE0NZFDPmTUVOkrqTFce6BiADGHUSQhuGDkCzNMnJKIC7RlXfERvxe8KfZ8NdqtPHHbnUmb
FXJ95zEKXPAdHMJ2ZXZyJM0tpRC54QxngyNKKw==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
efqwEPeF4vYJ9mKOuYut57FSN19GzW4xpI0B5p1Nr4CMKvRh6rUR0963YyohKohdu5kJ7BNb6068
3uktD1L7mgC08Nju846rJNHjvje0d/fEIY2DCAnJAM+25mJx4zfafhCM7kkICgN5r3Fftm9zwUxs
crCwRNaWRCMrAxV515grN2eswrsdjLWf1G9bR1CJUuonbR1CQOtRL6DNIsv0ByV4bXbI7QUSg1EF
k7K6FI0vGEvHIBdlfH4B+W1TFqve7KEd2sfg2KzRV+lIzPlRt7r/vjW8OQsnCuX1lpFoLEUCI6Ij
ODJXlcxirnOL9Hg/GmtB1w+rXQ5Sn5WFPKPIhg==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 16304)
`pragma protect data_block
0HnlS6LJ1/OsJpPvoyaDajY++vA2ocO6rKdu+FvyTih1QGtfrrgCyWOsb/YBBKk2x4ZwUZG0/4jg
trg17HZeeVP21ZMtQ5I2skRUh7WB2go/inNvvwNDFX8Coweb+Mn39hNqc8aH674PCbrkXr6wZIda
QOG3DDIMOilCfXeSzx0saC6ET9o2N7JIQqejpnaGNIZwZgEfWwyPCEzv3hL9CiXTH6m4PjIr3D+1
e7yiSpRMaQUdWcyxPswUcyhp69ha2suxmQpimVUMGkfREyNBPw0eRl8EZde8tZUDEe+t7n373KyZ
B+bcCUzGSu0ByOZv8VeS6tBtde3XQLX3o8WTugBzRBDJZDFMiGMNUZ9wUmNOlHX2F9JpYgL3DArA
QVR1IJdbAoFsLDcdh01ysO7ZI1PcMNSPdO7QoB1ngG7qzHMppTLT0oIobq3FYJMpKhH9Fm3gNKaL
LCOkmdUTX9SaIOkqieK7y4skszoBthU8cOxCMxvD24RQwAKfby3P1CTmbw58EeFLtQseCT6nvD+f
38o7TiGdA9d+FaXjsbRV0Uikhx9r3Sfndy44De9ddrzznYL/p/YpLhBRkhgMRVktE2gWNcZ1soTe
9TZgN4j0Ynmd2sYZBjClJC77mxNTGxoPQVlnrigypiMjBK6lE6DCwkvp8hfs6kO2ULhy+vbQe/0N
AHWu7E8WTlZFzVruXbSTEChQ5U+kZd10I07+KkIQlXqhYwJ5TJz7LIrhKglKeUYZCERVmp1b03EZ
7wfY7nzV/8/eUm1RN+EV4ELgBWHfxN9UJXDy2bBAaadvozXhmsxm4eZIPYtfFMR0G65kpuLt7VwR
pNxoeqQfQEUzm8dCp6m/GthuqqtFwDxxMKBR190s+Gx/mAw/1bShvmnaT0ixSIX8SNwAZkJeJD06
56nDLirbHYZKVP7yLc9X6vRos9LS/JiYhITDSFsUsNmma2ZBJmr9VdfU9s0mUvt5a1z8AZ5Ngruj
MzGLtyXkf0M/AoMLdd2sVl2DVFMVZ6mTlhd0yyBsJ2Bm/UR2ArOA0Jw4a9Pj2qe/YWLXYykW8LxH
M4sH19UBWrBxQ6I2VLgyPGD27Fzndf8lBOV+uRZkQK89yx3pN91XAKc2o/7cg+5WAaeFuBDh6dcn
J5ZBIhZnSjra/H7Ln+Xt/ACBK8kMeUoiYJpLMiugMK9nQIfxBK0w9ANbLgDyPyPOJWlWzrQuiOxy
rs5ompDUHnnEtBsf2DJG/6IgpNyMWec5Ui7O0YAyJeVqwHJjr+KiCInpMUvPnK/6YN5a55wCP22h
/2d+dI4u29c0cxDDWbubDzbFenBO7AaHc9DI+e3AlkMqXxYVakT3RsRwHeKCwLVSUZqFkyx2sLyZ
ulHQlMgy29JceUYOMbt7WxjQxsPbLN9giHI949bguU8DSH8TjFCrSTFCxDoI9pk/fnFl4IkWBgFT
g+4Qs0PBsgtZfl38NrcUyqLKXaRSl6br8DUi7jT935STqD5Ybx0UrjsIbMpGhUSiwAL/gE0vm6lP
q37Fwh8SXGuAeT6IkxhfC+gWPFF3CFwcOZSQ86PmL6Aq/gvFrpiQg7vKKyHwmIoa8YwfM8pFRNS2
CJwsxwR7n0npaXvNCgO0Y8fqwgRhQedcW6/A0tA10uj1Wf6ujj5xNtKmT+MGrbinCcf4RyuRlB5K
WeFmAjsX4vp95LD9LAFoZIbaPVWWDMHcONAacb6jFnflCZWtG2+XpJ9pywjZmw8lxDNfxzmgSRMT
Z58eApW9efMx28jzbo8xDsE2zBNHa2pGsn0U0sSyxgZuCtgZHifVuE9xJnk9qfFXVfayNU2Hffsf
dI2c+zuvYWUlHoKCkQBJMRgeNXSWoYgRmm62cl4AmYMvT2dyxYe8z+nKO0Csz6LjY4DN1p1u43W9
rQLgGzjkhc55un1K9ciAsoJSp4w+uO/tgYJxEjTgaYlO4+5t4zZ0xajtpzXodjmlgd5t2sdPWhYo
KjU5LR2uw8WTU8/+uJNLicOp2OJFzt5ma987wwL/tPNB6Mey4JurD7cIvgOSlhTOrBNEhcdHfoRE
UuWU8RRYy/TRDYbhGPh/08JtoGLv+6gl2OkLAhlJB8n8l3NBwzbnumPty4322FMJjLDN6NDaL7TC
MWcjO4sgesFGnD1hSJDwPOVQVidxJtYiPM0cXVywCpcXp1HE3o7VvRpAAbYlcS1SfWWxL9vjvUEg
JuDjrpmbYLVTOpBoNFxg4rUeDahuhrMVHlxDzbzIliX8XhvqpS4G9L2dEhagve+oFYpzGqNyyv2R
4McJA5i8XhHvsevcM66aWw818Q8NBQShIRk8fyhnbUzgIb1ikxnMX/d6bFYX9Fojgj/40mYDfX7v
+9rIQ7YOZ/hHzZh6yjG2liWQbqHKzx3jupKpzUmvaXW+/KuAz/9C5X5hCiMOFqVrRHGTxNd1DYAS
ctramzCQbtAqqaXyxcV13PyAQNiGYIg2tulxzU2kpZD35t9dXKcEtwl2uRkFZmKMS+qbZxcZ64t3
hE+k9F/ttZpBJUTIkMV6I+MQcsp4yV1/BREZy3PWZYOln1tqA+xXJ2kO/FQL0c/5BLj3GIFcYtf2
oekBNj7uzMwQrQJsa1obgcPq3eXjkT5waKAAo6Ocd+zgFf9m+TceFFrTYZkv85bS1s3purAIU+kC
l5+otM6VeYAuAXpppmvmDrvoPIp4HCpkRCq3+izLwkvX13eBD+FUl4hzDLYnINJtPVKewVDmsjOh
aeNOYK7D+GObp5LBLZ1GBJOJRuwiaWUoLns+Wff+Yy/sIz6BcGO/Tg0DK35YKWOdJ7oiVhN3zCsM
j3Xp6FeHgsrKRwq8KBw9Mic2AD38ZrbWaQG/27HG1qj+yWiSbyDEPq+f7ckScdZImZjhx/HVG7Km
o37U6wDGRTN/i76iZEUmCn+NaYFdxgTT+zluhkBOlzPpYjno1WpMxl3kmvzaf5jYOHQl+KnlVGh9
5sJJQIk8xMHkEowhOh/AVm8euUNk/scPxE4Qa64z+Pe6kE85RSqsgrQC/Nmxlt+qa+6Ix9OeP6VJ
ZWk6Q6S8ZsOONdNOS9mj1bQqbwPzBUkJ+jWBcb7UWK2vaaDH2EpwzWOXs9AJqQtryR5yE+LcPPQg
bAR/dUecrZZ1WBVLmZwXunUz3vycHgedEw2x6bRsS7zcLStZep2zmG55qbqPZlsqlrgLSU1lbpm7
B4CKo+nRseYQeZENGe3ryuHqp1MYa3VPMjCj3rHxu6JMLk0+QI9O0owsZ8/hTsN+P6OdX6X4xgdL
vYI0JFhWg8dc40PMcOnudhjDe4Myy62WaVi5dFueHvS1YTW4DU7KqlOJUMoqD1RJc/33yA55mvoR
3poeaBlM8+tnEaEMQbkOdFCfFMRZcIdDpvwBcT5ijzJeBPYh9GKo47RY/dHaOuxHYbP1VSi7P8mA
OQou2WVvG+iU2i4dDyvtrOgZB0Gz9iykHbDI7YoBHOF7dO4QcqY/xYqg3Yj1kOFhsfdMmayPsSkp
QiFN2Du/PtzYWTDjp7QfAN2yjhPtqxTOOUmIGaqd10Y4wepq/yCM1azkeRMlYjmPiT/iJUrTjZlk
dv8bw58HUDIHWpbMZvftHLzx4T71vwM9gN22M2UyVQGoeOvWxNId59sba25Yy16icptk4kr1oprr
eVULhRp9Suo46AjfSZ6AEWA221xYpYhWrinG6tmyClponSgvZRgkmUYuq7B8Z8pFAdfqmms+tL2h
SCfW6zE0DCRRI1bBDTBOp3ozuNIBnmqN6bMzkjONceuAuBSeQtAdOSsSQ51LUagVA+5pM9GwMXeG
qSOhZL5/pKCrE9riIXCqHbBL4BVJee4weWUcJgZfsRm3BYzK6jBsF6GI0S2A0I0X22l3y+ewIvwS
aOLUueazK1cVXSUImbEIJll9lIIFyZlwZ3vRTmoKTpfTEzok5MelTnYbFxPJeyk/ymHYeOm2f2bd
nQBLdjuTU+dkPyJyaixHgxXjBkCgoSMwzeVV7XeT+TG+vXDUrh4xJWPgUScTaYCQvyGVW76aklmJ
DmXvcjW01MhMv+qmE5vD45nSz6ptnhOzgaCAgFdKRrinDzBT51ZHL/TprPIQvdRa/QYUrPaQT7NH
FEXna3mdqnjOtyP8SiMgkyaHA2BRf+vEwX48t3a4tGDYnGTgaf444Q7EBj6Sw9tmY4b8aWPO1jmZ
TI/Q0lwzkMtARq/yEs60ExA/UR7UUAG9vrdfq68xHDo1CDkKhnsS0cKXE8AqohmizVnMhw4EuTGz
MFP/uyMCyakM0RtRya9kql65QYMqVW3CCYsrsmGr7ffuJeQIXqRnxKieN+tPLuZDRQh3AKQeP1xi
0cMlA/fSJOZMTPeA8UkEYVf4oHcURnMQpe8599BowE8AO2a04YjhBA2LLqQEdQsJKcZ6wneP7ysr
1cYiyaAi7Wod139OkChs+7h7K2VRJv1io0F8umRpsmue9Rnuz/PMhIuZPhGJjT1chru/6RlxJc2L
tL27v/zFCV/TzzQOPd3JiBVwij9jTFIiGAmTXA34w6419jh75cm4+EfHvhSGy//HrWHt+avBMjHt
Wx+kwwuaB5nAJ09LYO1fL/j50qSoRKLUhcvlsth9r3qS42uIJJn6WwWI1B86iWQThMaWlTUCWBST
+gJLVjxMpxLawhmScHfCUtGrUFf6o4gvXNE4tAailG2Q7B3W3DqPdY/WtJ6ku/rj6S7CSVizOrJw
DSvuXWw9x2Di9QlXjn6T5YsfD7Hc3KzcPhvkTk1fLlQKz7PeX7KYbfrWrm8a5JAMTmf9kgJ21jI0
jYPcbeSq1K38cQp7ceKEV3fB0okZuGnv0pQyGNGgIbKvpubzubronl3IDajTuNF2IZGizbcRxuDl
hL3x2KfKHMVYvz3fY9QrSfUvfs2uy6GWxU2XoEXMK98HTyMIlbCkzzWPXGBudEWy21qyg94cGYNQ
U5Allt8Yq0GA4oPklwTbJ41tg6FpheFSSdNHfapYkW2/C7nPipjv9DRb7yTYjZ8Tne8PU/VxY8t3
QsL6JO6CHoM2IAyIgzHuoyDOAV3q2XAFr+qOvTVvzGU4O1+BNfSt0Ee25E6HKHfp6O3atA7K1LBW
OyROrH87/2v0VWdWr11F0ZSWHO+Wh9DYbxIh3wSLqKyEI9i/IzdjiZaNjmxYu6zJ8HQST/ZYKpJw
L0ggIBBJAHT9aEeavxMbTL0tOe73nmuxU3+x02r6Z9veYmLVy+pi8jKFukCUw8BxRiybkGz41COh
K3wXo4IM2XUyNs+q3P5ROa43iOa5Za5vHOm5vQnLy/qbWeUE2gaWR0YnjkXXDTBDZTeDnAxNa0E0
HgATfAHr/CirZI/QzrQu2+9LFn3BOecSrxMtNshRYc1siKhf/tU1GvSDKgDdesQ5qF29A2Dphnry
F3ZTqMrO2rcoG856T50LiR3Y95+jWC3WMYmIYu11Ger9B2ucjXnpy214T9top0W1GqDkoopCvLmK
fbVhH5fLKO4DM8gE0qnMRltGbi7QoGreUko2nagSGTJD0AcIeoHl1MxRg78KOJ8MRou18vpx0zKi
wOxN/a+Pjv46b0TT3pVFwoTayovdZvNK7xIduSyWcQdNJvlA20mvbXJ/u1JZdrCfd7n4vRefeJOc
gQQENg7i2FJI+TGqTkH9+8l2vjhgVrZ7K0Tdo4N5abxAdbZirJYUWyG4PhTzsLVDVfG9jc3+Dk/8
ucpzpSLiryLYRQHtmzdpTvsNRcKFWd9sA9jljXLv0hpgGw5f4Za8MaU6izxGRE74eT2Z7iX3UXBU
DvIjvIAOUrdybfLI66rzvVoWRwEP/36it9MaXUI9zh/QY/FiTJs+bQvkU7YEBdJ6TZKKp70coDc+
EgfY1FKr4FhwAS2z/GUYWZ0Z+xOCqiVQ0CsGzeoGA7fqxIRrvdBoB6k6y94Y9ZABn+eJnHGCUCj/
YOU9AWepQe97rIjpQdDunigL+2mUucTnqSK1FssO1FVbOwcmkzUZvrpzsn7udOGchT4edgFIukBQ
BHtA8wTsM6D1GNbtu/DnNNNt0mdLZTCVxgI3DeLg48PTUNvwaf3kGqKYY4nfcLadSGB0x7WV8KfX
fznPEnEjLWQsTpvqX0T/cLhJMZwmxBqnV5XOeCpNREIjKLs1DMY9kh35j56xazS93vddoxn8Tinm
goSPW6SjXBKMiXK4AkbPfdz/qGTAbKYInjElfNSGS3hIY/RIwDsN3l96kOqeW3AkEXcNG1/Gr3ah
uTyIm6w0n8ddnHk2A/b6LjSyPXjOynpajJZr4xWVEplDFAw81lg+tZEpwbtj3qgUMZ1zczLrm/n3
K/5qsxMjWZ1azoM+6NBMkNv+QNgbeKty0czJk3xQNAh79fkjuTXaOkFd/WabxGPMkE9zvGxv8GAN
dYFDqMQ6fz7G/nDF0k5f4uYoUYCWy98+pJDyn1HG7Ja/rN71TZMelvvXG80ciwX+aJpur0j9S0aJ
MCt4LfvyCnlL7Rc/dCE4LgQ+BqBnQwEp3v/bVyTSXGRkZlOT4hYocT46VQczlC8B61tjVwG4c0tb
ANEPnqoOacnEeiK9N9oJIIk0nMMBqHJqpgHU22Cv1Ne1/kocr1xvjANVoWcH5E+vNEralJdZBC4F
NSbvDTdYQfyalkGAuycdHDxBKKAQzmas4cCewn9zWOMwpAfGLo6BAsrinnsxWHZ9h+KTwGZnRI8p
cEUm6Z+UxwBsborxrS3DDJ2E77sD8gWCgyxk4vHaeNkimwXNvjcMmohrvsgo8PXcScJDDoMd09uv
z9CQ9rGyT21/82TKM78VyYX59syoer4zc76O9VD7BZANiHVFWzcp1usK9krgf2nKEBC3HarLRIuJ
QgquQhNF5+ZBuunRCGe58Jg934NzSXh2JjJF8mAAoXYzG+GP7uiZZnH1PafYv1rapU1XGpZphZdk
/6VqHTW0ZEZgR5AeLjfashtmM0SWqKR0IOcMQ6b5Fd0QA4oJL2jge8xU5R2AyFuvU3GQoD/IC32P
+8Rc7tA/l6+hOowKb3UtbdMXBMzK5HN6ECyeZzPNp4PoVhZVRZthJ+34y770tXFTfwRuILHQjRGQ
YghqP7KiHU/+NJlB37EVSqUhX+glHOrfaARFzxmi22ftptTf7LLRviAv/9oNwScRamsQoTpthPRU
DkBxkNwgEN/0GDP26oNtugHbtVzdf3sfDkeig7m2/X36NvRvEJalFt/B9Yb7FZT9BvezN0vp3e51
q307/x1vY148Yt266krRxdUb5apIkdKEL5zOCOzMVebwtirfpGsgeO8gHh0sMMZ6R2a5+6wlkTez
fjF/cfq55LB1TsJRnVnP7jS5jgVThUIWXma3P5EprQBB/vwn6lMjB3Gf/w9/sR+uGU4//TKK/9ML
c06KGjhX2p30Lp8+aduax8laKfTDAsx48Tr00P4rlZUXkXMfWD4cQs7FKswQ9bxx7UWy9aqA6Dd0
gbOtT8MRHj32DJwnCdeuKfHcQ3bQfVHoA//IkOIYKIcZU0asyPkTHC1R/yL3eIJNSl2uOQA3ip7v
2BCnrsgOvZhWLxio677pj1fuzYGWw0qDy5odwSV4lkLUitfLtAFCCudaBpnZz6UkE5poAqYSKoM5
FuEQHtGvC/8MId82wvABQpBq1tnn0C01Nx1ith8/1+SsK1ctcoH4gOqQ6uoafjlsVzuF/gyWu/ZH
OvRXjSo7F9QpvoTCo/R8T59WwY9KJ5NxpKlVf4DcBR9LXSMyMGtaRxH8OLtu+Kr4TAqDQkZYYh6j
AnL0xFrkrSd9rm60MZWLp5EwUG833/Z8arshTwpBJ21KuBCaw6MNQ2CBRPR0FtTqMF+gVk3gd9V4
FBH86WOOqq4Kti5m3r09t6eDwXO0kSz7xZMvDdFoovQXdEj6ccYHJSfdXNmDUq8sx8jWwAbhzdHW
QmPd8+6kZsMfUejlID68VXClZp4d89kbDfP7ERC3H0phkYXiz9o4caxIunTp9q1NCeMwefwKhNS3
g8JNAGdFqU2htcCEqbAdT9nHRjdihre/Vh2h3fzQhAtIIHasrp6OhPa91jTDwbPvcsO5PBRFz9dJ
rUUNw1QsnkgcAmX7vHUNwu+fMmUNWHlDi/yb3sMUZKhsdpwU4LcZWjXUOVHYHdO9k5mAq4sXZz1N
Whop33JKcoLoK4y61t4N1svLQyPeYyilbSdZsrKrnAnYab1CSHjmupd9wq/8r4keDXoQNG9nGWAK
6IZwAwIeP5Xgq3rsvrQMaH3/2d10Q687KTa+ZfqY9CerC4hOGoOqjwRHQvrVHXWNAyrHG9IBwYpJ
H+hC4MV8NhsfqbpqSSGdyniRYST9HbgUunUbpJxZGbI8kqAuk2b60bQZfxAOk+wPAzJMD1zAzXC9
jyB7h1Wk0kUv+MmPOXNKt5sCJCHrnnDk++1RckxwpwjWvbxGM5ZWo7oa/0sM/0gPszNW3vt+IjUO
/R6XXYfLdAdLYnPUp37bIvQC/YaOHwtFZ9Kp8iLF5HEtDs8cpUDZKPDXCBdOK3aKe+VKzaRWJv7Z
zF8H124+78oWl200pRvfkqsQRitPbfr5qVdj0p2yxb8wiB3v3avX+b3AtExzvKIcWhd1Uug0fih6
7CtyM0nI5VKUy8xxh+SiUCBeZbag1+qKzQkAGHiskwiw4ByCzBMaWWTVeoUhp4IILShz9s7nkOmz
AFPgN7kQdhQYHil2sO33dikpiJhbLATZofJBn4d9dWDmbkmPGWzOQwJGYo6y5XJK/5FKdh6vZUFE
h5ZnPGcpo9FJsglTgHy+wRQr+RFyp3wgtrLFIK0ISE9RjUQ3gvD3LANtsDnb+4Azv8MLdt3dGhQe
cvQTn7FB79ugqZYUU7nS2QAr+IYMkENdVkqNhy1MgU6lwH+zWq1QSQvErxxYRz+A9WmoglHxagi3
hESpANaEZYJ561Dua1iENbwVe6ghPoc6TEIJSHJkQ4JCWc0Qr3gczMYTyYMdlDB05g85eFzkjb3k
8VgByROQLWUht2d4eBwrzFLl9wS1q4dYDv8tIBPSEWtT2mR4UyY171ZkE2ojSaCcKiYrybYcA8jP
VKEY02SfBqnYy4zpR3MYSStLxaVSfaRdOzVCEfgXNe+dAmedbzr3M+0Wr00aprEMd62Oaz0mI19V
w7j1g4iPL82BDMgH7CZT8k7+0Q5YQ+Iro3rCsjs2y1JUCKRTKJIFKe2wg9h9Ex6sS/i2s2qU5xQo
0gO1GSnYv2vyBtJymrR/OxItSQHEWJnyEwMPcO5MsgkxvT1O+64dkTMhAsG0LYs0gwZgHhoyIaBW
YDi8tlPIS3ItS1ap0waw4R8AP+AO1Tp5HRfiNeA7Z8P7Qm+8scr89QRzjTq8J+iITiowAT/axAbo
oVyvRvoV6mXuW0VoG7ZNpfgroupqhmGxkEpD3wZ78P76/tnAHOzRng0ZshLN9leTAdNSlcFW3FGw
eRzgiBEIvz/qztSOZB4mptspxgxy3px66DeJM/KxDAIMa7+K6GFZC2P+yCeaTS2AEqVhC0my4yAe
Z0Z9zULkeO+XdiTZ1nsOp8mer5dmcw26OXjH2lbkiPY4sxYyVtrdiJQNtn32x8LttlWAj+lAISHt
pj07yInwpNQ7DfWzGGRaqerRv8IXLU38ZmTQ0+jxj7yPoWjp0VeOwHeQ7uTHLRScbMMBl3eyNfVV
ZeKzagGbrRo8V74cx6Y0MzL2BmXGjOoSy1mUiUDZFANR0ksqonbF22BYGg3j3kwJ02kuQZ0u/ucS
q3WoIqMJmkKFYNmseMn/dl/mR9T2mtEsydWRq2TOpU6x9+W3Zww8wm1Al9WTNcegUVvi74igmof1
OzhucqFDYh2+AL0h+sLGlxpFrlUmuzNMWTC3I8QPP1ZtNAWSWOQux0ZwPs2G+104jqpms2wu7Blg
gVmGCoz4wOMQYTVOg96C3Mf+vqM/s2a3uMIYnN3bua4wFIbnvJ1qpcLOxY7XyrqlH2zKMXTx/FZR
x/E2Qly1uIcgVba9zD5botiprMP/tYtJEoF6uUO/RXqDud5kZWCT88FM+wB2l8er9Vy9Bs8oPNyH
zGzdqKf+X8FgHc1gzrYbF5VgIxL5p4pfMkq6WGgHH4CiQz88NzVle//IpC4HyD6D6ny1hNOUb11h
02viKq4afkBD4xJSVzStpHiB3RGY3Q37Ww/3Gzj1hHVthtUEn2SrxrLkVMJwe44zRrE/JLvW1cmR
qDT5rm/QvATNT1eVi+lwi0PduGQLSD8bxm0cGC3W1KT3WvnWnvNj5BrjF8PMAGJEb/uUG/hFdjke
AnbbotpesVIdCtYzbYFrsXjIgjWQHfS7MTzByR3hbv6Ivon2sZO1M7BqmoE4gM+9x5Ca0CHL7wYK
R9DhMXQQbqkTNBTxnc4o8hRl14Hkk09BLruP6qSxc6AbxiHBwz+0cBJg02SL9r6HnUOTNUbaI6XJ
CcBhsSDfnDUuGzFfQzZyV+ReyEZbx+rv4TmEwYsHWAav0f0jue6OKA/nsDW5fiL9HA5H4mGQ/saM
17EN02tGJXqQYh7/Jm+CCXGpZVKQStsC07pmRorrIaMyw+sFmFNTDZbvQMhkM/kM5apBC8jrG7BI
UxahX845kYQod946rLCDWvtGti3J/xUTa2dJZRZLUMT2WcnmLQr7oALsztp9zNTmzV3yewZ75k+D
JEuHKCkME9MrqVb9rjYL3V5VTZadz3CtT+TnEE2P3tbd+i+qw5zTxCtTRDxu66iXqSzOSNAu13Up
3h75Esz4dhwFppQc8JgEI/+LCZTxG0FYkt9D96t1gl2l3c9tXDNzNTZkp1/7iZkK4bmyHQ8OfWbd
t/yYtZ5tUrXbJdoZgd+PJVw8BesUmCJi283ZyW7TkGsYT92pYKA+z/iDkTlzVd6d42z10/pfLyQT
tV3wptJTNr+LxbeN89CXMGpiKrjnHux1PLlfsb+W/460GSNmpeu++HAo5q0KEQ531ch3xAHyCxk+
IxgMYGwZgKPHudAPbIDmAw81GDtQL1CWpGmpPjyHNEqEHYwx7uCh4BGzCzxFcIcWREAyRxckXcCl
llE+j/KPjTMInf/nkvRH1WLrBwBhgL8vXoo5e2nsMdo9w4by66RrP1v6GepTkpBf9golaRS4+YJB
pppQQFzbMQ8hajX6hI80T4i+CoMSms0VhoF/r1z/lnZfENeVEwSSI3yTolF9fJoUCEv7EmL8blGR
yHNo35BE9FR8FJx5Z32R9MqDECx3LuW6bRa6s1T52xvg2wh8IAkHh2H/h5xtJ4c65iHTo0W0UMFS
DTYKz2s3AoRIo6J1N5i2HZp8m/flxYv8lpu7ILNxYcpJvSyRo4k5/8PZtHnoGIGv+fnLtz/FRZZt
NGNB3kuGRPN2Urx7oTj9UZ0rH0UnqDgHsAK7IwOysXMWMM+OMzgDzGgDkWb/jpI/+1qCfryGdyGE
oQHxroRYP03hM6pDfVWb9fDrW6QtHl7C2WfN3vnB0CKSTWBRCwWSQLPohcUd3DPVDs1iLoBB1290
NezyWSY3xEC5vSOzfLrYgBWpYQryePrzMhF7H/sSKgM4lJPdOI9i/XJxe19VqMFD8bkoXCy8bgTe
L5oBXYw6e95J5z+Sz9pkO693RlNcNeE6y/WWO5kyEn/EyrAWy0ReEX6MncC9Cna7oO0hKMZFEBEd
2qtV7ihbRLIhDyRlYZExfWt6XbA0kvJwgxv9kz+ggf+QtScvMAHIqHGvkEHmPkvO1f5UUYr3i7Np
nDTA2s4Gb174cWubs7sBvMi0WihudyiXQ7hzWKrnMQS34bul5rqY8F9NtsWOtUFPI22y5u6KvJ5H
UGrwwV2gp2rSDZ+fGEgNSQ62x75/n+YMI49eGMFwt1pJjWN5t7y9hgOEvkIwzMK8DkU6qJCtpYQY
0mFhARNrKyIp4CvDlisvC5ViDZulyHSKLxWoqFVJRdoNwgoe9w1fg/18vU4n3UcfTbMmT9EYTljJ
qUPNLG0iuyhR8TMtl1cQkYtms0xZgXyYNb+EyPyiJlm9UdiQXwd2h8FZfSaQG/+XxUt7NpNqwqNn
amCBjDt4TD8sorP+QFUlIpZSZeV86rvdjquglsigT/895kOKdSzQuWv7S1Te6AL5SGF24bEzE2FF
Q92tJZI8ucDeSWRKqkk+TQ5mxS4pERFXODqoBMk3FQJBVCZjucJ07wr4qgR2o6V9LKdHdE2ebTCB
17QgdTYgV9b4JESyewTWcEQRi8JrqiOJXdtoL+e0z5MpIq+11CNXZB36g1k1il6nBjwfdU19TBHi
mvV9/wH53I+B3WyRmaE8FD9UyOvRle+vQlS0T587ms4etnwZz/kHWZzbf/thTcEr9BtY6T7si7km
dkuhZOo4EIKzO7phYWJOgtojuPLEfKxFT/aaYo/TS3HgpT7DqywPG+t553iFNTZv9R/5ujFMqb5M
D/odU2MZQLuLLa1YvO6T5tu50cq+tYXqimMX6jyAYIn/8UFagpE1socbGmDr6ELcrztYxOmtZ2pw
5XuoIm5YP83C/Je5XNyeCtbQCh0qoohuc9YNAzluaaPWfVfafut3YjJ1bq8TQOri8v34R5UPiDDD
SKZryWvsFWlpFDiaCz/PP0uLjGzlKtTWL40nwfmnVWiDzhIDhZPZ4j72WhVR5DD/JUEyCu7Hq8ib
EhmAdW05V9h5ZumpI44Y+Z+qLkHIg8EAkv1BP1PcnHhIZnl1yVCXSS79D3ALbTchykXSI94zs2o6
5knJl1Vm4Je2ZHIRUBhNGxTU8D6cFy9TNjHqs5Z7Wi29eyh3MuKontO3cFbPVxKGzJqHFnSF4el0
HrqUcMqNPWepOzMwGRAjmjTiYbwkFHzbekG3iRXQojWp9Ob3Znrd0a88k0CiKR5laSnqoAYISZzF
HGa4f+6mzzEi+uE1v1W4GrVCj9zTejH9m46cVaZ0hFdQJzHELMvlk8xUKeIWA1soAVf2e3HDYA/L
Saf1CDV8ITd8Xyt1JBxWpQrr7ODfNjqA+EUkQ+AHTpB1LV18AL+EognaLNfS94cKD63eLVqwj1eI
IfxIyYjMsZb0w0OkzraVVu3k3HB9La5GhA653dRFMXCKBqcTRmMle7Xrs94Jafq9beY02WhxJ8UV
VvnYVxa3RKIiC+IznK716D9k25Pr2I7U9Ug9NKVquLN7O9svy2iUE7C7aB87BNNLd5emjZrkHlJ8
1tSwDKjIhPOgdtp1d3aoO+UaIjk5scszbTO86GoM8HwIri95n1o+mIv9a351RCZpioXIlEp3cIie
+2nbKUGceD68s0hQ/mOQpxZp70gnXNLixh37QUJjxa/zBM2HWyoA1VuAQS9QXu6oOR7tZinL25g0
Bvg65yMVx5maQKw9Cl8BIaUqAOpM4RDNqfwIy8XuiuIDdq0JtDJWT9uMSKxqVFRZQyJpmy8zhfCT
B3/USapi4ApfRkbfHKTEHcnPb5IYOaWJBiEuXX7842wtSM55b+lQtMNoQQT704LpG/ln26vlOqT0
DBU4JpWQNjrIOeVC17lw56CwYaUfavtzi0g1sjEN4ipNoeinM2x6zMcG2f7I6yL7MuDHpeTkkhIj
UM7Y3FJ+S0kmXF+3TwoqJf90Jh83pTQKnBdsvUvPDeV3SKrw4N2YujmIcKQYGCkg+r2Wt+/AZQXo
z+rKDUKhGMfiqNkENheIvLvSIkoqPlD/Zl7BY83WWb0XSQG9JN3dRbYgkyQ2MwE+H/T6iVmqLLI+
RxZEkxOUmXvX/GhQGtqmdR+bUJhCHa9sFNRpX2c4tml6cs7ku4HfaD0upSymKZ21RibWZyNK5AxD
7sM3xtss6cQxJU8L6q02OgELvWDPwwWgqAcZZBmCmY28CDI7OaB0s0/sy3NBiJoWrlkiEbG3xqaS
AiHwDFKzHnakTz4hil4/KGbWDvD2atGgv4dKnT+uLZFomYY8UsR2M02JVFV7V58vja0lwK03Wl2W
HoAn9ktqqncYc3fclawlfVt2B0NcUiZITTcas6revbym2MFu8dcSshV32llczuiose1hdOkEN/tg
Qw87iyxfSuOWt6Soqcv3gc4LGks63TSlj/JedNNrBdrwRUJcI7Rsh99jBFMospxT66+vLimNptQx
c9aGv6MfJr8L3vZtKZ/5BftPy2fPzUQ3FBpdfSDuEkGmc6g1G0JKMmP5Pgd3c7Ru+fC/MYMFqbzy
v7s9jLmqm0TIpFp6VVP1I4juTPEAXXeFKw0+Iy7JxQmqX+XqgPl/JOTXhBoPAgo+d3W2gb7J8PnG
uaE3lPcepjllmvJhKIg5yS5Z66/Mw0B3PV7pg+0/6T4PQqItRgipUSfK/P9GomHUjobTnKSj6giy
c6O5IjN/QQ9nnWKfsf/s8+ku8Lq6+Glt43UscYFIiJUhPLUxA6VC55Br/kLiAMPFdm6YzBtXh57B
ZBCXXgVVSjKWgbecPB5KVJqJPcxGTMUM8bi7EqN2U5MVMUXjMr1baoHzmRTcdYMLwAh0H7/bjbfx
zxjq2CiQ8LTFLl8Wjh1bVIprdwr/2yyPIqvYibsJfwds/CG7AjMp0q6PgZbWlGDEZ2jAY6k4Sso8
UV5sxUGuTzOMit/QpY7QjjGQx6oglrN4PHTwCjyJ2k4UzBGnjKig54mxa0eduBkjjPBXMVap1Zh6
8Fn8ZtRyY9OJxwzOyGqy8hMk29WiIfbAdQBr2GnJmSXpJmwdIZBX/Qe5+XXj6gsTo4u9bfgpbcAy
CBHofW4mVhuqtx7k3qrlWjdgUaSWLIn7dSz2nh5PIfE9DsBkYAvcHgqvqT2bSLtVXL9HZQQk68zf
tNftVH/xowAHfjG5+M+t1ayUEp63xnGuBknfd4rhHBHwhKjfgVje8S34SYL3zHbG47s9a7VHXyyK
do8nV3QBgwlJvCByp9hbWLL8nMzXQvyVIRCdXKuN9T8On9Vkq0Nv9eGM9utZA93d7vlTbli4Lru/
d54jrlLmDHDoTejfp2We8PjR2muQqzYsUPZQrfYfWn9LAppl58qGp+0kmgQXOZLx84qKszgymO2L
uIwgj/SEkl/VxtjG83+/s49a8lFnigvMxZiiWdrsbdL7fM04McvdNWTT5RyjDI3E0QdYi+0OjaXk
97UsjrA4OFzdQLg46ip1qAS1w3/H2bOnzCW1cK333H88a3nuziekRsSxn33gO3r+zbr0vhtLFI8h
VpN8NTXHvVEYHNbQdyEePCxz2GR4322LqKaq79xcVMzOuuFVI+o0g0T11Jlhv76sw8tJLJPpUuhl
6FCfDKdWWQseqL4AIwhVlbJNU6G9ME/6apflUcXDBLjlO3haZLGUFdBHKAQTR44uvf1t8ckE6S96
TRwb/XaajSaMItPEVbJqy8h3JprANrgVUgfji0mrIswPDyjTjaDsgusiivym29ltjAJLJRROO0ui
Wmj9knjQ+b7l4zepJUTFgSCNnd1KwBBDQehAMamIHKukyjTlNNBuW4JJk6196OFR7gYJbWgcHwtU
Y6wgXgV6+j2imLRA3UXLio0dJdDyMCitZ6GBEikX2CayMzRywR1xutMe7yfnbZ703tAeOw9W41YI
/C2io4A8tI0QK0Honzidbbvo3OnyfxL6jmwZTT5Ej04zwirqAdFA7JBzSW/JzFTeJ7sAXQpW7lES
AWdwnu5OmMcsh5y/yoHT4Og4VfVAT8vzq27pDgyTyV/e3H1rnqKdMXGMfvAZoM/Zd41gRyMNnu4u
bgDQk1inoZPDtXyrfKmjle68LNRR2H5H0Nbp5D2l1uh4Yyeg8K39TO8PhHYmHjdsufEEZjQLp/en
MJlpZhDOYBZ8mEWdIjswpIlz3vgToF46ZNPBIGjtn5Xei6B5aoIH9V5zzpkC5IOv3XeXTyeEPcKZ
1H9cX+imxETLs3l7D0Nl73JbJ1d+/mqYzgSKLrSL9B4PB/Xbl/cJAP8v09vDivN+htzg1/piGsL+
RM+STobaSht92goA1lKEqOUrBdf+u4IA2pYD/VYUtd7XG+ygp/tf/IMseGnac+hFOKSLSqUFC3yn
Vncwow3o45JDX87VHnj+mH4+XHaYqXnEJYzwMVpCUlKK5+NZUI2B7cSV5aZbtIRqqRB4vl9GlaHS
Cju4fFGCE20YDKhOvb4gxuDBIxhRdceubZnwSj1dnsuwDUp3jpfh3H5RCtoFDOLXsunFUejEDwLg
+PmXeatdnnx0VspKFLB/gTMvPXOgPMKZjt2JwDrSxKWTalhVeHmiCoiVV99/d+ZVGxyESIy5MFwo
KdlRmE0IUdsLRf9LKCt58cE5Gz3WFm9x6/sRqzX9KJOp3VMibGihx6+M/p4a/aXWp947EItLI/1l
899NqeKuJrejxes3+u6RMd2KN4PM1KSWEN07CZwe01ZkKknEful1dqKOp8R4FHVbb4QlfYsdAMar
npdwPriaiSfrT2IMVnOAXnRjXi1iE1L0xqCFRKo0pu7mQH7uHtICXv+tGMUkIKA2Vqqkrr5virGo
1UV/zxyORgee9uUq3LL2DUJda71GuDmMaPf1zJfpLOu2Yqr6Orf8ZkyVX4E6J/K8aFN28hf3g7JP
GwWzrB6EKfmmRonGLke1Rvi1vumsKxtVpJCEVLHmjvCsPbD8Vhc4AWHrzjNOL7a3XAmPHYg+YBZm
JXAVmreejazLvQe6fWpj6GBD7M7R8erfPWwv7CJBoNmJV9AkI0uA6/aNg0VQyt6Y/7l8+70MiyrH
YmiuKqJjZNxImyVLLo9ViElDbdOOEEjzqwVKuRlEioemQSQ5kskYUA3CXvtmxGCINQL5swL1Q0ur
crestWaxkk0tTBQLsPygzmVG6SRETDWhtt1+KcTvcwFBGKC3967VwY+iorl0xp3NF42l3KYyYY38
kfDwDi5rb6sUS/oC7qRwyqSNulUz3RLP22VSV6qnL1n5fdgwHoypbMPjuQ2Fk0oN7cXlfQhWxnVS
ltDmh+AWehRBQjTimzq8B5YL0KybdmHppokx8hhDo7vTwd5VWdrC79SRo+dFFoha6sUcyx5x5Dvj
sm6g4hCvB2Kr1YE3+K7divoJnLSijG38aRgFPhixZa0Xy8SoHyYKGDuWSRhUTSpmgGdLjI6THOUI
WLpjEQUQNhuhPXtl3bpw//Vs4PN6Eb/hOlJj2B+ctilgKk1oV+el2MdyTG4vMdk3UCiYBc3tbkdI
VrRjIoqRaJlaqrzhPMq9hOXUlGjFMDEz7G4eHzoKnfPIkyAKZRKe9/GIRBwFZqrA0edkX5t54ScD
9EhBEHJSYahe20K/WrpTUu4cvIWALKO64IEwWze2ig/X0g9cF6W8jaCBljRBSVP4UbM/vhjM9e+3
/im4Xe2KnLyk0ADov+fFjsaaGKnXz2egME9XNrca42uw4g4yC4Npes/NnKcAGnlfxSeN4VWXcVWq
XFaTcSEbrZQxIFcHPmWjc1ue8S4TF7dh11zG8oHkvZnSVpfftzeH9HczQI/Qyx/ytGqtTxUAyvKo
POR9LVfGryqPrHG48WcGmdypbcY3efxhM8ctZBJkKMJSPz+GrsEEm7+pmjZb5xOI6+ph3fKsgk7+
K/resY8Syohh0aif3QuoqsDaz/w0+tueTBHtMvb3fu4nyTiLIwDlqfd9go7bxU6i+tHFxjeajf25
a8fKsqta7SHaI55nIMjunGNw+9xW+VR0uiJ6R4zvCfmThSF3EnN+qixc/nA7jacJOAxkwytjKjz0
HadW+kIkOGcqlU/S/u/tu+znM4QbCYmJH+JGnpCmnqCKJdbGL0IpWL2nOD6tgHoAEQo1MO2yD2pm
zQskJjd6/DRa0sozGhBU3Aqfl9frNvs58hY9a0btSAJOnE0uyjBgpu31IP0ibvXTv6ZLeEX6zz/D
NKjROprNnhMdd3rts5P2a2zBRSxvg7QQ3GTo+ziqAi5Zwlh9if0ktv0srAlSYEUEdjFOykEtIhuT
kT2DucxLguiJIl8PAVRZJJifgdhtSo8pcPqwfnC+9bjEaJAl0eUzeBBQozWPUKT5gz9QyXfrHHBr
kVozdKdX3VPg18JmdIMn5HQ1UhQ2Xl2ynHTtGRifizX/Ds6f5+B4+75+UNyVwxQ7dDWA6BaYHNEA
GzfrU1ccvZ7XpGThpkaqAwI2kqngmGn1ITvTF86On7NJsHuYIs9c1HLVLVZ0gKfai9uOaxhPp7Tj
4GAu+5aBDv8rdzFRkQNUNKp9gmlO1gepdVepG04vjDFpBLgX0+gfzInpfAH0tKVcdCJbcZ11euNo
s42h/TIp9eg+06Ea2fZeEuWOz+K5RU7kK22nRytMEnt3rEQfe0ZgAufcSL5pRYfo6ECRdbMrnL1Y
gJxnpVpYTHuIjLitXgVk5qBmJNHCjCwFZv94ylBA6u2544AtvrqdsnY18qHEoGsd6TgrbpNdshBY
QDiRExMSo5PybtceUG8XjANoF+9+CPS78uklnYninJMlDZ6k0nWk7A2u20Z36rDoXTlh1OIPcOAu
1/i3ObBtBVNOwFOwUk79KV69EKIqNRLfrCPDZM/7RwRZHgmyRjoJqjpwOHLRMWhz1LEpGyPsTO69
VPbhrtbJmlijcv4fkhdOZAJUBokA3F79Hlc0tJQjG/6YKKJJsVdcPApmiF9gLd7D4k/bTc7T4wBz
wcyBbPbqq6G6pSiNVqD6E0hPyzIKJ9o0cyq9Ou3hGvyiqYqT00mAgoyDla5Th3prM4lSt3dRqHhZ
FrYrFA0NNrqWQwip4kWp26IkfiTfvjMrpaNjtFeUfBiAi2OPNq1tjJ/Uc+PbF23F7yNaPE5NytxX
qcY5ZRYMqbTEONaIhYVRxyF54zU1Row+H+XCCLmdPu21hZrAs0WTiBVAwk+5JdppeegK7AJFNJq0
g9YKT6tpYfYZE+1VEHAnMVpmupWp6jAM2TbLD/8JVf4fHFi9aGxa06BsVcoQKPTESOCBwncP8hoc
2ha6q7H0tDAeGRLOXLDUCtY2/O5iNMJhpQpXP1AcsykMf9Xtbj8pGVoz4Po7Z2InGIis4Fi2EGq9
Cxgjy9wB+yQ96kjBDbngIlC+ISoNE786LNhHpvy/zmsSGd1w7M4BBaXDe0SG7mcpCvgGdF7Oo1Gy
cGExaYxvnrLZXCDGLKMgYJaGrqjcdA4P6XAyDNJ+dcBFgglFsmcyUheW7rs1c+orEu1lF0xph20k
DybX5pYj5a66Pqzj424cb1rxc7UwGJFcTXhNJ3HLI3pDYA2K8QVdl+C49UjsNPiQme/AMlcfg5fU
Tt5fb/pnlGAE+MXGo7aehf2QxmUSVxHSPgW8gt0lCAErxHueMakF3TUqfSrMTSWNw6z65kpFfv7s
WA7RE+gcPq/DhYticCbDkj5r0ZPqXjKeouRag0d+EN89eVAziIWEtmiTn3uy25ax16yWpiux9cOy
fsc5r/20onp+zCD2theMkB85AHGAxNywhuR7ILZPZlSlzkzkCTAl9CYBS/ntXjJ5EMiizJ4uTx+D
AoneEBbXn0JKFKnrpos5TDbArCVZf+gDmQ1D4y4EIsN/+MDx51sKZ9p9AfaQT63b0upoXkcIEtDI
Uq5AytPGVWrzllxWfveW03RE7n9Mb+93RsRJvb9x5wj3EBO7nH7hnm9fIpdaE4VFW0SYfB8ClWI4
+de/8jzPYSXAZlDordnPuBzpK5r36Osuibs6rmWAD/bMJtkJN7/AWssAmJnR3PlSp3maxg9PUJR/
dxwFIEeneCPYMtaitPY9ZzXEBMR3h2aDjmHPYqB8qwQD8JszMgQCm0+bsOaXdTjTHDBeB2yHBMsJ
wkRYCpr3YFtZaCYtIPJK++QXW2c0APbjBNquQKNlY3xeOYdjnQpRvFJIRMLUk8vt/1+5dL/nl4+q
1ES4duetAc5aj68M1QSeHdBIdEkU6usz/vX3kNIrKYPseBNjL6jssdOICEIQ/dFz1pztywtg229C
R5tyVdBrQ3JNjFsNcNQVz9jtBaIug1pcPvnaifsDfx+i8TlfqgK52qYxWWbcxCNXcWnqY57kTQ1N
uCq0aLw/zIRFOn2Ruj7+F5I7BRWdmY1/0kyMTLm7uh4SFAWHYmLv7PIK6npNSN8fhj9kQv6ByQbm
H9XGR0vcN1ggES3BL2dFVMgH9bDmrytgrYkPb+uPuTe+tYMSGQdwANYzmJCicmJ1DbJwTJBsTvfn
RTXSb37TEIif2Z+n3E9y8KtLj1hTrZU7aIRQ4S4vOHMKjoffl5ncgNLUTcVlx85nHxb86C9TMjAq
OB9/OBkHrXU4FrF28picQMZ+NLV2i3SFzSNwlzJDsGiD9O8tVS46MSkUEcHkUS43xwloMWV8digH
AYLzgrH4c77UQj7c40A6nmyyCxVQAgTllSoQby3org4gAoW2V98VomhIT0qbvCSXzNODWDVg1u10
AvE1VuJ1vk/sovU3ZkvJDiIHANhLgrPIIG0jCGxjXOzK++hI8BJoKoVNahySpui4/E+973rf9cRo
O/4UmOOVW3d7mzts/64oP6w0dJcAi7YXllsoSbgJ898SmojTS71mfF25k/9IXfGJv7FscQunTAA8
0PuUfNY8Cl+V1hEXQJ8fPSZYfBVKnMARXHhaGrIGLxxj6UgolVs13Hprcwer0dEzigt0s9WxsB0Z
Gj61AJi7T6V+BLRcPUuWV4mX/Q8hFRCmIM++xBDSgIh1eIuBtbJ+VpTSyPjn7HKoCR881NcTeNyM
Bf5+yInblIjwxg4KDNpy8tp8sppn6pVSPi0kZSLeeUKeTmZQql/RggUTWJpc9Qob9dANUBhPBPQ1
XYMPNJ7/k/6KYKsBzghn5A3A+aSb1kPQyL5xpOATbNk0v8sPmY/g0Rak9cQdDpjIoVTrOYjrpjeP
EnS3R3kg+h9pT5sqCL9bvA+8uyVkzOWB9D/LjSCbAiGGg7HqzIg21GdIqV97s2jxbwi0Bdgp4zah
rlMsq8nYMoisNLF+nZ4mwXbWKW5wVyabG6T2SgsmrtpwJeGUYd+ZKg/djC1UnVLox2TVsPgOoUPF
GsldoS6xB1JTwyx65ZxrXzbzkd6qWUfuXcPmeds9eLe12+spcsiZ0s/JROJEvqS/+DGyuo089tN/
49w37hWuFv27rQct/f1r2qkUynH8XfCd0/19BlK5Zgbx4uy+5trFO+bFqHmp/5GD6uKfhkNbbHDz
wykZkeB9XItMV5JvWMM1roJK0vGma12f/i5u+/hrY2GnNu3lsrxd4CedJAUN6VRRNmAKTnOBUOSU
EN4MxYVzUN2SlwVmcKq5IuRzZsfNa9Ct0sXK/48XH6VNHo9X2JO7s8LP9L7EHxYELopm1gftArBc
zu3RUqzBGV1p5iVBEoygRZ1s2eQ8wR7fdA9NpBroYysc9j4VKk4smhbH3o/rD5Uev6anL73aOKAD
tamxRyLy9WjUz7WKWWz6RAjLCItURcKtGcX/hMMbvwea3ZshxnwkhdLF8RCHLAkrYxzicC/E0nKE
5HjxTWYz5MvyqxYO/JSbvZKwICWGZI8lC5ZxTZUN4g1aRXUk8FDC5s2zj88rJe8F0EZGuJSaWzNG
0ToWSD9K96sIr0+BHvhGnI9dhN6doK9e9oDVqWpoInvxrQV6jFPtQ0Y8Y5XL3b4ZFWzK/YqZtngI
YVl8Ky7PCFooPx1lTTaMH7dA9h1vm8gr6J9NVtTSftPfNAlZOeYm5I+j96ddY5s6G/QZERoZt9uH
KDpjeELdI4BlP9G+ONyi/ofK2SUY6HBSiJy6XQqoMkUUiQjKKKlG3X3N+HrEwvxqrZq0FHcOxEhJ
r3kA1MGUBiok0sEwBKPdFzNNrv3EzoSRk99oCTrtAR5ZosatCJlb0bg8ixemRRfwTE1R7JfuybCH
D8w=
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif

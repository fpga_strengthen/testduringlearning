//+FHEADER //////////////////////////////////////////////////////////////////////
//  版权所有者： 
//  Copyright 2014. All Rights Reserved.
//  保密级别：  
//---------------------------------------------------------------------------
//  文件名 :  can_init.v
//  原始作者：丁云冰
//  联系方式：
// --------------------------------------------------------------------------
// 版本升级历史：  
//                 
// 版本生效日期：  作者    版本
// 1.0 2014-01-13  丁云冰  第1版  
// 2020.6.8 王雪
//---------------------------------------------------------------------------
// 关键字 :   
//---------------------------------------------------------------------------
// 模块功能: inital  mcp2515 chip
//           主要依照mcp2515 data sheet用spi指令对mcp2515芯片中各个寄存器进行
//           初始化操作    
// --------------------------------------------------------------------------
// 参数文件名：  
// --------------------------------------------------------------------------
// 重复使用问题：
// 复位策略:负跳变沿异步复位信号 i_rst_n
// 时钟域 : i_clk_27M 
// 关键时序: N/A
// 是否需要实例引用别的模块 : No
// 可综合否 : Yes 
//-FHEADER //////////////////////////////////////////////////////////////////////
module can_init
        (
        //time & rst
        i_clk_27M,
        i_rst_n,
        
        //input control
        iv_set_id,
        i_init_state_en,   //mcp2515初始化使能，高电平有效
        i_send_reg_ok,     //脉冲信号，代表数据已成功写入mcp2515的某个寄存器中
        i_read_ok,         //脉冲信号，module has read enough bytes setted by parameter
        i_reset_ok,        //脉冲信号，中断寄存器已成功复位
        
        //data in
        i_reg_out,         //读取寄存器的数据
        
        //output
        o_can_init_finish, //脉冲信号，初始化完成
        o_send_en,         //电平信号，使能can_send_register
        o_read_en,         //电平信号，使能can_read_register
        o_mcp_rst,         //can软件复位信号,低电平有效
        o_reset_en,        //电平信号，高有效，通过spi指令进行复位
        
        //data out
        o_send_add,        //寄存器地址
        o_send_data,       //初始化数据
        o_read_add,        //读寄存器地址
        o_read_num         //读数目
        );
///////////////////////////////////////////////
  `include "parameter.v"
///////////////////////////////////////////////
    input i_clk_27M;
    input i_rst_n;
    input [63:0]iv_set_id;
    input i_init_state_en;
    input i_send_reg_ok; 
    input i_read_ok;     
    input[7:0] i_reg_out;
    input i_reset_ok;    
    
    output o_can_init_finish;
    output o_send_en;
    output o_read_en;
    output o_mcp_rst;
    output o_reset_en;
    output[7:0] o_send_add;
    output[7:0] o_send_data;
    output[7:0] o_read_add;
    output[7:0] o_read_num; 
///////////////////////////////////////////////
    parameter IDLE            = 26'b00000000000000000000000001,  //wait for start signal
              RESET           = 26'b00000000000000000000000010,  //send reset signal to msp2515
              READ_CANSTAT    = 26'b00000000000000000000000100,  //read the state of mcp2515
              WAIT            = 26'b00000000000000000000001000,
              WAIT2           = 26'b00000000000000000000010000,
              SET_TX0CTRL     = 26'b00000000000000000000100000,  //set reg  
              SET_TX1CTRL     = 26'b00000000000000000001000000,  //...
              SET_TX2CTRL     = 26'b00000000000000000010000000,  
              SET_TXRTSCTRL   = 26'b00000000000000000100000000,  
              SET_RX0CTRL     = 26'b00000000000000001000000000,  
              SET_RX1CTRL     = 26'b00000000000000010000000000,   
              SET_BFPCTRL     = 26'b00000000000000100000000000,
              SET_CNF1        = 26'b00000000000001000000000000,
              SET_CNF2        = 26'b00000000000010000000000000,
              SET_CNF3        = 26'b00000000000100000000000000,
              SET_CANINTE     = 26'b00000000001000000000000000,
              SET_CANINTF     = 26'b00000000010000000000000000,             
              USUAl_MODE      = 26'b00000000100000000000000000,  //turn to usual mode to wait for sending message 
              DELAY_SET       = 26'b00000001000000000000000000,
              STOP            = 26'b00000010000000000000000000,
              CLEAR           = 26'b00000100000000000000000000,
              READ_REG        = 26'b00001000000000000000000000,
              DELAY           = 26'b00010000000000000000000000,
              RESET_DELAY     = 26'b00100000000000000000000000,
              SET_MODE        = 26'b01000000000000000000000000, 
              RESET_DONE      = 26'b10000000000000000000000000,
              //
              //滤波配置
              SET_RXM0SIDH    = 26'b10000000000000000000000011,
              SET_RXM0SIDL    = 26'b10000000000000000000000111,
              SET_RXM0EID8    = 26'b10000000000000000000001111,
              SET_RXM0EID0    = 26'b10000000000000000000011111,
              SET_RXM1SIDH    = 26'b10000000000000000000111111,
              SET_RXM1SIDL    = 26'b10000000000000000001111111,
              SET_RXM1EID8    = 26'b10000000000000000011111111,
              SET_RXM1EID0    = 26'b10000000000000000111111111,
              
              SET_RXF0SIDH    = 26'b10000000000000001111111111,
              SET_RXF0SIDL    = 26'b10000000000000011111111111,
              SET_RXF0EID8    = 26'b10000000000000111111111111,
              SET_RXF0EID0    = 26'b10000000000001111111111111,
              SET_RXF1SIDH    = 26'b10000000000011111111111111,
              SET_RXF1SIDL    = 26'b10000000000111111111111111,
              SET_RXF1EID8    = 26'b10000000001111111111111111,
              SET_RXF1EID0    = 26'b10000000011111111111111111,
              WAIT_INIT    = 26'b10000000111111111111111111,
              
              USUAl_MODE_delay = 26'b10000000000000000000000001;
//////////////////////////////////////////////
    reg[25:0] cstate;
    reg[25:0] nstate;
    
    reg o_mcp_rst;
    reg o_send_en;
    reg o_read_en;
    reg[7:0] o_send_add;
    reg[7:0] o_send_data;
    reg[7:0] o_read_add;
    reg[7:0] o_read_num;
    reg o_can_init_finish;
    reg o_reset_en;
    reg [15:0]cnt_delay_1ms; 
    wire delay_1ms;
////////////////////////////////////////////////
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if( !i_rst_n )
            cnt_delay_1ms <= 16'd0;
        else if(cnt_delay_1ms >= 16'd25000)//  10ms
            cnt_delay_1ms <= 16'd0;
        else if(i_reset_ok || i_send_reg_ok)
            cnt_delay_1ms <= 16'd1;
        else if(cnt_delay_1ms)
            cnt_delay_1ms <= cnt_delay_1ms +16'd1;   
        else
            cnt_delay_1ms <= cnt_delay_1ms;
    end
        
    assign delay_1ms = (cnt_delay_1ms == 16'd25000) ? 1'b1:1'b0;
  
    reg i_init_en_temp1;
    always@(posedge i_clk_27M or negedge i_rst_n)
        if(!i_rst_n)
            i_init_en_temp1 <= 1'b0;
        else
            i_init_en_temp1 <= i_init_state_en;
      
    wire init_state_en_p;
    assign init_state_en_p = i_init_state_en & ~i_init_en_temp1;

//////////////////////////////////////////////
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if(!i_rst_n) 
            cstate <= IDLE;
        else 
            cstate <= nstate;
    end
  
    always@(cstate
        or i_init_state_en
        or i_reg_out
        or i_send_reg_ok
        or i_read_ok
        or delay_1ms
        or i_reset_ok
         )
    begin
        case(cstate)
        IDLE: 
            if(init_state_en_p)
                nstate = WAIT_INIT;//RESET;
            else
                nstate = IDLE;      
        WAIT_INIT:
            if(iv_set_id != 64'hffffffffffffffff)
                nstate = RESET;
            else nstate = WAIT_INIT;
        RESET:
            if(i_reset_ok)
                nstate = RESET_DELAY;
            else
                nstate = RESET;
        RESET_DELAY:
            if(delay_1ms)
                nstate = SET_MODE;
            else
                nstate = RESET_DELAY;
        SET_MODE:
            if(i_send_reg_ok && (iv_set_id != 64'hffffffffffffffff))
                nstate = RESET_DONE;
            else
                nstate = SET_MODE;
        RESET_DONE:
            if(delay_1ms)
                nstate = READ_CANSTAT;
            else
                nstate = RESET_DONE;
        READ_CANSTAT: 
            if(i_read_ok)
                nstate = DELAY_SET;
            else 
                nstate = READ_CANSTAT;  
        DELAY_SET: 
            nstate = WAIT;                          
        WAIT:        
            if(i_reg_out[7] )
                nstate = SET_TX0CTRL;
            else
                nstate = READ_CANSTAT;
        SET_TX0CTRL: 
            if(i_send_reg_ok)
                nstate = SET_TX1CTRL;
            else
                nstate = SET_TX0CTRL;           
        SET_TX1CTRL: 
            if(i_send_reg_ok)
                nstate = SET_TX2CTRL;
            else
                nstate = SET_TX1CTRL; 
        SET_TX2CTRL: 
            if(i_send_reg_ok)
                nstate = SET_TXRTSCTRL;
            else
                nstate = SET_TX2CTRL;       
        /*SET_TXRTSCTRL: 
            if(i_send_reg_ok)
                nstate = SET_RX0CTRL;
            else
                nstate = SET_TXRTSCTRL; */        
        //滤波
        SET_TXRTSCTRL: 
            if(i_send_reg_ok)
                nstate = SET_RXM0SIDH;
            else
                nstate = SET_TXRTSCTRL;        
        SET_RXM0SIDH: 
            if(i_send_reg_ok)
                nstate = SET_RXM0SIDL;
            else
                nstate = SET_RXM0SIDH;
        SET_RXM0SIDL: 
            if(i_send_reg_ok && iv_set_id[59])
                nstate = SET_RXM0EID8;
            else if(i_send_reg_ok && (!iv_set_id[59]))
                nstate = SET_RXM1SIDH;
            else
                nstate = SET_RXM0SIDL;
        SET_RXM0EID8: 
            if(i_send_reg_ok)
                nstate = SET_RXM0EID0;
            else
                nstate = SET_RXM0EID8;
        SET_RXM0EID0: 
            if(i_send_reg_ok)
                nstate = SET_RXM1SIDH;
            else
                nstate = SET_RXM0EID0;
        SET_RXM1SIDH: 
            if(i_send_reg_ok)
                nstate = SET_RXM1SIDL;
            else
                nstate = SET_RXM1SIDH;   
        SET_RXM1SIDL: 
            if(i_send_reg_ok && iv_set_id[59])
                nstate = SET_RXM1EID8;
            else if(i_send_reg_ok && (!iv_set_id[59]))
                nstate = SET_RXF0SIDH;
            else
                nstate = SET_RXM1SIDL;
        SET_RXM1EID8: 
            if(i_send_reg_ok)
                nstate = SET_RXM1EID0;
            else
                nstate = SET_RXM1EID8;   
        SET_RXM1EID0: 
            if(i_send_reg_ok)
                nstate = SET_RXF0SIDH;
            else
                nstate = SET_RXM1EID0;
        SET_RXF0SIDH: 
            if(i_send_reg_ok)
                nstate = SET_RXF0SIDL;
            else
                nstate = SET_RXF0SIDH;
        SET_RXF0SIDL: 
            if(i_send_reg_ok && iv_set_id[59])
                nstate = SET_RXF0EID8;
            else if(i_send_reg_ok && (!iv_set_id[59]))
                nstate = SET_RXF1SIDH;
            else
                nstate = SET_RXF0SIDL;
        SET_RXF0EID8: 
            if(i_send_reg_ok)
                nstate = SET_RXF0EID0;
            else
                nstate = SET_RXF0EID8;
        SET_RXF0EID0: 
            if(i_send_reg_ok)
                nstate = SET_RXF1SIDH;
            else
                nstate = SET_RXF0EID0;
        SET_RXF1SIDH: 
            if(i_send_reg_ok)
                nstate = SET_RXF1SIDL;
            else
                nstate = SET_RXF1SIDH;   
        SET_RXF1SIDL: 
            if(i_send_reg_ok && iv_set_id[59])
                nstate = SET_RXF1EID8;
            else if(i_send_reg_ok && (!iv_set_id[59]))
                nstate = SET_RX0CTRL;
            else
                nstate = SET_RXF1SIDL;
        SET_RXF1EID8: 
            if(i_send_reg_ok)
                nstate = SET_RXF1EID0;
            else
                nstate = SET_RXF1EID8;   
        SET_RXF1EID0: 
            if(i_send_reg_ok)
                nstate = SET_RX0CTRL;
            else
                nstate = SET_RXF1EID0;
        SET_RX0CTRL: 
            if(i_send_reg_ok)
                nstate = SET_RX1CTRL;
            else
                nstate = SET_RX0CTRL;       
        SET_RX1CTRL: 
            if(i_send_reg_ok)
                nstate = SET_BFPCTRL;
            else
                nstate = SET_RX1CTRL;
        SET_BFPCTRL: 
            if(i_send_reg_ok)
                nstate = SET_CNF1;
            else
                nstate = SET_BFPCTRL;       
        SET_CNF1: 
            if(i_send_reg_ok)
                nstate = SET_CNF2;
            else
                nstate = SET_CNF1;      
        SET_CNF2: 
            if(i_send_reg_ok)
                nstate = SET_CNF3;
            else
                nstate = SET_CNF2;      
        SET_CNF3: 
            if(i_send_reg_ok)
                nstate = SET_CANINTE;
            else
                nstate = SET_CNF3;      
        SET_CANINTE: 
            if(i_send_reg_ok)
                nstate = SET_CANINTF;
            else
                nstate = SET_CANINTE;
        SET_CANINTF: 
            if(i_send_reg_ok)
                nstate = USUAl_MODE;
            else
                nstate = SET_CANINTF;           
        USUAl_MODE: 
            if(i_send_reg_ok)
                nstate = USUAl_MODE_delay;
            else
                nstate = USUAl_MODE; 
        USUAl_MODE_delay:
            if(delay_1ms)
                nstate = READ_REG;
            else
                nstate = USUAl_MODE_delay;   
///////////////////////////////////////////////////////
        READ_REG: 
            if(i_read_ok)
                nstate = DELAY;
            else 
                nstate = READ_REG;  
 //////////////////////////////////////////////////////// 
        DELAY:  nstate = WAIT2;
        WAIT2:        
            if(!i_reg_out[7])
                nstate = STOP;
            else
                nstate = READ_REG;
        STOP:   nstate = CLEAR;
        CLEAR:  nstate = IDLE;
        default: nstate = IDLE;     
        endcase
        end
        
 /////////////////////////////////////////
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if(!i_rst_n) begin
            o_mcp_rst <=1'd1;
             o_send_en <=1'd0;
             o_read_en <=1'd0;
             o_read_add <= 0;
             o_read_num <= 0;
             o_send_add <= 0;
             o_send_data <= 0;
             o_can_init_finish <= 0;
        end
        else
        case(cstate)
        IDLE,CLEAR: begin
             o_send_en <=1'd0;
             o_read_en <=1'd0;
             o_read_add <= 0;
             o_read_num <= 0;
             o_send_add <= 0;
             o_send_data <= 0;
             o_can_init_finish <= 0;
             o_mcp_rst <= 1; 
             o_reset_en <= 0;
        end
        RESET: begin
            o_reset_en <= 1;
        end  
        RESET_DELAY: begin
            o_reset_en <= 0; 
            o_mcp_rst <= 0; 
        end 
        SET_MODE: begin
            o_mcp_rst <= 1'd1;
            o_send_en <=1'd1;
            o_read_en <=1'd0;
            o_read_add <= 0;
            o_read_num <= 0;
            o_send_add <=  `CANCTRL_ADD;
            o_send_data <= `CANCTRL;             
        end 
        RESET_DONE: begin
            o_mcp_rst <= 1'd1;
            o_send_en <=1'd0;
            o_read_en <=1'd0;
            o_read_add <= 0;
            o_read_num <= 0;
            o_send_add <=  8'b0;
            o_send_data <= 8'b0;    
        end
        READ_CANSTAT: begin
            o_send_en <=1'd0;
            o_read_en <= 1'd1;
            o_read_add <= `CANSTAT_ADD;
            o_read_num <= 8'd1;
            o_reset_en <= 0;
        end
        WAIT: begin
            o_send_en <=1'd0;
            o_read_en <=1'd0;
            o_read_add <= 0;
            o_read_num <= 0;
            o_send_add <= 0;
            o_send_data <= 0;   
        end      
        SET_TX0CTRL: begin
            o_reset_en <= 0;
            o_send_en <=1'd1;
            o_read_en <=1'd0;
            o_read_add <= 0;
            o_read_num <= 0;
            o_send_add <=  `TX0CTRL_ADD;
            o_send_data <= `TX0CTRL;             
        end 
        SET_TX1CTRL: begin
            o_send_add <=  `TX1CTRL_ADD;
            o_send_data <= `TX1CTRL;        
        end
        SET_TX2CTRL: begin
            o_send_add <= `TX2CTRL_ADD;
            o_send_data <= `TX2CTRL;
        end   
        SET_TXRTSCTRL: begin
            o_send_add <= `TXRTSCTRL_ADD;
            o_send_data <= `TXRTSCTRL;
        end 
        // 验收屏蔽寄存器 n 标准标识符高位
        SET_RXM0SIDH: begin
            o_send_add <= 8'h20;
            o_send_data <= 8'b11111111;
        end
        // 验收屏蔽滤波器 n 标准标识符低位
        SET_RXM0SIDL: begin
            o_send_add <= 8'h21;
            if(iv_set_id[59])
                o_send_data <= 8'b11100011;
            else 
                // o_send_data <= 8'b11100000;
                o_send_data <= 8'hc0;
        end
        // 验收屏蔽寄存器 n 扩展标识符高位
        SET_RXM0EID8: begin
            o_send_add <= 8'h22;
            o_send_data <= 8'b11111111;
        end 
        SET_RXM0EID0: begin
            o_send_add <= 8'h23;
            o_send_data <= 8'b11111111;
        end 
        SET_RXM1SIDH: begin
            o_send_add <= 8'h24;
            o_send_data <= 8'b11111111;
        end  
        SET_RXM1SIDL: begin
            o_send_add <= 8'h25;
            if(iv_set_id[59])
                o_send_data <= 8'b11100011;
            else
                // o_send_data <= 8'b11100000;
                o_send_data <= 8'hc0;
        end 
        SET_RXM1EID8: begin
            o_send_add <= 8'h26;
            o_send_data <= 8'b11111111;
        end  
        SET_RXM1EID0: begin
            o_send_add <= 8'h27;
            o_send_data <= 8'b11111111;
        end 
        // 验收滤波寄存器 n 标准标识符高位
        SET_RXF0SIDH: begin
            o_send_add <= 8'h00;
            o_send_data <= iv_set_id[29:22];
        end 
        SET_RXF0SIDL: begin
            o_send_add <= 8'h01;
            o_send_data <= {iv_set_id[21:19],1'b0,iv_set_id[59],1'b0,iv_set_id[18:17]};
        end 
        SET_RXF0EID8: begin
            o_send_add <= 8'h02;
            o_send_data <= iv_set_id[16:9];
        end 
        SET_RXF0EID0: begin
            o_send_add <= 8'h03;
            o_send_data <= iv_set_id[8:1];
        end 
        SET_RXF1SIDH: begin
            o_send_add <= 8'h08;
            o_send_data <= iv_set_id[29:22];
        end   
        SET_RXF1SIDL: begin
            o_send_add <= 8'h09;
            o_send_data <= {iv_set_id[21:19],1'b0,iv_set_id[59],1'b0,iv_set_id[18:17]};
        end 
        SET_RXF1EID8: begin
            o_send_add <= 8'h0a;
            o_send_data <= iv_set_id[16:9];
        end   
        SET_RXF1EID0: begin
            o_send_add <= 8'h0b;
            o_send_data <= iv_set_id[8:1];
        end 
        SET_RX0CTRL: begin
            if(iv_set_id[0])begin
                o_send_add <= `RXB0CTRL_ADD;
                // o_send_data <= 8'h04;
                o_send_data <= 8'h26;
            end
            else begin
                o_send_add <= `RXB0CTRL_ADD;
                o_send_data <= 8'h64;
            end
        end       
        SET_RX1CTRL: begin
            if(iv_set_id[0])begin
                o_send_add <= `RXB1CTRL_ADD;
                // o_send_data <= 8'h00;
                o_send_data <= 8'h20;
            end
            else begin
                o_send_add <= `RXB1CTRL_ADD;
                o_send_data <= 8'h60;
            end
        end 
        SET_BFPCTRL: begin
            o_send_add <= `BFPCTRL_ADD;
            o_send_data <= `BFPCTRL;
        end
        SET_CNF1: begin
            if(iv_set_id[60])begin
                o_send_add <= `CNF1_ADD;
                o_send_data <= `CNF1_1M;
            end
            else if(iv_set_id[61])begin
                o_send_add <= `CNF1_ADD;
                o_send_data <= `CNF1_500k;
            end
            else if(iv_set_id[62])begin
                o_send_add <= `CNF1_ADD;
                o_send_data <= `CNF1_250k;
            end
            else if(iv_set_id[63])begin 
                o_send_add  <= `CNF1_ADD;
                o_send_data <= `CNF1_125k;
            end
            else begin
                o_send_add  <= o_send_add; 
                //o_send_add <= `CNF1_ADD;
                o_send_data <= o_send_data;
                //o_send_data <= `CNF1_125k;
                //o_send_data <= 8'h00;
            end
        end 
        SET_CNF2: begin
            if(iv_set_id[60])begin
                o_send_add <= `CNF2_ADD;
                o_send_data <= `CNF2_1M;
            end
            else if(iv_set_id[61])begin
                o_send_add <= `CNF2_ADD;
                o_send_data <= `CNF2_500k;
            end
            else if(iv_set_id[62])begin
                o_send_add <= `CNF2_ADD;
                o_send_data <= `CNF2_250k;
            end
            else if(iv_set_id[63])begin 
                o_send_add  <= `CNF2_ADD;
                o_send_data <= `CNF2_125k;
            end
            else begin
                o_send_add  <= o_send_add;
                o_send_data <= o_send_data;
                //o_send_add <= `CNF2_ADD;
                //o_send_data <= `CNF2_125k;
                //o_send_data <= 8'h82;
            end
        end 
        SET_CNF3: begin
            if(iv_set_id[60])begin
                o_send_add <= `CNF3_ADD;
                o_send_data <= `CNF3_1M;
            end
            else if(iv_set_id[61])begin
                o_send_add <= `CNF3_ADD;
                o_send_data <= `CNF3_500k;
            end
            else if(iv_set_id[62])begin
                o_send_add <= `CNF3_ADD;
                o_send_data <= `CNF3_250k;
            end
            else if(iv_set_id[63])begin 
                o_send_add  <= `CNF3_ADD;
                o_send_data <= `CNF3_125k;
            end
            else begin
                o_send_add  <= o_send_add;
                o_send_data <= o_send_data;
                //o_send_add <= `CNF3_ADD;
                //o_send_data <= `CNF3_125k;
                //o_send_data <= 8'h02;
            end
        end 
        SET_CANINTE: begin
            o_send_add <= `CANINTE_ADD;
            o_send_data <= `CANINTE;
        end  
        SET_CANINTF: begin
            o_send_add <= `CANINTF_ADD;
            o_send_data <= `CANINTF_C;
        end  
        USUAl_MODE: begin
            o_send_add <= `CANCTRL_ADD;
            o_send_data <= `CANCTRL_UAL;
        end
        USUAl_MODE_delay: begin
            o_send_en <=1'd0;
            o_send_add <= 0;
            o_send_data <= 0;
        end         
        READ_REG: begin
            o_send_en <=1'd0;
            o_read_en <= 1'd1;
            o_read_add <= `CANSTAT_ADD;
            o_read_num <= 8'd1;
        end
        WAIT2: begin
            o_send_en <=1'd0;
            o_read_en <=1'd0;
            o_read_add <= 0;
            o_read_num <= 0;
            o_send_add <= 0;
            o_send_data <= 0;   
        end 
        STOP: 
            o_can_init_finish <= 1; 
        endcase
    end
      
endmodule

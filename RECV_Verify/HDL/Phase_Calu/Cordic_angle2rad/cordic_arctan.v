// 通过Cordic的10级流水迭代计算相位θ=arctan(y/x)值
// 该模块计算出的结果为角度值，非弧度值
// 计算出的相位在一四象限是对的，对于二三象限要进行180°的相位补偿.
// 为了将结果统一为无符号数,对第四象限的角度也进行修正,修正角度为360°,即-30°~330°.
// 注:该程序只是一个初始版本,计算出相位并映射到量化后的弧度值0~8192.
// 程序比较繁琐,后续会考虑将按照循环来书写从而减少代码量.
module cordic_arctan(
    input   wire    i_clk_108M,
    input   wire    i_clk_27M_en,
    input   wire    i_rst_n,
    input   wire    signed  [12:0]  iv_im_data,
    input   wire    signed  [12:0]  iv_re_data,

    output  wire            [13:0]  ov_phase   
);

/*
        角度值θ满足：tan(θi)=2^(-i),则θi=arctan(2^(-i)),θi∈[0,2pi]~[0,8192].
        下面为i∈[0,15]时,满足上式条件的θi值,并进行了16位的定点量化.
*/
    // 角度值进行16位的量化
    parameter   angle_0  = 22'd2949120,         // 45度 * 2^16
                angle_1  = 22'd1740967,         // 26.565051177077990度 * 2^16
                angle_2  = 22'd919879,          // 14.0362434679265度   * 2^16
                angle_3  = 22'd466945,          // 7.1250163489018度    * 2^16
                angle_4  = 22'd234379,          // 3.57633437499735度   * 2^16
                angle_5  = 22'd117303,          // 1.78991060824607度   * 2^16
                angle_6  = 22'd58666,           // 0.895173710211074度  * 2^16
                angle_7  = 22'd29335,           // 0.447614170860553度  * 2^16
                angle_8  = 22'd14668,           // 0.22381050036853度   * 2^16
                angle_9  = 22'd7334,            // 0.111905677066207度  * 2^16
                angle_10 = 22'd3667,            // 0.0559528918938037度 * 2^16
                angle_11 = 22'd1833,            // 0.0279764526170037度 * 2^16
                angle_12 = 22'd917,             // 0.013988227142265度  * 2^16
                angle_13 = 22'd458,             // 0.00699411367535292度 * 2^16
                angle_14 = 22'd229,             // 0.00349705685070401度 * 2^16
                angle_15 = 22'd115;             // 0.00174852842698045度 * 2^16
        
    parameter   NUM_Pipeline = 5'd16;

    reg signed  [28:0]  im_yr,  re_xr,  angle_zr;
    reg signed  [28:0]  im_y0,  re_x0,  angle_z0;
    reg signed  [28:0]  im_y1,  re_x1,  angle_z1;
    reg signed  [28:0]  im_y2,  re_x2,  angle_z2;
    reg signed  [28:0]  im_y3,  re_x3,  angle_z3;
    reg signed  [28:0]  im_y4,  re_x4,  angle_z4;
    reg signed  [28:0]  im_y5,  re_x5,  angle_z5;
    reg signed  [28:0]  im_y6,  re_x6,  angle_z6;
    reg signed  [28:0]  im_y7,  re_x7,  angle_z7;
    reg signed  [28:0]  im_y8,  re_x8,  angle_z8;
    reg signed  [28:0]  im_y9,  re_x9,  angle_z9;
    reg signed  [28:0]  im_y10, re_x10, angle_z10;
    reg signed  [28:0]  im_y11, re_x11, angle_z11;
    reg signed  [28:0]  im_y12, re_x12, angle_z12;
    reg signed  [28:0]  im_y13, re_x13, angle_z13;
    reg signed  [28:0]  im_y14, re_x14, angle_z14;
    reg signed  [28:0]  im_y15, re_x15, angle_z15;

    // 先确定坐标象限
    // 如果是二三象限，则需要按照原点对称，计算后再进行相位的补偿
    always@(posedge i_clk_108M or negedge i_rst_n)
    begin 
        if(i_rst_n == 1'b0)begin 
            re_xr <= 29'd0;
            im_yr <= 29'd0;
            angle_zr <= 29'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin
            if((iv_im_data == 13'd0)&&(iv_re_data == 13'd0))begin 
                re_xr <= 29'd0;
                im_yr <= 29'd0;
                angle_zr <= 29'd0;
            end
            else if(iv_re_data[12] == 1'b1)begin                // 二三象限,x<0
                re_xr <= ((~iv_re_data + 1'b1) <<< 16);         // 取相反数，并左移16位
                im_yr <= ((~iv_im_data + 1'b1) <<< 16);         // 该计算方式对正负数均成立
                angle_zr <= 29'd0;
            end
            else begin          // 一四象限,x>0,保持不变,直接进行移位操作
                re_xr <= (iv_re_data <<< 16);
                im_yr <= (iv_im_data <<< 16);
                angle_zr <= 29'd0;
            end
        end
    end

//--------------------------- 象限判断 ----------------------------//
    
    // 按照当前的计算逻辑，二三象限的坐标会按照坐标原点进行对称.
    // 所以当x<0即第二三象限时,要对相位进行180°的补偿
    // 当x=0时分两种情况:
    //      (1)y>0,则相位为90°;y<0,则相位为270°
    //      (2)y=0,则相位为0°.
    //  当16次迭代完成时,re_x15的值应当为原始向量模长的1.67倍;如果re_x15=0,说明原始坐标在原点,相位为0.
    reg [0:0]   x_negative  [NUM_Pipeline:0];
    reg [0:0]   y_negative  [NUM_Pipeline:0];

    integer i;
    // 对输入的实部数据符号位进行缓存,用于迭代结束后相位补偿的判断依据.
    always@(posedge i_clk_108M or negedge i_rst_n)
    begin : qudrant_x_leq_0
        if(i_rst_n == 1'b0)begin 
            for(i=0;i<NUM_Pipeline+1;i=i+1)begin:quadrant_init
                x_negative[i] <= 1'b0;
                y_negative[i] <= 1'b0;
            end
        end
        else if(i_clk_27M_en == 1'b1)begin 
            x_negative[0] <= iv_re_data[12];
            y_negative[0] <= iv_im_data[12];
            for(i=1;i<NUM_Pipeline+1;i=i+1)begin:quadrant_record
                x_negative[i] <= x_negative[i-1];
                y_negative[i] <= y_negative[i-1];
            end
        end     // else keep
    end

//------------------------ 16级流水迭代计算 ---------------------------//
    
    // 第1次迭代
    always@(posedge i_clk_108M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)begin 
            re_x0 <= 29'd0;
            im_y0 <= 29'd0;
            angle_z0 <= 29'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin
            if(im_yr[28] == 1'b1)begin         // y<0
                re_x0 <= re_xr - im_yr;
                im_y0 <= im_yr + re_xr;
                angle_z0 <= angle_zr - angle_0;
            end
            else begin              // y>0 
                re_x0 <= re_xr + im_yr;
                im_y0 <= im_yr - re_xr;
                angle_z0 <= angle_zr + angle_0;
            end
        end
    end

    // 第2次迭代
    always@(posedge i_clk_108M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)begin
            re_x1 <= 29'd0;
            im_y1 <= 29'd0;
            angle_z1 <= 29'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin
            if(im_y0[28] == 1'b1)begin
                re_x1 <= re_x0 - (im_y0 >>> 1);
                im_y1 <= im_y0 + (re_x0 >>> 1);
                angle_z1 <= angle_z0 - angle_1;
            end
            else begin              // y>0
                re_x1 <= re_x0 + (im_y0 >>> 1);
                im_y1 <= im_y0 - (re_x0 >>> 1);
                angle_z1 <= angle_z0 + angle_1;
            end
        end
    end

    // 第3次迭代
    always@(posedge i_clk_108M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)begin
            re_x2 <= 29'd0;
            im_y2 <= 29'd0;
            angle_z2 <= 29'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin
            if(im_y1[28] == 1'b1)begin
                re_x2 <= re_x1 - (im_y1 >>> 2);
                im_y2 <= im_y1 + (re_x1 >>> 2);
                angle_z2 <= angle_z1 - angle_2;
            end
            else begin              // y>0
                re_x2 <= re_x1 + (im_y1 >>> 2);
                im_y2 <= im_y1 - (re_x1 >>> 2);
                angle_z2 <= angle_z1 + angle_2;
            end
        end
    end

    // 第4次迭代
    always@(posedge i_clk_108M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)begin
            re_x3    <= 29'd0;
            im_y3    <= 29'd0;
            angle_z3 <= 29'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin
            if(im_y2[28] == 1'b1)begin
                re_x3    <= re_x2 - (im_y2 >>> 3);
                im_y3    <= im_y2 + (re_x2 >>> 3);
                angle_z3 <= angle_z2 - angle_3;
            end
            else begin              // y>0
                re_x3    <= re_x2 + (im_y2 >>> 3);
                im_y3    <= im_y2 - (re_x2 >>> 3);
                angle_z3 <= angle_z2 + angle_3;
            end
        end
    end

    // 第5次迭代
    always@(posedge i_clk_108M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)begin
            re_x4    <= 29'd0;
            im_y4    <= 29'd0;
            angle_z4 <= 29'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin
            if(im_y3[28] == 1'b1)begin
                re_x4    <= re_x3 - (im_y3 >>> 4);
                im_y4    <= im_y3 + (re_x3 >>> 4);
                angle_z4 <= angle_z3 - angle_4;
            end
            else begin              // y>0
                re_x4    <= re_x3 + (im_y3 >>> 4);
                im_y4    <= im_y3 - (re_x3 >>> 4);
                angle_z4 <= angle_z3 + angle_4;
            end
        end
    end

    // 第6次迭代
    always@(posedge i_clk_108M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)begin
            re_x5    <= 29'd0;
            im_y5    <= 29'd0;
            angle_z5 <= 29'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin
            if(im_y4[28] == 1'b1)begin
                re_x5    <= re_x4 - (im_y4 >>> 5);
                im_y5    <= im_y4 + (re_x4 >>> 5);
                angle_z5 <= angle_z4 - angle_5;
            end
            else begin              // y>0
                re_x5    <= re_x4 + (im_y4 >>> 5);
                im_y5    <= im_y4 - (re_x4 >>> 5);
                angle_z5 <= angle_z4 + angle_5;
            end
        end
    end

    // 第7次迭代
    always@(posedge i_clk_108M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)begin
            re_x6    <= 29'd0;
            im_y6    <= 29'd0;
            angle_z6 <= 29'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin
            if(im_y5[28] == 1'b1)begin
                re_x6    <= re_x5 - (im_y5 >>> 6);
                im_y6    <= im_y5 + (re_x5 >>> 6);
                angle_z6 <= angle_z5 - angle_6;
            end
            else begin              // y>0
                re_x6    <= re_x5 + (im_y5 >>> 6);
                im_y6    <= im_y5 - (re_x5 >>> 6);
                angle_z6 <= angle_z5 + angle_6;
            end
        end
    end

    // 第8次迭代
    always@(posedge i_clk_108M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)begin
            re_x7    <= 29'd0;
            im_y7    <= 29'd0;
            angle_z7 <= 29'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin
            if(im_y6[28] == 1'b1)begin
                re_x7    <= re_x6 - (im_y6 >>> 7);
                im_y7    <= im_y6 + (re_x6 >>> 7);
                angle_z7 <= angle_z6 - angle_7;
            end
            else begin              // y>0
                re_x7    <= re_x6 + (im_y6 >>> 7);
                im_y7    <= im_y6 - (re_x6 >>> 7);
                angle_z7 <= angle_z6 + angle_7;
            end
        end
    end

    // 第9次迭代
    always@(posedge i_clk_108M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)begin
            re_x8    <= 29'd0;
            im_y8    <= 29'd0;
            angle_z8 <= 29'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin
            if(im_y7[28] == 1'b1)begin
                re_x8    <= re_x7 - (im_y7 >>> 8);
                im_y8    <= im_y7 + (re_x7 >>> 8);
                angle_z8 <= angle_z7 - angle_8;
            end
            else begin              // y>0
                re_x8    <= re_x7 + (im_y7 >>> 8);
                im_y8    <= im_y7 - (re_x7 >>> 8);
                angle_z8 <= angle_z7 + angle_8;
            end
        end
    end

    // 第10次迭代
    always@(posedge i_clk_108M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)begin
            re_x9    <= 29'd0;
            im_y9    <= 29'd0;
            angle_z9 <= 29'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin
            if(im_y8[28] == 1'b1)begin
                re_x9    <= re_x8 - (im_y8 >>> 9);
                im_y9    <= im_y8 + (re_x8 >>> 9);
                angle_z9 <= angle_z8 - angle_9;
            end
            else begin              // y>0
                re_x9    <= re_x8 + (im_y8 >>> 9);
                im_y9    <= im_y8 - (re_x8 >>> 9);
                angle_z9 <= angle_z8 + angle_9;
            end
        end
    end

    // 第11次迭代
    always@(posedge i_clk_108M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)begin
            re_x10    <= 29'd0;
            im_y10    <= 29'd0;
            angle_z10 <= 29'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin
            if(im_y9[28] == 1'b1)begin
                re_x10    <= re_x9 - (im_y9 >>> 10);
                im_y10    <= im_y9 + (re_x9 >>> 10);
                angle_z10 <= angle_z9 - angle_10;
            end
            else begin              // y>0
                re_x10    <= re_x9 + (im_y9 >>> 10);
                im_y10    <= im_y9 - (re_x9 >>> 10);
                angle_z10 <= angle_z9 + angle_10;
            end
        end
    end

    // 第12次迭代
    always@(posedge i_clk_108M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)begin
            re_x11    <= 29'd0;
            im_y11    <= 29'd0;
            angle_z11 <= 29'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin
            if(im_y10[28] == 1'b1)begin
                re_x11    <= re_x10 - (im_y10 >>> 11);
                im_y11    <= im_y10 + (re_x10 >>> 11);
                angle_z11 <= angle_z10 - angle_11;
            end
            else begin              // y>0
                re_x11    <= re_x10 + (im_y10 >>> 11);
                im_y11    <= im_y10 - (re_x10 >>> 11);
                angle_z11 <= angle_z10 + angle_11;
            end
        end
    end

    // 第13次迭代
    always@(posedge i_clk_108M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)begin
            re_x12    <= 29'd0;
            im_y12    <= 29'd0;
            angle_z12 <= 29'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin
            if(im_y11[28] == 1'b1)begin
                re_x12    <= re_x11 - (im_y11 >>> 12);
                im_y12    <= im_y11 + (re_x11 >>> 12);
                angle_z12 <= angle_z11 - angle_12;
            end
            else begin              // y>0
                re_x12    <= re_x11 + (im_y11 >>> 12);
                im_y12    <= im_y11 - (re_x11 >>> 12);
                angle_z12 <= angle_z11 + angle_12;
            end
        end
    end

    // 第14次迭代
    always@(posedge i_clk_108M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)begin
            re_x13    <= 29'd0;
            im_y13    <= 29'd0;
            angle_z13 <= 29'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin
            if(im_y12[28] == 1'b1)begin
                re_x13    <= re_x12 - (im_y12 >>> 13);
                im_y13    <= im_y12 + (re_x12 >>> 13);
                angle_z13 <= angle_z12 - angle_13;
            end
            else begin              // y>0
                re_x13    <= re_x12 + (im_y12 >>> 13);
                im_y13    <= im_y12 - (re_x12 >>> 13);
                angle_z13 <= angle_z12 + angle_13;
            end
        end
    end

    // 第15次迭代
    always@(posedge i_clk_108M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)begin
            re_x14    <= 29'd0;
            im_y14    <= 29'd0;
            angle_z14 <= 29'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin
            if(im_y13[28] == 1'b1)begin
                re_x14    <= re_x13 - (im_y13 >>> 14);
                im_y14    <= im_y13 + (re_x13 >>> 14);
                angle_z14 <= angle_z13 - angle_14;
            end
            else begin              // y>0
                re_x14    <= re_x13 + (im_y13 >>> 14);
                im_y14    <= im_y13 - (re_x13 >>> 14);
                angle_z14 <= angle_z13 + angle_14;
            end
        end
    end

//----------------------- 第16次迭代和输出相位值 -----------------------//
    
    // 第16次迭代时,输出计算结果
    always@(posedge i_clk_108M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)begin
            re_x15    <= 29'd0;
            im_y15    <= 29'd0;
            angle_z15 <= 29'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin
            if(im_y14[28] == 1'b1)begin
                re_x15    <= re_x14 - (im_y14 >>> 15);
                im_y15    <= im_y14 + (re_x14 >>> 15);
                angle_z15 <= (angle_z14 - angle_15) >>> 16;
            end
            else begin              // y>0
                re_x15    <= re_x14 + (im_y14 >>> 15);
                im_y15    <= im_y14 - (re_x14 >>> 15);
                angle_z15 <= (angle_z14 + angle_15) >>> 16;         
            end
        end
    end

//-------------------------- 输出相位值 -----------------------------//

    /*  (x_sign,y_sign)           象限和相位补偿
            2'b00       :   第一象限，不需要修正
            2'b10       :   第二象限，需要180°的修正
            2'b11       :   第三象限，需要180°的修正
            2'b01       :   第四象限，需要360°的修正        */

    reg     signed  [12:0]  angle_tan;

    // 对计算的相位进行补偿
    always@(posedge i_clk_108M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            angle_tan <= 13'd0;
        else if(i_clk_27M_en == 1'b1)begin 
            if(re_x15 == 28'd0)                             // 实部为0而虚部不为0,则相位为90°;如果迭代完成时实部仍为0,则该向量模长为0,无意义,相位为0.
                angle_tan <= 13'd0;
            else if(&angle_z15[12:0] == 1'b1)               // 算术右移后可能会产生全1的情况,对应0°,此时误差为1°,最终结果可能为-1°,需要进行修正
                angle_tan <= 13'd0;
            else begin 
                case({x_negative[NUM_Pipeline],y_negative[NUM_Pipeline]})
                    2'b00 : angle_tan <= angle_z15;              // 第一象限，无需进行相位补偿
                    2'b10 : angle_tan <= angle_z15 + 13'd180;    // 第二象限，进行180°的相位补偿
                    2'b11 : angle_tan <= angle_z15 + 13'd180;    // 第三象限，进行180°的相位补偿
                    2'b01 : angle_tan <= angle_z15 + 13'd360;    // 第四象限，进行360°的相位补偿
                    default : angle_tan <= angle_tan;         // 其他情况下，保持当前相位不变
                endcase
            end
        end
        else
            angle_tan <= angle_tan;
    end

    // 此时的角度值取低9位作为ROM的查表地址,将角度值映射到0~2pi,即0~8192. 

    rom_angle2rad U_rom_angle2rad(
	    .address(angle_tan[8:0]),
	    .clock  (i_clk_108M),
        .clken  (i_clk_27M_en),
	    .q      (ov_phase)
    );

//------------------------ Cordic循环迭代 ---------------------------//

/*
    // 使用16级流水的顺序结构
    reg signed  [28:0]  x_pipeline[16:0];
    reg signed  [28:0]  y_pipeline[16:0];
    reg signed  [28:0]  z_pipeline[16:0];

    integer i;

    always@(posedge i_clk_108M or negedge i_rst_n)
    begin 
        if(i_rst_n == 1'b0)begin 
            for(i=0;i<=15;i=i+1)begin 
                x_pipeline[i] <= 29'd0;
                y_pipeline[i] <= 29'd0;
                z_pipeline[i] <= 29'd0;
            end
        end
        else begin
            // 判断象限:如果在二三象限需要按照原点将坐标对称，之后再进行相位的补偿
            if(iv_re_data[12] == 1'b1)begin     // 二三象限,x<0
                x_pipeline[0] <= (~iv_re_data + 1'b1) <<< 16;
                y_pipeline[0] <= (~iv_im_data + 1'b1) <<< 16;
                z_pipeline[0] <= 29'd0;
            end
            else begin                          // 一四象限,x>0
                x_pipeline[0] <= (iv_re_data <<< 16);
                y_pipeline[0] <= (iv_im_data <<< 16);
                z_pipeline[0] <= 29'd0;
            end
            // Cordic迭代计算arctan(y/x)
            for(i=1;i<=16;i=i+1)begin 
                if(y_pipeline[i][28] == 1'b1)begin 
                    x_pipeline[i] <= x_pipeline[i-1] - (y_pipeline[i-1] >>> (i-1));
                    y_pipeline[i] <= y_pipeline[i-1] + (x_pipeline[i-1] >>> (i-1));
                    z_pipeline[i] <= z_pipeline[i-1] - angle[i-1];
                end
                else begin
                    x_pipeline[i] <= x_pipeline[i-1] + (y_pipeline[i-1] >>> (i-1));
                    y_pipeline[i] <= y_pipeline[i-1] - (x_pipeline[i-1] >>> (i-1));
                    z_pipeline[i] <= z_pipeline[i-1] + angle[i-1];
                end
            end
        end
    end
*/

endmodule
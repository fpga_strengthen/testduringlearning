



%%  解调算法设计中的中间版本，已舍弃       



%FSK解调判决仿真
%目的检测频偏对判决错误的影响
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%FSK调制%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc
format long
close all
clear 

%% %标准条件下主要参数
f0_true = 3.951;
f1_true = 4.516 ;         %比特0和比特1的频点，单位MHz
f_mid =4.234; % (3.951+4.516) / 2
sample_rate_true = 27.095;% 标准采样速率，单位MHz
BPS_data_true = 0.56448;  % 标准数据传输速率，单位MHz
Hd = LP_6M;

%%%%%%%%% 产生相位连续的FSK调制信号进行ADC采样后作为输入 %%%%%%%%%%%%%
src_data =randint(1,100);%以比特向量方式输入的待调制数据，随机产生

%%%%%%%%% FSK数据的关键参数 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%通过控制FSK的参数产生带有偏差数据源
%A点
% f0 = f_mid - 0.282 ;%
% f1 = f_mid + 0.282;%比特0和比特1的频点，单位MHz
%B点
f0 = f_mid - 0.477 ;%
f1 = f_mid + 0.127;%比特0和比特1的频点，单位MHz

% %C点
% f0 = f_mid - 0.43749 ;%
% f1 = f_mid + 0.0879;%比特0和比特1的频点，单位MHz

% %D点
% f0 = f_mid - 0.0879 ;%
% f1 = f_mid + 0.43749;%比特0和比特1的频点，单位MHz

sample_rate =  sample_rate_true*1.0001;%采样速率，单位MHz
BPS_data = BPS_data_true*1.025;%数据传输速率，单位MHz
FSK_data  = FSK_mod(src_data,f0,f1,sample_rate,BPS_data);

%对输入数据进行定标
FSK_data = FSK_data  - 2048;%FixPointNum(FSK_data  - 2048, 12, 0);%字长12bit，精度0位小数 + FSK_data_noise
length_fsk_data = length(FSK_data);

%% 相位折线求拐点
%理论上的Hilbert变换
hilbert_fsk = hilbert(FSK_data);  %求出理论上的解析信号

%对中心频率和2pi进行修正
%理论实现
arctan_fsk = hilbert_fsk .* exp(-j*[1:length(hilbert_fsk)]*2*pi*4.234/sample_rate);%处理中心频率；
arctan_fsk = angle(arctan_fsk); %求出解析信号所对应的相位
arctan_fsk = unwrap(arctan_fsk,pi);  %对相位进行2pi的修正

%最小二乘法
%用理论计算的相位进行理论上的最小二乘法
[ intersection_slop_decisioned,slope_forward_total_theroy] = Least_Square_Method_theroy_1( arctan_fsk );

%% hilbert累加求同步点
%构造本地载波
static_lenth_x_axis = length_fsk_data;%本地载波长度
S0 = cos( f0_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);
S1 = cos( f1_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);
S0_Q = sin(f0_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);%%移相90°
S1_Q = sin(f1_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);%%移相90°

%相关解调
value_cos0 = FSK_data .* S0;
value_sin0 = FSK_data .* S0_Q;
value_cos1 = FSK_data .* S1;
value_sin1 = FSK_data .* S1_Q;

%滤除高频信号
value_cos0 = filter(Hd,FSK_data .* S0);
value_sin0 = filter(Hd,FSK_data .* S0_Q);
value_cos1 = filter(Hd,FSK_data .* S1);
value_sin1 = filter(Hd,FSK_data .* S1_Q);

%% 消除Ws的影响 hilbert法
%将与本地载波（cos路）相乘后的结果进行希尔伯特变换（只考虑2路）

 %用实验室方法求希尔伯特变换
[ im_datain_solve ] = hilbert_trans( value_cos0 );
% im_datain 有N/2的时延，N=11；前N个和最后N个数据无效
% 数据对齐
Length_data = length(im_datain_solve);
im_datain = im_datain_solve(6:Length_data);
value_cos0 = value_cos0(1:Length_data-5);

[ re_datain_solve ] = hilbert_trans( value_sin0 );
re_datain = re_datain_solve(6:Length_data);
value_sin0 = value_sin0(1:Length_data-5);

%将Hilbert变换结果进一步处理
value_cos0  = value_cos0-re_datain*2;
value_sin0  = im_datain*2+value_sin0;


%将希尔伯特处理后的结果24点相加，消除高频分量
for i =1:length(value_cos0)-48 %相干解调过程,24个点求和
    sum_value0_after_hilbert(i) =(sum(value_cos0(i:i+23)) )^2 + (sum(value_sin0(i:i+23)) )^2 - (sum(value_cos0(i+24:i+47)) )^2 - (sum(value_sin0(i+24:i+47)) )^2;
end

for i =3:length(sum_value0_after_hilbert)-3 
    if(abs(sum_value0_after_hilbert(i))>1*10^8)
        extremum(i) = ( abs(sum_value0_after_hilbert(i-2))<abs(sum_value0_after_hilbert(i))  & abs(sum_value0_after_hilbert(i-1))<abs(sum_value0_after_hilbert(i))  &  abs(sum_value0_after_hilbert(i+1))<abs(sum_value0_after_hilbert(i))  & abs(sum_value0_after_hilbert(i+2))<abs(sum_value0_after_hilbert(i)))  ;
    else
        extremum(i) = 0;
    end
end
figure(2),
plot(extremum*1*10e8,'r');
hold on,
plot(abs(sum_value0_after_hilbert(1:length(sum_value0_after_hilbert)-1) ));
hold on,
plot(intersection_slop_decisioned*6*10e7,'g');


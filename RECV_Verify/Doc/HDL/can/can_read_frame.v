//+FHEADER //////////////////////////////////////////////////////////////////////
//  版权所有者： 
//  Copyright 2014. All Rights Reserved.
//  保密级别：  
//---------------------------------------------------------------------------
//  文件名 :  can_read_frame.v
//  原始作者：丁云冰
//  联系方式：
// --------------------------------------------------------------------------
// 版本升级历史：
// 版本生效日期： 
// 1.0 2014-02-22  丁云冰  第1版  
// 2020.6.8 王雪
//---------------------------------------------------------------------------
// 关键字 :   can bus,mcp2515,read frame
//---------------------------------------------------------------------------
// 模块功能:  读取一帧数据
//             
// --------------------------------------------------------------------------
// 参数文件名：  
// --------------------------------------------------------------------------
// 重复使用问题：
// 复位策略:负跳变沿异步复位信号 i_rst_n
// 时钟域 : i_clk_27M 
// 关键时序: N/A
// 是否需要实例引用别的模块 : No
// 可综合否 : Yes 
//-FHEADER //////////////////////////////////////////////////////////////////////
module can_read_frame
       (
         //time & rst
         i_clk_27M,
         i_rst_n,
         
         //input control
         i_mcp_int,
         i_read_frame_en, 
         i_read_ok,
         i_read_byte,
         i_mody_ok,
         
         //data input
         i_reg_out,         //读取到的寄存器数据
         
         //output
         o_read_frame_ok,   //读取一帧完成标志
         o_read_en,         
         o_mody_en,
         
         //data out
         o_read_add,
         o_read_num,
         o_mody_add,
         o_mody_data,
         o_mody_screen,
         //读取数据帧结果输出
         ov_recv_data,
         o_databyte_en,
         o_receive_ok_can,
         o_databyte_clk
         );
    input i_clk_27M;
    input i_rst_n;
    input i_mcp_int;
    input i_read_frame_en;
    input i_read_ok;
    input i_read_byte;
    input i_mody_ok;
    input[7:0] i_reg_out;
    
    output reg[7:0]ov_recv_data;
    output o_read_frame_ok;
    output o_read_en;
    output o_mody_en;
    output[7:0] o_read_add;
    output[7:0] o_read_num;
    output[7:0] o_mody_add;
    output[7:0] o_mody_data;
    output[7:0] o_mody_screen;
    output o_databyte_clk;
    output reg o_receive_ok_can;
    output reg o_databyte_en;
  
///////////////////////////////////////////////
  `include"parameter.v"
///////////////////////////////////////////////
    parameter IDLE           = 10'b0000000001, 
              READ_STATE     = 10'b0000000010,  
              DELAY          = 10'b0000000100,  
              WAIT           = 10'b0000001000,   
              READ_RXB0SIDH  = 10'b0000010000, 
              READ_RXB1      = 10'b0000100000,
              SET_CANIF      = 10'b0001000000,
              WAIT_CANIF     = 10'b0010000000,
              STOP           = 10'b0100000000,
              CLEAR          = 10'b1000000000,
              //
              TEST           = 10'b1000000001;              
//////////////////////////////////////////////
    reg[9:0] cstate;
    reg[9:0] nstate;
    
    reg o_read_en;
    reg[7:0] o_read_add;
    reg[7:0] o_read_num;
    reg o_read_frame_ok;
    reg o_mody_en;
    reg[7:0] o_mody_add;
    reg[7:0] o_mody_data;
    reg[7:0] o_mody_screen; 
    reg[3:0] cnt_databyte;
    reg o_databyte_clk; 
    
    reg i_read_frame_en_temp1;
    reg read_frame_en_p;
////////////////////////////////////////////////  
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if(!i_rst_n)begin
            i_read_frame_en_temp1 <= 1'b0;
            read_frame_en_p <= 1'b0;
        end
        else begin
            i_read_frame_en_temp1 <= i_read_frame_en;
            read_frame_en_p <= i_read_frame_en && (~i_read_frame_en_temp1);
        end
    end
           
//////////////////////////////////////////////
    //不同接收缓存区标志,清中断时需要
    reg flag;//5.11
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if(!i_rst_n) 
            flag <= 1'b0;
        else if(cstate == WAIT && i_reg_out == 8'b00001100)
            flag <= 1'b0;
        else if(cstate == WAIT && i_reg_out == 8'b00001110)
            flag <= 1'b1;
        else flag <= flag;
    end
    
    reg [15:0]cnt_delay_1ms; 
    wire delay_1ms;
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if( !i_rst_n )
            cnt_delay_1ms <= 16'd0;
        else if(cnt_delay_1ms >= 16'd25000)//  10ms
            cnt_delay_1ms <= 16'd0;
        else if(cstate == WAIT_CANIF)
            cnt_delay_1ms <= 16'd1;
        else if(cnt_delay_1ms)
            cnt_delay_1ms <= cnt_delay_1ms +16'd1;   
        else
            cnt_delay_1ms <= cnt_delay_1ms;
    end
        
    assign delay_1ms = (cnt_delay_1ms == 16'd25000) ? 1'b1:1'b0;
    
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if(!i_rst_n) 
            cstate <= IDLE;
        else
            cstate <= nstate;
    end
  
    always@(cstate
      or read_frame_en_p
      or i_reg_out
      or i_read_ok
      or i_mcp_int
      or i_mody_ok
      )
    begin
        case(cstate)
        IDLE: 
            if(read_frame_en_p )
                nstate = READ_STATE; 
            else
                nstate = IDLE;
        READ_STATE: 
            if(i_read_ok)
                nstate = DELAY;
            else
                nstate = READ_STATE;
        DELAY:
            nstate = WAIT;
        WAIT:
            if(i_reg_out == 8'b00001100)
                nstate = READ_RXB0SIDH;
            else if (i_reg_out == 8'b00001110)
                nstate = READ_RXB1;
            else
                nstate = WAIT;
        READ_RXB0SIDH:
            if(i_read_ok)
                nstate = SET_CANIF;
            else
                nstate = READ_RXB0SIDH;         
        READ_RXB1:    
            if(i_read_ok)
                nstate = SET_CANIF;
            else
                nstate = READ_RXB1;     
/////////////////////////////////////////////////////
        SET_CANIF: 
            if(i_mody_ok)
                nstate = WAIT_CANIF;
            else 
                nstate = SET_CANIF; 
        WAIT_CANIF:
                nstate = STOP;
        TEST:   
            if(delay_1ms)
                nstate = STOP;
            else
                nstate = TEST;
        STOP:   nstate = CLEAR;
        CLEAR:  nstate = IDLE;
        default: nstate = IDLE;     
        endcase
    end
//////////////////////////////////////////
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
    if(!i_rst_n) begin
        o_read_en <=1'd0;
        o_read_add <= 0;
        o_read_num <= 0;
        o_read_frame_ok <=0;
        o_mody_en <=0;
        o_mody_add <=0;
        o_mody_data <=0;
        o_mody_screen <=0;           
    end
    else
    case(cstate)
    IDLE,CLEAR: begin
        o_read_en <=1'd0;
        o_read_add <= 0;
        o_read_num <= 0;
        o_read_frame_ok <=0;
        o_mody_en <=0;
        o_mody_add <=0;
        o_mody_data <=0;
        o_mody_screen <=0;              
    end        
    READ_STATE: begin
        o_read_en <= 1'd1;
        o_read_add <= `CANSTAT_ADD;
        o_read_num <= 8'd1;           
    end
    WAIT: begin
        o_read_en <=1'd0;
        o_read_add <= 0;
        o_read_num <= 0;
    end       
    READ_RXB0SIDH: begin
        o_read_en <= 1'd1;
        o_read_add <= `RXB0SIDH_ADD;
        o_read_num <= 8'd13;
        o_read_frame_ok <=0;
        o_mody_en <=0;
        o_mody_add <=0;
        o_mody_data <=0;
        o_mody_screen <=0; 
    end
    READ_RXB1: begin
        o_read_en <= 1'd1;
        o_read_add <= `RXB1SIDH_ADD;
        o_read_num <= 8'd13;
    end
    SET_CANIF: begin //5.11
        if(flag) begin
            o_read_en <=1'd0;
            o_read_add <= 0;
            o_read_num <= 0;
            o_mody_en <=1;
            o_mody_add <=`CANINTF_ADD;
            o_mody_data <= 0;
            o_mody_screen <=8'h02; 
        end
        else begin
            o_read_en <=1'd0;
            o_read_add <= 0;
            o_read_num <= 0;
            o_mody_en <=1;
            o_mody_add <=`CANINTF_ADD;
            o_mody_data <= 0;
            o_mody_screen <=8'h01;
        end                  
    end 
    WAIT_CANIF: begin
        o_read_en <=1'd0;
        o_read_add <= 0;
        o_read_num <= 0;          
        o_mody_en <=0;
        o_mody_add <=0;
        o_mody_data <=0;
        o_mody_screen <=0;   
        o_read_frame_ok <=1'b0;
    end  
    STOP: begin
        o_read_en <=1'd0;
        o_read_add <= 0;
        o_read_num <= 0;          
        o_mody_en <=0;
        o_mody_add <=0;
        o_mody_data <=0;
        o_mody_screen <=0;   
        o_read_frame_ok <=1'b1;     
    end  
    endcase
    end     
        
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if(!i_rst_n)
            cnt_databyte <= 4'b0;
        else if(i_mcp_int || (i_mody_ok))//5.12
            cnt_databyte <= 4'b0;
        else if((cnt_databyte > 4'b0) && i_read_byte)
            cnt_databyte <= cnt_databyte + 1'b1;
        else if((cstate == READ_RXB0SIDH || cstate == READ_RXB1) && cnt_databyte == 4'b0)
            cnt_databyte <= 4'b1;
        else 
            cnt_databyte <= cnt_databyte;
    end
    //只输出数据帧中id和数据字节
    wire databyte_en;
    assign databyte_en = ((cnt_databyte > 4'd0 && cnt_databyte < 4'd5) || (cnt_databyte > 4'd5 && cnt_databyte < 4'd14))? 1'b1 : 1'b0;
    reg databyte_en_r;
    reg databyte_en_r1;
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if(!i_rst_n)begin
            databyte_en_r <= 1'd0;
            databyte_en_r1 <= 1'd0;
            o_databyte_en <= 1'd0;
        end
        else begin
            databyte_en_r <= databyte_en;
            databyte_en_r1 <= databyte_en_r;
            o_databyte_en <= databyte_en_r1;
        end
    end
    //打拍对齐输出
    reg databyte_clk_r;
    reg databyte_clk_r1;
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if(!i_rst_n)begin
            databyte_clk_r <= 1'd0;
            databyte_clk_r1 <= 1'd0;
        end
        else begin
            databyte_clk_r <= i_read_byte;
            databyte_clk_r1 <= databyte_clk_r;
        end
    end

    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if(!i_rst_n)begin
            o_databyte_clk <= 1'd0;
        end
        else if(databyte_clk_r1)begin
            o_databyte_clk <= 1'd1;
        end
        // else if(o_read_frame_ok)begin//多给一个无效时钟,接收端使用fifo时用于将数据送入fifo
        //     o_databyte_clk <= 1'd1;
        // end
        else begin
            o_databyte_clk <= 1'd0;
        end
    end
    
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if(!i_rst_n)
            ov_recv_data <= 8'd0;
        else 
            ov_recv_data <= i_reg_out;
    end
    
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if(!i_rst_n)begin
            o_receive_ok_can <= 1'd0;
        end
        else if(o_read_frame_ok)begin
            o_receive_ok_can <= 1'd1;
        end
        else begin
            o_receive_ok_can <= 1'd0;
        end
    end
    
                          
endmodule
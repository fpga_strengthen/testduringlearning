module data_round
#(
    parameter   IN_WIDTH     = 8,
    parameter   OUT_WIDTH    = 6,
    parameter   UP_CUT_WIDTH = 0        //1
)(
    input   wire    [IN_WIDTH-1:0]  DATA_IN,
    output  reg     [OUT_WIDTH-1:0] DATA_OUT
);

    localparam  OUT_WIDTH_1 = OUT_WIDTH - 1;                        //5=6-1
    localparam  TEMP_WIDTH2 = OUT_WIDTH + UP_CUT_WIDTH + 1;         //7=6+0+1   //8=6+1+1
    localparam  TEMP_WIDTH1 = IN_WIDTH - TEMP_WIDTH2 + 1;           //2=8-7+1   //1=8-8+1

    wire    [TEMP_WIDTH2-1:0]   temp_data;

    wire    flag1;
    wire    flga2;

    //第一部分是扩展符号位，并将数据截短，去掉了低两位，即算数右移2位
    //第二部分是判断截取的最低位的右边一位是否为1，如果是，则加上它相当于进行了四舍五入，提高了精度；如果是0，则不影响
    assign  temp_data = {DATA_IN[IN_WIDTH - 1], DATA_IN[IN_WIDTH - 1 : TEMP_WIDTH1]} + DATA_IN[TEMP_WIDTH1 - 1];

    //flag1中两个相或运算的部分用来判断temp_data的所有在符号位的比特是否相同（00或11）；如果不同，说明发生了溢出
    assign  flag1 = ((~|temp_data[TEMP_WIDTH2 - 1 : OUT_WIDTH - 1]) || (&temp_data[TEMP_WIDTH2 - 1 : OUT_WIDTH - 1])) ? 1'b0 : 1'b1;
    
    assign  flga2 = temp_data[TEMP_WIDTH2 - 1];     //取符号位

    //对数据进行备份，当没有发生溢出时就输出该部分
    wire    [OUT_WIDTH - 1 : 0] temp_data_bit_more;

    assign  temp_data_bit_more = temp_data[OUT_WIDTH-1:0];

    always@(flag1 or flga2 or temp_data_bit_more)begin 
        if(flag1)begin              //有溢出
            if(flga2 == 1'b1)       //符号位为1，说明是负数
                DATA_OUT = {1'b1,{OUT_WIDTH_1{1'b0}}};          //按照下溢处理
            else
                DATA_OUT = {1'b0,{OUT_WIDTH_1{1'b1}}};          //正数，按照上溢处理
        end
        else
            DATA_OUT = temp_data_bit_more;                      //未发生溢出，输出原数据
    end

endmodule
`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Module Name: axi_wr_master
// Description: 根据wr_buffer模块的输出信号对AXI的写通道信号进行输出.
// Revision:
// Revision 0.01 - File Created
//////////////////////////////////////////////////////////////////////////////////

module axi_wr_master #(
	parameter   AXI_DATA_WIDTH = 128,				// 转换到的AXI数据位宽
	parameter   AXI_ADDR_WIDTH = 32					// AXI写地址宽度
	)(
	input	wire							i_axi_clk,
	input	wire							i_rst_n,

	/*--------------------------AXI写地址---------------------------*/ 
	input	wire							i_axi_aw_req_en,	// 写请求
	output	reg								o_axi_aw_ready,		// 地址准备信号
	input	wire	[7:0]					iv_axi_aw_burst_len,// 写地址突发长度
	input	wire	[AXI_ADDR_WIDTH-1:0]	iv_axi_aw_addr,		// 写地址

	/*--------------------------AXI写数据---------------------------*/ 
	input	wire	[AXI_DATA_WIDTH-1:0]	iv_axi_w_data,		// AXI写数据
	input	wire							i_axi_w_valid,		// 写buffer给的数据有效信号
	output	reg								o_axi_w_ready,		// wr_master准备好
	input	wire							i_axi_w_last,		// 最后一个数据的标志

	/*-------------------------写通道写地址--------------------------*/ 
	output	reg								o_m_axi_awvalid,	// 有效信号，表明此通道的地址控制信号有效
	input	wire							i_m_axi_awready,	// 表明“从”已经准备好,可以接收地址和对应的控制信号
	output	reg		[AXI_DATA_WIDTH-1:0]	ov_m_axi_awaddr,	// 写地址，给出一次写突发传输的写地址
	output	reg		[3:0]					ov_m_axi_awid,		// 写地址 ID，用来标志一组写信号
	output	reg		[7:0]					ov_m_axi_awlen,		// 突发长度，给出突发传输的次数
	output	reg		[1:0]					ov_m_axi_awburst,	// 突发类型
	output	reg		[2:0]					ov_m_axi_awsize,	// 突发大小，给出每次突发传输的字节数
	output	reg		[2:0]					ov_m_axi_awprot,	// 保护类型，表明一次传输的特权级及安全等级
	output	reg		[3:0]					ov_m_axi_awqos,		// 质量服务 QoS
	output	reg								o_m_axi_awlock,		// 总线锁信号，可提供操作的原子性
	output	reg		[3:0]					ov_m_axi_awcache,	// 内存类型，表明一次传输是怎样通过系统的

	/*-------------------------写通道写数据--------------------------*/ 
	output	reg		[AXI_DATA_WIDTH-1:0]	ov_m_axi_wdata,		// 写数据
	output	reg		[AXI_DATA_WIDTH/8-1:0]	ov_m_axi_wstrb,		// 写数据有效的字节线，用来表明哪8bits数据是有效的
	output	reg								o_m_axi_wlast,		// 表明此次传输是最后一个突发传输
	output	reg								o_m_axi_wvalid,		// 写有效，表明此次写有效
	input	wire							i_m_axi_wready,		// 表明从机可以接收写数据

	/*-------------------------写通道响应通道-------------------------*/ 
	input	wire 	[3:0]					iv_m_axi_bid,		// 写响应 IDTAG
	input	wire 	[1:0]					iv_m_axi_bresp,		// 写响应，表明写传输的状态
	input	wire 							i_m_axi_bvalid,		// 写响应有效
	output 	wire							o_m_axi_bready		// 表明主机能够接收写响应
);

	assign	o_m_axi_bready = 1'b1;

/*-------------------------------------------*\
					状态机
\*-------------------------------------------*/

	reg	[2:0]	cur_status;
	reg	[2:0]	nxt_status;

	localparam  AXI_WR_IDLE = 3'b000,
				AXI_WR_PRE  = 3'b001,
				AXI_WR_DATA = 3'b010,
				AXI_WR_END  = 3'b100;

	localparam 	BURST_TYPE_INCR = 2'b01;

/*-------------------------------------------*\
			  复位信号Sync定义
\*-------------------------------------------*/

  	(* dont_touch="true" *)	reg		axi_rstn_sync_d0;
  	(* dont_touch="true" *)	reg		axi_rstn_sync_d1;
  	(* dont_touch="true" *)	reg		axi_rstn_sync;

/*-------------------------------------------*\
			  	复位信号CDC
\*-------------------------------------------*/
	always @(posedge i_axi_clk) begin
		axi_rstn_sync_d0 <= i_rst_n;
		axi_rstn_sync_d1 <= axi_rstn_sync_d0;
		axi_rstn_sync    <= axi_rstn_sync_d1;
	end

	always @(posedge i_axi_clk) begin
		ov_m_axi_awprot  <= 3'b0;
		ov_m_axi_awid    <= 4'b0;
		ov_m_axi_awburst <= BURST_TYPE_INCR;
		o_m_axi_awlock   <= 1'b0;
		ov_m_axi_awcache <= 4'b0;
		ov_m_axi_awqos   <= 4'b0;
		ov_m_axi_wstrb   <= {AXI_DATA_WIDTH/8{1'b1}};
		// AXI宽度width和AxSize的对应关系 : width = (2^AxSize)*8
		ov_m_axi_awsize  <= (AXI_DATA_WIDTH == 512) ? 3'h6 :
						   	(AXI_DATA_WIDTH == 256) ? 3'h5 :
						   	(AXI_DATA_WIDTH == 128) ? 3'h4 :
						   	(AXI_DATA_WIDTH ==  64) ? 3'h3 :
						   	(AXI_DATA_WIDTH ==  32) ? 3'h2 : 
						   	(AXI_DATA_WIDTH ==  16) ? 3'h1 : 3'h0;
	end

/*-------------------------------------------*\
					状态机
\*-------------------------------------------*/

	always @(posedge i_axi_clk or negedge axi_rstn_sync) begin
		if(~axi_rstn_sync)
			cur_status <= AXI_WR_IDLE;
		else 
			cur_status <= nxt_status;
	end

	always @(*) begin
		if(~axi_rstn_sync)
			nxt_status <= AXI_WR_IDLE;
		else begin
			case (cur_status)
				AXI_WR_IDLE : begin
					if(i_axi_aw_req_en)
						nxt_status <= AXI_WR_PRE;
					else
						nxt_status <= cur_status;
				end
				AXI_WR_PRE  : nxt_status <= AXI_WR_DATA;
				AXI_WR_DATA : begin
					if(o_m_axi_wvalid && i_m_axi_wready && o_m_axi_wlast)
						nxt_status <= AXI_WR_END;
					else
						nxt_status <= cur_status;
				end
				AXI_WR_END : nxt_status <= AXI_WR_IDLE;
				default : nxt_status <= AXI_WR_IDLE;
			endcase
		end
	end

/*------------------------------------------------------*\
	输出给wr_buffer模块两个写ready信号(地址和数据各一个)
\*------------------------------------------------------*/

	always @(*) begin
		if(~axi_rstn_sync) begin
			o_axi_aw_ready <= 1'b0;
			o_axi_w_ready  <= 1'b0;
		end else begin
			o_axi_aw_ready <= (cur_status == AXI_WR_PRE);
			o_axi_w_ready  <= i_m_axi_wready;	// 直接将从机的写数据准备信号交给写buffer模块.
		end
	end

/*------------------------------------------------------*\
						写通道写地址
\*------------------------------------------------------*/
	
	// 地址有效信号:m开始的表示是与从机交互的信号
	always @(posedge i_axi_clk or negedge axi_rstn_sync) begin
		if(~axi_rstn_sync)
			o_m_axi_awvalid <= 1'b0;
		else if(o_m_axi_awvalid && i_m_axi_awready)
			o_m_axi_awvalid <= 1'b0;	// 此时握手成功,表明从机接收到了写地址,就拉低.
		else if(i_axi_aw_req_en && o_axi_aw_ready)
			o_m_axi_awvalid <= 1'b1;	// 准备好了写地址时就拉高.
		else
			o_m_axi_awvalid <= o_m_axi_awvalid;
	end

	// 锁存写地址和突发长度并输出
	always @(posedge i_axi_clk or negedge axi_rstn_sync) begin
		if(~axi_rstn_sync) begin
			ov_m_axi_awaddr <= {AXI_ADDR_WIDTH{1'b0}};
			ov_m_axi_awlen  <= 8'h0;
		end 
		else if(i_axi_aw_req_en && o_axi_aw_ready)begin	// 地址准备好时就接收并发送
			ov_m_axi_awaddr <= iv_axi_aw_addr;
			ov_m_axi_awlen  <= iv_axi_aw_burst_len;
		end
		else
		begin
			ov_m_axi_awaddr <= ov_m_axi_awaddr;
			ov_m_axi_awlen  <= ov_m_axi_awlen;
		end
	end

/*------------------------------------------------------*\
						写通道写数据
\*------------------------------------------------------*/

	// 数据有效信号
	always @(posedge i_axi_clk or negedge axi_rstn_sync) begin
		if(~axi_rstn_sync) 
			o_m_axi_wvalid <= 1'b0;
		else if(o_m_axi_wvalid && i_m_axi_wready && o_m_axi_wlast)
			o_m_axi_wvalid <= 1'b0;
		else if(i_axi_w_valid && o_axi_w_ready)
			o_m_axi_wvalid <= 1'b1;		// wr_buffer传来的数据有效且当前模块准备好写数据时就拉高
		else
			o_m_axi_wvalid <= o_m_axi_wvalid;
	end

	// 最后一个数据的标志信号
	always @(posedge i_axi_clk or negedge axi_rstn_sync) begin
		if(~axi_rstn_sync)
			o_m_axi_wlast <= 1'b0;
		else if(o_m_axi_wvalid && i_m_axi_wready && o_m_axi_wlast)
			o_m_axi_wlast <= 1'b0;		// valid信号表明主从机握手状态;last信号表明是最后一个数据.检测到最后一个拉高一个clk后就拉低.
		else if(i_axi_w_valid && o_axi_w_ready && i_axi_w_last)
			o_m_axi_wlast <= 1'b1;		// 当检测到wr_buffer发来最后一个数据时就拉高
		else
			o_m_axi_wlast <= o_m_axi_wlast;
	end

	// AXI4写数据
	always @(posedge i_axi_clk or negedge axi_rstn_sync) begin
		if(~axi_rstn_sync) 
			ov_m_axi_wdata <= {AXI_DATA_WIDTH{1'b0}};
		// 只要wr_buffer和当前模块(当前的准备信号即从机准备信号)都准备好了就接收数据发送.
		else if(i_axi_w_valid && o_axi_w_ready)
			ov_m_axi_wdata <= iv_axi_w_data;
		else
			ov_m_axi_wdata <= ov_m_axi_wdata;	// 此时valid信号无效,所以wdata给任何值都可以.
	end

endmodule

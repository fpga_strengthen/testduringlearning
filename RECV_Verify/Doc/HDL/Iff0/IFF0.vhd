
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_signed.all;
use ieee.std_logic_arith.conv_std_logic_vector;

entity IFF0 is
	port (	clock_in		: in std_logic;
			reset_in		: in std_logic;
			one_wire_b:    inout std_logic;			
			enable_out	: out std_logic
			);
end IFF0;

architecture beha of IFF0 is

	component RNG_8Bits
	port (	clock		: in std_logic;
			reset		: in std_logic;
						
			RNG_Byte	: out std_logic_vector(7 downto 0)
			);
	end component;
component small_micro_32 is
	generic (SECRET_KEY		: std_logic_vector(63 downto 0) := X"5555555555555555");
	port (	
			resetn			: in std_logic;
			clock			: in std_logic;
			
			OW_Command		: out std_logic_vector(3 downto 0);
			OW_Write_Byte	: out std_logic_vector(7 downto 0);
			RNG_Byte		: in std_logic_vector(7 downto 0);
			OW_Read_Byte	: in std_logic_vector(7 downto 0);
			OW_Busy			: in std_logic;
			OW_No_Device	: out std_logic;
			Enable			: out std_logic
			);
end component;
component One_Wire is
	generic (SYSCLOCK		: integer := 27000000);
	port (	resetn			: in std_logic;
			clock			: in std_logic;
			
			Address			: in std_logic_vector(3 downto 0);
			Write_Byte_in	: in std_logic_vector(7 downto 0);
			Read_Byte_out	: out std_logic_vector(7 downto 0);
			
			One_Wire_data	: inout std_logic;
			
			Busy			: out std_logic);
end component;
   
    
    signal rng,ow_byte,or_byte:std_logic_vector(7 downto 0);
signal ow_cmd:std_logic_vector(3 downto 0);
	signal en, b:std_logic;
	
begin
enable_out<=en;
	--process (clock_in,reset_in)
	--begin		
       --if (clock_in'event and clock_in = '1') then
   u1: RNG_8bits 
	port map(clock		=>clock_in,
				reset		=>en,
				RNG_Byte	=>rng);
	u2: small_micro_32 
	port map(clock				=>clock_in,
				resetn			=>reset_in,
				RNG_Byte			=>rng,
				OW_Read_Byte	=>or_byte,
				OW_Busy			=>b,
				OW_Command		=>ow_cmd,
				OW_Write_Byte	=>ow_byte,
				--Enable			=>enable_out,
				Enable			=>en
				);
	u3: One_Wire 
	port map(clock				=>clock_in,
				resetn			=>reset_in,
				Address			=>ow_cmd,
				Write_Byte_in		=>ow_byte,
				Read_Byte_out	=>or_byte,
				One_Wire_data	=>one_wire_b,
				Busy				=>b);  			
       -- end if;						
--	end process;	
	
	

end beha;
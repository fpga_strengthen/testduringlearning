function [ angle_fit ] = Phase_Correction( angle_tan )
    %对相位进行中心频率修正和2pi修正
    %0-2pi 转化为 0-8192
    % 输入为原始相位值
    % 输出为修正后的相位值
    %%
    %--------------------------------转换为matlab实现-----------------------------------------
    %对中心频率进行修正
    angle_fit = angle_tan - [1:length(angle_tan)]*1280;
    %对2pi进行修正
    for i=2:length(angle_tan)
        if(angle_tan(i)+4096<=angle_tan(i-1))
            angle_fit(i:end) = angle_fit(i:end) + 8192;
    %     elseif(angle_tan(i)-4096>angle_tan(i-1))    
    %         angle_fit(i:end) = angle_fit(i:end) - 8192;
        end %if
    end %for
    
    
    %定标
    %     angle_fit = FixPointNum(angle_fit,21,0);
    
    %--------------------------------verilog程序-----------------------------------------
    % always @ ( negedge i_rst_n or posedge i_clk )
    % begin
    %     if( !i_rst_n )
    %         angle_sum <= 21'd0;
    %    else if( i_cke_27M ) begin
    %       if( o_hilbert_data_out_dge == 1'b1 ) begin
    %           if ( angle_tan + 14'd4096 <= angle_tan_delay )//对2pi进行修正
    %               angle_sum <= 21'd6912;//8192-1280
    %           else
    %               angle_sum <= 21'd0 - 21'd1280;//减去中心频率
    %       end
    %       else if( angle_tan + 14'd4096 <= angle_tan_delay )
    %           angle_sum <= angle_sum + 21'd6912;
    %       else
    %           angle_sum <= angle_sum - 21'd1280;
    %   end
    %   else begin
    %         angle_sum <= angle_sum;
    %   end
    %        end
    %   ...
    % angle_fit<= {7'b0,angle_tan_delay} + angle_sum;//加上叠加量
    end
    
    
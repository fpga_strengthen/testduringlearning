/*=======================================================================================*\
  Filename    : transform.v
  Author      : 吕旌阳
  time        ：2016.08.12
  Description : 该模块完成对10bit报文转8比特报文
                1.读4个10比特字，保存5个字节
                2.shift模块给出start信号后，从shift模块读10比特字。由本模块给出读时钟并在时
                钟上跳沿后2拍读入数据。6拍读入一个字。
                2.对读入的10比特字做CRC校验，对存入RAM的转换后的字节数据做32位CRC
                3.RAM操作： a)写入转换后的字节数据及CRC；b)在输入输出请求后，输出数据及时钟；
                            c)擦除RAM，并读出校验                
               
  Called by   : 
  Revision History :
  Author              Date                  Version          Description
  郭庆阳              20160823              V1.1.0           调整时序和读写8bitRAM的处理
  郭庆阳              20160927              V1.1.1           对crc32_d8处的时序逻辑做了修改
  郭庆阳              20161028              V1.1.2           新加对相同报文的防护处理               
  Email       : 
  Company     :

\*========================================================================================*/
module TransformMsg(
                i_clk_81M,
                i_rst_n,            //低电平有效
                //新报文指示信号
                i_new_msg_flag,     //新报文指示信号，低电平有效            
                //与FistWord接口
                i_start,            //11bit向10bit转换完成标志，模块开始工作
                i_check_err,        //11bit转10bit查表时找到不符合规范的报文，脉冲
                i_read_data_sck,    //从11bit转10bit读入的10bit报文的时钟(使能)
                iv_read_data,       //从11bit转10bit读入的10bit报文
                //对外部接口                
                i_transport_en,     //transform输入数据使能
                i_out_data_sck,     //发送模块给的读时钟(读使能)
                ov_out_data,        //转换后的报文
                o_transform_ready   //转换成功                                         
                );

input i_clk_81M;
input i_rst_n;              
input i_new_msg_flag;
input i_start;              
input i_check_err;          
input i_read_data_sck;     
input [9:0]iv_read_data;    

input i_transport_en;       
input i_out_data_sck;       
output reg [7:0]ov_out_data;
output reg o_transform_ready;  
///////////////////////////////////////////////  
  `include "../Crc/Crc32_D10_Check.v"
  `include "../Crc/Crc32_D8_Gene.v"  
///////////////////////////////////////////////

//------------变量声明----------
reg new_msg_flag_r;
reg new_msg_flag_n;
reg new_msg_flag_p;
reg send_msg_ctrl;
reg start_r;
reg start_p;
reg check_err_long;
reg read_sck_r;
reg read_sck_p;             //取数据时钟上跳沿
reg [9:0]iv_read_data_temp1;//读入数据
reg [9:0]iv_read_data_temp2;//读入数据

reg [2:0]cnt_7;             //计数从1～7，每个字顺序统计一次
reg [6:0]cnt_read_data;     //1～87
reg [31:0]LFSR_CRC32_D10;
reg CRC32_D10_done;         //10bit报文CRC计算结束标志，电平
reg CRC32_D10_ok;           //10bit报文CRC计算结束结果，1为CRC校验正确，电平
reg wr_ram_en;              //RAM写使能
reg [6:0]wr_ram_addr;       //ram写地址
reg [7:0]wr_ram_data;       //data
reg [7:0]data_8bit;         //转换后的8bit数据
reg [31:0]LFSR_CRC32_D8;
reg CRC32_D8_clear;
reg  transport_en_r1;
reg  transport_en_r2;
wire transport_en_p;
reg  out_data_sck_r1;
reg  out_data_sck_r2;
wire out_data_sck_p;
reg  rd_ram_en;         //RAM读使能
reg  rd_ram_en_r1;      //打一拍
reg  rd_ram_en_r2;      //打一拍
reg  [6:0]rd_ram_addr;  //RAM读地址 
wire [7:0]rd_ram_data;  //RAM读数据      

/*****************************************************************************\
新报文指示信号取沿：
   1.下降沿表示有新数据即将到来；下降沿不同于start信号，数据还得比较长的时间
   才来；而start信号距离数据到来就差几个时钟，即new_msg_flag_n先于start_p；
   2.上升沿表示译码模块接收的数据已经出现了误码；
\******************************************************************************/

always @(negedge i_rst_n or posedge i_clk_81M) 
begin
    if (!i_rst_n)begin
        new_msg_flag_r <= 1'b0;
        new_msg_flag_n <= 1'b0;
        new_msg_flag_p <= 1'b0;
    end 
    else begin
        new_msg_flag_r <= i_new_msg_flag;
        new_msg_flag_n <= (~i_new_msg_flag) && new_msg_flag_r;
        new_msg_flag_p <= i_new_msg_flag && (~new_msg_flag_r);
    end   
end //always

always @(negedge i_rst_n or posedge i_clk_81M) 
begin
    if (!i_rst_n)
        send_msg_ctrl <= 1'b0;
    else if(rd_ram_addr != 7'd0)//来取数据时复位
        send_msg_ctrl <= 1'b0;
    else if(new_msg_flag_p)//上升沿复位
        send_msg_ctrl <= 1'b0;
    else if(new_msg_flag_n)//只有在新报文标志下降沿时才可以启动发送
        send_msg_ctrl <= 1'b1;
    else
        send_msg_ctrl <= send_msg_ctrl;
end //always

//------------------------------------------------------------------------------//

//取工作信号i_start信号上跳沿作为工作开始标志
always @(negedge i_rst_n or posedge i_clk_81M) 
begin
    if (!i_rst_n) begin
        start_r <= 1'b0;
        start_p <= 1'b0;        
    end
    else begin
        start_r <= i_start;
        start_p <= i_start && (~start_r);
    end
end //always

//对i_check_err进行展宽，否则时长不够触发下一逻辑
always @(negedge i_rst_n or posedge i_clk_81M) 
begin
    if (!i_rst_n)
        check_err_long <= 1'b0;
    else if(start_p)
        check_err_long <= 1'b0;
    else if(i_check_err)
        check_err_long <= 1'b1;
    else
        check_err_long <= check_err_long;
end

//取数据时钟i_read_data_sck_r信号上跳沿
always @(negedge i_rst_n or posedge i_clk_81M) 
begin
    if (!i_rst_n) begin
        read_sck_r <= 1'b0;
        read_sck_p <= 1'b0;
    end
    else begin
        read_sck_r <= i_read_data_sck;
        read_sck_p <= i_read_data_sck && (~read_sck_r);
    end
end //always

//在时钟i_read_data_sck_p上跳沿，取数据iv_read_data
always @(negedge i_rst_n or posedge i_clk_81M) 
begin
    if (!i_rst_n) begin
        iv_read_data_temp1 <= 10'b0;
        iv_read_data_temp2 <= 10'b0;
    end
    else if(read_sck_p) begin
        iv_read_data_temp1 <= iv_read_data;
        iv_read_data_temp2 <= iv_read_data_temp1;
    end
end //always

//计数器控制拼接和crc过程
always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if (!i_rst_n)
        cnt_7 <= 3'b0;
    else if(start_p)
        cnt_7 <= 3'b0;
    else if(read_sck_p)
        cnt_7 <= 3'd1;
    else if (cnt_7 != 3'b0)
        cnt_7 <= cnt_7 + 3'd1;
    else 
        cnt_7 <= cnt_7;
end 
//------------------------------------------------------------------------------//
//输入信号字统计。输入10*(83+4CRC)共87个字，bit11to10L290注释为只读取前83个报文数据
always @(negedge i_rst_n or posedge i_clk_81M) 
begin
    if (!i_rst_n)begin 
        cnt_read_data <= 7'b0;
    end
    else if(start_p) begin
        if(read_sck_p) //如果star_p与read_sck同时到，则为1
            cnt_read_data <= 7'd1;        
        else //如果star_p与read_sck不是同时到，则复位
            cnt_read_data <= 7'd0;      
    end
    else if(read_sck_p) begin
        if(cnt_read_data == 7'd87)  //记到87个归零，该帧应该不会再重新计数了
            cnt_read_data <= 7'd0;
        else              //统计新到的字
            cnt_read_data <= cnt_read_data + 7'd1;        
    end
end //always

//--------------------------------------对10bit数据做CRC校验--------------------//

//检查输入信号CRC
always @(negedge i_rst_n or posedge i_clk_81M) 
begin
    if (!i_rst_n) 
        LFSR_CRC32_D10 <= 32'b0;
    else if(start_p) //当一组报文到达时，将寄存器组清空
        LFSR_CRC32_D10 <= 32'b0;
    else if( (cnt_read_data >= 7'd2) && (cnt_7 == 3'd1) )//start_p信号后一拍，从第二字节开始做CRC  
        LFSR_CRC32_D10 <= CRC32check_D10(iv_read_data_temp1,LFSR_CRC32_D10);
    else   
        LFSR_CRC32_D10 <= LFSR_CRC32_D10;   
end //always

//输出CRC结果与采样使能，电平信号
always @(negedge i_rst_n or posedge i_clk_81M) 
begin
    if (!i_rst_n) begin
        CRC32_D10_done <= 1'b0;
        CRC32_D10_ok   <= 1'b0; 
    end
    else if( new_msg_flag_n || transport_en_p || start_p)begin
        CRC32_D10_done <= 1'b0;
        CRC32_D10_ok   <= 1'b0;
    end
    else if((cnt_read_data == 7'd87) && (cnt_7 >= 3'd2)) begin //当一组报文的CRC计算完毕时，输出CRC结果
            CRC32_D10_done <= 1'b1;  //输出CRC校验标志
        if(LFSR_CRC32_D10 == 32'b0)  //输出CRC校验结果
            CRC32_D10_ok   <= 1'b1;  //CRC正确
        else
            CRC32_D10_ok   <= 1'b0; 
    end
    else begin
        CRC32_D10_done <= CRC32_D10_done;
        CRC32_D10_ok   <= CRC32_D10_ok; 
    end
end //always

//---------------------------10bit——8bit-拼接-----------------------------------//
//cnt_7==1时进行拼接
always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if (!i_rst_n)
        data_8bit  <= 8'b0;
    else if(start_p)
        data_8bit  <= 8'b0;
    else if((cnt_read_data < 7'd83) && (cnt_read_data >=7'd1)) begin //报文有83+4个字，剩余4个字是CRC，前83个需要进行拼接
        case(cnt_read_data[1:0])
            2'b01:begin  
                if(cnt_7 == 3'b1) //计数器为1时，准备写数据和使能，同时地址加1
                    data_8bit <= iv_read_data_temp1[9:2];
                else 
                    data_8bit <= data_8bit ;            
            end
            2'b10:begin
                if(cnt_7 == 3'b1)//计数器为1时，准备写数据和使能，同时地址加1
                    data_8bit <= {iv_read_data_temp2[1:0],iv_read_data_temp1[9:4]};
                else 
                    data_8bit <= data_8bit;            
            end
            2'b11:begin
                if(cnt_7 == 3'b1) //计数器为1时，准备写数据和使能，同时地址加1
                    data_8bit <= {iv_read_data_temp2[3:0],iv_read_data_temp1[9:6]};
                else
                    data_8bit <= data_8bit;            
            end
            2'b00:begin
                if(cnt_7 == 3'b1)//计数器为1时，准备写数据和使能，同时地址加1
                    data_8bit <= {iv_read_data_temp2[5:0],iv_read_data_temp1[9:8]};
                else if(cnt_7 == 3'd4) //计数器为4时，准备写数据和使能，同时地址加1
                    data_8bit <= iv_read_data_temp1[7:0];
                else
                    data_8bit <= data_8bit;                           
            end
            default: 
                data_8bit <= data_8bit;            
        endcase
    end
    else if(cnt_read_data == 7'd83) begin//拼接数据的最后一位特殊处理
        if(cnt_7 == 3'd1)//计数器为1时，准备写数据和使能，同时地址加1
            data_8bit <= {iv_read_data_temp2[3:0],iv_read_data_temp1[9:6]};
        else if(cnt_7 == 3'd4)
            data_8bit <= {iv_read_data_temp1[5:0],2'b0};
        else
            data_8bit <= data_8bit;  
    end
end         
//----------------------------------data_8bit 写进RAM---------------------------//
//计数器cnt_7==2时或者等于5时，data_8bit写进RAM，拉高写使能，同时写地址加一。
//ram地址从1->104，且cnt_7==5时优先级最高；
always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if (!i_rst_n)begin        
        wr_ram_en   <= 1'b0;
        wr_ram_addr <= 7'd0;
        wr_ram_data <= 8'b0;
    end
    else if(start_p)begin        
        wr_ram_en   <= 1'b0;
        wr_ram_addr <= 7'd0;
        wr_ram_data <= 8'b0;
    end
    else if(rd_ram_addr >= 7'd1)begin//读优先，只要开始读了就不在写数据 
        wr_ram_en   <= 1'b0;
        wr_ram_addr <= 7'd0;
        wr_ram_data <= 8'b0;   
    end
    else if( (cnt_read_data > 7'd83) && (cnt_7 == 3'd2) )begin       
        wr_ram_en   <= 1'b0;
        wr_ram_addr <= 7'd0;
        wr_ram_data <= 8'b0;
    end   
    else if( (cnt_read_data < 7'd83) && (cnt_read_data >= 7'd1) )begin 
        if( (cnt_read_data[1:0] == 2'd0) && (cnt_7 == 3'd2) )begin
            wr_ram_en   <= 1'b1;
            wr_ram_addr <= wr_ram_addr + 7'd1;
            wr_ram_data <= data_8bit;           
        end
        else if( (cnt_read_data[1:0] == 2'd0) && (cnt_7 == 3'd5) )begin
            wr_ram_en   <= 1'b1;
            wr_ram_addr <= wr_ram_addr + 7'd1;
            wr_ram_data <= data_8bit;
        end
        else if( (cnt_read_data[1:0] != 2'd0) && (cnt_7 == 3'd2) ) begin        
            wr_ram_en   <= 1'b1;
            wr_ram_addr <= wr_ram_addr + 7'd1;
            wr_ram_data <= data_8bit;
        end
        else begin
            wr_ram_en   <= 1'b0;
            wr_ram_addr <= wr_ram_addr;
            wr_ram_data <= wr_ram_data;
        end
    end
    else if( (cnt_read_data == 7'd83) && ( (cnt_7 == 3'd2) || (cnt_7 == 3'd5) ) ) begin
        wr_ram_en   <= 1'b1;
        wr_ram_addr <= wr_ram_addr + 7'd1;
        wr_ram_data <= data_8bit; 
    end  
    else begin       
        wr_ram_en   <= 1'b0;
        wr_ram_addr <= wr_ram_addr;
        wr_ram_data <= wr_ram_data;
    end
end         
//----------------------------8bit CRC校验--------------------------------------//
//写数据CRC32_8bit
always @(negedge i_rst_n or posedge i_clk_81M) 
begin
    if (!i_rst_n)
        LFSR_CRC32_D8 <= 32'b0;        
    else if(rd_ram_addr >= 7'd1)//在外部读本地8bit的ram时，该crc32寄存器组不能被清0
        LFSR_CRC32_D8 <= LFSR_CRC32_D8;
    else if(start_p)//当新一组报文到达时、解调数据出现误码时要将crc寄存器组清空 || new_msg_flag_p
        LFSR_CRC32_D8 <= 32'b0;            
    else if(wr_ram_en)
        LFSR_CRC32_D8 <= getCRC32_D8(wr_ram_data,LFSR_CRC32_D8);       
    else
        LFSR_CRC32_D8 <= LFSR_CRC32_D8;
end 

//检查上一次CRC32寄存器组是否清零
always @(negedge i_rst_n or posedge i_clk_81M) 
begin
    if (!i_rst_n)
        CRC32_D8_clear <= 1'b0;
    else if(new_msg_flag_n || transport_en_p || start_p)//复位为0
        CRC32_D8_clear <= 1'b0;
    else if( (cnt_read_data == 7'd1) && (cnt_7 == 3'd1) && (LFSR_CRC32_D8 == 32'b0) ) //在第一个8bit未进行crc32计算时输出校验结果
        CRC32_D8_clear <= 1'b1;
    else
        CRC32_D8_clear <= CRC32_D8_clear;
end

/**************************************************\
对于连续的相同报文只发一次：
    1、CRC32_D10校验正确（隐含写完104个8bit数据）；
    2、上次的CRC32_D8寄存器组结果清0正确；
    3、新报文发送控制信号拉高。
以上都满足时才通知下层取数据
\**************************************************/

//转换完成信号
always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if (!i_rst_n)
        o_transform_ready <= 1'b0;
    else if( new_msg_flag_n || transport_en_p)//新数据到来和下层取数据后置为无效
        o_transform_ready <= 1'b0;    
    else if(CRC32_D10_ok && CRC32_D8_clear && send_msg_ctrl)
        o_transform_ready <= 1'b1;
    else
        o_transform_ready <= o_transform_ready;
end

//-----------------------------------下层取数据操作----------------------------//

//需跨时钟域处理 组帧模块所用时钟为27M
always @(negedge i_rst_n or posedge i_clk_81M) 
begin
    if (!i_rst_n) begin
        transport_en_r1 <= 1'b0;
        transport_en_r2 <= 1'b0;        
    end
    else begin
        transport_en_r1 <= i_transport_en;
        transport_en_r2 <= transport_en_r1;        
    end
end //always 

assign transport_en_p = transport_en_r1 && (~transport_en_r2);

//需跨时钟域处理 组帧模块所用时钟为27M
always @(negedge i_rst_n or posedge i_clk_81M) 
begin
    if (!i_rst_n) begin
        out_data_sck_r1 <= 1'b0;
        out_data_sck_r2 <= 1'b0;
    end
    else begin
        out_data_sck_r1 <= i_out_data_sck;
        out_data_sck_r2 <= out_data_sck_r1;        
    end
end 

assign out_data_sck_p = out_data_sck_r1 && (~out_data_sck_r2);

//--------------------------------------RAM读地址-------------------------------//
always @ (negedge i_rst_n or posedge i_clk_81M)
begin
    if(!i_rst_n)begin
        rd_ram_en   <= 1'd0;
        rd_ram_addr <= 7'd0;                
    end
    else if(check_err_long)begin//如果CRC32_D10一直校验不对，或者是上一模块查表错误，那么该RAM永远不会被读出数据//(!(CRC32_D10_done && CRC32_D10_ok))
        rd_ram_en   <= 1'd0;    //如果i_start来的时候较早，而后续模块没取完RAM中数据，也取不完了
        rd_ram_addr <= 7'd0;
    end     
    else if( transport_en_p || (rd_ram_addr >= 7'd108) )begin
        rd_ram_en   <= 1'd0;
        rd_ram_addr <= 7'd0;
    end
    else if( out_data_sck_p && (rd_ram_addr >= 7'd104) )begin//读完104字节后，再来sck不读ram直接发寄存器值
        rd_ram_en   <= 1'd0;        
        rd_ram_addr <= rd_ram_addr + 7'd1;    
    end 
    else if( out_data_sck_p )begin
        rd_ram_en   <= 1'd1;        
        rd_ram_addr <= rd_ram_addr + 7'd1;               
    end 
    else begin
        rd_ram_en   <= 1'd0;
        rd_ram_addr <= rd_ram_addr;
    end
end
//---------------------------读取RAM8bit数据以及生成8bitCRC操作-------------//
always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if (!i_rst_n)begin
        rd_ram_en_r1 <= 1'b0;
        rd_ram_en_r2 <= 1'b0;
    end
    else begin 
        rd_ram_en_r1 <= rd_ram_en;
        rd_ram_en_r2 <= rd_ram_en_r1;
    end
end

always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if (!i_rst_n)       
        ov_out_data <= 7'b0; 
    else if(transport_en_p)
        ov_out_data <= 7'b0;
    else if( rd_ram_en_r2 && (rd_ram_addr <= 7'd104) )        
        ov_out_data <= rd_ram_data;     
    else if(rd_ram_addr == 7'd105)
        ov_out_data <= LFSR_CRC32_D8[31:24];    
    else if(rd_ram_addr == 7'd106)
        ov_out_data <= LFSR_CRC32_D8[23:16];
    else if(rd_ram_addr == 7'd107)
        ov_out_data <= LFSR_CRC32_D8[15:8];
    else if(rd_ram_addr == 7'd108)      
        ov_out_data <= LFSR_CRC32_D8[7:0];            
    else         
        ov_out_data <= ov_out_data;
end
//------------------------------------------------------------------------------//
   
ram8bit ram8bit_u ( 
          .data(wr_ram_data),
          .rdaddress(rd_ram_addr),
          .rdclock(i_clk_81M),
          .rden(rd_ram_en),
          .wraddress(wr_ram_addr),
          .wrclock(i_clk_81M),
          .wren(wr_ram_en),
          .q(rd_ram_data)
          );

////////////////////////////////////////////////////////////////////////////////          
          
endmodule
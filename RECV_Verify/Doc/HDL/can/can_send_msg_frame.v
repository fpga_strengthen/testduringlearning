//+FHEADER //////////////////////////////////////////////////////////////////////
//  版权所有者：                                                                 
//  Copyright 2014. All Rights Reserved.                                         
//  保密级别：                                                                   
//---------------------------------------------------------------------------    
//  文件名 :  can_send_msg_frame.v                                                     
//  原始作者：丁云冰                                                             
//  联系方式：                                                                   
// --------------------------------------------------------------------------    
// 版本升级历史： 添加了i_busy控制信号                                                                
// 版本生效日期：                                                                
// 1.0 2014-03-16   丁云冰  第1版 
// 1.1 2015-10-23   郭庆阳 
// 2020.6.8 王雪                                                                                              
//---------------------------------------------------------------------------    
// 关键字 :                                            
//---------------------------------------------------------------------------    
// 模块功能:  send one can frame to the bus                                   
//                        
// --------------------------------------------------------------------------    
// 参数文件名：                                                                  
// --------------------------------------------------------------------------    
// 重复使用问题：                                                                
// 复位策略:负跳变沿异步复位信号 i_rst_n                                         
// 时钟域 : i_clk_27M                                                            
// 关键时序: N/A                                                                 
// 是否需要实例引用别的模块 : No                                                 
// 可综合否 : Yes                                                                
//-FHEADER //////////////////////////////////////////////////////////////////////
module can_send_msg_frame
       (
         //time & rst
         i_clk_27M,
         i_rst_n,
         
         //input control
         i_mcp_int,        //电平信号，低有效，代表mcp1515有数据要发送给FPGA
         i_send_frame_en,  //电平信号，开始发送帧使能
         i_send_reg_ok,    //脉冲信号，向寄存器写入数据成功
         i_read_ok,        //脉冲信号，读取一个字节成功
         i_mody_ok,        //脉冲信号，位修改完成
         i_busy,
         
         //data input
         i_reg_out,        //从mcp2515读出的数据
         i_byte_ready,
         i_msg_data_in,    //要发送的数据
         iv_set_id,      //本地ID
         //output
         o_send_req,       //输出到control模块的发送数据帧请求
         o_send_frame_ok,  //脉冲信号，发帧成功
         o_send_en,        //电平信号，使能can_send_register
         o_read_en,        //电平信号，使能can_read-register
         o_mody_en,        //电平信号
         o_sendbyte_ok, //脉冲信号，表示地址变化
         
         //data out
         o_send_add,       //写寄存器地址
         o_send_data,      //写数据
         o_read_add,       //读寄存器地址
         o_read_num,       //读数目
         o_mody_add,       //修改寄存器地址
         o_mody_data,      //修改数据
         o_mody_screen     //屏蔽位
         );
    input i_clk_27M;
    input i_rst_n;
    input i_mcp_int;
    input i_send_frame_en;
    input i_send_reg_ok;
    input i_read_ok;
    input i_mody_ok;
    input i_busy;
    input[7:0] i_reg_out;
    input i_byte_ready;
    input[7:0] i_msg_data_in;
    input[63:0] iv_set_id;
    
    output o_send_req;
    output o_send_frame_ok;
    output o_send_en;
    output o_read_en;
    output o_mody_en;
    output o_sendbyte_ok;
    output[7:0] o_send_add;
    output[7:0] o_send_data;
    output[7:0] o_read_add;
    output[7:0] o_read_num;
    output[7:0] o_mody_add;
    output[7:0] o_mody_data;
    output[7:0] o_mody_screen;
  
///////////////////////////////////////////////
  `include"parameter.v"
///////////////////////////////////////////////
    parameter IDLE           = 22'b0000000000000000000001,  //wait for start signal
              SET_TXB0SIDH   = 22'b0000000000000000000010,  //send reset signal to msp2515
              SET_TXB0SIDL   = 22'b0000000000000000000100,  //read the state of mcp2515
              SET_TXB0EID8   = 22'b0000000000000000000011,  
              SET_TXB0EID0   = 22'b0000000000000000000111,
              SET_TXB0DLC    = 22'b0000000000000000001000,  //set reg   
              SET_TXB0D0     = 22'b0000000000000000010000,  //...
              SET_TXB0D1     = 22'b0000000000000000100000,  
              SET_TXB0D2     = 22'b0000000000000001000000,  
              SET_TXB0D3     = 22'b0000000000000010000000,  
              SET_TXB0D4     = 22'b0000000000000100000000,   
              SET_TXB0D5     = 22'b0000000000001000000000,
              SET_TXB0D6     = 22'b0000000000010000000000,
              SET_TXB0D7     = 22'b0000000000100000000000,
              SEND_FRAME     = 22'b0000000001000000000000,
              WAIT           = 22'b0000000010000000000000,
              READ_REG       = 22'b0000000100000000000000,
              WAIT1          = 22'b0000001000000000000000,              
              READ_REG1      = 22'b0000010000000000000000,
              DELAY2         = 22'b0000100000000000000000,
              WAIT2          = 22'b0001000000000000000000,
              SET_CANIF      = 22'b0010000000000000000000,
              STOP           = 22'b0100000000000000000000,
              CLEAR          = 22'b1000000000000000000000;
//////////////////////////////////////////////
    reg[21:0] cstate;
    reg[21:0] nstate;
    
    wire o_send_en;
    reg o_read_en;
    reg[7:0] o_send_add;
    reg[7:0] o_send_data;
    reg[7:0] o_read_add;
    reg[7:0] o_read_num;
    reg o_send_frame_ok;
    reg o_mody_en;
    reg[7:0] o_mody_add;
    reg[7:0] o_mody_data;
    reg[7:0] o_mody_screen; 
    reg change_addr_en; 
    
    wire o_sendbyte_ok;
    reg[7:0] txb0sidl_local;
    reg i_send_frame_en_temp1;
    reg send_frame_en_p;
//////////////////////////////////////////////
    reg byte_ready_temp1;
    reg byte_ready_p;
    always@(posedge i_clk_27M or negedge i_rst_n)
        begin
            if(!i_rst_n)begin
                byte_ready_temp1 <= 1'b0;
                byte_ready_p <= 1'b0;
            end
            else begin
                byte_ready_temp1 <= i_byte_ready;
                byte_ready_p <= i_byte_ready && (~byte_ready_temp1);
            end
        end
    reg [7:0]msg_data_in;
    always@(posedge i_clk_27M or negedge i_rst_n)
        begin
            if(!i_rst_n)
                msg_data_in <= 8'd0;
            else 
                msg_data_in <= i_msg_data_in;
        end
    //利用字节发送计数器做包有效脉冲 8个字节为一包
    ////////////////////////////////////
    reg[3:0]cnt_send_data;
    wire o_send_req;
    assign o_send_req = (cnt_send_data == 4'd1)?1'b1:1'b0;
    //发送字节计数器
    always @(negedge i_rst_n or posedge i_clk_27M)
    begin
        if(!i_rst_n) begin
            cnt_send_data <= 4'b0;
        end
        else if((cnt_send_data == 4'd8) && o_sendbyte_ok) begin//发送完一帧数据清0
            cnt_send_data <= 4'b0;
        end
        else if((cnt_send_data != 4'b0) && o_sendbyte_ok) begin//发送完1字节计数加1
            cnt_send_data <= cnt_send_data + 4'd1;
        end
        else if(byte_ready_p) begin//有数据待发送,计数置1
            cnt_send_data <= 4'd1;
        end
        else begin
            cnt_send_data <= cnt_send_data;
        end
    end
////////////////////////////////////////////// 
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if(!i_rst_n)begin
            i_send_frame_en_temp1 <= 1'b0;
            send_frame_en_p <= 1'b0;
        end
        else begin
            i_send_frame_en_temp1 <= i_send_frame_en;
            send_frame_en_p <= i_send_frame_en && (~i_send_frame_en_temp1);
        end
    end
         
//////////////////////////////////////////////
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if(!i_rst_n) 
            cstate <= IDLE;
        else 
            cstate <= nstate;
    end
    
    always@(cstate
        or send_frame_en_p
        or i_send_reg_ok
        or i_reg_out
        or i_read_ok
        or i_busy
        or i_mcp_int
        or i_mody_ok
             )
    begin
        case(cstate)
        IDLE: 
            if( send_frame_en_p )
                nstate = SET_TXB0SIDH; 
            else
                nstate = IDLE;
        SET_TXB0SIDH: 
            if(i_send_reg_ok)
                nstate = SET_TXB0SIDL;
            else
                nstate = SET_TXB0SIDH;
        SET_TXB0SIDL: 
            if(i_send_reg_ok && (!iv_set_id[59]))
                nstate = SET_TXB0DLC;
            else if(i_send_reg_ok && iv_set_id[59])
                nstate = SET_TXB0EID8;
            else
            nstate = SET_TXB0SIDL;          
        SET_TXB0EID8:
            if(i_send_reg_ok)
                nstate = SET_TXB0EID0;
            else
                nstate = SET_TXB0EID8;
        SET_TXB0EID0:
            if(i_send_reg_ok)
                nstate = SET_TXB0DLC;
            else
                nstate = SET_TXB0EID0;
        SET_TXB0DLC: 
            if(i_send_reg_ok)
                nstate = SET_TXB0D0;
            else
                nstate = SET_TXB0DLC; 
        SET_TXB0D0: 
            if(i_send_reg_ok)
                nstate = SET_TXB0D1;
            else
                nstate = SET_TXB0D0;        
        SET_TXB0D1: 
            if(i_send_reg_ok)
                nstate = SET_TXB0D2;
            else
                nstate = SET_TXB0D1;        
        SET_TXB0D2: 
            if(i_send_reg_ok)
                nstate = SET_TXB0D3;
            else
                nstate = SET_TXB0D2;        
        SET_TXB0D3: 
            if(i_send_reg_ok)
                nstate = SET_TXB0D4;
            else
                nstate = SET_TXB0D3;        
        SET_TXB0D4: 
            if(i_send_reg_ok)
                nstate = SET_TXB0D5;
            else
                nstate = SET_TXB0D4;        
        SET_TXB0D5: 
            if(i_send_reg_ok)
                nstate = SET_TXB0D6;
            else
                nstate = SET_TXB0D5;        
        SET_TXB0D6: 
            if(i_send_reg_ok)
                nstate = SET_TXB0D7;
            else
                nstate = SET_TXB0D6;        
        SET_TXB0D7: 
            if(i_send_reg_ok)
                nstate = SEND_FRAME;
            else
                nstate = SET_TXB0D7;
        SEND_FRAME:  
            if(i_send_reg_ok)
                nstate = WAIT;
            else
                nstate = SEND_FRAME;
        WAIT:   
            if(~i_busy)
                nstate = READ_REG;
            else
                nstate =  WAIT ; 
        READ_REG: 
            if(i_read_ok)
                nstate = WAIT1;
            else 
                nstate = READ_REG;
/////////////////////////////////////////////////
        WAIT1:
            if(~i_mcp_int)//
                nstate = READ_REG1;
            else
                nstate = WAIT1;
/////////////////////////////////////////////////
        READ_REG1: 
            if(i_read_ok)
                nstate = DELAY2;
            else 
                nstate = READ_REG1; 
////////////////////////////////////////////////// 
        DELAY2:
            nstate = WAIT2;
        WAIT2:
            if(i_reg_out[4])
                nstate = SET_CANIF;
            else
                nstate = READ_REG1;
/////////////////////////////////////////////////
        SET_CANIF: 
            if(i_mody_ok )
                nstate = STOP;
            else 
                nstate = SET_CANIF; 
        STOP:   nstate = CLEAR;
        CLEAR:  nstate = IDLE;           
        default: nstate = IDLE;     
        endcase
    end
//////////////////////////////////////////////
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if(!i_rst_n) begin
            //o_send_en <=1'd0;
            o_read_en <=1'd0;
            o_read_add <= 0;
            o_read_num <= 0;
            o_send_add <= 0;
            o_send_data <= 0;
            o_send_frame_ok <=0;
            o_mody_en <=0;
            o_mody_add <=0;
            o_mody_data <=0;
            o_mody_screen <=0;
            change_addr_en <=0;          
        end
        else
        case(cstate)
        IDLE: begin
            //o_send_en <=1'd0;
            o_read_en <=1'd0;
            o_read_add <= 0;
            o_read_num <= 0;
            o_send_add <= 0;
            o_send_data <= 0;
            o_send_frame_ok <= 0; 
            o_mody_en <=0;
            o_mody_add <=0;
            o_mody_data <=0;
            o_mody_screen <=0;
            change_addr_en <=0;          
        end  
        SET_TXB0SIDH: begin
            //o_send_en <=1'd1;
            o_read_en <=1'd0;
            o_read_add <= 0;
            o_read_num <= 0;
            o_send_add <=  8'h51;//`TXB0SIDH_ADD;
            o_send_data <= iv_set_id[58:51];//`TXB0SIDH;            
        end 
        SET_TXB0SIDL: begin
            //o_send_en <=1'd1;
            o_read_en <=1'd0;
            o_read_add <= 0;
            o_send_add <=  8'h52;//`TXB0SIDL_ADD;
            o_send_data <= {iv_set_id[50:48],1'b0,iv_set_id[59],1'b0,iv_set_id[47:46]};//`TXB0SIDL;     
        end
        SET_TXB0EID8: begin
            //o_send_en <=1'd1;
            o_read_en <=1'd0;
            o_read_add <= 0;
            o_read_num <= 0;
            o_send_add <=  8'h53;//`TXB0SIDH_ADD;
            o_send_data <= iv_set_id[45:38];//`TXB0SIDH;            
        end
        SET_TXB0EID0: begin
            //o_send_en <=1'd1;
            o_read_en <=1'd0;
            o_read_add <= 0;
            o_read_num <= 0;
            o_send_add <=  8'h54;//`TXB0SIDH_ADD;
            o_send_data <= iv_set_id[37:30];//`TXB0SIDH;            
        end
        SET_TXB0DLC: begin
            //o_send_en <=1'd1;
            o_read_en <=1'd0;
            o_read_add <= 0;
            o_send_add <= 8'h55;//`TXB0DLC_ADD;
            o_send_data <= `TXB0DLC;        //发送缓冲器n数据长度寄存器
        end   
        SET_TXB0D0: begin
            //o_send_en <=1'd1;
            o_read_en <=1'd0;
            o_read_add <= 0;
            o_send_add <= 8'h56;//`TXB0D0_ADD;
            o_send_data <= msg_data_in;
            change_addr_en <= 1'd1;//发送第一个数据字节,置1
        end 
        SET_TXB0D1: begin
            //o_send_en <=1'd1;
            o_read_en <=1'd0;
            o_read_add <= 0;
            o_send_add <= 8'h57;//`TXB0D1_ADD;
            o_send_data <= msg_data_in ;
        end     
        SET_TXB0D2: begin
            //o_send_en <=1'd1;
            o_read_en <=1'd0;
            o_read_add <= 0;
            o_send_add <= 8'h58;//`TXB0D2_ADD;
            o_send_data <= msg_data_in;
        end  
        SET_TXB0D3:begin
            //o_send_en <=1'd1;
            o_read_en <=1'd0;
            o_read_add <= 0;
            o_send_add <= 8'h59;//`TXB0D3_ADD;
            o_send_data <= msg_data_in;
        end
        SET_TXB0D4:begin
            //o_send_en <=1'd1;
            o_read_en <=1'd0;
            o_read_add <= 0;
            o_send_add <= 8'h5a;//`TXB0D4_ADD;
            o_send_data <= msg_data_in;
        end 
        SET_TXB0D5: begin
            //o_send_en <=1'd1;
            o_read_en <=1'd0;
            o_read_add <= 0;
            o_send_add <= 8'h5b;//`TXB0D5_ADD;
            o_send_data <= msg_data_in;
        end 
        SET_TXB0D6:begin
            //o_send_en <=1'd1;
            o_read_en <=1'd0;
            o_read_add <= 0;
            o_send_add <= 8'h5c;//`TXB0D6_ADD;
            o_send_data <= msg_data_in;
        end 
        SET_TXB0D7: begin
            //o_send_en <=1'd1;
            o_read_en <=1'd0;
            o_read_add <= 0;
            o_send_add <= 8'h5d;//`TXB0D7_ADD;
            o_send_data <= msg_data_in;
        end  
        SEND_FRAME: begin
            //o_send_en <=1'd1;
            o_read_en <=1'd0;
            o_read_add <= 0;
            change_addr_en <=0; //发送完一帧数据,清0
            o_send_add <= 8'h50;//`TX0CTRL_ADD;     //使能报文发送
            o_send_data <= 8'b00001011;//`CAN_SEND;
        end
        WAIT: begin
            //o_send_en <=1'd0;
            o_read_en <=1'd0;
            o_read_add <= 0;
            o_read_num <= 0;
            o_send_add <= 0;
            o_send_data <= 0;
            o_send_frame_ok <=0;
            o_mody_en <=0;
            o_mody_add <=0;
            o_mody_data <=0;
            o_mody_screen <=0;
            change_addr_en <=0;  
        end 
/////////////////////////////////test         
        READ_REG: begin
            //o_send_en <=1'd0;
            o_read_en <= 1'd1;
            o_read_add <= `CANINTF_ADD;
            //o_read_add <= 8'h50;//测试发送控制寄存器
            o_read_num <= 8'd1;
        end
        WAIT1: begin
            //o_send_en <=1'd0;
            o_read_en <=1'd0;
            o_read_add <= 0;
            o_read_num <= 0;
            o_send_add <= 0;
            o_send_data <= 0;
        end 
////////////////////////////////////test          
        READ_REG1: begin
            o_mody_en <=0;
            //o_send_en <=1'd0;
            o_read_en <= 1'd1;
            o_read_add <= `CANINTF_ADD;
            o_read_num <= 8'd1;
        end
        WAIT2: begin
           // o_send_en <=1'd0;
            o_read_en <=1'd0;
            o_read_add <= 0;
            o_read_num <= 0;
            o_send_add <= 0;
            o_send_data <= 0;
        end 
        SET_CANIF: begin
            o_mody_en <=1;
            o_mody_add <=`CANINTF_ADD;
            o_mody_data <= 0;
            o_mody_screen <=8'h10;               
        end       
        STOP: begin
            //o_send_en <=1'd0;
            o_read_en <=1'd0;
            o_read_add <= 0;
            o_read_num <= 0;
            o_send_add <= 0;
            o_send_data <= 0;
            o_send_frame_ok <= 1; 
            o_mody_en <=0;
            o_mody_add <=0;
            o_mody_data <=0;
            o_mody_screen <=0;      
        end 
        CLEAR: begin
            //o_send_en <=1'd0;
            o_read_en <=1'd0;
            o_read_add <= 0;
            o_read_num <= 0;
            o_send_add <= 0;
            o_send_data <= 0;
            o_send_frame_ok <= 0; 
            o_mody_en <=0;
            o_mody_add <=0;
            o_mody_data <=0;
            o_mody_screen <=0;      
        end               
        endcase
    end
    
    assign o_send_en = ((cstate >= SET_TXB0SIDH) && ( cstate <= SEND_FRAME)) ? 1'b1 :1'b0;//解决使能过长的问题，避免误触发，SET_TXB0SIDH->SEND_FRAME使能拉高
    assign o_sendbyte_ok = change_addr_en & i_send_reg_ok;  
endmodule

//-------------------------------------------------------------------
//vJTAG_interface模块承担JTAG状态机的进一步解码工作，格式非常固定（P264）
//-------------------------------------------------------------------
//对虚拟JTAG接口只需添加3个动作就可以完成逻辑设计与JTAG接口的交互：
//（1）指令译码，即针对ir_in获知当前应当做的动作，相当于case选择；
//（2）根据sdr的动作和ir_in决定如何将tdi数据移位存储到临时寄存器中，并在udr有效时，将临时寄存器的内容保存为永久内容；
//（3）根据ir指令决定tdo的输出，如果没有指令（全0状态），必须将tdi回环到tdo.
//--------------------------------------------------------------------
//将vJTAG_interface和FPGA连接后，就能通过TCL窗口操作FPGA，观察LED灯的状态，并写入对应值。
//--------------------------------------------------------------------
module vJTAG_interface(
    input   tck,                    //所有虚拟JTAG的共享时钟.
    input   tdi,                    //串接输入数据.
    input   aclr,                   //异步清零.
    input   ir_in,                  //需要指定带宽，该信号通常全为0.
    input   v_sdr,                  //Indicates that v-jtag is in Shift_DR state.
    input   udr,                    //Indicates that v-jtag is in Updata_DR state.

    output  reg [3:0]   LEDs,
    output  reg         tdo
);

    //Safegurad in case bad IR ia sent through JTAG
    reg         DR0_bypass_reg;     
    reg [3:0]   DR1;

    wire    select_DR0 = !ir_in;        //ir指令寄存器为0，表示不对寄存器操作
    wire    select_DR1 =  ir_in;        //ir指令寄存器为1，表示对DR1的状态进行读取或配置

    always@(posedge tck or posedge aclr) begin
        if(aclr)begin 
            DR0_bypass_reg <= 1'b0;
            DR1 <= 4'b0000_000;
        end
        else begin 
            DR0_bypass_reg <= tdi;
            if(v_sdr)
                if(select_DR1)
                    DR1 <= {tdi,DR1[3:1]};
        end
    end

    //Maintain the TDO Continuity. 标准设置，即如果无指令，应当将tdi回传到tdo
    always@(*)begin 
        if(select_DR1)
            tdo <= DR1[0];
        else
            tdo <= DR0_bypass_reg;
    end

    //当udr信号有效时，表示数据已经完全移位好，可以更新要求写入的寄存器
    always@(udr)begin
        LEDs <= DR1;
    end

endmodule
`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Module Name: axi_wr_channel_top
// Description: 
// Revision 0.01 - File Created
//////////////////////////////////////////////////////////////////////////////////

module axi_wr_channel_top #(
	parameter   USER_WR_DATA_WIDTH = 16,				// 用户数据位宽
	parameter   AXI_DATA_WIDTH     = 128,				// 转换到的AXI数据位宽
	parameter   AXI_ADDR_WIDTH     = 32,				// AXI写地址宽度
	parameter   WR_BURST_LENGTH    = 4096				// 最大写突发长度,以字节为单位
	)(
	input 	wire								i_user_wr_clk,			// 用户写时钟
	input 	wire								i_rst_n,				// 复位
	input 	wire								i_axi_clk,				// AXI时钟
	
	input 	wire								i_ddr_init_done,		// DDR3初始化完成信号
	input   wire                             	i_user_wr_mode,            // 模式选择,设置0为v1版本功能,设置1为v2版本功能

	/*--------------------------用户端信号-----------------------------*/
	input 	wire	[USER_WR_DATA_WIDTH-1:0]	iv_user_wr_data,		// 用户数据
	input 	wire								i_user_wr_en,			// 数据同步有效信号
	input 	wire	[AXI_ADDR_WIDTH-1:0]		iv_user_wr_base_addr,	// 用户写数据的基地址
	input  	wire	[AXI_ADDR_WIDTH-1:0]		iv_user_wr_end_addr,	//用户写数据的结束地址

	// v2模式下添加的用户接口信号
    input   wire   	[AXI_ADDR_WIDTH-1:0]        iv_user_wr_addr,        // 写地址
    input   wire   	[11:0]                      iv_user_wr_length,      // 写长度,地址偏移量,最大为4096
    input   wire                              	i_user_wr_last,         // 用户端最后一个数据标志

	//-------------------------AXI从机交互信号-----------------------//
	/*-------------------------写通道写地址--------------------------*/ 
	output	wire								o_m_axi_awvalid,	// 有效信号，表明此通道的地址控制信号有效
	input	wire								i_m_axi_awready,	// 表明“从”已经准备好,可以接收地址和对应的控制信号
	output	wire	[AXI_DATA_WIDTH-1:0]		ov_m_axi_awaddr,	// 写地址，给出一次写突发传输的写地址
	output	wire	[3:0]						ov_m_axi_awid,		// 写地址 ID，用来标志一组写信号
	output	wire	[7:0]						ov_m_axi_awlen,		// 突发长度，给出突发传输的次数
	output	wire	[1:0]						ov_m_axi_awburst,	// 突发类型
	output	wire	[2:0]						ov_m_axi_awsize,	// 突发大小，给出每次突发传输的字节数
	output	wire	[2:0]						ov_m_axi_awprot,	// 保护类型，表明一次传输的特权级及安全等级
	output	wire	[3:0]						ov_m_axi_awqos,		// 质量服务 QoS
	output	wire								o_m_axi_awlock,		// 总线锁信号，可提供操作的原子性
	output	wire	[3:0]						ov_m_axi_awcache,	// 内存类型，表明一次传输是怎样通过系统的

	/*-------------------------写通道写数据--------------------------*/ 
	output	wire								o_m_axi_wvalid,		// 写有效，表明此次写有效
	input	wire								i_m_axi_wready,		// 表明从机可以接收写数据
	output	wire	[AXI_DATA_WIDTH-1:0]		ov_m_axi_wdata,		// 写数据
	output	wire	[AXI_DATA_WIDTH/8-1:0]		ov_m_axi_wstrb,		// 写数据有效的字节线，用来表明哪8bits数据是有效的
	output	wire								o_m_axi_wlast,		// 表明此次传输是最后一个突发传输

	/*-------------------------写通道响应通道-------------------------*/ 
	input	wire 	[3:0]						iv_m_axi_bid,		// 写响应 IDTAG
	input	wire 	[1:0]						iv_m_axi_bresp,		// 写响应，表明写传输的状态
	input	wire 								i_m_axi_bvalid,		// 写响应有效
	output 	wire								o_m_axi_bready		// 表明主机能够接收写响应
);

/*-------------------------模块连线--------------------------*/ 
	
	// wr_ctrl
	wire 							wr_req_en;
	wire 	[7:0]					wr_burst_length;
	wire 	[AXI_ADDR_WIDTH-1:0]	wr_data_addr;
	wire 	[AXI_DATA_WIDTH-1:0]	wr_user_data;
	wire 							wr_data_valid;
	wire 							wr_data_last;

	// 调试信号
	wire 							err_4k_border;
	wire 							err_user_addr;

	// wr_buffer地址
	wire 							axi_aw_req_en;
	wire 							axi_aw_ready;
	wire 	[7:0]					axi_aw_burst_len;
	wire 	[AXI_ADDR_WIDTH-1:0]	axi_aw_addr;
	
	// wr_buffer数据
	wire 	[AXI_DATA_WIDTH-1:0]	axi_w_data;
	wire 							axi_w_valid;
	wire 							axi_w_ready;
	wire 							axi_w_last;
	wire 							wr_data_fifo_err;
	wire 							wr_cmd_fifo_err;

/*-------------------------模块例化--------------------------*/ 

	wr_ctrl #(
		.USER_WR_DATA_WIDTH (USER_WR_DATA_WIDTH),
		.AXI_DATA_WIDTH     (AXI_DATA_WIDTH),
		.AXI_ADDR_WIDTH     (AXI_ADDR_WIDTH),
		.WR_BURST_LENGTH    (WR_BURST_LENGTH)
	) u0 (
		.i_clk                (i_user_wr_clk),
		.i_rst_n              (i_rst_n),
		.i_ddr_init_done      (i_ddr_init_done),
		.i_user_wr_mode       (i_user_wr_mode),
		.iv_user_wr_data      (iv_user_wr_data),
		.i_user_wr_en         (i_user_wr_en),
		.iv_user_wr_base_addr (iv_user_wr_base_addr),
		.iv_user_wr_end_addr  (iv_user_wr_end_addr),

		.iv_user_wr_addr      (iv_user_wr_addr),
		.iv_user_wr_length    (iv_user_wr_length),
		.i_user_wr_last       (i_user_wr_last),

		.o_wr_req_en          (wr_req_en),
		.ov_wr_burst_length   (wr_burst_length),
		.ov_wr_data_addr      (wr_data_addr),
		.ov_wr_data_out       (wr_user_data),
		.o_wr_data_valid      (wr_data_valid),
		.o_wr_data_last       (wr_data_last),

		.o_err_4k_border      (err_4k_border),
		.o_err_user_addr      (err_user_addr)
	);

	wr_buffer #(
		.AXI_DATA_WIDTH (AXI_DATA_WIDTH),
		.AXI_ADDR_WIDTH (AXI_ADDR_WIDTH)
	) u1 (
		.i_clk               (i_user_wr_clk),
		.i_rst_n             (i_rst_n),
		.i_axi_clk           (i_axi_clk),

		.i_wr_req_en         (wr_req_en),
		.iv_wr_burst_length  (wr_burst_length),
		.iv_wr_data_addr     (wr_data_addr),
		.iv_wr_data_in       (wr_user_data),
		.i_wr_data_valid     (wr_data_valid),
		.i_wr_data_last      (wr_data_last),

		.o_axi_aw_req_en     (axi_aw_req_en),
		.i_axi_aw_ready      (axi_aw_ready),
		.ov_axi_aw_burst_len (axi_aw_burst_len),
		.ov_axi_aw_addr      (axi_aw_addr),

		.ov_axi_w_data       (axi_w_data),
		.o_axi_w_valid       (axi_w_valid),
		.i_axi_w_ready       (axi_w_ready),
		.o_axi_w_last        (axi_w_last),
		.o_wr_data_fifo_err  (wr_data_fifo_err),
		.o_wr_cmd_fifo_err   (wr_cmd_fifo_err)
	);

	axi_wr_master #(
		.AXI_DATA_WIDTH  (AXI_DATA_WIDTH),
		.AXI_ADDR_WIDTH  (AXI_ADDR_WIDTH)
	) u2 (
		.i_axi_clk           (i_axi_clk),
		.i_rst_n             (i_rst_n),

		.i_axi_aw_req_en     (axi_aw_req_en),
		.o_axi_aw_ready      (axi_aw_ready),
		.iv_axi_aw_burst_len (axi_aw_burst_len),
		.iv_axi_aw_addr      (axi_aw_addr),

		.iv_axi_w_data       (axi_w_data),
		.i_axi_w_valid       (axi_w_valid),
		.o_axi_w_ready       (axi_w_ready),
		.i_axi_w_last        (axi_w_last),

		.o_m_axi_awvalid     (o_m_axi_awvalid),
		.i_m_axi_awready     (i_m_axi_awready),
		.ov_m_axi_awaddr     (ov_m_axi_awaddr),
		.ov_m_axi_awid       (ov_m_axi_awid),
		.ov_m_axi_awlen      (ov_m_axi_awlen),
		.ov_m_axi_awburst    (ov_m_axi_awburst),
		.ov_m_axi_awsize     (ov_m_axi_awsize),
		.ov_m_axi_awprot     (ov_m_axi_awprot),
		.ov_m_axi_awqos      (ov_m_axi_awqos),
		.o_m_axi_awlock      (o_m_axi_awlock),
		.ov_m_axi_awcache    (ov_m_axi_awcache),
		.ov_m_axi_wdata      (ov_m_axi_wdata),
		.ov_m_axi_wstrb      (ov_m_axi_wstrb),
		.o_m_axi_wlast       (o_m_axi_wlast),
		.o_m_axi_wvalid      (o_m_axi_wvalid),
		.i_m_axi_wready      (i_m_axi_wready),

		.iv_m_axi_bid        (iv_m_axi_bid),
		.iv_m_axi_bresp      (iv_m_axi_bresp),
		.i_m_axi_bvalid      (i_m_axi_bvalid),
		.o_m_axi_bready      (o_m_axi_bready)
	);
endmodule

N = 12;
n = 0:0.1:N;
h_n = (1-cos(n.*pi))/(n.*pi);
w1 = 1/2*(1+cos(n*pi/N));
w2 = 0.56 + 0.44 * cos(n*pi/N);
hold on


plot(w1*h_n);
plot(w2*h_n);
legend('w1','w2');
`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date: 2024/08/15 23:56:07
// Design Name: 
// Module Name: ethernet_top
// Description: 
// Revision 0.01 - File Created
//////////////////////////////////////////////////////////////////////////////////

module ethernet_top(
	input wire			i_clk_50M,
	input wire			i_rst_n,
	// Reference clk for delay
	// input wire			i_delay_refclk,		// 200MHz
	// signals from PHY
	input wire			i_phy_rgmii_rx_clk,
	input wire			i_phy_rgmii_rx_ctl,
	// Data comes by two parts in both posedge and negedge.
	input wire	[3:0]	iv_phy_rgmii_rx_data,

	output wire			o_phy_rgmii_tx_clk,
	output wire			o_phy_rgmii_tx_ctl,
	// Data outputs by only posedge,so its width turns to be 8 from 4.
	output wire	[7:0]	ov_phy_rgmii_tx_data,

	output wire			o_phy_reset_n	
);

/*-----------------------------------------*\
				信号定义
\*-----------------------------------------*/

	// 参数定义
	parameter   LOCAL_MAC_ADDR  = 48'hFF_FF_FF_FF_FF_FF;
	parameter   TARGET_MAC_ADDR = 48'hFF_FF_FF_FF_FF_FF;
	parameter   LOCAL_IP_ADDR   = 32'h0;
	parameter   TARGET_IP_ADDR  = 32'h0;
	parameter   LOCAL_PORT      = 16'h0;
	parameter   TARGET_PORT     = 16'h0;

	// 时钟和复位
	wire			clk_125M;
	wire			clk_200M;
	wire			sys_rst_n;

	// GMII接口信号
	wire			gmii_rx_vld;
	wire			gmii_rx_error;
	wire	[7:0]	gmii_rx_data;

	wire			phy_rx_clk;
	wire			phy_tx_clk;
	wire			app_rx_clk;
	wire			app_tx_clk;

	wire			gmii_tx_vld;
	wire			gmii_tx_data;

	wire			delay_refclk;

	// 用户端信号连接
	wire 			app_tx_data_vld;
	wire 			app_tx_data_last;
	wire 	[7:0]	app_tx_data	;
	wire 	[15:0]	app_tx_length;
	wire			app_tx_ready;

	wire 			app_rx_data_vld;
	wire 			app_rx_data_last;
	wire 	[7:0]	app_rx_data;
	wire 	[15:0]	app_rx_length;

/*-----------------------------------------*\
				上板回环实验
\*-----------------------------------------*/

	// 进行回环实验,只需要将用户端的输入和输出连接即可构成闭环.
	// 需要注意的就是添加一定的时延,可以通过IP核将数据进行一定的d打拍时延.
	// 已经调用过的IP核可以将数据延迟最多64拍(输入63);延迟的数据为8位宽,为了省事可以先这样凑着用.
	
	// 将数据同步信号核last标志延时60个clk
	data_delay20 top_delay1 (
  		.A(6'd60),      									// input wire [5 : 0] A
  		.D({app_rx_data_vld,app_rx_data_last,6'b0}),      	// input wire [7 : 0] D
  		.CLK(clk_200M),  									// input wire CLK
  		.Q({app_tx_data_vld,app_tx_data_last,6'b0})      	// output wire [7 : 0] Q
	);

	// 将数据延时60个clk
	data_delay20 top_delay2 (
  		.A(6'd60),     	 		// input wire [5 : 0] A
  		.D(app_rx_data),      	// input wire [7 : 0] D
  		.CLK(clk_200M),  		// input wire CLK
  		.Q(app_tx_data)      	// output wire [7 : 0] Q
	);

	// 将数据长度延时60拍
	shift_ram_w16xd64 length_delay_60 (
  		.A(6'd60),      		// input wire [5 : 0] A
  		.D(app_rx_length),      // input wire [15 : 0] D
  		.CLK(clk_200M),  		// input wire CLK
  		.Q(app_tx_length)      	// output wire [15 : 0] Q
	);
	
/*-----------------------------------------*\
				assign
\*-----------------------------------------*/
	
	assign  delay_refclk  = clk_200M;		// Set reference clock 200MHz.
	assign  phy_tx_clk    = clk_125M;		// Set phy transmit clock 125MHz.
	// PHY reset_n
	assign  o_phy_reset_n = i_rst_n;

	// 回环实验:用户时钟可以自由设定,这里都设置为200M
	assign  app_rx_clk    = clk_200M;
	assign  app_tx_clk    = clk_200M;

/*-----------------------------------------*\
					CLK
\*-----------------------------------------*/
	
	// Generate Clk and System Reset(low Valid).
	clk_rst_sync U_clk_rst_sync
	(
		.i_clk_50M   (i_clk_50M),
		.o_clk_125M  (clk_125M),
		.o_clk_200M  (clk_200M),
		.o_sys_rst_n (sys_rst_n)
	);

/*-----------------------------------------*\
				RGMII接口信号
\*-----------------------------------------*/
	
	rgmii_interface U_rgmii_interface(
		// .i_rst_n              (i_rst_n),

		.i_delay_refclk       (delay_refclk),

		// FPGA和PHY芯片之间的数据都是双沿传输，而内部又是单沿处理，所以接收发送都要进行转换
		// 外部输入进来的时钟、控制信号和数据，为双沿传输
		.i_phy_rgmii_rx_clk   (i_phy_rgmii_rx_clk),
		.i_phy_rgmii_rx_ctl   (i_phy_rgmii_rx_ctl),
		.iv_phy_rgmii_rx_data (iv_phy_rgmii_rx_data),
		// 通过原语将双沿数据转换为单沿数据然后交给FPGA内部进行处理
		.o_gmii_rx_clk        (phy_rx_clk),
		.o_gmii_rx_vld        (gmii_rx_vld),
		.o_gmii_rx_error      (gmii_rx_error),
		.ov_gmii_rx_data      (gmii_rx_data),

		// --------------- 内部处理后再发送出去 ----------------------//
		// 内部传输来的单沿数据、时钟和有效信号
		.i_gmii_tx_clk        (phy_tx_clk),
		.i_gmii_tx_vld        (gmii_tx_vld),
		.iv_gmii_tx_data      (gmii_tx_data),
		// 通过原语将单沿数据转化为双沿数据发送出去
		.o_phy_rgmii_tx_clk   (o_phy_rgmii_tx_clk),
		.o_phy_rgmii_tx_ctl   (o_phy_rgmii_tx_ctl),
		.ov_phy_rgmii_tx_data (ov_phy_rgmii_tx_data)
	);

	// UDP协议栈顶层例化
	udp_protocal_stack #(
		.LOCAL_MAC_ADDR  (LOCAL_MAC_ADDR),
		.TARGET_MAC_ADDR (TARGET_MAC_ADDR),
		.LOCAL_IP_ADDR   (LOCAL_IP_ADDR),
		.TARGET_IP_ADDR  (TARGET_IP_ADDR),
		.LOCAL_PORT      (LOCAL_PORT),
		.TARGET_PORT     (TARGET_PORT)
	) U_udp_protocal_stack (
		.i_phy_rx_clk       (phy_rx_clk),
		.i_phy_tx_clk       (phy_tx_clk),
		.i_rst_n            (sys_rst_n),

		.i_gmii_rx_data_vld (gmii_rx_vld),
		.iv_gmii_rx_data    (gmii_rx_data),
		.o_gmii_tx_data_vld (gmii_tx_vld),
		.ov_gmii_tx_data    (gmii_tx_data),

		.i_app_rx_clk       (app_rx_clk),
		.o_app_rx_data_vld  (app_rx_data_vld),
		.o_app_rx_data_last (app_rx_data_last),
		.ov_app_rx_data     (app_rx_data),
		.ov_app_rx_length   (app_rx_length),

		.i_app_tx_clk       (app_tx_clk),
		.i_app_tx_data_vld  (app_tx_data_vld),
		.i_app_tx_data_last (app_tx_data_last),
		.iv_app_tx_data     (app_tx_data),
		.iv_app_tx_length   (app_tx_length),

		.o_app_tx_ready     (app_tx_ready)
	);

endmodule
onbreak {quit -force}
onerror {quit -force}

asim -t 1ps +access +r +m+fifo_data_w9xd4096 -L xil_defaultlib -L xpm -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.fifo_data_w9xd4096 xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {fifo_data_w9xd4096.udo}

run -all

endsim

quit -force

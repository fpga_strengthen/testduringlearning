//+FHEADER //////////////////////////////////////////////////////////////////////  
//  版权所有者：                                                                   
//  Copyright 2014. All Rights Reserved.                                           
//  保密级别：                                                                     
//---------------------------------------------------------------------------      
//  文件名 :  can_spi.v                                                          
//  原始作者：丁云冰                                                               
//  联系方式：                                                                     
// --------------------------------------------------------------------------      
// 版本升级历史： 对来自mcp2515的信号打两拍，把以前的四分频做成八分频，换了sck
//                的产生方式，不能用取反，最好是直接赋值，防止相位误翻。                                                                
//                                                                                
// 版本生效日期：  作者    版本                                                    
// 1.0 2014-01-13  丁云冰  第1版                                                   
// 2.0 2015-11-13  郭庆阳  第2版                                                                                 
//---------------------------------------------------------------------------      
// 关键字 :                                                                        
//---------------------------------------------------------------------------      
// 模块功能: spi protocol between fpga and mcp2515                                                   
//           byte_clk上升沿比数据早1个27兆时钟，需打1拍再用数据                                                                     
// --------------------------------------------------------------------------      
// 参数文件名：                                                                    
// --------------------------------------------------------------------------      
// 重复使用问题：                                                                  
// 复位策略:负跳变沿异步复位信号 i_rst_n                                           
// 时钟域 : i_clk_27M                                                              
// 关键时序: N/A                                                                   
// 是否需要实例引用别的模块 : No                                                   
// 可综合否 : Yes                                                                  
//-FHEADER //////////////////////////////////////////////////////////////////////  
module can_spi
        (
        clk_27M,
        rst_n,
        mcp_simo,       //FPGA向mcp2515的bit输入
        mcp_somi,       //mcp2515向FPGA的bit输出
        mcp_sclk,       //给mcp2515用来采数据的时钟
        o_cs,           //spi的片选使能
        spi_data,       //FPGA输出的数据
        spi_tx_en,      //电平信号，向本模块发送数据的使能
        spi_rx_en,      //电平信号，从本模块接收数据的使能
        send_ok,        //脉冲信号，发送一个字节成功 
        somi,           //送给FPGA的串并转化后的数据
        byte_clk        //脉冲信号，字节有效
        );
    input clk_27M;
    input rst_n;
    input mcp_somi;        
    input[7:0] spi_data;   
    input spi_tx_en;       
    input spi_rx_en;        
    
    output mcp_simo;       
    output mcp_sclk;       
    output o_cs;                 
    output send_ok;        
    output[7:0] somi;      
    output byte_clk;       
////////////////////////////////////////   
    reg[2:0] cnt;          //count flag 
    reg mcp_sclk;          //sclk produce 4 divided
    wire o_cs;
    reg[2:0] cnt_idle;
    reg simo;
    reg send_ok;
    reg mcp_simo;
    reg[7:0] somi;
    reg byte_clk;
////////////////////////////////////////
    reg mcp_somi_temp1;
    reg mcp_somi_temp2; 
    
    reg spi_rx_en_temp1;   
    reg[2:0] cnt_temp1;   
    reg[2:0] cnt_idle_temp1;        
////////////////////////////////////////打两拍的处理
    always@(posedge clk_27M  or negedge rst_n)
    begin
        if(!rst_n)begin
            mcp_somi_temp1 <= 1'b0;
            mcp_somi_temp2 <= 1'b0;
            spi_rx_en_temp1 <= 1'b0;
            cnt_temp1 <= 3'd0;
            cnt_idle_temp1 <= 3'd0; 
        end  
        else begin
            mcp_somi_temp1 <= mcp_somi;
            mcp_somi_temp2 <= mcp_somi_temp1;
            spi_rx_en_temp1 <= spi_rx_en;
            cnt_temp1 <= cnt;            
            cnt_idle_temp1 <= cnt_idle; 
        end
    end
          
////////////////////////////////////////
//count flag
///////////////////////////////////////
    always@(posedge clk_27M or negedge rst_n)
    begin
        if(!rst_n)
            cnt<=3'd0;
        else if(spi_tx_en || spi_rx_en) 
            cnt<=cnt+3'd1;
        else 
            cnt <= 0;
    end
////////////////////////////////////////
//sclk produce 8 divided      
////////////////////////////////////////  
    always@(posedge clk_27M or negedge rst_n)
    begin
        if(!rst_n)
            mcp_sclk<=1'd1;
        else if(cnt == 3'd3) //在cnt为1和3时翻转
            mcp_sclk <= 1'b0;
        else if(cnt == 3'd7)
            mcp_sclk <= 1'b1;
        else
            mcp_sclk <= mcp_sclk;
    end
////////////////////////////////////////
//cs
////////////////////////////////////////
    
    assign o_cs = ~(spi_tx_en || spi_rx_en);

////////////////////////////////////////
//data number count
////////////////////////////////////////
    always@(posedge clk_27M or negedge rst_n)
    begin
        if(!rst_n) 
            cnt_idle<=3'd0;
        else if(cnt == 3'd7 )
            cnt_idle<=cnt_idle+3'd1; //waste LUE
        else if( ~(spi_tx_en || spi_rx_en)) 
            cnt_idle<=3'd0;
    end
////////////////////////////////////////
//simo
////////////////////////////////////////
  
    always@(spi_tx_en or cnt_idle or spi_data)
    begin
        if(spi_tx_en) begin
            case(cnt_idle)
            3'd0:simo = spi_data[7];
            3'd1:simo = spi_data[6];
            3'd2:simo = spi_data[5];
            3'd3:simo = spi_data[4];
            3'd4:simo = spi_data[3];
            3'd5:simo = spi_data[2];
            3'd6:simo = spi_data[1];
            3'd7:simo = spi_data[0];
            default: simo = 0;
            endcase
        end
        else
            simo = 1'd0;
    end
     
    always@(posedge clk_27M or negedge rst_n)
    begin
        if(!rst_n) 
            mcp_simo<=1'd1;
        else if(cnt == 3'd3 && spi_tx_en)
            mcp_simo<=simo;
        else if(!spi_tx_en)
            mcp_simo<=1'd1;
    end
////////////////////////////////////////
//8bit send ok
////////////////////////////////////////
    always@(posedge clk_27M  or negedge rst_n)
    begin
        if(!rst_n)
            send_ok<=1'd0;
        else if(cnt_idle == 3'd7 && cnt == 3'd6 && spi_tx_en )
            send_ok<=1'd1;
        else 
            send_ok <=1'd0;
    end
////////////////////////////////////////
//somi
//////////////////////////////////////// 
    always@(posedge clk_27M  or negedge rst_n)
    begin
        if(!rst_n) 
            somi <=0;
        else if(cnt_temp1 == 3'd7 && spi_rx_en_temp1) 
            somi <= {somi[6:0],mcp_somi_temp2};
        else if(!spi_rx_en_temp1)
            somi<=0;
    end
///////////////////////////////////////// 
    always@(posedge clk_27M  or negedge rst_n)
    begin
        if(!rst_n) 
            byte_clk<=0;
        else if(cnt_temp1 == 3'd6 && cnt_idle_temp1 == 3'd7 && spi_rx_en_temp1 )
            byte_clk<=1'd1;
        else 
            byte_clk <=1'd0;
    end
   
endmodule
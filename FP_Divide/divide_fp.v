//------------------------------------------
//基于有符号数的除法函数的实现
//采用辗转相减的办法实现除法和求余计算
//------------------------------------------

module divide_fp();
    
    assign  {quot,mod} = div(param1,param2);

    function [a_width+b_width-1:0]  div;
        
        input   [a_width-1:0]   a;
        input   [b_width-1:0]   b;

        reg     [b_width:0]     sum;                //width = b_width + 1
        reg     [a_width-1:0]   dividend;
        reg     [b_width:0]     rem_adjust;
        reg     [b_width:0]     temp_b;

        reg     [b_width-1:0]   rem;

        integer i;

        begin 
            sum = {b_width{1'b0}};                      //sum初始化
            dividend = a;                               //被除数
            sum[0] = a[a_width-1];
            dividend = dividend << 1'b1;
            temp_b = ~b + 1'b1;
            sum = sum + temp_b;
            dividend[0] = ~sum[b_width];

            for(i = 0;i < a_width - 1'b1; i = i + 1'b1)
            begin 
                if(sum[b_width])
                    temp_b = b;
                else
                    temp_b = ~b + 1'b1;

                sum = sum << 1'b1;
                sum[0] = dividend[a_width-1];
                dividend = dividend << 1'b1;
                sum = sum + temp_b;
                dividend[0] = ~sum[b_width];
            end

            rem_adjust = sum[b_width] ? sum + b : sum;
            rem = rem_adjust[b_width-1:0];
            div = {dividend,rem};
        end

    endfunction

endmodule
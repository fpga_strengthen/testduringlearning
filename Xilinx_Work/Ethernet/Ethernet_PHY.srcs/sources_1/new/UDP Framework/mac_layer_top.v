`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date: 2024/08/16 15:44:12
// Module Name: mac_layer_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: This module is the top of MAC layer.
//////////////////////////////////////////////////////////////////////////////////


module mac_layer_top#(
	parameter   LOCAL_MAC_ADDR  = 48'hFF_FF_FF_FF_FF_FF,     // local MAC address
	parameter   TARGET_MAC_ADDR = 48'hFF_FF_FF_FF_FF_FF,
	parameter   CRC_CHECK_EN    = 1'b1                       // CRC check enable
	)(
	
	//------------------------ 时钟 -----------------------------//
	input	wire		i_app_tx_clk,			// 用户发送时钟
	input	wire		i_app_rx_clk,			// 用户接收时钟
	input	wire		i_phy_tx_clk,			// PHY发送时钟
	input	wire		i_phy_rx_clk,			// PHY接收时钟

	//------------------------ 复位 -----------------------------//
	input	wire		i_app_rx_rst_n,			// 用户发送端端复位信号
	input	wire		i_app_tx_rst_n,			// 用户接收端端复位信号
	input	wire		i_phy_tx_rst_n,			// PHY发送端复位信号
	input	wire		i_phy_rx_rst_n,			// PHY接收端复位信号

	/*---------------- rgmii_receive 模块交互信号--------------------*/
	input wire			i_gmii_rx_data_vld,		// 数据有效信号
	input wire	[7:0]	iv_gmii_rx_data,		// MAC数据包数据(单沿)

	/*---------------- mac_to_arp_ip 模块交互信号--------------------*/
	output wire			o_mac_rx_data_vld,		// 数据有效信号
	output wire			o_mac_rx_data_last,		// MAC层数据的最后一个数据的标志
	output wire	[7:0]	ov_mac_rx_data,			// 从MAC数据包解析出MAC层数据
	output wire	[15:0]	ov_mac_rx_frame_type,	// 帧类型

	/*---------------- rgmii_send 模块交互信号--------------------*/
	output wire			o_gmii_tx_data_vld,		// 数据有效信号
	output wire	[7:0]	ov_gmii_tx_data,		// MAC数据包数据(单沿)

	/*------------------- ip_send 模块交互信号 -------------------*/
	input wire			i_mac_tx_data_vld,		// 数据有效信号
	input wire			i_mac_tx_data_last,		// MAC层数据的最后一个数据的标志
	input wire	[7:0]	iv_mac_tx_data,			// 从MAC数据包解析出MAC层数据
	input wire	[15:0]	iv_mac_tx_frame_type,	// 帧类型
	input wire	[15:0]	iv_mac_tx_length,		// 接收数据长度

	input wire			i_rd_arp_list_mac_vld,	// 数据有效信号
	input wire	[47:0]	iv_rd_arp_list_mac		// MAC地址
);

/*-----------------------------------------------*\
					模块连线
\*-----------------------------------------------*/

	wire	[31:0]	rx_CRC_data		;				// 收到的CRC
	wire			rx_CRC_din_vld	;
	wire	[7:0]	rx_CRC_din		;
	wire			rx_CRC_done		;

	wire	[31:0]	tx_CRC_data		;				// 收到的CRC
	wire			tx_CRC_din_vld	;
	wire	[7:0]	tx_CRC_din		;
	wire			tx_CRC_done		;
	
/*-----------------------------------------------*\
					模块例化
\*-----------------------------------------------*/
	
	//------------------------- MAC层接收部分 ----------------------------//
	// MAC 接收
	mac_receive #(
		.LOCAL_MAC_ADDR(LOCAL_MAC_ADDR),		// local MAC address
		.CRC_CHECK_EN(CRC_CHECK_EN)				// CRC check enable
	) 
	u1_1 (
		.i_clk 					(i_app_rx_clk),					// 用户端接收时钟
		.i_phy_rx_clk			(i_phy_rx_clk),					// phy芯片提供的时钟

		.i_phy_rx_rst_n			(i_phy_rx_rst_n),				// phy接收端复位
		.i_rst_n 				(i_app_rx_rst_n), 				// 用户端复位信号

		/*---------------- rgmii_receive 模块交互信号--------------------*/
		.i_gmii_rx_data_vld		(i_gmii_rx_data_vld	),			// 数据有效信号
		.iv_gmii_rx_data		(iv_gmii_rx_data	),			// MAC数据包数据(单沿)

		/*---------------- mac_to_arp_ip 模块交互信号--------------------*/
		.o_mac_rx_data_vld		(o_mac_rx_data_vld	),			// 数据有效信号
		.o_mac_rx_data_last		(o_mac_rx_data_last	),			// MAC层数据的最后一个数据的标志
		.ov_mac_rx_data			(ov_mac_rx_data		),			// 从MAC数据包解析出MAC层数据
		.ov_mac_rx_frame_type	(ov_mac_rx_frame_type),			// 帧类型

		/*---------------- mac_to_arp_ip 模块交互信号--------------------*/
		.iv_rx_CRC_data			(rx_CRC_data		),			// 收到的CRC数据
		.o_rx_CRC_din_vld		(rx_CRC_din_vld		),			// CRC校验使能信号
		.ov_rx_CRC_din			(rx_CRC_din			),			// 一个字节的CRC
		.o_rx_CRC_done			(rx_CRC_done		)			// 一帧数据的CRC结束
	);

	// CRC校验
	crc32_d8 rx_crc32_d8(
		.i_clk         (i_phy_rx_clk),
		.i_rst_n       (i_phy_rx_rst_n),
		.iv_crc_din    (rx_CRC_din),
		.i_crc_din_vld (rx_CRC_din_vld),
		.i_crc_done    (rx_CRC_done),

		.ov_crc_dout   (rx_CRC_data)
	);

	//------------------------- MAC层发送部分 ----------------------------//
	// MAC层发送
	mac_send #(
		.TARGET_MAC_ADDR(TARGET_MAC_ADDR),
		.LOCAL_MAC_ADDR	(LOCAL_MAC_ADDR)
	) u1_2 (
		.i_clk                (i_app_tx_clk			),
		.i_rst_n              (i_app_tx_rst_n		),

		.i_phy_tx_clk         (i_phy_tx_clk			),
		.i_phy_rst_n          (i_phy_tx_rst_n		),

		.o_gmii_tx_data_vld   (o_gmii_tx_data_vld	),
		.ov_gmii_tx_data      (ov_gmii_tx_data		),

		.i_mac_tx_data_vld    (i_mac_tx_data_vld	),
		.i_mac_tx_data_last   (i_mac_tx_data_last	),
		.iv_mac_tx_data       (iv_mac_tx_data		),
		.iv_mac_tx_frame_type (iv_mac_tx_frame_type	),
		.iv_mac_tx_length     (iv_mac_tx_length		),
		
		.iv_tx_CRC_data       (tx_CRC_data			),
		.o_tx_CRC_din_vld     (tx_CRC_din_vld		),
		.ov_tx_CRC_din        (tx_CRC_din			),
		.o_tx_CRC_done        (tx_CRC_done			),

		.i_rd_arp_list_mac_vld(i_rd_arp_list_mac_vld),	// 数据有效信号
		.iv_rd_arp_list_mac	  (iv_rd_arp_list_mac)		// MAC地址
	);

	// CRC校验
	crc32_d8 tx_crc32_d8(
		.i_clk         (i_phy_tx_clk	),
		.i_rst_n       (i_phy_tx_rst_n	),
		.iv_crc_din    (tx_CRC_din		),
		.i_crc_din_vld (tx_CRC_din_vld	),
		.i_crc_done    (tx_CRC_done		),
		
		.ov_crc_dout   (tx_CRC_data		)
	);

endmodule

module decode_top(
    input   wire            i_clk_108M,
    input   wire            i_clk_27M_en,
    input   wire            i_rst_n,
    input   wire    [11:0]  iv_ADC_data,

    output  wire            o_demod_result,
    output  wire            o_framing_start
);

    wire    demod_result_sck;
    wire    demod_en;

    Sync_top U_Sync_top(
        .i_clk_108M         (i_clk_108M),
        .i_clk_27M_en       (i_clk_27M_en),
        .i_rst_n            (i_rst_n),
        .iv_fsk_data        (iv_ADC_data),
        .i_demod_result     (o_demod_result),
        .i_demod_result_sck (demod_result_sck),

        .o_demod_en         (demod_en),
        .o_framing_start    (o_framing_start)
    );

    Demodulation U_Demodulation(
        .i_clk_108M         (i_clk_108M),
        .i_clk_27M_en       (i_clk_27M_en),
        .i_rst_n            (i_rst_n),

        .iv_ADC_data        (iv_ADC_data),
        .i_demod_en         (demod_en),

        .o_demod_result     (o_demod_result),
        .o_demod_result_sck (demod_result_sck)
    );

endmodule
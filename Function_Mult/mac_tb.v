`timescale 1ns/1ps

module mac_tb();

//--------------reg & params & wire define------------------//
    reg     clk;
    reg     rst_n;

    parameter   N_INST = 4;         //定义要计算的乘数的位宽

    parameter   DLY = 20;           //一个时钟周期的持续时间
    parameter   LOOP_TIMES = 1;

    reg     [N_INST-1:0]    opa;
    reg     [N_INST-1:0]    opb;

    wire    [2*N_INST-1:0]  result;

//----------------------clk & rst generate---------------------//
    
    //时钟、复位和初始值赋值形为描述
    initial begin
        clk = 1'b1;
        rst_n <= 1'b0;
        opa  <= 0;
        opb  <= 0;
        #DLY
        rst_n <= 1'b1;
    end

    always #(DLY/2) clk = ~clk;          //时钟，50M

//------------------------input generate-----------------------//

    //行为描述
    initial begin 
        #(2*DLY);
        repeat(LOOP_TIMES) begin
            opa <= {$random} % 16;          //模拟产生4位输入,范围0~15
            opb <= {$random} % 16;
            $display("opa=%0d,  opb=%0d",opa,opb);
            repeat(4) @(posedge clk);
        end
    end

//---------------------module instantiation--------------------//

    //结构化描述
    mac #(
        .N(N_INST)
    )
    mac_inst(
        .clk    (clk),
        .rst_n  (rst_n),
        .opa    (opa),
        .opb    (opb),

        .out    (result)
    );



endmodule
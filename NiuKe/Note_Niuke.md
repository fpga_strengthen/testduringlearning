# 1.串并转换

题目链接：

[数据串并转换电路]: https://www.nowcoder.com/practice/6134dc3c8d0741d08eb522542913583d?tpId=302&amp;tags=&amp;title=&amp;difficulty=0&amp;judgeStatus=0&amp;rp=0&amp;sourceUrl=%2Fexam%2Foj

具体的要求可以参考下边的文件头。

主要要求在于：当接收到6个比特之后，就要在下一个时钟周期对数据进行输出。另外要注意先收到的比特**最终**要放在最低位。

> **方法一**

该方法虽然做出了通过的结果，但是输出部分会产生锁存器，这是应当避免的。下面逐步分析解决的过程。

```verilog
//--------------------------------------------------------------------------------------------
//初始代码，有问题待修改
//Module Descrtption: 
//实现串并转换电路，输入端输入单bit数据，每当本模块接收到6个输入数据后，输出端输出拼接后的6bit数据
//本模块输入端与上游采用valid-ready双向握手机制，输出端与下游采用valid-only握手机制。数据拼接时先接收到的数据放到data_b的低位。
//valid_a用来指示数据输入data_a的有效性，valid_b用来指示数据输出data_b的有效性；
//ready_a用来指示本模块是否准备好接收上游数据，本模块中一直拉高
module s_to_p(
	input 				clk 		,   
	input 				rst_n		,
	input				valid_a		,
	input	 			data_a		,
 
 	output	reg 		ready_a		,
 	output	reg 		valid_b		,
	output  reg  [5:0] 	data_b
);

    //输出并行数据的字长
    parameter   P_LENGTH = 3'd6;

    //ready_a持续拉高
    always@(posedge clk or negedge rst_n)
    begin : ready_a_output
        if(rst_n == 1'b0)
            ready_a <= 1'b0;
        else
            ready_a <= 1'b1;
    end

    //采用移位寄存器存储串行比特
    reg [P_LENGTH-1:0]  b_temp;
    reg [2:0]           cnt_bit;

    always@(posedge clk or negedge rst_n)
    begin : data_b_temp
        if(rst_n == 1'b0)
            b_temp <= {P_LENGTH{1'b0}};
        else if(valid_a == 1'b1)        //注意先收到的数据最终要放在低位
            b_temp <= {data_a,b_temp[5:1]};
        else
            b_temp <= b_temp; 
    end

    //control the counter of input bit
    always@(posedge clk or negedge rst_n)
    begin : bit_a_cnt
        if(rst_n == 1'b0)
            cnt_bit <= 3'd0;
        else if(cnt_bit == P_LENGTH - 1'b1)
            cnt_bit <= 3'd0;
        else if(valid_a == 1'b1)
            cnt_bit <= cnt_bit + 1'b1;
        else
            cnt_bit <= cnt_bit;
    end

    always@(posedge clk or negedge rst_n)begin 
        if(rst_n == 1'b0)begin
            data_b  <= 6'b0;
            valid_b <= 1'b0;
        end
        else if(cnt_bit == P_LENGTH - 1'b1)begin
            data_b  <= b_temp;
            valid_b <= 1'b1;
        end
        else begin
            data_b  <= data_b;
            valid_b <= 1'b0;
        end
    end

endmodule
```

通过仿真可以看到输出的是5个比特而非6个比特的串并转换结果(应当为`6'b111111`)，但如改成在计数器为6时输出就会晚2拍的结果。

<img src="./pics/1.png" style="zoom:80%;float:left" />

如下图，如果修改为`cnt_bit==6`时进行输出，由于`b_temp`和`data_b`均是时序逻辑，所以串并转换的结果在7个周期后才输出。

<img src="./pics/2.png" style="zoom:80%;" />

因此考虑将输出改为组合逻辑，代码更改为：

```verilog
	//control the counter of input bit
    always@(posedge clk or negedge rst_n)
    begin : bit_a_cnt
        if(rst_n == 1'b0)
            cnt_bit <= 3'd0;
        else if(cnt_bit == P_LENGTH)
            cnt_bit <= 3'd0;
        else if(valid_a == 1'b1)
            cnt_bit <= cnt_bit + 1'b1;
        else
            cnt_bit <= cnt_bit;
    end

    //set output
    always@(*)begin 
        if(rst_n == 1'b0)
            data_b <= 6'b0;
        else if(cnt_bit == P_LENGTH)
            data_b <= b_temp;
        else
            data_b <= data_b;
    end

	assign  valid_b = (cnt_bit == P_LENGTH) ? 1'b1 : 1'b0;
```

如图，该部分的修改后第一次串并转换正确，但由于计数器清零的问题导致后面会产生累积错误，第2次串并转换在7个比特后才输出。

<img src="./pics/3.png" style="zoom:80%;" />

分析之后，**将计数器计满逻辑的清零改为置1**，避免清零带来的滞后时间累积效应，代码更改和仿真结果如下。

```verilog
    //control the counter of input bit
    always@(posedge clk or negedge rst_n)
    begin : bit_a_cnt
        if(rst_n == 1'b0)
            cnt_bit <= 3'd0;
        else if(cnt_bit == P_LENGTH)
            cnt_bit <= 3'd1;			//更改为置1
        //else if(cnt_bit == P_LENGTH)
        //    cnt_bit <= 3'd0;
        else if(valid_a == 1'b1)
            cnt_bit <= cnt_bit + 1'b1;
        else
            cnt_bit <= cnt_bit;
    end
```

<img src="./pics/4.png" style="zoom:80%;float:left" />

如上图可以看到此时的串并转换结果是正确的。因此整个修改后的代码如下所示，但是该输出部分会产生锁存器，所以并不太好，而且有凑出结果的感觉。

```verilog
//最终代码
module s_to_p(
	input 				clk 		,   
	input 				rst_n		,
	input				valid_a		,
	input	 			data_a		,
 
 	output	reg 		ready_a		,
 	output	wire		valid_b		,
	output  reg  [5:0] 	data_b
);
    //输出并行数据的字长
    parameter   P_LENGTH = 3'd6;
    
    //ready_a持续拉高
    always@(posedge clk or negedge rst_n)
    begin : ready_a_output
        if(rst_n == 1'b0)
            ready_a <= 1'b0;
        else
            ready_a <= 1'b1;
    end

    //采用移位寄存器存储串行比特
    reg [P_LENGTH-1:0]  b_temp;
    reg [2:0]           cnt_bit;

    always@(posedge clk or negedge rst_n)
    begin : data_b_temp
        if(rst_n == 1'b0)
            b_temp <= {P_LENGTH{1'b0}};
        else if(valid_a == 1'b1)        //注意先收到的数据最终要放在低位
            b_temp <= {data_a,b_temp[5:1]};
        else
            b_temp <= b_temp; 
    end

    //control the counter of input bit
    always@(posedge clk or negedge rst_n)
    begin : bit_a_cnt
        if(rst_n == 1'b0)
            cnt_bit <= 3'd0;
        else if(cnt_bit == P_LENGTH)
            cnt_bit <= 3'd1;
        else if(valid_a == 1'b1)
            cnt_bit <= cnt_bit + 1'b1;
        else
            cnt_bit <= cnt_bit;
    end

    //set output
    always@(*)begin 		
        if(rst_n == 1'b0)
            data_b <= 6'b0;
        else if(cnt_bit == P_LENGTH)
            data_b <= b_temp;
        else
            data_b <= data_b;	//here maybe create Latch
    end

    assign  valid_b = (cnt_bit == P_LENGTH) ? 1'b1 : 1'b0;

endmodule
```



> 方法二

关键步骤在于输出`data_b`时还进行的这一次移位拼接操作，避免了在时序上更晚一拍的问题。非常巧妙。另外将模块参数化，可以用于其他字长的接收。

```verilog
module s_to_p
#(
    parameter	P_LENGTH = 3'd6			//输出并行数据的字长，只需修改该参数就可以进行其他字长的串并转换
)(
	input 				clk 		,   
	input 				rst_n		,
	input				valid_a		,
	input	 			data_a		,
 
 	output	reg 		ready_a		,
 	output	reg 		valid_b		,
    output  reg  [P_LENGTH-1:0] 	data_b
);
    //计数器字长
    localparam	CNT_LENGTH = $clog2(P_LENGTH);
    
    //ready_a持续拉高
    always@(posedge clk or negedge rst_n)
    begin : ready_a_output
        if(rst_n == 1'b0)
            ready_a <= 1'b0;
        else
            ready_a <= 1'b1;
    end

    //采用移位寄存器存储串行比特
    reg [P_LENGTH	-1:0]  	b_temp;
    reg [CNT_LENGTH -1:0]   cnt_bit;

    always@(posedge clk or negedge rst_n)
    begin : data_b_temp
        if(rst_n == 1'b0)
            b_temp <= {P_LENGTH{1'b0}};
        else if(valid_a == 1'b1)        //注意先收到的数据最终要放在低位
            b_temp <= {data_a,b_temp[5:1]};
        else
            b_temp <= b_temp; 
    end

    //control the counter of input bit
    always@(posedge clk or negedge rst_n)
    begin : bit_a_cnt
        if(rst_n == 1'b0)
            cnt_bit <= {CNT_LENGTH{1'b0}};
        else if(cnt_bit == P_LENGTH - 1'b1)
            cnt_bit <= {CNT_LENGTH{1'b0}};
        else if(valid_a == 1'b1)
            cnt_bit <= cnt_bit + 1'b1;
        else
            cnt_bit <= cnt_bit;
    end

    always@(posedge clk or negedge rst_n)begin
        if(rst_n == 1'b0)begin
            data_b  <= {P_LENGTH{1'b0}};
            valid_b <= 1'b0;
        end
        else if(cnt_bit == P_LENGTH - 1'b1)begin
            data_b  <= {data_a,b_temp[P_LENGTH-1:1]};     //这一步很重要，避免了计数器的延时一拍的问题
            valid_b <= 1'b1;
        end
        else begin
            data_b  <= data_b;
            valid_b <= 1'b0;
        end
    end
endmodule
```

如下是仿真结果，计数器也是按照实际比特数计数6个比特即可。

<img src="./pics/5.png" style="zoom:80%;" />






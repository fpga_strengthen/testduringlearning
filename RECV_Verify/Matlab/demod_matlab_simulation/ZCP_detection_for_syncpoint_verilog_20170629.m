close all;
clear;
clc;
% --------------------------------------------------------
%将ZCP_detection_for_syncpoint.m转变为verilog相似的语句
%对相位折线进行29点最小二乘法，求斜率曲线，根据其过零点修正中心频率，进而求同步点

%%%%%%%%%%%%%%%%%%%产生相位连续的FSK调制信号进行ADC采样后作为输入%%%%%%%%5%%%%%%%%%%%%
sample_rate = 27.095;%采样速率，单位MHz
data_bit = 100;
src_data([1:2:data_bit-1])=1;
src_data([2:2:data_bit])=0;
% src_data = [1 1 1 1 1 1 1 1 1 1 0 1 0 1 0 1 0 1 1 1 1 1 0 0 1 1 1 1 0 1 1 1 1 1 0 0 0 0 0 1 1 0 0 1 1 1 1 1 1 1 1 0 0 0 0 0 1 1 1 1 1 1 0 0 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 0 0   ];
% src_data = ones(1,1000);
BPS_data = 0.56448 ;  %数据传输速率，单位MHz
freq_0 = 4.234 - 0.282  ; %
freq_1 = 4.234 + 0.282  ; %比特0和比特1的频点，单位MHz
i_data_ADC_0  = FSK_mod(src_data,freq_0,freq_1,sample_rate,BPS_data);
% 频偏变化
freq_0 = 4.234 - 0.282  + 0.175 ;
freq_1 = 4.234 + 0.282  + 0.175 ; %比特0和比特1的频点，单位MHz
i_data_ADC_1  = FSK_mod(src_data,freq_0,freq_1,sample_rate,BPS_data);
% 频偏变化
freq_0 = 4.234 - 0.282  -0.139 ;
freq_1 = 4.234 + 0.282  -0.139 ; %比特0和比特1的频点，单位MHz
i_data_ADC_2  = FSK_mod(src_data,freq_0,freq_1,sample_rate,BPS_data);

i_data_ADC = [i_data_ADC_0,i_data_ADC_1,i_data_ADC_2]-2048;
% i_data_ADC = i_data_ADC_2 - 2048;

% %%%%%%%%%%%%%%%%%%%%带内干扰%%%%%%%%%%%%%%%%%%%%%
% src_data_noise = [1,1,1,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
% f1_noise = 4.234 - 0.01 ;
% f0_noise = 4.234 + 0.15 ;
% FSK_data_noise  = FSK_mod(src_data_noise,f0_noise,f1_noise,sample_rate,BPS_data)./10;
% FSK_data_noise = FSK_data_noise(1:end);
% i_data_ADC (475:475+length(FSK_data_noise)-1) = i_data_ADC (475:475+length(FSK_data_noise)-1)+ FSK_data_noise;
% i_data_ADC (2000:2000+length(FSK_data_noise)-1) = i_data_ADC (2000:2000+length(FSK_data_noise)-1)+ FSK_data_noise;

% %%%%%%%%%%%%%%%%%%%%加入数字滤波器产生相移%%%%%%%%%%%%%%%%%%%%%
% Hd = LP_8M_phase10degree;
% i_data_ADC = filter(Hd,i_data_ADC);

% %%%%%%%%%%%%%%%%%%%%%%读取ADC采样报文作为输入16进制%%%%%%%%%%%%%%%%%%%%%
% ADC_data_hex = textread('C:\Users\lenovo\Desktop\data\新版接收板程序数据\1018单通道接收板新版程序数据\i_adc_data.txt','%s');
% i_data_ADC_unsigned   = hex2dec(ADC_data_hex)';
% i_data_ADC = i_data_ADC_unsigned - 2048;

% %%%%%%%%%%%%%%%%%%%%%%读取ADC采样报文作为输入10进制%%%%%%%%%%%%%%%%%%%%%
% ADC_data_hex = textread('D:\lab_project\BTM\测试\单通道测试文档\1123加高斯噪声测试报文\27M\九阶滤波器加高斯-2dBm临界_adc.txt','%s');
% i_data_ADC =  char(ADC_data_hex) ;
% i_data_ADC = str2num(i_data_ADC)'- 2048;

% %%%%%%%%%%%%%%%%%%%%%%读取ADC采样报文作为输入%%%%%%%%%%%%%%%%%%%%%
% ADC_data_14bit_bin = textread('D:\new_convert.txt','%u');
% [i_data_ADC_unsigned] = transform8to12(ADC_data_14bit_bin);
% i_data_ADC = i_data_ADC_unsigned - 2048;
% i_data_output = dec2hex(i_data_ADC_unsigned');

% %% %%%%%%%%%%%%%%%%%%%%%读取同步跟踪模块signaltap数据画图%%%%%%%%%%%%%%%%%%%%%
% det_sample = hex2dec(textread('C:\Users\lenovo\Desktop\data\新版接收板程序数据\0705新版程序数据\1\1_det_sample.txt','%s'))';
% cnt_48 = hex2dec(textread('C:\Users\lenovo\Desktop\data\新版接收板程序数据\0705新版程序数据\1\1_cnt_48.txt','%s'))';
% cnt_sync_point = str2num(char(textread('C:\Users\lenovo\Desktop\data\新版接收板程序数据\0705新版程序数据\1\1_cnt_sync_point.txt','%s')))';
% o_sync_point = str2num(char(textread('C:\Users\lenovo\Desktop\data\新版接收板程序数据\0705新版程序数据\1\1_o_sync_point.txt','%s')))';
% 
% figure(100),
% plot(det_sample),
% hold on,
% plot(cnt_48,'r'),
% plot(cnt_sync_point,'g'),
% plot(o_sync_point.*50,'c'),
% legend('det_sample','cnt_48','cnt_sync_point','o_sync_point');

%% 转换为对应于verilog的算法
% i_data_ADC = FixPointNum(i_data_ADC, 12, 0)-2048;%字长12bit，精度0位小数  (13000:16300)
%导出数据
% i_data_ADC = fix(i_data_ADC);
% i_data_ADC   = dec2hex(i_data_ADC);

ADC_data = textread('C:\Users\lenovo\Desktop\4M_1.txt','%s');
i_data_ADC =  char(ADC_data) ;
i_data_ADC = str2num(i_data_ADC)';

%% 窗函数功率谱
sample_rate = 27.095e6;
nfft = length(i_data_ADC);
window = ones(1,length(i_data_ADC))
[Pxx1,f1] = periodogram(i_data_ADC,window,nfft,sample_rate); %直接法求功率谱 h_n(num_windows+1,:)
Pxx_windows  = 10*log10(Pxx1);   %num_windows+1,:
figure(4),
plot(f1,Pxx_windows);
title('ADC数据功率谱');

%% ------------------计算同步点-------------------------------
% Hilbert变换 并将数据对齐
[im_datain0,data_add_5,data_H5,im_datain_solve] = hilbert_trans(i_data_ADC);
Length_data = length(im_datain0);
im_datain = im_datain0(11:Length_data-1);
i_data_ADC = i_data_ADC(6:Length_data-6);

figure(1),
subplot(2,1,1);
plot(i_data_ADC);
hold on,
plot(im_datain,'r');
legend('实部','虚部');

%% ------------------幅值检测算法------------------
im_abs = abs(im_datain);
re_abs = abs(i_data_ADC);
amplitude_after = max(re_abs,im_abs) + min(re_abs,im_abs)./2;   %化简后
amplitude_before = (re_abs.^2 + im_abs.^2).^(1/2);              %化简前
%取平均
amplitude_average = (amplitude_after([1:end-3]) + amplitude_after([2:end-2]) + amplitude_after([3:end-1]) + amplitude_after([4:end]))./4;

figure(101),
plot(amplitude_after);
hold on,
plot(amplitude_before);
plot(i_data_ADC,'r');
plot(im_datain,'g');
plot(amplitude_average,'k');
legend('幅值简化后','幅值简化前','ADC数据','虚部','求平均');


%% 对比modelsim仿真结果
% i_data_modelsim = hex2dec(textread('D:\lab_project\BTM\BTM_RCV_3C25\leexiao\BTM_RCV_3C25_defsk_V1.3.6.0\modelsim_simulation_defsk_v1.3.2.4\i_data.txt','%s'))';
% figure(6);
% subplot(2,2,1);
% plot(i_data_ADC_unsigned);
% hold on,
% plot(i_data_modelsim(2:end),'r');
% legend('matlab仿真的ADC采样','modelsim仿真的ADC采样');
% 
% data_add_5_modelsim = textread('D:\lab_project\BTM\BTM_RCV_3C25\leexiao\BTM_RCV_3C25_defsk_V1.3.6.0\modelsim_simulation_defsk_v1.3.2.4\data_add_5.txt','%s');
% data_add_5_modelsim   = hex2decWithSign(data_add_5_modelsim,14)';
% figure(6);
% subplot(2,2,2);
% plot(data_add_5);
% hold on,
% plot(data_add_5_modelsim(4:end),'r');
% legend('matlab仿真data_add_5','modelsim仿真data_add_5');
% 
% data_H5_r_modelsim = textread('D:\lab_project\BTM\BTM_RCV_3C25\leexiao\BTM_RCV_3C25_defsk_V1.3.6.0\modelsim_simulation_defsk_v1.3.2.4\data_H5_r.txt','%s');
% data_H5_r_modelsim   = hex2decWithSign(data_H5_r_modelsim,32)';
% figure(6);
% subplot(2,2,3);
% plot(data_H5);
% hold on,
% plot(data_H5_r_modelsim(5:end),'r');
% legend('matlab仿真data_H5','modelsim仿真data_H5');
% 
% im_datain_solve_modelsim = textread('D:\lab_project\BTM\BTM_RCV_3C25\leexiao\BTM_RCV_3C25_defsk_V1.3.6.0\modelsim_simulation_defsk_v1.3.2.4\im_datain_solve.txt','%s');
% im_datain_solve_modelsim   = hex2decWithSign(im_datain_solve_modelsim,34)';
% figure(6);
% subplot(2,2,4);
% plot(im_datain_solve);
% hold on,
% plot(im_datain_solve_modelsim(6:end),'r');
% legend('matlab仿真未截短的虚部','modelsim仿真未截短的虚部');
% 
% im_datain_modelsim = textread('D:\lab_project\BTM\BTM_RCV_3C25\leexiao\BTM_RCV_3C25_defsk_V1.3.6.0\modelsim_simulation_defsk_v1.3.2.4\im_datain.txt','%s');
% im_datain_modelsim   = hex2decWithSign(im_datain_modelsim,13)';
% figure(7);
% subplot(2,2,1);
% plot(im_datain0);
% hold on,
% plot(im_datain_modelsim(7:end),'r');
% legend('matlab仿真的虚部','modelsim仿真的虚部');


%% 相位归一化后求arctan值
[ angle_tan,tan_quotient ] = Phase_normalization_to_Calculate_phase( i_data_ADC , im_datain );
angle_tan = FixPointNum( angle_tan , 14, 0);

% %% 对比modelsim仿真结果
% angle_tan_modelsim = textread('D:\lab_project\BTM\BTM_RCV_3C25\leexiao\BTM_RCV_3C25_defsk_V1.3.6.0\modelsim_simulation_defsk_v1.3.2.4\angle_tan.txt','%s');
% angle_tan_modelsim   = hex2dec(angle_tan_modelsim)';
% figure(7);
% subplot(2,2,2);
% plot(angle_tan);
% hold on,
% plot(angle_tan_modelsim(20:end),'r');
% legend('matlab仿真的原始相位','modelsim仿真的原始相位');

%% 对中心频率和2pi进行修正
angle_sum = zeros(1,length(angle_tan));
for i=2:length(angle_tan)
    if ( angle_sum(i-1) > 16384 )%相位修正累积量超过+2*8192 上溢处理 恢复初值
        if ( angle_tan(i) + 4096 <= angle_tan(i-1) )%对2pi进行修正
            angle_sum(i) = 6912;%+8192-1280
        else
            angle_sum(i) = - 1280;% -1280;
        end
    elseif( angle_sum(i-1)  < -16384 )%相位修正累积量超过-2*8192 下溢处理 恢复初值
        if ( angle_tan(i) + 4096 <= angle_tan(i-1) )%对2pi进行修正
            angle_sum(i) = 6912;%+8192-1280
        else
            angle_sum(i) = - 1280;%-1280;
        end
    else %相位修正累积量在+-2*8192之间时
        if ( angle_tan(i) + 4096 <= angle_tan(i-1) )%对2pi进行修正
            angle_sum(i) = angle_sum(i-1) + 6912;%+8192-1280
        else
            angle_sum(i) = angle_sum(i-1) - 1280;% - 1280;
        end
    end%if
    
    %取N=14用于之后进行29点最小二乘法
    if( (angle_sum(i-1)>16384) || (angle_sum(i-1)<-16384) ) %溢出时
        angle_fit(i) = angle_tan(i-1);
        angle_fit_delay_14(i)   = angle_fit(i-1) - angle_sum(i-1);
        angle_fit_delay_13(i)   = angle_fit_delay_14(i-1) - angle_sum(i-1);
        angle_fit_delay_12(i)   = angle_fit_delay_13(i-1) - angle_sum(i-1);
        angle_fit_delay_11(i)   = angle_fit_delay_12(i-1) - angle_sum(i-1);
        angle_fit_delay_10(i)   = angle_fit_delay_11(i-1) - angle_sum(i-1);
        angle_fit_delay_9(i)    = angle_fit_delay_10(i-1) - angle_sum(i-1);
        angle_fit_delay_8(i)    = angle_fit_delay_9(i-1) - angle_sum(i-1);
        angle_fit_delay_7(i)    = angle_fit_delay_8(i-1) - angle_sum(i-1);
        angle_fit_delay_6(i)    = angle_fit_delay_7(i-1) - angle_sum(i-1);
        angle_fit_delay_5(i)    = angle_fit_delay_6(i-1) - angle_sum(i-1);
        angle_fit_delay_4(i)    = angle_fit_delay_5(i-1) - angle_sum(i-1);
        angle_fit_delay_3(i)    = angle_fit_delay_4(i-1) - angle_sum(i-1);
        angle_fit_delay_2(i)    = angle_fit_delay_3(i-1) - angle_sum(i-1);
        angle_fit_delay_1(i)    = angle_fit_delay_2(i-1) - angle_sum(i-1);
        angle_fit_delay_0(i)    = angle_fit_delay_1(i-1) - angle_sum(i-1);
        angle_fit_delay_1_n(i)  = angle_fit_delay_0(i-1) - angle_sum(i-1);
        angle_fit_delay_2_n(i)  = angle_fit_delay_1_n(i-1) - angle_sum(i-1);
        angle_fit_delay_3_n(i)  = angle_fit_delay_2_n(i-1) - angle_sum(i-1);
        angle_fit_delay_4_n(i)  = angle_fit_delay_3_n(i-1) - angle_sum(i-1);
        angle_fit_delay_5_n(i)  = angle_fit_delay_4_n(i-1) - angle_sum(i-1);
        angle_fit_delay_6_n(i)  = angle_fit_delay_5_n(i-1) - angle_sum(i-1);
        angle_fit_delay_7_n(i)  = angle_fit_delay_6_n(i-1) - angle_sum(i-1);
        angle_fit_delay_8_n(i)  = angle_fit_delay_7_n(i-1) - angle_sum(i-1);
        angle_fit_delay_9_n(i)  = angle_fit_delay_8_n(i-1) - angle_sum(i-1);
        angle_fit_delay_10_n(i) = angle_fit_delay_9_n(i-1) - angle_sum(i-1);
        angle_fit_delay_11_n(i) = angle_fit_delay_10_n(i-1) - angle_sum(i-1);
        angle_fit_delay_12_n(i) = angle_fit_delay_11_n(i-1) - angle_sum(i-1);
        angle_fit_delay_13_n(i) = angle_fit_delay_12_n(i-1) - angle_sum(i-1);
        angle_fit_delay_14_n(i) = angle_fit_delay_13_n(i-1) - angle_sum(i-1);
    else
        angle_fit(i) = angle_tan(i) + angle_sum(i);
        angle_fit_delay_14(i)   = angle_fit(i-1) ;
        angle_fit_delay_13(i)   = angle_fit_delay_14(i-1);
        angle_fit_delay_12(i)   = angle_fit_delay_13(i-1);
        angle_fit_delay_11(i)   = angle_fit_delay_12(i-1);
        angle_fit_delay_10(i)   = angle_fit_delay_11(i-1);
        angle_fit_delay_9(i)    = angle_fit_delay_10(i-1);
        angle_fit_delay_8(i)    = angle_fit_delay_9(i-1)
        angle_fit_delay_7(i)    = angle_fit_delay_8(i-1);
        angle_fit_delay_6(i)    = angle_fit_delay_7(i-1);
        angle_fit_delay_5(i)    = angle_fit_delay_6(i-1);
        angle_fit_delay_4(i)    = angle_fit_delay_5(i-1);
        angle_fit_delay_3(i)    = angle_fit_delay_4(i-1);
        angle_fit_delay_2(i)    = angle_fit_delay_3(i-1);
        angle_fit_delay_1(i)    = angle_fit_delay_2(i-1);
        angle_fit_delay_0(i)    = angle_fit_delay_1(i-1);
        angle_fit_delay_1_n(i)  = angle_fit_delay_0(i-1);
        angle_fit_delay_2_n(i)  = angle_fit_delay_1_n(i-1);
        angle_fit_delay_3_n(i)  = angle_fit_delay_2_n(i-1);
        angle_fit_delay_4_n(i)  = angle_fit_delay_3_n(i-1);
        angle_fit_delay_5_n(i)  = angle_fit_delay_4_n(i-1);
        angle_fit_delay_6_n(i)  = angle_fit_delay_5_n(i-1);
        angle_fit_delay_7_n(i)  = angle_fit_delay_6_n(i-1);
        angle_fit_delay_8_n(i)  = angle_fit_delay_7_n(i-1);
        angle_fit_delay_9_n(i)  = angle_fit_delay_8_n(i-1);
        angle_fit_delay_10_n(i) = angle_fit_delay_9_n(i-1);
        angle_fit_delay_11_n(i) = angle_fit_delay_10_n(i-1);
        angle_fit_delay_12_n(i) = angle_fit_delay_11_n(i-1);
        angle_fit_delay_13_n(i) = angle_fit_delay_12_n(i-1);
        angle_fit_delay_14_n(i) = angle_fit_delay_13_n(i-1);
    end
end

% 画出相位折线
figure(1),
subplot(2,1,2);
plot(angle_fit);
title('相位折线');
grid on;

% %% 与modelsim仿真结果进行对比
% angle_fit_modelsim = textread('D:\lab_project\BTM\BTM_RCV_3C25\leexiao\BTM_RCV_3C25_defsk_V1.3.6.0\modelsim_simulation_defsk_v1.3.2.4\angle_fit.txt','%s');
% angle_fit_modelsim   = hex2decWithSign(angle_fit_modelsim,17)';
% figure(7);
% subplot(2,2,3);
% plot(angle_fit);
% hold on,
% plot(angle_fit_modelsim(23:4000),'r');
% legend('matlab仿真的相位折线','modelsim仿真的相位折线');

%% 按照最小二乘法公式先进行对应位相减
num_14_reduce_14n = angle_fit_delay_14 - angle_fit_delay_14_n;
num_13_reduce_13n = angle_fit_delay_13 - angle_fit_delay_13_n;
num_12_reduce_12n = angle_fit_delay_12 - angle_fit_delay_12_n;
num_11_reduce_11n = angle_fit_delay_11 - angle_fit_delay_11_n;
num_10_reduce_10n = angle_fit_delay_10 - angle_fit_delay_10_n;
num_9_reduce_9n   = angle_fit_delay_9  - angle_fit_delay_9_n ;
num_8_reduce_8n   = angle_fit_delay_8  - angle_fit_delay_8_n ;
num_7_reduce_7n   = angle_fit_delay_7  - angle_fit_delay_7_n ;
num_6_reduce_6n   = angle_fit_delay_6  - angle_fit_delay_6_n ;
num_5_reduce_5n   = angle_fit_delay_5  - angle_fit_delay_5_n ;
num_4_reduce_4n   = angle_fit_delay_4  - angle_fit_delay_4_n ;
num_3_reduce_3n   = angle_fit_delay_3  - angle_fit_delay_3_n ;
num_2_reduce_2n   = angle_fit_delay_2  - angle_fit_delay_2_n ;
num_1_reduce_1n   = angle_fit_delay_1  - angle_fit_delay_1_n ;

%乘对应常数，用于求拐点斜率，为拐点的横坐标分母
num_14_reduce_14n_x14 = num_14_reduce_14n.*14;
num_13_reduce_13n_x13 = num_13_reduce_13n.*13;
num_12_reduce_12n_x12 = num_12_reduce_12n.*12;
num_11_reduce_11n_x11 = num_11_reduce_11n.*11;
num_10_reduce_10n_x10 = num_10_reduce_10n.*10;
num_9_reduce_9n_x9 = num_9_reduce_9n.*9;
num_8_reduce_8n_x8 = num_8_reduce_8n.*8;
num_7_reduce_7n_x7 = num_7_reduce_7n.*7;
num_6_reduce_6n_x6 = num_6_reduce_6n.*6;
num_5_reduce_5n_x5 = num_5_reduce_5n.*5;
num_4_reduce_4n_x4 = num_4_reduce_4n.*4;
num_3_reduce_3n_x3 = num_3_reduce_3n.*3;
num_2_reduce_2n_x2 = num_2_reduce_2n.*2;
num_1_reduce_1n_x1 = num_1_reduce_1n.*1;


%斜率
slope = num_14_reduce_14n_x14 + num_13_reduce_13n_x13 + num_12_reduce_12n_x12 + num_11_reduce_11n_x11 + num_10_reduce_10n_x10 + num_9_reduce_9n_x9 + num_8_reduce_8n_x8 + num_7_reduce_7n_x7 + num_6_reduce_6n_x6 + num_5_reduce_5n_x5 + num_4_reduce_4n_x4 + num_3_reduce_3n_x3 + num_2_reduce_2n_x2 + num_1_reduce_1n_x1;

%% 斜率修正
slope_reg = zeros(1,51);     %斜率移位寄存器
slope_for_sync = zeros(1,2); %斜率和中心频率偏移处理后寄存，用于判断同步点
slope_right = 1;             %斜率范围正确指示
slope_correction = 0;        %斜率修正量
cnt_sync_point = zeros(1,length(slope)+1);          %计算同步点间距离

%对频率进行限制 给出正确指示
extension = 1.5;
f1_L = 53389/extension;   %具体参数见文档
f1_H = 292726*extension;
f0_L = -268163*extension;
f0_H = -77952/extension;
for ii = 2:length(slope)
    slope_reg = [slope_reg(2:end),slope];%斜率移位寄存器，寄存51个点的斜率值。用于中心频率修正，同步点计算
    
    %% ---------------------中心频率修正--------------------------------------
    % 取平均,求w0，w1
    % 寄存的51个点的斜率值中，第26点用来判断是否过0。第26点过0时，根据斜率寄存器第1到3，第49到51点可以比较准确地估计w0，w1的值
    % 1:8:估计水平斜率；9：38：估计过零点，26点为过零点；39：46估计水平斜率
    slope_average_correction_forward = sum(slope_reg(44:51));
    slope_average_correction_after = sum(slope_reg(1:8));
    mid_freq = fix(( slope_average_correction_after + slope_average_correction_forward )/16);
    
    %对频率进行限制 给出正确指示
    if( ( (slope_average_correction_forward/8<=f1_H)&&(slope_average_correction_forward/8>=f1_L) ) && ( (slope_average_correction_after/8<=f0_H)&&(slope_average_correction_after/8>=f0_L) ) )
        slope_right = 1;
    elseif( ((slope_average_correction_forward/8<=f0_H)&&(slope_average_correction_forward/8>=f0_L)) && ((slope_average_correction_after/8<=f1_H)&&(slope_average_correction_after/8>=f1_L)) )
        slope_right = 1;
    else
        slope_right = 0;
    end %if
    
    % 根据估算的w0和w1的值求中心频率偏移量，用来后续同步点的判决
    if( (slope_reg(25)<=0) && (slope_reg(26)>=0) )  % + -  过零判决
        slope_correction = mid_freq;
    elseif( (slope_reg(25)>=0) && (slope_reg(26)<=0) ) % - +  过零判决
        slope_correction = mid_freq;
    else
        slope_correction = slope_correction;
    end %if
    
    slope_for_sync = [slope_for_sync(2),slope_reg(25)-slope_correction];%将修正后的斜率移入移位寄存器
    
    slope_uncorrect(ii) = slope_reg(25);%绘图用
    slope_corrected(ii) = slope_reg(25)-slope_correction;%绘图用
    %斜率限制图 绘图用
    slope_correction_plot(ii) = slope_correction;
    slope_right_plot(ii) = slope_right;
    slope_average_correction_forward_plot(ii) = slope_average_correction_forward/8;
    slope_average_correction_after_plot(ii) = slope_average_correction_after/8;
    f1_L_plot(ii) = f1_L;
    f1_H_plot(ii) = f1_H;
    f0_L_plot(ii) = f0_L;
    f0_H_plot(ii) = f0_H;
    
    % 判决是否是拐点
    if( (slope_for_sync(1)<=0) && (slope_for_sync(2)>=0) && (slope_right==1) )  % + -  过零判决
        sync_point(ii) = 1;%判决的拐点
    elseif( (slope_for_sync(1)>=0) && (slope_for_sync(2)<=0) && (slope_right==1) ) % - +  过零判决
        sync_point(ii) = 1;%判决的拐点
    else
        sync_point(ii) = 0;
    end %if
    
%     %相邻同步点之间理论间隔48点，实际过零点可能出现抖动，导致出现____|-|__|-|____等情况，此时取第一个上升沿作为同步点输出
%     %计数两个同步点间的点数
%     if( sync_point(ii)==1 )
%         cnt_sync_point(ii+1) = 1;
%     elseif(cnt_sync_point(ii) == 48)
%         cnt_sync_point(ii+1) = 0;
%     elseif(cnt_sync_point(ii) ~=0)
%         cnt_sync_point(ii+1) = cnt_sync_point(ii) + 1;
%     else
%         cnt_sync_point(ii+1) = cnt_sync_point(ii);
%     end
%     
%     %消除错误的同步点
%     if( sync_point(ii) && ((cnt_sync_point(ii)>5) || (cnt_sync_point(ii)==0)) )
%         sync_point_corrected(ii) = 1;
%     else
%         sync_point_corrected(ii) = 0;
%     end
end %for ii

% 画出29点最小二乘法求出的斜率曲线以及拐点
figure(2);
subplot(2,1,1);
plot(slope_uncorrect,'r');
hold on;
plot(slope_corrected,'g');
plot(sync_point*10^5,'b');
legend('未修正的斜率','修正后的斜率','同步点');
title('过零点判决得到的同步结果');
grid on;

%% 与modelsim仿真结果进行对比
% slope_modelsim = textread('D:\lab_project\BTM\BTM_RCV_3C25\leexiao\BTM_RCV_3C25_defsk_V1.3.6.0\modelsim_simulation_defsk_v1.3.2.4\slope.txt','%s');
% slope_modelsim   = hex2decWithSign(slope_modelsim,26)';
% slope_correction_modelsim = textread('D:\lab_project\BTM\BTM_RCV_3C25\leexiao\BTM_RCV_3C25_defsk_V1.3.6.0\modelsim_simulation_defsk_v1.3.2.4\slope_correction.txt','%s');
% slope_correction_modelsim   = hex2decWithSign(slope_correction_modelsim,26)';
% figure(7);
% subplot(2,2,4);
% plot(slope);
% hold on,
% plot(slope_modelsim(28:4000),'r');
% plot(slope_correction_plot);
% plot(slope_correction_modelsim(1:4000),'r');
% legend('matlab仿真的斜率曲线','modelsim仿真的斜率曲线','matlab仿真的斜率修正量','modelsim仿真的斜率修正量');
% 
% slope_delay_26_correction_modelsim = textread('D:\lab_project\BTM\BTM_RCV_3C25\leexiao\BTM_RCV_3C25_defsk_V1.3.6.0\modelsim_simulation_defsk_v1.3.2.4\slope_delay_26_correction.txt','%s');
% slope_delay_26_correction_modelsim   = hex2decWithSign(slope_delay_26_correction_modelsim,27)';
% 
% sync_point_modelsim = str2num(char(textread('D:\lab_project\BTM\BTM_RCV_3C25\leexiao\BTM_RCV_3C25_defsk_V1.3.6.0\modelsim_simulation_defsk_v1.3.2.4\sync_point.txt','%s')))';
% 
% figure(8);
% plot(slope_corrected(8:end));
% hold on,
% plot(slope_delay_26_correction_modelsim(34:end),'r');
% plot(sync_point(9:end).*4.5*10^5,'LineWidth',2);
% plot(sync_point_modelsim(36:4000).*5*10^5,'r','LineWidth',2);
% legend('matlab仿真修正后的斜率曲线','modelsim仿真修正后的斜率曲线','matlab仿真修正后的同步点','modelsim仿真修正后的同步点');

% %test 20171018 单通道接收板自激问题
% figure(100),
% cnt48 = textread('C:\Users\lenovo\Desktop\data\新版接收板程序数据\1018单通道接收板新版程序数据\cnt48.txt','%s');
% cnt48 =  char(cnt48) ;
% cnt48 = str2num(cnt48)';
% plot(cnt48)

%% -----------------------------对比实验室现用算法----------------------------------
%对中心频率和2pi进行修正
angle_fit_lab = Phase_Correction(angle_tan);
%最小二乘法
[sync_p_8,slope_forward_lab,slope_after_lab] =  Least_Square_Method( angle_fit_lab );

figure(3);
subplot(2,2,1);
plot(angle_fit);
legend('29点相位折线');
subplot(2,2,2);
plot(slope_corrected,'r');
hold on;
plot(sync_point*5*10^5,'g');
legend('29点斜率','29点同步点');
subplot(2,2,3);
plot(angle_fit_lab,'r');
legend('8点相位折线');
subplot(2,2,4);
plot(slope_forward_lab,'r');
hold on;
plot(slope_after_lab,'c');
plot(sync_p_8*5*10^4,'g');
% plot(2100.*ones(1,length(sync_point)));%上下限
% plot(-2100.*ones(1,length(sync_point)));
% plot(12700.*ones(1,length(sync_point)));
% plot(-12700.*ones(1,length(sync_point)));
legend('前8点斜率','后8点斜率','8点同步点');

%% ---------------------------------------相干解调-------------------------------------------------
[demodulation_result] = demodulation(i_data_ADC)
demodulation_result = [zeros(1,9),demodulation_result];

%% -----------------------------将同步点导入到同步算法中得到判决时刻----------------------------------
% 29点最小二乘法的判决时刻
[decision_time_29,sync_plot,flag_47] =  SyncFsk( sync_point );

% 观察判决时刻和斜率曲线对应关系
figure(2);
subplot(2,1,2);
plot(decision_time_29(1:end)*1.5,'r');%判决时刻*3*10^9
hold on;
plot(demodulation_result,'LineWidth',2);%相干解调结果
legend('判决时刻','相干解调结果');
title('判决时刻和斜率曲线对应关系');

%% -----------------------------统计同步点的偏差量----------------------------------
%标准的48点
standard_48 = zeros(1,length(sync_point));
standard_48([48:48:length(sync_point)])=1;

figure(5),
plot(flag_47(6:end).*1.75,'r','LineWidth',2);
hold on;
plot(sync_point(7:end).*1.5,'g');
plot([ones(1,24),standard_48(31:end)].*2,'b');
plot(demodulation_result,'k');
legend('cnt_48=47','同步点','标准的48点','解调结果');

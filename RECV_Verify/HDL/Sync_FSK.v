// 同步跟踪模块,用于修正同步点,给出解调使能信号
module Sync_FSK(
    input   wire    i_clk_108M,
    input   wire    i_clk_27M_en,
    input   wire    i_rst_n,
    input   wire    i_sync_point,           // 同步点
    input   wire    i_demod_result,         // 解调结果
    input   wire    i_demod_result_sck,     // 同步有效信号

    output  reg     o_demod_en,             // 解调使能信号
    output  reg     o_framing_start         // 起始组帧标志
);

    // 三级同步窗
    parameter   Sync_Window_Max     = 7'd16;        // 同步窗的最大宽度
    parameter   Length_Window1      = 7'd3;         // 第0级同步窗,中心点和采样周期都不做修正
    parameter   Length_Window2      = 7'd6;         // 第1级同步窗
    parameter   Length_Window3      = 7'd9;         // 第2级同步窗
    parameter   Length_Window4      = 7'd13;        // 第3级同步窗

    parameter   Half_Sample         = 7'd24;        // 一个bit采样周期的一半,一个周期包含48个采样点
    parameter   MAX_CNT_SYNC_POINT  = 4'd8;         // 连续8个正确的同步点表示同步
    parameter   T_Modify            = 6'd48;        // 一个周期48个采样点

    // 48个采样点计数器,控制同步窗的移动
    reg [5:0]   cnt_48;
    // 同步状态机
    reg [1:0]   sync_state;
    parameter   NONE_SYNC   = 2'b00,            // 未同步状态
                HALF_SYNC   = 2'b01,            // 半同步状态
                FULL_SYNC   = 2'b11;            // 完全同步状态

    // 输入数据寄存
    reg     demod_result_r;
    reg     demod_result_sck_r;

    always@(posedge i_clk_108M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)begin 
            demod_result_r     <= 1'b0;
            demod_result_sck_r <= 1'b0;
        end
        else begin 
            demod_result_r     <= i_demod_result;
            demod_result_sck_r <= i_demod_result_sck;
        end
    end

    // 采样时刻的偏差值
    reg     [5:0]   delta_sample;

    always@(posedge i_clk_108M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            delta_sample <= 6'd0;
        else if(i_clk_27M_en == 1'b1)begin 
            if(cnt_48 == Half_Sample)
                delta_sample <= cnt_48;
            else
                delta_sample <= T_Modify - cnt_48;
        end
    end

    // 与采样周期同步的时钟
    reg     clk_out_h;

    always@(posedge i_clk_108M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            clk_out_h <= 1'b0;
        else if(i_clk_27M_en == 1'b1)begin 
            if(cnt_48 == Half_Sample)    // 采样到24点时拉高
                clk_out_h <= 1'b1;
            else if((i_sync_point == 1'b1)&&(delta_sample <= Sync_Window_Max)&&(clk_out_h == 1'b1))
                clk_out_h <= 1'b0;
            else
                clk_out_h <= clk_out_h;
        end
        else
            clk_out_h <= clk_out_h;
    end

    // 同步状态下,解调结果中出现连续10个相同的bit,说明解调结果不正确
    // 根据编码规范,正确报文中最多只有8个连续相同的bit(FF)
    reg [9:0]   demod_result_reg;

    always @(posedge i_clk_108M or negedge i_rst_n) begin
        if(i_rst_n == 1'b0)
            demod_result_reg <= 10'b1;      // 默认置1
        else if(sync_state != FULL_SYNC)
            demod_result_reg <= 10'b1;      // 非完全同步时,置1
        else if(demod_result_sck_r == 1'b1)begin 
            if(demod_result_r == 1'b1)      // 判决结果1,则1入队
                demod_result_reg <= {demod_result_reg[8:0],1'b1};
            else                            // 判决结果0,则0入队
                demod_result_reg <= {demod_result_reg[8:0],1'b0};
        end
        else
            demod_result_reg <= demod_result_reg;
    end

    // 两个连续拐点间超过25个bit,认为这两个拐点只是干扰导致的,判定为没有FSK
    // 一个拐点到来时,delta_sample的值小于同步窗的最大范围,即该拐点落在同步窗内,认为该拐点有效
    reg [4:0]   cnt_25;

    always@(posedge i_clk_108M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_25 <= 5'd0;
        else if(sync_state == NONE_SYNC)
            cnt_25 <= 5'd0;
        else if((delta_sample <= Sync_Window_Max)&&(i_sync_point == 1'b1)&&(clk_out_h == 1'b1))
            cnt_25 <= 5'd0;             // 检测到一个有效的拐点,计数器清零
        else if(cnt_25 == 5'd31)
            cnt_25 <= cnt_25;           // 计数到最大值就保持
        else if(demod_result_sck_r == 1'b1)
            cnt_25 <= cnt_25 + 5'd1;    // 一个判决时刻代表一个bit,用来计数两个有效同步点之间的bit数
        else
            cnt_25 <= cnt_25;
    end

    // 同步状态机控制采样同步窗的偏移
    always@(posedge i_clk_108M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_48 <= 6'd0;
        else if(i_clk_27M_en == 1'b1)begin 
            case(sync_state)
                NONE_SYNC : begin 
                    if(i_sync_point == 1'b1)
                        cnt_48 <= 6'd0;     // 捕捉状态,只要检测到有拐点,将计数器清零,准备开始周期计数
                    else if(cnt_48 == 6'd47)
                        cnt_48 <= 6'd0;
                    else
                        cnt_48 <= cnt_48 + 6'd1;
                end
                HALF_SYNC , FULL_SYNC : begin 
                    if(i_sync_point == 1'b1)begin 
                        if((delta_sample <= Sync_Window_Max)&&(clk_out_h == 1'b1))begin 
                            if(cnt_48 >= T_Modify)  // 落在同步窗外,不修正了
                                cnt_48 <= cnt_48 - T_Modify;
                            else if(cnt_48 >= Half_Sample)begin 
                                if (delta_sample >= Length_Window4)
                                    cnt_48 <= cnt_48 + 6'd14;
                                else if(delta_sample >= Length_Window3)
                                    cnt_48 <= cnt_48 + 6'd10;
                                else if(delta_sample >= Length_Window2)
                                    cnt_48 <= cnt_48 + 6'd7;
                                else if(delta_sample >= Length_Window1)
                                    cnt_48 <= cnt_48 + 6'd4;
                                else
                                    cnt_48 <= 6'd0;
                            end
                            else if(cnt_48 <= Half_Sample)begin 
                                if(delta_sample >= Length_Window4)
                                    cnt_48 <= cnt_48 - 6'd14;
                                else if(delta_sample >= Length_Window3)
                                    cnt_48 <= cnt_48 - 6'd10;
                                else if(delta_sample >= Length_Window2)
                                    cnt_48 <= cnt_48 - 6'd7;
                                else if(delta_sample >= Length_Window1)
                                    cnt_48 <= cnt_48 - 6'd4;
                                else
                                    cnt_48 <= 6'd0;
                            end
                            else
                                cnt_48 <= cnt_48;
                        end
                        else begin          // 同步点落在同步窗之外时
                            if(cnt_48 >= T_Modify)
                                cnt_48 <= cnt_48 - T_Modify;
                            else
                                cnt_48 <= cnt_48 + 1'b1;
                        end
                    end
                    else if(cnt_48 >= T_Modify)
                        cnt_48 <= cnt_48 - T_Modify;
                    else
                        cnt_48 <= cnt_48 + 1'b1;
                end
                default : 
                    cnt_48 <= cnt_48;
            endcase
        end
        else
            cnt_48 <= cnt_48;
    end

    // 同步状态机的跳转计数器
    reg [3:0]   cnt_sync_point;

    always@(posedge i_clk_108M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_sync_point <= 4'd0;
        else if(i_clk_27M_en == 1'b1)begin 
            if((demod_result_reg == 10'd1023)||(demod_result_reg == 10'd0))
                cnt_sync_point <= 4'd0;     // 连续10个相同的bit,说明解调结果错误
            else if(cnt_25 >= 5'd20)
                cnt_sync_point <= 4'd0;     // 同步状态下连续25个bit都没有拐点,则说明没有信号到达,强制失步
            else begin 
                case(sync_state)
                    NONE_SYNC : 
                        if(i_sync_point == 1'b1)
                            cnt_sync_point <= 4'd1;
                        else 
                            cnt_sync_point <= cnt_sync_point;
                    HALF_SYNC : 
                        if(i_sync_point == 1'b1)begin 
                            if((delta_sample <= Sync_Window_Max)&&(clk_out_h == 1'b1))
                                cnt_sync_point <= cnt_sync_point + 1'b1;    // 跳变点落在同步窗内时计数器增加
                            else
                                cnt_sync_point <= cnt_sync_point - 1'b1;    // 跳变点不在同步窗内,同步点计数器减1
                        end
                        else
                            cnt_sync_point <= cnt_sync_point;
                    FULL_SYNC : 
                        if(i_sync_point == 1'b1)begin 
                            if((delta_sample <= Sync_Window_Max)&&(clk_out_h == 1'b1))begin 
                                if(cnt_sync_point >= MAX_CNT_SYNC_POINT)
                                    cnt_sync_point <= cnt_sync_point;       // 同步点达到计数器上限,则不再增加,这样退出同步状态会比较快
                                else
                                    cnt_sync_point <= cnt_sync_point + 1'b1;
                            end
                            else
                                cnt_sync_point <= cnt_sync_point - 1'b1;    //  落在同步窗外,计数器减1
                        end
                        else
                            cnt_sync_point <= cnt_sync_point;
                    default :
                        cnt_sync_point <= cnt_sync_point;
                endcase
            end
        end
        else
            cnt_sync_point <= cnt_sync_point;
    end

    // 同步状态机的跳转
    always@(posedge i_clk_108M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            sync_state <= NONE_SYNC;
        else if(i_clk_27M_en == 1'b1)begin 
            case(sync_state)
                NONE_SYNC : begin 
                    if(cnt_sync_point == 4'd1)
                        sync_state <= HALF_SYNC;
                    else
                        sync_state <= NONE_SYNC;
                end
                HALF_SYNC : begin 
                    if(cnt_sync_point == MAX_CNT_SYNC_POINT)
                        sync_state <= FULL_SYNC;
                    else if(cnt_sync_point == 4'd0)
                        sync_state <= NONE_SYNC;
                    else
                        sync_state <= HALF_SYNC;
                end
                FULL_SYNC : begin 
                    if(cnt_sync_point == 4'd0)
                        sync_state <= NONE_SYNC;
                    else
                        sync_state <= FULL_SYNC;
                end
                default :
                    sync_state <= sync_state;
            endcase
        end
        else
            sync_state <= sync_state;
    end

    // 在每个bit的中间给出判决时刻
    always@(posedge i_clk_108M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            o_demod_en <= 1'b0;
        else if(i_clk_27M_en == 1'b1)begin 
            if((cnt_48 == 6'd24)&&(sync_state != NONE_SYNC))
                o_demod_en <= 1'b1;
            else
                o_demod_en <= 1'b0;
        end
        else
            o_demod_en <= o_demod_en;
    end

    // 给出组帧的起始信号
    always@(posedge i_clk_108M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            o_framing_start <= 1'b0;
        else if(i_clk_27M_en == 1'b1)begin 
            if(sync_state == FULL_SYNC)
                o_framing_start <= 1'b1;
            else
                o_framing_start <= 1'b0;
        end
        else
            o_framing_start <= o_framing_start;
    end

endmodule
`define TIMES 3
module pipeline_none(
    input   wire    [7:0]   i_X,
    input   wire            clk,
    input   wire            start,
    output  reg     [7:0]   Times_NP,
    output  wire            finished
);

    reg [7:0]   times;

    always@(posedge clk)begin 
        if(start)begin 
            Times_NP <= i_X;
            times    <= `TIMES;          //宏定义
        end
        else if(!finished)begin 
            times    <= times - 1;
            Times_NP <= Times_NP + i_X;
        end
        else begin 
            times    <= times;
            Times_NP <= Times_NP;
        end
    end

    assign  finished = (times == 0);

endmodule
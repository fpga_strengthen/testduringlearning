module fir_da_simple_opt
#(
    parameter   BIT_WIDTH  = 16,
    parameter   N_taps     = 24
)(
    input   wire    clk,
    input   wire    clk_en,
    input   wire    rst_n,
    input   wire    signed  [BIT_WIDTH-1:0] filter_in,
    output  reg     signed  [BIT_WIDTH-1:0] filter_out
);
    localparam  DA_WIDTH  = BIT_WIDTH + `log2(N_taps);
    localparam  SUM_WIDTH = BIT_WIDTH + `log2(BIT_WIDTH) + `log2(N_taps);

    reg     signed  [N_taps-1:0]    delay_pipeline  [BIT_WIDTH-1:0];
    wire    signed  [SUM_WIDTH-1:0] sum [BIT_WIDTH-1:0]; 

    assign  sum[0] = 0;

    integer j;

    generate
        genvar  i,k;
        for(i=0;i<BIT_WIDTH;i=i+1)
        begin : BIT_WIDTH_1bit_LUT
            always@(posedge clk)
            begin
                if(rst_n == 1'b0)begin 
                    for(j=0;j<N_taps;j=j+1)
                    begin : bit_matrix_initial
                        delay_pipeline[i][j] <= 1'b0;           //比特矩阵初始化
                    end
                end
                else begin
                    delay_pipeline[i][0] <= filter_in[i];
                    for(j=1;j<N_taps;j=j+1)
                    begin : bit_matrix_buffer
                        delay_pipeline[i][j] <= filter_in[j-1];
                    end
                end
            end

            //关键代码，用于每4位进行一次查表；如改为6输入查找表，只需要将4替换为6即可
            //因为每4位进行一次查表，因此总查表次数变为原来的1/4
            wire    signed  [DA_WIDTH-1:0]  DA_data [N_taps/4-1:0];     //块内声明，用于块内计算

            for(k=0;k<N_taps/4;k=k+1)
            begin : rom_k
                DA_bit_ROM #(DA_WIDTH,k)    U_bit0(.addr(delay_pipeline[i][k*4+3,k*4]),.data(DA_data[k]));
            end

            //将4输入查找表的查表结果进行累加，得到1位N_tap结算结果
            reg signed  [DA_WIDTH-1:0] DA_data_sum  [N_taps/4:0];

            always(*)begin 
                DA_data_sum[0] = DA_data[0];
                for(j=1;j<N_taps/4;j=j+1)
                begin : DA_DATA_SUM
                    DA_data_sum[j] = DA_data_sum[j-1] + DA_data[j];
                end
            end

            //关键代码，将0~B-1位进行累加，此处没有考虑符号位的特殊处理
            assign  sum[i+1] = sum[i] + (DA_data_sum[N_taps/4] <<< i);
        end
    endgenerate

    //输出滤波结果
    always@(posedge clk)begin
        if(rst_n == 1'b0)
            filter_out <= 0;
        else if(clk_en == 1'b1)
            filter_out <= sum[N_taps][SUM_WIDTH - 1 : SUM_WIDTH - BIT_WIDTH];
        else
            filter_out <= filter_out;
    end

endmodule
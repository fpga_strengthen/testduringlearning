`timescale 1ns/1ps

//Description
//产生2FSK信号，两个子载波频率分别为5M（比特0）和10MHz（比特1）
//暂时不考虑包络问题，只考虑产生FSK信号
module DAROM_tb;

    //时钟和采样点相关参数
    parameter   CLK_PERIOD = 4'd10;
    //设置的比特数，设置500个Bit
    parameter   Length_Data_Bit = 500;
    //设定每个bit的采样点数均为50
    parameter   Sample_Per_bit = 50;
    //50M速率分别进行采样
    //如果想增加每个周期的点数让波形变得好看，就要提高采样速率
    //采样点数是50，采样频率是50M，则50M/50 * 5 = 5M，即在采样点为50的情况下5M（比特0）的波形周期数为5；同理10M（比特1）的周期数是10
    parameter   Sample_bit0_Periods = 5, Sample_bit1_Periods = 10;
    //添加高频分量，以验证滤波器算法；采样点数和比特0、1设置一致，都是50点；50M/50 * 25 = 25M
    parameter   Sample_High_Freq = 25;

    logic           clk;                //时钟，100M
    logic           sample_clk;         //采样时钟，50M
    logic           rst_n;              //复位，低有效
    int             i;
    
    //存储初始化bit，设定一个位宽为1、深度为报文长度的存储器,即基带信号
    logic           data_bit    [Length_Data_Bit-1:0];
    logic   [8:0]   cnt_baseband_bit;           //对基带信号的比特进行计数
    logic   [7:0]   cnt_sample_bit;             //每个bit的采样点计数器
    //存储三角函数的浮点数计算结果，存储的数据是浮点数，数据个数为该bit的采样点数
    real            fsk_bit_0   [Sample_Per_bit-1:0];
    real            fsk_bit_1   [Sample_Per_bit-1:0];
    //暂存三角函数计算结果
    real            tmp_fsk;

    //生成的的定点2FSK信号
    logic  signed   [11:0]   fsk_sig_fp;
    //滤波处理后的2FSK信号
    logic  signed   [11:0]   fsk_filtered;

    //时钟初始化
    initial begin
        clk = 1'b1;
        rst_n <= 1'b0;
        #(CLK_PERIOD * 3);
        rst_n <= 1'b1;
    end

    //时钟100M，采样速率设为50M
    always #(CLK_PERIOD/2) clk = ~clk;

    //采样时钟
    initial begin 
        forever begin
            @(posedge clk)  sample_clk = 1;
            @(posedge clk)  sample_clk = 0;
        end
    end

    //初始化基带信号
    initial begin 
        for(i=0;i<Length_Data_Bit;i++)
        begin:baseband_bit_init
            data_bit[i] = {$random} % 2;
        end
    end

    //初始化比特0，1的调制信号，产生标准FSK信号
    //s(t) = sin(2*pi*f*t)
    initial begin:fsk_bit0_modulation
        for(i=0;i<=Sample_Per_bit;i++)begin
            tmp_fsk = $sin(6.283185307179586 * i * Sample_bit0_Periods / Sample_Per_bit) + $sin(6.283185307179586 * i * Sample_High_Freq / Sample_Per_bit);
            fsk_bit_0[i] = tmp_fsk / 2.0;       //除2是为了做归一化，避免量化时造成溢出
            tmp_fsk = $sin(6.283185307179586 * i * Sample_bit1_Periods / Sample_Per_bit) + $sin(6.283185307179586 * i * Sample_High_Freq / Sample_Per_bit);
            fsk_bit_1[i] = tmp_fsk / 2.0;
        end
    end

    //产生FSK信号采样
    //初始化比特0、1的调制信号时就已经计算出了两个载波频率的与采样点对应的幅度表，下来根据输入的基带信号对应取出
    //提前计算的两个表中分别存储着比特0、1各自对应的50个采样点的幅度信息
    initial begin 
        forever begin
            for(cnt_baseband_bit=0;cnt_baseband_bit < Length_Data_Bit;cnt_baseband_bit++)begin 
                //将每个比特对应的所有采样点幅值查表取出
                for(cnt_sample_bit = 0;cnt_sample_bit < Sample_Per_bit;cnt_sample_bit++)begin
                    @(posedge sample_clk)       //在采样时钟有效时才可以执行，否则就会一次性计算完毕
                    if(data_bit[cnt_baseband_bit] == 1'b1)
                        tmp_fsk = fsk_bit_0[cnt_sample_bit];
                    else
                        tmp_fsk = fsk_bit_1[cnt_sample_bit];

                    //将计算的浮点数做定点化处理
                    //注意这里是有符号数，最高位是符号位，如果2^11不减1会导致溢出，从+2047变为-2048
                    fsk_sig_fp = tmp_fsk * (2**11 - 1);
                    //if(fsk_sig_fp < -2048)
                    //    fsk_sig_fp = 0;
                    //if(fsk_sig_fp > 2049)
                    //    fsk_sig_fp = 2048;
                end
            end
        end
    end
    
    DA_Filter #(
        .N_TAPS   (11),
        .BIT_WIDTH(12)
    )U_DA_Filter(
        .i_clk_100M     (clk),                  //时钟，100M
        .i_rst_n        (rst_n),                //复位，低有效
        .sample_clk_50M (sample_clk),           //采样时钟，50M
        .fsk_sig        (fsk_sig_fp),           //FSK信号，有符号数

        .fsk_filtered   (fsk_filtered)          //滤波结果
    );
    
endmodule
# 解码板的同步、解调部分

$$总体思想$$：同步采用相位折线法，通过相位折线的拐点得到同步点，之后再进行同步状态的判断。

整体框图如下：

<img src="./pics/overall_framework.svg" style="zoom:120%;" title="同步部分整体框图设计" />



## 1. Hilbert变换

该变换的主要目的是得到解析信号，从而得到信号的实虚部，之后可以用于计算反正切得到相位。

Hilbert变换可以看成是将原始信号通过一个滤波器，这个系统的冲激响应为$h(t)$，其物理意义如下：

<img src="./pics/hilbert_1.png" style="zoom:80%;" />

由此可以看到复信号才是完整的，而实信号只是在复平面的实轴上的一个投影。

### 1.1 定义

Hilbert变换主要是为了引出解析信号。

令$f(t)$为实函数，则它的希尔伯特变换为：
$$
\hat{f(t)}=f(t)*\dfrac{1}{\pi t}=\dfrac{1}{\pi}\int_{-\infty}^{\infty}\dfrac{f(\tau)}{t-\tau}{\rm{d}\tau}
\tag{1-1}
$$
由于$\dfrac{1}{\pi t}\Longleftrightarrow -j{\rm {sgn}}(f)$，希尔波特变换频域

表示为：
$$
H(f)=-j{\rm{sgn}}(f)=\begin{cases}-j\quad & f>0 \\ j\quad &f<0 \end{cases}
\tag{1-2}
$$
由$\displaystyle{e^{-j\frac{\pi}{2}}}$和$\displaystyle{e^{j\frac{\pi}{2}}}$可以看出希尔伯特变换等效于一个理想相移器：在$f>0$域相移$-\dfrac{\pi}{2}$，在$f<0$域相移$\dfrac{\pi}{2}$.

则复信号$z(t)=f(t)+j\hat{f(t)}$为$f(t)$的解析信号。

$H(f)$的傅里叶反变换的冲激响应为：
$$
h_H(n)=\dfrac{1}{2\pi}\int_{-\pi}^{\pi}H(e^{j\omega})e^{jn\omega}{\rm{d}}\omega
=\dfrac{1}{2\pi}\int_{-\pi}^{0}je^{j\omega}{\rm{d}}\omega+\dfrac{1}{2\pi}\int_{0}^{\pi}(-j)e^{j\omega}{\rm{d}}\omega
=\dfrac{1-\cos(n\pi)}{n\pi}
=\begin{cases}
\dfrac{2}{\pi n}\quad & if\ n\ is\ odd \\
0\quad &if\ n\ is\ even.
\end{cases}
\tag{1-3}
$$


### 1.2 性质

- $f(t)={\rm{Re}} [z(t)]=\dfrac{1}{2}\left[z(t)+z^*(t) \right]$.
- $Z(f)=F(f)+j\left[-j{\rm sgn}(f) \right]F(f)=F(f)[1+{\rm sgn}{(f)}]=\begin{cases}2F(f) &f\geq 0\\ 0 &else \end{cases}$.

解析信号从频域上看仅有正频率部分，解析信号的共轭仅有负频率部分。



### 1.3 加窗和改进

由于$\rm Hilbert$变换的冲激响应是无限长的，在使用中通常采用截断并加窗或镶边，使得既缩短冲激相应长度同时避免吉布斯现象。
$$
h_H(n)=\begin{cases}
w(n)h_H(n)\ &|n|\leq N \\
\quad\quad0\ &|n|>N
\end{cases}
\tag{1-4}
$$


其中加的时窗函数$w(n)$为：
$$
w(n)=
\begin{cases}
w_1(n)=\dfrac{1}{2}\left(1+\cos\dfrac{n\pi}{N} \right) \\
w_2(n)=0.56+0.44\times\cos\dfrac{n\pi}{N}\\
w_3(n)=e^{-\lambda N^2}
\end{cases}
\tag{1-5}
$$
所以，以上公式可被表示为滤波器形式，公式为：
$$
H_A(e^{j\Omega})=1+jH_H(e^{j\Omega}).
\tag{1-6}
$$



### 1.4 离散Hilbert变换

离散$\rm Hilbert$变换可以由$\rm{FIR}$滤波器实现，可以由窗口法来实现。

设滤波器输入为$x(n)$，输出为$y(n)$，则$N-1$阶$\rm{FIR}$滤波器的差分方程为：
$$
y(n)=\sum\limits_{k=0}^{N-1}b_kx(n-k)=b_0x(n)+b_1x(n-1)+\cdots+b_{N-1}x(n-N+1) \tag{1-7}
$$
其中，$b_k$为滤波器系数。对于线性时不变系统，输出可以看作输入与系统冲激响应的卷积：
$$
y(n)=\sum\limits_{k=0}^{N-1}h(k)x(n-k)\tag{1-8}
$$
采用$11$阶的滤波器，系数具有对称性，如下表：

|     -5     |  -4  |     -3     |  -2  |     -1     |  0   |     1     |  2   |     3     |  4   |     5     |
| :--------: | :--: | :--------: | :--: | :--------: | :--: | :-------: | :--: | :-------: | :--: | :-------: |
| $-0.12732$ |  0   | $-0.21221$ |  0   | $-0.63662$ |  0   | $0.63662$ |  0   | $0.21221$ |  0   | $0.12732$ |

系数$h(n)$具有对称性，所以可以简化计算公式：
$$
\begin{align}
y(n)=&\sum\limits_{k=-5}^{5}h(k)x(n-k) \\
=& h(-5)x(n+5)+h(-3)x(n+3)+h(-1)x(n+1)+h(1)x(n-1)+h(3)x(n-3)+h(5)x(n-5) \\
=&\left[x(n-5)-x(n+5) \right]h(5)+\left[x(n-3)-x(n+3) \right]h(3)+\left[x(n-1)+x(n+1) \right]h(1)\\
\Longrightarrow y(0)=&[x(-5)-x(5)]h(5)+[x(-3)-x(3)]h(3)+[x(-1)-x(1)]h(1)
\end{align}
$$
对上式进行`z`变换，可以得到：
$$
\begin{align}
Y(z)&=\left[X(z)z^5-X(z)z^{-5} \right]h(5)+\left[X(z)z^3-X(z)z^{-3} \right]h(3)+\left[X(z)z^1-X(z)z^{-1} \right]h(1)\\
&=X(z)z^5\left[(1-z^{-10})h(5)+(z^{-2}-z^{-8})h(3)+(z^{-4}-z^{-6})h(1) \right]\\
&=X(z)z^5Q(z)\\
where\ Q(z)&=(1-z^{-10})h(5)+(z^{-2}-z^{-8})h(3)+(z^{-4}-z^{-6})h(1)
\end{align}
$$
根据上式可以画出如下框图：

<img src="./pics/hilbert_2.svg" style="zoom:70%" title="希尔伯特变换">

式中$X(z)z^5Q(z)$的$z^5$可以忽略，因为它在时序上相当于只是将序列的计算右移了$5$点，但是序列本身就是滑动窗式的计算，这里即使不做右移也没有影响，计算出的解析信号（该框图计算出虚部）仍然是正确的。由于计算出虚部需要$4$个时钟，因此从滤波器中心点向右偏移$4$个点就是与之对齐的实部数据。



## 2. 相位计算

相位计算采用$10$级流水线的$\rm Cordic$算法实现，属于以面积换速度。

### 2.1 旋转和伪旋转

> 平面坐标系旋转

如下图，是一个矢量$\vec{r_1}=(x_1,y_1)$旋转了$\theta$后到矢量$\vec{r_2}=(x_2,y_2)$，下面推导出$(x_2,y_2)$与$(x_1,y_1)$之间关系。

<img src="./pics/cordic_1.png" style="zoom:70%" title="Cordic矢量图">

设$\vec{r_1}$和横轴的夹角为$\alpha$，圆的半径为$R$，则$\displaystyle{R=\dfrac{x_1}{\cos\alpha}=\dfrac{x_2}{\cos(\alpha+\theta)}}$，$\displaystyle{R=\dfrac{y_1}{\sin\alpha}=\dfrac{y_2}{\sin(\alpha+\theta)}}$.

对以上两式分别处理：
$$
\begin{align}
&\dfrac{x_1}{\cos\alpha}=\dfrac{x_2}{\cos(\alpha+\theta)}\Longrightarrow x_1\cos(\alpha+\theta)=x_2\cos\alpha\\
\rightarrow&x_2=\dfrac{x_1\cos(\alpha+\theta)}{\cos\alpha}= \dfrac{x_1(\cos\alpha\cos\theta-\sin\alpha\sin\theta)}{\cos\alpha}=x_1\cos\theta-x_1\tan\alpha\sin\theta \\
\because &\ \tan\alpha=\dfrac{y_1}{x_1},\qquad\quad\therefore y_1=x_1\tan\alpha \\
\rightarrow & x_2=x_1\cos\theta-y_1\sin\theta
\end{align}
\tag{2-1}
$$
同理可以得到$y_2$和$(x_1,y_1)$之间的关系为：$y_2=y_1\cos\theta-x_1\sin\theta$.

将以上两式进行整理如下：
$$
x_2=x_1\cos\theta-y_1\sin\theta=\cos\theta(x_1-y_1\tan\theta) \\
y_2=y_1\cos\theta-x_1\sin\theta=\cos\theta(y_1-x_1\tan\theta)
\tag{2-2}
$$


> 伪旋转

上面是完全在保持模长不变的条件下将矢量进行旋转前后的坐标变化关系。

如果去掉$\cos\theta$，得到伪旋转公式，但是$\theta$不变，只是$x_2,y_2$的值增加了$\cos^{-1}\theta$倍。由于$\cos^{-1}\theta>1$，所以模长会变大。

这里本身是无法消除掉$\cos\theta$项的，但是去掉后可以化简坐标平面旋转的计算过程。记：
$$
\hat{x}_2=x_1-y_1\tan\theta \\
\hat{y}_2=y_1-x_1\tan\theta
\tag{2-3}
$$
如下图所示，是旋转前后的坐标和伪坐标的变化，以及相应的模长关系。

<img src="./pics/false_zuobiao.png" style="zoom:40%;" />

以上就是平面坐标系旋转的基础内容。



### 2.2 ${Cordic}$方法

为了便于硬件的计算，采用迭代思想，将旋转角度$\theta$分解为多次旋转$\theta_i$实现，并规定$\tan\theta_i=2^{-i}$，这样和正切值的乘法就可以转化为移位操作。

> 角度累加器

引入一个旋转因子$d_i$，用来确定每次旋转的方向是顺/逆时针。第$i$步旋转时，顺时针$d_i=-1$，逆时针$d_i=1$。

前边的式$(2-2)$就可以表示为：
$$
x_{i+1}=x_i-d_i2^{-i}y_i \\
y_{i+1}=y_i+d_i2^{-i}x_i
\tag{2-3}
$$
以$y$为例，向下（顺时针）旋转$y_i$就会减小，所以顺时针时$d_i=-1$，逆时针时$d_i=+1$.

再引入第三个方程，用来在每次迭代中最终累加的旋转角度，表示第$n$次旋转后剩下未旋转的角度，定义为：
$$
z_{i+1}=z_i-d_i\theta_i ,\quad where\ d_i=\pm1.\tag{2-4}
$$


### 2.3 移位-加法算法

> 运算量概述

原始的算法可以简化为使用向量的伪旋转方程式来表示迭代算法。
$$
x_{i+1}=x_i-d_i\cdot2^{-i}y_i \\
y_{i+1}=y_i+d_i\cdot2^{-i}x_i \\
z_{i+1}=z_i-d_i\theta_i
\tag{2-5}
$$
式$(2-5)$每次迭代包含$2$次移位、$1$次查找表和$3$次加法运算：

- $2$次移位：$2^{-i}$用移位实现；
- $1$次查找表：每次迭代会有一个固定角度的累加，这个角度和$2^{-i}$对应，查表实现，满足$\theta_i=\arctan(2^{-i})$；
- $3$次加法：$x,y,z$各包含一次加法。



> 相位计算

迭代的公式如上所示，迭代的最终效果应当是$y\rightarrow 0$，如下图所示：

<img src="./pics/cordic_2.svg" style="zoom:150%;" />

要计算向量与$x$轴的夹角，如图所示，该情况下，就是先顺时针旋转，此时$y$减小。

减小后，如果$y$仍然大于$0$，说明减的不够，继续再减；如果$y<0$，说明减多了，就再加。以此类推，直到$y$的精度满足要求。

在每次旋转时，$x$也会跟随变化，当$y\rightarrow0$时，$x$在横轴上的最终坐标也就是向量的伪模长$x_N$。

因为在讨论时忽略了$\cos^{-1}\theta_i$，所以要还原实际模长，还要考虑伪旋转系数$\displaystyle{K_{n}=\prod_{i=0}^{n}\cos^{-1}\theta_i}$，可写成：
$$
K_{n+1}=K_n\times\cos^{-1}\theta_i=K_n\times\dfrac{1}{\sqrt{1+2^{-2k}}},\quad K_0=\cos^{-1}\theta_0
$$
上式也是存在极限的，因为$\theta_i$是根据查找表事先确定的，所以$\displaystyle{\lim\limits_{n\to\infty}K_n=1.64676,\quad\lim\limits_{n\to\infty}\dfrac{1}{K_n}=0.6072563}$.

当迭代完成后，实际模长$L=K_N\times x_N$，即落在$x$轴上的$x$坐标再乘以对应的模长因子就是实际的模长。

在迭代完成后，$z_i$经过一系列的累加和减小，最终的累积量就是要计算的角度$\theta=\sum\limits_{i=0}^{N}\theta_i$.

对于任意角度$\theta$，都可以通过旋转一系列的小角度$\theta_i$来完成：第一次旋转$45$度，第二次旋转$26.6$度，第三次旋转$14$度，$\cdots$。

这样下来所有的角度累加起来实际是有上限的，其范围为$-99.7^\circ <\theta <99.7^\circ$。

对于该角度范围之外的角度，可以先进行一定的预处理：

- 第二象限：按照原点对称到第四象限，计算出$\theta$后再补偿$+180^\circ$，将其还原到第二象限；
- 第三象限：按照原点对称到第一象限，计算出$\theta$后再补偿$+180^\circ$，将其还原到第三象限。

这部分的$\rm{Matlab}$程序如下：

```matlab
% 基本公式如下：
% x(n+1) = x(n) - d(n)*2^(-n)*y(n)
% y(n+1) = y(n) + d(n)*2^(-n)*x(n)
% z(n+1) = z(n) - d(n)*theta(n) = z(n) - d(n)*arctan(2^(-n))
% 初始化：制作查找表，相位theta_i满足tan(theta_i)=2^(-i)
% 迭代次数
N = 16;
% tan表
tan_table = 2.^(-(0 : N-1));
% 角度查找表
angle_LUT = atan(tan_table);

% (x,y)是初始坐标值
if((x==0) && (y==0))
    radian_out = 0;
    angle_acumulate = 0;
else                % 象限判断，得到相位补偿值
    if(x > 0)       % 一四象限
        phase_shift = 0;
    else            % 二三象限
        phase_shift = pi;
        % 将(x,y)按照原点对称后再进行相位补偿
        x = -x;
        y = -y;
    end
	% 这里进行了定标，和HDL程序保持一致，便于SV仿真时对比中间结果
    x_record(1) = x * 2^16;
    y_record(1) = y * 2^16;
    angle_record(1) = 0;

    % 开始迭代，N=16，在FPGA中采用16级流水线实现
    for k=0 : N-1
        x_temp = x;
        if(y < 0)       % 在x轴下方要逆时针旋转,则d(k)=1
            x = x_temp - y*2^(-k);
            y = y + x_temp*2^(-k);
            angle_acumulate = angle_acumulate - angle_LUT(k+1);
        else            % 在x轴上方要顺时针旋转,则d(k)=-1
            x = x_temp + y * 2^(-k);
            y = y - x_temp * 2^(-k);
            angle_acumulate = angle_acumulate + angle_LUT(k+1);
        end
        % 记录x,y的中间值,数据定标28位,1位符号位
        x_record(k+2) = x * 2^N;
        y_record(k+2) = y * 2^N;

        % 输出弧度
        radian_out = angle_acumulate + phase_shift ;
        angle_record(k+2) = fix((radian_out*180/pi) * 2^N);       % 定标16位
    end
    % 幅值输出
    amplitude_out = x * K;
end
```

在数字逻辑电路中，$\pi$本身是不存在的。所以在解码板的解调程序中，规定$2\pi=8192=360^\circ$，进行这样的量化。

那么迭代时$\theta_i$的查找表也要做出相应的改变，对于以角度表示的相位，首先归一化到弧度域，再进行量化：$\theta_{dp}=\dfrac{{\rm angle}}{360^\circ}\times8192$.

例如$45^\circ$可以量化为：$\theta_{dp}=\dfrac{45^\circ}{360^\circ}\times8192=1024$。如下是查找表中$10$级流水线下其他角度的量化值，满足$\theta_i=\arctan(2^{-i})$：

```verilog
parameter   	angle_0  = 11'd1024,            // i=0,对应45°
                angle_1  = 11'd605,             // i=1
                angle_2  = 11'd319,             // i=2
                angle_3  = 11'd162,             // i=3
                angle_4  = 11'd81,              // i=4
                angle_5  = 11'd41,              // i=5
                angle_6  = 11'd20,              // i=6
                angle_7  = 11'd10,              // i=7
                angle_8  = 11'd5,               // i=8
                angle_9  = 11'd3;               // i=9
```

在$\rm{ModelSim}$仿真中看到$10$级流水线的$\rm{Cordic}$算法已经可以足够精确的计算出相位值了，具体可以参考$\rm HDL$程序和相应的仿真波形。



## 3. 折线拟合

计算的相位$\theta$都是在$0-2\pi$范围内，而$\tan\theta=\tan(\theta+2\pi)$，即如果到了下一个周期，**累计**相位值会有一个$2\pi$模糊度需要处理。

### 3.1 最小二乘法

最小二乘法用于拟合直线，并且是拐点左右两侧的两条相位直线$y=ax+b$，主要是计算出直线的斜率值$a_n$，其中$x$是横坐标，$y$就是相位$\theta$.

> 概述

最小二乘法形如：标函数$\begin{align}=\sum(\ 观测值\ -\ 理论值)^2\end{align}$，观测值就是多组样本，理论值就是假设拟合函数$y$.

这里最小二乘法的目标就是找到$a,b$，使得残差平方和$\begin{align}\sum\limits_{i=1}^{n}(h(x_i)-y_i)^2\end{align}$最小，这样拟合的直线就更接近。

在上式中，$y,x$是已知的，估算出最合适的$a,b$，则$\begin{align}\chi^2(a,b)=\sum\limits_{i=1}^{N}(y_i-ax_i-b)^2 \end{align}$，计算该值最小值时的$a,b$.

根据多元函数微分学，可知就是计算上面这个函数的极值点，分别对$a,b$求偏导然后联立解方程组。

经过计算，得到：$\begin{align}a=(\sum\limits_{i=-N}^{N}i^2)^{-1}\sum\limits_{i=-N}^{N}iy_i\end{align}$，$\begin{align}b=\dfrac{1}{2N+1}\sum\limits_{i=-N}^{N}y_i\end{align}$，其中$i$就是横坐标，$y_i$就是对应的纵坐标相位值$\theta_i$.

> 推导

在FPGA中实现的话，$a，b$中的累加项是比较容易实现的，打拍就可以，主要是前面的系数要想办法处理掉，这里主要考虑$a$，而截距$b$并不重要，因为当折线出现拐点时，意味着斜率$a$发生了正负的变化，所以如果当两点之间的斜率符号相异时，就意味着出现了拐点，即同步点。

如果现在暂时舍弃掉$a$中的系数部分$\begin{align}C=(\sum\limits_{i=-N}^{N}i^2)^{-1}\end{align}$，而记$\begin{align}a_n=\sum\limits_{i=-N}^{N}iy_{n+i}\end{align}$，当$n=0$时，$a_0=a$.

这里的系数$C$实际上可以忽略，因为对于确定的$N$，这个系数就是一个常数，对于斜率而言，关心的是正负跳变，对于同符号的大小并不关心。

<font color="red">但是这里忽略的参数$C$在后边的$4.3$节还要用到，到时候会具体解释原因</font>。

所以对于$\begin{align}a_n=\sum\limits_{i=-N}^{N}iy_{n+i}\end{align}$，对于不同的$n$，就是一个移动求斜率的表达式。
$$
\begin{align}
a_{n+1}&=\sum\limits_{i=-N}^{N}iy_{n+1+i}\overbrace{=}^{j=i+1}\sum\limits_{j=-N+1}^{N+1}(j-1)y_{n+i} \\
&=\sum\limits_{i=-N+1}^{N+1}iy_{n+i}-\sum\limits_{i=-N+1}^{N+1}y_{n+i} \\
&=[\sum\limits_{i=-N}^{N}iy_{n+i}+Ny_{n-N}+(N+1)y_{n+N+1}]-[\sum\limits_{i=-N}^{N}y_{n+i}-y_{n-N}+y_{n+N+1}] \\
&=a_n-\sum\limits_{i=-N}^{N}y_{n+i}+(N+1)y_{n-N}+Ny_{n+N+1}\\
\end{align}
\tag{3-1}
$$
对于单频信号，相位折线理论上为直线，设斜率为$\Delta$，则$y_{n+1}-y_n=(n+1-n)\Delta=\Delta$.

对于上式的$\begin{align}\sum\limits_{i=-N}^{N}y_{n+i}\end{align}$，再进行如下处理，从而和其他项统一，便于化简：
$$
\begin{align}
\sum\limits_{i=-N}^{N}y_{n+i}
&=\sum\limits_{i=-N}^{N}y_{n+i}-\sum\limits_{i=-N}^{N}y_{n-N}+\sum\limits_{i=-N}^{N}y_{n-N}=(2N+1)y_{n-N}+\sum\limits_{i=-N}^{N}(y_{n+i}-y_{n-N})\\
&=(2N+1)y_{n-N}+\sum\limits_{i=-N}^{N}((n+i)-(n-N))\Delta=(2N+1)y_{n-N}+\sum\limits_{i=-N}^{N}(i+N)\Delta\\
&=(2N+1)y_{n-N}+\sum\limits_{i=-N}^{N}i\Delta+\sum\limits_{i=-N}^{N}N\Delta=(2N+1)y_{n-N}+0+N(2N+1)\Delta\\
&=(2N+1)y_{n-N}+N(2N+1)\Delta
\end{align}
\tag{3-2}
$$
将式子`3-2​`代入到`3-1`中，可以得到：
$$
\begin{align}
a_{n+1}
&=a_n-\sum\limits_{i=-N}^{N}y_{n+i}+(N+1)y_{n-N}+Ny_{n+N+1}\\
&=a_n-(2N+1)y_{n-N}-N(2N+1)\Delta+(N+1)y_{n-N}+Ny_{n+N+1}\\
&=a_n-Ny_{n-N}-N(2N+1)\Delta+Ny_{n+N+1}\\
&=a_n+Ny_{n+N+1}-N[y_{n-N}+(2N+1)\Delta]\\
&=a_n+Ny_{n+N+1}-Ny_{n+N+1}=a_n.\\
{\color\red \rm Note:}&{\quad\color\red y_{n+N+1}-y_{n-N}=(n+N+1-(n-N))\Delta=(2N+1)\Delta}\rightarrow y_{n+N+1}=y_{n-N}+(2N+1)\Delta\\
\end{align}
\tag{3-3}
$$
由`3-3`可知，$a_{n+1}=a_n$，因此可知斜率值$a_n$和$n$并没有关系，所以斜率为：$\begin{align}a=\sum\limits_{i=-N}^{N}iy_i\end{align}$，再将该式子展开：
$$
\begin{align}
\sum\limits_{i=-N}^{N}iy_i
&=-Ny_{-N}+(-N+1)y_{-N+1}+\cdots-y_{-1}+{\color\red 0\cdot y_0}+y_1+\cdots+(N-1)y_{N-1}+Ny_N\\
&=N(y_N-y_{-N})+(N-1)(y_{N-1}-y_{-N+1})+\cdots+2(y_{2}-y_{-2})+(y_1-y_{-1})+0\\
&=\sum\limits_{i=1}^{N}i(y_i-y_{-i})=a
\tag{3-4}
\end{align}
$$
式子`3-4`就是计算斜率的式子，可以看到这个结构用HDL实现实际非常方便，有点类似于滤波器的结构，如下图所示：

<img src="./pics/slope_calu.svg" style="zoom:150%;" >

其中相减后再相乘的系数就是式`3-4`中的`i`，通过该结构即可计算出斜率值$a$，从而用于后续的拐点判断。



### 3.2 相位补偿

> 相位说明

在一个周期内，相位应当是线性递增的，最大就是$2\pi$。$N_1=\dfrac{27.095}{3.951}\approx7,\quad N_2=\dfrac{27.095}{4.516}\approx6$.

两个子载波的频率分别为$f_1=3.951{\rm MHz},f_2=4.516\rm MHz$，中心频率$f_c=\dfrac{f_1+f_2}{2}=4.234\rm MHz$，

采样频率$f_s =\rm 27MHz$，则两个子载波的各自两点之间的相位差为：
$$
\Delta\varphi_0=w_0\Delta t=2\pi f_0\Delta t=8192\times4.516M\times\dfrac{1}{27.095M}=1365 \\
\Delta\varphi_1=w_1\Delta t=2\pi f_1\Delta t=8192\times3.951M\times\dfrac{1}{27.095M}=1194
$$
中心频率为$\begin{align} \varphi_c=\dfrac{\Delta\varphi_0+\Delta\varphi_1}{2}=1280 \end{align}$.

> 相位累积

对于一个比特而言，如果要画出相位直线，根据最小二乘法的推导，就是要将相位一直累加，如图所示：

<img src="pics/phase_makeup.svg" style="zoom:90%;" />

图中的蓝色部分是一个载波周期内的采样点数，由于比特速率为$564.48\rm kbps$，采样速率$27.095\rm MHz$，所以一个比特的采样点数为$48$点。而比特$0$的载波频率为$3.951\rm MHz$，载波周期数为$N_1=7$，则每个载波周期的采样点数为$48/7\approx7$，也就是图中蓝色的点。

在比特没有变化时，相位直线应当一直近似是直的，相位会一直累加，正如上图中所示。

但在程序中，每一个点计算的相位范围$\theta\in[0,2\pi]$，而$\tan\theta=\tan(\theta+2\pi)$，所以当从一个周期到下一个周期时，要加上$2\pi$，否则就会出现下图的情况：

<img src="pics/phase_no_makeup.svg" style="zoom:90%;" />

此时看到相位折线就是不连续的，是错的，因此在相位迭代累加时必须要考虑$2\pi$模糊度的处理。

根据以上分析，相位直线（也即相位累积量）的表达式为$\begin{align}y=2\pi\times \dfrac{n}{N}=8192\times\dfrac{n}{N},\ N=6,7 \end{align}$，分别对应$\rm 4.516MHz$和$\rm 3.951MHz$的一个时钟周期内的采样周期数，$n$表示两个拐点之间的第几个采样周期。由上图可以知道，每当一个点和上一个点不在同一周期时，就要加$2\pi$，保证相位直线的线性性质。

> **unwrap()**函数

这里的$2\pi$模糊度的处理在$\rm Matlab$中即`unwrap`函数：`Q = unwrap(P)` 展开向量 `P` 中的弧度相位角。每当连续相位角之间的跳跃大于或等于$\pi$弧度时，`unwrap` 就会通过增加$\pm2\pi$的整数倍来平移相位角，直到跳跃小于$\pi$。



> **跨$2\pi$判断**

这里设定当前点的相位值加上$\pi$如果小于上一个点的相位值，表明当前点和上一个点已经不在同一个周期，此时就需要将累积量重置。

<img src="pics/phase_over_2pi.svg" style="zoom:80%;" />

如图所示，$P1,P2,P3$的相位分别为$w_1,w_2,w_3$，显然$w_2+\pi>w_1,w_3+\pi<w_2$，所以$P3$这里有跨$2\pi$的行为，要进行累积。

- 相位折线

  对于当前频率，相位折线实际上都是斜率为正的，只不过斜率大小不同。

  在这里，只要将载波频率$f_1,f_2$去掉中心频率$f_c$，得到$f_1=-282{\rm kHz}，f_2=+282{\rm kHz}$的相反的两个频率。

  由于相位和频率是正比关系，所以在去掉中心频率后，频率相反，相位也相反，所以此时相位折线也是斜率相反的。

  因此只要找到相位折线斜率正负符号相反的时刻，就是相位折线的拐点出现的时刻。

  因此将去中心频率的操作合并到上边相位累积量的计算中，对每个点都要去中心频率：$\begin{align} y=8192\times\dfrac{n}{N}-1280n \end{align}$.

- 溢出保护

  当检测到拐点之后，就需要将累积量进行**重置**，否则一直累加就会超出阈值。而且在拐点之后，保留上一个比特的相位累积量已经没有意义。

  实际上只需要关注每个比特的相位累积量即可。这里的溢出保护包含上溢和下溢两个方面。

  这里的上下阈值定为$\pm4\pi$，当累积量达到阈值时，就重置为$\pm2\pi$，注意**重置并没有什么影响，相当于只是将直线下拉了，但是斜率本身并不改变**。
  
  

### 3.3 HDL实现

下面实际上就是相位折线（包含累积量）的HDL实现，包含了上下溢处理、去中心频率以及$2\pi$模糊度的判断。

但是可以看到这部分没有把相位加进去，这个在程序中只需要在最小二乘法实现时，在第一个相位打拍时一起加上去就可以了。

```verilog
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
begin
    if( !i_rst_n )
      	angle_sum <= 17'd0;
    else if(i_clk_27M_en == 1'b1)begin //27M
        if((angle_sum > 17'd16384) &&(angle_sum[16]==1'b0) )begin//相位修正累积量超过+2*8192 上溢处理 恢复初值
            if ( angle_tan + 14'd4096 <= angle_tan_delay )//对2pi进行修正
              	angle_sum <= 17'd6912;//+8192-1280
            else
              	angle_sum <= 17'd0 - 17'd1280;// -1280;
        end     
        else if((angle_sum  < 17'd114688)&&(angle_sum[16]==1'b1))begin//相位修正累积量超过-2*8192 下溢处理 恢复初值
            if ( angle_tan + 14'd4096 <= angle_tan_delay )//对2pi进行修正
              angle_sum <= 17'd6912;//+8192-1280
            else
              angle_sum <= 17'd0 - 17'd1280;//-1280;
        end
        else begin//相位修正累积量在+-2*8192之间时
            if ( angle_tan + 14'd4096 <= angle_tan_delay )//对2pi进行修正
              angle_sum <= angle_sum + 17'd6912;//+8192-1280
            else
              angle_sum <= angle_sum - 17'd1280;// - 1280;
        end        
    end//i_clk_27M_en   
    else
      	angle_sum <= angle_sum;
end
```

简单来说，就是$y_n=x+y_{n-1}+Corr$，其中$y_{n-1}$是相位折线在上一个点时的值，$x$是当前相位值，$Corr$是修正量，习惯的计算方式是在计算时吧$x$加过来，但是会打乱最小二乘法计算的结构，所以根据相位计算补偿量加过去，就不会影响结构了，反正都是加法，并不影响结果。



## 4. 同步点计算

这里首先对计算的斜率值进行处理，因为斜率有可能会出现抖动，所以不能直接用来判断拐点。进一步修正之后，才能得到过零点。

### 4.1 过零点判断

首先要清楚的是对于同一bit，其相位直线的斜率应基本是稳定的，所以此时的斜率描述出来大体如下图所示：

<img src="pics/slope_distribution.svg" style="zoom:97%;" />

由于斜率都是是在理想值附近有一定波动，所以采取多点求和再求均值的办法对多点进行平滑。

实际上在每个比特最后的采样再进行过零点判决其实并不准确，这里考虑相位折线的中点进行判断，会更稳妥一些。

> 判断策略

如下图所示，取拐点左右两侧折线中点附近8点，这16点平均后应当是近似为0的，将16点均值作为修正量对中间点进行修正，进行过零判决。

寄存器共寄存51点，第1-8点、44-51点这16点计算水平斜率，中间第25、26点用来作过零判决。

<img src="pics/phase_slope_ZCP.svg" style="zoom:120%;" />

后边的延时也都是基于上图进行。其中前后8点合起来用来计算均值作为频率偏移量，对中间第25点处的频率（斜率）进行修正。



### 4.2 过零点计算和修正

先计算前8点之和，再延时43点，16点再平均得到了两段相位直线各自中间8点附近的斜率（频率）均值，该均值对中点判决点修正后再进行过零判决。

> RAM的8点读写控制

首先对RAM的读写要$8$点动态连续求和，所以要控制读写使能和地址来实现。

<img src="pics/ram_delay8_sum.svg" style="zoom:90%;" />

如图所示，是$\rm RAM\ \ 8$点滑动求和的时序图。每当滑动窗向右移动一个点时，求和结果要减去最左边的一个点。

从图中也可以看到，`s0`是第一个写入的点，在经过$8$个点后被读出，与第$9$个点对齐，即开始了下一轮的求和（滑动窗右移$1$点）。

用公式可表示为：$\begin{align} S_k(x)=\sum\limits_{i=k}^{k+7}s_i(x)-s_{k-1}(x) \end{align}$，其中$s_i$表示第$i$点的斜率值，$S_k(x)$表示从第$k$点起8点斜率的和，$s_{k-1}$表示第$k-1$点的斜率值。

关于地址的控制，写地址是$9$点，从$0$到$8$；读地址也是$9$点，从$0-8$。当写地址为$8$时，写地址清零；当写地址为$6$时，对读地址清零，读地址到$8$时也清零。



> 首尾16点求均值的原因

因为要在中间的点做过零点判决，所以$48$点的两头应当对应不同频率点$w_0,w_1$，并且频率点满足一定范围，才算得到了要进行判决的时刻。

所以取折线两头各自$8$点的均值，作为判断依据，该范围参数的确定在$4.3$节会进行详细说明。

首尾$8$点合起来$16$点再求均值，该均值是理论上不同段的相位直线斜率在稳定时、中间点的频率偏移量，用来对中间的过零点进行频率修正，修正后的频率在频率点满足范围的条件下如果过零点就是找到了同步点。



> RAM的48点斜率延时解释

如下图所示，为了使逻辑简单，在计算出前$8$点斜率和之后，将和值存入$\rm RAM$实现$43$点延时，

<img src="pics/slope_delay_43_for_48point.svg" style="zoom:120%;" />

上图中对齐的这个时刻其实已经完成了前$8$点`s0~s7`求和的计算，这个时刻应当已经输出了第一组$8$点斜率之和`sum0`.

> 滑动窗的滑动次数

如下图，宽度为$8$的滑动窗从图示位置开始滑动，向右滑动到$N$需要滑动的次数为$N-8+1$次。当滑动到$50$时，需要滑动$50-8+1=43$次。

<img src="pics/slip_window.svg" style="zoom:110%;" />

所以在计算出首部的$8$点均值后，还要延时$43$个点，这样在时序上就刚好能对上。



> 计算速度和时序链接

**【注意】**因为是滑动窗方式计算均值，所以从第$8$点之后起，计算均值的速度也是一个时钟一个结果。如下图所示。 

<img src="pics/slope_sum8_delay43.svg" style="zoom:120%;" />

从`a0`到`a42`共$43$个点，最后的$8$点求和结果是`a43`，所以要在`a43`这个时刻进行首尾$16$点的加法。也就是说**`a0`延时了$43$个点之后是和`a43`是对齐的**。

如下图所示，是对$8$点斜率之和进行$43$点延时再进行$16$点求和的时序图。这里将写地址在$44$清零，和前边写地址在$8$清零类似，都是为了读写地址衔接。

<img src="pics/ram_slope_delay43_sum.svg" style="zoom:100%;" />

图中，`sum0`是前$8$点斜率之和，最后$8$点斜率之和是`sum43`，两者在写地址为$43$时对齐。

因此要控制读地址在写地址为$41$时进行清零，读地址然后读出前$8$点斜率之和`sum0`，并在读出后和此时的后$8$点斜率之和进行$16$点求和。

如果将以上两个图合起来，在时序上对齐，就如下图所示：

<img src="pics/ram_delay43_sum8p.svg" style="zoom:100%;" />

可以看到，后$8$点斜率之和在第$52$个点即`s51`时刻输出，$16$点求和同时也在该时刻进行了计算，在下一时刻输出结果。



> RAM的17点延时和判决说明

因为要在中间时刻进行过零判决，所以要将$8$点均值再延时$17$点，到$25,26$点时进行过零点判决。时序图如下所示，其中包含了判决。

<img src="pics/sync_point_judge.svg" style="zoom:120%;" />

如图所示，$17$点读地址在写地址为$0$时清零，是因为：

（1）对`s0`单独打拍用于和`s1`比较是否异号，如是，则修正，在图中也就是第$25$点`s24`进行修正；

（2）修正后还需要再打一拍，将修正前后的两个点进行比较，如此时斜率还异号，则表示有同步点。

原来的程序中将计算的修正量在修正前判断异号时使用了阻塞赋值，应该就是为了避免这里有一拍延时导致判决后延一个点。



### 4.3 2FSK参数归一化

> 频率限定条件

两个载波频率分别对应逻辑"$0$"（$f_L$）和逻辑"$1$"（$f_H$），两种频率相互转换时，载波信号应当具有连续相位。

规定**中心频率**应为$4.234\pm0.175 \rm MHz$，频率偏移应为$282.24\rm KHz\ (1\pm7\%)$.

根据以上两个要求，就限定了两个频点$f_L,f_H$满足以下不等式：
$$
\begin{align}
4.324M-175k &\leq \dfrac{f_H+f_L}{2} \leq 4.234M+175k  \\
282.24k\times(1-7\%) &\leq\dfrac{f_H-f_L}{2} \leq 282.24k\times(1+7\%)
\end{align}
\tag{4-1}
$$
对上式$(4-1)$进行如下处理，其中在量化之前的频率点都以$\rm MHz$为单位：
$$
\begin{align}
&
\begin{cases}
4.059 &\leq \dfrac{f_H+f_L}{2} \leq 4.409 \\
0.262 &\leq \dfrac{f_H-f_L}{2} \leq 0.302
\end{cases}
\Longrightarrow
\begin{cases}
4.321 &\leq f_H &\leq 4.711 \\
-3.797 & \leq  f_L& \leq 4.107
\end{cases} \\
\overbrace{\Longrightarrow}^{去掉中心频率}_{-4.234\rm MHz}
&
\begin{cases}
0.087&\leq f_H &\leq 0.477 \\
-0.437 &\leq f_L &\leq -0.127 
\end{cases}
\overbrace{\Longrightarrow}^{\times \dfrac{8192}{27.095}}_{量化} 
\begin{cases}
26.3 &\leq f_H &\leq 144.2 \\
-132.1 &\leq f_L &\leq -38.4
\end{cases}
\overbrace{\Longrightarrow}^{\times2030}
\begin{cases}
53389   &\leq f_H &\leq 292723 \\
-268163 &\leq f_L &\leq -77952
\end{cases}
\end{align}
\tag{4-2}
$$
式$(4-2)$中主要注意的是：

- 去掉中心频率，对两个频点减去$4.234\rm MHz$；
- 乘以$2030$，这个和前边最小二乘法在计算斜率时舍弃的常数$C$有关，而$\begin{align}C=\sum\limits_{i=-14}^{14}(i^2)^{-1}=\dfrac{1}{2030}\end{align}$.

> 参数说明

这里要和最小二乘法计算保持一致性，最小二乘法在对相位直线进行拟合时，斜率舍弃了常系数$C$，而斜率其实就是频率。

举个例子，对于信号$s(t)=cos(\theta_t)$和$s(t)=\cos(2\pi f_it)$，两者是表示同一信号的不同表达式，则$\theta=2\pi f_it$，其中$\theta$是根据$\arctan(\dfrac{\rm im}{\rm re})$得到的相位，而计算相位直线斜率时用的就是$\theta$，所以相位直线的斜率和频率$f_i$具有一致性。最小二乘法使用$\theta$计算斜率舍弃了$C$，则式$(4-2)$在对频率归一化处理时也要舍弃$C$，所以才要乘以$C^{-1}=2030$，以约去系数$C$，从而保持斜率计算和$\rm 2FSK$参数归一化的一致性。

程序中在判断频率点是否符合范围时，并未严格按照该参数进行设定，还进行了上下$50\%$的扩展，即如$(4-3)$所示：
$$
\begin{align}
\begin{cases}
53389/(1+50\%)&\leq f_H &\leq 292723\times(1+50\%) \\
-268163\times(1+50\%)&\leq f_L &\leq-77952/(1+50\%)
\end{cases}
\tag{4-3}
\end{align}
$$
也就是在原频率点范围窗的基础上，保持中心不动，将范围窗的长度加长了$50\%$，如下图所示。

<img src="pics/freq_window_extend.svg" style="zoom:150%;" title="2FSK参数归一化" />

将宽度扩展后的窗作为频率点的判断范围，符合该范围时给出标志信号`slope_right`，在过零点判决时作为其中一个条件。



## 5. 同步点的跟踪修正

### 5.1 同步状态机的跳转

根据同步点的数量来判断未同步、半同步和完全同步状态。这里采用状态机来实现，会有一个辅助状态机判断进行状态跳转的计数器`cnt_sync_point`。

当检测到一个同步点时，`cnt_sync_point`置$1$，此时状态机进入半同步状态。

在半同步状态下，当拐点来临时，如果跳变点落在了同步窗内，`cnt_sync_point`就加$1$，否则认为跳变沿不正确，计数器减$1$。

当`cnt_sync_point`计数到最大同步点数时，状态机进入完全同步状态；如果计数器减到了$0$，直接跳转到未同步状态。

在完全同步状态下，如果同步拐点计数器达到最大值（$8$个），就一直保持，方便后边自减时比较快速的退出同步状态。

> 状态转移图

`cnt_sync_point`的计数逻辑和同步状态机结合起来，可以画出如下的状态转移图：

<img src="./pics/state_fsm_cnt_sync_point.svg" style="zoom:90%;" title="同步状态机" />

同步点计数器决定了同步状态机的跳转，具体的关系如上图所示。



### 5.2 三级同步跟踪修正

> 采样周期计数器

这里引入了一个计数器`cnt_48`。由于一个bit包含$48$个采样带点，一个同步点对应的位置应该是一个bit的结尾，即`cnt_48`为$47$时。

判决的最好方式是在一个比特的中间位置进行判决，可以有效降低误码率，所以以计数周期为$48$的计数器`cnt_48`在计数到$24$时进行判决。

这里引入三级同步跟踪修正的原因，是考虑**波特率的抖动以及同步点判决的误差**。

下面主要说明这个计数器的处理逻辑。

在未同步状态下，只要检测到有拐点，就将计数器清零，准备开始周期计数；没有拐点时，该计数器对采样点计数（$\rm{27M}$使能），计满$48$点就清零。

在同步状态下（半同步、完全同步），在检测到拐点时，将采样点计数器`cnt_48`分为前后两部分`cnt_48 <= 24`和`cnt_48 >= 24`：

- 前一半

  说明同步窗中心点相对拐点偏右，即采样周期偏大，要将计数器根据距离同步窗中心点的偏差对`cnt_48`进行减小，保证采样点落在同步窗的最小范围内。

- 后一半

  说明同步窗中心点相对拐点偏左，即采样周期偏小了，此时同上，要将`cnt_48`进行调大，保证采样点落在同步窗的最小范围内。

如果`cnt_48`大于最大采样采样点数$48$，表明同步点落在了同步窗外部，此时不再进行修正，并将`cnt_48`减去$48$，让采样点计数器回到正常范围内。

<img src="./pics/cnt_48.svg" style="zoom:110%;" title="cnt_48与同步窗的关系"/>

如图所示，$A$点左右两侧分别是两段相位直线。当$\rm{cnt\_48} \leq 24$时，则视为同步窗的中心是靠近同步点A的，否则就是靠近同步点$B$的。

当$\rm{cnt\_48}\leq 24$，同步窗中心靠近点$A$，则这里要相对$A$点来讲，采样周期是偏大的，所以要将同步窗中心进行左移，后有具体图示。

当$\rm{cnt\_48}\geq 24$，同步窗中心靠近点$B$，则这里要相对$B$点来讲，采样周期是偏小的，因为在$B$的左边，所以要将同步窗的中心进行右移。



> 同步窗的修正

以下讨论的**前提是采样点定义的同步窗中心点（同步点）要落在最大同步窗内**，否则不做讨论。采样偏差按照采样周期偏大和偏小分别定义。

```verilog
// 采样时刻偏差的绝对值
always @( negedge i_rst_n or posedge i_clk_81M )
begin
    if( !i_rst_n )
        det_sample <= 6'd0;
    else if(i_clk_27M_en)begin
        if(cnt_48 <= half_T)		// 采样周期偏大
            det_sample <= cnt_48;
        else						// 采样周期偏小
            det_sample <= T_modify - cnt_48;
    end
    else
        det_sample <= det_sample;  
end
```

引入三级同步窗，窗的宽度定义如下：

```verilog
parameter  sync_window_max = 7'd16;     // 同步窗的最大范围
parameter  length_window1  = 7'd3;      // 第0级同步窗，中心点和采样周期都不进行修正
parameter  length_window2  = 7'd6;      // 第1级同步窗，中心点修正1，采样周期修正1,
parameter  length_window3  = 7'd9;      // 第2级同步窗，中心点修正2，采样周期修正2
parameter  length_window4  = 7'd13;     // 第3级同步窗，中心点修正2，采样周期修正2
```

结合上面图中采样周期偏大偏小的情况分别进行图示说明。

- 采样周期偏大

  根据定义，应当是$\rm{cnt\_48}\leq 24$，即上图中绿色的一段直线。下图是同步窗的示意图：

  <img src="./pics/sync_window1.svg" style="zoom:150%;" title="采样周期偏大时" />

  其中$\rm A$是拐点，横坐标是采样点数，以`det_sample`表示，下面用`s`来代替，同步窗中心以$c$来表示，简化说明。

  当采样周期$13 \leq s \leq 16$时，将$c$向左偏移$13$个采样点，$c$落在了$w_{max}$内，后面就可以继续迭代调整；

  当采样周期$9\leq s < 13$时，将$c$向左偏移$9$个采样点，同步窗中心落在$w_2$内，下一步就可以继续迭代调整；

  当采样周期$6\leq s <9$时，将$c$向左偏移$6$个采样点，同步窗中心落在$w_2$内，下一步可以继续迭代调整；

  当采样周期$3\leq s < 6$时，将$c$向左偏移$3$个采样点，同步窗中心落在$w_1$内，之后就不用调整了；

  当采样周期$0\leq s <3$时，采样点和中心同中心点$c$都不需要修正。

- 采样周期偏小

  根据定义，应当是$\rm{cnt\_48}\geq24$，即图中的蓝色直线。下面是同步窗中心点修正的示意图。

  <img src="./pics/sync_window2.svg" style="zoom:150%;" title="采样周期偏小时" />

  其中$\rm{B}$是拐点，横坐标是采样点数，以$\rm{cnt\_48}$表示，下面以$s$代替。

  当采样周期$31 \leq s \leq 34$时，将$c$右移$11$个采样点，刚好可以落在误差最小的$\rm{B}$点处，可以少迭代且更准确。

  当采样周期$34<s\leq38$时，将$c$右移$9$个采样点，中心点$c$落在$w_1$内；

  当采样周期$38<s\leq41$时，将$c$右移$6$个采样点，中心点$c$落在$w_1$内；

  当采样周期$41<s\leq44$时，将$c$右移$3$个采样点，中心点$c$落在$w_1$内；

  当采样周期$44<s\leq47$时，已经落在最小同步窗内，此时对中心点不做修正。

如果出现了$\rm{cnt\_48}\geq 48$的情况，说明**同步点落在了同步窗之外**，此时直接将同步点左移$48$点，即：

```verilog
parameter	T_modify = 6'd47;
if(cnt_48 >= T_modify)
	cnt_48 <= cnt_48 - T_modify;
else		// 在27M_en的使能下才能加1
	cnt_48 <= cnt_48 + 1'b1;
```



### 5.3 同步点计数器

> 编码规范

根据编码规范，正确的报文中最多只有$8$个连续相同的比特，所以如果出现了$10$个连续相同的比特，说明解调结果错误。

> 失步状态

如果已经进入了同步状态，但连续$25$个比特都没有找到拐点，说明此时没有信号到来，强制置零失步，退出同步。

> 计数逻辑

同步点计数器`cnt_sync_point`的计数逻辑主要根据同步状态来确定。

- 未同步状态

  在未同步状态`sync_state=0`时，如果来了一个拐点，计数器置$1$，此时进入半同步状态。

- 半同步状态

  在半同步状态下，如果跳变点在同步窗内，计数器加$1$，否则认为跳变点不正确，计数器减$1$。

- 完全同步状态

  在半同步状态下，如果同步拐点计数达到$8$个，就进入完全同步状态。在该状态下，同步点计数达到$8$个

  但在进入完全同步状态后，如果拐点没有落在同步窗内，计数器减$1$，但不会立刻退出同步状态。

  如果后续又有拐点到来，计数器就加$1$，加到最大值$8$时，就一直保持。

  因为后续每当到来的同步点没有落在同步窗内时，计数器会减$1$，如果计数器上限较大，<font color="red">退出同步状态就会特别慢</font>。



## 6. 解调部分

### 6.1 原理

设两载波信号分别为$s_1(t)=\cos(\omega_1 t),s_2(t)=\cos(\omega_2 t)$，分别是1、0比特的载波，接收到的2FSK信号为$s(t)$，则：
$$
a=\left\{\sum\limits_{n=0}^{47}s(n\Delta T)*\cos(\omega_1n\Delta T)  \right\}^2+\left\{\sum\limits_{n=0}^{47}s(n\Delta T)*\sin(\omega_1n\Delta T) \right\}^2 \\
b=\left\{\sum\limits_{n=0}^{47}s(n\Delta T)*\cos(\omega_2n\Delta T)  \right\}^2+\left\{\sum\limits_{n=0}^{47}s(n\Delta T)*\sin(\omega_2n\Delta T) \right\}^2
$$
即当前信号$s(t)$和两个载波相乘后得到$a,b$。若$a>b$，则判决为$1$，否则判决为$0$。



### 6.2 流程控制

解调部分通过一个计数器`cnt_ctrl`来控制，该计数器基于48（采样）点进行。

当该计数器为0时，27 M采样进来的数据来一个写一个，并以108 M速率往出读，大概的时序图如下所示：

<img src="./pics/demod_cnt_ctrl_0.svg" style="zoom :80%;">

上图并不严谨（数据采样速率不同），只是一个示意：在没有同步脉冲信号时，第一次写RAM，同时紧跟着读出的数据是x（表示是垃圾数据）。当RAM写够80时，读地址置1，且读地址总比写地址大1，这样就使得刚开始写入的数据也可以被读出来。RAM开始第二轮的写操作时，读操作会比写操作早一个地址，把即将被覆盖的上一轮的这个地址的数据读出来。这个就是上图所表示的意思。



## 7. 问题

> 频偏的产生？

频偏就是调制波频率摆动的幅度。比如比特0的频点为3.951MHz，但实际频率会有一定偏移，我们针对的是在频偏在282.24kHz范围以内的信号，即实际信号的频率会在$\rm{3.951MHZ\pm282.24kHz}$范围内抖动。这个偏移主要来自于电路中的RC振荡器，由于环境的影响，振荡器并不能稳定的输出想要的频率。

具体来说，频率偏移是指实际信号的频率与预期或标准频率之间的差异。在电路中，频率偏移主要由以下一些原因产生：

- 温度变化

  电子元件通常具有温度特性，随着温度的变化，其参数会发生改变。

  例如，对于振荡器电路，电容、电感等元件的参数会受温度影响，温度升高可能导致电容和电感值变化，从而改变振荡器的振荡频率，产生频率偏移。

- 电源电压波动

  许多电路的工作性能受电源电压的影响。对于一些频率产生电路，如压控振荡器，电源电压的波动可能会改变其控制特性，进而导致输出频率发生变化。例如，当电源电压不稳定时，压控振荡器的控制电压发生变化，使得输出频率偏离设定值。

- 元件老化

  长时间使用后，电子元件会出现老化现象，比如电容电感的元件参数会发生变化，引起电路中的频率偏移。

- 外部干扰

  - 电磁干扰

    来自周围环境的电磁辐射可能会耦合到电路中，如果干扰信号的频率与电路的工作频率相近，可能会引起频率偏移。

  - 机械振动

    在一些特定的应用环境中，电路可能会收到机械振动的影响。比如晶体振荡器这种对机械应力敏感的电路，机械振动会导致元件参数发生变化。

- 负载变化

  某些电路的输出频率可能会受到负载变化的影响。比如当一个振荡器的负载发生变化时，其输出阻抗会改变，从而影响振荡频率。这是因为负载的变化会导致电路中的反馈系数、相位等参数发生变化，进而引起频率偏移。



> 解释一下非相干解调？相干解调？

相干解调，相干解调的本质是在接收机侧产生同频同相载波信号，通过相乘和低通滤波得到基带信号。

而相干解调的必要手段就是载波恢复，从接收到的信号当中恢复出载波信号，主要是恢复出载波的周期和相位。

但是FSK信号中提取相干载波相对比较困难，所以多用非相干解调法。非相干解调就是在解调中不需要提取载波信息来进行解调。



> 项目遇到的问题？

第一个问题是利用最小二乘法实现相位直线的拟合，要结合数字电路的特点，直线斜率的推导比较麻烦。

第二个问题是同步点检测算法仍有待完善，从而可以在更大频偏的情况下仍然可以实现同步。


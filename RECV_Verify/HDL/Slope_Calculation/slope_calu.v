// This is a simple example.
// You can make a your own header file and set its path to settings.
// (Preferences > Package Settings > Verilog Gadget > Settings - User)
//
//      "header": "Packages/Verilog Gadget/template/verilog_header.v"
//
// -----------------------------------------------------------------------------
// Copyright (c) 2014-2024 All rights reserved
// -----------------------------------------------------------------------------
// Author : yongchan jeon (Kris) poucotm@gmail.com
// File   : slope_calu.v
// Create : 2024-08-06 15:05:54
// Revise : 2024-08-06 15:05:54
// Editor : sublime text4, tab size (4)
// -----------------------------------------------------------------------------




// 本模块用于计算相位折线的斜率
module slope_calu(
    input wire          i_clk_108M,
    input wire          i_clk_27M_en,
    input wire          i_rst_n,
    input wire  [13:0]  iv_phase,

    output wire [25:0]  ov_slope
);

//------------------------------ 常变量声明 -----------------------------//
    // 相位修正的部分常量参数
    localparam  PHASE_2PI        = 17'd8192,
                PHASE_PI         = 14'd4096,        // pi,用于判断当前点和上一个点是否在同一周期
                MID_FREQ         = 17'd1280,        // 中心频率相位值:w=2pift=2*8192*((3.951+4.516)/2)*(1/27.095)=1280.
                PHASE_2PI_CORECT = PHASE_2PI - MID_FREQ,
                PHASE_0PI_CORECT = 17'd0 - MID_FREQ;
    // 最小二乘法计算的点数: 30-1          =29点
    parameter   NUM_PIPELINE     = 5'd30;

    integer     i;                  // 循环变量
    // 寄存器变量声明
    reg [13:0]  phase_r1;           // 输入相位打一拍和27M对齐
    reg [13:0]  phase_r2;           // 再打一拍用于和相位修正量对齐,用于相位修正
    // 相位修正量在±4pi之间.实际处理除溢出点之外,在±2pi之间.
    // 字长问题:原始相位为无符号数,输入2pi范围内,考虑补偿量为4pi范围,再加上溢出防范,以及去中心频率(转为有符号数),需要扩展3位
    reg signed  [16:0]    phase_corect;     // 相位修正量(累积量)
    // 修正量上、下溢的标志信号
    wire    overflow_up;
    wire    overflow_down;
    
    // 存储器变量声明
    reg signed  [17:0]  phase_reduce    [13:0];                 // 相隔14点两两作差
    reg signed  [16:0]  phase_pipeline  [NUM_PIPELINE-1:0];     // 将修正量加到相位上
    reg signed  [21:0]  phase_mult      [13:0];                 // 将作差结果乘以不同的常系数
    // 将乘法结果进行三级加法，具体可以参见框图
    reg signed  [22:0]  phase_mult_add1 [6:0];                  // 第一级
    reg signed  [23:0]  phase_mult_add2 [3:0];                  // 第二级
    reg signed  [25:0]  phase_mult_add3;                        // 第三级,是最终的斜率


//-------------------------- 将输入的相位按照使能信号对齐 --------------------//
    
    always@(posedge i_clk_108M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            phase_r1 <= 14'b0;
        else if(i_clk_27M_en == 1'b1)
            phase_r1 <= iv_phase;
        else
            phase_r1 <= phase_r1;
    end

//------------------------------ 计算相位补偿量 ----------------------------//
    
    // 延时一拍，用于相位修正
    always@(posedge i_clk_108M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            phase_r2 <= 14'b0;
        else if(i_clk_27M_en == 1'b1)
            phase_r2 <= phase_r1;
        else 
            phase_r2 <= phase_r2;
    end

    // 修正量上、下溢的标志
    assign  overflow_up   = (phase_corect > 17'd16384 )&&(phase_corect[16] == 1'b0);
    assign  overflow_down = (phase_corect < 17'd114688)&&(phase_corect[16] == 1'b1);
    
    // 根据相位计算补偿
    // 如果当前相位值加上pi小于上一个点的相位值,说明这两个点已经不在同一个周期,需要重置为2pi.
    always@(posedge i_clk_108M or negedge i_rst_n)
    begin 
        if(i_rst_n == 1'b0)
            phase_corect <= 17'd0;
        else if(i_clk_27M_en == 1'b1)begin
            if(overflow_up == 1'b1)begin             // 上溢,大于2*2pi
                if(phase_r1 + PHASE_PI <= phase_r2)         // 进行2pi修正,相邻两个点不在同一周期
                    phase_corect <= PHASE_2PI_CORECT;       // 重置为2pi
                else
                    phase_corect <= PHASE_0PI_CORECT;       // 在同一周期时,只需要减去中心频率即可
            end
            // -16384以原码表示即17'b1_0100_0000_0000_0000,补码表示为17'b1_1100_0000_0000_0000,如果以无符号数看待即是114688
            else if(overflow_down == 1'b1)begin       // 下溢,小于-2*2pi
                if(phase_r1 + PHASE_PI <= phase_r2)         // 进行2pi修正,相邻两个点不在同一周期
                    phase_corect <= PHASE_2PI_CORECT;       // 重置为2pi
                else
                    phase_corect <= PHASE_0PI_CORECT;       // 在同一周期时,只需要减去中心频率即可
            end
            else begin          // 未溢出
                if(phase_r1 + PHASE_PI <= phase_r2)         // 对2pi进行修正
                    // phase_+corect <= phase_corect + PHASE_2PI_CORECT;        // +8192 - 1280
                    phase_corect <= PHASE_2PI_CORECT;
                else
                    phase_corect <= phase_corect - MID_FREQ;                // 减中心频率1280
            end
        end
        else
            phase_corect <= phase_corect;
    end

//-------------------------- 最小二乘法的直线拟合 ---------------------------//

    // 29点最小二乘法每一级的计算具有相似性，用循环代替
    // 存储器额外的一个数是第一个点进行相位补偿的计算.
    always@(posedge i_clk_108M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)begin 
            for(i=0;i<NUM_PIPELINE;i=i+1)
                phase_pipeline[i] <= 17'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin 
            if((overflow_up == 1'b1)||(overflow_down == 1'b1))begin 
                phase_pipeline[0] <= {3'b0,phase_r2};       // 溢出时对该点进行相位补偿
                for(i=1;i<NUM_PIPELINE;i=i+1)begin
                    phase_pipeline[i] <= phase_pipeline[i-1] - phase_corect;
                end
            end
            else begin          // 无溢出时进行正常的2pi补偿
                phase_pipeline[0] <= {3'b0,phase_r2} + phase_corect;    // 时序上,phase_pipeline[0]和phase_corect是对齐的
                for(i=1;i<NUM_PIPELINE;i=i+1)
                    phase_pipeline[i] <= phase_pipeline[i-1];
            end
        end
        else begin          // 其他情况下保持即可
            for(i=0;i<NUM_PIPELINE;i=i+1)
                phase_pipeline[i] <= phase_pipeline[i];
        end
    end

    // 相隔14点两两作差
    always@(posedge i_clk_108M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)begin 
            for(i=0;i<14;i=i+1)
                phase_reduce[i] <= 18'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin         // 第15点为中心点
            // x_1 - x_29, x_2 - x_28, .... ,x_14 - x_16
            for(i=0;i<14;i=i+1)         
                phase_reduce[i] <= phase_pipeline[i+1] - phase_pipeline[NUM_PIPELINE-1-i];
        end
        else begin 
            for(i=0;i<14;i=i+1)
                phase_reduce[i] <= phase_reduce[i];
        end
    end

    // 将作差结果再乘以常数,所乘常数最大为14,需要扩展4位
    always@(posedge i_clk_108M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)begin
            for(i=0;i<14;i=i+1)
                phase_mult[i] <= 22'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin 
            // x*14 = x * (16-2)
            phase_mult[0] <= {phase_reduce[0],4'b0} - {{3{phase_reduce[0][17]}},phase_reduce[0],1'b0};      
            // x*13 = x * (8+4+1)
            phase_mult[1] <= {{phase_reduce[1][17]},phase_reduce[1],3'b0} + {{2{phase_reduce[1][17]}},phase_reduce[1],2'b0} + {{4{phase_reduce[1][17]}},phase_reduce[1]};
            // x*12 = x * (8+4)
            phase_mult[2] <= {{phase_reduce[2][17]},phase_reduce[2],3'b0} + {{2{phase_reduce[2][17]}},phase_reduce[2],2'b0};
            // x*11 = x * (8+2+1)
            phase_mult[3] <= {{phase_reduce[3][17]},phase_reduce[3],3'b0} + {{3{phase_reduce[3][17]}},phase_reduce[3][17],1'b0} + {{4{phase_reduce[3][17]}},phase_reduce[3]};
            // x*10 = x * (8+2)
            phase_mult[4] <= {{phase_reduce[4][17]},phase_reduce[4],3'b0} + {{3{phase_reduce[4][17]}},phase_reduce[4],1'b0};
            // x*9 = x * (8+1)
            phase_mult[5] <= {{phase_reduce[5][17]},phase_reduce[5],3'b0} + {{4{phase_reduce[5][17]}},phase_reduce[5]};
            // x*8 = x <<< 3
            phase_mult[6] <= {{phase_reduce[6][17]},phase_reduce[6],3'b0};
            // x*7 = x * (8-1)
            phase_mult[7] <= {{phase_reduce[7][17]},phase_reduce[7],3'b0} - {{4{phase_reduce[7][17]}},phase_reduce[7]};
            // x*6 = x * (4+2)
            phase_mult[8] <= {{2{phase_reduce[8][17]}},phase_reduce[8],2'b0} + {{3{phase_reduce[8][17]}},phase_reduce[8],1'b0};
            // x*5 = x * (4+1)
            phase_mult[9] <= {{2{phase_reduce[9][17]}},phase_reduce[9],2'b0} + {{4{phase_reduce[9][17]}},phase_reduce[9]};
            // x*4 = x <<< 2
            phase_mult[10] <= {{2{phase_reduce[10][17]}},phase_reduce[10],2'b0};
            // x*3 = x * (2+1)
            phase_mult[11] <= {{3{phase_reduce[11][17]}},phase_reduce[11],1'b0} + {{4{phase_reduce[11][17]}},phase_reduce[11]};
            // x*2 = x <<< 1
            phase_mult[12] <= {{3{phase_reduce[12][17]}},phase_reduce[12],1'b0};
            // x*1 
            phase_mult[13] <= {{4{phase_reduce[13][17]}},phase_reduce[13]};
        end
        else begin          // keep
            for(i=0;i<14;i=i+1)
                phase_mult[i] <= phase_mult[i];
        end
    end

    // 将乘法结果两两相加，进行三级加法
    // 第一级：两两相加，共7组加法
    always@(posedge i_clk_108M or negedge i_rst_n)
    begin 
        if(i_rst_n == 1'b0)begin
            for(i=0;i<7;i=i+1)
                phase_mult_add1[i] <= 23'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin 
            for(i=0;i<13;i=i+2)      // 注意i每次递增2,这里i从0取到12,除以2，共累加了7次
                phase_mult_add1[i/2] <= phase_mult[i] + phase_mult[i+1];
        end
        else begin 
            for(i=0;i<7;i=i+1)
                phase_mult_add1[i] <= phase_mult_add1[i];
        end
    end

    // 第二级：将第一级计算的结果两两相加，共4组
    always@(posedge i_clk_108M or negedge i_rst_n)
    begin
        if(i_rst_n == 1'b0)begin 
            for(i=0;i<4;i=i+1)
                phase_mult_add2[i] <= 24'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin 
            phase_mult_add2[0] <= phase_mult_add1[0] + phase_mult_add1[1];
            phase_mult_add2[1] <= phase_mult_add1[2] + phase_mult_add1[3];
            phase_mult_add2[2] <= phase_mult_add1[4] + phase_mult_add1[5];
            phase_mult_add2[3] <= phase_mult_add1[6];
        end
        else begin 
            for(i=0;i<4;i=i+1)
                phase_mult_add2[i] <= phase_mult_add2[i];
        end
    end

    // 第三级:将第二级计算的结果相加,共1组
    always@(posedge i_clk_108M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            phase_mult_add3 <= 26'd0;
        else if(i_clk_27M_en == 1'b1)
            phase_mult_add3 <= phase_mult_add2[0] + phase_mult_add2[1] + phase_mult_add2[2] + phase_mult_add2[3];
        else
            phase_mult_add3 <= phase_mult_add3;
    end

    // 输出相位折线的斜率值
    assign  ov_slope = phase_mult_add3;

endmodule
`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date: 2024/08/28 16:36:19
// Module Name: mux2_arbit
// Description: 双通道仲裁器,仲裁方式为轮询仲裁,该模块可进行复用:
//				1.对UDP层和ICMP层发送的数据仲裁后交给IP层;
//				2.对IP层和ARP层发送的数据仲裁后交给MAC层.
// 代码逻辑:
// 当收到数据时就会一直把vld信号作为写使能将数据写入FIFO,当最后一个数据写入时,last信号有效并保持一个clk.
// 此时该last信号会作为写使能将类型和长度信息写入FIFO,此时ctrl_fifo就是非空,并且数据全部写入data_fifo中.
// 此时,ctrl_fifo就会非空,ch0进入busy状态,状态机锁定在CH0的发送状态,并且ctrl_fifo中的数据会读出,1个clk之后其读使能拉低.
// 当ctrl_fifo的读使能拉高时,data_fifo也开始将数据读出,直到data_fifo读空后,拉低读使能,CH0发送完毕,切换到CH1状态.
//////////////////////////////////////////////////////////////////////////////////

module mux2_arbit(
	// 时钟和复位
	input	wire			i_clk,
	input 	wire			i_rst_n,

	// 通道0数据
	input	wire	[7:0]	iv_ch0_data,
	input	wire			i_ch0_data_last,
	input 	wire			i_ch0_data_vld,
	input	wire	[15:0]	iv_ch0_length,
	input	wire	[15:0]	iv_ch0_type,

	// 通道1数据
	input 	wire			i_ch1_data_vld,
	input	wire			i_ch1_data_last,
	input	wire	[7:0]	iv_ch1_data,
	input	wire	[15:0]	iv_ch1_length,
	input	wire	[15:0]	iv_ch1_type,

	// 仲裁结果输出
	output	reg				o_send_data_vld,
	output	reg				o_send_data_last,
	output	reg		[7:0]	ov_send_data,
	output	reg		[15:0]	ov_send_length,
	output	reg		[15:0]	ov_send_type
);

/*-----------------------------------------------*\
					信号定义
\*-----------------------------------------------*/

	//------------------ 通道0 ------------------//
	reg		[8:0]	ch0_data_din;		// 数据信息
	reg				ch0_data_wren;
	reg				ch0_data_rden;		// 数据读使能

	reg		[31:0]	ch0_ctrl_din;		// 控制信息,包括长度和类型.
	reg				ch0_ctrl_wren;
	reg				ch0_ctrl_rden;		// 控制信息读使能
	wire	[31:0]	ch0_ctrl_dout;

	wire			ch0_data_wrfull;
	wire			ch0_data_rdempty;
	wire	[8:0]	ch0_data_dout;
	wire			ch0_ctrl_wrfull;
	wire			ch0_ctrl_rdempty;

	reg				ch0_busy;

	//------------------ 通道1 ------------------//
	reg		[8:0]	ch1_data_din;		// 数据信息
	reg				ch1_data_wren;
	reg				ch1_data_rden;

	reg		[31:0]	ch1_ctrl_din;		// 控制信息,包括长度和类型.
	reg				ch1_ctrl_wren;
	reg				ch1_ctrl_rden;		// 控制信息读使能
	wire	[31:0]	ch1_ctrl_dout;
	wire			ch1_ctrl_wrfull;
	wire			ch1_ctrl_rdempty;

	wire			ch1_data_wrfull;
	wire			ch1_data_rdempty;
	wire	[8:0]	ch1_data_dout;

	reg				ch1_busy;

	// 状态机
	localparam  IDLE     = 2'b00,   // 空闲
				CH0_SEND = 2'b01,   // 通道0的发送状态
				CH1_SEND = 2'b10;   // 通道1的发送状态

	reg	[1:0]	cur_status;
	reg	[1:0]	nxt_status;

/*------------------------------------------------------*\
					FIFO数据输入
\*------------------------------------------------------*/
	// 仲裁方式采用轮询仲裁,所以需要将两个通道的数据都缓存到FIFO中

	// 数据和写使能
	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n) begin
			ch0_data_din  <= 9'b0;
			ch0_data_wren <= 1'b0;
			ch1_data_din  <= 9'b0;
			ch1_data_wren <= 1'b0;
		end 
		else begin
			ch0_data_din  <= {i_ch0_data_last,iv_ch0_data};
			ch0_data_wren <= i_ch0_data_vld;
			ch1_data_din  <= {i_ch1_data_last,iv_ch1_data};
			ch1_data_wren <= i_ch1_data_vld;
		end
	end

	// 控制信息
	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n) begin
			ch0_ctrl_din  <= 32'b0;
			ch0_ctrl_wren <= 1'b0;
			ch1_ctrl_din  <= 32'b0;
			ch1_ctrl_wren <= 1'b0;
		end 
		else begin
			ch0_ctrl_din  <= {iv_ch0_type,iv_ch0_length};
			ch0_ctrl_wren <= i_ch0_data_last;
			ch1_ctrl_din  <= {iv_ch1_type,iv_ch1_length};
			ch1_ctrl_wren <= i_ch1_data_last;
		end
	end

/*------------------------------------------------------*\
					busy信号
\*------------------------------------------------------*/

	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n) 
			ch0_busy <= 1'b0;
		else if(o_send_data_last)
			ch0_busy <= 1'b0;
		else if((cur_status == CH0_SEND)&&(~ch0_ctrl_rdempty))
			ch0_busy <= 1'b1;
		else
			ch0_busy <= ch0_busy;
	end

	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n) 
			ch1_busy <= 1'b0;
		else if(o_send_data_last)
			ch1_busy <= 1'b0;
		else if((cur_status == CH1_SEND)&&(~ch1_ctrl_rdempty))
			ch1_busy <= 1'b1;
		else
			ch1_busy <= ch1_busy;
	end

/*------------------------------------------------------*\
					控制信息的读使能
\*------------------------------------------------------*/

	always @(posedge i_clk or negedge i_rst_n) begin 
		if(~i_rst_n)
			ch0_ctrl_rden <= 1'b0;
		else if((cur_status == CH0_SEND) && (~ch0_ctrl_rdempty) && (~ch0_busy))
			ch0_ctrl_rden <= 1'b1;
		else
			ch0_ctrl_rden <= 1'b0;
	end

	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n)
			ch1_ctrl_rden <= 1'b0;
		else if((cur_status == CH1_SEND) && (~ch1_ctrl_rdempty) && (~ch1_busy))
			ch1_ctrl_rden <= 1'b1;
		else
			ch1_ctrl_rden <= 1'b0;
	end

/*------------------------------------------------------*\
						数据读使能
\*------------------------------------------------------*/

	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n) 
			ch0_data_rden <= 1'b0;
		else if(ch0_ctrl_rden)
			ch0_data_rden <= 1'b1;
		else if((cur_status == CH0_SEND)&&(ch0_data_dout[8]))
			ch0_data_rden <= 1'b0;		// 读出最后一个数据时,拉低
		else
			ch0_data_rden <= ch0_data_rden;
	end

	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n)  
			ch1_data_rden <= 1'b0;
		else if(ch1_ctrl_rden)
			ch1_data_rden <= 1'b1;
		else if((cur_status == CH1_SEND)&&(ch1_data_dout[8]))
			ch1_data_rden <= 1'b0;
		else
			ch1_data_rden <= ch1_data_rden;
	end

/*------------------------------------------------------*\
					FIFO例化
\*------------------------------------------------------*/

	// 【注】FIFO取名有点问题,实际输入数据位宽为9而非8
	fifo_data_w8xd2048 data_fifo_ch0 (
  		.clk   (i_clk),      		// input wire clk
  		.srst  (~i_rst_n),   	 	// input wire srst
  		.din   (ch0_data_din),      // input wire [8 : 0] din
  		.wr_en (ch0_data_wren),  	// input wire wr_en
  		.rd_en (ch0_data_rden),  	// input wire rd_en
  		.dout  (ch0_data_dout),    	// output wire [8 : 0] dout
  		.full  (ch0_data_wrfull),   // output wire full
  		.empty (ch0_data_rdempty)  	// output wire empty
	);

	// CH1 data fifo
	fifo_data_w8xd2048 data_fifo_ch1 (
  		.clk   (i_clk),      		// input wire clk
  		.srst  (~i_rst_n),    		// input wire srst
  		.din   (ch1_data_din),      // input wire [8 : 0] din
  		.wr_en (ch1_data_wren),  	// input wire wr_en
  		.rd_en (ch1_data_rden),  	// input wire rd_en
  		.dout  (ch1_data_dout),    	// output wire [8 : 0] dout
  		.full  (ch1_data_wrfull),   // output wire full
  		.empty (ch1_data_rdempty)  	// output wire empty
	);

	// 控制信息 : CH0
	fifo_ctrl_w32xd16 ctrl_fifo_ch0 (
  		.clk   (i_clk),      		// input wire clk
  		.srst  (~i_rst_n),    		// input wire srst
  		.din   (ch0_ctrl_din),      // input wire [31 : 0] din
  		.wr_en (ch0_ctrl_wren),  	// input wire wr_en
  		.rd_en (ch0_ctrl_rden),  	// input wire rd_en
  		.dout  (ch0_ctrl_dout),    	// output wire [31 : 0] dout
  		.full  (ch0_ctrl_wrfull),   // output wire full
  		.empty (ch0_ctrl_rdempty)  	// output wire empty
	);

	// 控制信息 : CH1
	fifo_ctrl_w32xd16 ctrl_fifo_ch1 (
  		.clk   (i_clk),      		// input wire clk
  		.srst  (~i_rst_n),    		// input wire srst
  		.din   (ch1_ctrl_din),      // input wire [31 : 0] din
  		.wr_en (ch1_ctrl_wren),  	// input wire wr_en
  		.rd_en (ch1_ctrl_rden),  	// input wire rd_en
  		.dout  (ch1_ctrl_dout),    	// output wire [31 : 0] dout
  		.full  (ch1_ctrl_wrfull),   // output wire full
  		.empty (ch1_ctrl_rdempty)  	// output wire empty
	);

/*------------------------------------------------------*\
					状态机控制仲裁
\*------------------------------------------------------*/

	// 时序跳转
	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n)
			cur_status <= IDLE;
		else
			cur_status <= nxt_status;
	end

	// 组合逻辑状态转移
	always @(*) begin
		if(~i_rst_n) 
			nxt_status <= IDLE;
		else begin
			case (cur_status)
				// 轮询仲裁,先无条件到CH0,如果CH0有数据就先发CH0,否则发CH1的
				IDLE : nxt_status <= CH0_SEND;
				CH0_SEND : begin
					if(~ch0_busy && ch0_data_rdempty)
						nxt_status <= CH1_SEND;
					else if(o_send_data_last)
						nxt_status <= CH1_SEND;
					else
						nxt_status <= cur_status;
				end
				CH1_SEND : begin
					if(~ch1_busy && ch1_data_rdempty)
						nxt_status <= CH0_SEND;		// 当CH1发送完毕时就轮询再检查CH0
					else if(o_send_data_last)
						nxt_status <= CH0_SEND;
					else
						nxt_status <= cur_status;
				end
				default : nxt_status <= IDLE;
			endcase
		end
	end

	// 状态机输出
	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n) begin
			o_send_data_last <= 1'b0;
			o_send_data_vld  <= 1'b0;
			ov_send_data	 <= 8'h0;
			ov_send_length   <= 16'h0;
			ov_send_type     <= 16'h0;
		end 
		else begin
			case (cur_status)
				IDLE : begin 
					o_send_data_last <= 1'b0;
					o_send_data_vld  <= 1'b0;
					ov_send_data	 <= 8'h0;
					ov_send_length   <= 16'h0;
					ov_send_type     <= 16'h0;
				end
				CH0_SEND : begin
					if(ch0_ctrl_rden)begin
						ov_send_type   <= ch0_ctrl_dout[31:16];
						ov_send_length <= ch0_ctrl_dout[15:0];
					end
					else begin
						ov_send_type   <= ov_send_type;
						ov_send_length <= ov_send_length;
					end

					if(ch0_data_rden)begin
						o_send_data_last <= ch0_data_dout[8];
						ov_send_data     <= ch0_data_dout[7:0];
						o_send_data_vld  <= 1'b1;
					end
					else begin
						o_send_data_last <= 1'b0;
						ov_send_data     <= 8'h0;
						o_send_data_vld  <= 1'b0;
					end
				end
				CH1_SEND : begin
					if(ch1_ctrl_rden)begin
						ov_send_type   <= ch1_ctrl_dout[31:16];
						ov_send_length <= ch1_ctrl_dout[15:0];
					end
					else begin
						ov_send_type   <= ov_send_type;
						ov_send_length <= ov_send_length;
					end

					if(ch1_data_rden)begin
						o_send_data_last <= ch1_data_dout[8];
						ov_send_data     <= ch1_data_dout[7:0];
						o_send_data_vld  <= 1'b1;
					end
					else begin
						o_send_data_last <= 1'b0;
						ov_send_data     <= 8'h0;
						o_send_data_vld  <= 1'b0;
					end
				end
				default : begin
					o_send_data_last <= 1'b0;
					o_send_data_vld  <= 1'b0;
					ov_send_data	 <= 8'h0;
					ov_send_length   <= ov_send_length;
					ov_send_type     <= ov_send_type;
				end
			endcase
		end
	end


endmodule

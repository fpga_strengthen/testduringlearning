%FSK解调求同步点仿真
%目的检测频偏和相位偏差对判决错误的影响
%24点积分求平方和，最小二乘法二次曲线拟合
%初始值为低通滤波后的解调结果
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%FSK调制%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc
format long
close all
clear all

%% 标准条件下主要参数
f0_true = 3.951 ;
f1_true = 4.516 ;              %比特0和比特1的频点，单位MHz
f_mid = (f0_true + f1_true)/2; % 中心频率
sample_rate_true = 27.095;     % 标准采样速率，单位MHz
BPS_data_true = 0.56448;       % 标准数据传输速率，单位MHz
ws = BPS_data_true*2*pi;
sample_point = ceil(sample_rate_true/BPS_data_true); %一个周期的采样点数 48
sum_bits = 100;%比特数
% src_data = randint(1,sum_bits);%以比特向量方式输入的待调制数据，随机产生
src_data([1:2:sum_bits-1])=1;
src_data([2:2:sum_bits])=0;
src_data_sample =  kron(src_data,ones(1,sample_point));%将src_data采样

t = 1./sample_rate_true.*[1:sample_point*sum_bits];

N = 3; % 2N+1点最小二乘法
coeff_numerator = 3*[-N:N];           %极值点多项式分子
coeff_denominator = 8 - 2 * [-N:N].^2;%极值点多项式分母

%% 比较特殊频率处理抵消效果
for w0 = -0.10*2*pi:0.01*2*pi:0.2*2*pi %频偏f0在-100K~200K之间，间隔10K连续变化
    for w1 = -0.10*2*pi:0.01*2*pi:0.2*2*pi %%频偏f1在-100K~200K之间，间隔10K连续变化
        %         for phase_0 = -pi : pi/5 : pi %初始相位在[-pi,pi]之间变化
        % for w1 = -w0:2*w0:w0
        phase_0 = 0;
        %相关解调 忽略高频后的结果
        FSK_data_S0 = (1/2*2048).*((1 - src_data_sample).*cos(w0.*t + phase_0) + src_data_sample.*cos((w1+ ws).*t + phase_0));
        FSK_data_S0_Q = (1/2*2048).*((1 - src_data_sample).*(-sin(w0.*t + phase_0)) + src_data_sample.*(-sin((w1+ ws).*t + phase_0)));
        FSK_data_S1 = (1/2*2048).*((1 - src_data_sample).*cos((w0-ws).*t + phase_0) + src_data_sample.*cos(w1.*t + phase_0));
        FSK_data_S1_Q = (1/2*2048).*((1 - src_data_sample).*(-sin((w0-ws).*t + phase_0)) + src_data_sample.*(-sin(w1.*t + phase_0)));
        
        figure(1),
        plot(FSK_data_S0,'r'),
        %         hold on,
        plot(FSK_data_S0_Q,'g'),
        plot(FSK_data_S1,'m'),
        plot(FSK_data_S1_Q,'b'),
        title(strcat('FSK信号频偏：w0=' ,num2str(w0/(2*pi)),' ,w1= ',num2str(w1/(2*pi)), 'Mhz,初始相位',num2str(phase_0)));
        
        %% 求同步点
        length_fsk_data = length(FSK_data_S0);
        for i =1:length_fsk_data-23 %相干解调过程,24个点求和，再除以24
            sum_bit0_point(i) = ( (sum(FSK_data_S0(i:i+23)))^2 + (sum(FSK_data_S0_Q(i:i+23)))^2 )./24  ;
            sum_bit1_point(i) = ( (sum(FSK_data_S1(i:i+23)))^2 + (sum(FSK_data_S1_Q(i:i+23)))^2 )./24  ;
        end
        sum_bit0_mins_sum_bit0 = sum_bit0_point(1:end-24) - sum_bit0_point(25:end);
        sum_bit1_mins_sum_bit1 = sum_bit1_point(1:end-24) - sum_bit1_point(25:end);
        sum_bit0_mins_sum_bit1 = sum_bit0_mins_sum_bit0 - sum_bit1_mins_sum_bit1;
        
        sum_bit0_mins_sum_bit1 = [zeros(1,12),sum_bit0_mins_sum_bit1(1:end)];%第1个点减第25个点得到的是第12个点的结果，而目前将该结果存在第1个位置，所以需要移位12点才正确
        sum_bit0_mins_sum_bit1 = abs(sum_bit0_mins_sum_bit1);%去掉符号位
        for i = 1:length(sum_bit0_mins_sum_bit1)
            if (sum_bit0_mins_sum_bit1(i) < max(sum_bit0_mins_sum_bit1)*0.8)%下溢清零
                sum_bit0_mins_sum_bit1(i) = 0;
            else
                sum_bit0_mins_sum_bit1(i) = sum_bit0_mins_sum_bit1(i) - max(sum_bit0_mins_sum_bit1)*0.8; %减小位数
            end %if
        end %for
        
        %% 求极值点坐标处理 最小二乘法的二次曲线拟合
        temp_val = toeplitz(sum_bit0_mins_sum_bit1(1:2*N+1),sum_bit0_mins_sum_bit1);
        sync_point = (coeff_numerator * temp_val) ./ ( coeff_denominator * temp_val);%详见文档极值点坐标公式推导
        
        sync_point(sync_point > 5) = 5;%极值应在[-3,3]之间，上溢处理
        sync_point(sync_point < -5) = -5;
        sync_point = (abs(sync_point)<0.6).* (sum_bit0_mins_sum_bit1>0);%对称轴在[-0.6,0.6]之间且该点不为零时，该坐标轴原点位置为所求极值点
        sync_point = sync_point(4:end); %该算法会让极值点向后移3个点，是否与N有关？
        sync_point = sync_point(1:end).*(~[0,sync_point(1:end-1)])%取上升沿作极值点
        
        %% 画图
        figure(2),
        subplot(2,1,1),
        hold off,
        plot(sum_bit0_point,'r');
        hold on,
        plot(sum_bit1_point,'g');
        plot(sum_bit0_mins_sum_bit1);
        legend('f0混频滤波24点平方和','f1混频滤波24点平方和','相差24点后相减结果');
        title(strcat('FSK信号频偏：w0=' ,num2str(w0/(2*pi)),' ,w1= ',num2str(w1/(2*pi)), 'Mhz,初始相位',num2str(phase_0)));
        
        figure(2),
        subplot(2,1,2),
        hold off,
        %     plot(sync_point,'r-.');
        hold on;
        plot(sum_bit0_mins_sum_bit1/max(sum_bit0_mins_sum_bit1)*8,'k');
        %     plot(sync_point_comp,'r-*');
        plot(sync_point.*10 ,'g-+');
        legend('判断折线','同步点')
        grid on;
        %     plot(( (abs(sync_point)<0.5) .* (abs(sum_bit0_mins_sum_bit1)>max(sum_bit0_mins_sum_bit1)*.8))*50,'r');
        
        %% 判断两相邻极值点是否是48的整数倍
        edge_position = find(sync_point == 1);%极值点的位置
        edge_variance = mod(edge_position(2:length(edge_position)) - edge_position(1:length(edge_position)-1),48);%两极值点之间的间隔对48点取模，得到偏差量
        
%         theory_position = [49 49+48];
%         for i=2:length(edge_position)-1
%             if ( (abs(edge_position(i-1)-theory_position(i))>24) && (abs(edge_position(i)-theory_position(i))<24) &&(abs(edge_position(i+1)-theory_position(i))>24) )%理论拐点附近有单一拐点
%                 deviation(i) = abs(edge_position(i)-theory_position(i));
%                 theory_position(i+1) = theory_position(i) + 48;
%             elseif ( (abs(edge_position(i-1)-theory_position(i))>24) && (abs(edge_position(i)-theory_position(i))<24) &&(abs(edge_position(i+1)-theory_position(i))<24) )%理论拐点附近有2个拐点
%                 deviation(i) = abs(edge_position(i)-theory_position(i));
%                 theory_position(i+1) = theory_position(i) ;
%              elseif ( (abs(edge_position(i-1)-theory_position(i))<24) && (abs(edge_position(i)-theory_position(i))<24) &&(abs(edge_position(i+1)-theory_position(i))<24) )%理论拐点附近有3个拐点
%                 deviation(i) = min(deviation(i-1),abs(edge_position(i)-theory_position(i)));
%                 theory_position(i+1) = theory_position(i) ; 
%              elseif ( (abs(edge_position(i-1)-theory_position(i))<24) && (abs(edge_position(i)-theory_position(i))<24) &&(abs(edge_position(i+1)-theory_position(i))>24) )%理论拐点附近有2个拐点
%                 deviation(i) = min(deviation(i-1),abs(edge_position(i)-theory_position(i)));
%                 theory_position(i+1) = theory_position(i) + 48;    
%              elseif ( abs(edge_position(i)-theory_position(i))>24)%理论拐点附近无拐点，漏判
%                 deviation(i) = 100;
%                 theory_position(i+1) = theory_position(i) + 48;    
%                 edge_position(i+1) = edge_position(i);
%             end %if
%         end%for
%         
%         figure(10),
%         plot(deviation);
        
        
        
        x_axis = [-23:24];
        y_axis = zeros(1,7);
        y_axis(1) = length (find(edge_variance==23));%多23个点
        y_axis(2) = length (find(edge_variance==22));%多22个点
        y_axis(3) = length (find(edge_variance==21));%多21个点
        y_axis(4) = length (find(edge_variance==20));%多20个点
        y_axis(5) = length (find(edge_variance==19));%多19个点
        y_axis(6) = length (find(edge_variance==18));%多18个点
        y_axis(7) = length (find(edge_variance==17));%多17个点
        y_axis(8) = length (find(edge_variance==16));%多16个点
        y_axis(9) = length (find(edge_variance==15));%多15个点
        y_axis(10) = length (find(edge_variance==14));%多14个点
        y_axis(11) = length (find(edge_variance==13));%多13个点
        y_axis(12) = length (find(edge_variance==12));%多12个点
        y_axis(13) = length (find(edge_variance==11));%多11个点
        y_axis(14) = length (find(edge_variance==10));%多10个点
        y_axis(15) = length (find(edge_variance==9));%多9个点
        y_axis(16) = length (find(edge_variance==8));%多8个点
        y_axis(17) = length (find(edge_variance==7));%多7个点
        y_axis(18) = length (find(edge_variance==6));%多6个点
        y_axis(19) = length (find(edge_variance==5));%多5个点
        y_axis(20) = length (find(edge_variance==4));%多4个点
        y_axis(21) = length (find(edge_variance==3));%多3个点
        y_axis(22) = length (find(edge_variance==2));%多2个点
        y_axis(23) = length (find(edge_variance==1));%多1个点
        y_axis(24) = length (find(edge_variance==0));%无偏差
        y_axis(25) = length (find(edge_variance==47));%少1个点
        y_axis(26) = length (find(edge_variance==46));%少2个点
        y_axis(27) = length (find(edge_variance==45));%少3个点
        y_axis(28) = length (find(edge_variance==44));%少4个点
        y_axis(29) = length (find(edge_variance==43));%少5个点
        y_axis(30) = length (find(edge_variance==42));%少6个点
        y_axis(31) = length (find(edge_variance==41));%少7个点
        y_axis(32) = length (find(edge_variance==40));%少8个点
        y_axis(33) = length (find(edge_variance==39));%少9个点
        
        y_axis(34) = length (find(edge_variance==38));%少10个点
        y_axis(35) = length (find(edge_variance==37));%少11个点
        y_axis(36) = length (find(edge_variance==36));%少12个点
        y_axis(37) = length (find(edge_variance==35));%少13个点
        y_axis(38) = length (find(edge_variance==34));%少14个点
        y_axis(39) = length (find(edge_variance==33));%少15个点
        y_axis(40) = length (find(edge_variance==32));%少16个点
        y_axis(41) = length (find(edge_variance==31));%少17个点
        y_axis(42) = length (find(edge_variance==30));%少18个点
        y_axis(43) = length (find(edge_variance==29));%少19个点
        y_axis(44) = length (find(edge_variance==28));%少20个点
        y_axis(45) = length (find(edge_variance==27));%少21个点
        y_axis(46) = length (find(edge_variance==26));%少22个点
        y_axis(47) = length (find(edge_variance==25));%少23个点
        y_axis(48) = length (find(edge_variance==24));%少24个点
        y_axis = y_axis./sum(y_axis);
        
        figure(3),
        bar(x_axis,y_axis);
        xlabel('偏差量');
        ylabel('统计概率值');
        set(gca,'xtick',-23:1:24);
        title(strcat('FSK信号频偏:w0=' ,num2str(w0/(2*pi)),' , w1=',num2str(w1/(2*pi)), 'Mhz,初始相位:',num2str(phase_0),'时统计偏移量'));
        
        pause;
        close all;
        %         end %for phase_0
    end %for w1
end %for w0

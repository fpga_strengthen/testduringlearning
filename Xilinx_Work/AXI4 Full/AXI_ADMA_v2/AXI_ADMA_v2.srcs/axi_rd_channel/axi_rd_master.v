`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Module Name: axi_rd_master
// Description: 
// Revision 0.01 - File Created
//////////////////////////////////////////////////////////////////////////////////

module axi_rd_master #(
	parameter   AXI_DATA_WIDTH  = 128,		// AXI数据位宽
	parameter   AXI_ADDR_WIDTH  = 32		// AXI读地址宽度
	)(
	input		wire							i_rst_n,				// 复位,低有效
	input 		wire							i_axi_clk,				// AXI时钟

	/*----------------------与rd_buffer交互信号-------------------*/
	input		wire							i_axi_ar_req_en,		// 读地址写请求
	output		wire							o_axi_ar_ready,			// master准备信号
	input		wire	[7:0]					iv_axi_ar_burst_len,	// 突发长度
	input		wire	[AXI_ADDR_WIDTH-1:0]	iv_axi_ar_addr,			// 读地址

	output		reg								o_axi_r_valid,			// 由从机读出的数据的有效信号
	output		reg		[AXI_DATA_WIDTH-1:0]	ov_axi_r_data,			// 由从机传来的数据
	output		reg								o_axi_r_last,			// 最后一个数据的标志

	/*-----------------------与从机的交互信号----------------------*/
	// 读通道读数据
	input		wire	[3:0]					iv_m_axi_rid,			// 读 IDtag
	input		wire	[AXI_DATA_WIDTH-1:0]	iv_m_axi_rdata,			// 读数据
	input		wire	[1:0]					iv_m_axi_rresp,			// 读响应，表明读传输的状态
	input		wire							i_m_axi_rlast,			// 表明读突发的最后一次传输
	input		wire							i_m_axi_rvalid,			// 表明此通道信号有效
	output		wire							o_m_axi_rready,			// 表明主机能够接收读数据和响应信息

	// 读通道地址与控制信号
	output		reg		[3:0]					ov_m_axi_arid,			// 读地址 ID，用来标志一组写信号
	output		reg		[AXI_ADDR_WIDTH-1:0]	ov_m_axi_araddr,		// 读地址，给出一次写突发传输的读地址
	output		reg		[7:0]					ov_m_axi_arlen,			// 突发长度，给出突发传输的次数
	output		reg		[2:0]					ov_m_axi_arsize,		// 突发大小，给出每次突发传输的字节数
	output		reg		[1:0]					ov_m_axi_arburst,		// 突发类型
	output		reg								o_m_axi_arlock,			// 总线锁信号，可提供操作的原子性
	output		reg		[3:0]					ov_m_axi_arcache,		// 内存类型，表明一次传输是怎样通过系统的
	output		reg		[2:0]					ov_m_axi_arprot,		// 保护类型，表明一次传输的特权级及安全等级
	output		reg		[3:0]					ov_m_axi_arqos,			// 质量服务 QOS
	output		reg								o_m_axi_arvalid,		// 有效信号，表明此通道的地址控制信号有效
	input		wire							i_m_axi_arready			// 表明“从”可以接收地址和对应的控制信号
);

/*-------------------------------------------*\
			  复位信号Sync定义
\*-------------------------------------------*/
  	(* dont_touch="true" *)	reg		axi_rstn_sync_d0;
  	(* dont_touch="true" *)	reg		axi_rstn_sync_d1;
  	(* dont_touch="true" *)	reg		axi_rstn_sync;

/*-------------------------------------------*\
					状态机
\*-------------------------------------------*/

	reg	[2:0]	cur_status;
	reg	[2:0]	nxt_status;

	localparam  AXI_RD_IDLE = 3'b000,
				AXI_RD_PRE  = 3'b001,
				AXI_RD_DATA = 3'b010,
				AXI_RD_END  = 3'b100;

/*-------------------------------------------*\
			  	复位信号CDC
\*-------------------------------------------*/
	always @(posedge i_axi_clk) begin
		axi_rstn_sync_d0 <= i_rst_n;
		axi_rstn_sync_d1 <= axi_rstn_sync_d0;
		axi_rstn_sync    <= axi_rstn_sync_d1;
	end

	always @(posedge i_axi_clk) begin
		ov_m_axi_arprot  <= 3'b0;
		ov_m_axi_arid    <= 4'b0;
		ov_m_axi_arburst <= 2'b01;		// INCR类型
		o_m_axi_arlock   <= 1'b0;
		ov_m_axi_arcache <= 4'b0;
		ov_m_axi_arqos   <= 4'b0;
		// AXI宽度width和AxSize的对应关系 : width = (2^AxSize)*8
		ov_m_axi_arsize  <= (AXI_DATA_WIDTH == 512) ? 3'h6 :
						   	(AXI_DATA_WIDTH == 256) ? 3'h5 :
						   	(AXI_DATA_WIDTH == 128) ? 3'h4 :
						   	(AXI_DATA_WIDTH ==  64) ? 3'h3 :
						   	(AXI_DATA_WIDTH ==  32) ? 3'h2 : 
						   	(AXI_DATA_WIDTH ==  16) ? 3'h1 : 3'h0;
	end

/*-------------------------------------------*\
					assign
\*-------------------------------------------*/

	assign	o_m_axi_rready = 1'b1;		// 主机的准备状态一直拉高即可
	assign	o_axi_ar_ready = (cur_status == AXI_RD_PRE);

/*-------------------------------------------*\
					状态机
\*-------------------------------------------*/

	always @(posedge i_axi_clk or negedge axi_rstn_sync) begin
		if(~axi_rstn_sync)
			cur_status <= AXI_RD_IDLE;
		else 
			cur_status <= nxt_status;
	end

	always @(*) begin
		if(~axi_rstn_sync)
			nxt_status <= AXI_RD_IDLE;
		else begin
			case (cur_status)
				AXI_RD_IDLE : begin
					if(i_axi_ar_req_en)
						nxt_status <= AXI_RD_PRE;
					else
						nxt_status <= cur_status;
				end
				AXI_RD_PRE : nxt_status <= AXI_RD_DATA;
				AXI_RD_DATA : begin
					if(o_m_axi_rready && i_m_axi_rvalid && i_m_axi_rlast)
						nxt_status <= AXI_RD_END;
					else
						nxt_status <= cur_status;
				end
				AXI_RD_END : nxt_status <= AXI_RD_IDLE;
				default : nxt_status <= AXI_RD_IDLE;
			endcase
		end
	end

/*-------------------------------------------*\
				读通道读地址输出
\*-------------------------------------------*/
	
	// 地址有效信号:地址只需要传一次突发地址即可.
	always @(posedge i_axi_clk or negedge axi_rstn_sync) begin
		if(~axi_rstn_sync) 
			 o_m_axi_arvalid <= 1'b0;
		else if(o_m_axi_arvalid && i_m_axi_arready)
			o_m_axi_arvalid <= 1'b0;	// 收到读准备信号后就拉低
		else if(o_axi_ar_ready && i_axi_ar_req_en)
			o_m_axi_arvalid <= 1'b1;
		else
			o_m_axi_arvalid <= o_m_axi_arvalid;
	end

	// 发送读地址和突发长度
	always @(posedge i_axi_clk or negedge axi_rstn_sync) begin
		if(~axi_rstn_sync) begin
			ov_m_axi_araddr <= {AXI_ADDR_WIDTH{1'b0}};
			ov_m_axi_arlen  <= 8'h0;
		end	 
		else if(i_axi_ar_req_en && o_axi_ar_ready)begin
			ov_m_axi_araddr <= iv_axi_ar_addr;
			ov_m_axi_arlen  <= iv_axi_ar_burst_len;
		end
		else begin
			ov_m_axi_araddr <= ov_m_axi_araddr;
			ov_m_axi_arlen  <= ov_m_axi_arlen;
		end
	end

	// 将由从机读出的数据传到rd_buffer进行跨时钟域处理
	always @(posedge i_axi_clk or negedge axi_rstn_sync) begin
		if(~axi_rstn_sync) begin
			ov_axi_r_data <= {AXI_DATA_WIDTH{1'b0}};
			o_axi_r_valid <= 1'b0;
			o_axi_r_last  <= 1'b0;
		end
		else begin
			ov_axi_r_data <= iv_m_axi_rdata;
			o_axi_r_valid <= i_m_axi_rvalid;
			o_axi_r_last  <= i_m_axi_rlast;
		end 
	end

endmodule
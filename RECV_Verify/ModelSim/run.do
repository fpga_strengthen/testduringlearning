# To run this example, bring up the simulator and type the following at the prompt:
#     do run.do
# or, to run from a shell, type the following at the shell prompt:
#     vsim -c -do run.do
# (omit the "-c" to see the GUI while running from the shell)
# Remove the "quit -f" command from this file to view the results in the GUI.
#
quit -sim
.main clear
#set testbench name;设置顶层仿真module名
set tb_name Sync_tb
#set the sim home dir ;设置仿真目录
set sim_home "../Modelsim"
#set the src code home dir;设置源文件目录
set src_home "../HDL"

#set the testbench code home dir;设置testbench源文件目录
set tb_home "../Testbench"
#新建work库
if [file exists work] {
    vdel -all
}
# 仿真时如果报错提示找不到mif文件,将mif放置到和run.do的相同目录下即可
vlib ${sim_home}/rtl_work
#映射默认work库到sim home下的work库
vmap work ${sim_home}/rtl_work
#编译altera_lib下所有仿真库
vlog ${sim_home}/libraries/220model.v
vlog ${sim_home}/libraries/altera_mf.v

# Compile the library sources.
# IP Core Library
vlib lpm_ver
vmap lpm_ver lpm_ver
vlog -work lpm_ver {./libraries/220model.v}

vlib altera_mf_ver
vmap altera_mf_ver altera_mf_ver
vlog -work altera_mf_ver {./libraries/altera_mf.v}

#----------------------编译src目录下所有的v文件-----------------------
# HilbertTransform
vlog ${src_home}/HilbertTransform/IP_Mult/mult_signed_hilbert*.v
vlog ${src_home}/HilbertTransform/Hilbert.v

# Cordic used to calculate phase
vlog ${src_home}/Phase_Calu/Cordic_Phase_Map/cordic_arctan_rad.v

# Calculate slope
vlog ${src_home}/Slope_Calculation/slope_calu.v

# Calaulate and judge Sync_point
vlog ${src_home}/ZCP_Judge/IP_RAM_8x9/ram_sum_8p.v
vlog ${src_home}/ZCP_Judge/IP_RAM_26x17/ram_slope_delay17.v
vlog ${src_home}/ZCP_Judge/IP_RAM_Delay43/ram_slope_delay_43p.v
vlog ${src_home}/ZCP_Judge/ZCP_judge.v

# Compile top module
vlog ${src_home}/Sync_top.v

#----------------------------------------------------------------------

#编译仿真目录下指定的.v文件
vlog -sv +define +ASSERT ${tb_home}/Sync_tb.sv

# Simulate the design.
vsim -L lpm_ver -L altera_mf_ver Sync_tb -voptargs="+acc"

# View the results.
#if {![batch_mode]} {
#	log -r /*
#	add wave -divider "Testbench Signals"
#	add wave /testbench/clock

#	wave zoomfull
#}
do wave_sync.do

run 1ms
#quit -f

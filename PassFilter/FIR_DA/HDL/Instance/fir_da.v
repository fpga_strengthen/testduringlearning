module fir_da 
#(
    parameter   N_taps    = 9,
    parameter   BIT_WIDTH = 16
)(
    input   wire    clk,
    input   wire    clk_en,
    input   wire    rst_n,
    input   wire    signed  [BIT_WIDTH-1:0] filter_in,
    output  reg     signed  [BIT_WIDTH-1:0] filter_out
);
    
    localparam  DA_WIDTH = BIT_WIDTH  + `log2(N_taps);
    localparam  SUM_WIDTH = BIT_WIDTH + `log2(BIT_WIDTH) + `log2(N_taps);

    //定义一个存储器，用来缓存输入的数据
    reg     signed  [N_taps-1:0]    delay_pipeline  [BIT_WIDTH-1:0];

    wire    signed  [DA_WIDTH-1:0]  DA_data [BIT_WIDTH-1:0];
    wire    signed  [SUM_WIDTH-1:0] sum     [BIT_WIDTH:0];

    assign  sum[0] = 0;

    integer j;
    //该生成块用于实现存储器的初始化，并将输入数据存储到比特矩阵中
    //如果for循环的方式不利于理解，可以将其展开看
    generate
        genvar i;
        for(i=0;i < BIT_WIDTH;i=i+1)begin:BIT_WIDTH_1bit_LUT
            always@(posedge clk)begin 
                if(rst_n == 1'b0)begin 
                    for(j=0;j < N_taps;j=j+1)begin:bit_matrix_intial 
                        delay_pipeline[i][j] <= 1'b0;           //初始化比特矩阵
                    end
                end
                else begin 
                    if(clk_en == 1'b1)begin 
                        delay_pipeline[i][0] <= filter_in[i];
                        for(j=1;j < N_taps;j=j+1)begin:bit_matrix
                            delay_pipeline[i][j] <= delay_pipeline[i][j-1];
                        end
                    end
                end
            end
            //通过DA查找表，得到每个位的计算结果
            DA_ROM #(N_taps,DA_WIDTH) U_bit(.addr(delay_pipeline[i]), .data(DA_data[i]));
            //对每位结果进行移位累加，保证权重正确
            assign  sum[i+1] = sum[i] + (DA_data[i] << i);
        end
    endgenerate

/*
    //将其中一个for循环展开后可以写成如下形式
    generate
        genvar i;
        for(i=0;i < BIT_WIDTH;i=i+1)begin:BIT_WIDTH_1bit_LUT
            always@(posedge clk)begin 
                if(rst_n == 1'b0)begin 
                    for(j=0;j < N_taps;j=j+1)begin:bit_matrix_intial 
                        delay_pipeline[i][j] <= 1'b0;           //初始化比特矩阵
                    end
                end
                else begin 
                    if(clk_en == 1'b1)begin
                        //当clk_en和posedge clk有效时，就会一次性执行所有的i和j
                        //也就是这一瞬间就完成了filter_in的比特存储和N_taps次打拍
                        delay_pipeline[i][0] <= filter_in[i];
                        delay_pipeline[i][1] <= delay_pipeline[i][0];
                        delay_pipeline[i][2] <= delay_pipeline[i][1];
                        delay_pipeline[i][3] <= delay_pipeline[i][2];
                        delay_pipeline[i][4] <= delay_pipeline[i][3];
                        delay_pipeline[i][5] <= delay_pipeline[i][4];
                        delay_pipeline[i][6] <= delay_pipeline[i][5];
                        delay_pipeline[i][7] <= delay_pipeline[i][6];
                        delay_pipeline[i][8] <= delay_pipeline[i][7];
                    end
                end
            end
        end
    endgenerate
*/

    //输出滤波器结果，没有做幅度调整
    always@(posedge clk)begin 
        if(rst_n == 1'b0)
            filter_out <= 0;
        else if(clk_en == 1'b1)     
            filter_out <= sum[N_taps][SUM_WIDTH - 1 : SUM_WIDTH - BIT_WIDTH];
        else
            filter_out <= filter_out;
    end

endmodule
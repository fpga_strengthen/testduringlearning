//============================================================
//
//--- ALL RIGHTS RESERVED
//--- File		: mcp_tx_msg.v
//--- Auther	: smartbai
//--- Data		: 2020-11-19
//--- Version	: v1.0.0.0
//--- Abstract	: 
//
//------------------------------------------------------------
//Description	:
//------------------------------------------------------------
//--- Modification History:
//--- Date		By		Version		Change Description
//------------------------------------------------------------
//
//============================================================

module mcp_tx_msg (
    i_clk,          //27MHz
    i_clk_81M,
    i_rst_n,

    //msg rx 81MHz
    i_msg_end,
    i_msg_en,
    iv_msg_data,

    i_frame_msg_en,

    i_byte_ok,
    i_frame_ok,
    o_byte_en,
    ov_byte,

    ov_frame_id,        //7bit 0x01~0x1E 30个循环
    o_tx_msg_req,       //发送msg报文请求
    o_frame_msg_ok     //发送msg报文完成信号
);
    input i_clk;          //27MHz
    input i_clk_81M;
    input i_rst_n;

    //msg rx 81MHz
    input i_msg_end;
    input i_msg_en;
    input [7:0] iv_msg_data;

    input i_frame_msg_en;

    input i_byte_ok;
    input i_frame_ok;
    output o_byte_en;
    output [7:0] ov_byte;

    output [6:0] ov_frame_id;        //7bit 0x01~0x1E 30个循环
    output o_tx_msg_req;       //发送msg报文请求
    output o_frame_msg_ok;     //发送msg报文完成信号
//-------------------------------------------------------------------//
    wire rd_en;
    wire [7:0] rd_data;

    //发送控制
    reg tx_clk;         //发送时钟控制
    reg [7:0] cnt_send; //发送计数器
    
    //发送使能和发送数据
    reg byte_en;
    reg [7:0] byte_data;
//-------------------------------------------------------------------//
//实际数据240字节
    // TxFIFO U_FIFO_MSG(
    //     .wrclk( i_clk_81M ),    //27Hz
    //     .wrreq( i_msg_en  ),
    //     .data( iv_msg_data ),   // 外部接口
    //     .rdclk( i_clk ),        //27MHz
    //     .rdreq( rd_en ),
    //     .q( rd_data ),
    //     .rdusedw(  )
    // );
    reg [8:0] wr_ram_addr;
    reg [8:0] rd_ram_addr;

    always @(posedge i_clk_81M or negedge i_rst_n) begin
        if(~i_rst_n)
            wr_ram_addr <= 9'd0;
        else if(i_msg_end)
            wr_ram_addr <= 9'd0;
        else if(i_msg_en)
            wr_ram_addr <= wr_ram_addr + 1'b1;
        else
            wr_ram_addr <= wr_ram_addr;
    end

    msg_ram msg_ram_u (
    .data(iv_msg_data),
    .rdaddress(rd_ram_addr),
    .rdclock(i_clk),
    .rden(rd_en),
    .wraddress(wr_ram_addr),
    .wrclock(i_clk_81M),
    .wren(i_msg_en),
    .q(rd_data)
    );

    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n)
            rd_ram_addr <= 9'd0;
        else if((rd_ram_addr == 9'd239) && rd_en)
            rd_ram_addr <= 9'd0;
        else if(rd_en)
            rd_ram_addr <= rd_ram_addr +1'b1;
        else
            rd_ram_addr <= rd_ram_addr;
    end
//-------------------------------------------------------------------//
//向mcp控制模块产生发送msg数据请求
    //81MHz时钟域
    reg msg_end_r;
    // reg msg_end_p;
    always @(posedge i_clk_81M or negedge i_rst_n) begin
        if(~i_rst_n) begin
            msg_end_r <= 1'b0;
            // msg_end_p <= 1'b0;
        end
        else begin
            msg_end_r <= i_msg_end;
            // msg_end_p <= i_msg_end && (~msg_end_r);
        end
    end

    //81MHz 同步到 27MHz
    reg tx_msg_req_r1;
    reg tx_msg_req_r2;
    reg tx_msg_req_r3;
    always @(posedge i_clk or posedge msg_end_r) begin
        if(msg_end_r) begin
            tx_msg_req_r1 <= 1'b1;
            tx_msg_req_r2 <= 1'b0;
            tx_msg_req_r3 <= 1'b0;
        end
        else begin
            tx_msg_req_r1 <= 1'b0;
            tx_msg_req_r2 <= tx_msg_req_r1;
            tx_msg_req_r3 <= tx_msg_req_r2;
        end
    end

    assign o_tx_msg_req = tx_msg_req_r3 && (~tx_msg_req_r2);

//-------------------------------------------------------------------//
    reg byte_ok_r;
    reg byte_ok_p;
    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n) begin
            byte_ok_r <= 1'b0;
            byte_ok_p <= 1'b0;
        end
        else begin
            byte_ok_r <= i_byte_ok;
            byte_ok_p <= i_byte_ok && (~byte_ok_r);
        end
    end
    reg frame_ok_r;
    // reg frame_ok_p;
    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n) begin
            frame_ok_r <= 1'b0;
            // frame_ok_p <= 1'b0;
        end
        else begin
            frame_ok_r <= i_frame_ok;
            // frame_ok_p <= i_frame_ok && (~frame_ok_r);
        end
    end

    reg frame_msg_en_r;
    reg frame_msg_en_p;
    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n) begin
            frame_msg_en_r <= 1'b0;
            frame_msg_en_p <= 1'b0;
        end
        else begin
            frame_msg_en_r <= i_frame_msg_en;
            frame_msg_en_p <= i_frame_msg_en && (~frame_msg_en_r);
        end
    end

//-------------------------------------------------------------------//
//产生发送时钟
    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n)
            tx_clk <= 1'b0;
        else if(frame_msg_en_p || ((cnt_send >= 8'd1 && cnt_send < 8'd240) && byte_ok_p))
            tx_clk <= 1'b1;
        else
            tx_clk <= 1'b0;
    end

    assign rd_en = tx_clk;

    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n)
            cnt_send <= 8'd0;
        else if(frame_msg_en_p)
            cnt_send <= 8'd0;
        else if((cnt_send == 8'hF0) && byte_ok_p)
            cnt_send <= 8'd0;
        else if(tx_clk)
            cnt_send <= cnt_send + 1'b1;
        else
            cnt_send <= cnt_send;
    end

    reg tx_clk_r1;
    reg tx_clk_r2;
    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n) begin
            tx_clk_r1 <= 1'b0;
            tx_clk_r2 <= 1'b0;
        end
        else begin
            tx_clk_r1 <= tx_clk;
            tx_clk_r2 <= tx_clk_r1;
        end
    end

    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n)
            byte_en <= 1'b0;
        else if(tx_clk_r2)
            byte_en <= 1'b1;
        else
            byte_en <= 1'b0;
    end

    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n)
            byte_data <= 8'd0;
        else if(tx_clk_r2)
            byte_data <= rd_data;
        else
            byte_data <= byte_data;
    end

    assign o_byte_en = byte_en;
    assign ov_byte = byte_data;
//-------------------------------------------------------------------//

    reg [6:0] frame_id;
    always @(posedge i_clk or negedge i_rst_n) begin
        if(!i_rst_n)
            frame_id <= 7'd0;
        else if(frame_msg_en_p)
            frame_id <= 7'd1;
        else if(frame_ok_r)
            frame_id <= frame_id + 1'b1;
        else
            frame_id <= frame_id;
    end
    
    assign ov_frame_id = frame_id;
//---------------------------------------------------------//
//产生发送完成信号
    reg frame_msg_ok;
    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n)
            frame_msg_ok <= 1'b0;
        else if((cnt_send == 8'hF0) && byte_ok_p)
            frame_msg_ok <= 1'b1;
        else
            frame_msg_ok <= 1'b0;
    end

    assign o_frame_msg_ok = frame_msg_ok;

endmodule
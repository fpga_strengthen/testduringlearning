//-----------------------------------------------------------------------//
//用来判断天线状态，如果超过5S没有收到自检或者报文则报天线异常，如果有收到自检或者报文则通过CAN报天线正常
//--- Modification History:
//--- Date          By          Version         Change Description 
//-------------------------------------------------------------------
//--- 2014.05.16  dengneng    V1.1              添加保密芯片，将保密信息状态通过CAN总线传输给通信单元
//-------------------------------------------------------------------------//
module nofsk(
          i_rst_n,
          i_clk_27M,
          i_fsk_come,//有自检信号到来,用m_ready_p
          i_data_come,//有报文到来,用RAM存完报文的使能表示
//          i_safe_enable, //保密芯片工作正常信号
          
          o_ant_nor,//天线正常信号
          o_ant_err//天线异常，3S钟没有收到任何信号       
          );
          
input i_rst_n;
input i_clk_27M;
input i_fsk_come;
input i_data_come;
//input i_safe_enable;

output o_ant_nor;
output o_ant_err;

//------------------------信号声明------------------------------------//
wire fsk_come_p;
reg fsk_come_temp1;
reg fsk_come_temp2;

wire data_come_p;
reg data_come_temp1;
reg data_come_temp2;

reg tx_state;//天线有自检或者报文的标志
reg tx_state_temp1;
reg tx_state_temp2;
wire tx_state_p;

reg [29:0]cnt_20s;//20s没有任何信号
reg alarm;
wire sign_nofsk;

reg [26:0]cnt_3s_auto;//每隔3S自动上传一次状态
reg ant_state;//天线状态
reg state_ant_err;
reg state_ant_nor;
reg [3:0]cnt_ant_err;
reg [3:0]cnt_ant_nor;
wire  o_ant_err;
wire  o_ant_nor;

/////////////////////////////////////////////////////////////
//取沿
always @ (negedge i_rst_n or posedge i_clk_27M)
  begin
    if(!i_rst_n)
      begin
        fsk_come_temp1 <= 1'b0;
        fsk_come_temp2 <= 1'b0;
      end
    else
      begin
        fsk_come_temp1 <= i_fsk_come;
        fsk_come_temp2 <= fsk_come_temp1;
      end
  end

assign  fsk_come_p = ~fsk_come_temp2 && fsk_come_temp1;


reg data_come_r;
always @(posedge i_clk_27M or posedge i_data_come) begin
    if(i_data_come == 1'b1)
        data_come_r <= 1'b1;
    else
        data_come_r <= 1'b0;
end

always @ (negedge i_rst_n or posedge i_clk_27M) begin
    if(!i_rst_n) begin
        data_come_temp1 <= 1'b0;
        data_come_temp2 <= 1'b0;
    end
    else begin
        data_come_temp1 <= data_come_r;
        data_come_temp2 <= data_come_temp1;
    end
end

assign  data_come_p = ~data_come_temp2 && data_come_temp1;

//////////////////////////////////////////////////////////////
//天线有自检或者报文的标志
always @ (negedge i_rst_n or posedge i_clk_27M )
  begin
    if(!i_rst_n)
      tx_state <= 1'b0;
    else if(fsk_come_p || data_come_p)
      tx_state <= 1'b1;
    else
      tx_state <= 1'b0;
  end
    
always @ (negedge i_rst_n or posedge i_clk_27M)
  begin
    if(!i_rst_n)
      begin
        tx_state_temp1 <= 1'b0;
        tx_state_temp2 <= 1'b0;
      end
    else
      begin
        tx_state_temp1 <= tx_state;
        tx_state_temp2 <= tx_state_temp1;
      end
  end

assign  tx_state_p = ~tx_state_temp2 && tx_state_temp1;

//////////////////////////////////////////////////////////////
//由于挥应答器速度慢会解出多条报文，利用CAN给通信单元发送状态时会发送多条状态，现在改为当有报文来时不发送状态
//reg tx_state_selfcheck;//只报天线自检的状态，报文的状态不报
//always @ (negedge i_rst_n or posedge i_clk_27M )
//  if(!i_rst_n)
//    tx_state_selfcheck <= 1'b0;
//  else if(fsk_come_p )
//    tx_state_selfcheck <= 1'b1;
//  else
//    tx_state_selfcheck <= 1'b0;
//    
//reg tx_state_selfcheck_temp1;
//reg tx_state_selfcheck_temp2;
//wire tx_state_selfcheck_p;
//always @ (negedge i_rst_n or posedge i_clk_27M)
//  if(!i_rst_n)
//    begin
//      tx_state_selfcheck_temp1 <= 1'b0;
//      tx_state_selfcheck_temp2 <= 1'b0;
//    end
//  else
//    begin
//      tx_state_selfcheck_temp1 <= tx_state_selfcheck;
//      tx_state_selfcheck_temp2 <= tx_state_selfcheck_temp1;
//    end
//assign  tx_state_selfcheck_p = ~tx_state_selfcheck_temp2 && tx_state_selfcheck_temp1;

///////////////////////超过20S没有任何信号，表明天线异常/////////////////////////
always @ (negedge i_rst_n or posedge i_clk_27M)
  begin
    if ( !i_rst_n )
      cnt_20s <= 30'd0;
    else if( tx_state_p )
      cnt_20s <= 30'd0;
  //  else if( cnt_5s == 28'd135475000 )
    else if( cnt_20s == 30'd541900000 )  // 27.095M*20s = 541900000
      cnt_20s <= 30'd0;
    else
      cnt_20s <= cnt_20s + 30'd1;
  end
    
always @ (negedge i_rst_n or posedge i_clk_27M)
  begin
    if ( !i_rst_n )
      alarm <= 1'b0;
    else if( cnt_20s == 30'd541900000 )
      alarm <= 1'b1;
    else
      alarm <= 1'b0;
  end

assign  sign_nofsk = alarm;

//reg [3:0]cnt_ant_err;
//always @ (negedge i_rst_n or posedge i_clk_27M)
//  if ( !i_rst_n )
//    cnt_ant_err <= 4'd0;
//  else if(sign_nofsk)
//    cnt_ant_err <= 4'b1;
//  else if(cnt_ant_err)
//    cnt_ant_err <= cnt_ant_err + 4'b1;
//  else
//    cnt_ant_err <= 4'b0;
//    
//reg [3:0]cnt_ant_nor;
//always @ (negedge i_rst_n or posedge i_clk_27M)
//  if ( !i_rst_n )
//    cnt_ant_nor <= 4'd0;
//  else if(tx_state_p)
//    cnt_ant_nor <= 4'b1;
//  else if(cnt_ant_nor)
//    cnt_ant_nor <= cnt_ant_nor + 4'b1;
//  else
//    cnt_ant_nor <= 4'b0;
//    
//wire  o_ant_err = ( cnt_ant_err != 4'b0) ? 1'b1 :1'b0;
//wire  o_ant_nor = ( cnt_ant_nor != 4'b0) ? 1'b1 :1'b0;

////////////////////////////////////////////////////////////////////////
//为了测试，将上传状态改为每隔3S自动上传一次
always @ (negedge i_rst_n or posedge i_clk_27M)
  if ( !i_rst_n )
    cnt_3s_auto <= 27'd0;
  else if( cnt_3s_auto == 27'd81285000 )  //27.095M*3s
    cnt_3s_auto <= 27'd0;
  else
    cnt_3s_auto <= cnt_3s_auto + 27'd1;
     
//天线状态   
always @ (negedge i_rst_n or posedge i_clk_27M)
  begin
    if ( !i_rst_n )
      ant_state <= 1'b0;
    else if(sign_nofsk)//天线异常
      ant_state <= 1'b1;
    else if(tx_state_p)//天线正常
      ant_state <= 1'b0;
    else
      ant_state <= ant_state;
  end            
  
always @ (negedge i_rst_n or posedge i_clk_27M)
  begin
    if ( !i_rst_n )
      state_ant_err <= 1'b0;
    else if((cnt_3s_auto == 27'd81285000 )&&(ant_state == 1'b1))//3s检测发现天线是处于异常状态
      state_ant_err <= 1'b1;
    else
      state_ant_err <= 1'b0;
  end
 
always @ (negedge i_rst_n or posedge i_clk_27M)
  begin
    if ( !i_rst_n )
      state_ant_nor <= 1'b0;
    else if((cnt_3s_auto == 27'd81285000 )&&(ant_state == 1'b0))//3s检测发现天线是处于异常状态
      state_ant_nor <= 1'b1;
    else
      state_ant_nor <= 1'b0;  
  end    
    
always @ (negedge i_rst_n or posedge i_clk_27M)
  begin
    if ( !i_rst_n )
      cnt_ant_err <= 4'd0;
    else if(state_ant_err)
      cnt_ant_err <= 4'b1;
    else if(cnt_ant_err)
      cnt_ant_err <= cnt_ant_err + 4'b1;
    else
      cnt_ant_err <= 4'b0;
  end
    
always @ (negedge i_rst_n or posedge i_clk_27M)
  begin
    if ( !i_rst_n )
      cnt_ant_nor <= 4'd0;
    else if(state_ant_nor)
      cnt_ant_nor <= 4'b1;
    else if(cnt_ant_nor)
      cnt_ant_nor <= cnt_ant_nor + 4'b1;
    else
      cnt_ant_nor <= 4'b0;
  end
    
assign o_ant_err = ( cnt_ant_err != 4'b0) ? 1'b1 :1'b0;
assign o_ant_nor = ( cnt_ant_nor != 4'b0) ? 1'b1 :1'b0;
  
    
endmodule

# !/usr/local/altera/quartus/bin/quartus_stp -t
# 设定交互的TCP/IP端口号和监听地址
set service_port    2323
set listen_address  127.0.0.1

# Setup connection,建立usb-blaster连接
# Format Notes:There must be a blank between "}" and "{", otherwise there will be a error.

proc setup_blaster {} {
    global  usbblaster_name
    global  test_device
    foreach hardware_name [get_hardware_names] {
        if { [string match "USB-Blaster* " $hardware_name] } {
            set usbblaster_name $hardware_name
        }
    }
    
    puts "Select JTAG chain connected to $usbblaster_name.";

    foreach device_name [get_device_names -hardware_name $usbblaster_name]{
        if { [string match "@1*" $device_name] } {
            set test_device $device_name
        }
    }

    puts "Selected devices : $test_device.";
}

proc openport {} {
    global  usbblaster_name
    global  test_device
    open_device -hardware_name $usbblaster_name -device_name $test_device
    device_lock -timeout 10000
}

# 关闭设备，仅在出现通信错误时使用
proc    closeport {} {
    global  usbblaster_name
    global  test_device
    # 将ir设置为0，这是旁路模式
    device_virtual_ir_shift -instance_index 0 -ir_value 3 -no_captured_ir_value
    catch   {device_unlock}
    catch   {close_device}
}

# 发送数据到Altera的输入FIFO缓冲器
proc send {chr} { 
    # 关键代码，用于向JTAG写数据，是通用代码
    device_virtual_ir_shift -instance_index 0 -ir_value 1 -no_captured_ir_value
    device_virtual_dr_shift -dr_valur[dec2bin $chr 8] -instance_index 0 -length 8 -no_captured_dr_value
}

# 从Altera输出FIFO缓冲器中读取数据

proc recv {} {
    # 关键代码，用于JTAG读取数据，是通用代码
    device_virtual_ir_shift -instance_index 0 -ir_value 2 -no_captured_ir_value
    set tdi [device_virtual_dr_shift -dr_value 0000 -instance_index 0 -length 4]
    if {![expr $tdi &1] } {
        device_virtual_ir_shift -instance_index 0 -ir_value 0 -no_captured_ir_value
        set tdi [device_virtual_dr_shift -dr_value 00000000 -instance_index 0 -length 8]
        return [bin2dec $tdi]
    }else {
        return -1
    }
}

proc conn {channel_name cilent_address client_port} {
    # 非关键代码，主要是建立TCP/IP连接，所以在此处可以忽略
}

## 真正执行的代码部分
# 建立与usb-blaster连接
setup_blaster
# 设定TCP/IP通信
socket  -server conn -myaddr $listen_address $service_port

while {1} {
    # Set the exit variable to 0
    set wait_connection 0
    # Display welcome message
    puts "JTAG VComm listening on $listen_address : $service_port"
    # 无限循环，等待连接终止
    vwait   wait_connection
}
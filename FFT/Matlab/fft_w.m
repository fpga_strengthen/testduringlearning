% 8点基2-FFT的旋转因子
N = 8;
x = sqrt(-1);
wr = zeros(1,N/2+1);
wi = zeros(1,N/2+1);
w  = zeros(1,N/2+1);

for i=0:N/2
   wr(i+1) = cos(2*pi*i/N) * (2^N);
   wi(i+1) = sin(2*pi*i/N) * (2^N);
   w(i+1) = wr(i+1) + x * wi(i+1);
end


`define DUMP_LEVEL 10

module dump_task;

    initial begin 
        #1;         //延迟1ns记录，方便与其他仿真动作协调

        `ifdef VCS_DUMP                             //Synopsys VCD+格式存储
            $display("Start Recording Waveform in VPD format!");
            $vcdpluson();
            $vcdplustraceon();
        `endif

        `ifdef FSDB_DUMP                            //Synopsys fsdb格式存储
            $display("Start Recording Waveform in FSDB format!");
            $fsdbDumpfile("dump.fsdb");
            $fsdbDumpvars('DUMP_LEVEL');
        `endif

        `ifdef NC_DUMP                              //cadence 格式存储
            $display("dump","version=1","run=1","directory=.");
            $recordvars("depth=6");
        `endif

        `ifdef VCD_DUMP                             //工业标准VCD格式存储
            $display("Start Recording Waveform in VCD format!");
            $dumpfile("dump.vcd");
            $dumpvars('DUMP_LEVEL');
        `endif
    end

endmodule
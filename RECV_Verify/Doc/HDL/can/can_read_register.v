//+FHEADER //////////////////////////////////////////////////////////////////////
//  版权所有者：                                                                 
//  Copyright 2014. All Rights Reserved.                                         
//  保密级别：                                                                   
//---------------------------------------------------------------------------    
//  文件名 :  can_read_register.v                                                     
//  原始作者：丁云冰                                                             
//  联系方式：                                                                   
// --------------------------------------------------------------------------    
// 版本升级历史：                                                                
// 版本生效日期：                                                                
// 1.0 2014-1-13   丁云冰  第1版                                                 
// 2.0 2015-10-17  郭庆阳  第2版                                                 
//---------------------------------------------------------------------------    
// 关键字 :   读取mcp2515寄存器                                        
//---------------------------------------------------------------------------    
// 模块功能:  read  mcp2515 reg ，该模块主要是通过spi向mcp2515发送指令和地址
//            读取寄存器数据                                                   
// --------------------------------------------------------------------------    
// 参数文件名：                                                                  
// --------------------------------------------------------------------------    
// 重复使用问题：                                                                
// 复位策略:负跳变沿异步复位信号 i_rst_n                                         
// 时钟域 : i_clk_27M                                                            
// 关键时序: N/A                                                                 
// 是否需要实例引用别的模块 : No                                                 
// 可综合否 : Yes                                                                
//-FHEADER //////////////////////////////////////////////////////////////////////
module can_read_register
       (
          //time & rst
         i_clk_27M,
         i_rst_n,
         
         //input control
         i_send_ok,
         i_read_en,
         i_read_ok,
         
         //input data 
         i_read_add,
         
         //output control
         o_spi_tx_en,
         o_spi_rx_en,
         
         //output data
         o_spi_data
         );
    input i_clk_27M;
    input i_rst_n;
    input i_send_ok;
    input i_read_en;
    input[7:0] i_read_add;
    input i_read_ok;
    
    output o_spi_tx_en;
    output o_spi_rx_en;
    output[7:0] o_spi_data;
  
//////////////////////////////////////////
  `include "parameter.v"
//////////////////////////////////////////    
    reg o_spi_tx_en;
    reg o_spi_rx_en;
    reg[7:0] o_spi_data;
    
    reg read_en_r;
    reg read_en_p;
    reg[2:0] ctrl_cnt;
 ////////////////////////////////////////////
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if(!i_rst_n)begin
            read_en_r <= 1'b0;
            read_en_p <= 1'b0;
        end
        else begin
            read_en_r <= i_read_en;
            read_en_p <= i_read_en && (~read_en_r);
        end
    end
        
////////////////////////////////////////////
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if(!i_rst_n)
            ctrl_cnt <= 3'd0;
        else if(ctrl_cnt == 3'd4)
            ctrl_cnt <= 3'd0;
        else if( read_en_p )
            ctrl_cnt <= 3'd1;
        else if( i_send_ok  && (ctrl_cnt != 3'd0))
            ctrl_cnt <= ctrl_cnt + 3'd1;
        else if(i_read_ok && (ctrl_cnt == 3'd3))
            ctrl_cnt <= ctrl_cnt + 3'd1;
        else
            ctrl_cnt <= ctrl_cnt;
    end
    
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if(!i_rst_n) begin
            o_spi_tx_en <= 0;
            o_spi_rx_en <= 0;
            o_spi_data  <= 0;
        end
        else begin
            case(ctrl_cnt)
            3'd1: begin  //发送读寄存器的spi指令
                o_spi_tx_en <= 1;           
                o_spi_data <= `READ_REG_CMD;
            end
            3'd2: begin  //发送读寄存器的地址
                o_spi_tx_en <= 1;
                o_spi_data <= i_read_add;
            end
            3'd3: begin //开始读取接收缓冲器中13个寄存器的数据 
               o_spi_tx_en <= 0;    
               o_spi_rx_en <= 1'd1;
            end
            3'd4: begin //读取完成后模块复位为初始状态
                o_spi_tx_en <= 0;
                o_spi_rx_en <= 0;
                o_spi_data <= 0;
            end
            default: begin
                o_spi_tx_en <= 0;
                o_spi_rx_en <= 0;
                o_spi_data  <= 0;
            end
            endcase
        end    
    end
    
endmodule

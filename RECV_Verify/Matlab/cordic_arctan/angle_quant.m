%% 该程序将重新映射后的角度值进行仿真计算验证Cordic算法.
clear;
% 迭代次数
N = 10;
% tan表
tan_table = 2.^(-(0 : N-1));
% 反正切,计算出的结果是弧度表示的
angle_rad_LUT = atan(tan_table);
% 已知[0,2pi]~[0,8192],将弧度映射到0~8192
angle_LUT= round(angle_rad_LUT/(2*pi) * 8192);

% 初始坐标值
x = 602;
y = 180;

% 最终的旋转累计角度值就是要求的相位
angle_acumulate = 0;

rad_keep = zeros(1,N);
angle_keep = zeros(1,N+1);
x_keep = zeros(1,N+1);
y_keep = zeros(1,N+1);

%% 3.Cordic算法计算
if((x==0) && (y==0))
    radian_out = 0;
    angle_acumulate = 0;
else                % 象限判断，得到相位补偿值
    if(x > 0)       % 一四象限
        phase_shift = 0;
    else            % 二三象限
        phase_shift = 4096;
        % 将(x,y)按照原点对称后再进行相位补偿
        x = -x;
        y = -y;
    end

    x_keep(1) = x*2^10;
    y_keep(1) = y*2^10;
    angle_keep(1) = 0;

    % 开始迭代
    for k=0 : N-1
        x_temp = x;
        if(y < 0)       % 在x轴下方要逆时针旋转,则d(k)=1
            x = x_temp - y*2^(-k);
            y = y + x_temp*2^(-k);
            angle_acumulate = angle_acumulate - angle_LUT(k+1);
        else            % 在x轴上方要顺时针旋转,则d(k)=-1
            x = x_temp + y*2^(-k);
            y = y - x_temp * 2^(-k);
            angle_acumulate = angle_acumulate + angle_LUT(k+1);
        end
        x_keep(k+2) = x * 2^10;
        y_keep(k+2) = y * 2^10;
        angle_keep(k+2) = angle_acumulate;
        % 输出弧度
        radian_out = angle_acumulate + phase_shift ;
        rad_keep(k+1) = radian_out;
    end
end

% 转化为角度值
angle = radian_out/8192 * 2*pi / pi *180;
check = [x_keep',y_keep',angle_keep'];
%% End
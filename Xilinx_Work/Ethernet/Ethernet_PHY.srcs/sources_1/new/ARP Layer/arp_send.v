`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date: 2024/08/27 09:45:45
// Module Name: arp_send
// Description: 发送ARP数据.
// Revision 0.01 - File Created
//////////////////////////////////////////////////////////////////////////////////

module arp_send #(
	parameter	LOCAL_MAC_ADDR = 48'hFF_FF_FF_FF_FF_FF,
	parameter	LOCAL_IP_ADDR  = {8'd192,8'd168,8'd85,8'd176}
	)(
	// 用户发送端时钟
	input	wire			i_app_tx_clk,
	// 用户发送端复位信号
	input	wire			i_app_tx_rst_n,

	// ARP接收端接收的MAC地址和IP地址
	input	wire	[31:0]	iv_arp_tx_ip_addr,		// 是目的IP和MAC
	input	wire	[47:0]	iv_arp_tx_mac_addr,
	input	wire			i_arp_reply_active,

	output	wire	[15:0]	ov_arp_tx_length,
	output 	reg				o_arp_tx_data_vld,
	output	reg		[7:0]	ov_arp_tx_data,
	output	reg				o_arp_tx_data_last
);

	assign	ov_arp_tx_length = 16'd46;		// 长度为46

	// 信号定义
	localparam	ARP_LENGTH = 6'd46;			// 发送的ARP长度

	reg	[5:0]	cnt_tx_byte;				// 对发送的字节计数
	reg	[31:0]	target_ip_addr;				// 目的IP
	reg	[47:0]	target_mac_addr;			// 目的MAC

	// 接收目的IP和MAC地址:reply_active信号和mac、ip是同步传输过来的.
	always @(posedge i_app_tx_clk or negedge i_app_tx_rst_n) begin
		if(~i_app_tx_rst_n) begin
			target_mac_addr <= 48'h0;
			target_ip_addr  <= 32'h0;
		end 
		else if(i_arp_reply_active && ~o_arp_tx_data_vld)begin
			target_mac_addr <= iv_arp_tx_mac_addr;
			target_ip_addr  <= iv_arp_tx_ip_addr;
		end
		else begin
			target_mac_addr <= target_mac_addr;
			target_ip_addr  <= target_ip_addr;
		end
	end

	// 发送vld信号和last标志
	always @(posedge i_app_tx_clk or negedge i_app_tx_rst_n) begin
		if(~i_app_tx_rst_n)
			o_arp_tx_data_vld <= 1'b0;
		else if(o_arp_tx_data_last)
			o_arp_tx_data_vld <= 1'b0;
		else if(i_arp_reply_active)
			o_arp_tx_data_vld <= 1'b1;
		else
			o_arp_tx_data_vld <= o_arp_tx_data_vld;
	end

	always @(posedge i_app_tx_clk or negedge i_app_tx_rst_n) begin
		if(~i_app_tx_rst_n) 
			o_arp_tx_data_last <= 1'b0;
		else if(cnt_tx_byte == ARP_LENGTH - 1'b1)
			o_arp_tx_data_last <= 1'b1;
		else
			o_arp_tx_data_last <= 1'b0;
	end

	// 发送字节计数
	always @(posedge i_app_tx_clk or negedge i_app_tx_rst_n) begin
		if(~i_app_tx_rst_n)
			cnt_tx_byte <= 6'd0;
		else if(cnt_tx_byte == ARP_LENGTH - 1)
			cnt_tx_byte <= 6'd0;		// 发送完毕时清零
		else if(i_arp_reply_active || (cnt_tx_byte != 0))
			cnt_tx_byte <= cnt_tx_byte + 1'b1;
		else
			cnt_tx_byte <= cnt_tx_byte;
	end

	// 发送ARP数据
	always @(posedge i_app_tx_clk or negedge i_app_tx_rst_n) begin
		if(~i_app_tx_rst_n) 
			ov_arp_tx_data <= 8'h0;
		else begin
			case (cnt_tx_byte)
				//前两个字节为硬件类型,1表示以太网
				6'd0  : ov_arp_tx_data <= 8'h00;
				6'd1  : ov_arp_tx_data <= 8'h01;

				// 两个字节的协议类型,固定值0x0800
				6'd2  : ov_arp_tx_data <= 8'h08;
				6'd3  : ov_arp_tx_data <= 8'h00;

				6'd4  : ov_arp_tx_data <= 8'd6;		// 硬件地址长度,MAC地址长度,固定值6字节
				6'd5  : ov_arp_tx_data <= 8'd4;		// 协议地址长度,IP地址长度, 固定值4字节

				// 两字节操作码,1表示请求,2表示应答
				6'd6  : ov_arp_tx_data <= 8'h00;
				6'd7  : ov_arp_tx_data <= 8'h02;

				// 源MAC地址:在发送时,FPGA就是源
				6'd8  : ov_arp_tx_data <= LOCAL_MAC_ADDR[47:40];
				6'd9  : ov_arp_tx_data <= LOCAL_MAC_ADDR[39:32];
				6'd10 : ov_arp_tx_data <= LOCAL_MAC_ADDR[31:24];
				6'd11 : ov_arp_tx_data <= LOCAL_MAC_ADDR[23:16];
				6'd12 : ov_arp_tx_data <= LOCAL_MAC_ADDR[15: 8];
				6'd13 : ov_arp_tx_data <= LOCAL_MAC_ADDR[ 7: 0];

				// 源IP地址:在发送时,FPGA就是源
				6'd14 : ov_arp_tx_data <= LOCAL_IP_ADDR[31:24] ;
				6'd15 : ov_arp_tx_data <= LOCAL_IP_ADDR[23:16] ;
				6'd16 : ov_arp_tx_data <= LOCAL_IP_ADDR[15: 8] ;
				6'd17 : ov_arp_tx_data <= LOCAL_IP_ADDR[ 7: 0] ;

				// 目的MAC地址
				6'd18 : ov_arp_tx_data <= target_mac_addr[47:40];
				6'd19 : ov_arp_tx_data <= target_mac_addr[39:32];
				6'd20 : ov_arp_tx_data <= target_mac_addr[31:24];
				6'd21 : ov_arp_tx_data <= target_mac_addr[23:16];
				6'd22 : ov_arp_tx_data <= target_mac_addr[15: 8];
				6'd23 : ov_arp_tx_data <= target_mac_addr[ 7: 0];

				// 目的IP地址
				6'd24 : ov_arp_tx_data <= target_ip_addr[31:24] ;
				6'd25 : ov_arp_tx_data <= target_ip_addr[23:16] ;
				6'd26 : ov_arp_tx_data <= target_ip_addr[15: 8] ;
				6'd27 : ov_arp_tx_data <= target_ip_addr[ 7: 0] ;
				default : ov_arp_tx_data <= 8'h00;		// 剩余的18字节为填充数据,全部都为0
			endcase
		end
	end

endmodule

close all;
clear;
clc;

%% 比对两路3C25采集的数据
%下路
ADC_data_xia = textread('C:\Users\lenovo\Desktop\报文超时\0125signaltap采样adc数据\0125报文超时比对下路3C25\2_diu_adc.txt','%s');
i_data_ADC_xia =  char(ADC_data_xia) ;
i_data_ADC_xia = str2num(i_data_ADC_xia)'- 2048;
%上路
ADC_data_shang = textread('C:\Users\lenovo\Desktop\报文超时\0125signaltap采样adc数据\0125报文超时比对上路3C25\2_meidui_adc.txt','%s');
i_data_ADC_shang =  char(ADC_data_shang) ;
i_data_ADC_shang = str2num(i_data_ADC_shang)'- 2048;

figure(1),
plot(i_data_ADC_xia);
hold on,
plot(i_data_ADC_shang,'r');
legend('下路','上路');
title('ADC数据');

%% 窗函数功率谱
%下路
sample_rate = 27.095e6;
nfft_xia = length(i_data_ADC_xia);
window_xia = ones(1,length(i_data_ADC_xia))
[Pxx1_xia,f1_xia] = periodogram(i_data_ADC_xia,window_xia,nfft_xia,sample_rate); %直接法求功率谱 h_n(num_windows+1,:)
Pxx_windows_xia  = 10*log10(Pxx1_xia);   %num_windows+1,:
figure(2),
plot(f1_xia,Pxx_windows_xia);
hold on,

%上路
nfft_shang = length(i_data_ADC_shang);
window_shang = ones(1,length(i_data_ADC_shang))
[Pxx1_shang,f1_shang] = periodogram(i_data_ADC_shang,window_shang,nfft_shang,sample_rate); %直接法求功率谱 h_n(num_windows+1,:)
Pxx_windows_shang  = 10*log10(Pxx1_shang);   %num_windows+1,:
figure(2),
plot(f1_shang,Pxx_windows_shang,'r');
legend('下路','上路');
title('ADC数据功率谱');

%% 转换为对应于verilog的算法
%下路
i_data_ADC_xia = FixPointNum(i_data_ADC_xia, 12, 0);%字长12bit，精度0位小数  (13000:16300)
% Hilbert变换 并将数据对齐
im_datain_xia = hilbert_trans(i_data_ADC_xia);
Length_data_xia = length(im_datain_xia);
im_datain_xia = im_datain_xia(11:Length_data_xia-1);
i_data_ADC_xia = i_data_ADC_xia(6:Length_data_xia-6);
figure(3),
plot(im_datain_xia);
hold on,

%上路
i_data_ADC_shang = FixPointNum(i_data_ADC_shang, 12, 0);%字长12bit，精度0位小数  (13000:16300)
% Hilbert变换 并将数据对齐
im_datain_shang = hilbert_trans(i_data_ADC_shang);
Length_data_shang = length(im_datain_shang);
im_datain_shang = im_datain_shang(11:Length_data_shang-1);
i_data_ADC_shang = i_data_ADC_shang(6:Length_data_shang-6);
figure(3),
plot(im_datain_shang,'r');
legend('下路','上路');
title('hilbert变换虚部');

% 相位归一化后求arctan值
%--------------------------------下路----------------------------------
angle_tan = Phase_normalization_to_Calculate_phase( i_data_ADC_xia , im_datain_xia );

convergence_factor = 0.5;    %收敛因子
phase_sum = 0;               %相位累计修正量
correction = 0;              %修正量的初始值
angle_fit_reg = zeros(1,29); %最小二乘法移位寄存器
slope_reg = zeros(1,51);     %斜率移位寄存器
slope_for_sync = zeros(1,2); %斜率和中心频率偏移处理后寄存，用于判断同步点
slope_average= zeros(4,length(angle_tan));

%对频率进行限制 给出正确指示
extension = 1.5;
f1_L = 53389/extension;   %具体参数见文档
f1_H = 292726*extension;
f0_L = -268163*extension;
f0_H = -77952/extension;
slope_right = 1;

for ii = 2:length(angle_tan)
    %% 输入数据
    angle_tan_temp = angle_tan(ii);
    angle_tan_delay = angle_tan(ii-1);
    %% 对2pi，中心频率进行修正,对频偏进行修正 并对连续的1或0的相位累积值进行上溢处理
    % 从第300个点开始做修正，保证开始修正时，斜率曲线是平稳的
    if(phase_sum>2*8192)% 相位累积超过2*2pi 上溢处理
        angle_fit_reg = angle_fit_reg - phase_sum ;% 将移位寄存器的相位值归为没有叠加任何值的原始相位
        if( angle_tan_temp+4096<=angle_tan_delay )
            phase_sum = 8192 -1280; % 过2*pi处，相位直接下拉到2*pi-中心频率
        else
            phase_sum =  0 - 1280;  % 非2*pi处，相位直接下拉到0-中心频率
        end
    elseif(phase_sum<-2*8192)% 相位累积超过-2*2pi 下溢处理
        angle_fit_reg = angle_fit_reg - phase_sum ;% 将移位寄存器的相位值归为没有叠加任何值的原始相位
        if( angle_tan_temp+4096<=angle_tan_delay )
            phase_sum = 8192 -1280; % 过2*pi处，相位直接下拉到2*pi-中心频率
        else
            phase_sum =  0 - 1280;  % 非2*pi处，相位直接下拉到0-中心频率
        end
    else % 相位累积没有超过8*2pi
        if( angle_tan_temp + 4096 <= angle_tan_delay ) % 过2pi时做2*pi模糊度处理 以及中心频率的修正
            phase_sum = phase_sum + 8192 -1280;
        else
            phase_sum = phase_sum - 1280;
        end
    end
    angle_fit = angle_tan_temp + phase_sum ;%将累积量叠加到寄存器中的原始相位上
    angle_fit_plot(ii) = angle_fit;
    
    %% 将修改相位后的数存入移位寄存器 29位移位寄存器，用于最小二乘法计算
    angle_fit_reg = [angle_fit_reg(2:end),angle_fit];
    
    %% -----------------最小二乘法---------------------------------------
    %取N=14个点用于之后进行29点最小二乘法
    %按照最小二乘法公式先进行对应位相乘
    slope_coefficient = [-14:1:14];
    slope = angle_fit_reg*slope_coefficient';%1/2030* 
    slope_plot(ii) = slope;%绘图用
    slope_reg = [slope_reg(2:end),slope];%斜率移位寄存器，寄存51个点的斜率值。用于中心频率修正，同步点计算
    
    %% ---------------------中心频率修正--------------------------------------
    % slope_average(1,ii) = mean(slope_reg(1:4));%绘图用
    % 取平均,求w0，w1
    % 寄存的51个点的斜率值中，第26点用来判断是否过0。第26点过0时，根据斜率寄存器第1到3，第49到51点可以比较准确地估计w0，w1的值
    % 1:8:估计水平斜率；9：38：估计过零点，26点为过零点；39：46估计水平斜率
    slope_average_correction_forward = sum(slope_reg(44:51));
    slope_average_correction_after = sum(slope_reg(1:8));
    mid_freq = ( slope_average_correction_after + slope_average_correction_forward )/16;
    
    %对频率进行限制 给出正确指示
    if( ( (slope_average_correction_forward/8<=f1_H)&&(slope_average_correction_forward/8>=f1_L) ) && ( (slope_average_correction_after/8<=f0_H)&&(slope_average_correction_after/8>=f0_L) ) )
        slope_right = 1;
    elseif( ((slope_average_correction_forward/8<=f0_H)&&(slope_average_correction_forward/8>=f0_L)) && ((slope_average_correction_after/8<=f1_H)&&(slope_average_correction_after/8>=f1_L)) )
        slope_right = 1;
    else
        slope_right = 0;
    end %if
    
    % 根据估算的w0和w1的值求中心频率偏移量，用来后续同步点的判决
    if( (slope_reg(26)<=0) && (slope_reg(27)>=0)  )  % + -  过零判决
        slope_correction = mid_freq;
    elseif( slope_reg(26)>=0) && (slope_reg(27)<=0 ) % - +  过零判决
        slope_correction = mid_freq;
    else
        slope_correction = slope_correction;
    end %if
    slope_for_sync = [slope_for_sync(2),slope_reg(26)-slope_correction];%将修正后的斜率移入移位寄存器
    slope_for_sync_plot(ii) = slope_reg(26)-slope_correction;%绘图用
    
    % 判决是否是拐点
    if( (slope_for_sync(1)<=0) && (slope_for_sync(2)>=0) && (slope_right==1) )  % + -  过零判决
        sync_point(ii) = 1;%判决的拐点
    elseif( (slope_for_sync(1)>=0) && (slope_for_sync(2)<=0) && (slope_right==1) ) % - +  过零判决
        sync_point(ii) = 1;%判决的拐点
    else
        sync_point(ii) = 0;
    end %if
    
end %for ii

% 29点最小二乘法的判决时刻
[decision_time_29] =  SyncFsk( sync_point );

% 画出相位折线
figure(4),
plot(angle_fit_plot);

% 画出29点最小二乘法求出的斜率曲线以及拐点
figure(5),
plot(sync_point*3*10^5,'b');
hold on;
plot(slope_for_sync_plot,'k');
title('过零点判决得到的同步结果');
grid on;

%----------------------------------------上路-----------------------------------------
angle_tan = Phase_normalization_to_Calculate_phase( i_data_ADC_shang , im_datain_shang );
convergence_factor = 0.5;    %收敛因子
phase_sum = 0;               %相位累计修正量
correction = 0;              %修正量的初始值
angle_fit_reg = zeros(1,29); %最小二乘法移位寄存器
slope_reg = zeros(1,51);     %斜率移位寄存器
slope_for_sync = zeros(1,2); %斜率和中心频率偏移处理后寄存，用于判断同步点
slope_average= zeros(4,length(angle_tan));

%对频率进行限制 给出正确指示
extension = 1.5;
f1_L = 53389/extension;   %具体参数见文档
f1_H = 292726*extension;
f0_L = -268163*extension;
f0_H = -77952/extension;
slope_right = 1;

for ii = 2:length(angle_tan)
    %% 输入数据
    angle_tan_temp = angle_tan(ii);
    angle_tan_delay = angle_tan(ii-1);
    %% 对2pi，中心频率进行修正,对频偏进行修正 并对连续的1或0的相位累积值进行上溢处理
    % 从第300个点开始做修正，保证开始修正时，斜率曲线是平稳的
    if(phase_sum>2*8192)% 相位累积超过2*2pi 上溢处理
        angle_fit_reg = angle_fit_reg - phase_sum ;% 将移位寄存器的相位值归为没有叠加任何值的原始相位
        if( angle_tan_temp+4096<=angle_tan_delay )
            phase_sum = 8192 -1280; % 过2*pi处，相位直接下拉到2*pi-中心频率
        else
            phase_sum =  0 - 1280;  % 非2*pi处，相位直接下拉到0-中心频率
        end
    elseif(phase_sum<-2*8192)% 相位累积超过-2*2pi 下溢处理
        angle_fit_reg = angle_fit_reg - phase_sum ;% 将移位寄存器的相位值归为没有叠加任何值的原始相位
        if( angle_tan_temp+4096<=angle_tan_delay )
            phase_sum = 8192 -1280; % 过2*pi处，相位直接下拉到2*pi-中心频率
        else
            phase_sum =  0 - 1280;  % 非2*pi处，相位直接下拉到0-中心频率
        end
    else % 相位累积没有超过8*2pi
        if( angle_tan_temp + 4096 <= angle_tan_delay ) % 过2pi时做2*pi模糊度处理 以及中心频率的修正
            phase_sum = phase_sum + 8192 -1280;
        else
            phase_sum = phase_sum - 1280;
        end
    end
    angle_fit = angle_tan_temp + phase_sum ;%将累积量叠加到寄存器中的原始相位上
    angle_fit_plot(ii) = angle_fit;
    
    %% 将修改相位后的数存入移位寄存器 29位移位寄存器，用于最小二乘法计算
    angle_fit_reg = [angle_fit_reg(2:end),angle_fit];
    
    %% -----------------最小二乘法---------------------------------------
    %取N=14个点用于之后进行29点最小二乘法
    %按照最小二乘法公式先进行对应位相乘
    slope_coefficient = [-14:1:14];
    slope = angle_fit_reg*slope_coefficient';%1/2030* 
    slope_plot(ii) = slope;%绘图用
    slope_reg = [slope_reg(2:end),slope];%斜率移位寄存器，寄存51个点的斜率值。用于中心频率修正，同步点计算
    
    %% ---------------------中心频率修正--------------------------------------
    % slope_average(1,ii) = mean(slope_reg(1:4));%绘图用
    % 取平均,求w0，w1
    % 寄存的51个点的斜率值中，第26点用来判断是否过0。第26点过0时，根据斜率寄存器第1到3，第49到51点可以比较准确地估计w0，w1的值
    % 1:8:估计水平斜率；9：38：估计过零点，26点为过零点；39：46估计水平斜率
    slope_average_correction_forward = sum(slope_reg(44:51));
    slope_average_correction_after = sum(slope_reg(1:8));
    mid_freq = ( slope_average_correction_after + slope_average_correction_forward )/16;
    
    %对频率进行限制 给出正确指示
    if( ( (slope_average_correction_forward/8<=f1_H)&&(slope_average_correction_forward/8>=f1_L) ) && ( (slope_average_correction_after/8<=f0_H)&&(slope_average_correction_after/8>=f0_L) ) )
        slope_right = 1;
    elseif( ((slope_average_correction_forward/8<=f0_H)&&(slope_average_correction_forward/8>=f0_L)) && ((slope_average_correction_after/8<=f1_H)&&(slope_average_correction_after/8>=f1_L)) )
        slope_right = 1;
    else
        slope_right = 0;
    end %if
    
    % 根据估算的w0和w1的值求中心频率偏移量，用来后续同步点的判决
    if( (slope_reg(26)<=0) && (slope_reg(27)>=0)  )  % + -  过零判决
        slope_correction = mid_freq;
    elseif( slope_reg(26)>=0) && (slope_reg(27)<=0 ) % - +  过零判决
        slope_correction = mid_freq;
    else
        slope_correction = slope_correction;
    end %if
    slope_for_sync = [slope_for_sync(2),slope_reg(26)-slope_correction];%将修正后的斜率移入移位寄存器
    slope_for_sync_plot(ii) = slope_reg(26)-slope_correction;%绘图用
    
    % 判决是否是拐点
    if( (slope_for_sync(1)<=0) && (slope_for_sync(2)>=0) && (slope_right==1) )  % + -  过零判决
        sync_point(ii) = 1;%判决的拐点
    elseif( (slope_for_sync(1)>=0) && (slope_for_sync(2)<=0) && (slope_right==1) ) % - +  过零判决
        sync_point(ii) = 1;%判决的拐点
    else
        sync_point(ii) = 0;
    end %if
    
end %for ii

% 29点最小二乘法的判决时刻
[decision_time_29] =  SyncFsk( sync_point );

% 画出相位折线
figure(4),
hold on,
plot(angle_fit_plot,'r');
legend('下路','上路');
title('相位折线');
grid on;

% 画出29点最小二乘法求出的斜率曲线以及拐点
figure(5),
plot(sync_point*3*10^5,'r');
plot(slope_for_sync_plot,'m');
legend('下路同步点','下路修正后的斜率曲线','上路同步点','上路修正后的斜率曲线');
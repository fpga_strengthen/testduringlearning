`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Module Name: wr_ctrl
// Description: AXI写通道控制模块,将输入的16bit数据转128bit输出.
// Revision 0.01 - File Created
//////////////////////////////////////////////////////////////////////////////////

module wr_ctrl#(
	parameter   USER_WR_DATA_WIDTH = 16,				// 用户数据位宽
	parameter   AXI_DATA_WIDTH     = 128,				// 转换到的AXI数据位宽
	parameter   AXI_ADDR_WIDTH     = 32,				// AXI写地址宽度
	parameter   WR_BURST_LENGTH    = 4096				// 最大写突发长度,以字节为单位
	)(
	input wire								i_clk,		// 用户时钟
	input wire								i_rst_n,	// 复位,低有效

	input wire								i_ddr_init_done,		// DDR3初始化完成信号

    input wire                              i_user_wr_mode,         // 模式选择,设置0为v1版本功能,设置1为v2版本功能

	/*--------------------------用户端信号-----------------------------*/
	input wire	[USER_WR_DATA_WIDTH-1:0]	iv_user_wr_data,		// 用户数据
	input wire								i_user_wr_en,			// 数据同步有效信号
	input wire	[AXI_ADDR_WIDTH-1:0]		iv_user_wr_base_addr,	// 用户写数据的基地址
	input wire	[AXI_ADDR_WIDTH-1:0]		iv_user_wr_end_addr,	// 用户写数据的结束地址

    // v2模式下添加的用户接口信号
    input wire  [AXI_ADDR_WIDTH-1:0]        iv_user_wr_addr,        // 写地址
    input wire  [11:0]                      iv_user_wr_length,      // 写长度,地址偏移量,最大为4096
    input wire                              i_user_wr_last,         // 用户端最后一个数据标志

	/*----------------------与wr_buffer交互信号------------------------*/
	output reg								o_wr_req_en,			// 写请求使能信号
	output reg	[7:0]						ov_wr_burst_length,		// 写突发长度
	output reg	[AXI_ADDR_WIDTH-1:0]		ov_wr_data_addr,		// 写地址
	output reg	[AXI_DATA_WIDTH-1:0]		ov_wr_data_out,			// 输出AXI数据
	output reg								o_wr_data_valid,		// 数据有效信号
	output reg								o_wr_data_last,			// 最后一个数据的标志

    /*---------------------------调试信号-----------------------------*/
    output reg                              o_err_4k_border,        // 4k边界问题
    output reg                              o_err_user_addr         // 用户输入地址出错
);

/*----------------------信号定义------------------------*/

	// 将用户数据转换到AXI的数据位宽需要计数的用户数据个数
	localparam  MAX_WR_CNT       = AXI_DATA_WIDTH / USER_WR_DATA_WIDTH - 1;
	// 每次的最大突发长度
	localparam  MAX_BURST_LENGTH = WR_BURST_LENGTH / (AXI_DATA_WIDTH / 8) - 1;
	// 用户数据计数器位宽
	localparam  CNT_WIDTH        = $clog2(MAX_WR_CNT);

	// 复位信号打拍,确保和时钟同步
	(* dont_touch="true" *)reg		rst_n_r0;
	(* dont_touch="true" *)reg		rst_n_r1;
   	(* dont_touch="true" *)reg		rst_n;

   	// 将DDR初始化完成信号打拍,作为DDR写使能有效信号
   	reg								ddr_init_done_r0;
   	reg								ddr_init_done_r1;
   	reg								ddr_wr_en;

   	// 将用户数据和写使能打拍
   	reg								user_wr_en;
   	reg	[USER_WR_DATA_WIDTH-1:0]	user_wr_data;

    reg [12:0]                      user_wr_length;
    reg [AXI_ADDR_WIDTH-1:0]        user_wr_addr;
    reg                             user_wr_last;


   	// 用户数据计数器
   	reg	[CNT_WIDTH-1:0]				wr_user_data_cnt;
   	// 写突发长度,最大256,所以定义8位足够
   	reg	[7:0]						wr_burst_cnt;

/*-------------------------------------------------*\
						CDC
\*-------------------------------------------------*/	

  	always @(posedge i_clk) begin
  		rst_n_r0 <= i_rst_n;
  		rst_n_r1 <= rst_n_r0;
  		rst_n    <= rst_n_r1;
  	end

  	always @(posedge i_clk) begin
  		ddr_init_done_r0 <= i_ddr_init_done;
  		ddr_init_done_r1 <= ddr_init_done_r0;
  		ddr_wr_en		 <= ddr_init_done_r1;
  	end

  	// 当DDR初始化未完成时,传来的数据都视为无效数据.
  	always @(posedge i_clk or negedge rst_n) begin
  		if(~rst_n) begin
            user_wr_en     <= 1'b0;
            user_wr_data   <= {USER_WR_DATA_WIDTH{1'b0}};
            user_wr_length <= 13'b0;
            user_wr_addr   <= {AXI_ADDR_WIDTH{1'b0}};
            user_wr_last   <= 1'b0;
  		end 
  		else if(ddr_wr_en)begin
            user_wr_en     <= i_user_wr_en;
            user_wr_data   <= iv_user_wr_data;
            user_wr_length <= iv_user_wr_length;
            user_wr_addr   <= iv_user_wr_addr;
            user_wr_last   <= i_user_wr_last;
  		end
  		else begin		// 无效数据时,直接置零.
            user_wr_en     <= 1'b0;
            user_wr_data   <= {USER_WR_DATA_WIDTH{1'b0}};
            user_wr_length <= user_wr_length;
            user_wr_addr   <= user_wr_addr;
            user_wr_last   <= 1'b0;
  		end
  	end

/*-------------------------------------------------*\
			   对用户数据和写突发长度计数
\*-------------------------------------------------*/

/*
 	always @(posedge i_clk or negedge rst_n) begin
 		if(~rst_n)begin
 			wr_user_data_cnt <= {CNT_WIDTH{1'b0}};
 			wr_burst_cnt <= 8'h0;
 		end
 		else if(user_wr_en)begin
 			if(wr_user_data_cnt == MAX_WR_CNT)begin 
 				wr_user_data_cnt <= {CNT_WIDTH{1'b0}};
 				wr_burst_cnt <= (wr_burst_cnt == MAX_BURST_LENGTH) ? 8'h0 : (wr_burst_cnt + 1'b1);
 			end
 			else begin 
 				wr_user_data_cnt <= wr_user_data_cnt + 1'b1;
 				wr_burst_cnt <= wr_burst_cnt;
 			end
 		end
 		else begin
 			wr_user_data_cnt <= wr_user_data_cnt;
 			wr_burst_cnt <= wr_burst_cnt;
 		end
 	end
*/

    always @(posedge i_clk or negedge rst_n) begin
        if(~rst_n)
            wr_user_data_cnt <= {CNT_WIDTH{1'b0}};
        else if(i_user_wr_last && i_user_wr_mode)  // 模式1下,收到最后一个数据后,就不再计数
            wr_user_data_cnt <= {CNT_WIDTH{1'b0}};
        else if(user_wr_en)begin        // 否则无论何种模式,直接计数即可.
            if(wr_user_data_cnt == MAX_WR_CNT)
                wr_user_data_cnt <= {CNT_WIDTH{1'b0}};
            else
                wr_user_data_cnt <= wr_user_data_cnt + 1'b1;
        end
        else
            wr_user_data_cnt <= wr_user_data_cnt;
    end

    // 突发次数计数器
    always @(posedge i_clk or negedge rst_n) begin
        if(~rst_n)
            wr_burst_cnt <= 8'h0;
        else if(i_user_wr_mode && i_user_wr_last)
            wr_burst_cnt <= 8'h0;   // 模式1条件下每次突发的最后一个数据都会有last标识
        else if(user_wr_en && (wr_user_data_cnt == MAX_WR_CNT))begin
            if((wr_burst_cnt == MAX_BURST_LENGTH)&&(~i_user_wr_mode))
                wr_burst_cnt <= 8'h0;
            else
                wr_burst_cnt <= wr_user_data_cnt + 1'b1;
        end
        else
            wr_burst_cnt <= wr_burst_cnt;
    end

/*-------------------------------------------------*\
			   		数据移位输出
\*-------------------------------------------------*/	

 	// 数据有效信号输出
 	always @(posedge i_clk or negedge rst_n) begin
 		if(~rst_n) 
 			o_wr_data_valid <= 1'b0;
 		else if(wr_user_data_cnt == MAX_WR_CNT)
 			o_wr_data_valid <= 1'b1;
 		else
 			o_wr_data_valid <= 1'b0;
 	end

 	// 移位采用右移,将先收到的数据最终要放到低位.
 	// 移位的参量表示,代入数值就是: out <= {iv_data[15:0],out[127:16]} 
 	always @(posedge i_clk or negedge rst_n) begin
 		if(~rst_n) 
 			ov_wr_data_out <= {AXI_DATA_WIDTH{1'b0}};
 		else if(user_wr_en)
 			ov_wr_data_out <= {user_wr_data,ov_wr_data_out[AXI_DATA_WIDTH - 1 : USER_WR_DATA_WIDTH]};
 		else
 			ov_wr_data_out <= ov_wr_data_out;
 	end

/*
 	// 如果将同步信号和数据写在一起就是如下的样子,相较于分开写较难理解.
 	always @(posedge i_clk or negedge rst_n) begin
 		if(~rst_n) begin
 			o_wr_data_valid <= 1'b0;
 			ov_wr_data_out  <= {AXI_DATA_WIDTH{1'b0}};
 		end 
 		else if(user_wr_en)begin
 			ov_wr_data_out  <= {user_wr_data,ov_wr_data_out[AXI_DATA_WIDTH - 1 : USER_WR_DATA_WIDTH]};
 			if(wr_user_data_cnt == MAX_WR_CNT)
 				o_wr_data_valid <= 1'b1;
 			else
 				o_wr_data_valid <= 1'b0;
 		end
 		else begin
 			o_wr_data_valid <= 1'b0;
 			ov_wr_data_out  <= {AXI_DATA_WIDTH{1'b0}};
 		end
 	end
*/

/*-------------------------------------------------*\
			   		输出last信号
\*-------------------------------------------------*/	

 	always @(posedge i_clk or negedge rst_n) begin
 		if(~rst_n)
 			o_wr_data_last <= 1'b0;
 		else if((wr_user_data_cnt == MAX_WR_CNT)&&(wr_burst_cnt == MAX_BURST_LENGTH)&&(~i_user_wr_mode))
 			o_wr_data_last <= 1'b1;
        else if(i_user_wr_mode && i_user_wr_last)
            o_wr_data_last <= 1'b1;
 		else
 			o_wr_data_last <= 1'b0;
 	end

/*-------------------------------------------------*\
			   输出req信号:与last信号同理
\*-------------------------------------------------*/	

 	always @(posedge i_clk or negedge rst_n) begin
 		if(~rst_n)
 			o_wr_req_en <= 1'b0;
 		else if((wr_user_data_cnt == MAX_WR_CNT)&&(wr_burst_cnt == MAX_BURST_LENGTH)&&(~i_user_wr_mode))
 			o_wr_req_en <= 1'b1;
        else if(i_user_wr_mode && i_user_wr_last)
            o_wr_req_en <= 1'b1;
 		else
 			o_wr_req_en <= 1'b0;
 	end

/*-------------------------------------------------*\
			   			写地址
\*-------------------------------------------------*/	

    always @(posedge i_clk or negedge rst_n) begin
        if(~rst_n)
            ov_wr_data_addr <= iv_user_wr_base_addr;
        else if(i_user_wr_mode && user_wr_en)
            ov_wr_data_addr <= user_wr_addr;
        else if(~i_user_wr_mode)begin
            if(ov_wr_data_addr >= iv_user_wr_base_addr - WR_BURST_LENGTH)
                ov_wr_data_addr <= iv_user_wr_base_addr;
            else
                ov_wr_data_addr <= ov_wr_data_addr + WR_BURST_LENGTH;
        end
        else
            ov_wr_data_addr <= ov_wr_data_addr;
    end

/*-------------------------------------------------*\
                    写突发长度
\*-------------------------------------------------*/

    always @(posedge i_clk or negedge rst_n) begin
        if(~rst_n)
            ov_wr_burst_length <= 8'h0;
        else if(i_user_wr_mode && user_wr_en) // 该length与parameter声明中的RD_BURST_LENGTH含义一致
            ov_wr_burst_length <= iv_user_wr_length/(AXI_DATA_WIDTH/8) - 1;
        else if(~i_user_wr_mode)
            ov_wr_burst_length <= MAX_BURST_LENGTH;
        else
            ov_wr_burst_length <= ov_wr_burst_length;
    end

/*-------------------------------------------------*\
                    调试信号
\*-------------------------------------------------*/

    // 检测用户发送的地址是否会产生4k边界问题
    always @(posedge i_clk or negedge rst_n) begin
        if(~rst_n)
            o_err_4k_border <= 1'b0;
        else if(user_wr_en && (user_wr_addr + user_wr_length[11:0] > 13'd4096))
            o_err_4k_border <= 1'b1;  // 取低12位就减去了4096,查看相对长度会产生4k边界问题.
        else
            o_err_4k_border <= 1'b0;
    end

    // 检查用户输入的地址是否规范
    // 如果AXI数据位宽128bit,对应16字节,那输入的地址最好是16的倍数.
    // 突发传输的数据量以字节为单位,1个128bit的AXI数据是16字节;如果突发地址不是16的倍数,
    // 那意味着突发地址可能在某个128bitAXI数据中的某个字节开始,这样就将从这个数据开始的AXI数据破坏掉了.
    always @(posedge i_clk or negedge rst_n) begin
        if(~rst_n) 
            o_err_user_addr <= 1'b0;
        else if(user_wr_en && (user_wr_addr[$clog2(AXI_DATA_WIDTH/8)-1:0]))
            o_err_user_addr <= 1'b1;
        else
            o_err_user_addr <= 1'b0;
    end

endmodule

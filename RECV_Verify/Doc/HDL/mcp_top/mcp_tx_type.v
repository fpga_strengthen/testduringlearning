//============================================================
//
//--- ALL RIGHTS RESERVED
//--- File		: mcp_tx_type.v
//--- Auther	: smartbai
//--- Data		: 2020-11-18
//--- Version	: v1.0.0.0
//--- Abstract	: 
//
//------------------------------------------------------------
//Description	:
//------------------------------------------------------------
//--- Modification History:
//--- Date		By		Version		Change Description
//------------------------------------------------------------
//
//============================================================

module mcp_tx_type (
    i_clk,          //27MHz
    i_rst_n,

    i_frame_type_en,

    i_ant_err,      //天线正常
    i_ant_nor,      //天线异常
    i_safe_enable,  //保密芯片是否工作正常，1表示正常，0表示异常

    i_byte_ok,
    o_byte_en,
    ov_byte,
    o_frame_type_ok     //版本号or状态信息发送完成
);
    input i_clk;          //27MHz
    input i_rst_n;

    input i_frame_type_en;

    input i_ant_err;
    input i_ant_nor;
    input i_safe_enable;

    input i_byte_ok;
    output o_byte_en;
    output [7:0] ov_byte;
    output o_frame_type_ok;
//--------------------------------------------------------//
//信号定义
    reg [7:0] mem_type [7:0];       //存储状态信息

//--------------------------------------------------------//
// 对输入状态信号打拍或取沿
    reg ant_err_r;
    // reg ant_err_p;
    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n) begin
            ant_err_r <= 1'b0;
            // ant_err_p <= 1'b0;
        end
        else begin
            ant_err_r <= i_ant_err;
            // ant_err_p <= i_ant_err && (~ant_err_r);
        end
    end

    reg ant_nor_r;
    // reg ant_nor_p;
    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n) begin
            ant_nor_r <= 1'b0;
            // ant_nor_p <= 1'b0;
        end
        else begin
            ant_nor_r <= i_ant_nor;
            // ant_nor_p <= i_ant_nor && (~ant_nor_r);
        end
    end

    reg safe_enable_r;
    // reg safe_enable_p;
    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n) begin
            safe_enable_r <= 1'b0;
            // safe_enable_p <= 1'b0;
        end
        else begin
            safe_enable_r <= i_safe_enable;
            // safe_enable_p <= i_safe_enable && (~safe_enable_r);
        end
    end

    reg byte_ok_r;
    reg byte_ok_p;
    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n) begin
            byte_ok_r <= 1'b0;
            byte_ok_p <= 1'b0;
        end
        else begin
            byte_ok_r <= i_byte_ok;
            byte_ok_p <= i_byte_ok && (~byte_ok_r);
        end
    end

    reg frame_type_en_r;
    reg frame_type_en_p;
    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n) begin
            frame_type_en_r <= 1'b0;
            frame_type_en_p <= 1'b0;
        end
        else begin
            frame_type_en_r <= i_frame_type_en;
            frame_type_en_p <= i_frame_type_en && (~frame_type_en_r);
        end
    end

//---------------------------------------------------------//
//准备版本和状态信息保存到mem中,等待发送

    // mem_type[0]: 版本状态ID码 1~255循环
    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n)
            mem_type[0] <= 8'd0;
        else if(&mem_type[0] == 1'b1)
            mem_type[0] <= 8'd0;
        else if(frame_type_en_p)
            mem_type[0] <= mem_type[0] + 1'b1;
        else
            mem_type[0] <= mem_type[0];
    end

    // //目前版本号1.2.3.1
    // // 目前版本号1.2.4.0
    // 目前版本号1.2.4.1
    // mem_type[1]: 版本前两位, 4个bit表示一位
    // mem_type[2]: 版本后两位, 4个bit表示一位
    // mem_type[3]: FPGA工作状态, 0：表示FPGA工作正常 1：表示FPGA工作异常
    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n) begin
            mem_type[1] <= {4'd1, 4'd2};
            mem_type[2] <= {4'd4, 4'd1};
            mem_type[3] <= 8'd0;
        end
        else begin
            mem_type[1] <= {4'd1, 4'd2};
            mem_type[2] <= {4'd4, 4'd1};
            mem_type[3] <= 8'd0;
        end
    end

    // mem_type[4]: 加密芯片状态, 0：表示正常 1：表示异常
    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n)
            mem_type[4] <= 8'd0;
        else if(safe_enable_r)
            mem_type[4] <= 8'd0;
        else
            mem_type[4] <= 8'd1;
    end

    // mem_type[5]: 天线自检状态, 0：表示正常 1：表示异常
    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n)
            mem_type[5] <= 8'd0;
        else if(ant_err_r)
            mem_type[5] <= 8'd1;
        else if(ant_nor_r)
            mem_type[5] <= 8'd0;
        else
            mem_type[5] <= mem_type[5];
    end

//---------------------------------------------------------//
// 准备发送
    reg tx_clk;
    reg [2:0] cnt_send;

    //CRC16
    wire crc_ctr;
    wire crc_clr;   //CRC清空
    wire [15:0] checksum16;

    //产生发送时钟
    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n)
            tx_clk <= 1'b0;
        else if(frame_type_en_p || ((|cnt_send == 1'b1) && byte_ok_p))
            tx_clk <= 1'b1;
        else
            tx_clk <= 1'b0;
    end

    //发送数据计数器
    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n)
            cnt_send <= 3'd0;
        else if(frame_type_en_p)
            cnt_send <= 3'd0;
        else if(tx_clk)
            cnt_send <= cnt_send + 1'b1;
        else
            cnt_send <= cnt_send;
    end

    //产生CRC校验
    assign crc_ctr = tx_clk && (cnt_send <= 3'd5);
    assign crc_clr = frame_type_en_p;
    Crc16 u_Crc16(
        .i_clk_27M(i_clk),
        .i_rst_n(i_rst_n),
        .iv_data(mem_type[cnt_send]),   //外部输入信号
        .i_crc_ctr(crc_ctr),             //控制信号，高点平时进行校验
        .i_crc_clr(crc_clr),            //清零信号
        .ov_checksum16(checksum16)      //校验输出
    );

    always @(*) begin
        mem_type[6] <= checksum16[15:8];
        mem_type[7] <= checksum16[7:0];
    end

    //发送使能和发送数据
    reg byte_en;
    reg [7:0] byte_data;
    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n)
            byte_en <= 1'b0;
        else if(tx_clk)
            byte_en <= 1'b1;
        else
            byte_en <= 1'b0;
    end

    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n)
            byte_data <= 8'd0;
        else if(tx_clk)
            byte_data <= mem_type[cnt_send];
        else
            byte_data <= byte_data;
    end

    assign o_byte_en = byte_en;
    assign ov_byte = byte_data;

//---------------------------------------------------------//
//产生发送完成信号
    reg frame_type_ok;
    reg [3:0] cnt_byte_ok;
    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n)
            cnt_byte_ok <= 4'd0;
        else if(frame_type_en_p)
            cnt_byte_ok <= 4'd1;
        else if((cnt_byte_ok == 4'b1000) && byte_ok_p)
            cnt_byte_ok <= 4'd0;
        else if((|cnt_byte_ok == 1'b1) && byte_ok_p)
            cnt_byte_ok <= cnt_byte_ok + 1'b1;
        else
            cnt_byte_ok <= cnt_byte_ok;
    end

    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n)
            frame_type_ok <= 1'b0;
        else if((cnt_byte_ok == 4'b1000) && byte_ok_p)
            frame_type_ok <= 1'b1;
        else
            frame_type_ok <= 1'b0;
    end

    assign o_frame_type_ok = frame_type_ok;

endmodule
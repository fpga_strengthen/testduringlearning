function decval=hex2decWithSign(hexval, length)
%UNTITLED2 Summary of this function goes here
%   16进制带符号数转为10进制带符号数
%   hexval 16进制数
%   length 对应2进制符号位

decval = hex2dec(hexval);
sign = bitget(decval, length);
negative_numbers = (sign == 1);
decval(negative_numbers) = decval(negative_numbers) - bitshift(1, length);

end


module framing
  (
  //----------------------------------------------
  //时钟和复位
  input        i_clk_81M,
  input        i_rst_n,
  
  input        i_framing_start,//完全同步上时，该信号为高电平，开始组帧
  input        i_demod_result,
  input        i_demod_result_sck,//该信号为81M时钟沿
  
  output reg[10:0] ov_data_11bit,
  output reg       o_data_11bit_sck,//该信号为81M时钟沿
  
  output reg[7:0]  ov_data_8bit,
  output reg       o_data_8bit_sck,//点灯及btminnerbus需要27M时钟沿
  
  output reg       o_data_1bit,
  output reg       o_data_1bit_sck//自检部分需要27M时钟沿
  );
  
  //输入数据寄存（来之同一个时钟域，寄存一次）
  reg        demod_result_r;
  reg        demod_result_sck_r;
  reg        demod_result_sck_delay1;//延时两拍用于将81M扩展为27M时钟沿
  reg        demod_result_sck_delay2;
  reg        framing_start_r;
  always@(posedge i_clk_81M or negedge i_rst_n)
  begin
    if(!i_rst_n) begin
        demod_result_r <= 1'b0;
        demod_result_sck_r <= 1'b0;
        demod_result_sck_delay1 <= 1'b0;
        demod_result_sck_delay2 <= 1'b0;
        framing_start_r <= 1'd0;
    end
    else begin
        demod_result_r <= i_demod_result;
        demod_result_sck_r <= i_demod_result_sck;
        demod_result_sck_delay1 <= demod_result_sck_r;
        demod_result_sck_delay2 <= demod_result_sck_delay1;
        framing_start_r <= i_framing_start;
    end    
  end
  //组成11bit一组的数据
  reg [3:0] cnt_11;
  always@(posedge i_clk_81M or negedge i_rst_n)
  begin
    if(!i_rst_n) 
        cnt_11 <= 4'd0;
   else if(!framing_start_r)//没有完全同步时，置0
       cnt_11 <= 4'd0;
    else if( cnt_11 == 4'd10 && demod_result_sck_r )
        cnt_11 <= 4'd0;
    else if( demod_result_sck_r )
        cnt_11 <= cnt_11 + 4'd1;
    else
        cnt_11 <= cnt_11;
  end
  
  always@(posedge i_clk_81M or negedge i_rst_n)
  begin
    if(!i_rst_n) 
        o_data_11bit_sck <= 1'd0;      
    else if( cnt_11 == 4'd10 && demod_result_sck_r )
        o_data_11bit_sck <= 1'd1;
    else
        o_data_11bit_sck <= 1'd0;
  end
  
  always@(posedge i_clk_81M or negedge i_rst_n)
  begin
    if(!i_rst_n) 
        ov_data_11bit <= 11'd0;
    else if( demod_result_sck_r )
        ov_data_11bit <= {ov_data_11bit[9:0],demod_result_r};
    else
        ov_data_11bit <= ov_data_11bit;
  end
  
  //组成8bit一组的数据
  reg [2:0] cnt_8;
  always@(posedge i_clk_81M or negedge i_rst_n)
  begin
    if(!i_rst_n) 
        cnt_8 <= 3'd0;
   else if(!framing_start_r)//没有完全同步时，置0
       cnt_8 <= 3'd0;
    else if( demod_result_sck_r )
        cnt_8 <= cnt_8 + 3'd1;
    else
        cnt_8 <= cnt_8;
  end
  
  reg data_8bit_sck_81M;
  always@(posedge i_clk_81M or negedge i_rst_n)
  begin
    if(!i_rst_n) 
        data_8bit_sck_81M <= 1'd0;
    else if( cnt_8 == 3'd7 && demod_result_sck_r)//由于demod_result_sck_r为81M时钟沿，故得到的8bitsck为81M时钟沿
        data_8bit_sck_81M <= 1'd1;
    else
        data_8bit_sck_81M <= 1'd0;
  end
  
  
  reg data_8bit_sck_81M_delay1;
  reg data_8bit_sck_81M_delay2;
  always@(posedge i_clk_81M or negedge i_rst_n)
  begin
    if(!i_rst_n) begin
	   data_8bit_sck_81M_delay1 <= 1'd0;
		data_8bit_sck_81M_delay2 <= 1'd0;
	   o_data_8bit_sck <= 1'd0;
    end
    else begin
        data_8bit_sck_81M_delay1 <= data_8bit_sck_81M;
		  data_8bit_sck_81M_delay2 <= data_8bit_sck_81M_delay1;
	     o_data_8bit_sck <= data_8bit_sck_81M || data_8bit_sck_81M_delay1 || data_8bit_sck_81M_delay2;//81M/3=27M时钟沿
    end
  end
  
  always@(posedge i_clk_81M or negedge i_rst_n)
  begin
    if(!i_rst_n) 
        ov_data_8bit <= 8'd0;
    else if( demod_result_sck_r )
        ov_data_8bit <= {ov_data_8bit[6:0],demod_result_r};
    else
        ov_data_8bit <= ov_data_8bit;
  end
  
  //组成1bit一组的数据
  always@(posedge i_clk_81M or negedge i_rst_n)
  begin
    if(!i_rst_n) begin
        o_data_1bit_sck <= 1'd0;
        o_data_1bit     <= 1'd0;
    end
   else if(!framing_start_r)begin//没有完全同步时，置0
       o_data_1bit_sck <= 1'd0;
        o_data_1bit     <= 1'd0;
   end
    else begin
        o_data_1bit_sck <= demod_result_sck_r||demod_result_sck_delay1||demod_result_sck_delay2;//该信号为27M时钟沿
        o_data_1bit     <= demod_result_r;
    end
  end
  
endmodule
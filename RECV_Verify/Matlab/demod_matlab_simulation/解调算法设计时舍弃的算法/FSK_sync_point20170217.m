%FSK解调求同步点仿真
%目的检测频偏对判决错误的影响
%24点积分求平方和，最小二乘法二次曲线拟合
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%FSK调制%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc
format long
close all
clear all

%% %标准条件下主要参数
f0_true = 3.951 ;
f1_true = 4.516 ;         %比特0和比特1的频点，单位MHz
f_mid =4.234;             % (3.951+4.516) / 2
sample_rate_true = 27.095;% 标准采样速率，单位MHz
BPS_data_true = 0.56448;  % 标准数据传输速率，单位MHz
sum_bits = 100;%比特数
Hd = LP_6M;
% FSK数据的关键参数
FSK_freq_table = [  4.51624,    3.95176;...
    4.4964832,  3.9320032;...
    4.5359968,  3.9715168;...
    4.34124,    3.77676;...
    4.3214832,  3.7570032;...
    4.3609968,  3.7965168;...
    4.69124,    4.12676;...
    4.6714832,  4.1070032;...
    4.7109968,  4.1465168];%单位MHz

%%%%%%%%% 产生相位连续的FSK调制信号进行ADC采样后作为输入 %%%%%%%%%%%%%

src_data =randint(1,sum_bits);%以比特向量方式输入的待调制数据，随机产生
static_lenth_x_axis = 48 * sum_bits + 50;

%干扰信号
f0_noise = 3.951   ;
f1_noise = 4.516 ;%比特0和比特1的频点，单位MHz
FSK_data_noise  = FSK_mod(src_data,f0_noise,f1_noise,sample_rate_true,BPS_data_true)/10;

% 构造本地载波
S0 = cos( f0_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);
S1 = cos( f1_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);
S0_Q = sin(f0_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);%%移相90°
S1_Q = sin(f1_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);%%移相90°

N = 3; % 2N+1点最小二乘法
coeff_numerator = 3*[-N:N];           %极值点多项式分子
coeff_denominator = 8 - 2 * [-N:N].^2;%极值点多项式分母

%% 比较特殊频率处理抵消效果
for freq_num = 1:size(FSK_freq_table,1)
    %产生FSK调制信号
    BPS_data = BPS_data_true;%数据传输速率，单位MHz
    sample_rate =  sample_rate_true;%采样速率，单位MHz
    FSK_data  =FSK_mod(src_data,FSK_freq_table(freq_num,2),FSK_freq_table(freq_num,1),sample_rate,BPS_data);
    
    %对输入数据进行定标
    FSK_data = FSK_data  - 2048;%FixPointNum(FSK_data  - 2048, 12, 0);%字长12bit，精度0位小数 + FSK_data_noise
    length_fsk_data = length(FSK_data);
    
    %% 相关解调
    % 混频并滤除高频信号
    FSK_data_S0     = filter(Hd,FSK_data .* S0(1:length_fsk_data));%filter(Hd,
    FSK_data_S0_Q   = filter(Hd,FSK_data .* S0_Q(1:length_fsk_data));%filter(Hd,
    FSK_data_S1     = filter(Hd,FSK_data .* S1(1:length_fsk_data));%filter(Hd,
    FSK_data_S1_Q   = filter(Hd,FSK_data .* S1_Q(1:length_fsk_data));% filter(Hd,
    
    %% 24点累加平方和法求同步点
    for i =1:length_fsk_data-23 %相干解调过程,24个点求和，再除以24
        sum_bit0_point(i) = ( (sum(FSK_data_S0(i:i+23)))^2 + (sum(FSK_data_S0_Q(i:i+23)))^2 )./24  ;
        sum_bit1_point(i) = ( (sum(FSK_data_S1(i:i+23)))^2 + (sum(FSK_data_S1_Q(i:i+23)))^2 )./24  ;
    end %for
    
    sum_bit0_mins_sum_bit0 = sum_bit0_point(1:end-24) - sum_bit0_point(25:end);
    sum_bit1_mins_sum_bit1 = sum_bit1_point(1:end-24) - sum_bit1_point(25:end);
    sum_bit0_mins_sum_bit1 = sum_bit0_mins_sum_bit0 - sum_bit1_mins_sum_bit1;
    
    sum_bit0_mins_sum_bit1 = [zeros(1,12),sum_bit0_mins_sum_bit1(1:end)];%第1个点减第25个点得到的是第12个点的结果，而目前将该结果存在第1个位置，所以需要移位12点才正确
    sum_bit0_mins_sum_bit1 = abs(sum_bit0_mins_sum_bit1);%去掉符号位
    
    for i = 1:length(sum_bit0_mins_sum_bit1)
        if (sum_bit0_mins_sum_bit1(i) < max(sum_bit0_mins_sum_bit1)*0.8)%下溢清零
            sum_bit0_mins_sum_bit1(i) = 0;
        else
            sum_bit0_mins_sum_bit1(i) = sum_bit0_mins_sum_bit1(i) - max(sum_bit0_mins_sum_bit1)*0.8; %减小位数
        end %if
    end %for
    
    %% 求极值点坐标处理 最小二乘法的二次曲线拟合
    temp_val = toeplitz(sum_bit0_mins_sum_bit1(1:2*N+1),sum_bit0_mins_sum_bit1);
    %     for ii = 1:size(temp_val,2)
    %
    %         poly_val = polyfit([-N:N].',temp_val(:,ii),2);%最小二乘法二次曲线拟合求目标方程系数
    %         sync_point_comp(ii) = - poly_val(2)/poly_val(1)/2;
    %     end
    sync_point = (coeff_numerator * temp_val) ./ ( coeff_denominator * temp_val);%详见文档极值点坐标公式推导
    
    sync_point(sync_point > 5) = 5;%极值应在[-3,3]之间，上溢处理
    sync_point(sync_point < -5) = -5;
    sync_point = (abs(sync_point)<0.6).* (sum_bit0_mins_sum_bit1>0);%对称轴在[-0.6,0.6]之间且该点不为零时，该坐标轴原点位置为所求极值点
    sync_point = sync_point(4:end); %该算法会让极值点向后移3个点，是否与N有关？
    sync_point = sync_point(1:end).*(~[0,sync_point(1:end-1)])%取上升沿作极值点
    
    %% 画图
    figure(1),
    subplot(2,1,1),
    hold off,
    plot(sum_bit0_point/max(sum_bit0_point)*8,'r');
    hold on,
    plot(sum_bit1_point/max(sum_bit1_point)*8,'g');
    plot(sum_bit0_mins_sum_bit1/max(sum_bit0_mins_sum_bit1)*8);
    plot(sync_point.*10 ,'k-+');
    legend('f0混频滤波24点平方和','f1混频滤波24点平方和','相差24点后相减得到的判断折线','同步点');
    grid on;
    title(strcat('FSK信号频点' ,num2str(FSK_freq_table(freq_num,2)-3.951),' , ',num2str(FSK_freq_table(freq_num,1)-4.516), 'Mhz时24点累加平方和法同步曲线'));
    
%     figure(1),
%     subplot(2,1,2),
%     hold off,
%     plot(sync_point,'r-.');
%     hold on;
%     plot(sum_bit0_mins_sum_bit1/max(sum_bit0_mins_sum_bit1)*8,'k');
%     plot(sync_point_comp,'r-*');
%     plot(sync_point.*10 ,'g-+');
%     legend('判断折线','同步点')
%     plot(( (abs(sync_point)<0.5) .* (abs(sum_bit0_mins_sum_bit1)>max(sum_bit0_mins_sum_bit1)*.8))*50,'r');
    
    %% 判断两相邻极值点是否是48的整数倍 统计偏差点
    edge_position = find(sync_point == 1);%极值点的位置
    edge_variance = mod(edge_position(2:length(edge_position)) - edge_position(1:length(edge_position)-1),48);%两极值点之间的间隔对48点取模，得到偏差量
    x_axis = [-23:24];
    y_axis = zeros(1,7);
    y_axis(1) = length (find(edge_variance==23));%多23个点
    y_axis(2) = length (find(edge_variance==22));%多22个点
    y_axis(3) = length (find(edge_variance==21));%多21个点
    y_axis(4) = length (find(edge_variance==20));%多20个点
    y_axis(5) = length (find(edge_variance==19));%多19个点
    y_axis(6) = length (find(edge_variance==18));%多18个点
    y_axis(7) = length (find(edge_variance==17));%多17个点
    y_axis(8) = length (find(edge_variance==16));%多16个点
    y_axis(9) = length (find(edge_variance==15));%多15个点
    y_axis(10) = length (find(edge_variance==14));%多14个点
    y_axis(11) = length (find(edge_variance==13));%多13个点
    y_axis(12) = length (find(edge_variance==12));%多12个点
    y_axis(13) = length (find(edge_variance==11));%多11个点
    y_axis(14) = length (find(edge_variance==10));%多10个点
    y_axis(15) = length (find(edge_variance==9));%多9个点
    y_axis(16) = length (find(edge_variance==8));%多8个点
    y_axis(17) = length (find(edge_variance==7));%多7个点
    y_axis(18) = length (find(edge_variance==6));%多6个点
    y_axis(19) = length (find(edge_variance==5));%多5个点
    y_axis(20) = length (find(edge_variance==4));%多4个点
    y_axis(21) = length (find(edge_variance==3));%多3个点
    y_axis(22) = length (find(edge_variance==2));%多2个点
    y_axis(23) = length (find(edge_variance==1));%多1个点
    y_axis(24) = length (find(edge_variance==0));%无偏差
    y_axis(25) = length (find(edge_variance==47));%少1个点
    y_axis(26) = length (find(edge_variance==46));%少2个点
    y_axis(27) = length (find(edge_variance==45));%少3个点
    y_axis(28) = length (find(edge_variance==44));%少4个点
    y_axis(29) = length (find(edge_variance==43));%少5个点
    y_axis(30) = length (find(edge_variance==42));%少6个点
    y_axis(31) = length (find(edge_variance==41));%少7个点
    y_axis(32) = length (find(edge_variance==40));%少8个点
    y_axis(33) = length (find(edge_variance==39));%少9个点
    y_axis(34) = length (find(edge_variance==38));%少10个点
    y_axis(35) = length (find(edge_variance==37));%少11个点
    y_axis(36) = length (find(edge_variance==36));%少12个点
    y_axis(37) = length (find(edge_variance==35));%少13个点
    y_axis(38) = length (find(edge_variance==34));%少14个点
    y_axis(39) = length (find(edge_variance==33));%少15个点
    y_axis(40) = length (find(edge_variance==32));%少16个点
    y_axis(41) = length (find(edge_variance==31));%少17个点
    y_axis(42) = length (find(edge_variance==30));%少18个点
    y_axis(43) = length (find(edge_variance==29));%少19个点
    y_axis(44) = length (find(edge_variance==28));%少20个点
    y_axis(45) = length (find(edge_variance==27));%少21个点
    y_axis(46) = length (find(edge_variance==26));%少22个点
    y_axis(47) = length (find(edge_variance==25));%少23个点
    y_axis(48) = length (find(edge_variance==24));%少24个点
    y_axis = y_axis./sum(y_axis);
    
    figure(1),
    subplot(2,1,2),
    bar(x_axis,y_axis);
    xlabel('偏差量');
    ylabel('统计概率值');
    set(gca,'xtick',-23:1:24);
    title(strcat('FSK信号频偏' ,num2str((FSK_freq_table(freq_num,2)-3.951)*1000),' , ',num2str((FSK_freq_table(freq_num,1)-4.516)*1000), 'Khz','时统计偏移量'));
    
    %% 非相干解调法 间隔T/2相乘
    FSK_S0_mult = FSK_data_S0(1:end-23) .* FSK_data_S0(24:end) ;
    FSK_S0_Q_mult = FSK_data_S0_Q(1:end-23) .* FSK_data_S0_Q(24:end) ;
    FSK_S1_mult = FSK_data_S1(1:end-23) .* FSK_data_S1(24:end) ;
    FSK_S1_Q_mult = FSK_data_S1_Q(1:end-23) .* FSK_data_S1_Q(24:end) ;
    %两路相加
    S0_sum_S0_Q = FSK_S0_mult +FSK_S0_Q_mult;
    S1_sum_S1_Q = FSK_S1_mult +FSK_S1_Q_mult;
    %前后24点相减后两路相加
    sum0_mins_sum0 = S0_sum_S0_Q(1:end-24) - S0_sum_S0_Q(25:end);
    sum1_mins_sum1 = S1_sum_S1_Q(1:end-24) - S1_sum_S1_Q(25:end);
    sum0_mins_sum1 = sum0_mins_sum0 - sum1_mins_sum1;
    sum0_mins_sum1 = [zeros(1,12),sum0_mins_sum1(1:end)];%第1个点减第25个点得到的是第12个点的结果，而目前将该结果存在第1个位置，所以需要移位12点才正确
    sum0_mins_sum1 = abs(sum0_mins_sum1);%去掉符号位
    %优化曲线
    for i = 1:length(sum0_mins_sum1)
        if (sum0_mins_sum1(i) < max(sum0_mins_sum1)*0.8)%下溢清零
            sum0_mins_sum1(i) = 0;
        else
            sum0_mins_sum1(i) = sum0_mins_sum1(i) - max(sum0_mins_sum1)*0.8; %减小位数
        end %if
    end %for
    %最小二乘法二次曲线拟合
    temp_val = toeplitz(sum0_mins_sum1(1:2*N+1),sum0_mins_sum1);
    sync_point = (coeff_numerator * temp_val) ./ ( coeff_denominator * temp_val);%详见文档极值点坐标公式推导
    %求极值点
    sync_point(sync_point > 5) = 5;%极值应在[-3,3]之间，上溢处理
    sync_point(sync_point < -5) = -5;
    sync_point = (abs(sync_point)<0.6) .* (sum0_mins_sum1>0);%对称轴在[-0.6,0.6]之间且该点不为零时，该坐标轴原点位置为所求极值点
    sync_point = sync_point(4:end); %该算法会让极值点向后移N个点
    sync_point = sync_point(1:end).*(~[0,sync_point(1:end-1)])%取上升沿作极值点
    
    figure(2),
    subplot(2,1,1),
    hold off,
    plot(S0_sum_S0_Q/max(S0_sum_S0_Q)*8,'r');
    hold on,
    plot(S1_sum_S1_Q/max(S1_sum_S1_Q)*8,'g');
    plot(sum0_mins_sum1/max(sum0_mins_sum1)*8,'b');
    plot(sync_point.*10 ,'k-+');
    legend('S0_sum_S0_Q','S1_sum_S1_Q','sum0_mins_sum1','sync_point');
    title(strcat('FSK信号频偏' ,num2str((FSK_freq_table(freq_num,2)-3.951)*1000),' , ',num2str((FSK_freq_table(freq_num,1)-4.516)*1000), 'Khz','时非相干解调法同步曲线'));
    
    % 判断两相邻极值点是否是48的整数倍 统计偏差点
    edge_position = find(sync_point == 1);%极值点的位置
    edge_variance = mod(edge_position(2:length(edge_position)) - edge_position(1:length(edge_position)-1),48);%两极值点之间的间隔对48点取模，得到偏差量
    x_axis = [-23:24];
    y_axis = zeros(1,7);
    y_axis(1) = length (find(edge_variance==23));%多23个点
    y_axis(2) = length (find(edge_variance==22));%多22个点
    y_axis(3) = length (find(edge_variance==21));%多21个点
    y_axis(4) = length (find(edge_variance==20));%多20个点
    y_axis(5) = length (find(edge_variance==19));%多19个点
    y_axis(6) = length (find(edge_variance==18));%多18个点
    y_axis(7) = length (find(edge_variance==17));%多17个点
    y_axis(8) = length (find(edge_variance==16));%多16个点
    y_axis(9) = length (find(edge_variance==15));%多15个点
    y_axis(10) = length (find(edge_variance==14));%多14个点
    y_axis(11) = length (find(edge_variance==13));%多13个点
    y_axis(12) = length (find(edge_variance==12));%多12个点
    y_axis(13) = length (find(edge_variance==11));%多11个点
    y_axis(14) = length (find(edge_variance==10));%多10个点
    y_axis(15) = length (find(edge_variance==9));%多9个点
    y_axis(16) = length (find(edge_variance==8));%多8个点
    y_axis(17) = length (find(edge_variance==7));%多7个点
    y_axis(18) = length (find(edge_variance==6));%多6个点
    y_axis(19) = length (find(edge_variance==5));%多5个点
    y_axis(20) = length (find(edge_variance==4));%多4个点
    y_axis(21) = length (find(edge_variance==3));%多3个点
    y_axis(22) = length (find(edge_variance==2));%多2个点
    y_axis(23) = length (find(edge_variance==1));%多1个点
    y_axis(24) = length (find(edge_variance==0));%无偏差
    y_axis(25) = length (find(edge_variance==47));%少1个点
    y_axis(26) = length (find(edge_variance==46));%少2个点
    y_axis(27) = length (find(edge_variance==45));%少3个点
    y_axis(28) = length (find(edge_variance==44));%少4个点
    y_axis(29) = length (find(edge_variance==43));%少5个点
    y_axis(30) = length (find(edge_variance==42));%少6个点
    y_axis(31) = length (find(edge_variance==41));%少7个点
    y_axis(32) = length (find(edge_variance==40));%少8个点
    y_axis(33) = length (find(edge_variance==39));%少9个点
    y_axis(34) = length (find(edge_variance==38));%少10个点
    y_axis(35) = length (find(edge_variance==37));%少11个点
    y_axis(36) = length (find(edge_variance==36));%少12个点
    y_axis(37) = length (find(edge_variance==35));%少13个点
    y_axis(38) = length (find(edge_variance==34));%少14个点
    y_axis(39) = length (find(edge_variance==33));%少15个点
    y_axis(40) = length (find(edge_variance==32));%少16个点
    y_axis(41) = length (find(edge_variance==31));%少17个点
    y_axis(42) = length (find(edge_variance==30));%少18个点
    y_axis(43) = length (find(edge_variance==29));%少19个点
    y_axis(44) = length (find(edge_variance==28));%少20个点
    y_axis(45) = length (find(edge_variance==27));%少21个点
    y_axis(46) = length (find(edge_variance==26));%少22个点
    y_axis(47) = length (find(edge_variance==25));%少23个点
    y_axis(48) = length (find(edge_variance==24));%少24个点
    y_axis = y_axis./sum(y_axis);
    
    figure(2),
    subplot(2,1,2),
    bar(x_axis,y_axis);
    xlabel('偏差量');
    ylabel('统计概率值');
    set(gca,'xtick',-23:1:24);
    title(strcat('FSK信号频偏' ,num2str((FSK_freq_table(freq_num,2)-3.951)*1000),' , ',num2str((FSK_freq_table(freq_num,1)-4.516)*1000), 'Khz','时统计偏移量'));
    
    pause;
    close all;
end

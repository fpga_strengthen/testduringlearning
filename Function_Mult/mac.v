module mac
#(
    parameter   N = 8
)
(
    input   wire            clk,
    input   wire            rst_n,
    input   wire [N-1:0]    opa,
    input   wire [N-1:0]    opb,
    output  reg  [2*N-1:0]  out
);
    //定义函数计算乘积
    function [2*N-1:0] mult;
        input   [N-1:0]     opa;
        input   [N-1:0]     opb;
        reg     [2*N-1:0]   result;

        integer i;
        begin 
            result = opa[0] ? opb : 0;          //opb与opa的最低位相乘
            //result = 0;
            //$display("INIT finish , i = %0d , initial result = %0d",i,result);
            //$display("for-loop starts now.");
            for (i = 1; i < N ; i=i+1) begin
                if(opa[i] == 1)begin
                    result = result + (opb << (i));
                    //$stop(1);
                    //$display("i=%0d ,result = %0d",i,result);
                end      
            end
            mult = result;          //函数返回值，注意要写在函数计算体内部
        end

    endfunction

    wire    [2*N-1:0]   sum;

    assign  sum = mult(opa,opb);

    always@(posedge clk or negedge rst_n)begin 
        if(rst_n == 1'b0)
            out <= {(2*N){1'b0}};
        else
            out <= sum;
    end



endmodule
# 1. AXI4

AXI4协议包括三种：AXI4_FULL、AXI4_LITE、AXI4_STREAM，主要描述了**主设备**和**从设备**之间的**数据传输方式**。

AXI LITE：主要是通过配置IP核，包括xdma(PCIe)、AXI DMA(ZYNQ)、srio高速接口。数据位宽只支持32bit或者64bit.

AXI FULL（AXI4）：主要是用来读写DDR；

AXI STREAM：读写IP核，做数据端口。



## 1. **AXI Stream**

AXI4_STREAM丢弃了地址项，常用于高速数据传输。

如下图，是AXI4 Stream的接口信号：

<img src="pics/1.png" style="zoom:80%;" />

其中，最主要的是上边一组：

- **axis_tvalid**和**axis_tready**：

  **axis_tvalid**是由主机发送的信号，表示主机发送的数据有效；

  **axis_tready**是从机发送的信号，表示从机准备好了。只有**两个信号同时有效**时，主机发送的数据才会写入到从机中。

- **axis_tlast**：

  一次发送的最后一个数据的标志。比如AXI一次发送3个数据，在第三个数据发送时，该信号会拉高，第三个数据写入后又会拉低。

- **axis_tkeep**和**axis_tstrb**：

  **axis_keep**和**axis_tstrb**的字长等于**axis_sdata**的字长除以8，表示**axis_data**包含多少个字节；

  **axis_keep**和**axis_tstrb**的**每一位**就对应**axis_data**的每一个字节；只有它们同时为高时（指与字节对应的位），对应的字节才是有效的。

  <img src="pics/2.png" style="zoom:67%;" />

- **axis_tid**和**axis_tdest**：

  分别表示主机的ID和从机的ID。

- **axis_tuser**

  暂时不管，是非必要的，不用的时候可以置零。



## 2. AXI4 LITE

### 2.1 端口信号

AXI4_LITE不支持突发传输，常用于数据量较小的传输，可以理解为轻量级的AXI4_FULL。

AXI4_LITE包含五个通道：读操作的2个通道和写操作的3个通道：

<img src="pics/3.png" style="zoom:75%;" />

对比就可以发现，读写是分离的，并且读操作无响应，写操作有响应。

- LITE总线和STREAM一样，也是基于**握手**传输数据，包含的接口信号如下：

  ![](pics/4.png)

  <img src="pics/5.png" style="zoom:100%;" />

  对于最后的RRESP信号，只有RVALID和RREADY信号同手有效时，该信号才有效。

- AXI4 **LITE**传输的**数据位宽只支持32位**，也就是`WDATA`和`RDATA`只能为**32位**。

- 读写过程：

  - **主机**发送写地址和写数据的顺序关系：

    主机可以先发送写地址，后发送写数据；

    主机可以先发送写数据，后发送写地址；

    主机可以**同时**发送写地址和写数据。

  - **从机**接收写地址和写数据的顺序关系：

    从机可以先接收写地址，后接收写数据；

    从机可以先接收写数据，后接收写地址；

    从机可以**同时**接收写地址和写数据；

  - 而**最常用**的是同时发送写地址和写数据。




### 2.2 AXI LITE主机实现思路

这里实现的功能就是将用户端和AXI从机进行连接，除了读写以外，就是不同的时钟问题：写时钟、读时钟和AXI时钟。

当主机向从机写数据时，需要在写时钟下操作，需要**数据+地址**，所以需要两个FIFO分别将数据和地址从写时钟下的时序转换到AXI时钟下的时序。

当主机从从机读数据时，需要两个FIFO，一个用来将读时钟转换到AXI时钟下的时序，另一个就是将读出的数据从AXI时钟域转换到读时钟域下。

 

## 3. AXI Full

AXI Full即AXI4总线。

### 3.0 握手机制

 AXI4所采用的是一种READY，VALID握手通信机制，即主从模块进行数据通信前，先根据操作对各所用到的数据、地址通道进行握手。主要操作包括传输发送者A等到传输接受者B的READY信号后，A将数据与VALID信号同时发送给B。



### 3.1 端口信号

AXI4的端口信号根据读写操作来区分，AXI4共有5个通道，分别是写地址、写数据、写响应、读地址以及读数据通道。

更具体的端口信号可以参考野火的pdf教程的535页开始。

数据的读写都采用突发形式，突发分三种类型：FIXED、INCR和WRAP，最常用的是INCR突发，即只需要发送首地址后，后面数据的地址会自动增加，直到达到突发长度时算一次突发操作完成。而WRAP与INCR类似，但是每次结束后，地址又会重新回到首地址。FIXED突发即传输过程中突发地址固定，用于FIFO传输。

AXI4的读写端口时序也可以参考教程，以及Vivado本身提供的仿真例程。



### 3.2 4K边界

> 概念和举例

AXI4每次写突发时，要求写的**字节数**不能越过4K边界。1k即1024字节，每次写操作都是在一个长度为4096的区间内，如第一次写突发的地址范围在**[0,4095]**，则要求第一次写突发**过程中**的地址不能越过4095。

举例来讲，数据位宽为128bit，对应16字节，突发长度为100，第一次突发写操作的首地址为0，则写突发长度为$100\times(128/8)=1600$字节，故第一次写突发结束时的地址为1599；第二次写突发起始地址为1600，结束地址为3199；第三次写突发起始地址为3200，结束地址（理论上）为4799，但是此时这次突发传输过程中显然越过了**4k边界（4095个字节）**，所以这次写突发是违反了4k边界的规定的。



> 4k边界下的最大突发长度

假设数据位宽128bit，则最大突发长度为$4096/(128/8)=256$；

假设数据位宽256bit，则最大突发长度为$4096/(256/8)=128$；

假设数据位宽512bit，则最大突发长度为$4096/(512/8)=64$；

假设**数据位宽64bit**，则最大突发长度为$4096/(128/8)=512$，但**AXI4协议规定突发传输的长度在0~256之间**，所以此时即使计算出最大的突发长度为512，实际最大突发传输的字节数还是256字节。



> 避免4k边界问题

如果写突发长度一定，每次突发的地址偏移量可以被4096整除，就可以避免4k边界问题。

比如数据位宽128bit，突发长度为128，则地址偏移量为$128\times(128/8)=2048$，那第二次突发的结束地址就是4095，不会存在突发过程中出现地址越过4k边界的问题。

如果**每次突发的长度不固定**，设数据位宽128bit，起始地址为0，第一次突发长度为100，则地址偏移量为$128\times(128/8)=1600$，尾地址为1599，第二次的起始地址为1600；第二次突发长度为200，则地址偏移量为$200\times(128/8)=3200$，第二次的尾地址为$1599+3200=4799>4096$。

像这种每次突发长度不固定导致的4k边界的问题，**就对每次突发都分配新的4k边界**，即第二次的首地址从4096开始，而非1599。



### 3.3 Outstanding

Outstanding指**不需要等待上一次突发完成，就可以发生一次新的突发传输**。

<img src="../Ethernet/pics/25.png" style="zoom:77%;float:left" />

<img src="../Ethernet/pics/26.png" style="zoom:80%;float:left" />

上边的读写都是在写突发或读突发未完成时就发起了下一次突发请求，在读写DDR时最好不要用Outstanding功能，时序会比较复杂。



### 3.4 AXI4的仿真

主机Master自己写，从机AXI4 Slave有三种方式：

（1）自己写一个，但是非常麻烦；

（2）调用Vivado AXI4 VIP，这种也不方便；

（3）使用带有AXI4接口的RAM，此方式最为简单，在IP中选择BRAM，并将接口类型设置为AXI4即可。



### 3.5 ADMA读写DDR3

> 时钟域

用户进来的数据是16位的，AXI总线位宽为128位，所以第一步要先将数据进行“串并转换”处理，将16bit转128bit；

转换后的数据仍在用户时钟域下（较慢），读写DDR3一般是通过MIG IP核，而MIG核的时钟一般要比用户时钟快，所以这里要把向AXI从机发送的地址、突发长度和数据进行跨时钟域处理，需要用到异步FIFO。

而对于FIFO而言，其中有两个比较重要的信号，即empty和full信号。由于是在写操作下才会有FIFO满的情况，所以full信号是在写时钟域下产生的；而读操作才会导致FIFO空，所以empty信号是在读时钟下产生的。



> 为什么不用FIFO转data位宽？

1. 自己写代码使用移位寄存器会更简单；

2. FIFO转位宽有限制，有最大8倍的关系；

3. 时序不稳定，且逻辑要根据具体的位宽不停变化，比较麻烦。



> FIFO底层资源

- 缓存数据量较少时，FIFO建议使用DRAM。DRAM是使用逻辑资源搭建达成，消耗的是LUT。

  一般用于缓存命令、地址、帧长度等一些数据，缓存深度和数据宽度不要超过100。

- 缓存数据量较大时，FIFO建议使用BRAM。BRAM本身就是FPGA的一种资源，在Xilinx的FPGA中有18k和36k两种，单位bit。

  - 对于限制18k条件下：

    $\rm 18k=18\times1024bit=36bit(最大宽度)\times512(深度)=18bit(宽度)\times1024(深度)=9bit(宽度)\times2048(深度)$，

    因此对于只使用一个18kBRAM的情况，深度和对应最大字长的选择就是以上情况。

    如果在任意一种情况扩大宽度或深度，就要消耗一个36kBRAM来实现了。

  - 对于限制36k条件下，因为36kBRAM是用两个18kBRAM构成的，所以：
    $$
    \begin{align}
    \rm
    36k=2*18k=72bit*512=36bit*1024=18bit*2048
    \end{align}
    $$
    因此对于一个36kBRAM，可以存储的一些数据深度和宽度如上所示，限制的最大数据位宽为1024bit。

- FIFO采用BRAM的时候，消耗最少相同的BRAM数量，建议把FIFO的深度和宽度开到最大。



> wr_buffer的逻辑

在前边已经通过wr_ctrl模块实现了16bit转128bit的程序，并输出了写地址、写突发长度等信息。

wr buffer的主要功能是将用户时钟域下的128bit数据以及写地址、突发长度转换到AXI时钟域下输出。

数据会和last信号组合后随着同步有效信号一直写入到FIFO中，当一次突发完成后，收到上级模块发来的写请求信号（此信号仅仅持续一个clock，是在wr_ctrl模块发送完最后一个数据时产生的），此请求信号作为写使能将写地址和突发长度写入到cmd_fifo中。写入后状态机检测到该FIFO非空，就会跳转到写请求状态，向从机发送写地址写请求信号，在收到从机返回的有效ready信号之后，主从机完成握手，此时状态机跳转到数据发送状态，主机会将写地址和突发类型在AXI时钟下读出并发送给从机，在此状态下，数据FIFO的读使能会在valid信号、ready都有效时拉高，将FIFO中的数据读出并发送。其中，valid信号在主机发起写请求时就会拉高，在主从机的valid和ready持续有效（即握手成功状态下）检测到最后一个数据标志last信号时就拉低，主机发送完毕。

其中，写地址和突发长度使用DRAM缓存，数据则使用BRAM缓存。BRAM的存储深度是512，虽然数据位宽是128，此时消耗的是两个36k的BRAM，此时该BRAM可承受的的最大数据宽度是$\rm 2*(36*1024)/512=144bit$，所以为了充分利用此BRAM，合并了last信号作为写输入（129bit），并将剩余的$\rm 144-129=15bit$全部进行了补零操作，这样这2个36k的BRAM的所有空间就被全部利用了。

另外，为了适应多种数据位宽，使用了参数化表示，并结合generate-if语句，在不同位宽条件下例化不同的FIFO来跨时钟域。



## 4. 一些问题

> 为什么有写响应通道而没有读响应通道？

首先AXI4的Valid/Ready握手机制的每一次握手都只允许单方向的数据流。对于写通道，数据流的方向是从master到slave的，而写响应是从slave到master的，两者的方向是相反的；但是读通道中，读响应和读出的数据方向都是从slave到master，所以觉得应该是把读响应合并到了读数据通道中了。

至于为什么不把读响应合并到读地址通道，个人理解一方面是为了不破坏读地址通道的完整性，另一方面是相较写数据通道而言，写数据有strb信号对数据的有效性进行了指示，而读数据通道则没有，所以这样也可对读数据的有效性进行一定程度的指示。

而写通道不把写响应合并到写数据通道，个人理解是每个通道的数据都是单向的，这样会造成该通道产生相反的两个方向的数据流（一个是从mater到slave的数据，一个是从slave到mater的resp），这样其实会使总线的设计变得不那么清晰。



> 为什么要设置一个cmd fifo？它的作用？

主要作用是**将用户发送的突发长度和地址信息进行缓存并跨时钟域**。

以读操作为例，当用户发起一次读请求时，主机要发送读地址以及相应的valid信号，还要等待从机回应的ready信号，此过程需要一定的时间，如果在此期间用户又发起一次请求，那该请求可能会被丢掉，因为当前的读请求尚未完成。而使用FIFO可以将此请求缓存下来，在当前的读操作结束后再发起该次请求，这样就不会有少读的情况。



> 响应异常或者ready信号不拉高的可能原因？

写响应或者读响应的返回值不是00，或者ready信号不拉高，一般是因为代码的原因：

- AXI4读写地址越过了4k边界；

- m_axi_wlast信号提前拉高，比如主机发送的突发长度是100字节，但是在第99个字节时就将last信号拉高；

- m_axi_awsize信号m_axi_wdata位宽对应不上，个人总结有个对应关系：

  将awsize记为$ws$，wdata的位宽记为$ds$，则有$ds=2^{ws}*8$的关系，乘以8是因为一个字节8bit。

- 主机与从机的地址位宽不匹配。



> 对AXI4总线了解多少？AXI4、AXI4 Lite和AXI4 Stream的区别？

- AXI4的了解

  AXI是ARM公司提出的AMBA中的一部分，AXI4是一种高性能、高带宽、低延迟的片内总线；

  主要描述了主设备和从设备之间的数据传输方式；

- 分类及其区别

  根据不同需求分为AXI4、AXI4-Lite和AXI4-Stream，区别为：

  - AXI4：主要面向高性能地址映射通信的需求，支持突发传输，突发长度为1~256；
  - AXI4-Lite：是一个简单的吞吐量地址映射性通信总线，不支持突发传输，常用于数据量较少的传输，可理解为轻量级的AXIFull；
  - AXI4-Stream：丢弃了地址项，常用于高速数据流传输。

  它们的对比列表如下：

  <img src="pics/6.png" style="zoom:70%;float:middle" />

- **AXI4**的优势

  - 通过统一的AXI接口，开发者为开发IP Core只需要学习一种协议即可；
  - AXI4是面向地址映射的接口，允许最大256轮的数据突发传输；
  - AXI4-Lite是一个轻量级的地址映射单次传输接口，占用很少的逻辑单元；
  - AXI4-Stream去掉了地址项，允许无限制的数据突发传输规模。



> AXI4主机模块实现的思路？

- 首先，包含两端，分别是用户端和AXI4主机端口，如何将用户端转主机端口，以写通道为例；
- 第一步，首先有两个异步FIFO，一个DATA FIFO用来缓存用户端传来的数据，另一个CMD FIFO用来缓存AXI4的突发长度和地址；
- 第二步，用户端的数据为16bit，但是AXI4的数据位宽为128bit，先通过移位寄存器的方式将用户端数据拼接为128bit，然后将128bit数据写入FIFO。由于用户端写时钟和AXI4时钟不同，所以DATA FIFO又起到了一个跨时钟域的作用（介绍如何写入FIFO）；
- 第三步，往DATA FIFO里面写数据的时候，对写入的数据计数，当计数到突发长度的时候，往CMD FIFO里写入一个AXI4写地址和突发长度；
- 第四步，由于CMD FIFO里边有数据，所以CMD FIFO会变成非空状态，empty信号会拉低。当CMD FIFO非空时，就发起一次AXI4请求。
- 读通道和写通道是完全类似。



> 4k边界相关问题

- 为什么突发长度一般是2/4/8/16/.../128/256这些？
- 如何计算突发长度等？



> ID信号的作用？

以主机为例，主机写地址和数据给从机，AWID为0，则从机会返回一个BID为0。

ID信号一般用来**多通道读写，以不同ID区分不同的通道**。ID信号也可以代表**优先级**，ID值越小，优先级越高。



> outstanding是什么？（前边3.3已经写过）

> 乱序传输、乱序穿插机制是什么?

这个和ID信号相关，比如发起两次AXI4读请求，第一次读请求的ARID为0，第二次读请求的ARID为1，不过返回读数据时，先返回RID为1的读数据，再返回RID为0的读数据。



> 独占访问机制、锁定机制？

这些机制和lock信号相关。比如有多主一从的情况，当一个主机要独占对从机的访问时，就将lock信号置1。此时如果其他主机访问从机，会得到从机返回错误的响应值。（一般00就代表正常，非0就代表异常）。



# 2. DDR3

## 2.1 简介

DDR3全称第三代双倍速率同步动态随机存储器，主要有以下特点：

- 掉电无法保存数据，需要周期性的刷新；
- 时钟上升沿和下降沿都会传输数据；
- 突发传输，突发长度`burst length`一般为8。



## 2.2 MIG IP

> MIG IP核的两个输入时钟和两个输出时钟

- MIG IP核有两个**输入时钟**，一个是系统时钟**sys\_clk**，一个是参考时钟**ref_clk**：
  - **sys_clk**：时钟频率没有要求，时钟来源可以是FPGA内部的PLL，也可以是外部的晶振；
  - **ref_clk**：**时钟频率有要求，建议200M(固定)**，而时钟来源可以由FPGA内部PLL得到，也可以由外部晶振得到。

- MIG IP核会输出一个**差分时钟**给DDR3，作为DDR3的工作时钟，该时钟有两个要求：
  - 不能超过FPGA芯片的最高时钟频率；
  - 不能超过DDR3芯片的最高时钟频率。
- MIG IP核会**输出一个ui_clk给用户端**，作为用户与MIG IP核交互的时钟。

> 注意时钟的比例关系

MIG IP核**给DDR3的时钟**与**给用户的时钟**有固定的**2:1或4:1**的比例关系；但当DDR3的时钟为800M时，比例只能为4:1。比如，DDR3的时钟为300M，则给用户的时钟可以为150M或75M；但当DDR3时钟为800M时（**大于等于400M**），用户时钟只能是200M。



> 数据位宽最大值

设DDR3的芯片位宽为16bit，时钟频率为800M，则其数据速率为$800\times2\times16\ bps$，乘2是因为双沿传输；

用户端时钟此时只能为200M，要求用户端的数据速率小于等于DDR3的数据速率，

即$200\times W_u\leq800\times2\times16$，即$W_u\leq128$，因此用户端的数据最大位宽就是128bit。



> 关于时钟属性的设置

如果MIG IP的系统时钟是来自于外部晶振的单端时钟，就设置为**Single-Ended**；

如果此时钟是来自于外部晶振的差分时钟，就设置为**Differential**；

<img src="pics/7.png" style="zoom:80%;" />

如果此时钟是来自于内部PLL，而且因为PLL自带buffer，所以就设置为**No Buffer**。

原因在于外部的时钟进来可能会不稳定，所以系统会为其加上一个buffer，使其更稳定：

<img src="pics/8.png" style="zoom:100%;" />



> 制作ucf文件

在打开MIG IP的配置时，选择DDR3的芯片型号以及位宽，之后选择Fixed Pin，再对照原理图配置管脚，保存后即生成ucf文件。



## 2.3 AXI4 接口

如下所示，是MIG IP核的端口信号，其中包含了与DDR3的接口与用户端的接口信号等。

```verilog
mig_7series_0 u_mig_7series_0 (
    // Memory interface ports
    
    // 这些端口信号是与FPGA直接相连的信号
    .ddr3_addr                  (ddr3_addr),  // output [13:0]      ddr3_addr
    .ddr3_ba                    (ddr3_ba),  // output [2:0]     ddr3_ba
    .ddr3_cas_n                 (ddr3_cas_n),  // output            ddr3_cas_n
    .ddr3_ck_n                  (ddr3_ck_n),  // output [0:0]       ddr3_ck_n
    .ddr3_ck_p                  (ddr3_ck_p),  // output [0:0]       ddr3_ck_p
    .ddr3_cke                   (ddr3_cke),  // output [0:0]        ddr3_cke
    .ddr3_ras_n                 (ddr3_ras_n),  // output            ddr3_ras_n
    .ddr3_reset_n               (ddr3_reset_n),  // output          ddr3_reset_n
    .ddr3_we_n                  (ddr3_we_n),  // output         ddr3_we_n
    .ddr3_dq                    (ddr3_dq),  // inout [15:0]     ddr3_dq
    .ddr3_dqs_n                 (ddr3_dqs_n),  // inout [1:0]       ddr3_dqs_n
    .ddr3_dqs_p                 (ddr3_dqs_p),  // inout [1:0]       ddr3_dqs_p

    // DDR3初始化完成信号,当该信号为低时,对MIG IP核的读写都是无效的.
    .init_calib_complete            (init_calib_complete),  // output           init_calib_complete

    .ddr3_cs_n                      (ddr3_cs_n),  // output [0:0]       ddr3_cs_n
    .ddr3_dm                        (ddr3_dm),  // output [1:0]     ddr3_dm
    .ddr3_odt                       (ddr3_odt),  // output [0:0]        ddr3_odt

    // Application interface ports

	// MIG IP核产生给用户端的时钟
    .ui_clk                         (ui_clk),  // output            ui_clk

	// MIG IP核输出给用户的复位信号,注意是高有效.
    .ui_clk_sync_rst                (ui_clk_sync_rst),  // output           ui_clk_sync_rst

	// 类似于PLL的locked信号,DDR3内部也会有一个PLL,当它稳定是就会将此locked信号拉高.用不上可以悬空.
    .mmcm_locked                    (mmcm_locked),  // output           mmcm_locked

	// 复位,低有效
    .aresetn                        (aresetn),  // input            aresetn

    // 校准刷新使用:这里用不上,输入可以置零,输出可以悬空.
    .app_sr_req                     (app_sr_req),  // input         app_sr_req
    .app_ref_req                    (app_ref_req),  // input            app_ref_req
    .app_zq_req                     (app_zq_req),  // input         app_zq_req
    
    .app_sr_active                  (app_sr_active),  // output         app_sr_active
    .app_ref_ack                    (app_ref_ack),  // output           app_ref_ack
    .app_zq_ack                     (app_zq_ack),  // output            app_zq_ack

    // Slave Interface Write Address Ports

	// AXI4从机地址端口信号
    .s_axi_awid                     (s_axi_awid),  // input [3:0]           s_axi_awid
    .s_axi_awaddr                   (s_axi_awaddr),  // input [27:0]            s_axi_awaddr
    .s_axi_awlen                    (s_axi_awlen),  // input [7:0]          s_axi_awlen
    .s_axi_awsize                   (s_axi_awsize),  // input [2:0]         s_axi_awsize
    .s_axi_awburst                  (s_axi_awburst),  // input [1:0]            s_axi_awburst
    .s_axi_awlock                   (s_axi_awlock),  // input [0:0]         s_axi_awlock
    .s_axi_awcache                  (s_axi_awcache),  // input [3:0]            s_axi_awcache
    .s_axi_awprot                   (s_axi_awprot),  // input [2:0]         s_axi_awprot
    .s_axi_awqos                    (s_axi_awqos),  // input [3:0]          s_axi_awqos
    .s_axi_awvalid                  (s_axi_awvalid),  // input          s_axi_awvalid
    .s_axi_awready                  (s_axi_awready),  // output         s_axi_awready


    // Slave Interface Write Data Ports
    .s_axi_wdata                    (s_axi_wdata),  // input [127:0]            s_axi_wdata
    .s_axi_wstrb                    (s_axi_wstrb),  // input [15:0]         s_axi_wstrb
    .s_axi_wlast                    (s_axi_wlast),  // input            s_axi_wlast
    .s_axi_wvalid                   (s_axi_wvalid),  // input           s_axi_wvalid
    .s_axi_wready                   (s_axi_wready),  // output          s_axi_wready

    // Slave Interface Write Response Ports
	// 从机写响应端口
    .s_axi_bid                      (s_axi_bid),  // output [3:0]           s_axi_bid
    .s_axi_bresp                    (s_axi_bresp),  // output [1:0]         s_axi_bresp
    .s_axi_bvalid                   (s_axi_bvalid),  // output          s_axi_bvalid
    .s_axi_bready                   (s_axi_bready),  // input           s_axi_bready

    // Slave Interface Read Address Ports
	// 从机读地址端口
    .s_axi_arid                     (s_axi_arid),  // input [3:0]           s_axi_arid
    .s_axi_araddr                   (s_axi_araddr),  // input [27:0]            s_axi_araddr
    .s_axi_arlen                    (s_axi_arlen),  // input [7:0]          s_axi_arlen
    .s_axi_arsize                   (s_axi_arsize),  // input [2:0]         s_axi_arsize
    .s_axi_arburst                  (s_axi_arburst),  // input [1:0]            s_axi_arburst
    .s_axi_arlock                   (s_axi_arlock),  // input [0:0]         s_axi_arlock
    .s_axi_arcache                  (s_axi_arcache),  // input [3:0]            s_axi_arcache
    .s_axi_arprot                   (s_axi_arprot),  // input [2:0]         s_axi_arprot
    .s_axi_arqos                    (s_axi_arqos),  // input [3:0]          s_axi_arqos
    .s_axi_arvalid                  (s_axi_arvalid),  // input          s_axi_arvalid
    .s_axi_arready                  (s_axi_arready),  // output         s_axi_arready

    // Slave Interface Read Data Ports
	// 从机读数据端口
    .s_axi_rid                      (s_axi_rid),  // output [3:0]           s_axi_rid
    .s_axi_rdata                    (s_axi_rdata),  // output [127:0]           s_axi_rdata
    .s_axi_rresp                    (s_axi_rresp),  // output [1:0]         s_axi_rresp
    .s_axi_rlast                    (s_axi_rlast),  // output           s_axi_rlast
    .s_axi_rvalid                   (s_axi_rvalid),  // output          s_axi_rvalid
    .s_axi_rready                   (s_axi_rready),  // input           s_axi_rready

    // System Clock Ports
    // MIG IP核的系统时钟
    .sys_clk_i                      (sys_clk_i),
    // MIG IP核的复位信号,虽然没有_n后缀,但是在配置时是设置为了低有效的.
    .sys_rst                        (sys_rst) // input sys_rst
);
```



## 2.4 Native接口

- Native接口的特点

  不像AXI4接口，Native接口不支持突发，为地址+数据的模式，读写通过app_cmd控制(0表示写，1表示读)。

  不能同时进行读写操作，如果同时产生了读写请求，需要进行仲裁。

- 接口信号

```verilog
mig_7series_0 u_mig_7series_0(
    // Memory interface ports
    // 与DDR3进行物理连接的端口信号
    .ddr3_addr          (ddr3_addr),  // output [13:0]		ddr3_addr
    .ddr3_ba            (ddr3_ba),  // output [2:0]		ddr3_ba
    .ddr3_cas_n         (ddr3_cas_n),  // output			ddr3_cas_n
    .ddr3_ck_n          (ddr3_ck_n),  // output [0:0]		ddr3_ck_n
    .ddr3_ck_p          (ddr3_ck_p),  // output [0:0]		ddr3_ck_p
    .ddr3_cke           (ddr3_cke),  // output [0:0]		ddr3_cke
    .ddr3_ras_n         (ddr3_ras_n),  // output			ddr3_ras_n
    .ddr3_reset_n       (ddr3_reset_n),  // output			ddr3_reset_n
    .ddr3_we_n          (ddr3_we_n),  // output			ddr3_we_n
    .ddr3_dq            (ddr3_dq),  // inout [15:0]		ddr3_dq
    .ddr3_dqs_n         (ddr3_dqs_n),  // inout [1:0]		ddr3_dqs_n
    .ddr3_dqs_p         (ddr3_dqs_p),  // inout [1:0]		ddr3_dqs_p
    .ddr3_cs_n          (ddr3_cs_n),  // output [0:0]       ddr3_cs_n
    .ddr3_dm            (ddr3_dm),  // output [1:0]     ddr3_dm
    .ddr3_odt           (ddr3_odt),  // output [0:0]        ddr3_odt

    // 初始化完成信号
    .init_calib_complete(init_calib_complete),  // output			init_calib_complete

    // Application interface ports
    .app_addr           (app_addr),  // input [27:0]		app_addr
    .app_cmd            (app_cmd),  // input [2:0]		app_cmd

    // app_en与app_rdy类似于AXI4的valid和ready信号一样,也是握手机制.
    .app_en             (app_en),  // input				app_en
    .app_rdy            (app_rdy),  // output           app_rdy

    .app_wdf_data       (app_wdf_data),  // input [127:0]		app_wdf_data
    .app_wdf_end        (app_wdf_end),  // input				app_wdf_end
    .app_wdf_wren       (app_wdf_wren),  // input				app_wdf_wren


    .app_rd_data        (app_rd_data),  // output [127:0]		app_rd_data
    .app_rd_data_valid  (app_rd_data_valid),  // output         app_rd_data_valid

    // 这个end信号和AXI4的last信号不同,不要搞混了.
    .app_rd_data_end    (app_rd_data_end),  // output			app_rd_data_end

    .app_wdf_rdy        (app_wdf_rdy),  // output			app_wdf_rdy
    .app_sr_req         (app_sr_req),  // input			app_sr_req
    .app_ref_req        (app_ref_req),  // input			app_ref_req
    .app_zq_req         (app_zq_req),  // input			app_zq_req
    .app_sr_active      (app_sr_active),  // output			app_sr_active
    .app_ref_ack        (app_ref_ack),  // output			app_ref_ack
    .app_zq_ack         (app_zq_ack),  // output			app_zq_ack
    .app_wdf_mask       (app_wdf_mask),  // input [15:0]        app_wdf_mask

    // 给用户端的时钟
    .ui_clk             (ui_clk),  // output			ui_clk
    .ui_clk_sync_rst    (ui_clk_sync_rst),  // output			ui_clk_sync_rst

    // System Clock Ports
    .sys_clk_i          (sys_clk_i),    // input			sys_clk_i
    .sys_rst            (sys_rst)       // input sys_rst,注意此系统复位低有效.
);
```



# 3. 项目调试

## 3.1 项目实现

> 项目划分

根据总体的功能，首先将主要模块划分为串口模块、DDR读写相关模块、UDP传输模块和ADC模块；

其次，DDR读写和UDP传输之间要有一个桥接模块，即adc_send模块；同理，串口和ADC模块之间也要有联通。

> 调试步骤

（1）将项目总体分为以太网模块、AXI主机读写模块、MIG IP配置、ADC控制模块、串口指令解析以及RGMII接口转换模块；

（2）打通ADMA到DDR再到UDP的链路。用户自产生数据，用vio控制数据发生，写入ADMA之后看上位机能否正常接收；

（3）在用户自产生数据模块，加入DDS，产生正弦波，上位机软件观察波形；

（4）加入指令解析和串口相关的模块；

（5）将DDS替换为ADC控制模块。



## 3.2 指标

带宽利用率，对于千兆以太网而言，设数据速率为1000Mbps，用户数据为16bit，那最大用户数据速率为$1000/16=62.5\rm Mbps$。

在实际应用时，仍设数据位宽为16bit，而ADC的采样速率为**S**，那带宽利用率为$\dfrac{16\times S}{1000}$。

如果ADC采样率很低，那带宽利用率就会很低，项目效率低，不太现实，所以要注意ADC的型号选择。

用户自产生数据发送时，要注意发送的字节数量，受到了以太网数据包长度的限制（最多1500字节），所以突发设置为1024比较合适。



## 3.3 时钟规划

时钟的规划如下图所示：

<img src="pics/10.png" style="zoom:100%;" />

其中，时钟产生模块提供的200M时钟会提供给RGMII模块作为参考时钟。



## 3.4 上位机指令处理

> 测温指令

指令内容为：`55 aa 00 08 00 01 00 00 00 00 00 00 1e f0 `，具体含义：

`55 aa`为帧头，`f0`为帧尾，`1e`为CRC校验，`00`是地址，`08`是数据段的长度，其后边就是数据部分，其中`00 01`就表示测温。



> ADC采集指令

| 帧头  | 地址 | 数据段长度 | ADC采集 | 开始/停止采集/采集N个点 |      N      | CRC  | 帧尾 |       含义       |
| :---: | :--: | :--------: | :-----: | :---------------------: | :---------: | :--: | :--: | :--------------: |
| 55 aa |  00  |     08     |  00 02  |          00 00          | 00 00 00 00 |  78  |  F0  |     开启采集     |
| 55 aa |  00  |     08     |  00 02  |          00 01          | 00 00 00 00 |  1A  |  F0  |     停止采集     |
| 55 aa |  00  |     08     |  00 02  |          00 02          | 00 00 4E 20 |  DA  |  F0  | 采集16'h4e20个点 |
| 55 aa |  00  |     08     |  00 02  |          00 02          | 00 00 04 00 |  E8  |  F0  |   采集1024个点   |

FPGA在收到上位机的指令后，也会向上位机返回一个指令，此指令帧头为`55 BB`，其余都和上位机下发指令相同。



> 串口调试

通过上位机的串口调试助手，自定义发送数据，然后在ILA中抓取关键信号，比如ADC_Start信号的上升沿。



# 4. 问题

> DDR3的时钟是多少？ 

DDR3的时钟为800MHz。DDR3的时钟由MIG IP核提供，此IP核分别有两个输入、输出时钟：

两个输入时钟分别是系统时钟和参考时钟，都可以来自于外部或内部的PLL，系统时钟可以任意，代码里是200M，但参考时钟最好就建议是200M；

两个输出时钟分别是给DDR3的时钟和给用户端的时钟，且两者之间有一定的比例关系。这里给DDR3的时钟是800MHz。由于DDR3的时钟高于400M，所以DDR3时钟和用户时钟`ui_clk`的比例只能是`4:1`，因此用户端时钟就是200M；如果低于400M，比例就可以是`4:1`或`2:1`。


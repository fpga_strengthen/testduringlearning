function[slope_fifo,angle_fit_fifo] = slope_fifo(angle_tan)
phase_sum = 0;               %相位累计修正量
angle_fit_reg = zeros(1,30); %最小二乘法移位寄存器
slope =0;
bn = 0;
cnt = 0;
an = 0;
for ii = 2:length(angle_tan)
    %% 输入数据
    angle_tan_temp = angle_tan(ii);
    angle_tan_delay = angle_tan(ii-1);
    %% 对2pi，中心频率进行修正,对频偏进行修正 并对连续的1或0的相位累积值进行上溢处理
    % 从第300个点开始做修正，保证开始修正时，斜率曲线是平稳的
    
    if(phase_sum>=2*8192)
        cnt = 1;
        flag_p = 1;
        flag_n = 0;
    elseif(phase_sum<=-2*8192)
        cnt = 1;
        flag_p = 0;
        flag_n = 1;
    elseif(cnt>=1 && cnt<=28)
        cnt = cnt + 1;
        flag_p = flag_p;
        flag_n = flag_n;
    else
        cnt = 0;
        flag_p = 0;
        flag_n = 0;
    end
    
    if(phase_sum<=-2*8192)%相位累积超过-2*2pi 下溢处理
        if( angle_tan_temp + 4096 <= angle_tan_delay ) % 过2pi时做2*pi模糊度处理 以及中心频率的修正
            phase_sum = phase_sum + 3*8192 -1280;
        else
            phase_sum = phase_sum + 2*8192 - 1280;
        end
    elseif(phase_sum<2*8192) % 相位累积没有超过2*2pi
        if( angle_tan_temp + 4096 <= angle_tan_delay ) % 过2pi时做2*pi模糊度处理 以及中心频率的修正
            phase_sum = phase_sum + 8192 -1280;
        else
            phase_sum = phase_sum - 1280;
        end
    else% 相位累积超过2*2pi 上溢处理(phase_sum>=2*8192)
        if( angle_tan_temp+4096<=angle_tan_delay )
            phase_sum = phase_sum - 8192 - 1280; % 过2*pi处，相位直接下拉2*pi
        else
            phase_sum =  phase_sum - 2*8192 - 1280;  % 非2*pi处，相位也直接下拉2*pi
        end
    end
    
    angle_fit = angle_tan_temp + phase_sum ;%将累积量叠加到寄存器中的原始相位上
    
    cnt_plot(ii) = cnt;
    flag_p_plot(ii) = flag_p;
    flag_n_plot(ii) = flag_n;
    phase_sum_plot(ii) = phase_sum;
    angle_fit_fifo(ii) = angle_fit;
    
    %% 将修改相位后的数存入移位寄存器 29位移位寄存器，用于最小二乘法计算
    angle_fit_reg = [angle_fit_reg(2:end),angle_fit];
    
    %% -----------------最小二乘法---------------------------------------
    %取N=14个点用于之后进行29点最小二乘法
    %按照最小二乘法公式先进行对应位相乘
    %法1
    if(cnt && flag_p)
        slope = slope - bn + 15*angle_fit_reg(1)+14*angle_fit_reg(30) +2*8192*(15-cnt);
    elseif(cnt && flag_n)
        slope = slope - bn + 15*angle_fit_reg(1)+14*angle_fit_reg(30) -2*8192*(15-cnt);
    else
        slope = slope - bn + 15*angle_fit_reg(1)+14*angle_fit_reg(30);
    end
    
    %法2
    %     an = an - bn + 15*angle_fit_reg(1)+14*angle_fit_reg(30)
    bn = bn + angle_fit_reg(30) - angle_fit_reg(1);
    %
    %     if(cnt)
    %         slope = an + 8192*(29-cnt)*cnt;%4096*(29-cnt)*cnt
    %     else
    %         slope = an;%4096*(29-cnt)*cnt
    %     end
    
    slope_fifo(ii) = slope;%绘图用
end %for ii
figure (102)
plot(cnt_plot*5*10e3,'r');
hold on
plot(flag_p_plot*10e5,'g');
plot(flag_n_plot*10e5,'y');
plot(angle_fit_fifo*50,'k');
plot(slope_fifo,'LineWidth',2);
plot(phase_sum_plot*10,'m');
legend('修正计数','上溢处理指示','下溢处理指示','补偿后的相位折线','斜率','叠加量');
grid on
//+FHEADER //////////////////////////////////////////////////////////////////////
//  版权所有者：                                                                 
//  Copyright 2014. All Rights Reserved.                                         
//  保密级别：                                                                   
//---------------------------------------------------------------------------    
//  文件名 :  can_send_register.v                                                     
//  原始作者：丁云冰                                                             
//  联系方式：                                                                   
// --------------------------------------------------------------------------    
// 版本升级历史：                                                                
// 版本生效日期：                                                                
// 1.0 2014-1-13   丁云冰  第1版                                                 
// 2.0 2015-10-17  郭庆阳  第2版                                                 
//---------------------------------------------------------------------------    
// 关键字 :   写寄存器                                        
//---------------------------------------------------------------------------    
// 模块功能:  write  mcp2515 reg                                                                 
// --------------------------------------------------------------------------    
// 参数文件名：                                                                  
// --------------------------------------------------------------------------    
// 重复使用问题：                                                                
// 复位策略:负跳变沿异步复位信号 i_rst_n                                         
// 时钟域 : i_clk_27M                                                            
// 关键时序: N/A                                                                 
// 是否需要实例引用别的模块 : No                                                 
// 可综合否 : Yes                                                                
//-FHEADER //////////////////////////////////////////////////////////////////////
module can_send_register
       (
         //time & rst
        i_clk_27M,
        i_rst_n,
        
        //input control
        i_send_ok,          //脉冲信号，发送字节完成
        i_send_en,          //电平信号，使能本模块
        
        //input data 
        i_send_add,        //写寄存器地址
        i_send_data,       //写数据
        
        //output control
        o_spi_tx_en,       //电平信号，spi发送使能
        o_send_reg_ok,     //脉冲信号，写入某个寄存器成功 
        
        //output data
        o_spi_data
        );
    input i_clk_27M;
    input i_rst_n;
    input i_send_ok;
    input i_send_en;
    input[7:0] i_send_add;
    input[7:0] i_send_data;
    
    output o_spi_tx_en;
    output o_send_reg_ok;
    output[7:0] o_spi_data;
//////////////////////////////////////////
  `include "parameter.v"
/////////////////////////////////////////   
    reg[2:0] ctrl_cnt;
    reg o_spi_tx_en;
    reg[7:0] o_spi_data;
    //reg o_send_reg_ok;
    wire o_send_reg_ok;
    assign o_send_reg_ok = ((ctrl_cnt == 3'd3) && i_send_ok);
    
////////////////////////////////////////////

    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if(!i_rst_n) 
            ctrl_cnt <= 3'd0;
        else if(ctrl_cnt == 3'd4)
            ctrl_cnt <= 3'd0;
        else if(i_send_ok && (ctrl_cnt != 3'd0))
            ctrl_cnt <= ctrl_cnt + 3'd1;
        else if(i_send_en && (ctrl_cnt >= 3'd1))
            ctrl_cnt <= ctrl_cnt;
        else if(i_send_en)
            ctrl_cnt <= 3'd1;  
    end
    
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if(!i_rst_n) begin
            o_spi_tx_en <= 1'b0;  
            o_spi_data  <= 8'd0;   
            //o_send_reg_ok <= 1'b0;
        end
        else begin
            case(ctrl_cnt)
            3'd1: begin
                o_spi_tx_en <= 1'b1;           
                o_spi_data <= `SEND_REG_CMD;//发送写寄存器指令
            end
            3'd2: begin
                o_spi_tx_en <= 1'b1;
                o_spi_data <= i_send_add;  //发送写寄存器地址            
            end
            3'd3: begin
                o_spi_tx_en <= 1'b1; 
                o_spi_data <= i_send_data; //发送写寄存器数据
            end
            3'd4: begin
                o_spi_tx_en <= 1'b0;     
                o_spi_data <= 8'd0;      
                //o_send_reg_ok <= 1'b1;    //写一个字节数据成功
            end                    
            default: begin
                o_spi_tx_en <= 1'b0;  
                o_spi_data  <= 8'd0;   
                //o_send_reg_ok <= 1'b0;                     
            end
            endcase
        end
    end
       
endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date: 2024/08/20 15:07:40
// Module Name: ip_send
// Description: 将UDP层发送的数据进行打包发送。
// Revision 0.01 - File Created
//////////////////////////////////////////////////////////////////////////////////

module ip_send #(
	parameter       LOCAL_IP_ADDR  = 32'h0,
	parameter       TARGET_IP_ADDR = 32'h0
	)(
	// 时钟和复位
	input wire			i_clk,
	input wire			i_rst_n,

	/*---------------- udp_send 模块交互信号--------------------*/
	input wire			i_udp_tx_data_vld,		// 数据有效信号
	input wire			i_udp_tx_data_last,		// MAC层数据的最后一个数据的标志
	input wire	[7:0]	iv_udp_tx_data,			// 从MAC数据包解析出MAC层数据
	input wire	[15:0]	iv_udp_tx_length,		// 帧类型
	input wire	[7:0]	iv_tx_type,

	/*---------------- mac_send 模块交互信号--------------------*/
	output	reg			o_ip_tx_data_vld,
	output	reg			o_ip_tx_data_last,
	output 	reg	[15:0]	ov_ip_tx_length,
	output	reg	[7:0]	ov_ip_tx_data,

	output	reg			o_mac_list_rden,
	output	reg	[31:0]	ov_list_ip_addr
);

/*-----------------------------------------------*\
				信号与连接线定义
\*-----------------------------------------------*/	
	
	reg				tx_data_vld_d0;
	reg				tx_data_vld_d1;
	reg		[10:0]	cnt_tx_byte;			// 对接收的字节进行计数
	wire	[7:0]	udp_tx_data_delay20;	// 将数据演示20拍
	reg 	[15:0]	package_id;				// 标识
	reg		[15:0]	ip_head_check_sum;		// ip首部校验和
	// 用于计算首部校验和
	reg		[31:0]	add_sum_0;
	reg		[31:0]	add_sum_1;
	reg		[31:0]	add_sum_2;
	reg		[31:0]	check_sum_temp;

	reg		[7:0]	type_r;

/*-----------------------------------------------*\
			发送MAC查找使能信号和IP地址
\*-----------------------------------------------*/	

	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n) begin
			tx_data_vld_d0 <= 1'b0;
			tx_data_vld_d0 <= 1'b0;
		end else begin
			tx_data_vld_d0 <= i_udp_tx_data_vld;
			tx_data_vld_d1 <= tx_data_vld_d0;
		end
	end

	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n) begin
			o_mac_list_rden <= 1'b0;
			ov_list_ip_addr <= 32'h0;
		end 
		else if(tx_data_vld_d0 && (~tx_data_vld_d1))begin	// 取上升沿
			o_mac_list_rden <= 1'b1;
			ov_list_ip_addr <= LOCAL_IP_ADDR;
		end
	end

/*-----------------------------------------------*\
					锁存length
\*-----------------------------------------------*/	

	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n)
			ov_ip_tx_length <= 16'b0;
		else if(i_udp_tx_data_vld)
			ov_ip_tx_length <= iv_udp_tx_length + 'd20;
		else
			ov_ip_tx_length <= ov_ip_tx_length;
	end

/*-----------------------------------------------*\
					锁存type
\*-----------------------------------------------*/	

	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n)
			type_r <= 8'b0;
		else 
			type_r <= iv_tx_type;
	end

/*-----------------------------------------------*\
				cnt_tx_byte
\*-----------------------------------------------*/	

	always @(posedge i_clk or negedge i_rst_n) begin 
		if(~i_rst_n)
			cnt_tx_byte <= 11'd0;
		else if(cnt_tx_byte == ov_ip_tx_length - 1)
			cnt_tx_byte <= 11'd0;
		else if(i_udp_tx_data_vld || (cnt_tx_byte != 0))
			cnt_tx_byte <= cnt_tx_byte + 1'b1;
		else
			cnt_tx_byte <= cnt_tx_byte;
	end

/*-----------------------------------------------*\
		将数据打拍20拍,留出添加IP头部的时间
\*-----------------------------------------------*/	

	data_delay20  ip_delay20(
  		.A   (19),      				// If delay N clocks, input (N-1) here.
  		.D   (iv_udp_tx_data),      	// input wire [7 : 0] D
  		.CLK (i_clk),  					// input wire CLK
  		.Q   (udp_tx_data_delay20)      // output wire [7 : 0] Q
	);

/*-----------------------------------------------*\
					标识
\*-----------------------------------------------*/

	// 每发完一个数据包,标识就加1
	always @(posedge i_clk or negedge i_rst_n) begin : proc_
		if(~i_rst_n)
			package_id <= 16'd0;
		else if(o_ip_tx_data_last)
			package_id <= package_id + 1'b1;
		else
			package_id <= package_id;
	end

/*-----------------------------------------------*\
					计算IP首部校验和
\*-----------------------------------------------*/

	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n) begin
			add_sum_0 <= 32'b0;
			add_sum_1 <= 32'b0;
			add_sum_2 <= 32'b0;
			check_sum_temp <= 32'h0;
		end 
		else begin
			add_sum_0 <= 16'h4500 + ov_ip_tx_length + package_id;
			add_sum_1 <= 16'h4000 + 16'h8011 + LOCAL_IP_ADDR[31:16];
			add_sum_2 <= LOCAL_IP_ADDR[15:0] + TARGET_IP_ADDR[31:16] + TARGET_IP_ADDR[15:0];
			check_sum_temp <= add_sum_0 + add_sum_1 + add_sum_2;
		end
	end

	// 时序说明
	// 用于计算首部校验和的数据中,只有tx_length是一直变的;而当收到正确的tx_length时,是在cnt_tx_byte=3后;
	// 此时的add_sum_0才是正确的,然后计算正确的check_sum_temp又比cnt_tx_byte=3晚一拍即cnt_tx_byte=4时.
	// 计算的首部之和就在cnt_tx_byte=5时有效。所以在cnt_tx_byte=5时将其高低16位相加、在=6时对相加结果取反。
	// 也就是在cnt_tx_byte=7时得到了正确的首部校验和了.
	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n)
			ip_head_check_sum <= 16'h0;
		else if(cnt_tx_byte == 11'd5)
			ip_head_check_sum <= check_sum_temp[31:16] + check_sum_temp[15:0];
		else if(cnt_tx_byte == 11'd6)
			ip_head_check_sum <= ~ip_head_check_sum;
		else
			ip_head_check_sum <= ip_head_check_sum;
	end

/*-----------------------------------------------*\
				IP数据包组包发送
\*-----------------------------------------------*/	

	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n)
			ov_ip_tx_data <= 8'h0;
		else begin
			case (cnt_tx_byte)
				0 : ov_ip_tx_data <= {4'h4,4'h5};	// 版本+首部长度,共1字节
				1 : ov_ip_tx_data <= 8'h0;			// 服务类型,一般为8'h0

				2 : ov_ip_tx_data <= ov_ip_tx_length[15:8];		// 总长度,高字节
				3 : ov_ip_tx_data <= ov_ip_tx_length[7:0];		// 总长度,低字节

				4 : ov_ip_tx_data <= package_id[15:8];			// 标识,高字节
				5 : ov_ip_tx_data <= package_id[7:0];			// 标识,低字节

				// 标记(3bit)+分段偏移(8bit+5bit),共两字节
				6 : ov_ip_tx_data <= {3'b010,5'b0};				// 标记通常默认为3'b010
				7 : ov_ip_tx_data <= 8'h0;

				// 生存时间
				8 : ov_ip_tx_data <= 8'h80;						// Windows系统通常默认为8'h80
				// 协议,为UDP
				9 : ov_ip_tx_data <= type_r;					// UDP:8'd17; TCP:8'd6; ICMP:8'd1

				// 发送首部校验和
				10 : ov_ip_tx_data <= ip_head_check_sum[15:8];		// 先发送高字节
				11 : ov_ip_tx_data <= ip_head_check_sum[7:0];		// 后发送低字节

				// 发送源地址
				12 : ov_ip_tx_data <= LOCAL_IP_ADDR[31:24];
				13 : ov_ip_tx_data <= LOCAL_IP_ADDR[23:16];
				14 : ov_ip_tx_data <= LOCAL_IP_ADDR[15: 8];
				15 : ov_ip_tx_data <= LOCAL_IP_ADDR[ 7: 0];

				// 发送目的地址
				16 : ov_ip_tx_data <= TARGET_IP_ADDR[31:24];
				17 : ov_ip_tx_data <= TARGET_IP_ADDR[23:16];
				18 : ov_ip_tx_data <= TARGET_IP_ADDR[15: 8];
				19 : ov_ip_tx_data <= TARGET_IP_ADDR[ 7: 0];
				default : ov_ip_tx_data <= udp_tx_data_delay20;
			endcase
		end
	end

/*-----------------------------------------------*\
				数据结束标志
\*-----------------------------------------------*/	

	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n)
			o_ip_tx_data_last <= 1'b0;
		else if(cnt_tx_byte == ov_ip_tx_length - 1)
			o_ip_tx_data_last <= 1'b1;
		else
			o_ip_tx_data_last <= 1'b0;
	end

/*-----------------------------------------------*\
				数据有效信号
\*-----------------------------------------------*/	

	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n)  
			o_ip_tx_data_vld<= 1'b0;
		else if(o_ip_tx_data_last)
			o_ip_tx_data_vld <= 1'b0;
		else  if(i_udp_tx_data_vld)		// 存疑？？ udp_vld和数据的同步中是否包含UDP头部？？
			o_ip_tx_data_vld <= 1'b1;
		else
			o_ip_tx_data_vld <= o_ip_tx_data_vld;
	end

endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2024/09/15 22:47:36
// Design Name: 
// Module Name: rgmii_rx
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module rgmii_rx (
	input 	wire			i_rgmii_rx_clk,    // Clock
	input 	wire			i_rgmii_rx_ctl,
	input 	wire	[3:0]	iv_rgmii_rxd,
	input	wire			i_rgmii_cken,

	output	wire			o_gmii_rx_clk,
	output	wire			o_gmii_rx_dv,
	output	wire	[7:0]	ov_gmii_rxd
);
	
	wire	[1:0]	gmii_rx_ctl;
	(* dont_touch="true" *)wire	[3:0]	gmii_rx_low;
	(* dont_touch="true" *)wire	[3:0]	gmii_rx_high;
	
	assign	o_gmii_rx_dv  = gmii_rx_ctl[0] & gmii_rx_ctl[1];
	assign	o_gmii_rx_clk = i_rgmii_rx_clk;

	assign	gmii_rx_low  = ov_gmii_rxd[3:0];
	assign	gmii_rx_high = ov_gmii_rxd[7:4];

	// 首先获取有效的DV信号
	// 根据正常模式下的时序图可知,RX_CTL在时钟上升沿时采样输出DV信号;下降沿采样输出ERR信号.
	IDDR #(
      	.DDR_CLK_EDGE("SAME_EDGE"), // "OPPOSITE_EDGE", "SAME_EDGE"  or "SAME_EDGE_PIPELINED" 
      	.INIT_Q1(1'b0), 		// Initial value of Q1: 1'b0 or 1'b1
      	.INIT_Q2(1'b0), 		// Initial value of Q2: 1'b0 or 1'b1
      	.SRTYPE("SYNC") 		// Set/Reset type: "SYNC" or "ASYNC" 
   	) IDDR_rx_inst (
      	.Q1(gmii_rx_ctl[0]), 	// 1-bit output for positive edge of clock
      	.Q2(gmii_rx_ctl[1]), 	// 1-bit output for negative edge of clock
      	.C(i_rgmii_rx_clk),   	// 1-bit clock input
      	.CE(i_rgmii_cken), 		// 1-bit clock enable input
      	.D(i_rgmii_rx_ctl),   	// 1-bit DDR data input
      	.R(1'b0),   			// 1-bit reset, which is high valid.
      	.S(1'b0)    			// 1-bit set
   	);

   	// 在RXC的上升沿会传输数据的低4位,下降沿传输数据的高4位.
   	genvar i;
   	generate
   		for (i = 0; i < 4; i=i+1) begin : data_trans
   			IDDR #(
      			.DDR_CLK_EDGE("SAME_EDGE"), // "OPPOSITE_EDGE", "SAME_EDGE"  or "SAME_EDGE_PIPELINED" 
      			.INIT_Q1(1'b0), 			// Initial value of Q1: 1'b0 or 1'b1
      			.INIT_Q2(1'b0), 			// Initial value of Q2: 1'b0 or 1'b1
      			.SRTYPE("SYNC") 			// Set/Reset type: "SYNC" or "ASYNC" 
   			) IDDR_rxd_inst (
      			.Q1(ov_gmii_rxd[i]), 		// 1-bit output for positive edge of clock
      			.Q2(ov_gmii_rxd[4+i]), 		// 1-bit output for negative edge of clock
      			.C(i_rgmii_rx_clk),   		// 1-bit clock input
      			.CE(1'b1), 			// 1-bit clock enable input
      			.D(iv_rgmii_rxd[i]),   		// 1-bit DDR data input
      			.R(1'b0),   				// 1-bit reset, which is high valid.
      			.S(1'b0)    				// 1-bit set
   			);
   		end
   	endgenerate

endmodule
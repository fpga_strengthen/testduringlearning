`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date: 2024/08/27 14:38:26
// Module Name: icmp_receive
// Tool Versions: 
// Description: icmp接收端,获取标识符和序号,并跨时钟域到发送端,经发送方校验后给与相同回应的数据包.
//				即实现一个ping功能.
// Revision 0.01 - File Created
//////////////////////////////////////////////////////////////////////////////////

module icmp_receive (
	input wire			i_icmp_rx_clk,   
	input wire			i_icmp_tx_clk,
	input wire			i_icmp_tx_rst_n,
	input wire			i_icmp_rx_rst_n,  	// Asynchronous reset active low

	input wire			i_icmp_rx_vld,
	input wire	[7:0]	iv_icmp_rx_data,
	input wire			i_icmp_rx_last,
	input wire	[15:0]	iv_icmp_rx_length,

	output reg			o_icmp_reply_req,	// 回复包请求
	output reg	[15:0]	ov_icmp_identity,	// 标识符
	output reg	[15:0]	ov_icmp_sequence	// 序号
);

/*-----------------------------------------------*\
					信号定义
\*-----------------------------------------------*/
	
	reg		[10:0]	cnt_rx_byte;		// 接收字节计数
	reg		[ 7:0]	rx_icmp_type;		// ICMP报文类型
	reg		[15:0]	rx_icmp_identity;	// icmp标识符
	reg		[15:0]	rx_icmp_sequence;	// 序号

	// FIFO
	reg				icmp_wren;			// FIFO写使能
	reg		[31:0]	icmp_din;			// 写数据
	wire			icmp_rden;
	wire			icmp_rdempty;
	wire			icmp_wrfull;
	wire	[31:0]	icmp_dout;

	// FIFO读使能
	assign	icmp_rden = ~icmp_rdempty;

/*-----------------------------------------------*\
			接收ICMP报文并提取标识符和序号
\*-----------------------------------------------*/
	
	always @(posedge i_icmp_rx_clk or negedge i_icmp_rx_rst_n) begin
		if(~i_icmp_rx_rst_n)
			cnt_rx_byte <= 11'd0;
		else if(i_icmp_rx_vld)
			cnt_rx_byte <= cnt_rx_byte + 1'b1;
		else
			cnt_rx_byte <= 11'd0;
	end

	// 接收ICMP报文类型(1字节)
	always @(posedge i_icmp_rx_clk or negedge i_icmp_rx_rst_n) begin
		if(~i_icmp_rx_rst_n)
			rx_icmp_type <= 8'h0;
		else if(cnt_rx_byte == 11'd0)
			rx_icmp_type <= iv_icmp_rx_data;
		else
			rx_icmp_type <= rx_icmp_type;
	end

	// 接收标识符
	always @(posedge i_icmp_rx_clk or negedge i_icmp_rx_rst_n) begin
		if(~i_icmp_rx_rst_n) 
			rx_icmp_identity <= 16'h0;
		else if((cnt_rx_byte == 11'd4)||(cnt_rx_byte == 11'd5))
			rx_icmp_identity <= {rx_icmp_identity[7:0],iv_icmp_rx_data};
		else
			rx_icmp_identity <= rx_icmp_identity;
	end

	// 接收序号
	always @(posedge i_icmp_rx_clk or negedge i_icmp_rx_rst_n) begin
		if(~i_icmp_rx_rst_n)
			rx_icmp_sequence <= 16'h0;
		else if((cnt_rx_byte == 11'd6)||(cnt_rx_byte == 11'd7))
			rx_icmp_sequence <= {rx_icmp_sequence[7:0],iv_icmp_rx_data};
		else
			rx_icmp_sequence <= rx_icmp_sequence;
	end

/*-----------------------------------------------*\
					写FIFO准备
\*-----------------------------------------------*/

	always @(posedge i_icmp_rx_clk or negedge i_icmp_rx_rst_n) begin
		if(~i_icmp_rx_rst_n) begin
			icmp_din  <= 32'h0;
			icmp_wren <= 1'b0;
		end 
		else if(i_icmp_rx_last)begin
			icmp_din  <= {rx_icmp_identity,rx_icmp_sequence};
			icmp_wren <= 1'b1;
		end
		else begin 
			icmp_din  <= 32'h0;
			icmp_wren <= 1'b0;
		end
	end

/*-----------------------------------------------*\
					FIFO例化
\*-----------------------------------------------*/

	fifo_w32xd16 rx_icmp_fifo (
  		.rst    (~i_icmp_rx_rst_n),      // input wire rst
  		.wr_clk (i_icmp_rx_clk),  		// input wire wr_clk
  		.rd_clk (i_icmp_tx_clk),  		// input wire rd_clk
  		.din    (icmp_din),        		// input wire [31 : 0] din
  		.wr_en  (icmp_wren),    		// input wire wr_en
  		.rd_en  (icmp_rden),    		// input wire rd_en
  		.dout   (icmp_dout),      		// output wire [31 : 0] dout
  		.full   (icmp_wrfull),      	// output wire full
  		.empty  (icmp_rdempty)    		// output wire empty
	);
/*-----------------------------------------------*\
					FIFO输出
\*-----------------------------------------------*/
	
	always @(posedge i_icmp_tx_clk or negedge i_icmp_tx_rst_n) begin
		if(~i_icmp_tx_rst_n) begin
			o_icmp_reply_req <= 1'b0;
			ov_icmp_sequence <= 16'h0;
			ov_icmp_identity <= 16'h0;
		end 
		else if(icmp_rden)begin
			o_icmp_reply_req <= 1'b1;
			ov_icmp_sequence <= icmp_dout[15:0];
			ov_icmp_identity <= icmp_dout[31:16];
		end
		else begin
			o_icmp_reply_req <= 1'b0;
			ov_icmp_sequence <= 16'h0;
			ov_icmp_identity <= 16'h0;
		end
	end

endmodule
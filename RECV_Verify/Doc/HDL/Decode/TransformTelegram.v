/*=======================================================================================*\
  Filename    : TransformTelegram.v
  Author      : 郭庆阳
  time        ：2016.11.10
  Description : 该模块完成对11bit报文转8比特报文
                1.首先接收1023bit报文，并将11bit转换为8bit，在最后的1个字节加的最后1bit做补
                  0处理
                2.转换后的报文写入RAM的同时做CRC32D8的crc计算，并将计算结果存到寄存器组中
                3.在接收到shift_ok后开始读RAM并做CRC32D8的校验，并与2中寄存器组的结果进行比
                  较，若相同将crc32_check_right拉高
                4.在指示新报文的下降沿来时置高新报文标志，如果同时crc32_check_right也处于高
                  电平，那么将telegram_trans_ready拉高。指示组帧模块来取数           
               
  Called by   : 
  Revision History :
  Author              Date                  Version          Description
  郭庆阳              20161110              V1.0.0
  郭庆阳              20161128              V1.1.0    将crc32计算的结果给组帧模块用，不再自己用了                                                             
  Email       : 
  Company     :

\*========================================================================================*/
module TransformTelegram(
        i_clk_81M,
        i_rst_n, 
        //与shift_bit模块接口
        i_shift_ok,
        i_new_msg_flag,
        i_data_start,
        i_data_shift_sck,
        iv_data_shift,
        //与组帧模块接口
        i_telegram_transport_en,
        i_telegram_data_sck,
        ov_telegram_data,       
        o_telegram_trans_ready        
        );
input i_clk_81M;
input i_rst_n;
input i_shift_ok;
input i_new_msg_flag;
input i_data_start;
input i_data_shift_sck;
input [10:0]iv_data_shift;

input i_telegram_transport_en;
input i_telegram_data_sck;
output reg [7:0]ov_telegram_data;       
output reg o_telegram_trans_ready;
////////////////////////////////////////
    `include "../Crc/Crc32_D8_Gene.v" 
////////////////////////////////////////

//----------------信号声明--------------
reg  data_start_r1;
reg  data_start_p0;
reg  data_start_p1;
reg  data_shift_sck_r1;
reg  data_shift_sck_p;
reg  shift_ok_r;
reg  shift_ok_p;
reg  new_msg_flag_r1;
reg  new_msg_flag_n;
reg  new_msg_flag_p;
reg  [1:0]cnt_ready;
reg  send_msg_ctrl;
reg  [10:0]data_shift_temp1;
reg  [10:0]data_shift_temp2;
reg  [6:0]cnt_read_data;
reg  [2:0]cnt_6;
reg  [7:0]data_8bit;
reg  wr_ram_en; 
reg  [7:0]wr_ram_addr; 
reg  [7:0]wr_ram_data;
reg  rd_ram_en;
reg  rd_ram_en_r1;
reg  rd_ram_en_r2; 
reg  [7:0]rd_ram_addr;      //涵盖了寄存器组的地址
wire [7:0]rd_ram_data;
  
reg  [31:0]LFSR_CRC32_D8;
reg  crc32_clr_ok;           //crc32计算结果寄存器的清0校验，高电平表示清0成功

reg  transport_en_r1;
reg  transport_en_r2;
wire transport_en_p;

reg  telegram_data_sck_r1;
reg  telegram_data_sck_r2;
wire telegram_data_sck_p;

////////////////////////////////////////////////////////////////////////////////

//同步完成标志取沿
always @(negedge i_rst_n or posedge i_clk_81M) 
begin
    if (!i_rst_n)begin
        shift_ok_r <= 1'b0;
        shift_ok_p <= 1'b0;
    end
    else begin
        shift_ok_r <= i_shift_ok;
        shift_ok_p <= i_shift_ok && (~shift_ok_r);
    end
end //always

//新报文指示信号取沿:这里的即new_msg_flag_n落后于start_p；
always @(negedge i_rst_n or posedge i_clk_81M) 
begin
    if (!i_rst_n)begin
        new_msg_flag_r1 <= 1'b0;
        new_msg_flag_n  <= 1'b0;
        new_msg_flag_p  <= 1'b0;
    end 
    else begin
        new_msg_flag_r1 <= i_new_msg_flag;
        new_msg_flag_n  <= (~i_new_msg_flag) && new_msg_flag_r1;
        new_msg_flag_p  <= i_new_msg_flag && (~new_msg_flag_r1);
    end   
end //always

//一定是shift_ok比new_msg_flag_n早来一点点
always @(negedge i_rst_n or posedge i_clk_81M) 
begin
    if (!i_rst_n)
        cnt_ready <= 2'd0;
    else if(cnt_ready == 2'd2)
        cnt_ready <= 2'd0;
    else if(new_msg_flag_p)
        cnt_ready <= 2'd0;
    else if(shift_ok_p)
        cnt_ready <= 2'd1;
    else if(new_msg_flag_n && cnt_ready != 2'd0)
        cnt_ready <= 2'd2;
    else
        cnt_ready <= cnt_ready;
end //always

//----------------------------------------------------------------------------//

//帧开始标志取沿
always @(negedge i_rst_n or posedge i_clk_81M) 
begin
    if (!i_rst_n)begin
        data_start_r1 <= 1'b0;
        data_start_p0 <= 1'b0;
        data_start_p1 <= 1'b0;
    end
    else begin
        data_start_r1 <= i_data_start;
        data_start_p0 <= i_data_start && (~data_start_r1);
        data_start_p1 <= data_start_p0;
    end    
end //always

//发送1023bit的控制信号 接收完一次新报文后在发送之前拉高
always @(negedge i_rst_n or posedge i_clk_81M) 
begin
    if (!i_rst_n)
        send_msg_ctrl <= 1'b0;
    else if(rd_ram_addr != 8'd0)//来取数据时可以拉低
        send_msg_ctrl <= 1'b0;
    else if(new_msg_flag_p)//误码发生，复位为0
        send_msg_ctrl <= 1'b0;
    else if(cnt_ready == 2'd2)//新数据到来，准备发送
        send_msg_ctrl <= 1'b1;
    else
        send_msg_ctrl <= send_msg_ctrl;
end //always

//数据采样时钟取沿
always @(negedge i_rst_n or posedge i_clk_81M) 
begin
    if (!i_rst_n)begin
        data_shift_sck_r1 <= 1'b0;
        data_shift_sck_p  <= 1'b0; 
    end
    else begin
        data_shift_sck_r1 <= i_data_shift_sck;    
        data_shift_sck_p  <= i_data_shift_sck && (~data_shift_sck_r1);
    end
end //always

//接收数据暂存
always @(negedge i_rst_n or posedge i_clk_81M) 
begin
    if (!i_rst_n)begin
        data_shift_temp1 <= 11'b0;
        data_shift_temp2 <= 11'b0;      
    end
    else if(data_shift_sck_p) begin
        data_shift_temp1 <= iv_data_shift;
        data_shift_temp2 <= data_shift_temp1;     
    end
end //always

//接收数据计数
always @(negedge i_rst_n or posedge i_clk_81M) 
begin
    if (!i_rst_n)
        cnt_read_data <= 7'd0;
    else if(data_start_p0)
        cnt_read_data <= 7'd0;
    else if(cnt_read_data == 7'd93 && cnt_6 == 3'd6)
        cnt_read_data <= 7'd0;
    else if(data_shift_sck_p)
        cnt_read_data <= cnt_read_data + 7'd1;
    else
        cnt_read_data <= cnt_read_data;
end //always

//----------------------------------------------------------------------------//

//11bit转8bit逻辑的控制器
always @(negedge i_rst_n or posedge i_clk_81M) 
begin
    if (!i_rst_n)
        cnt_6 <= 3'd0;
    else if(data_start_p0)
        cnt_6 <= 3'd0;
    else if(cnt_6 > 3'd6)//两个shift_sck_p相差6个87M时钟
        cnt_6 <= 3'd0;
    else if(data_shift_sck_p) 
        cnt_6 <= 3'd1;
    else if(cnt_6 != 3'd0)
        cnt_6 <= cnt_6 + 3'd1;
    else
        cnt_6 <= cnt_6;
end //always

//-------------------------------11bit转8bit处理逻辑--------------------------//

always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if (!i_rst_n)
        data_8bit <= 8'd0;
    else if(data_start_p0)
        data_8bit <= 8'd0;
    else if(cnt_read_data >= 7'd1 && cnt_read_data <= 7'd92)begin
        case(cnt_read_data[2:0])
            3'b001:begin
                if(cnt_6 == 3'd1)
                    data_8bit <= data_shift_temp1[10:3];     
                else
                    data_8bit <= data_8bit;
            end
            3'b010:begin
                if(cnt_6 == 3'd1)
                    data_8bit <= {data_shift_temp2[2:0],data_shift_temp1[10:6]};
                else
                    data_8bit <= data_8bit;
            end
            3'b011:begin
                if(cnt_6 == 3'd1)
                    data_8bit <= {data_shift_temp2[5:0],data_shift_temp1[10:9]}; 
                else if(cnt_6 == 3'd4) 
                    data_8bit <= data_shift_temp1[8:1];      
                else
                    data_8bit <= data_8bit;
            end
            3'b100:begin
                if(cnt_6 == 3'd1)
                    data_8bit <= {data_shift_temp2[0],data_shift_temp1[10:4]};                           
                else
                    data_8bit <= data_8bit;    
            end
            3'b101:begin
                if(cnt_6 == 3'd1)
                    data_8bit <= {data_shift_temp2[3:0],data_shift_temp1[10:7]};
                else
                    data_8bit <= data_8bit;
            end
            3'b110:begin
                if(cnt_6 == 3'd1)
                    data_8bit <= {data_shift_temp2[6:0],data_shift_temp1[10]};
                else if(cnt_6 == 3'd4)
                    data_8bit <= data_shift_temp1[9:2];    
                else
                    data_8bit <= data_8bit;
            end
            3'b111:begin
                if(cnt_6 == 3'd1)
                    data_8bit <= {data_shift_temp2[1:0],data_shift_temp1[10:5]};                               
                else
                    data_8bit <= data_8bit;
            end
            3'b000:begin
                if(cnt_6 == 3'd1)
                    data_8bit <= {data_shift_temp2[4:0],data_shift_temp1[10:8]};
                else if(cnt_6 == 3'd4)
                    data_8bit <= data_shift_temp1[7:0];
                else
                    data_8bit <= data_8bit;    
            end
        endcase
    end
    else if(cnt_read_data == 7'd93)begin
        if(cnt_6 == 3'd1)
            data_8bit <= {data_shift_temp2[3:0],data_shift_temp1[10:7]};
        else if(cnt_6 == 3'd4)
            data_8bit <= {data_shift_temp1[6:0],1'b0}; 
        else
            data_8bit <= data_8bit;           
    end 
    else
        data_8bit <= data_8bit;        
end//always
//-----------------------------8bit数据写入RAM--------------------------------//
always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if (!i_rst_n)begin
        wr_ram_en   <= 1'b0;
        wr_ram_addr <= 8'd0;        
        wr_ram_data <= 8'd0;    
    end
    else if(data_start_p0)begin
        wr_ram_en   <= 1'b0;
        wr_ram_addr <= 8'd0;         
        wr_ram_data <= 8'd0;  
    end
   else if(send_msg_ctrl || rd_ram_addr != 8'd0)begin//读优先：开始发送期间不允许写数据
        wr_ram_en   <= 1'b0;
        wr_ram_addr <= 8'd0;        
        wr_ram_data <= 8'b0;   
    end
    else if( (cnt_read_data == 7'd93) && (cnt_6 == 3'd6) )begin       
        wr_ram_en   <= 1'b0;
        wr_ram_addr <= 8'd0;
        wr_ram_data <= 8'b0;
    end 
    else if(cnt_read_data >= 7'd1 && cnt_read_data <= 7'd92)begin
        if(cnt_read_data[2:0] == 3'b011 && cnt_6 == 3'd5)begin
            wr_ram_en   <= 1'b1;
            wr_ram_addr <= wr_ram_addr + 8'd1;            
            wr_ram_data <= data_8bit;     
        end
        else if(cnt_read_data[2:0] == 3'b110 && cnt_6 == 3'd5)begin
            wr_ram_en   <= 1'b1;  
            wr_ram_addr <= wr_ram_addr + 8'd1;          
            wr_ram_data <= data_8bit;     
        end
        else if(cnt_read_data[2:0] == 3'b000 && cnt_6 == 3'd5)begin
            wr_ram_en   <= 1'b1; 
            wr_ram_addr <= wr_ram_addr + 8'd1;          
            wr_ram_data <= data_8bit;     
        end
        else if(cnt_6 == 3'd2)begin
            wr_ram_en   <= 1'b1;  
            wr_ram_addr <= wr_ram_addr + 8'd1;          
            wr_ram_data <= data_8bit;    
        end
        else begin
            wr_ram_en   <= 1'b0;
            wr_ram_addr <= wr_ram_addr;             
            wr_ram_data <= wr_ram_data;    
        end            
    end
    else if(cnt_read_data == 7'd93)begin
        if(cnt_6 == 3'd2)begin
            wr_ram_en   <= 1'b1;
            wr_ram_addr <= wr_ram_addr + 8'd1;            
            wr_ram_data <= data_8bit;    
        end
        else if(cnt_6 == 3'd5)begin
            wr_ram_en   <= 1'b1; 
            wr_ram_addr <= wr_ram_addr + 8'd1;           
            wr_ram_data <= data_8bit;    
        end
        else begin
            wr_ram_en   <= 1'b0;
            wr_ram_addr <= wr_ram_addr;            
            wr_ram_data <= wr_ram_data;    
        end                       
    end
    else begin
        wr_ram_en   <= 1'b0;
        wr_ram_addr <= wr_ram_addr;         
        wr_ram_data <= wr_ram_data;    
    end           
end//always

//------------------------------------CRC32校验结果生成-------------------------//

always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if (!i_rst_n)
        LFSR_CRC32_D8 <= 32'd0;
    else if(send_msg_ctrl || rd_ram_addr != 8'd0)
        LFSR_CRC32_D8 <= LFSR_CRC32_D8; 
    else if(data_start_p0 || new_msg_flag_p)//在新数据到来时、已经出现误码之后要对原计算的crc32结果清0。
        LFSR_CRC32_D8 <= 32'd0;
    else if(wr_ram_en) 
        LFSR_CRC32_D8 <= getCRC32_D8(wr_ram_data,LFSR_CRC32_D8);    
    else
        LFSR_CRC32_D8 <= LFSR_CRC32_D8;  
end//always

//CRC结果擦除校验
always @(negedge i_rst_n or posedge i_clk_81M) 
begin
    if (!i_rst_n)
        crc32_clr_ok <= 1'b0; 
    else if( data_start_p0 || transport_en_p )//在新数据到来和下层模块取数据后都要对该结果清0   
        crc32_clr_ok <= 1'b0;
    else if( data_start_p1 && (LFSR_CRC32_D8 == 32'd0) )
        crc32_clr_ok <= 1'b1;    
    else
        crc32_clr_ok <=crc32_clr_ok;  
end//always

//--------------------------------发出转换完成信号------------------------------//
//对于连续的相同报文只发一次
always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if (!i_rst_n)
        o_telegram_trans_ready <= 1'b0;
    else if(new_msg_flag_n || transport_en_p)//新数据到来和下层取数据后置为0
        o_telegram_trans_ready <= 1'b0;    
    else if(send_msg_ctrl && crc32_clr_ok)//同时在数据写完、上次的CRC32_D8结果清0正确、新报文标志拉高都满足时才转换完成
        o_telegram_trans_ready <= 1'b1;
    else
        o_telegram_trans_ready <= o_telegram_trans_ready;
end
//-----------------------------------------------------------------------------//

//跨时钟域处理
always @(negedge i_rst_n or posedge i_clk_81M) 
begin
    if (!i_rst_n) begin
        transport_en_r1 <= 1'b0;
        transport_en_r2 <= 1'b0;
    end
    else begin
        transport_en_r1 <= i_telegram_transport_en; 
        transport_en_r2 <= transport_en_r1;        
    end
end //always

//取沿
assign transport_en_p = transport_en_r1 && (~transport_en_r2);

//对组帧模块发来的sck时钟进行取沿 跨时钟域处理
always @(negedge i_rst_n or posedge i_clk_81M) 
begin
    if (!i_rst_n) begin
        telegram_data_sck_r1 <= 1'b0;
        telegram_data_sck_r2 <= 1'b0;
    end
    else begin
        telegram_data_sck_r1 <= i_telegram_data_sck;
        telegram_data_sck_r2 <= telegram_data_sck_r1;       
    end
end //always 

//取沿
assign telegram_data_sck_p = telegram_data_sck_r1 && (~telegram_data_sck_r2);

//读RAM操作
always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if(!i_rst_n)begin
        rd_ram_en   <= 1'd0;
        rd_ram_addr <= 8'd0;
    end 
    else if(transport_en_p)begin
        rd_ram_en   <= 1'd0;
        rd_ram_addr <= 8'd0;
    end 
    else if(rd_ram_addr == 8'd132)begin
        rd_ram_en   <= 1'd0;
        rd_ram_addr <= 8'd0;
    end 
    else if(telegram_data_sck_p && rd_ram_addr >= 8'd128)begin
        rd_ram_en   <= 1'd0;
        rd_ram_addr <= rd_ram_addr +8'd1;
    end    
    else if(telegram_data_sck_p)begin
        rd_ram_en   <= 1'd1;
        rd_ram_addr <= rd_ram_addr +8'd1;
    end        
    else begin 
        rd_ram_en <= 1'd0;
        rd_ram_addr <= rd_ram_addr;
    end
end //always

always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if(!i_rst_n)begin
        rd_ram_en_r1 <= 1'b0;
        rd_ram_en_r2 <= 1'b0;
    end
    else begin
        rd_ram_en_r1 <= rd_ram_en;
        rd_ram_en_r2 <= rd_ram_en_r1;
    end   
end //always

//----------------------------------发送数据-----------------------------------//

always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if(!i_rst_n)
        ov_telegram_data <= 8'd0;
    else if(transport_en_p)
        ov_telegram_data <= 8'd0;    
    else if(rd_ram_en_r2 && rd_ram_addr >= 8'd1 && rd_ram_addr <= 8'd128)//RAM地址只从1读到了128
        ov_telegram_data <= rd_ram_data;
    else if(rd_ram_addr == 8'd129)//取寄存器组中的CRC32结果
        ov_telegram_data <= LFSR_CRC32_D8[31:24];
    else if(rd_ram_addr == 8'd130)//取寄存器组中的CRC32结果
        ov_telegram_data <= LFSR_CRC32_D8[23:16];
    else if(rd_ram_addr == 8'd131)//取寄存器组中的CRC32结果
        ov_telegram_data <= LFSR_CRC32_D8[15:8];
    else if(rd_ram_addr == 8'd132)//取寄存器组中的CRC32结果
        ov_telegram_data <= LFSR_CRC32_D8[7:0];
    else
        ov_telegram_data <= ov_telegram_data;
end //always

/////////////////////////////////////////
//11bit转8bit的RAM 有效地址为1->128
/////////////////////////////////////////

ram_telegram ram_telegram_u(
	.data(wr_ram_data),
	.rdaddress(rd_ram_addr),
	.rdclock(i_clk_81M),
	.rden(rd_ram_en),
	.wraddress(wr_ram_addr),
	.wrclock(i_clk_81M),
	.wren(wr_ram_en),
	.q(rd_ram_data)
	);	
	
//------------------------------------------//  
 
endmodule
`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Module Name: top_tb
// Description: AXI主机仿真模块.
// Revision 0.01 - File Created
//////////////////////////////////////////////////////////////////////////////////


module top_tb();

	parameter USER_WR_DATA_WIDTH = 16;
	parameter USER_RD_DATA_WIDTH = 16;
	parameter AXI_DATA_WIDTH     = 128;
	parameter AXI_ADDR_WIDTH     = 32;
	parameter WR_BURST_LENGTH    = 1024;
	parameter RD_BURST_LENGTH    = 1024;

	reg									user_wr_clk;
	reg									user_rd_clk;
	reg									rst_n;
	reg									axi_clk;

	wire								ddr_init_done;

	wire	[USER_WR_DATA_WIDTH-1:0]	user_wr_data;
	wire								user_wr_en;
	wire	[AXI_ADDR_WIDTH-1:0]		user_wr_base_addr;
	wire	[AXI_ADDR_WIDTH-1:0]		user_wr_end_addr;

	wire								user_rd_req;
	wire	[AXI_ADDR_WIDTH-1:0]		user_rd_base_addr;
	wire	[AXI_ADDR_WIDTH-1:0]		user_rd_end_addr;
	wire								rd_req_busy;
	wire								user_rd_valid;
	wire	[USER_WR_DATA_WIDTH-1:0]	user_rd_data;
	wire								user_rd_last;

	wire								m_axi_awvalid;
	wire								m_axi_awready;
	wire	[AXI_DATA_WIDTH-1:0]		m_axi_awaddr;
	wire	[3:0]						m_axi_awid;
	wire	[7:0]						m_axi_awlen;
	wire	[1:0]						m_axi_awburst;
	wire	[2:0]						m_axi_awsize;
	wire	[2:0]						m_axi_awprot;
	wire	[3:0]						m_axi_awqos;
	wire								m_axi_awlock;
	wire	[3:0]						m_axi_awcache;

	wire								m_axi_wvalid;
	wire								m_axi_wready;
	wire	[AXI_DATA_WIDTH-1:0]		m_axi_wdata;
	wire	[AXI_DATA_WIDTH/8-1:0]		m_axi_wstrb;
	wire								m_axi_wlast;

	wire	[3:0]						m_axi_rid;
	wire	[AXI_DATA_WIDTH-1:0]		m_axi_rdata;
	wire	[1:0]						m_axi_rresp;
	wire								m_axi_rlast;
	wire								m_axi_rvalid;
	wire								m_axi_rready;
	
	wire	[3:0]						m_axi_arid;
	wire	[AXI_ADDR_WIDTH-1:0]		m_axi_araddr;
	wire	[7:0]						m_axi_arlen;
	wire	[2:0]						m_axi_arsize;
	wire	[1:0]						m_axi_arburst;
	wire								m_axi_arlock;
	wire	[3:0]						m_axi_arcache;
	wire	[2:0]						m_axi_arprot;
	wire	[3:0]						m_axi_arqos;
	wire								m_axi_arvalid;
	wire								m_axi_arready;

	wire	[3:0]						m_axi_bid; 
	wire	[1:0]						m_axi_bresp;
	wire								m_axi_bvalid;
	wire								m_axi_bready;

	// 忙信号
	wire	rsta_busy;
	wire	rstb_busy;

/*-------------------------------------------------*\
					时钟和复位
\*-------------------------------------------------*/

	// 读写时钟初始化
	initial begin
		user_wr_clk = 1'b0;
		user_rd_clk = 1'b0;
		axi_clk     = 1'b0;
	end

	always #(10)	user_wr_clk = ~user_wr_clk;	// 用户写时钟50M
	always #(5)		user_rd_clk = ~user_rd_clk;	// 用户读时钟100M

	// AXI时钟设置为200M
	always #(2.5)	axi_clk = ~axi_clk;

	// 复位
	initial begin
		rst_n <= 1'b0;
		#(1000)
		rst_n <= 1'b1;
	end

	// AXI复位信号(低有效)
	reg		s_aresetn_d0;
	reg		s_aresetn_d1;
	reg		s_aresetn;

	always @(posedge axi_clk) begin
		s_aresetn_d0 <= rst_n;
		s_aresetn_d1 <= s_aresetn_d0;
		s_aresetn    <= s_aresetn_d1;
	end

/*-------------------------------------------------*\
					assign
\*-------------------------------------------------*/	

	assign	ddr_init_done = 1'b1;		// DDR3的初始化信号设置为常高即可

	// 写操作基地址与结束地址
	assign 	user_wr_base_addr = 32'h0;
	assign	user_wr_end_addr  = 32'h00014000;

	// 读操作基地址与结束地址
	assign 	user_rd_base_addr = 32'h0;
	assign	user_rd_end_addr  = 32'h00014000;

/*-------------------------------------------------*\
					模块例化
\*-------------------------------------------------*/
	
	user_req_generate #(
			.USER_WR_DATA_WIDTH(USER_WR_DATA_WIDTH)
		) U_req_gen (
			.wr_clk       (user_wr_clk),
			.rd_clk       (user_rd_clk),
			.reset        (~rst_n),
			.axi_clk      (axi_clk),
			.m_axi_wlast  (m_axi_wlast),
			.user_wr_en   (user_wr_en),
			.user_wr_data (user_wr_data),
			.user_rd_req  (user_rd_req)
		);

	// 主机例化
	axi_adma_top #(
			.USER_WR_DATA_WIDTH (USER_WR_DATA_WIDTH),
			.USER_RD_DATA_WIDTH (USER_RD_DATA_WIDTH),
			.AXI_DATA_WIDTH     (AXI_DATA_WIDTH),
			.AXI_ADDR_WIDTH     (AXI_ADDR_WIDTH),
			.WR_BURST_LENGTH    (WR_BURST_LENGTH),
			.RD_BURST_LENGTH    (RD_BURST_LENGTH)
		) u_axi_adma_top (
			.i_user_wr_clk        (user_wr_clk),
			.i_user_rd_clk        (user_rd_clk),
			.i_rst_n              (rst_n),
			.i_axi_clk            (axi_clk),

			.i_ddr_init_done      (ddr_init_done),

			.iv_user_wr_data      (user_wr_data),
			.i_user_wr_en         (user_wr_en),
			.iv_user_wr_base_addr (user_wr_base_addr),
			.iv_user_wr_end_addr  (user_wr_end_addr),

			.i_user_rd_req        (user_rd_req),
			.iv_user_rd_base_addr (user_rd_base_addr),
			.iv_user_rd_end_addr  (user_rd_end_addr),
			.o_rd_req_busy        (rd_req_busy),
			.o_user_rd_valid      (user_rd_valid),
			.ov_user_rd_data      (user_rd_data),
			.o_user_rd_last       (user_rd_last),

			.o_m_axi_awvalid      (m_axi_awvalid),
			.i_m_axi_awready      (m_axi_awready),
			.ov_m_axi_awaddr      (m_axi_awaddr),
			.ov_m_axi_awid        (m_axi_awid),
			.ov_m_axi_awlen       (m_axi_awlen),
			.ov_m_axi_awburst     (m_axi_awburst),
			.ov_m_axi_awsize      (m_axi_awsize),
			.ov_m_axi_awprot      (m_axi_awprot),
			.ov_m_axi_awqos       (m_axi_awqos),
			.o_m_axi_awlock       (m_axi_awlock),
			.ov_m_axi_awcache     (m_axi_awcache),

			.o_m_axi_wvalid       (m_axi_wvalid),
			.i_m_axi_wready       (m_axi_wready),
			.ov_m_axi_wdata       (m_axi_wdata),
			.ov_m_axi_wstrb       (m_axi_wstrb),
			.o_m_axi_wlast        (m_axi_wlast),

			.iv_m_axi_bid         (m_axi_bid),
			.iv_m_axi_bresp       (m_axi_bresp),
			.i_m_axi_bvalid       (m_axi_bvalid),
			.o_m_axi_bready       (m_axi_bready),

			.iv_m_axi_rid         (m_axi_rid),
			.iv_m_axi_rdata       (m_axi_rdata),
			.iv_m_axi_rresp       (m_axi_rresp),
			.i_m_axi_rlast        (m_axi_rlast),
			.i_m_axi_rvalid       (m_axi_rvalid),
			.o_m_axi_rready       (m_axi_rready),

			.ov_m_axi_arid        (m_axi_arid),
			.ov_m_axi_araddr      (m_axi_araddr),
			.ov_m_axi_arlen       (m_axi_arlen),
			.ov_m_axi_arsize      (m_axi_arsize),
			.ov_m_axi_arburst     (m_axi_arburst),
			.o_m_axi_arlock       (m_axi_arlock),
			.ov_m_axi_arcache     (m_axi_arcache),
			.ov_m_axi_arprot      (m_axi_arprot),
			.ov_m_axi_arqos       (m_axi_arqos),
			.o_m_axi_arvalid      (m_axi_arvalid),
			.i_m_axi_arready      (m_axi_arready)
	);

	// 从机使用AXI4接口的BRAM实现
	blk_mem_gen_0 axi4_alave (
  		.rsta_busy     (rsta_busy),         // output wire rsta_busy
  		.rstb_busy     (rstb_busy),         // output wire rstb_busy

  		.s_aclk        (axi_clk),           // input wire s_aclk
  		.s_aresetn     (s_aresetn),         // input wire s_aresetn

  		.s_axi_awid    (m_axi_awid),        // input wire [3 : 0] s_axi_awid
  		.s_axi_awaddr  (m_axi_awaddr),    	// input wire [31 : 0] s_axi_awaddr
  		.s_axi_awlen   (m_axi_awlen),      	// input wire [7 : 0] s_axi_awlen
  		.s_axi_awsize  (m_axi_awsize),   	// input wire [2 : 0] s_axi_awsize
  		.s_axi_awburst (m_axi_awburst),  	// input wire [1 : 0] s_axi_awburst
  		.s_axi_awvalid (m_axi_awvalid),  	// input wire s_axi_awvalid
  		.s_axi_awready (m_axi_awready),  	// output wire s_axi_awready

  		.s_axi_wdata   (m_axi_wdata),      	// input wire [127 : 0] s_axi_wdata
  		.s_axi_wstrb   (m_axi_wstrb),      	// input wire [15 : 0] s_axi_wstrb
  		.s_axi_wlast   (m_axi_wlast),      	// input wire s_axi_wlast
  		.s_axi_wvalid  (m_axi_wvalid),    	// input wire s_axi_wvalid
  		.s_axi_wready  (m_axi_wready),    	// output wire s_axi_wready

  		.s_axi_bid     (m_axi_bid),         // output wire [3 : 0] s_axi_bid
  		.s_axi_bresp   (m_axi_bresp),      	// output wire [1 : 0] s_axi_bresp
  		.s_axi_bvalid  (m_axi_bvalid),    	// output wire s_axi_bvalid
  		.s_axi_bready  (m_axi_bready),    	// input wire s_axi_bready

  		.s_axi_arid    (m_axi_arid),        // input wire [3 : 0] s_axi_arid
  		.s_axi_araddr  (m_axi_araddr),    	// input wire [31 : 0] s_axi_araddr
  		.s_axi_arlen   (m_axi_arlen),      	// input wire [7 : 0] s_axi_arlen
  		.s_axi_arsize  (m_axi_arsize),    	// input wire [2 : 0] s_axi_arsize
  		.s_axi_arburst (m_axi_arburst),  	// input wire [1 : 0] s_axi_arburst
  		.s_axi_arvalid (m_axi_arvalid),  	// input wire s_axi_arvalid
  		.s_axi_arready (m_axi_arready),  	// output wire s_axi_arready

  		.s_axi_rid     (m_axi_rid),         // output wire [3 : 0] s_axi_rid
  		.s_axi_rdata   (m_axi_rdata),      	// output wire [127 : 0] s_axi_rdata
  		.s_axi_rresp   (m_axi_rresp),      	// output wire [1 : 0] s_axi_rresp
  		.s_axi_rlast   (m_axi_rlast),      	// output wire s_axi_rlast
  		.s_axi_rvalid  (m_axi_rvalid),    	// output wire s_axi_rvalid
  		.s_axi_rready  (m_axi_rready)    	// input wire s_axi_rready
	);

endmodule

% close all;
% clear;
% clc;
% % --------------------------------------------------------
% %将ZCP_detection_for_syncpoint.m转变为verilog相似的语句
% %对相位折线进行29点最小二乘法，求斜率曲线，根据其过零点修正中心频率，进而求同步点
% %% --------------------------------------------------------
% %%%%%%%产生相位连续的FSK调制信号进行ADC采样后作为输入%%%%%%%%%%%%%
% sample_rate = 27.095;%采样速率，单位MHz
% data_bit = 100;
% src_data([1:2:data_bit-1])=1;
% src_data([2:2:data_bit])=0;
% src_data = [ 1 1 1 1 1 1 1 1 1 1 0 0 1 1 1 1 1 1 1 1 1 1 0 0 1 1 1 1 1 1 1 1 1 1 0 0 1 1 1 1 1 1 1 1 1 1 0 0 1 1 1 1 1 1 1 1 1 1 0 0 1 1 1 1 1 1 1 1 1 1 0 0 1 1 1 1 1 1 1 1 1 1 0 0   ];
% BPS_data = 0.56448 ;  %数据传输速率，单位MHz
% freq_0 = 4.234 - 0.282 + 0.2  ;
% freq_1 = 4.234 + 0.282 + 0.1  ; %比特0和比特1的频点，单位MHz
% i_data_ADC_0  = FSK_mod(src_data,freq_0,freq_1,sample_rate,BPS_data)-2048;
% %频偏变化
% freq_0 = 4.234 - 0.282 + 0.1  ;
% freq_1 = 4.234 + 0.282 + 0.15  ; %比特0和比特1的频点，单位MHz
% i_data_ADC_1  = FSK_mod(src_data,freq_0,freq_1,sample_rate,BPS_data)-2048;
% %频偏变化
% freq_0 = 4.234 - 0.282 - 0.05  ;
% freq_1 = 4.234 + 0.282 - 0.05  ; %比特0和比特1的频点，单位MHz
% i_data_ADC_2  = FSK_mod(src_data,freq_0,freq_1,sample_rate,BPS_data)-2048;
% 
% i_data_ADC = [i_data_ADC_0,i_data_ADC_1,i_data_ADC_2];
% %i_data_ADC = i_data_ADC_0;
% % %% %%%%%%%%%%%%%%%%%%%%带内干扰%%%%%%%%%%%%%%%%%%%%%
% % src_data_noise = [1,1,1,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
% % f1_noise = 4.234 - 0.22 ;
% % f0_noise = 4.234 + 0.22 ;
% % FSK_data_noise  = FSK_mod(src_data_noise,f0_noise,f1_noise,sample_rate,BPS_data)./7;
% % FSK_data_noise = FSK_data_noise(1:500);
% % i_data_ADC (475:475+length(FSK_data_noise)-1) = i_data_ADC (475:475+length(FSK_data_noise)-1)+ FSK_data_noise;
% % i_data_ADC (2000:2000+length(FSK_data_noise)-1) = i_data_ADC (2000:2000+length(FSK_data_noise)-1)+ FSK_data_noise;
% % 
% % %% %%%%%%%%%%%%%%%%%%%%加入数字滤波器产生相移%%%%%%%%%%%%%%%%%%%%%
% % Hd = LP_8M_phase10degree;
% % i_data_ADC = filter(Hd,i_data_ADC);
% 
% % %%%%%%%%%%%%%%%%%%%%%%读取ADC采样报文作为输入%%%%%%%%%%%%%%%%%%%%%
% % ADC_data_hex = textread('D:\lab_project\BTM_RCV_3C25\leexiao\24点求和抵消算法求同步点matlab仿真\思诺应答器丢数据ADC采样数据.txt','%s');
% % i_data_ADC   = hex2dec(ADC_data_hex)' -2048;
% 
% %% 转换为对应于verilog的算法
% i_data_ADC = FixPointNum(i_data_ADC, 12, 0);%字长12bit，精度0位小数  (13000:16300)
% % Hilbert变换 并将数据对齐
% im_datain = hilbert_trans(i_data_ADC);
% Length_data = length(im_datain);
% im_datain = im_datain(11:Length_data-1);
% i_data_ADC = i_data_ADC(6:Length_data-6);
% 
% figure(1),
% subplot(2,1,1);
% plot(i_data_ADC);
% hold on,
% plot(im_datain,'r');
% legend('实部','虚部');
% 
% % 相位归一化后求arctan值
% angle_tan = Phase_normalization_to_Calculate_phase( i_data_ADC , im_datain );
% 
% figure(1),
% subplot(2,1,2);
% plot(angle_tan);
% legend('待修正的相位折线');
% 
% convergence_factor = 0.5;%收敛因子
% phase_sum = 0; %相位累计修正量
% correction = 0;              %修正量的初始值
% angle_fit_reg = zeros(1,29); %最小二乘法移位寄存器 
% slope_reg = zeros(1,57);     %斜率移位寄存器 
% 
% for ii = 2:length(angle_tan)
%    %% 输入数据
%     angle_tan_temp = angle_tan(ii);
%     angle_tan_delay = angle_tan(ii-1);
%    %% 对2pi，中心频率进行修正,对频偏进行修正 并对连续的1或0的相位累积值进行上溢处理
%     % 从第300个点开始做修正，保证开始修正时，斜率曲线是平稳的
%     if(phase_sum<8*8192) % 相位累积没有超过8*2pi 
%         if( angle_tan_temp+4096 <= angle_tan_delay ) % 过2pi时做2*pi模糊度处理 以及中心频率的修正
%             phase_sum = phase_sum + 8192 -( 1280 );% +correction
%         else
%             phase_sum = phase_sum - ( 1280 );% +correction
%         end
%     else% 相位累积超过8*2pi 上溢处理(phase_sum>=8*8192)
%         angle_fit_reg = angle_fit_reg - phase_sum ;% 将移位寄存器的相位值归为没有叠加任何值的原始相位
%         if( angle_tan_temp+4096<=angle_tan_delay ) 
%             phase_sum = 8192 -( 1280 ); % 过2*pi处，相位直接下拉到2*pi-中心频率 +correction
%         else
%             phase_sum =  0 - ( 1280 );  % 非2*pi处，相位直接下拉到0-中心频率 +correction
%         end
%     end
%     angle_fit = angle_tan_temp + phase_sum ;%将累积量叠加到寄存器中的原始相位上
%     angle_fit_plot(ii) = angle_fit;
%     
%     %% 将修改相位后的数存入移位寄存器 29位移位寄存器，用于最小二乘法计算
%     angle_fit_reg = [angle_fit_reg(2:end),angle_fit];
%     
%     %% -----------------最小二乘法---------------------------------------
%     %取N=14个点用于之后进行29点最小二乘法
%     %按照最小二乘法公式先进行对应位相乘
%     slope_coefficient = [-14:1:14];
%     slope = 1/2030* angle_fit_reg*slope_coefficient';
%     slope_plot(ii) = slope;
%     slope_reg = [slope_reg(2:end),slope];%斜率移位寄存器，寄存57个点的斜率值 。用于中心频率修正，同步点计算  
%     
%     %% ---------------------中心频率修正--------------------------------------
%     %取平均,求w0，w1
%     % 寄存的57个点的斜率值中，第29点用来判断是否过0。第29点过0时，斜率寄存器第15到43这几个点是01切换过渡段；第1到8,50到57点是斜率曲线的平坦部分，可以比较准确地估计w0，w1的值
%     slope_average_correction_forward = mean(slope_reg(50:57));
%     slope_average_correction_after = mean(slope_reg(1:8));
%     % 根据估算的w0和w1的值求出斜率修正量
%     if(ii>300)
%         slope_correction = convergence_factor*( slope_average_correction_after + slope_average_correction_forward )/2;
%     else
%         slope_correction = 0;
%     end
%     % 斜率修正量累积值
%     if( (slope_reg(29)<=0) && (slope_reg(30)>=0)  )  % + -  过零判决
%         correction = correction + slope_correction;
%         sync_point(ii) = 1;%判决的拐点
%     elseif( slope_reg(29)>=0) && (slope_reg(30)<=0 ) % - +  过零判决
%         correction =correction + slope_correction;
%         sync_point(ii) = 1;%判决的拐点
%     else
%         correction = correction;
%         sync_point(ii) = 0;
%     end %if
%     
% end %for ii
% 
% figure(2),
% plot(angle_fit_plot);
% legend('相位折线');
% 
% figure(3),
% hold on;
% plot(slope_plot,'r');
% plot(sync_point(25:end)*100,'g');
% legend('斜率曲线','同步点');
% grid on;










close all;
clear;
clc;
% --------------------------------------------------------
%将ZCP_detection_for_syncpoint.m转变为verilog相似的语句
%对相位折线进行29点最小二乘法，求斜率曲线，根据其过零点修正中心频率，进而求同步点
%% --------------------------------------------------------
%%%%%%%产生相位连续的FSK调制信号进行ADC采样后作为输入%%%%%%%%%%%%%
sample_rate = 27.095;%采样速率，单位MHz
data_bit = 100;
src_data([1:2:data_bit-1])=1;
src_data([2:2:data_bit])=0;
src_data = [ 1 1 1 1 1 1 1 1 1 1 0 0 1 1 1 1 1 1 1 1 1 1 0 0 1 1 1 1 1 1 1 1 1 1 0 0 1 1 1 1 1 1 1 1 1 1 0 0 1 1 1 1 1 1 1 1 1 1 0 0 1 1 1 1 1 1 1 1 1 1 0 0 1 1 1 1 1 1 1 1 1 1 0 0   ];
BPS_data = 0.56448 ;  %数据传输速率，单位MHz
freq_0 = 4.234 - 0.282 + 0.2  ;
freq_1 = 4.234 + 0.282 + 0.1  ; %比特0和比特1的频点，单位MHz
i_data_ADC_0  = FSK_mod(src_data,freq_0,freq_1,sample_rate,BPS_data)-2048;
%频偏变化
freq_0 = 4.234 - 0.282 + 0.1  ;
freq_1 = 4.234 + 0.282 + 0.15  ; %比特0和比特1的频点，单位MHz
i_data_ADC_1  = FSK_mod(src_data,freq_0,freq_1,sample_rate,BPS_data)-2048;
%频偏变化
freq_0 = 4.234 - 0.282 - 0.05  ;
freq_1 = 4.234 + 0.282 - 0.05  ; %比特0和比特1的频点，单位MHz
i_data_ADC_2  = FSK_mod(src_data,freq_0,freq_1,sample_rate,BPS_data)-2048;

i_data_ADC = [i_data_ADC_0,i_data_ADC_1,i_data_ADC_2];

%% %%%%%%%%%%%%%%%%%%%%带内干扰%%%%%%%%%%%%%%%%%%%%%
src_data_noise = [1,1,1,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
f1_noise = 4.234 - 0.22 ;
f0_noise = 4.234 + 0.22 ;
FSK_data_noise  = FSK_mod(src_data_noise,f0_noise,f1_noise,sample_rate,BPS_data)./7;
FSK_data_noise = FSK_data_noise(1:500);
i_data_ADC (475:475+length(FSK_data_noise)-1) = i_data_ADC (475:475+length(FSK_data_noise)-1)+ FSK_data_noise;
i_data_ADC (2000:2000+length(FSK_data_noise)-1) = i_data_ADC (2000:2000+length(FSK_data_noise)-1)+ FSK_data_noise;

%% %%%%%%%%%%%%%%%%%%%%加入数字滤波器产生相移%%%%%%%%%%%%%%%%%%%%%
Hd = LP_8M_phase10degree;
i_data_ADC = filter(Hd,i_data_ADC);

% %%%%%%%%%%%%%%%%%%%%%%读取ADC采样报文作为输入%%%%%%%%%%%%%%%%%%%%%
% ADC_data_hex = textread('D:\lab_project\BTM_RCV_3C25\leexiao\24点求和抵消算法求同步点matlab仿真\思诺应答器丢数据ADC采样数据.txt','%s');
% i_data_ADC   = hex2dec(ADC_data_hex)' -2048;

%% 转换为对应于verilog的算法
i_data_ADC = FixPointNum(i_data_ADC, 12, 0);%字长12bit，精度0位小数  (13000:16300)
% Hilbert变换 并将数据对齐
im_datain = hilbert_trans(i_data_ADC);
Length_data = length(im_datain);
im_datain = im_datain(11:Length_data-1);
i_data_ADC = i_data_ADC(6:Length_data-6);

figure(1),
subplot(2,1,1);
plot(i_data_ADC);
hold on,
plot(im_datain,'r');
legend('实部','虚部');

% 相位归一化后求arctan值
angle_tan = Phase_normalization_to_Calculate_phase( i_data_ADC , im_datain );

figure(1),
subplot(2,1,2);
plot(angle_tan);
legend('待修正的相位折线');

convergence_factor = 0.5;%收敛因子
phase_sum = 0; %相位累计修正量
correction = 0;              %修正量的初始值
angle_fit_reg = zeros(1,29); %最小二乘法移位寄存器 
slope_reg = zeros(1,51);     %斜率移位寄存器 
slope_for_sync = zeros(1,2);%斜率和中心频率偏移处理后寄存，用于判断同步点

for ii = 2:length(angle_tan)
   %% 输入数据
    angle_tan_temp = angle_tan(ii);
    angle_tan_delay = angle_tan(ii-1);
   %% 对2pi，中心频率进行修正,对频偏进行修正 并对连续的1或0的相位累积值进行上溢处理
    % 从第300个点开始做修正，保证开始修正时，斜率曲线是平稳的
    if(phase_sum<8*8192) % 相位累积没有超过8*2pi 
        if( angle_tan_temp+4096 <= angle_tan_delay ) % 过2pi时做2*pi模糊度处理 以及中心频率的修正
            phase_sum = phase_sum + 8192 -( 1280 );% +correction
        else
            phase_sum = phase_sum - ( 1280 );% +correction
        end
    else% 相位累积超过8*2pi 上溢处理(phase_sum>=8*8192)
        angle_fit_reg = angle_fit_reg - phase_sum ;% 将移位寄存器的相位值归为没有叠加任何值的原始相位
        if( angle_tan_temp+4096<=angle_tan_delay ) 
            phase_sum = 8192 -( 1280 ); % 过2*pi处，相位直接下拉到2*pi-中心频率 +correction
        else
            phase_sum =  0 - ( 1280 );  % 非2*pi处，相位直接下拉到0-中心频率 +correction
        end
    end
    angle_fit = angle_tan_temp + phase_sum ;%将累积量叠加到寄存器中的原始相位上
    angle_fit_plot(ii) = angle_fit;
    
    %% 将修改相位后的数存入移位寄存器 29位移位寄存器，用于最小二乘法计算
    angle_fit_reg = [angle_fit_reg(2:end),angle_fit];
    
    %% -----------------最小二乘法---------------------------------------
    %取N=14个点用于之后进行29点最小二乘法
    %按照最小二乘法公式先进行对应位相乘
    slope_coefficient = [-14:1:14];
    slope = 1/2030* angle_fit_reg*slope_coefficient';
    slope_plot(ii) = slope;
    slope_reg = [slope_reg(2:end),slope];%斜率移位寄存器，寄存57个点的斜率值 。用于中心频率修正，同步点计算  
    
    %% ---------------------中心频率修正--------------------------------------
    % 取平均,求w0，w1
    % 寄存的51个点的斜率值中，第26点用来判断是否过0。第26点过0时，根据斜率寄存器第1到3，第49到51点可以比较准确地估计w0，w1的值
    slope_average_correction_forward = mean(slope_reg(49:51));
    slope_average_correction_after = mean(slope_reg(1:3));
    
    % 根据估算的w0和w1的值求中心频率偏移量，用来后续同步点的判决
    if( (slope_reg(26)<=0) && (slope_reg(27)>=0)  )  % + -  过零判决
        slope_correction = ( slope_average_correction_after + slope_average_correction_forward )/2;
    elseif( slope_reg(26)>=0) && (slope_reg(27)<=0 ) % - +  过零判决
        slope_correction = ( slope_average_correction_after + slope_average_correction_forward )/2;
    else
        slope_correction = slope_correction;
    end %if
    slope_for_sync = [slope_for_sync(2),slope_reg(26)-slope_correction];
    slope_for_sync_plot(ii) = slope_reg(26)-slope_correction;
    % 判决是否是拐点（不同频偏时过零点处相邻两个点之间的斜率差不同。因此老师说的设定一个区间来判断拐点不可行）
    if( (slope_for_sync(1)<=0) && (slope_for_sync(2)>=0)  )  % + -  过零判决
        sync_point(ii) = 1;%判决的拐点
    elseif( (slope_for_sync(1)>=0) && (slope_for_sync(2)<=0)  ) % - +  过零判决
        sync_point(ii) = 1;%判决的拐点
    else
        sync_point(ii) = 0;
    end %if
    
end %for ii

figure(2),
plot(angle_fit_plot);
legend('相位折线');

figure(3),
hold on;
plot(sync_point*100,'g');
plot(slope_for_sync_plot,'b');

legend('斜率曲线','同步点');
grid on;


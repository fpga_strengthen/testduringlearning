/*=======================================================================================*\
  Filename    : demod_top.v
  Author      : 陈献彬
  time        ：2017.05.22
  Description :
               本模块主要完成对ADC采样的12bit位长的fsk数据进行解调 ，将解调结果11bit，8bit，1bit一组分别打包
          发送给译码，fsk上传，自检模块
  Called by   : 
  Revision History :
  Author              Date              Version 
  
  Email       : 
  Company     :
\*=========================================================================================*/
module demod_top
  (
  //----------------------------------------------
  //时钟和复位
  input         i_clk_81M,
  input         i_clk_27M_en,
  input         i_rst_n,
  
  input  [11:0] i_ADC_data,
  
  output [10:0] ov_data_11bit,   //解调结果11bit一组打包，给译码模块
  output        o_data_11bit_sck,
  
  output [7:0]  ov_data_8bit,    //解调结果8bit一组打包，给fsk发送模块
  output        o_data_8bit_sck,
  
  output        o_data_1bit,     //解调结果1bit一组打包，给自检模块
  output        o_data_1bit_sck,
  
  //output        o_ADC_noise_flag,//ADC数据为噪声信号指示，用于控制点亮A排灯的条件之一，高电平时为有噪声
  output        o_framing_start
  
  );

  
  wire demod_result;
  wire demod_result_sck;
  
  wire sync_point;
  wire demod_en;
  
  //--------------------------------------------------------------------------------------------------------
  //相干解调
  //该模块用的相干解调算法需要48个采样点与载波相乘再相加，也就是每次来一个12bit的ADC采样数据都需要和载波相乘
  //再和前47个采样点的乘积结果求和，将求和的结果再平方。ADC采样数据是27M速率，如此整个计算流程要想在27M内完成
  //从而不影响下一个ADC采样数据的解调过程。很显然这需要把主时钟提高到27xN。也就是在整个解调流程需要多少个时钟，
  //我们就需要在一个27M时钟间隔内通过倍频提供所有的时钟数。
  //很显然这是困难的，因为解调流程需要数十个时钟，将27M进行几十倍的倍频不太合理。因此我们选择了一种其他的方式
  //只在判决的时刻进行解调，也就是判决时刻对应的48个点进行相干解调。这样一个bit只需要解调一次，理论上解调一次的
  //时间大概为48个27M时钟。判决时刻由同步跟踪模块给出
  Demodulation u_Demodulation
    (
    //时钟和复位
    .i_clk_81M(i_clk_81M),
    .i_clk_27M_en(i_clk_27M_en),
    .i_rst_n(i_rst_n),
    
    .i_ADC_data(i_ADC_data),
    .i_demod_en(demod_en),        //开始解调的时刻，一个脉冲
    
    .o_demod_result(demod_result),//解调结果
    .o_demod_result_sck(demod_result_sck)//解调结果的同步时钟
    );
  
  //--------------------------------------------------------------------------------------------------------
  //同步跟踪模块
  //
  Sync_point_top u_Sync_point_top
    (
    .i_rst_n(i_rst_n),                  //输入复位信号，低电平有效
    .i_clk_81M(i_clk_81M),              //输入时钟信号，81M
    .i_clk_27M_en(i_clk_27M_en),        //输入时钟使能，27M
    .i_data(i_ADC_data),                //输入数据，用来进行希尔伯特滤波

    //.o_ADC_noise_flag(o_ADC_noise_flag),//ADC数据为噪声信号指示，用于控制点亮A排灯的条件之一，高电平时为有噪声
    .o_sync_point(sync_point)           //拐点的跳变点
    );

  Sync_Fsk u_Sync_Fsk
    (
    //----------------------------------------------
    //时钟和复位
    .i_clk_81M(i_clk_81M),
    .i_clk_27M_en(i_clk_27M_en),
    .i_rst_n(i_rst_n),
    
    //----------------------------------------------
    //同步点
    .i_sync_point(sync_point),      
    
    //----------------------------------------------
    //解调结果。用来判断两个相邻同步点间相同的bit数
    .i_demod_result(demod_result),    
    .i_demod_result_sck(demod_result_sck),
    
    //解调模块以该脉冲信号为标志开始解调。
    .o_demod_en(demod_en),
    .o_framing_start(o_framing_start)
    );
  
  framing u_framing
    (
    //----------------------------------------------
    //时钟和复位
    .i_clk_81M(i_clk_81M),
    .i_rst_n(i_rst_n),
    .i_framing_start(o_framing_start), 
   
    .i_demod_result(demod_result),
    .i_demod_result_sck(demod_result_sck),
  
    .ov_data_11bit(ov_data_11bit),
    .o_data_11bit_sck(o_data_11bit_sck),
  
    .ov_data_8bit(ov_data_8bit),
    .o_data_8bit_sck(o_data_8bit_sck),
  
    .o_data_1bit(o_data_1bit),
    .o_data_1bit_sck(o_data_1bit_sck)
    );
    
endmodule
clear
%% 本程序将角度值映射为弧度制并进行14位的量化(方案暂时舍弃不用了)
% 角度值范围0~360°
angle = 0:360;
% 将角度值按照2pi进行归一化
angle_norm = angle / 360;
% 映射到0~8192
angle_quant = angle_norm * 2^13;
% 数组长度
L= length(angle_quant);
% 保存四舍五入的结果
angle_fix = zeros(1,L);
% 对结果进行四舍五入
for i = 1:L
    % 取整数部分
    angle_tmp = fix(angle_quant(i));
    % 对整数部分加0.5和原数比较,用于四舍五入的判断依据
    if(angle_quant(i) - 0.5 >= angle_tmp)
        angle_fix(i) = fix(angle_quant(i) + 0.5);
    else
        angle_fix(i) = angle_tmp;
    end
end

% 画图查看结果
% plot(angle_quant);
% hold on
% plot(angle_fix);
% hold off
% legend('angle_quant','angle_fix');
angle_diff = angle_quant - angle_fix;

%% 数据写入mif文件
% 文件写入
fid = fopen('angle2rad.mif','w+');
fprintf(fid,'%s\n','WIDTH=14;');
fprintf(fid,'%s\n','DEPTH=512;');

fprintf(fid,'\n%s\n','ADDRESS_RADIX=UNS;');
fprintf(fid,'\n%s\n','DATA_RADIX=UNS;');
fprintf(fid,'\n');
fprintf(fid,'%s\n','CONTENT BEGIN');
for i=1:L
    fprintf(fid,'\t\t\t %d%s%d %s \n',angle(i),':',angle_fix(i),';');
end
% 对于剩余的地址,用0填补空白
for i = L+1 : 2^9-1
    fprintf(fid,'\t\t\t %d%s%d %s \n',i,':',0,';');
end
fprintf(fid,'%s','END;');
fclose(fid);







# 一、概述

## 1. STA概述

静态时序分析（STA，Static Timing Analysis）主要关心的两个指标：

- `Setup time` ：数据在时钟上升沿到来之前保持稳定的时间长度
- `Hold time`：数据在时钟上升沿之后保持稳定的时间长度

静态时序分析工具：`PT/Tempus`，语言`TCL`，如下是STA Flow.

<img src="pics/How_STA_work.png" style="zoom:67%;" />



## 2. 相关概念补充

- **STA的文件**

  **SDC**是STA分析时序的约束文件，网表**netlist**保存了设计信息；

  **SPEF**存储了设计中net中delay信息，**db**存储了标准单元的时序信息；

  scan def存储了DT串链信息，与静态时序分析无关。

- **线间串扰的修复**

  - **增加线间距**，可以减小耦合电容；
  - **增加via孔**，可以减小电阻，从而减少RC延迟；
  - **添加屏蔽线**：屏蔽线的电压通常为常量，电压变化极小，所以串扰极小；
  - **在net中插入buffer**：插入buffer可以增强受害网络的电流强度，将et打断，减小了net的偶尔电容。

  **cross talk**是指两条相近的net存在耦合电容，当一条net信号翻转时对另一条net上的信号产生的影响，可能使net_delay减小也可能增大，从而使setup变差或使hold变差，这取决于侵害网络与受害网络的电压变化方向。

  **crosstalk noise**和**crosstalk delay**区别在于两者产生的条件不一样，crosstalk noise是一条net上的信号翻转，在另一条信号不翻转的net上引起了电平的变化；而crosstalk delay是两个net上的信号的翻转时间上有重叠，侵害网络导致受害网络的翻转速度发生了变化。

- 时序单元约束信息

  建立时间、保持时间、恢复时间、移除时间以及最小脉冲宽度都是时序单元正常工作的前提；

  路径最大延时与路径最小延时可能是设计延时要求，但并不是时序单元正常工作的条件。这两个是无法约束的。

- 时钟频率越高越难收敛，频率高，周期小，setup就很难满足。

- 如果同一条path既有setup也有hold违例：

  - 尽量使用当前项目允许使用的阈值最低的cell减小违例.

    阈值越低的cell在不同的Corner下的delay差异越小，当setup与hold互卡时，应该尽量使用不同Corner下delay差异更小的cell,

  - 如果setup和hold只能修复一个，应该优先修复hold.

    setup与hold违例都可能导致电路发生错误翻转，但是setup的满足条件与时钟周期相关，但是hold满足条件只与data path delay与时钟delay 相关，因此当前频率下只能满足一个，优先解hold；

    setup还可通过降频来解决，只是影响电路性能。但如果优先解setup，把hold问题将无法解决，最终会导致电路发生错误，而不能正确使用。

    

# 二. TCL语言

## 1.变量列表

### 1.1 置换

TCL解释器运用规则把命令分成一个个独立的单词，同时进行必要的置换。

TCL置换分为以下三类：变量置换`$`    命令置换`[]`      反斜杠置换`\`

- TCL解释器会认为`$`后面为变量名，将变量名置换为它的值

  ```tcl
  set a "snow"
  >> snow
  puts $a
  >>snow
  puts a
  a
  ```

- 用[]表示命令置换，[]内是一个独立的TCL语句

  ```tcl
  set a [expr 3+4]
  >> 7
  puts $a
  >> 7
  ```

- 反斜杠置换：换行符、空格、[、$等被TCL解释器当作特殊符号对待的字符，加上反斜杠后变成普通字符。

  ```tcl
  puts "[expr $X + $Y]"
  >> 2.5
  puts "\[expr $X + $Y\]"
  >> [expr 1.2 + 1.3]
  puts "\[expr \$X + \$Y\]"
  >> [expr $X + $Y]
  ```

- 其他符号

  用`\t`表示TAB，用`\n`表示换行符；`#`表示注释；

  TCL对双引号中`$`和`[]`符号会进行变量置换和命令置换。

  在花括号中，所有特殊字符都将成为普通字符，TCL解释器不会对其作特殊处理；

### 1.2 变量数组列表

- 变量：变量名称在程序运行期间保持不变，但是其值通常会不断改变。

  定义： `set` `变量名` `变量值`

  取值：`$变量名`

  ```tcl
  set cell "buffx2"
  puts $cell
  >> bufx2
  ```

  ```tcl
  set a 2
  puts ${a}_1
  >> 2_1  		# 实现a可变，但是可以给后面加后缀
  ```

- 数组：TCL数组可以存储很多值，通过**元素名**来进行检索。

  ``` tcl
  # 定义方法：  set 数组名 (元素名) 值
  set cell_1(ref_name) "bufx2"
  >> bufx2
  set cell_1(full_name) "top/cell_1"
  >>top/cell_1
  set cell_1(pins) "A B C"
  >> A B C
  
  # 取值： $数组名 (元素名)
  puts $cell_1(ref_name)
  >> bufx2
  
  # 使用array指令获取数组信息
  array size cell_1		#使用size指令获取数组大小
  >> 3
  array names cell_1		#用names指令获取数组元素名称
  >> ref_name pins full_name
  ```

- 列表：是标量的有序集合。

  ```tcl
  # 定义 set 列表名 {元素1 元素2 元素3 ......}
  set ivt_list {ivtx1 ivtx2 ivtx3}
  # 取值 $列表名
  puts $ivt_list
  >> ivtx1 ivtx2 ivtx3
  ```

  TCL中有一系列便捷的列表操作命令

  |  命令   |         功能         |
  | :-----: | :------------------: |
  | contat  |     合并两个列表     |
  | lindex  | 选取列表中的某个元素 |
  | llength |       列表长度       |
  | lappend |  在列表末端追加元素  |
  |  lsort  |       列表排序       |

  ```tcl
  # concat指令
  set list1 {bufx1 bufx2 bufx3}
  set list2 {ivtx1 ivtx2 ivtx3}
  concat $list1 $list2				# 注意：1.可以跟不止两个列表  2.列表名前要有$符号
  >> bufx1 bufx2 bufx3 ivtx1 ivtx2 ivtx3
  ```

  ```tcl
  # llength指令
  set list1 {bufx1 bufx2 bufx3}    	# 定义数组
  llength $list1
  >> 3
  set list1 {bufx1 bufx2 bufx4}
  llength [concat $list1 $list1]		# 无条件拼接，重复不影响
  >> 6
  ```

  ```tcl
  # lindex指令：返回列表中第n个元素(从0开始计数)
  set list1 {bufx1 bufx2 bufx4}
  lindex $list1 1
  >> bufx2
  # 例:取出list1 = {a b c d e f}中的最后一个元素
  set list1 {a b c d e f}						# 定义数组
  llength $list1								# 获取长度(注意计数是从0开始，所以长度是n，但最后一个的索引是n-1)，为6
  expr [llength $list1] - 1					# 执行expr后边的表达式，结果为5
  lindex $list1 [expr [llength $list1] - 1]	#根据索引获取对应的值,为f
  ```

  ```tcl
  #lappend指令
  set a {1 2 3}		
  lappend a 4			# 语法格式:lappend 列表 新元素
  puts $a
  >> 1 2 3 4
  # 如果lappend一个列表，则会将这个列表当作一个元素追加至末尾
  set a {1 2 3}
  set b {4 5}
  lappend a $b
  >> 1 2 3 {4 5}
  # 如果要获取其中的4
  lindex [lappend a $b] 3					# 获取第三个元素，也就是取出 {4 5} 这个整体
  lindex [lindex [lappend a $b] 3] 0		# 访问这个整体的第0个元素，即4
  ```

  ```tcl
  # lsort指令
  # 语法格式：	lsort 开关 列表
  # 功能：将列表按照一定规则排序
  # 开关：缺省时默认按照ASCII码进行排序
  #     -real 	按照浮点数值大小排序
  #	  -unique	唯一化，删除重复元素
  set list1 {a b c d e f}
  lsort $list1				#按照ASCII码排序
  >> a b c d e f
  
  set list2 {-2 3.1 5 0}		# 按照数字大小排序
  lsort -real $list2
  >> -2 0 3.1 5
  
  set list3 {a c c b a d}		# 唯一化
  set -unique $list3
  >> a b c d
  
  # 例：取列表中的最小值
  set list1 {0 1.2 3 -4 5}
  lsort -real $list1					# 排序
  lindex [lsort -real $list1] 0		#取第0个
  ```

### 1.3 数学运算指令expr

- 语法格式：`expr` `运算表达式`，支持加减乘除和逻辑运算，由下例注意运算数的整型/浮点型对结果的影响

  ```tcl
  expr 6+4		# 10
  expr 5/2		# 2
  expr 5/2.0		# 2.5
  expr 5.0/2		# 2.5
  ```

  

## 2. 正则文本

### 2.1 控制流

- 控制流-`if`

  ```tcl
  # 注意：脚本语句的’{‘一定要写在上一行
  if {条件} {
  	脚本语句
  }elseif {条件}{
  	脚本语句
  }else {
  	脚本语句
  }
  ```

- 循环指令-`foreach`

  ```tcl
  # 语法格式：foreach 变量 列表 循环主体
  # 功能：从第0个元素开始，每次按顺序取得列表的一个元素将其赋值给变量，然后执行循环主体一次，直到列表最后一个元素
  set list1 {3 2 1}
  foreach i $list1 {
  puts $i
  }
  >> 3
  >> 2
  >> 1
  ```

- 循环控制指令-`break`，`continue`

  ```tcl
  # 语法格式：break,与C语言类似，功能是结束循环
  ```

  ```tcl
  # 语法格式：continue，功能是仅结束本次循环，即跳出本次循环，进行下一次循环
  ```

- 循环控制指令-`while`

  ```tcl
  # 语法格式：while 判断语句 循环主体
  while {$i>0}{
  puts $i
  incr i -1;
  }
  >> 3
  >> 2
  >> 1
  ```

- 循环控制指令-`for`

  ```tcl
  # 语法格式：for 参数初始化 判断语句 重新初始化参数 循环主体
  for {set i 3}{$i>0}{incr i -1}{
  puts $i
  }
  >> 3
  >> 2
  >> 1
  ```




### 2.2 过程函数

- 过程函数-`proc`

  ```tcl
  # 语法格式：proc 函数名 参数列表 函数主体
  # 功能：类似C中的函数，用户自定义功能，可以多次调用
  proc add {a b}{			# 定义一个函数proc，注意有花括号括起来
  set sum [expr $a + $b]
  return $sum
  }
  add 3 4
  >> 7
  ```

  - 全局变量与局部变量

    全局变量：在所有**过程之外**定义的变量；

    局部变量：对于在过程中定义的变量，因为他们只能在过程中被访问，并且当过程退出时会被自动删除。

    指令**`gloabl`**：可以在过程内部引用全局变量

    ```tcl
    set a 1
    proc sample {x}{
    global a					# 如果没有这句声明，则会报错提示没有'a'这个变量
    set a [expr $a + 1]
    return [expr $a + $x]
    }
    sample 3
    >> 5
    ```



### 2.3 正则匹配

- 定义：一种特殊的字符串模式，用来去匹配符合规则的字符串。

  **`\w`**，用来匹配一个字母、数字、下划线；**`\d`**，用来匹配一个数字。

  如果要匹配abc123，可以用`\w\w\w\d\d\d`来匹配，但是会过于繁琐。因此有以下的量词匹配规则。

- 量词：可以用来代替重复的匹配，常用以下三种量词

  | 符号 |       功能       |
  | :--: | :--------------: |
  |  *   | 零次或者多次匹配 |
  |  +   | 一次或者多次匹配 |
  |  ?   | 零次或者一次匹配 |

  ```tcl
  # 匹配 abc123
  \w+\d+ or \w*\d*
  ```

- 锚位

  用来指示字符串当中的开头和结尾的位置，使我们能够匹配到正确的字符。

  `^`，表示匹配字符串开头；`$`，表示匹配字符串结尾。

  比如要匹配`123abc456`，如果只要匹配开头的`123`，可以考虑`^\d\d\d`；如果要匹配结尾的`456`，可以用`\d\d\d$`.

- 其他字符

  常用的其他字符还有`\s`和`.`

  其中，`\s`表示空格，如匹配字符串`123 abc 123`，对应的表达式可以为`\d+\s\w+\s\d+`

  `.`表示任意一个字符，当不确定是什么字符时就可以用`.`表示。例如已知字符串为`xxx xxx xxx `(x为未知字符)，可以用`.+\s.+\s.+`匹配。

- 正则匹配指令`regexp`

  语法格式：`regexp?switches? exp string? matchVar? ?subMatchVar subMatchVar ...?`

  功能：在字符串中使用正则表达式匹配。

  - `switches`：-`nocase`将字符串中的大写都当成小写看待。
  - `exp`：自己写的正则表达式
  - `string`：用来进行匹配的字符串
  - `matchstring`：表示用正则表达式匹配的所有字符串
  - `sub1`：表示正则表达式中的第一个子表达式匹配的字符串
  - `sub2`：表示正则表达式中的第二个子表达式匹配的字符串

  ```tcl
  # 匹配字符串 abc456
  regexp {\w+\d+} "abc456"
  # 匹配一个以数字开头并且以数字结尾的字符串
  regexp {^\d.*\d$} "1 fsdal 1"
  ```

- 捕获变量

  - 通过`()`可以捕获字符串，例如将字符串"`Snow is 30 years old`"中的30捕获出来：

    ```tcl
    # 按照花括号里规则捕获的整个结果会指向total，花括号中圆括号的规则对应的捕获结果会指向age
    regexp {\s(\d+).*} "Snow is 30 years old" total age
    puts $total			# 花括号内的规则对应的捕获结果， 30 years old
    puts $age 			# 圆括号内的规则对应的捕获结果， 30
    ```

  - 一次捕获多个字符串，例如将上句话中的Snow和30一次捕获

    ```tcl
    regexp  {^(\w+)\s\w+\s(\d+).*} "Snow is 30 years old" total name age
    puts total			# Snow is 30 years old
    puts name 			# Snow
    puts age			# 30
    ```

    

### 2.4 文本处理

主要有以下三个常用指令：`open`，`gets`，`close`

- open
  - 语法格式： `open` `文件` `打开方式` (r表示读模式，w表示写模式)
  - 功能：打开文件

- gets
  - 语法格式：`gets` `fileId` `变量名`
  - 功能：gets和fileId标识的文件的下一行，并把该行赋值给变量，并返回该行的字符数（文件尾返回`-1`）

- close
  - 语法格式： `close` `fileId`
  - 功能：关闭文件。

```tcl
# 整个读入文件过程，file.txt和该编辑器在同一目录下
(bin) 9 % set file1 [open file.txt r]
file27eb5e55110
(bin) 10 % while {[gets $file1 line] >= 0} {puts "$line"}
123 
xixi
hah
456
(bin) 11 % close $file1

# 一个完整写入文件过程，如果没有该文件则会先自动创建该文件再写入
(bin) 12 % set fileid [open file1.txt w]
file27eb5e66b20
(bin) 14 % puts $fileid "hello world"
(bin) 15 % close $fileid
```

例：有一文本file.txt内容如下，写一TCL脚本求出所有`Slack`值之和。

```matlab
Slack = -0.051
Slack = -0.234
Slack = -0.311
Slack = -0.056
Slack = -0.434
Slack = -0.316
Slack = -0.151
Slack = -0.524
```

```tcl
set num 0		# 存储加和结果
set infile [open file.txt r]		# 以读模式打开文件
while {[gets $infile line] >= 0} {
	if {[regexp {^Slack\s+=\s(-\d+\.?\d+)} $line total slack]} {			# \s+ 标识匹配到多个数量（至少一个）的空格
		set num [expr $num + $slack]
	}
}
close $infile
puts $num
```



# 三. Synopsys TCL语言入门

## 1. TCL在EDA工具中的扩展与应用

<img src="./pics/image-20230802212321698.png" style="zoom:67%;float:middle">

> **综合软件中TCL的常见指令（指令名称后是指令的语法格式）**

- 指令1：`get_ports portsName` 

  指令功能：返回design中对应的ports object

  注意该指令是Synopsys TCL中的扩展指令，不是TCL的基础指令。

  **例1**：查看design中有没有一个port叫做CLK：`get_ports CLK`   ->  `{CLK}`

  **例2**：查看design中有没有一个port叫SPI：`get_ports SPI`   ->   `No object Found!`，这说明这个design中没有这个port

  **例3**：查看design当中所有的port(*是通配符)：`get_ports *`    ->	`{A B C D CLK OUT[0] OUT[1]}`

  **例4**：得到所有的C开头的port：`get_ports C*`	->	`{CLKA CLKB}`

  

- 指令2：`get_cells cellsName`

  指令功能：返回Design中对应的cell的instance name object

  - 概念回顾

    **reference name（ref name）**：在Verilog中写的每个module的名称

    **instance name**：一个module可能实例化一次甚至多次，这些实例化的模块名称就是instance name

    **例1**：想看design中有没有一个cell叫做U4：`get_cells U4`	->	`{U4}`

    **例2**：想查看design中所有的cell：`get_cells *`	->	`{U1 U2 U3 U4}`

    **例3**：想查看design中以3结尾的cells：`get_cells *3`	->	`{U3}`

    

- 指令3：`get_nets netsName`

  指令功能：返回design中net的object

  **例1**：查看design中有没有一个net以**INV**开头： `get_nets INV*`

  **例2**：查看design中所有的nets：`get_nets *`

  **例3**：查看design中有多少个net，使用TCL的基本语法：`llength [get_object_name [get_nets *]]`；

  ​	      使用扩展指令，`sizeof_collection [get_nets *]`，注意两者的区别：前者是tcl的基础语法，后者是DC的扩展指令。

  

- 指令4：`get_pins pinsName`

  指令功能：返回design中pin的object

  **例1**：查看design中有哪些pin的名字叫做Z	`get_pins */Z`	(这里的/是固定的，类似与路径)

  **例2**：查看design中有哪些pin的名字以Q开头	`get_pins */Q*`

  

> **数据类型：object与其属性**

- 说明：object是对于TCL脚本一个重要的扩展；常见的object有`cell`，`net`，`port`，`pin`；每种object有它的属性。

  下面将介绍一些常见属性：

  - 任何一个属性都可以用`get_attribute`得到；
  - `list_attribute -class *`可以得到所有object的属性；
  - 部分属性可以用`set_attribute`

- 属性介绍

  - **Cell object**

    属性`ref_name`：用来保存其map到的reference cell名称

    ```tcl
    get_attribute [get_cells -h U3] ref_name		# -h代指hierarachical
    >> {INV}
    ```

  - **Pin object**

    属性owner_net：用来保存与之相连的**net**的名称

    ```tcl
    get_attribute [get_pins U2/A] owner_net 	# 表示检查出与U2的管教Pin A相连的net的名称
    >>{BUS0}
    ```

  - **Port object**

    属性`direction`：用来保存port的方向

    ```tcl
    # get_ports A表示获取到端口A，再用get_attribute和direction获取这个端口的direction属性是in还是out
    get_attribute [get_ports A] direction
    >> {in}
    get_attribute [get_ports OUT[1]] direction		# 注意多位的端口实际也是一个一个的端口
    >> {out}
    ```

  - **Net object**

    属性`full_name`：用来保存net的名称

    ```tcl
    # 有两种方式，但注意要先用get_ports的方式将其取出
    get_attribute [get_nets INV0] full_name
    >>{INV0}
    get_object_name [get_nets INV0]
    >>{INV0}
    ```

- 通过属性进行更多的操作

  - **get_* -f** 

    说明：`-f`这个option可以用来过滤属性，以得到想要的object。

    ```tcl
    # 例1：得到所有方向是input的port
    get_ports * -f "direction==in"		# 是通配符
    >>{A B C D CLK}
    # 例2：得到所有方向是input的port
    get_ports * -f "direction==out"	
    >>{U1/Q0 U1/Q1 U2/Z U3/Z REGFILE/Q[0] REGFILE/Q[1]}
    # 例3：得到所有ref_name是INV的cell
    get_cells * -f "ref_name==INV"
    >>{U2 U3}
    ```

  - **get_* [object] -of**

    说明：`-of`这个option可以用来得到与指定object相连接的object

    object的连接关系是：

    - `--port object` <-> `net object`

      ```tcl
      get_nets -of [get_port A]		# 得到与端口A相连的net，结果是 A
      ```

    - `net object` <-> `port object/pin object`

      ```tcl
      get_net -of [get_pin U2/A]		# 得到与U2的Pin A相连的net，结果是 BUS0
      ```

    - `pin object` <-> `net object`

      ```tcl
      get_pin -of [get_net INV1]		# 得到与INV1这个net相连的pin,结果是 U3/Z
      ```

    - `cell object` <-> `pin object`

      ```tcl
      get_pins -of [get_cell U4]		# 得到与U4这个cell连接的所有Pin，结果是{U4/D0 U$/D1 REGFILE/Q1 REGFILE/Q2}
      ```

    

## 2.  使用TCL控制EDA工具流程

使用TCL设计DC的自动化Flow，后续再继续完善该部分内容。



# 四. STA基本概念

## 1. PrimeTime(PT)

**PrimeTime is a full-chip , gate-level static timing analysis tool that is an essential part of the design and analysis flow for today's large chip designs.**

<img src="./pics/image-20230802223502040.png" style="zoom:67%" />



## 2. STA Concepts

### 2.1 时序弧（Timing Arc）

静态时序分析中，时序弧用来描述两个节点之间的延时。时序弧通常又分为连线的延时和单元的延时。

> **单元延时(Cell delay)**

在计算该延时时，通常有两个地方需要注意：

一是`Transition delay`，指的是信号在翻转时的延时；二是逻辑的延时，指的是从Cell输入端口到输出端口的延时。



### 2.2 Setup time & Hold time

> Setup time

指在时钟上升沿之前，数据保持稳定的时间。通过建立时间来约束电路的**最大**延迟值不能超过Setup time的限制。

> Hold time

指在时钟上升沿之后，数据仍然保持稳定的时间。它约束的是一个**最小**的延迟值。



### 2.3 Timing Path 

时序路径要注意的是起点（Startpoint）和终点（endpoint）。

- 一条path的起点是一个时序单元的时钟引脚`clock pin`，或者一个design的`input port`。

  **注意pin和port的区别**，port是端口，有方向；pin是引脚，和net连接。

- 一条path的终点是一个时序单元（D触发器）的数据输入管脚`data input pin`，或者一个design的输出端口`output port`。

<img src="./pics/image-20230807211207504.png" style="zoom:50%;">

如上图，Path1 ~ Path4的起点分别是：A、clk、clk、A；终点分别是：D、D、Z、Z

> 总结

起点有两种，终点有两种，因此从起点到终点的时序路径有4中，分别是：

（1）`clk` -> `D`；	（2）`clk` -> `output`	（3）`input` -> `D`	（4）`input` -> `output`.

- 起点和终点中间可能有**多条路径**存在，并且起点到终点的时序检查可能**同时存在setup和hold同时违例**的情况。

  <img src="pics/start_end_multi_path.png" style="zoom:120%;" />



### 2.4 Clock Domains(时钟域)

在一个FPGA芯片的设计中，通常的时钟安排是全局异步、局部同步。DC/PT对于同步时钟可以分析，但对于异步时钟是无能为力的。

比如全局包括很多module，它们之间是异步的；而在每个module内部，都在同一个时钟域下，则是同步的。因此称为全局异步、局部同步。



### 2.5 Operating Conditions

- STA通常是在一个特定的操作条件下进行的。

- 一个操作条件被定义为三个指标的组合（**PVT**）：`Process`，`Voltage`，`Temperature`。

  - PVT通常还分为三种：**slow** process models，**typical** process 和 **fast** process models

  - slow和fast代表了两种极限的情况

    <img src="./pics/image-20230807213212908.png" style="zoom:50%;">

    如上图，以(b)(c)两图来看，分别取各自的Worst情况下对应的PVT，所以在高温低压情况下，应该延时最大，此时分析Setup time;

    如果分别取各自的Best情况下对应的PVT，所以在低温高压的情况下，应该延时最小，根据概念，此时应当分析Hold time。

- Cell和互联之间的延时必须基于特定的操作条件进行计算。



# 五. 标准单元库

## 1. Synoposys时序库概览

## 2. 非线性延时模型(NLDM)

> **线性延时模型**

例如，对于一个反相器而言，它的时序弧延时主要取决于两个因素：

- 一是输出负载，即反相器输出管脚上的电容负载；
- 二是输入信号的transition time。

通常而言，电容负载越大，延时越大；大多数情况下，延时也会随着输入transition time的增加而增加。

如果以一个线性模型来衡量该反向器的延时D，则$D=D_0+D_1\times S + D_2\times C$，其中$D_0,D_1,D_2$是常数，$S$是transition time，**$C$**是输出负载电容。

> **非线性延时模型**

非线性延时模型（NLDM，Non-Liner Delay Model），是以二维查找表的形式表示的。

通常情况下，实际的输入会在查找表中查不到，因为输入是近似连续的情况下，离散的查找表是查不到的。此时考虑高斯消元法。

$\begin{cases}0.227&=A+B\times0.098+C\times0.03+D\times0.098\times0.03 \\ 0.234&=A+B\times0.098+C\times0.06+D\times0.098\times0.06 \\ 0.323&=A+B\times0.587+C\times0.03+D\times0.587\times0.03 \\ 0.329&=A+B\times0.587+C\times0.06+D\times0.587\times0.06\end{cases}$ $\Longrightarrow A=0.2006,B=0.1983,C=0.2399,D=0.0677$.

则对于待查表的输入$(x_0,y_0)$，有：$Z=0.2006+0.1983x_0+0.2399y_0+0.0677\times0.32\times0.05$.



## 3. Threshold Specifications and Slew Derating

Slew因子可用于对查找表的缩放。设定的参数为`slew_derate_from_library`，这个参数也可以不指定。

如原来的抖动范围由10%~90%（80%跨度），而实际可能是30%-70%（40%跨度），设定该因子为0.5，就将原来的范围缩小了一半。如果要放大也是同理。

当slew derating确定时，在延时计算期间的内部slew值为：$\rm library\_transition\_time\_value \times slew\_derate$.

如果要调整功耗，如提高电压，那延时就会变小，查找表规模也会变小，延时就要重新计算，此时就要进行**K库**，对原表进行插值等操作，缩小规模。

**K库**：K指的是谐音“characterization”，意思是表征，指的是将标准单元stdcell的特性用lib的形式表征出来。



## 4. Timing Models

## 5. Wireload Models

使用一个分布式的RC树来表征互连线，包括两种：$\rm T-model$和$\rm \pi-model$.

例如对于一个线载模型，已知slope这个参数时，查找表定义扇出系数为1-5时的线长，那就可以根据斜率计算出扇出系数为8时的线长。

<img src="./pics/wireload_models.png" style="zoom:60%">



# 六. STA环境

## 1. 指定时钟约束

> 要定义一个时钟`create_clock`，需要提供以下信息：

- 时钟源（Clock Source）：可以是一个design的port，或者design内部cell的一个pin；
- 周期（Period）：时钟周期；
- 占空比（Duty Cycle）：高电平和低电平所占比例；
- 边缘翻转时间（Edge Times）：the times for the rising edge and the falling edge.

```tcl
# 通过create_clock命令创建一个时钟，SYSCLK和20都是可变的
# {0 5}代表在0时是上升沿，在5时为下降沿
# [get_ports SCLK] 指得到这样的一个端口，并将其定义为这样一个时钟
create_clock -name SYSCLK -period 20 -waveform {0 5} [get_ports SCLK]

# 定义一个周期为5的时钟,占空比50%
create_clock -period 5 [get_ports SCAN_CLK]
```



> 确定时钟不定性

通过对时钟设置一个更严苛的限制`set_clock_uncertainty`，使数据在指定的范围发生变化，从而使得整个逻辑电路更加稳健。

```tcl
set_clock_uncertainty -setup 0.2 [get_clocks CLK_CONFIG]
set_clock_uncertainty -hold 0.05 [get_clocks CLK_CONFIG]
```

<img src="./pics/image-20230808220724315.png" style="zoom:50%">

如上图，数据本来可以在第一个绿点处变化，但添加限制后，数据在第二个绿点处才变化，这样会使数据更稳定。

Uncertainty代表的含义如下图所示，其中的**Jitter**代表时钟的抖动：

<img src="./pics/image-20230808221400296.png" style="zoom:67%">

Jitter会影响setup违例，并且与设计和时钟频率无关，因为setup check在驱动与捕捉寄存器的时钟沿上；此外，jitter也是uncertainty的组成部分。

Skew是两个或多个[时钟信号](https://zhida.zhihu.com/search?q=时钟信号&zhida_source=entity&is_preview=1)之间的时间差：

例如，如果时钟树有500个[端点](https://zhida.zhihu.com/search?q=端点&zhida_source=entity&is_preview=1)（endpoints），并且Skew为50ps，则表示最长时钟路径和最短时钟路径之间的时间差为50ps。

Clock latency（[时钟延迟](https://zhida.zhihu.com/search?q=时钟延迟&zhida_source=entity&is_preview=1)）是从时钟源到终点的总时间。 Clock skew是时钟树到达不同终点的时间差。

**简言之，skew通常是时钟相位上的不确定，而jitter是指时钟频率上的不确定。**skew是指**最长时钟路径延时与最短时钟路径延时之差**。

set_clock_uncertainty指定一个窗口（window），时钟沿可能出现在该窗口内任何一个位置。

时钟边沿时序的uncertainty包含多个因素，例如[时钟周期](https://zhida.zhihu.com/search?q=时钟周期&zhida_source=entity&is_preview=1)jitter和用于STA额外的margins。



> 确定时钟-时钟延迟

包括两种时钟延迟，分别为**网络延迟（network latency）**和**源延迟（source latency）** 。

- 网络延迟是从定义的时钟（create_clock）到一个触发器的时钟管脚的延迟。

- 源延迟，是从时钟源到定义的时钟点的延迟。它可以代表片上或片下延迟。

<img src="./pics/image-20230808222340201.png" style="zoom:67%">

```tcl
# Secify a network latency of 0.8ns for rise,fall,max and min:
set_clock_latency 0.8 [get_clocks CLK_CONFIG]
# Specify a source latency:
set_clock_latency 1.9 -source [get_clockes SYS_CLK]
# Specify a min source latency
set_clock_latency 0.851 -source -min [get_clocks CFG_CLK]
# Specify a max source latency
set_clock_latency 1.322 -source -max [get_clocks CFG_CLK]
```

> 最大时钟延迟(max clock latency)：时钟源点到最远sink点的延迟值.



## 2. 生成时钟(Generated Clocks)

<img src="./pics/image-20230808223749322.png" style="zoom:75%;">

```tcl
# create a master clock with name CLKP of period 10ns with 50% duty cycle at the CLKOUT pin of the PLL
create_clock -name CLKP 10 [get_pins UPLL0/CLKOUT]
# Creates a generated clock with name CLKPDIV2 at the Q pin of filp-flop UFF0
# The master clock is at the CLKOUT pin of PLL.
# And the period of the generated clock is double that of the clock CLKP,that is,20ns.
create_generated_clock -name CLKPDIV2 -source UPLL0/CLKOUT -divide_by 2 [get_pins UFF0/Q]
```

- 生成时钟也可以是master时钟，但会有很多弊端。在一个生成时钟中，时钟源是master时钟而不是生成时钟。

- 如果把CLKDIV2也定义为master_clk而非generated_clk，系统会认为CLKDIV2和CLKP没有关系了。如果是generated_clk，就会认为它们是同步时钟（二分频）；如果定义成独立的master，STA在处理时就不会把它俩看作同步时钟而是看作异步时钟，而异步时钟在数据传输时是不能做setup和hold分析的。

- 如下的情况，两个时钟相与后得到的新时钟相对于源时钟已经产生了很多不确定性，所以可以把输出当作master时钟。

<img src="./pics/generated_clock.png" style="zoom:60%">



## 3. 输入和输出路径约束

对于一般的一条时序路径，是从一个D触发器的CLK_PIN到下一个D触发器的input pin，中间会有一个clk的延时。

而对于如下情况，CLK_PIN是没有直接接在D触发器上的，所以要设置一个**input delay**.

<img src="./pics/constrain_input_path.png" style="zoom:50%">

```tcl
# 实际根据C1和Tclk2q的最大最小值同样可以设置延时的最小值
set Tclk2q 0.9
set Tc1 0.6
# 设置INP1在CLKA上的最大延时等于经过D触发器的延时加上C1的延时
set_input_delay -clock CLKA -max [expr $Tclk2q + $Tc1] [get_ports INP1]
```

对于OUTPUT的设置也是同理，如下图所示。

<img src="./pics/constrain_output_path.png" style="zoom:65%">



## 4. 时序路径组(Timing Path Groups)

时序路径分组，依据是时序路径的终点。

> **外部属性的建模**

虽然在一个进行时序分析的Design中，用`create_clock`，`set_input_delay`和`set_output_delay`已经足够约束所有路径，但是对于获取block的IO pins的精确时序信息还是不够。下边是主要的一些外部属性。

- inputs

  用其中任意一个来指定slew：`set_drive`、`set_driving_cell`、`set_input_transition`.

  - **set_drive**

    对DUA的input pin上的驱动电阻确定一个值，确定驱动强度。值越小（最小0，代表驱动强度无穷大），驱动强度越高；反之越低。

    <img src="./pics/drive_strength.png" style="zoom:80%">

    ```tcl
    # 设定输入UCLK的驱动强度为100
    set_drive 100 UCLk
    # 上升沿和下降沿的驱动强度是不同的
    set_drive -rise 3 [all_inputs]
    set_drive -fall 2 [all_inputs]
    ```

  - **set_driving_cell**

    该指令会更方便和直接一些。

  - **set_input_transition**

    可以直接指定：`set_input_transition 0.85 [get_ports INPC]`

- outputs

  用`set_load`来指定output pin上的电容负载。示例如下：
  
  ```tcl
  # 在output port OUTX设置一个5pF的load
  set_load 5 [get_ports OUTX]
  # set 25pF load capacitance on all outputs
  set_load 25 [all_outputs]
  ```



## 5. Design Rule Checks (DRC)

在STA中最常用的两个设计规则是**最大偏移量和最大电容量**。基于以下两个指令：

- **set_max_transition**
- **set_max_capacitance**

```tcl
#Set al limitof 600ps on IOBANK
set_max_transition 0.6 IOBANK
# Max capacitance is set to 0.5pf on all nets in current design.
set_max_capacitance 0.5 [current_design]
```

除此之外还有：`set_max_fanout`(确定一个设计中所有管脚的扇出限制)、`set_max_area`(for a design)，但并没有上边两个更常用。



## 6. Virtual Clocks

虚拟时钟也是一个时钟，但是不和design的任何pin或port连接。作用是在STA中**作为一个参考**，确定相对于某个时钟的输入和输出延迟。

<img src="./pics/virtual_clocks.png" style="zoom:70%;float:left">



## 7. Refining the Timing Analysis

通过以下四个指令，可以让STA做的更加精细。

<img src="./pics/refine_sta.png" style="zoom:80%;float:left">



# 七、Setup和Hold检查

## 1. Setup Timing Check

### 1.1 触发器到触发器的时序约束

如下图所示，数据必须在$T_{setup}$之前保持稳定。

<img src="./pics/setup_time_check.png" style="zoom:65%">

上面的延时关系可以用如下式子表示出来，目的是为了保证数据和时钟之间的对应关系，这就是对$T_{seup}$的约束：
$$
T_{launch}+T_{ck2q}+T_{dp} \leq T_{capture} + T_{cycle} - T_{setup}
$$
如下是延时的分析过程，最后的判断依据是上面这个不等式。其中CLKM是起点，没有延时。

<img src="./pics/D-f2f-delay.png" style="zoom:60%;float:left">

<img src="pics/D-f2f-delay_1.png" style="zoom:60%;float:left" />

### 1.2 从输入到触发器

如下图，**INA**到**D**这条路径是要做STA的对象。

<img src="./pics/input_to_flip_flop_1.png" style="zoom:50%;float:left">

按照在STA环境中的内容，要对`INA`进行`input delay`的设置。图中`INA`应该是代表port，先关注Max delay。

```tcl
create_clock -name VIRTUAL_CLKM -period 10 -waveform {0 5}
set_input_delay -clock VIRTUAL_CLKM -max 2.55 [get_ports INA]
```



### 1.3 触发器到输出的PATH

与输入的约束类似，这里需要确定的是port上的load，指令是`set_load`.如下是一个例子：

<img src="./pics/d_filp_flop_to_output.png" style="zoom:60%;float:left">

其中比较关注的是给ROUT之前的组合逻辑留存的余量是否足够，下表是各部分的时延，**这里的5.10就把setup包含在其中了**：

<img src="pics/dfip_to_flop_delay.png" style="zoom:50%;float:left" />



### 1.4 Input to Output Path

如下是一个示意图：

<img src="pics/input2output_delay.png" style="zoom:67%;float:left" />

```tcl
set_input_delay -clock VIRTUAL_CLKM -max 3.6 [get_ports INB]
set_output_delay -clock VIRTUAL_CLKM -max 5.8 [get_ports POUT]
```

如上图所示，DUA的时延就是$T_{DUA}=\rm T-input\_delay-output\_delay$.

<img src="pics/output_delay_1.png" style="zoom:70%;float:left" />

<img src="pics/output_delay_2.png" style="zoom:70%;float:left" />

上图所示，最后slack<0，不满足时序要求，需要重新考虑。



## 2. Hold Timing Check

下图是Hold Time的定义的图解：

<img src="./pics/hold_time_def.png" style="zoom:75%;float:left">

### 2.1 触发器到触发器的时序约束

Hold时序检查，是在发数据的触发器和捕获数据的触发器之间，保持时间必须满足。这两个触发器的时钟是否相同都可以。

<img src="pics/hold_time_check_d2d.png" style="zoom:90%;float:left" />

- 要注意的是，**launch和capture的hold time的沿其实是同一个沿**，只是由于路径长短的原因有一点时差。

  对于数据而言，经过的时间是从$T_{launch}+T_{ck2q}+T_{dp}$，而capture的沿是同一个沿再加上一个Holdtime.

  由于caputure（UFF1）是对数据进行采样的，所以如果数据在Holdtime期间变化，采样到的就是不稳定的。

- 描述这部分的公式为：$\begin{align}T_{launch}+T_{ck2q}+T_{dp}\geq T_{capture}+T_{hold}\end{align}$，因此**hold time描述的就是满足时序的最小延时**。

- 如下是一个理想情况下的holdtime分析的时序报告（**注意到Path Type是min**）：

  <img src="pics/holdtime_checkTable1.png" style="zoom:65%;float:left" />

  <img src="pics/holdtime_checkTable2.png" style="zoom:50%;float:left" />

  注意和setup的区别，这里的时间量是增加的，而setup是减小的。

  原因是因为holdtime关注的是时钟上升沿之后数据保持稳定的时间，setup则是上升沿到来之前数据保持稳定的时间。

  对于holdtime而言，数据的到达必须要在holdtime之后，所以这里要在上升沿之后不断加上各种时延作为holdtime。

### 2.2 从触发器到输出

如下图所示，是从触发器到输出的Path，这里也是和输入的类似，引入了一个虚拟clock，因此最小的延时2.5ns里也包含了holdtime.

<img src="pics/d2output_1.png" style="zoom:67%;float:left" />

 如下是时序报告的数据：

<img src="pics/d2output2.png" style="zoom:67%;float:left" />

<img src="pics/d2output3.png" style="zoom:50%;float:left" />

最后的$slack=0.33-(-2.45)=2.78>0$，满足要求。



### 2.3 Input to Output Path

- 下图是图示，考虑的是中间部分的DUA的STA PATH：

<img src="pics/i2o__path_1.png" style="zoom:67%;float:left" />

- 然后对路径添加约束：

<img src="pics/i2o_path_constrain.png" style="zoom:60%;float:left" />

- 查看时序报告：

<img src="pics/i2o_report_1.png" style="zoom:50%;float:left" />

<img src="pics/i2o_report_2.png" style="zoom:43%;float:left" />



# 八、特殊时序检查

## 1. Multucycle Paths

如下图所示，中间的组合逻辑可能会占用最多3个clock，而不是很短的时间，所以做STA时要对这部分进行多周期时序路径检查。

<img src="pics/multicycle-1.png" style="zoom:67%;float:left" />

```tcl
create_clock -name CLKM -period 10 [get_Ports CLKM]
# 设置多周期检查为检查3个周期，注意检查的是setup
set_multicycle_path 3 -setup -from [get_pins UFF0/Q] -to [get_pins UFF1/D]
```

<img src="pics/multicycle-2.png" style="zoom:60%;float:left" />

如果多周期检查设为3，就要往后推3个时钟周期，默认会往前一个cycle来检查setup time.

而**Holdtime**的检查，默认是在setup的前一个沿，如下图所示（其中写的移2个是在默认1个的基础上再移）：

<img src="pics/multicycle-3.png" style="zoom:60%;float:left" />

因此在多周期设置时，特别要注意：

- 对setup进行检查时，一定要检查设定到第几个时钟；
- 对hold进行检查时，对hold要设定一个**N-1**的回溯，否则对hold的检查会很难通过时序检查。



默认情况下综合工具会把每条路径都定义为单周期，也就是源触发器在时钟的任一边沿启动的数据都应该由目的时钟的下一个上升沿捕获。但如果数据从起点到终点的传输时间需要一个周期以上，就称为多周期路径。在设计中有很多这种情况，比如当两个触发器之间的逻辑在一个时钟周期执行不完，通常有两种做法：①插入寄存器，使用流水线设计，切分组合逻辑，使其可以在单周期完成；②使用使能信号控制，若干周期读取一次。对于第二种情况，就需要使用多周期约束。 
链接：https://www.nowcoder.com/exam/interview/80043774/test?paperId=50270037&order=0



## 2. Half-Cycle Paths

如下图所示，如果在CLK的下降沿launch数据，而在上升沿capture数据，留给组合逻辑的时间就是半个CLK周期。

<img src="pics/half_cyle_path.png" style="zoom:100%;float:left" />

- setup的检查：这里要注意，时钟的起点(startpoint)和终点(endpoint)，不能理所当然的认为都是上升沿触发。

<img src="pics/rise_edge_half_cycle.png" style="zoom:67%;float:left" />

<img src="pics/rise_edge_half_cycle_setup.png" style="zoom:70%;float:left" />

半周期检查的作用就是放宽holdtime的要求，保证电路的处理不会很棘手。



## 3. False Paths(伪路径)

有些时序路径实际并不可能存在，这种路径可以在STA期间通过设置为False Path来关闭检查 。出现的原因可能有：

- 从一个时钟域到另一个时钟域
- 从一个触发器的时钟pin到另一个触发器的input.

<img src="pics/false_path.png" style="zoom:67%;float:left" />

- 设置false path时，推荐从时钟到时钟，而非从pin到pin

  ```tcl
  set_false_path -from [get_clocks clockA] -to [get_clocks clockB]	# use
  set_false_path -from [get_pins {regA_*}/CP] -to [get_pins {regB_*/D}] 	# unrecommend
  ```

- 减小`-through`选项的使用，因为会增加不必要的运行复杂度，除非十分必要且没有别的办法来确定flase path。



## 4. 多时钟时序

### 4.1 多时钟域的时序

#### 4.1.1 慢时钟到快时钟域

如下图，是两个不同时钟域的实例，为了更清楚，加上一个时钟约束的例子：

```tcl
# CLKM周期20，占空比50%
create_clock -name CLKM -period 20 -waveform {0 10} [get_ports CLKM]
# CLKP周期5，占空比50%,两个时钟是四分频的关系
create_clock -name CLKP -period 5 -waveform {0 2.5} [get_ports CLKP]
```

<img src="pics/timing_arcross_clk_domain.png" style="zoom:67%;float:left" />

<img src="pics/slow2fast_clock.png" style="zoom:80%;float:left" />

下面是对此的一份检查**setup_time**的时序报告：

<img src="pics/slow2fast_report_1.png" style="zoom:75%;float:left" />

<img src="pics/slow2fast_report_2.png" style="zoom:75%;float:left" />

注意**时间起点**，对于launch path，起点是0；而对于capture path，起点是5ns.

对于上面的时钟CLKP，如果在第四个时钟周期再进行capture，这样就可以将时间拉到和慢时钟一样快，而且时序不会很严苛。

所以刚好可以用到前面讲的多周期路径：

```tcl
# -end选项指定了多周期4是用于终点（endpoint)或捕获时钟的
set_multicycle_path 4 -setup -from [get_clocks CLKM] -to [get_clocks CLKP] -end
```

<img src="pics/setup_hold_time_slow2fast.png" style="zoom:65%;float:left" />

但是对于上图，如果Hodltime就按照默认的回退一个cycle来计算，就会造成**holdtime很长**的问题，不太合理，如下所示：

<img src="pics/slow2fast_holdtime_check.png" style="zoom:50%;float:left" />

所以检查holdtime时，同样利用**多周期Path检查**，设置它的检查提前，向前挪3个，**即0ns时刻**：

```tcl
set_multicycle_path 3 -hold -frame [get_clocks CLKM] -to [get_clocks CLKP] -end
```

<img src="pics/hold_time_slow2fast_forward.png" style="zoom:67%;float:left" />

> **总结**

如果对setup的检查设置了**N**个cycle的多周期延迟，也要最好给holdtime设置**N-1**个周期的multicycle。



#### 4.1.2 快时钟到慢时钟域

如下图所示，是一个从快时钟到慢时钟的示意：

<img src="pics/fast2slow_1.png" style="zoom:50%;float:left" />

上边的在Setup4检查实际是最严苛的，对于慢时钟的capture不太友好。

通常情况下，从快时钟到慢时钟确定数据的path一般是一个multicycle path。如果把setup check 放松两个周期：

```tcl
# The -start option refers to the launch clock and is the default for a multicycle hold.
set_multicycle_path 2 -setup -from [get_clocks CLKP] -to [get_clocks CLKM] -start
set_multicycle_path 1 -hold -from [get_clocks CLKP] -to [get_clocks CLKM] -start 
```

<img src="pics/setmultcycle_fast2slow.png" style="zoom:67%;float:left" />

对于Hold的multicycle设置，确保了早期数据能够可靠的在0ns时capture到，因为launch的沿也在0ns开始.

> **-end**用来约束capture clk，**-start**用来约束launch clk.



### 4.2 多时钟

分两种情况，分别为**整数倍时钟关系**和**非整数倍时钟关系**，这里的关系是指来自于同一个时钟源的CLKP和CLKM的频率是否是整数倍关系。

对于`整数倍`关系，和上边的快慢时钟域的分析是完全类似的；

对于`非整数倍`关系，则对两个时钟周期(单位:ns)取公倍数，这样就可以在`0~公倍数`这个时间段内对齐，然后分析严苛情况下的setup和hold.



### 4.3 相位移动

<img src="pics/phase_shifted_1.png" style="zoom:55%;float:left" />

setup的检查在0~0.5，而hold的检查则默认是往前推一个时钟周期，即比如在2.0ns发送，在0.5ns时capture.

这里就有点类似于前边的半周期路径。这里对于setup不太友好，但对于hold是比较友好的。

<img src="pics/hold_check.png" style="zoom:50%;float:left" />

<img src="pics/hold_check_1.png" style="zoom:50%;float:left" />

从时序报告可以看出，launch是从2.0开始的，而capture是从0.5开始的，这是对hold的检查，是可以满足要求的。



# 九、Roubst Verification

由于工艺和处理差异问题，同样的MOS管在带上的不同部分不会有相似的特性。这些差异可能是由如下诸多因素引起的：

<img src="pics/on_chip_diff_1.png" style="zoom:50%;float:left" />



## 1. Setup Timing Check

通过对launch path或capture path设置放大或缩小系数(`derate factors`)，使得两者的时间相近。

derate factor一般是对所有的net和cell进行应用的。

如果cell和net调整系数不同，可以用`-cell_delay`或`-net_delay`在`set_timing_derate`指令下的参数中设置：

```tcl
set_timing_derate -cell_delay -early 0.9		# 减少10%
set_timing_derate -cell_delay -late 1.0			# 1.0相当于没有放大也没有缩小
```

如果在launch和capture做derate时，有**共同路径**，就会产生Pessimism问题，这个问题叫做**Common Path Pessimism（CPP）**.

对此，在path中经常会列出**CPPR(Common Path Pessimism)**来报告这个问题，也叫做**Clock Reconvergence Pessimism Removal(CRPR)**.

Common Point的定义是，在时钟树的共同部分的上一个`cell`的`ouput pin`：

$CPP = LastetArrivalTime@CommenPoint\ -\ EarliestArrivalTime@CommonPoint$

<img src="pics/pessmism_problem_counter.png" style="zoom:50%;float:left" />

## 2. Hold Timing Check

launch clock 和data path比较快，而capture比较慢。

在考虑OCV时，可以考虑的方向是让data再往前，而hold的边界再往后一些，这个就是比较严苛的情况。

holdtime的检查使用该式：$LaunchClockPath\ +\ MinDataPath \ -\ CaptureClockPath \ -\ Thold\_UFF1 \geq0$.

上式说明数据的到达时间越过了capture的时间，也即在holdtime的后边，这样slack>0，就是满足要求的。

<img src="pics/holdtime_check_without_OCV.png" style="zoom:50%;float:left" />

<img src="pics/holdtime_check_with_OCV.png" style="zoom:50%;float:left" />

可见考虑了OCV后，时序会变的相对差一些。

<img src="pics/ocv1.png" style="zoom:50%;float:left" />

<img src="pics/ocv2.png" style="zoom:50%;float:left" />

<img src="pics/ocv3.png" style="zoom:50%;float:left" />

以上过程的文字表述大体如下：

<img src="pics/ocv4.png" style="zoom:50%;float:left" />



## 3. Time Borrowing(针对Latch)

对于电平触发的锁存器(Latch)和边沿触发的D触发器，在时序分析上有些地方是不同的，Time Borrowing是其中之一。

<img src="pics/time_borr1.png" style="zoom:70%;float:left" />

上面简单来说，就是电平跳变到有效电平后，就是opening edge；再跳变之后的电平，就是closing edge.

一般认为，数据应该在clock的active edge到来之前就准备好；

但是，由于latch在clock有效时是**透明状态**的，所以数据可以在clock的active edge之后到达，也就是说从下一个周期中**“借了时间”**。

如下图所示，数据DIN比clock的高电平来得早，在clock的高电平时对DIN采样就占用了Tb的一部分时间，但是仍然是可以采到正确的数据的。

<img src="pics/time_borr2.png" style="zoom:67%;float:left" />

下面是时间的划分，以及何种情况下会被认为是发生了时序违例。

<img src="pics/time_borr3.png" style="zoom:75%;float:left" />

更具体的几个例子可以参考视频链接：

[Time Borrowing]: https://www.bilibili.com/video/BV1if4y1p7Dq?p=13&amp;vd_source=120edfb88abb67ff33cc8befaca5167c	"instance"



## 4. Data To Data Check

Setup和hold的检查也可以用在任意两个数据管脚之间，并且这两个都不是时钟。

- 一个pin是被约束的pin，行为就像一个D触发器的数据pin;
- 第二个pin是与之相关的pin，其行为就像D触发器的一个时钟pin.

与之前Setup的检查的区别在于：

- Data to Data的检查是在同一个边沿上进行的(launch edge)；
- 不同于一般的触发器的setup检查，这里的capture clock边沿和launch clock edge是一个cycle的距离

因此，data to data的setup检查也叫做**zero-cycle check**或**same-cycle check**.

如下，是data to data的时序检查的图示：

<img src="pics/d2d_setup.png" style="zoom:50%;float:left" />

注意这里设置的起点是类似时钟的pin，即SDA；终点是被约束的pin，即SCTRL.

这种检查在自己设计的block中很有用，其中它很有必要给一个信号提供特定的相对于另一个信号的到达时间；

一种这样的常见情景：数据信号是通过一个enable信号进行gated的，它用来确保在数据信号到达时，这个使能信号是稳定的。

- 如果只想对引脚A1，A2进行相对的时序约束，需要在同一个边沿判断hold：

  <img src="pics/hold_d2d.png" style="zoom:50%;float:left" />

  除了上边的将hold往回推一个cycle的办法，还可以交换作为data和related pin的管脚关系：

  <img src="pics/hold_time_d2d_alternate_way_another.png" style="zoom:50%;float:left" />

- Data to data的check也可以用于定义一个不变化数据的check，具体操作如下所示：

  <img src="pics/d2d_setup_hold_data_nochange.png" style="zoom:50%;float:left" />



# 十、Clock Gating Checks

在了解**门控时钟的时序检查**之前，首先了解门控时钟的概念。

>  门控时钟

如下图所示，是数字IC设计不同层面所关心的主要内容，门控时钟的优化主要在RTL级：

<img src="pics/clock_gating_1.png" style="zoom:67%;" />

门控时钟就是在时钟的源头加一个门控，相当于一个开关。 如图所示：

<img src="pics/gate_clock_1.png" style="zoom:57%;" />

上图（左）是一个常见的门控时钟的简单思路，但是在EN和Clock没有对齐时，产生的新时钟会变成毛刺一样的信号。

处理的一个思路就是在与门之前，对Clock和EN加一个**负电平触发**的触发器Latch，可以看到在处理之后输出的门控时钟信号是比较好的。

如下所示，如果Gating cell的输出接到了后面的CK端，或者作为输出接到了下级芯片，那综合工具是可以把这个识别为门控时钟的；

如果把输出接到了下级的D端，那工具不会把它识别为一个门控时钟。

<img src="pics/gate_clock_2.png" style="zoom:50%;" />

- 有两种类型的门控时钟，分别是**高有效**和**低有效**的门控时钟，即**Gating pin为1或0**，如下所示：

  <img src="pics/type_two_gate_clock.png" style="zoom:50%;" />

  如果gating cell比较复杂（比如**与非门XOR或多路选择器MUX**），STA无法识别，通常会给出警告表示没有门控时钟，这个可以通过如下命令确定一个时钟门控关系来改变：

  ```tcl
  set_clock_gating_clock
  ```

  Active-high和Active-low的具体例子以及时序违例时的处理办法参考这个视频：

  [Active-High&Low]: https://www.bilibili.com/video/BV1if4y1p7Dq?p=15&amp;spm_id_from=pageDriver&amp;vd_source=120edfb88abb67ff33cc8befaca5167c	"处理办法"

  在视频中：Active-High的讲解在13:40开始，Active-Low的讲解在24:21开始.

- 门控时钟为MUX的情况

  如下图所示，门控时钟输出MCLK或TCLK是由S进行选择决定的：

  <img src="pics/gate_mux.png" style="zoom:60%;" />

  要注意的一点是，因为gating cell是一个选择器，门控时钟的check不会被自动推导出来，STA会出现如下的类似警告：

  <img src="pics/gating_check_warning.png" style="zoom:50%;" />

  这里可以通过增加一个`set_clock_gating_check`约束来消除警告：

  ```tcl
  set_clock_gating_check -high [get_cells UMUX0]		# 确定为active_high的检查
  set_disable_clock_gating_check UMUX0/I1		# 关闭TCLK连接的pin的检查，暂时不关心这个
  ```

- 对于有时钟取反的门控时钟的时序检查，可以通过`set_clock_gating_check`命令来确定给出时钟信号的门控信号的setup和hold的检查。

  <img src="pics/gating_check_inversion.png" style="zoom:50%;" />



# 十一、例题

## 1. Q1

把下图中的电路整体看作一个触发器，考虑以下4个问题。

<img src="./pics/q1.png" style="zoom:50%;" />

- **该电路的有效建立时间和保持时间分别是多少？**

  将时钟按照内外两部分分别来考虑，内部是指这个D触发器，外部则是针对这个触发器以外的部分：

  如图所示，对于内部的D触发器，其建立和保持时间是蓝色部分，按照定义，数据在这段时间内应该是稳定的。

  <img src="./pics/q1.svg" style="zoom:140%;" />

  > 时钟部分

  在D触发器的外部，CLK通过了一个延时为1ns的反相器，但注意到D触发器的CLk pin上还有一个取反的小圈，所以等于没有取反。

  因此外部时钟CLKOUT（绿色部分）延时1ns后得到内部时钟CLKIN（蓝色部分）.

  > 数据部分

  由于内部D触发器的输出Q到输入D有2ns的延时，因此Data_out会比Data_in早2ns时间；

  > 总结和计算

  因此对于外部时钟而言，即题目上的CLK（图中是CLK_OUT），其时钟上升沿左侧，数据保持稳定的时间即setup=2+1=3ns，上升沿之后数据保持稳定的时间就是1ns。因此对于整个电路而言，有效的建立时间setup_time=3ns，保持时间hold_time=1ns.

- **该电路的最高时钟频率是多少？**

  根据建立时间的约束规则，最高时钟频率应根据setup_time来计算，**时钟工作频率和hold是没有关系的**。

  这里选择launch data的时序path，应当是从clk pin开始到触发器的input pin，即CLk-Q-D，该路径的时间$t_{logic}=t_{CQ}+t_{delay}=4+2=6ns$.

  而对于launch data的时序而言，要满足$t_{logic}\leq T-t_{setup}$，即$6\leq T-2$，故$T\geq 8ns$，$f\leq1/T=125MHz$.

  因此电路的最高时钟频率是125MHZ.

- **该电路的功能和哪个触发器的功能比较相似： A. 带使能的D触发器    B. T触发器    C. JK触发器    D. RS触发器**

  注意到**只有一路**数据输入，而且输出OUT和输入进行了异或，所以应该是T触发器的结构。

  如用排除法，根据只有1路输入，就可以排除C、D；而**D触发器的使能门控**通常是**与非、与门，或者或非、或门**，而不是异或门，A排除。

- **对于一个同步电路，以下哪个公式可以用于计算最高工作频率？**

  <img src="./pics/freq_calu.png" style="zoom:50%;float:left" />

  因为时钟工作频率和holdtime并没有关系，所以含有holdtime的都不对，可以排除D之外的所有选项。



## 2. Q2

如下是一份时序报告，根据该报告回答后面几个问题。

<img src="pics/timing_report_1.png" style="zoom:80%;" />

<img src="pics/timing_report_2.png" style="zoom:80%;" />

<img src="pics/timing_report_3.png" style="zoom:78%;" />

<img src="pics/timing_report_4.png" style="zoom:79%;" />

- 报告分析

  <img src="pics/timing_report_6.png" style="zoom:80%;" />

  如上图所示，时序路径的起点和终点都是上升沿触发，因此这个路径属于**reg to reg**类型，且保留一个时钟周期的检查时间，而不是半个周期。

  如果是两个相反跳变的沿，就只留了半个周期的时间进行check.

  时序报告的最后一张图，是capture path的时序分析。最后的slack<0，时序违例，时序约束不满足要求。

- **What does  the above information represent?**

  这是一份**PT/DC**的时序报告，It shows there is a -0.1234ns reg2reg setup violation.

- **What type of tool might have produced the above report?**

  由于时序报告中时钟网络的延时为0，所以应当是从DC出来的，该阶段可能使用了Synopsys PT进行了分析。

- **What is the name and frequency  of the clock referenced?**

  时钟名字叫`my_clock`。计算capture path时间是从**时钟周期T**开始减的，时钟频率为$\rm 1/1.8ns$，周期$\rm T= 1.8ns$.

- **What frequency can the circuit above actually run at?**

  时序报告中，数据到达时间为1.8533ns，根据之前的包含时钟周期的公式（并标出相关数值）：
  $$
  \underbrace{T_{lauch}+T_{ck2q}+T_{dp}}_{data\_arrivaltime=1.8533{\rm ns}}
  \leq 
  \underbrace{T_{capture}}_{0ns}+T_{cycle}-\underbrace{T_{setup}}_{0.0701{\rm ns}}\\
  \rightarrow \quad T_{cycle}\geq (1.8533+0.0701){\rm ns}=1.9234{\rm ns} \\
  \rightarrow \quad f_{max} \leq \frac{1}{T_{cycle}}=\dfrac{1}{1.9234{\rm ns}}\approx 520{\rm MHz}
  $$
  因此根据以上计算，可知电路实际运行的频率为520MHz.

- **How many levels are there between the two flip-flops?**

  起点是fifo_rd_pd_reg_16\_，终点是we_bank0_reg_2_，可以直接数，并且在起点和终点之间每一级及其延时都会在report上标出，共有28级。

- **What component has the greatest delay through it?**

  要除flop以外，延时最大的地方就是第三列的**增量Incr**最大的地方.

  可以看到最大延时是0.1378，对应的component为DP_OP_248_5346_8_U51/CO0(AFCSHCINX2).

  <img src="pics/greatest_delay.png" style="zoom:100%;" />

- **What is the setup time of the last filp-flop in the path?**

  根据时序报告，最后一个触发器的setup time = 0.0701ns.

- **What is the hold time of the last filp-flop in the path?**

  根据开头的max可知这是一份setup time的时序报告，所以是不知道hold time的.

- **What assumptions can you make about the opertaing conditions from this report?**

  setup time考虑的是最差的情况下的时序，因此是Worst Case.

- **Would this report be useful near the end (tape-out) of a chip? Explain your answer.**

  No.由于时钟延迟都是理想的，因此这个报告应是逻辑综合DC阶段产生的，该阶段产生的时序实际是不准确的。

  因为还没有做布局布线、时钟树等，所以对流片并没有什么帮助。



## 3. Q3

<img src="pics/q2.png" style="zoom:70%;" />

- **如果要计算这个电路的时序，需要知道哪些参数？**

  如果要对这个电路进行时序分析，从下式考虑，从clock出发，这里只有一条时序路径$CK\rightarrow Q(Flop A)\rightarrow logic \rightarrow D(Flop B)$.
  $$
  T_{launch}+T_{ck2q}+T_{dp} \leq T_{capture} + T_{cycle} - T_{setup}
  $$
  对于launch而言，从clock到Flop B的input data pin(D)上，需要考虑`Flop A`的$CK\rightarrow Q$的时间$T_{ck2q}$；`logic`的$\rm input\rightarrow output$的时间$T_{dp}$；

  对于capture而言，要考虑`Buffer`的时延$T_{buf}$；要考虑`Flop B`的`setup timing requirement`和`hold timing requirement`；

  此外，还有时钟本身的因素，要知道时钟的周期period：$T_{cycle}$，以及时钟的uncertainty.

- **使用上面的参数，写出电路在Flop B上的setup time要满足的式子。**

  setup time 计算的是max delay，思想是数据在上升沿之前保持稳定的时间界限不能越过setup time的左边界，否则采样的数据不稳定。
  $$
  T_{cycle}+T_{buf}-T_{setup}-T_{uncertainty}\geq T_{ck2q}+T_{logic}
  $$
  主要的思路就是**capture的时间要大于launch的时间**，这样才能采到稳定的数据。

- **使用上面的参数，写出电路在Flop B上的hold time要满足的方程。**

  hold time计算的是min delay，思想是数据在上升沿之后保持稳定的时间要大于等于hold time，这样才能保证采到稳定的数据。
  $$
  T_{buf}+T_{hold}\leq T_{ck2q}+T_{logic}
  $$
  这里hold加不加clock uncertainty都可以，取决于check是否需要考虑这个因素。



## 4. Q4(Based on Q3)

在Q3的基础上，在第一个Flop的输入端加上与门：

<img src="pics/q3.png" style="zoom:67%;" />

- **要计算该电路的时序问题，需要考虑哪些新的参数？**

  在原来的基础上，**多了2条**timing path，一条是`Flop A`的$Q\rightarrow And \rightarrow D$，一条是经过与门`And`的$Input\_Port\rightarrow D(Flop A)$.

  对于与门`And`而言，要知道它的$Max\ delay 和Min\ delay$；

  对于`Singal X`而言，由于它是一个`Input port`，所以要知道它的$Max\ and\ Min\ Input\ delay$.

- **新增加了And这个cell之后，还需要考虑哪些问题？**

  添加了And之后，Flop A的负载会发生变化。

  对于Flop A而言，负载变化后，要将`Singal X`的**input delay**和**And delay**考虑到`Flop A`的`setup time`和`hold time`的约束中。

  而logic的考虑主要有2方面，分别是`input transition`和`output cap`.

  这个新负载的增加会导致logic中延时的变化，所以需要重新考虑这条路径的延时，对于Flop B的setup和hold的计算也要重新确认。
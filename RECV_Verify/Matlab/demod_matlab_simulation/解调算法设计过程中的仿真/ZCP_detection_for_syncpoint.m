close all;
clear;
clc;
% --------------------------------------------------------
%对相位折线进行29点最小二乘法，求斜率曲线，根据其过零点修正中心频率，进而求同步点
% --------------------------------------------------------
%%%%%%%产生相位连续的FSK调制信号进行ADC采样后作为输入%%%%%%%%%%%%%
sample_rate = 27.095;%采样速率，单位MHz
data_bit = 100;
src_data([1:2:data_bit-1])=1;
src_data([2:2:data_bit])=0;
BPS_data = 0.56448 ;  %数据传输速率，单位MHz
freq_0 = 4.234 - 0.282 + 0.2  ;
freq_1 = 4.234 + 0.282 + 0.2  ; %比特0和比特1的频点，单位MHz
i_data_ADC_0  = FSK_mod(src_data,freq_0,freq_1,sample_rate,BPS_data)-2048;
%频偏变化
freq_0 = 4.234 - 0.282 + 0.1  ;
freq_1 = 4.234 + 0.282 + 0.1  ; %比特0和比特1的频点，单位MHz
i_data_ADC_1  = FSK_mod(src_data,freq_0,freq_1,sample_rate,BPS_data)-2048;
%频偏变化
freq_0 = 4.234 - 0.282 - 0.05  ;
freq_1 = 4.234 + 0.282 - 0.05  ; %比特0和比特1的频点，单位MHz
i_data_ADC_2  = FSK_mod(src_data,freq_0,freq_1,sample_rate,BPS_data)-2048;

i_data_ADC = [i_data_ADC_0,i_data_ADC_1,i_data_ADC_2];
%%%%%%%%%%%%%%%%%%%%%%带内干扰%%%%%%%%%%%%%%%%%%%%%
% src_data_noise = [1,1,1,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
% f1_noise = 4.234 - 0.22 ;
% f0_noise = 4.234 + 0.22 ;
% FSK_data_noise  = FSK_mod(src_data_noise,f0_noise,f1_noise,sample_rate,BPS_data)./7;
% FSK_data_noise = FSK_data_noise(1:500);
% i_data_ADC (475:475+length(FSK_data_noise)-1) = i_data_ADC (475:475+length(FSK_data_noise)-1)+ FSK_data_noise;
% i_data_ADC (20000:20000+length(FSK_data_noise)-1) = i_data_ADC (20000:20000+length(FSK_data_noise)-1)+ FSK_data_noise;

%%%%%%%%%%%%%%%%%%%%%%加入数字滤波器产生相移%%%%%%%%%%%%%%%%%%%%%
Hd = LP_8M_phase10degree;
i_data_ADC = filter(Hd,i_data_ADC);

% %%%%%%%%%%%%%%%%%%%%%%读取ADC采样报文作为输入%%%%%%%%%%%%%%%%%%%%%
% ADC_data_hex = textread('D:\lab_project\BTM_RCV_3C25\leexiao\24点求和抵消算法求同步点matlab仿真\思诺应答器丢数据ADC采样数据.txt','%s');
% i_data_ADC   = hex2dec(ADC_data_hex)' -2048;

%% 理论算法
%% 求相位折线
hilbert_fsk = hilbert(i_data_ADC);  %Hilbert变换 求出理论上的解析信号
hilbert_fsk_imag = imag(hilbert_fsk);    %取虚部
atan_theroy = atan2(imag(hilbert_fsk),real(hilbert_fsk));% 相位归一化后求arctan值=29

correction=zeros( 1,length(hilbert_fsk) );%未计算时，初始化修正值为0
arctan_fsk=zeros(1,length(hilbert_fsk));%未计算时，初始化相位值为0
slope_29=zeros(1,length(hilbert_fsk));%未计算时，初始化斜率值为0
convergence_factor = 0.5;%收敛因子

for ii = 100:length(hilbert_fsk) %从第100个点开始进行中心频率修正
    arctan_fsk(1:ii) = unwrap(arctan_fsk(1:ii),pi);  %对相位进行2pi的修正
    if(ii>300)
    arctan_fsk(ii) = hilbert_fsk(ii) * exp( -j*ii*4.234*2*pi/sample_rate) * exp( -j*ii*correction(ii-1) );%处理中心频率,并根据估计的w0，w1的值修正中心频率；
    else
    arctan_fsk(ii) = hilbert_fsk(ii) * exp( -j*ii*4.234*2*pi/sample_rate) ;%处理中心频率,并根据估计的w0，w1的值修正中心频率；    
    end
    arctan_fsk(ii) = angle(arctan_fsk(ii)); %求出解析信号所对应的相位
    
    
    %% -----------------最小二乘法---------------------------------------
    %最小二乘法直线拟合29点
    N_29 = 14; % 2N+1点最小二乘法
    coeff_slope_29 = (1/2030)*[-N_29:N_29];    %斜率
    slope_29(ii-15) = coeff_slope_29 * arctan_fsk(ii-29:ii-1)' ;%斜率
    
    %% ---------------------中心频率修正---------------------------------------
    if(ii>300)
        if( ((slope_29(ii-40)<=0) && (slope_29(ii-40-1)>=0)) )%+ -
            statistic_negative = length(find(slope_29(ii-40:ii-40+15)<0));
            statistic_positive = length(find(slope_29(ii-40-16:ii-40-1)>0));
            if( (statistic_negative>12) && (statistic_positive>12)  )%大数判决，
%                 correction(ii) = correction(ii-1) + fix(   (0.5*( mean(slope_29(ii-40+22:ii-40+25)) + mean(slope_29(ii-40-25:ii-40-22)) )/2).*10e6   )./10e6;
%                 arctan_fsk(ii-28:ii) = arctan_fsk(ii-28:ii) - fix(   ([ii-28:ii].*0.5*( mean(slope_29(ii-40+22:ii-40+25)) + mean(slope_29(ii-40-25:ii-40-22)) )/2).*10e6    )./10e6;%在过零点时修正前29点的相位值，此时最小二乘法无相位突变
                  correction(ii) = correction(ii-1) + convergence_factor*( mean(slope_29(ii-40+22:ii-40+25)) + mean(slope_29(ii-40-25:ii-40-22)) )/2;
                  arctan_fsk(ii-28:ii) = arctan_fsk(ii-28:ii) - [ii-28:ii].*convergence_factor*( mean(slope_29(ii-40+22:ii-40+25)) + mean(slope_29(ii-40-25:ii-40-22)) )/2;%在过零点时修正前29点的相位值，此时最小二乘法无相位突变
                  sync_point(ii-40) = 1;
            else
                correction(ii) =correction(ii-1);
                sync_point(ii-40) = 0;
            end
        elseif((slope_29(ii-40)>=0) && (slope_29(ii-40-1)<=0)  )%- +
            statistic_positive = length(find(slope_29(ii-40:ii-40+15)>0));
            statistic_negative = length(find(slope_29(ii-40-16:ii-40-1)<0));
            if( (statistic_negative>12) && (statistic_positive>12) )%大数判决
                %                 correction(ii) =correction(ii-1) + fix(   (0.5*( mean(slope_29(ii-40+22:ii-40+25)) + mean(slope_29(ii-40-25:ii-40-22)) )/2).*10e6   )./10e6;
                %                 arctan_fsk(ii-28:ii) = arctan_fsk(ii-28:ii) - fix(   ([ii-28:ii].*0.5*( mean(slope_29(ii-40+22:ii-40+25)) + mean(slope_29(ii-40-25:ii-40-22)) )/2).*10e6    )./10e6 ;
                correction(ii) =correction(ii-1) + convergence_factor*( mean(slope_29(ii-40+22:ii-40+25)) + mean(slope_29(ii-40-25:ii-40-22)) )/2;
                arctan_fsk(ii-28:ii) = arctan_fsk(ii-28:ii) - [ii-28:ii].*convergence_factor*( mean(slope_29(ii-40+22:ii-40+25)) + mean(slope_29(ii-40-25:ii-40-22)) )/2;
                sync_point(ii-40) = 1;
            else
                correction(ii) = correction(ii-1);
                sync_point(ii-40) = 0;
            end
        else
            correction(ii) = correction(ii-1);
            sync_point(ii-40) = 0;
        end %if
    else
        correction(ii) = correction(ii-1);
        sync_point(ii-40) = 0;
    end

end

figure(1),
plot(arctan_fsk);
title('相位折线');

figure(2);
plot(slope_29,'r');
hold on;
plot(correction);
plot(sync_point./50,'g');
legend('斜率','修正量','同步点');
grid on;

figure(3);
x_axis = find(sync_point==1);
x = x_axis(2:end) - x_axis(1:end-1);
plot(x);
title('偏移量');
grid on;

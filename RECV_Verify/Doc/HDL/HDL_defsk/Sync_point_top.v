//--------------------------------------------------------------------------------------------------                                                                             
//                                                                                                                                                                              
//--- ALL RIGHTS RESERVED
//--- File      : Sync_point_top.v 
//--- Auther    : 
//--- Date      : 2015.05.22
//--- Version   : v1.3.2.1
//--- Abstract  : 完成对FSK信号的01切换点的计算
//
//-------------------------------------------------------------------------------------------------                                                                             
//                                                                                                                                                                              
// Description : 完成对低通滤波器之后的FSK信号的同步点计算工作，主要分为相位计算、斜率计算、过零点判决三部分                                                                                                                                                                
//                                                                                                                                                                              
//-------------------------------------------------------------------------------------------------                                                                             
//--- Modification History:
//--- Date          By          Version         Change Description 
//--------------------------------------------------------------------------------------------------
//--- 2015.05.22             
//--------------------------------------------------------------------------------------------------
module Sync_point_top(
          i_rst_n,         //输入复位信号，低电平有效
          i_clk_81M,       //输入时钟信号，81M
          i_clk_27M_en,    //输入时钟使能，27M
          i_data,          //输入数据，用来进行希尔伯特滤波

         // o_ADC_noise_flag,//ADC数据为噪声信号指示，用于控制点亮A排灯的条件之一，高电平时为有噪声
          o_sync_point     //同步点
          );

input i_rst_n;
input i_clk_81M;
input i_clk_27M_en;
input [11:0]i_data;

//output o_ADC_noise_flag;
output o_sync_point;

//-------变量声明------------//
wire [13:0]angle_tan;
wire [25:0]slope;

//-------求FSK数据的初始相位------------//
Phase_calculation U_Phase_calculation(
                  .i_rst_n(i_rst_n),              //输入复位信号，低电平有效
                  .i_clk_81M(i_clk_81M),          //输入时钟信号，81M
                  .i_clk_27M_en(i_clk_27M_en),    //输入时钟使能，27M
                  .i_data(i_data),                //输入数据，用来进行希尔伯特滤波
                  
                  //.o_flag_noise(o_ADC_noise_flag),//ADC数据为噪声信号指示，用于控制点亮A排灯的条件之一，高电平时为有噪声           
                  .o_angle_tan(angle_tan)         //相位值（0-2pi）
                  );
                            
//------求相位折线的斜率-----------//                             
Slope_calculation U_Slope_calculation(
          .i_rst_n(i_rst_n),         //输入复位信号，低电平有效
          .i_clk_81M(i_clk_81M),       //输入时钟信号，81M
          .i_clk_27M_en(i_clk_27M_en),    //输入时钟使能，27M
          .i_angle_tan(angle_tan),     //输入原始相位，修正后进行最小二乘法求斜率
          
          .o_slope(slope)          //斜率
          );

//----------求得同步点---------------//                                                                  
ZCP_judgment U_ZCP_judgment(
          .i_rst_n(i_rst_n),         //输入复位信号，低电平有效
          .i_clk_81M(i_clk_81M),       //输入时钟信号，81M
          .i_clk_27M_en(i_clk_27M_en),    //输入时钟使能，27M
          .i_slope(slope),         //输入数据，用来进行过零点判决

          .o_sync_point(o_sync_point)     //同步点
          );

endmodule
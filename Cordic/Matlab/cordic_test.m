x = 10;
y = 20;

x_n = zeros(12,1);
y_n = zeros(12,1);

% 原始数据
fprintf('original is \t %f, %f \n',x,y);
%% 第一步数据
i = 1;
[x_n(i),y_n(i)] = cordic_atan(x,y,pi/(bitshift(2,i)));      % pi/bitshift(2,1),等效于pi/4
fprintf('shift %d result is \t %f, %f \n',i,x_n(i),y_n(i));
%% 进入迭代
N = 6;      % 迭代次数
for i=2:N
   if(y_n(i-1) > 0)
        [x_n(i),y_n(i)] = cordic_atan(x_n(i-1),y_n(i-1),pi/(bitshift(2,i)));
   else
        [x_n(i),y_n(i)] = cordic_atan(x_n(i-1),y_n(i-1),-pi/(bitshift(2,i)));
   end
   fprintf('shift %d result is \t %f, %f \n',i,x_n(i),y_n(i));
end

%%
plot(x_n,y_n);

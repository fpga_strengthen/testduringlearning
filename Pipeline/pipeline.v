module pipeline(
    input   wire        clk,
    input   wire [7:0]  i_X,

    output  reg  [7:0]  Times_P
);
    reg [7:0]   Times_x1,Times_x2;
    reg [7:0]   X1,X2;

    always@(posedge clk)begin 
        //Pipeline state1
        X1 <= i_X;
        Times_x1 <= i_X;
        //Pipeline stage2
        X2 <= X1;
        Times_x2 <= Times_x1 + X1;
        //Pipeline stage3
        Times_P <= Times_x2 + X2;
    end

endmodule
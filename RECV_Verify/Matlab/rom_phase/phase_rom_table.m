% 数据宽度
N = 9;
% 角度值的步进宽度
theta_step = (pi/4)/(2^N);
% 角度值，范围0~pi/4
theta = 0:(pi/4)/(2^N):pi/4;
% 将角度值定点化,10位有效位
theta_dp = fix(theta * (2^N) );

% 文件写入
fid = fopen('rom_tan.mif','w+');
fprintf(fid,'%s\n','WIDTH=10;');
fprintf(fid,'%s\n','DEPTH=512;');

fprintf(fid,'\n%s\n','ADDRESS_RADIX=UNS;');
fprintf(fid,'\n%s\n','DATA_RADIX=UNS;');
fprintf(fid,'\n');
fprintf(fid,'%s\n','CONTENT BEGIN');
for i=1:2^N
    fprintf(fid,'\t\t\t %d%s%d %s \n',i-1,':',theta_dp(i),';');
end
fprintf(fid,'%s','END;');
fclose(fid);
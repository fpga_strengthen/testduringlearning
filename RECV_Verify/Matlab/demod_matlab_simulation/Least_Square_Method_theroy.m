

function [ numerator_theroy,slope_forward_theroy,intersection_slop_decisioned] = Least_Square_Method_theroy( arctan_fsk )
%   理论上的最小二乘法曲线拟合
%   输入为修正后的相位arctan_fsk
%   输出为理论上交点横坐标的分子numerator_theroy，分母numerator_theroy，和交点横坐标intersection
%%

N = 8;%做多少点的曲线拟合
slope_forward_theroy = zeros(1,length(arctan_fsk));  %前n点斜率
slope_after_theroy = zeros(1,length(arctan_fsk)); %后n点斜率
intercept_forward_theroy = zeros(1,length(arctan_fsk));  %前n点拟合直线截距
intercept_after_theroy = zeros(1,length(arctan_fsk));  %后n点拟合直线截距
intersection = zeros(1,length(arctan_fsk));  %交点坐标

tmp_data = toeplitz(arctan_fsk(1:N),arctan_fsk);
for ii= 1:size(tmp_data,2)-N
    p = polyfit([1:size(tmp_data,1)]',tmp_data(:,ii),1);%前N点做最小二乘法
    slope_forward_theroy(ii) = p(1);%斜率
    intercept_forward_theroy(ii) = p(2);%截距
    q = polyfit([1:size(tmp_data,1)]',tmp_data(:,ii+N),1);%后N点做最小二乘法
    slope_after_theroy(ii) = q(1);%斜率
    intercept_after_theroy(ii) = q(2);%截距
%交点横坐标的分子
    %numerator_theroy(ii) = abs(intercept_after_theroy(ii) - intercept_forward_theroy(ii));
    numerator_theroy(ii) = intercept_after_theroy(ii) - intercept_forward_theroy(ii);
    %交点横坐标的分母
    %denominator_theroy(ii) = abs(slope_forward_theroy(ii) - slope_after_theroy(ii) + eps);
    denominator_theroy(ii) = slope_forward_theroy(ii) - slope_after_theroy(ii) + eps;
    %交点横坐标
    %intersection(ii) = abs((intercept_after_theroy(ii) - intercept_forward_theroy(ii ))/(slope_forward_theroy(ii) - slope_after_theroy(ii) + eps)); %求出交点坐标
    intersection(ii) = (intercept_after_theroy(ii) - intercept_forward_theroy(ii ))/(slope_forward_theroy(ii) - slope_after_theroy(ii) + eps); %求出交点坐标
    
    %% 交点判决
    if(intersection(ii) > 1.5 | intersection(ii) < -1.5)
        intersection_decisioned(ii) = 0;
    elseif((intersection(ii)<1.5) & (intersection(ii)>=0) )
        intersection_decisioned(ii) = 1;
    elseif((intersection(ii)<0) & (intersection(ii)>-1.5) )
        intersection_decisioned(ii) = -1;
    else
        intersection_decisioned(ii) = 0;
    end %if
    
    %% 拐点判决
    if(abs(intersection(ii)) > 1.5)
        intersection_slop_decisioned(ii) = 0;
    else
        if( ( (slope_after_theroy(ii) > 0.02689) & (slope_after_theroy(ii) <0.11486) ) & ( (slope_forward_theroy(ii) > -0.11486) & (slope_forward_theroy(ii) < -0.02689) ))
            intersection_slop_decisioned(ii) = 2;
        elseif( (slope_forward_theroy(ii) > 0.02689) & (slope_forward_theroy(ii) <0.11486) & (slope_after_theroy(ii) > -0.11486) & (slope_after_theroy(ii) < -0.02689))
            intersection_slop_decisioned(ii) = 2;
        else
            intersection_slop_decisioned(ii) = 0;
        end
    end %if
    
    a(ii) =0.02689;
    b(ii) =-0.02689;
    c(ii) =-0.1148;
    d(ii) =0.1148;
    
end %for
figure(121),
plot(intersection_slop_decisioned,'g');
hold on,
plot(slope_after_theroy,'r');
hold on,
plot(slope_forward_theroy,'m');
hold on,
plot(a,'k');
hold on,
plot(b,'k');
hold on,
plot(c,'k');
hold on,
plot(d,'k');
hold on,
plot(abs(intersection)<1.5,'c');
end %function




function [ slope_forward_theroy,intersection_slop_decisioned] = Least_Square_Method_theroy_1( arctan_fsk )
%   理论上的最小二乘法曲线拟合
%   输入为修正后的相位arctan_fsk
%   输出为理论上交点横坐标的分子numerator_theroy，分母numerator_theroy，和交点横坐标intersection
%%
N = 8;%做多少点的曲线拟合
slope_forward_theroy = zeros(1,length(arctan_fsk));  %前n点斜率
slope_after_theroy = zeros(1,length(arctan_fsk)); %后n点斜率
intercept_forward_theroy = zeros(1,length(arctan_fsk));  %前n点拟合直线截距
intercept_after_theroy = zeros(1,length(arctan_fsk));  %后n点拟合直线截距
intersection = zeros(1,length(arctan_fsk));  %交点坐标
intersection_slop_decisioned = zeros(1,length(arctan_fsk));  

for ii= 2:length(arctan_fsk)-7-N
    %% 求相位折线前向与后向的斜率和截距
    p = polyfit([1:8],fliplr(arctan_fsk(ii:ii+7)),1);%前N点做最小二乘法
    slope_forward_theroy(ii) = -p(1);%斜率
    intercept_forward_theroy(ii) = p(2);%截距
    q = polyfit([1:8],arctan_fsk(ii+N:ii+7+N),1);%后N点做最小二乘法
    slope_after_theroy(ii) = q(1);%斜率
    intercept_after_theroy(ii) = q(2);%截距
    
    %交点横坐标的分子
    numerator_theroy(ii) = intercept_after_theroy(ii) - intercept_forward_theroy(ii);
    %交点横坐标的分母
    denominator_theroy(ii) = slope_forward_theroy(ii) - slope_after_theroy(ii) + eps;
    %交点横坐标
    intersection(ii) = (intercept_after_theroy(ii) - intercept_forward_theroy(ii ))/(slope_forward_theroy(ii) - slope_after_theroy(ii) + eps) ; %求出交点坐标
    
    %% 交点判决
    if(intersection(ii) > 1.5 || intersection(ii) < -1.5)
        intersection_decisioned(ii) = 0;
    elseif((intersection(ii)<1.5) && (intersection(ii)>=0) )
        intersection_decisioned(ii) = 1;
    elseif((intersection(ii)<0) && (intersection(ii)>-1.5) )
        intersection_decisioned(ii) = -1;
    else
        intersection_decisioned(ii) = 0;
    end %if
    
    %% 拐点判决
    % 3.951 + 7% ：4.0MHz --->  (4.0-4.234)/27.095*2*pi=
    % [0.02689，0.11486]
    
    if(abs(intersection(ii)) < 1.5)
        if((slope_after_theroy(ii) > 0.019 && slope_after_theroy(ii) <0.11177) && (slope_forward_theroy(ii) > -0.11177 && slope_forward_theroy(ii) < -0.019))
            intersection_slop_decisioned(ii) = 2;
        elseif((slope_forward_theroy(ii) > 0.019 && slope_forward_theroy(ii) <0.11177) && (slope_after_theroy(ii) > -0.11177 && slope_after_theroy(ii) < -0.019))
            intersection_slop_decisioned(ii) = 0;
        else
            intersection_slop_decisioned(ii) = intersection_slop_decisioned(ii-1);
        end
    else
        intersection_slop_decisioned(ii) = intersection_slop_decisioned(ii-1);
    end %if
    a(ii) =0.019;
    b(ii) =-0.019;
    c(ii) =-0.11177;
    d(ii) =0.11177;
    
end %for
intersection_slop_decisioned = xor( intersection_slop_decisioned(1:end-1),intersection_slop_decisioned(2:end) );
figure(13),
hold off,
plot(intersection_slop_decisioned*50,'g');
hold on,
plot(slope_after_theroy*50,'r');
plot(arctan_fsk,'m');

plot(a*50,'k');

plot(b*50,'k');

plot(c*50,'k');

plot(d*50,'k');

%plot(abs(intersection)<1.5,'c');
end %function





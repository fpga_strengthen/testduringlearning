close all;
clear;
clc;
% --------------------------------------------------------
%将ZCP_detection_for_syncpoint.m转变为verilog相似的语句
% %对相位折线进行29点最小二乘法，求斜率曲线，根据其过零点修正中心频率，进而求同步点
% 
% %%%%%产生相位连续的FSK调制信号进行ADC采样后作为输入%%%%%%%%%%%%%
% sample_rate = 27.095;%采样速率，单位MHz
% data_bit = 100;
% % src_data([1:2:data_bit-1])=1;
% % src_data([2:2:data_bit])=0;
% src_data = [ 1 0 1 0 1 0 1 0 1 1 1 1 1 0 0 1 1 1 1 0 1 1 1 1 1 0 0 0 0 0 1 1 0 0 1 1 1 1 1 1 1 1 0 0 0 0 0  1 1 1 1 1 1 0 0 1 1 1 1 1 1 1 1 0 0 0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 1 0 0   ];
% BPS_data = 0.56448 ;  %数据传输速率，单位MHz
% freq_0 = 4.234 - 0.282   ; %
% freq_1 = 4.234 + 0.282   ; %比特0和比特1的频点，单位MHz
% i_data_ADC_0  = FSK_mod(src_data,freq_0,freq_1,sample_rate,BPS_data)-2048;
% % 频偏变化
% freq_0 = 4.234 - 0.282  + 0.175 ;
% freq_1 = 4.234 + 0.282  + 0.17 ; %比特0和比特1的频点，单位MHz
% i_data_ADC_1  = FSK_mod(src_data,freq_0,freq_1,sample_rate,BPS_data)-2048;
% % 频偏变化
% freq_0 = 4.234 - 0.282  -0.16 ;
% freq_1 = 4.234 + 0.282  -0.156 ; %比特0和比特1的频点，单位MHz
% i_data_ADC_2  = FSK_mod(src_data,freq_0,freq_1,sample_rate,BPS_data)-2048;
% 
% i_data_ADC = [i_data_ADC_0,i_data_ADC_1,i_data_ADC_2];

% % %%%%%%%%%%%%%%%%%%%%带内干扰%%%%%%%%%%%%%%%%%%%%%
% src_data_noise = [1,1,1,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
% f1_noise = 4.234 - 0.01 ;
% f0_noise = 4.234 + 0.15 ;
% FSK_data_noise  = FSK_mod(src_data_noise,f0_noise,f1_noise,sample_rate,BPS_data)./7;
% FSK_data_noise = FSK_data_noise(1:end);
% i_data_ADC (475:475+length(FSK_data_noise)-1) = i_data_ADC (475:475+length(FSK_data_noise)-1)+ FSK_data_noise;
% i_data_ADC (2000:2000+length(FSK_data_noise)-1) = i_data_ADC (2000:2000+length(FSK_data_noise)-1)+ FSK_data_noise;
% % %%%%%%%%%%%%%%%%%%%%加入数字滤波器产生相移%%%%%%%%%%%%%%%%%%%%%
% Hd = LP_8M_phase10degree;
% i_data_ADC = filter(Hd,i_data_ADC);


% %%%%%%%%%%%%%%%%%%%%%%读取ADC采样报文作为输入%%%%%%%%%%%%%%%%%%%%%
% ADC_data_hex = textread('C:\Users\dn\Desktop\BTM_matlab\lixiao\24点求和抵消算法求同步点matlab仿真\思诺应答器采集数据\思诺应答器丢数据ADC采样数据.txt','%s');
% i_data_ADC   = hex2dec(ADC_data_hex)' -2048;

% %%%%%%%%%%%%%%%%%%%%%%读取ADC采样报文作为输入%%%%%%%%%%%%%%%%%%%%%
% ADC_data_hex = textread('C:\Users\dn\Desktop\BTM_matlab\lixiao\不同算法求同步点matlab仿真\西安丢报文采集数据\3c25_signaltap_data\stp3_auto_signaltap_3.txt','%s');
% i_data_ADC   = hex2dec(ADC_data_hex)' - 2048;

% %%%%%%%%%%%%%%%%%%%%%%读取ADC采样报文作为输入%%%%%%%%%%%%%%%%%%%%%
% ADC_data_hex = textread('C:\Users\dn\Desktop\接收板抓取数据\4.49Ｍ频偏\stp10_auto_signaltap_0.txt','%s');
% i_data_ADC   = hex2dec(ADC_data_hex)' - 2048;

% %%%%%%%%%%%%%%%%%%%%%%读取ADC采样报文作为输入%%%%%%%%%%%%%%%%%%%%%
% ADC_data_hex = textread('C:\Users\lenovo\Desktop\jieshou\不丢.txt','%s');
% i_data_ADC   = hex2dec(ADC_data_hex)' - 2048;

% %%%%%%%%%%%%%%%%%%%%%%读取ADC采样报文作为输入10进制%%%%%%%%%%%%%%%%%%%%%
ADC_data_hex = textread('D:\lab_project\BTM\测试\单通道测试文档\1123加高斯噪声测试报文\27M\九阶滤波器加高斯-2dBm临界_adc.txt','%s');
i_data_ADC =  char(ADC_data_hex) ;
i_data_ADC = str2num(i_data_ADC)'- 2048;

%% 转换为对应于verilog的算法
i_data_ADC = FixPointNum(i_data_ADC, 12, 0);%字长12bit，精度0位小数  (13000:16300)

%% ------------------相干解调--------------------------------%%
[demodulation_result] = demodulation(i_data_ADC)
demodulation_result = [zeros(1,9),demodulation_result];
%% ------------------计算同步点-------------------------------%%
% Hilbert变换 并将数据对齐 
im_datain = hilbert_trans(i_data_ADC);
Length_data = length(im_datain);
im_datain = im_datain(11:Length_data-1);
i_data_ADC = i_data_ADC(6:Length_data-6);

figure(1),
subplot(2,1,1);
plot(i_data_ADC);
hold on,
plot(im_datain,'r');
legend('实部','虚部');

% 相位归一化后求arctan值
angle_tan = Phase_normalization_to_Calculate_phase( i_data_ADC , im_datain );

convergence_factor = 0.5;    %收敛因子
phase_sum = 0;               %相位累计修正量
correction = 0;              %修正量的初始值
angle_fit_reg = zeros(1,29); %最小二乘法移位寄存器
slope_reg = zeros(1,51);     %斜率移位寄存器
slope_for_sync = zeros(1,2); %斜率和中心频率偏移处理后寄存，用于判断同步点
slope_average= zeros(4,length(angle_tan));

for ii = 2:length(angle_tan)
    %% 输入数据
    angle_tan_temp = angle_tan(ii);
    angle_tan_delay = angle_tan(ii-1);
    %% 对2pi，中心频率进行修正,对频偏进行修正 并对连续的1或0的相位累积值进行上溢处理
    % 从第300个点开始做修正，保证开始修正时，斜率曲线是平稳的
    if(phase_sum<2*8192) % 相位累积没有超过8*2pi
        if( angle_tan_temp + 4096 <= angle_tan_delay ) % 过2pi时做2*pi模糊度处理 以及中心频率的修正
            phase_sum = phase_sum + 8192 -1280;
        else
            phase_sum = phase_sum - 1280;
        end
    else% 相位累积超过8*2pi 上溢处理(phase_sum>=8*8192)
        angle_fit_reg = angle_fit_reg - phase_sum ;% 将移位寄存器的相位值归为没有叠加任何值的原始相位
        if( angle_tan_temp+4096<=angle_tan_delay )
            phase_sum = 8192 -1280; % 过2*pi处，相位直接下拉到2*pi-中心频率
        else
            phase_sum =  0 - 1280;  % 非2*pi处，相位直接下拉到0-中心频率
        end
    end
    angle_fit = angle_tan_temp + phase_sum ;%将累积量叠加到寄存器中的原始相位上
    angle_fit_plot(ii) = angle_fit;
    
    %% 将修改相位后的数存入移位寄存器 29位移位寄存器，用于最小二乘法计算
    angle_fit_reg = [angle_fit_reg(2:end),angle_fit];
    
    %% -----------------最小二乘法---------------------------------------
    %取N=14个点用于之后进行29点最小二乘法
    %按照最小二乘法公式先进行对应位相乘
    slope_coefficient = [-14:1:14];
    slope =  angle_fit_reg*slope_coefficient';%1/2030*
    slope_plot(ii) = slope;%绘图用
    slope_reg = [slope_reg(2:end),slope];%斜率移位寄存器，寄存51个点的斜率值。用于中心频率修正，同步点计算
    
    %% ---------------------中心频率修正--------------------------------------
    % 取平均,求w0，w1
    % 寄存的51个点的斜率值中，第26点用来判断是否过0。第26点过0时，根据斜率寄存器第1到3，第49到51点可以比较准确地估计w0，w1的值
    % 1:8:估计水平斜率；9：38：估计过零点，26点为过零点；39：46估计水平斜率
    slope_average_correction_forward = mean(slope_reg(44:51));
    slope_average_correction_after = mean(slope_reg(1:8));
    mid_freq = ( slope_average_correction_after + slope_average_correction_forward )/2;
  
    % 根据估算的w0和w1的值求中心频率偏移量，用来后续同步点的判决
    if( (slope_reg(26)<=0) && (slope_reg(27)>=0)  )  % + -  过零判决
        slope_correction = mid_freq;
    elseif( slope_reg(26)>=0) && (slope_reg(27)<=0 ) % - +  过零判决
        slope_correction = mid_freq;
    else
        slope_correction = slope_correction;
    end %if
    
    slope_for_sync = [slope_for_sync(2),slope_reg(26)-slope_correction];%将修正后的斜率移入移位寄存器
    slope_for_sync_29_plot(ii) = slope_reg(26)-slope_correction;%绘图用
    slope_for_sync_29_before_crct(ii) = slope_reg(26);%绘图用
    
    % 判决是否是拐点
    if( (slope_for_sync(1)<=0) && (slope_for_sync(2)>=0)  )  % + -  过零判决
        sync_point(ii) = 1;%判决的拐点
    elseif( (slope_for_sync(1)>=0) && (slope_for_sync(2)<=0)  ) % - +  过零判决
        sync_point(ii) = 1;%判决的拐点
    else
        sync_point(ii) = 0;
    end %if
    
end %for ii
[slope_fifo,angle_fit_fifo] = slope_fifo(angle_tan);
figure(101);
plot(slope_for_sync_29_plot,'b');
hold on;
plot(slope_fifo,'r');

% 画出相位折线
figure(1),
subplot(2,1,2);
plot(angle_fit_plot);
hold on;
plot(angle_fit_fifo+1,'r');
title('相位折线');
grid on;

% 画出29点最小二乘法求出的斜率曲线以及拐点
figure(2),
subplot(2,1,1);
hold on;
plot(sync_point*200,'r');
plot(slope_for_sync_29_plot,'b');
plot(slope_for_sync_29_before_crct,'k');
% plot(slope_plot,'g');
legend('同步点','修正后的斜率曲线','修正前的斜率曲线');
title('过零点判决得到的同步结果');
grid on;

%-----------------------------对比实验室现用算法----------------------------------
%对中心频率和2pi进行修正
angle_fit_lab = Phase_Correction(angle_tan);
%最小二乘法
[sync_p_8,slope_forward_lab] =  Least_Square_Method( angle_fit_lab );

% 画出8点最小二乘法求出的斜率曲线以及拐点
figure(2),
subplot(2,1,2);
hold on;
plot(sync_p_8*10000,'r');        %拐点
plot(slope_forward_lab(8:end),'b');%斜率
legend('同步点','前8点的斜率曲线');
title('实验室现用求拐点算法得到的同步结果');
grid on;

%% 29点和8点求出的同步点对比，并用29点的同步点修正8点的同步点
sync_point_temp = sync_point(32:end); %截短29点同步点，用于修正8点的同步点
sync_point_reg_16_for_8 = zeros(1,17);

for i = 1:length(sync_point_temp)-8
    sync_point_reg_16_for_8 = [sync_point_reg_16_for_8(2:end),sync_p_8(i+8)];
    sync_point_reg_1_for_29 = sync_point_temp(i);
    
    if(  length(find( sync_point_reg_16_for_8==1 ))>0  &&  sync_point_reg_1_for_29 == 1 )
        sync_point_change_result(i) = sync_p_8(i);
    elseif(length(find( sync_point_reg_16_for_8==1 ))==0  &&  sync_point_reg_1_for_29 == 1 )
        sync_point_change_result(i) = 1;
    else
        sync_point_change_result(i) = sync_p_8(i);
    end
end
figure(6);
plot(sync_p_8);
hold on,
plot(sync_point_temp*1.3,'r');
plot(sync_point_change_result*1.1,'k')
legend('8点同步点','29点同步点','修正后同步点');
title('同步点');
%-----------------------------统计同步点的偏差量----------------------------------
edge_position = find(sync_point == 1);%极值点的位置
edge_variance = mod(edge_position(2:length(edge_position)) - edge_position(1:length(edge_position)-1),48);%两极值点之间的间隔对48点取模，得到偏差量
x_axis = [-4:4];
y_axis = zeros(1,9);
y_axis(1) = length (find((edge_variance>=4)&(edge_variance<=23)));%多>3个点
y_axis(2) = length (find(edge_variance==3));%多3个点
y_axis(3) = length (find(edge_variance==2));%多2个点
y_axis(4) = length (find(edge_variance==1));%多1个点
y_axis(5) = length (find(edge_variance==0));%无偏差
y_axis(6) = length (find(edge_variance==47));%少1个点
y_axis(7) = length (find(edge_variance==46));%少2个点
y_axis(8) = length (find(edge_variance==45));%少3个点
y_axis(9) = length (find((edge_variance>=24)&(edge_variance<=44)));%少>4个点
y_axis = y_axis./sum(y_axis);

figure(3),
subplot(2,1,1);
bar(x_axis,y_axis);
xlabel('偏差量');
ylabel('统计概率值');
set(gca,'xtick',-23:1:24);
title(strcat('过零点判决求同步点的统计偏移量'));

edge_position = find(sync_p_8 == 1);%极值点的位置
edge_variance = mod(edge_position(2:length(edge_position)) - edge_position(1:length(edge_position)-1),48);%两极值点之间的间隔对48点取模，得到偏差量
x_axis = [-4:4];
y_axis = zeros(1,9);
y_axis(1) = length (find((edge_variance>=4)&(edge_variance<=23)));%多>3个点
y_axis(2) = length (find(edge_variance==3));%多3个点
y_axis(3) = length (find(edge_variance==2));%多2个点
y_axis(4) = length (find(edge_variance==1));%多1个点
y_axis(5) = length (find(edge_variance==0));%无偏差
y_axis(6) = length (find(edge_variance==47));%少1个点
y_axis(7) = length (find(edge_variance==46));%少2个点
y_axis(8) = length (find(edge_variance==45));%少3个点
y_axis(9) = length (find((edge_variance>=24)&(edge_variance<=44)));%少>4个点
y_axis = y_axis./sum(y_axis);

figure(3),
subplot(2,1,2);
bar(x_axis,y_axis);
xlabel('偏差量');
ylabel('统计概率值');
set(gca,'xtick',-23:1:24);
title(strcat('实验室求拐点算法的同步点统计偏移量'));

%% -----------------------------将同步点导入到同步算法中得到判决时刻----------------------------------
% 29点最小二乘法的判决时刻
[decision_time_29,sync_plot] =  SyncFsk( sync_point );

% 8点最小二乘法的判决时刻
[decision_time_8] =  SyncFsk( sync_p_8 );% 8点最小二乘法判决时刻

% 29点同步点修正8点同步点后，作出的判决时刻
[decision_time_8_corrected] =  SyncFsk( sync_point_change_result );

% 观察判决时刻和斜率曲线对应关系
figure(4);
plot(decision_time_29(1:end)*2,'r');%*3*10^9
hold on;
plot(demodulation_result,'LineWidth',2);
plot(decision_time_8(15:end)*1.5,'g');%*1.5*10^9
plot(decision_time_8_corrected(15:end),'m','LineWidth',2);
plot(sync_plot*1.1,'k','LineWidth',2);
for i=1:length(slope_for_sync_29_plot)
    if(slope_for_sync_29_plot(i)>0)
        demodulation_result_29(i) = 1;
    else
        demodulation_result_29(i) = 0;
    end
end
plot(~demodulation_result_29(49:end),'r','LineWidth',2);

legend('29点判决时刻','相干解调结果','8点判决时刻','修正后判决时刻','斜率做解调得出的解调结果');
title('判决时刻和斜率曲线对应关系');

% decision_result = 0;
% for i = 2:length(decision_time_29)
%     if(decision_time_29(i)==1)
%         decision_result(i) = demodulation_result(i);
%     else
%         decision_result(i) = decision_result(i-1);
%     end%if
% end%for
% figure(5),
% plot(decision_result);
% title('判决结果');
//============================================================
//
//--- ALL RIGHTS RESERVED
//--- File		: mcp_top.v
//--- Auther	: smartbai
//--- Data		: 2020-11-18
//--- Version	: v1.0.0.0
//--- Abstract	: 
//
//------------------------------------------------------------
//Description	:
//------------------------------------------------------------
//--- Modification History:
//--- Date		By		Version		Change Description
//------------------------------------------------------------
//
//============================================================

module mcp_top (
    i_clk,          //27MHz
    i_clk_81M,
    i_rst_n,

    i_ant_err,      //天线正常
    i_ant_nor,      //天线异常
    i_safe_enable,  //保密芯片是否工作正常，1表示正常，0表示异常

    //MSG RX
    i_msg_end,
    i_msg_en,
    iv_msg_data,

    ov_frame_id,
    //CAN RX
    i_can_wren,
    i_can_wrclk,
    i_can_reFrameEnd,
    iv_can_wrdata,
    //CAN TX
    i_tx_byte_ok,
    i_frame_ok,
    o_tx_byte_en,
    ov_tx_byte
);

    input i_clk;          //27MHz
    input i_clk_81M;
    input i_rst_n;

    input i_ant_err;      //天线正常
    input i_ant_nor;      //天线异常
    input i_safe_enable;  //保密芯片是否工作正常，1表示正常，0表示异常

    //MSG RX
    input i_msg_end;
    input i_msg_en;
    input [7:0] iv_msg_data;

    output [6:0] ov_frame_id;
    //CAN RX
    input i_can_wren;
    input i_can_wrclk;
    input i_can_reFrameEnd;
    input [7:0] iv_can_wrdata;
    //CAN TX
    input i_tx_byte_ok;
    input i_frame_ok;
    output o_tx_byte_en;
    output [7:0] ov_tx_byte;

//-------------------------------------------------------------------//
//请求发送信号
wire tx_type_en;
wire tx_status_en;
wire tx_msg_req;

//允许发送信号
wire frame_type_en;
wire frame_msg_en;

//发送完成信号
wire frame_type_ok;
wire frame_msg_ok;

wire tx_type_byte_en;
wire [7:0] tx_type_byte;
wire tx_msg_byte_en;
wire [7:0] tx_msg_byte;

//-------------------------------------------------------------------//
//can 发送控制
mcp_ctr  u_mcp_ctr (
    .i_clk                   ( i_clk            ),
    .i_rst_n                 ( i_rst_n          ),
    .i_tx_type_en            ( tx_type_en       ),
    .i_tx_status_en          ( tx_status_en     ),
    .i_tx_msg_en             ( tx_msg_req       ),

    .i_frame_type_ok         ( frame_type_ok    ),
    .i_frame_msg_ok          ( frame_msg_ok     ),
    .o_frame_type_en         ( frame_type_en    ),
    .o_frame_msg_en          ( frame_msg_en     )
);

//can 接收数据判断
mcp_re  u_mcp_re (
    .i_clk                   ( i_clk               ),
    .i_rst_n                 ( i_rst_n             ),
    .i_can_wren              ( i_can_wren          ),
    .i_can_wrclk             ( i_can_wrclk         ),
    .i_can_reFrameEnd        ( i_can_reFrameEnd    ),
    .iv_can_wrdata           ( iv_can_wrdata       ),

    .o_tx_type_en            ( tx_type_en          )
);

//can 发送状态信息
// mcp_tx_status_cnt3s.v  在不发报文时3s触发一次发送状态信息
mcp_tx_status_cnt3s  u_mcp_tx_status_cnt3s (
    .i_clk                   ( i_clk            ),
    .i_rst_n                 ( i_rst_n          ),
    .i_msg_framing           ( 1'b0    ),
    .i_ant_err               ( i_ant_err    ),
    .i_ant_nor               ( i_ant_nor    ),
    .o_tx_status_en          ( tx_status_en   )
);

wire [6:0] frame_id;
assign o_tx_byte_en = (frame_type_en & tx_type_byte_en) | (frame_msg_en & tx_msg_byte_en);
assign ov_tx_byte = frame_type_en ? tx_type_byte : (frame_msg_en ? tx_msg_byte : 8'd0);
assign ov_frame_id = frame_type_en ? 7'd0 : frame_id;

//can 发送版本号
mcp_tx_type  u_mcp_tx_type (
    .i_clk                   ( i_clk                  ),
    .i_rst_n                 ( i_rst_n                ),
    .i_frame_type_en         ( frame_type_en          ),
    .i_ant_err               ( i_ant_err              ),
    .i_ant_nor               ( i_ant_nor              ),
    .i_safe_enable           ( i_safe_enable          ),
    .i_byte_ok               ( i_tx_byte_ok           ),

    .o_byte_en               ( tx_type_byte_en        ),
    .ov_byte                 ( tx_type_byte           ),
    .o_frame_type_ok         ( frame_type_ok          )
);

//can 发送报文信息
mcp_tx_msg  u_mcp_tx_msg (
    .i_clk                   ( i_clk                 ),
    .i_clk_81M               ( i_clk_81M             ),
    .i_rst_n                 ( i_rst_n               ),
    .i_msg_end               ( i_msg_end             ),
    .i_msg_en                ( i_msg_en              ),
    .iv_msg_data             ( iv_msg_data           ),
    .i_frame_msg_en          ( frame_msg_en          ),
    .i_byte_ok               ( i_tx_byte_ok          ),
    .i_frame_ok              ( i_frame_ok            ),

    .o_byte_en               ( tx_msg_byte_en        ),
    .ov_byte                 ( tx_msg_byte           ),
    .ov_frame_id             ( frame_id           ),
    .o_tx_msg_req            ( tx_msg_req            ),
    .o_frame_msg_ok          ( frame_msg_ok          )
);

endmodule
//
//-------------------------------------------------------------------------------------------------
//Description:                                                                                                              
//-------------------------------------------------------------------------------------------------                          
//--- Modification History:
//--- Date          By          Version         Change Description 
//-------------------------------------------------------------------
//--- 2011.8.1		Dan Cui     v3.0			?????????Match?????????????????????fsk???????????????????ARM??
//--- 
//--------------------------------------------------------------------
module BtmInnerBus_new(
						clk_27M,	//--27.095M clk 
						rstn,		//--reset
						clk_uart,
						clk_uart_p,	//--clk_uart-->14.7456M/2  uart -->460800bps  clk_uart/uart=16
												
						location_time,	  
						status,				//--16bits status,high bits are  firstly to be sent
						test_wen11_coming_negedge,	
						status_ready,
						defsk_ready,		//--transform_ok      
						defskram_datain,	//--read fsk data input	 ----defsk--->data[7:0] 
						
						defsk_done,			//BtmInnerBusForFsk?????fsk?????????
						defsk_start,
						
						defskram_en,		//--read fsk data by using this enable 
						defskram_clk,		//?transforn???????
						defskram_addr,		//--read fsk data by using this addr  addr[6:0]  from 0---103 
						uart_tout,			//--UART output  
						uart_ctr,
						bus_tx_done_fsk			//--1 to send 0 to receive 
						);
						
input clk_27M;
input rstn;
input clk_uart;
input clk_uart_p;	

input test_wen11_coming_negedge;
input [15:0]location_time;		
input [15:0]status;
input status_ready;
input defsk_ready;	
input [7:0]defskram_datain;
input defsk_done;
input defsk_start;

output [6:0]defskram_addr;
output defskram_en;
output defskram_clk;
output uart_tout;
output uart_ctr;
output bus_tx_done_fsk;

//---------------------------------------------??????----------------------------------------------------//
reg uart_tx_start;
reg [7:0]uart_tx_data;
wire uart_tout_n;
wire uart_tx_done;
UartTx uu_UartTx(
				.CLK(clk_uart),
				.RESETn(rstn),
				.start(uart_tx_start),
				.data(uart_tx_data),
				.TIN(uart_tout_n), 
				.done(uart_tx_done)
				);	
wire uart_tout;				
assign uart_tout = ~uart_tout_n;

//-----------------------------------------uart_tx_done_p--------------------------------------------------------//
reg uart_tx_done_temp1;
reg uart_tx_done_temp2;
always @ ( posedge clk_27M or negedge rstn )
	if( !rstn )
		begin
			uart_tx_done_temp1 <= 1'b0;
			uart_tx_done_temp2 <= 1'b0;
		end
	else
		begin
			uart_tx_done_temp1 <= uart_tx_done;
			uart_tx_done_temp2 <= uart_tx_done_temp1;
		end

wire uart_tx_done_p;
assign uart_tx_done_p = uart_tx_done_temp1 & ( ~uart_tx_done_temp2 );

//-----------------------------------------uart_tx_start_p--------------------------------------------------------//
reg uart_tx_start_temp1;
reg uart_tx_start_temp2;
always @ ( posedge clk_27M or negedge rstn )
	if( !rstn )
		begin
			uart_tx_start_temp1 <= 1'b0;
			uart_tx_start_temp2 <= 1'b0;
		end
	else
		begin
			uart_tx_start_temp1 <= uart_tx_start;
			uart_tx_start_temp2 <= uart_tx_start_temp1;
		end

wire uart_tx_start_p;
assign uart_tx_start_p = uart_tx_start_temp1 & ( ~uart_tx_start_temp2 );


wire defskram_clk;
assign defskram_clk = uart_tx_done_p; //

//--------------------------------------------------defsk_done_p------------------------------------------------//
reg defsk_done_temp1;
reg defsk_done_temp2;
always @ ( posedge clk_27M or negedge rstn )
	if( !rstn )
		begin
			defsk_done_temp1 <= 1'b0;
			defsk_done_temp2 <= 1'b0;
		end
	else
		begin
			defsk_done_temp1 <= defsk_done;
			defsk_done_temp2 <= defsk_done_temp1;
		end

wire defsk_done_p;
assign defsk_done_p = defsk_done_temp1 & ( ~defsk_done_temp2 );
//--------------------------------------------------defsk_start_p------------------------------------------------//
reg defsk_start_temp1;
reg defsk_start_temp2;
always @ ( posedge clk_27M or negedge rstn )
	if( !rstn )
		begin
			defsk_start_temp1 <= 1'b0;
			defsk_start_temp2 <= 1'b0;
		end
	else
		begin
			defsk_start_temp1 <= defsk_start;
			defsk_start_temp2 <= defsk_start_temp1;
		end

wire defsk_start_p;
assign defsk_start_p = defsk_start_temp1 & ( ~defsk_start_temp2 );
			
//---------------------------------------------------sign_fsk_done--------------------------------------------//
reg sign_fsk_done;
always @ ( posedge clk_27M or negedge rstn )
	if( !rstn )
		sign_fsk_done <= 1'b1;
	else if( defsk_start_p )
		sign_fsk_done <= 1'b0;
	else if( defsk_done_p | defsk_ready )
		sign_fsk_done <= 1'b1;
	else 
		sign_fsk_done <= sign_fsk_done ;
		
//---------------------------------------------------------------------------------------------------------------//
//------------------------------------????????-----------------------------------------------------------//
//------------------------------------?????-------------------------------------------------//
//?transform????ram??????????
reg [6:0]defskram_addr;
reg [1:0]cnt_status;
reg [1:0]cnt_location;
reg [1:0]cnt_crc;

reg sign_data;
reg sign_status;
reg sign_location;

reg [3:0]state;
parameter	IDLE		=	4'd0,	//????
				HEAD		=	4'd1,	//??C0?
				KIND		=	4'd2,	//????---??01---??02---??04
				LENGTHH		=	4'd3,	//????????
				LENGTHL		=	4'd4,	//????????
				DATA		=	4'd5,	//????
				STATUS		=	4'd6,	//????
				LOCATION	=	4'd7,	//????
				CRC			=	4'd8,	//??crc??2??
				END			=	4'd9;	//??C0?
			
always @ ( posedge clk_27M or negedge rstn )
	if( !rstn )
		state <= IDLE ;
	else 
		case( state )
			IDLE:
				if( defsk_ready & (~sign_fsk_done) ) //??FSK??????
					state <= HEAD;
				else if( status_ready | test_wen11_coming_negedge )
					state <= HEAD;
				else
					state <= IDLE ;		
			HEAD:
				if( uart_tx_done_p )
					state <= KIND;
				else
					state <= state ;
			KIND:
				if( uart_tx_done_p )
					state <= LENGTHH;
				else
					state <= state ;
			LENGTHH:
				if( uart_tx_done_p )
					state <= LENGTHL;
				else
					state <= state ;
			LENGTHL:
				if( sign_data & uart_tx_done_p )
					state <= DATA;
				else if( sign_status & uart_tx_done_p )
					state <= STATUS ;
				else if( sign_location & uart_tx_done_p )
					state <= LOCATION ;
				else
					state <= state ;
			DATA:
				if( defskram_addr >= 7'd105 & uart_tx_done_p )
					state <= CRC;
				else
					state <= state ;
			STATUS:
				if( cnt_status == 2'd1 & uart_tx_done_p )
					state <= CRC;
				else
					state <= state ;
			LOCATION:
				if( cnt_location == 2'd1 & uart_tx_done_p )
					state <= CRC;
				else
					state <= state ;
			CRC:	
				if( cnt_crc == 2'd1 & uart_tx_done_p )
					state <= END;
				else
					state <= state ;
			END:
				if( uart_tx_done_p )
					state <= IDLE;
				else
					state <= state ;
			default:
				state <= IDLE ;
		endcase
//-----------------------------------------------------------------------------------------------//
//??????????????
//reg sign_data;
//reg sign_status;
//reg sign_location;
always @ ( posedge clk_27M or negedge rstn )
	if( !rstn )
		begin
			sign_data <= 1'b0;
			sign_status <= 1'b0;
			sign_location <= 1'b0;
		end
	else 
		case( state )
			IDLE:
				if( defsk_ready & (~sign_fsk_done) )
					begin
						sign_data <= 1'b1;
						sign_status <= 1'b0;
						sign_location <= 1'b0;
					end
				else if( status_ready )
					begin
						sign_data <= 1'b0;
						sign_status <= 1'b1;
						sign_location <= 1'b0;
					end
				else if( test_wen11_coming_negedge )
					begin
						sign_data <= 1'b0;
						sign_status <= 1'b0;
						sign_location <= 1'b1;
					end
				else
					begin
						sign_data <= sign_data;
						sign_status <= sign_status;
						sign_location <= sign_location;
					end  
			END:
				begin
					sign_data <= 1'b0;
					sign_status <= 1'b0;
					sign_location <= 1'b0;
				end
			default:
				begin
					sign_data <= sign_data;
					sign_status <= sign_status;
					sign_location <= sign_location;
				end  
		endcase				
//-----------------------------------------------------------------------------------------//
//???transform?????????????,??????????
//reg [6:0]defskram_addr;
//reg [1:0]cnt_status;
//reg [1:0]cnt_location;
//reg [1:0]cnt_crc;

reg defskram_en_r;
wire defskram_en;
assign defskram_en = defskram_en_r;

always @ ( posedge clk_27M or negedge rstn )
	if( !rstn )
		begin
			defskram_en_r <= 1'b0 ;
			defskram_addr <= 7'd0;
		end
	else 
		case( state )
			IDLE:
				begin
					defskram_en_r <= 1'b0 ;
					defskram_addr <= 7'd0;
				end
			LENGTHL:
				if( sign_data )		//?????????DATA??????defskram_en?1?????ram????1???
					begin
						defskram_en_r <= 1'b1 ;	
						defskram_addr <= 7'd1;
					end
				else
					begin
						defskram_en_r <= 1'b0 ;	
						defskram_addr <= 7'd0;
					end
			DATA:
				if( uart_tx_start_p )
					begin
//						uart_tx_data <= defskram_datain;
						defskram_addr <= defskram_addr + 7'd1;
						defskram_en_r <= 1'b1;
					end
				else
					begin
//						uart_tx_data <= defskram_datain;
						defskram_addr <= defskram_addr ;
						defskram_en_r <= 1'b1;
					end
			default:
				begin
					defskram_addr <= 7'd0 ;
					defskram_en_r <= 1'b0;
				end
		endcase
	
//------------------------------????????CRC???---------------------------------------------//
			
always @ ( posedge clk_27M or negedge rstn )
	if( !rstn )
		begin
			cnt_status <= 2'd0;
			cnt_location <= 2'd0;
			cnt_crc <= 2'd0;
		end
	else 
		case( state )
			IDLE:
				begin
					cnt_status <= 2'd0;
					cnt_location <= 2'd0;
					cnt_crc <= 2'd0;
				end
			STATUS:
				if( uart_tx_done_p )
					begin
						cnt_status <= cnt_status + 2'd1;
						cnt_location <= cnt_location;
						cnt_crc <= cnt_crc;
					end
				else 
					begin
						cnt_status <= cnt_status;
						cnt_location <= cnt_location;
						cnt_crc <= cnt_crc;
					end
			LOCATION:
				if( uart_tx_done_p )
					begin
						cnt_status <= cnt_status ;
						cnt_location <= cnt_location + 2'd1;
						cnt_crc <= cnt_crc;
					end
				else 
					begin
						cnt_status <= cnt_status;
						cnt_location <= cnt_location;
						cnt_crc <= cnt_crc;
					end
			CRC:	
				if( uart_tx_done_p )
					begin
						cnt_status <= cnt_status ;
						cnt_location <= cnt_location ;
						cnt_crc <= cnt_crc + 2'd1;
					end
				else 
					begin
						cnt_status <= cnt_status;
						cnt_location <= cnt_location;
						cnt_crc <= cnt_crc;
					end
			default:
				begin
					cnt_status <= cnt_status;
					cnt_location <= cnt_location;
					cnt_crc <= cnt_crc;
				end
		endcase
		
//--------------------------------------------??????????-------------------------------------//
reg [15:0]checksum_old;
always @ ( posedge clk_27M or negedge rstn )
	if( !rstn )
		uart_tx_data <= 8'd0 ;
	else 
		case( state )
			IDLE:
				uart_tx_data <= 8'h0;
			
			HEAD:
				uart_tx_data <= 8'hc0 ;
			KIND:
				if( sign_data )
					uart_tx_data <= 8'h01 ;
				else if( sign_status )
					uart_tx_data <= 8'h02 ;
				else if( sign_location )
					uart_tx_data <= 8'h04;
				else
					uart_tx_data <= uart_tx_data;
			LENGTHH:
				uart_tx_data <= 8'h0;
			LENGTHL:
				if( sign_data )
					uart_tx_data <= 8'h68 ;
				else if( sign_status )
					uart_tx_data <= 8'h02 ;
				else if( sign_location )
					uart_tx_data <= 8'h02;
				else
					uart_tx_data <= uart_tx_data;
			DATA:
				uart_tx_data <= defskram_datain ;
			STATUS:
				if( cnt_status == 2'd0 )
					uart_tx_data <= status[15:8]; 
				else if ( cnt_status == 2'd1 )
					uart_tx_data <= status[7:0]; 
			LOCATION:
				if( cnt_location == 2'd0 )
					uart_tx_data <= location_time[15:8]; 
				else if ( cnt_location == 2'd1 )
					uart_tx_data <= location_time[7:0]; 
			CRC:	
				if( cnt_crc == 2'd0 )
					uart_tx_data <= checksum_old[15:8]; 
				else if ( cnt_crc == 2'd1 )
					uart_tx_data <= checksum_old[7:0]; 
			END:
				uart_tx_data <= 8'hc0 ;
			default:
				uart_tx_data <= uart_tx_data ;
		endcase

//-------------------------------------------------------------------------------------------------------//
//reg defskram_clk;
//always @ ( posedge clk_27M or negedge rstn )
//	if( !rstn )
//		defskram_clk <= 1'b0 ;
//	else if( state == DATA )
//		defskram_clk = uart_tx_done_p;	
//	else		
//		defskram_clk = 1'b0
//---------------------------------------------??????----------------------------------------------//
//reg uart_tx_start;
always @ ( posedge clk_27M or negedge rstn )
	if( !rstn )
		uart_tx_start <= 1'b0 ;
	else 
		case( state )
			IDLE:
				uart_tx_start <= 1'b0 ;
			default:
				if ( !uart_tx_done ) 
					uart_tx_start <= 1'b0;
				else
					uart_tx_start <= 1'b1 ;
		endcase
//-----------------uart_ctr--1 to send-----------------------------------------------------------------------// 
reg uart_ctr;	
always @ ( negedge rstn or posedge clk_27M )
	if (!rstn)
		uart_ctr <= 1'b0;
	else if ( state != IDLE )
		uart_ctr <= 1'b1;     
	else
		uart_ctr <= 1'b0;		
		
//-----------------bus_tx_done_fsk---------------------- 
reg bus_tx_done_fsk;			
always @ ( negedge rstn or posedge clk_27M )
	if (!rstn)
		bus_tx_done_fsk <= 1'b0;	
	else if ( state == IDLE )
		bus_tx_done_fsk <= 1'b1;
	else if ( state == HEAD && sign_data == 1'b1)
		bus_tx_done_fsk <= 1'b0;
	else
		bus_tx_done_fsk <= bus_tx_done_fsk;
///////////////////////////////////crc/////////////////////////////////////////////
reg [7:0]cnt_uart_tx_start;	
always @ (negedge rstn or posedge clk_27M )
	if (!rstn )
		cnt_uart_tx_start <= 8'b00;
	else if ( uart_tx_start )
		cnt_uart_tx_start <= 8'b00;
	else if( clk_uart_p )
		if (cnt_uart_tx_start == 8'd160)
			cnt_uart_tx_start <= 8'b00;	   
		else
			cnt_uart_tx_start <= cnt_uart_tx_start + 1'b1;
	else
		cnt_uart_tx_start <= cnt_uart_tx_start ;

reg cnt160_uart_tx_start;	   
always @(negedge rstn or posedge clk_27M) 
	if (!rstn)
	   cnt160_uart_tx_start <= 1'b1;
	else if( clk_uart_p ) 
	   if (cnt_uart_tx_start == 8'd79)
		   cnt160_uart_tx_start <= 1'b0;
	   else if (cnt_uart_tx_start == 6'd0)
		   cnt160_uart_tx_start <= 1'b1;
	   else 
		   cnt160_uart_tx_start <= cnt160_uart_tx_start;
	else
		cnt160_uart_tx_start <= cnt160_uart_tx_start;

wire clk_data;
assign clk_data = ((state >= 4'd2) && (state <= 4'd7)) ? cnt160_uart_tx_start : 1'b0;

//-------------------------------------clk_data_p-----------------------------------------------------//
reg clk_data_temp1;
reg clk_data_temp2;
always @(negedge rstn or posedge clk_27M) 
	if (!rstn)
		begin
			clk_data_temp1 <= 1'b0;
			clk_data_temp2 <= 1'b0;
		end
	else
		begin
			clk_data_temp1 <= clk_data;
			clk_data_temp2 <= clk_data_temp1;
		end
wire clk_data_p;
assign clk_data_p = ( clk_data_temp1 ) & ( ~clk_data_temp2 );
//---------------------------------------------------------------------------------------------------------//
//reg [15:0]checksum_old;
always @(negedge rstn or posedge clk_27M or negedge uart_ctr) 
	if (!rstn)	
		checksum_old <= 16'b0;
	else if (!uart_ctr)	
		checksum_old <= 16'b0;				
	else if ( clk_data_p )
		checksum_old <= nextCRC16_D8(uart_tx_data,checksum_old);
	else
		checksum_old <= checksum_old ;
	
function [15:0] nextCRC16_D8;

input [7:0] Data;
input [15:0] crc;
reg [7:0] d;
reg [15:0] c;
reg [15:0] newcrc;
	begin
		d = Data;
		c = crc;

		newcrc[0] = d[7] ^ d[6] ^ d[5] ^ d[4] ^ d[3] ^ d[2] ^ d[1] ^ d[0] ^ c[8] ^ c[9] ^ c[10] ^ c[11] ^ c[12] ^ c[13] ^ c[14] ^ c[15];
		newcrc[1] = d[7] ^ d[6] ^ d[5] ^ d[4] ^ d[3] ^ d[2] ^ d[1] ^ c[9] ^ c[10] ^ c[11] ^ c[12] ^ c[13] ^ c[14] ^ c[15];
		newcrc[2] = d[1] ^ d[0] ^ c[8] ^ c[9];
		newcrc[3] = d[2] ^ d[1] ^ c[9] ^ c[10];
		newcrc[4] = d[3] ^ d[2] ^ c[10] ^ c[11];
		newcrc[5] = d[4] ^ d[3] ^ c[11] ^ c[12];
		newcrc[6] = d[5] ^ d[4] ^ c[12] ^ c[13];
		newcrc[7] = d[6] ^ d[5] ^ c[13] ^ c[14];
		newcrc[8] = d[7] ^ d[6] ^ c[0] ^ c[14] ^ c[15];
		newcrc[9] = d[7] ^ c[1] ^ c[15];
		newcrc[10] = c[2];
		newcrc[11] = c[3];
		newcrc[12] = c[4];
		newcrc[13] = c[5];
		newcrc[14] = c[6];
		newcrc[15] = d[7] ^ d[6] ^ d[5] ^ d[4] ^ d[3] ^ d[2] ^ d[1] ^ d[0] ^ c[7] ^ c[8] ^ c[9] ^ c[10] ^ c[11] ^ c[12] ^ c[13] ^ c[14] ^ c[15];
		nextCRC16_D8 = newcrc;
	end
endfunction

endmodule




	
	
	
	
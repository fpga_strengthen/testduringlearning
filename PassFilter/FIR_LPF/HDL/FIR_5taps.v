`timescale 1ns/1ps
//该滤波器结构为直接型FIR低通滤波器，并做了溢出保护
module FIR_5taps(
    input   wire    clk,
    input   wire    clk_en,         //相当于重采样
    input   wire    rst_x,
    input   wire    signed  [9:0]   filter_in,
    output  reg     signed  [19:0]  filter_out_my,
    output  reg     signed  [19:0]  filter_out_book
);

    //滤波器系数,注意到系数是上下对称的
    parameter signed [9:0] coeff1 = 10'b00011_00110;
    parameter signed [9:0] coeff2 = 10'b00110_01101;
    parameter signed [9:0] coeff3 = 10'b01000_00000;
    parameter signed [9:0] coeff4 = 10'b00110_01101;
    parameter signed [9:0] coeff5 = 10'b00011_00110;

    //内部信号,存储器用来存储延时结果z^(-1)
    //将数据进行缓存，方便与各级系数相乘
    reg     signed  [9:0]   delay_pipeline [0:4];
    wire    signed  [19:0]  sum;            //这里的求和结果未作溢出保护

    //这部分即对应z^N,N=0,-1,-2,-3,-4
    always@(posedge clk or negedge rst_x) begin
        if(!rst_x)begin
            delay_pipeline[0] <= 0;
            delay_pipeline[1] <= 0;
            delay_pipeline[2] <= 0;
            delay_pipeline[3] <= 0;
            delay_pipeline[4] <= 0;
        end
        else if(clk_en == 1'b1)begin
            delay_pipeline[0] <= filter_in;
            delay_pipeline[1] <= delay_pipeline[0];
            delay_pipeline[2] <= delay_pipeline[1];
            delay_pipeline[3] <= delay_pipeline[2];
            delay_pipeline[4] <= delay_pipeline[3];
        end
    end
    
    //解释：数据和系数均为有符号数，各包含一个符号位，相乘后无论正负都会产生2个符号位，但实际只需一个即可，所以需要去除1位符号位
    wire    [19:0]  product5;
    wire    [19:0]  product4;
    wire    [19:0]  product3;
    wire    [19:0]  product2;
    wire    [19:0]  product1;
    
    //系数5与4级缓存相乘，结果包含2个符号位，需要去除1个
    assign  product5 = delay_pipeline[4] * coeff5;
    assign  product4 = delay_pipeline[3] * coeff4;
    //由于第三个系数比较特殊，所以采用相应的计算方法
    assign  product3 = $signed({delay_pipeline[2][9:0],8'b0000_0000});
    assign  product2 = delay_pipeline[1] * coeff2;
    assign  product1 = delay_pipeline[0] * coeff1;

    //多级乘法结果进行累加，需要做溢出保护
    assign  sum = product1 + product2 + product3 + product4 + product5;

//----------------------------以下是考虑的溢出保护措施----------------------------

    //声明一个变量暂存多级乘法结果，5个20位的数据（按照全部同符号且满幅计算）求和结果最多需要扩展4个bit(2^2 < 5 < 2^3)，因此需要24位
    wire    signed      [23:0]  sum_tmp;
    reg     signed      [19:0]  sum_resolved;           //进行溢出处理过的求和结果

    assign  sum_tmp = product1 + product2 + product3 + product4 + product5;

    //溢出标志
    wire    of_flag;

    //判断所有符号位的比特是否相同，做异或运算，如果相同，则结果为零，表示没有因为溢出进位影响符号位；否则就是溢出
    assign  of_flag = ((&sum_tmp[23:22])||(~|sum_tmp[23:22])) ? 1'b0 : 1'b1;

    //溢出判断和处理
    always@(*)begin
        if(of_flag == 1'b1)begin        //有溢出
            if(sum_tmp == 1'b1)         //负数，下溢
                sum_resolved = {1'b1,{19{1'b0}}};
            else                        //正数，上溢
                sum_resolved = {1'b0,{19{1'b1}}};
        end
        else        //无溢出
            sum_resolved = {sum_tmp[23],sum_tmp[18:0]};
    end

    //将输出结果缓存
    always@(posedge clk or negedge rst_x)begin 
        if(rst_x == 1'b0)
            filter_out_my <= 0;
        else if(clk_en == 1'b1)
            filter_out_my <= sum_resolved;
        else
            filter_out_my <= filter_out_my;
    end

//-------------------------书中暂未提供溢出保护:  --------------------
    
    always@(posedge clk or negedge rst_x)begin 
        if(!rst_x)
            filter_out_book <= 0;
        else if(clk_en == 1'b1)
            filter_out_book <= sum[19:0];
    end

endmodule
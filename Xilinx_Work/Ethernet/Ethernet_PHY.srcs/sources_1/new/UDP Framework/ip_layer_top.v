`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date: 2024/08/22 14:34:16
// Module Name: ip_layer_top
// Description: IP层顶层.
// Revision 0.01 - File Created
//////////////////////////////////////////////////////////////////////////////////

module ip_layer_top#(
	parameter       LOCAL_IP_ADDR  = 32'h0,
	parameter       TARGET_IP_ADDR = 32'h0
)(
    // 时钟和复位
    input wire			i_rx_clk,
    input wire			i_tx_clk,
    input wire			i_rx_rst_n,
	input wire			i_tx_rst_n,

	//*********************** ip接收端 *****************************//
	/*---------------- mac_to_arp_ip 模块交互信号--------------------*/
	input wire			i_ip_rx_vld,			// 数据有效信号
	input wire			i_ip_rx_data_last,		// 最后一个数据的标志
	input wire	[7:0]	iv_ip_rx_data,			// 数据

	/*---------------- udp_receive 模块交互信号--------------------*/
	output wire			o_udp_rx_vld,			// 数据有效标志
	output wire			o_udp_rx_data_last,		// 数据部分最后一个
	output wire	[7:0]	ov_udp_rx_data,			// UDP层数据
	output wire	[15:0]	ov_udp_rx_length,		// UDP层数据包长度

	/*-------------------- icmp 模块交互信号----------------------*/
	output wire			o_icmp_rx_vld,			// 数据有效标志
	output wire			o_icmp_rx_data_last,	// 数据部分最后一个
	output wire	[7:0]	ov_icmp_rx_data,		// UDP层数据
	output wire	[15:0]	ov_icmp_rx_length,		// UDP层数据包长度


	//*********************** ip发送端 *****************************//
	/*---------------- udp_send 模块交互信号--------------------*/
	input wire			i_udp_tx_data_vld,		// 数据有效信号
	input wire			i_udp_tx_data_last,		// MAC层数据的最后一个数据的标志
	input wire	[7:0]	iv_udp_tx_data,			// 从MAC数据包解析出MAC层数据
	input wire	[15:0]	iv_udp_tx_length,		// 长度
	input wire	[7:0]	iv_tx_type,				// 帧类型

	/*---------------- mac_send 模块交互信号--------------------*/
	output wire			o_ip_tx_data_vld,
	output wire			o_ip_tx_data_last,
	output wire	[15:0]	ov_ip_tx_length,
	output wire	[7:0]	ov_ip_tx_data,

	//********************* 发送MAC地址查找命令 *********************//
	output wire			o_mac_list_rden,
	output wire	[31:0]	ov_list_ip_addr
);

	// IP层接收模块
	ip_receive #(
		.LOCAL_IP_ADDR	(LOCAL_IP_ADDR)
	) U_ip_receive (
		.i_clk               (i_rx_clk),
		.i_rst_n             (i_rx_rst_n),
		.i_ip_rx_vld         (i_ip_rx_vld),
		.i_ip_rx_data_last   (i_ip_rx_data_last),
		.iv_ip_rx_data       (iv_ip_rx_data),

		.o_udp_rx_vld        (o_udp_rx_vld),
		.o_udp_rx_data_last  (o_udp_rx_data_last),
		.ov_udp_rx_data      (ov_udp_rx_data),
		.ov_udp_rx_length    (ov_udp_rx_length),
		.o_icmp_rx_vld       (o_icmp_rx_vld),
		.o_icmp_rx_data_last (o_icmp_rx_data_last),
		.ov_icmp_rx_data     (ov_icmp_rx_data),
		.ov_icmp_rx_length   (ov_icmp_rx_length)
	);

	// IP层发送模块
	ip_send #(
		.LOCAL_IP_ADDR  (LOCAL_IP_ADDR),
		.TARGET_IP_ADDR (TARGET_IP_ADDR)
	) U_ip_send (
		.i_clk              (i_tx_clk),
		.i_rst_n            (i_tx_rst_n),
		.i_udp_tx_data_vld  (i_udp_tx_data_vld),
		.i_udp_tx_data_last (i_udp_tx_data_last),
		.iv_udp_tx_data     (iv_udp_tx_data),
		.iv_udp_tx_length   (iv_udp_tx_length),
		.iv_tx_type			(iv_tx_type),

		.o_ip_tx_data_vld   (o_ip_tx_data_vld),
		.o_ip_tx_data_last  (o_ip_tx_data_last),
		.ov_ip_tx_length    (ov_ip_tx_length),
		.ov_ip_tx_data      (ov_ip_tx_data),

		.o_mac_list_rden	(o_mac_list_rden),
		.ov_list_ip_addr	(ov_list_ip_addr)
	);
endmodule

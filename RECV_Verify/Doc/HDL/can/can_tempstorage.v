//+FHEADER //////////////////////////////////////////////////////////////////////
//  版权所有者： 
//  Copyright 2014. All Rights Reserved.
//  保密级别：  
//---------------------------------------------------------------------------
//  文件名 :  can_tempstorage.v
//  原始作者：丁云冰
//  联系方式：
// --------------------------------------------------------------------------
// 版本升级历史：
// 版本生效日期： 
// 1.0 2014-1-14  丁云冰  第1版  
// 
//---------------------------------------------------------------------------
// 关键字 :   storage
//---------------------------------------------------------------------------
// 模块功能:  storage data read from mcp2515
//            对从spi接收到的数据简单的处理一下对齐输出 
// --------------------------------------------------------------------------
// 参数文件名：  
// --------------------------------------------------------------------------
// 重复使用问题：
// 复位策略:负跳变沿异步复位信号 i_rst_n
// 时钟域 : i_clk_27M 
// 关键时序: N/A
// 是否需要实例引用别的模块 : No
// 可综合否 : Yes 
//-FHEADER //////////////////////////////////////////////////////////////////////
module can_tempstorage
       (
         //time & rst
         i_clk_27M,
         i_rst_n,
         
         //input control
         i_byte_clk,      //脉冲信号，代表字节有效      
         
         //data in
         i_read_data,      //从spi接收到的数据
         i_read_num,       //上层要读的字节数目
         
         //output
         o_read_ok,        //读完上层要求的字节数后给出的一个脉冲信号
         o_read_byte,      //给上层的一个字节脉冲信号
                           
         //data out        
         o_reg_out         //和o_read_byte的下降沿对齐输出
         );
    input i_clk_27M;
    input i_rst_n;
    input i_byte_clk;
    input[7:0] i_read_data;
    input[7:0] i_read_num;
    
    output o_read_ok;
    output o_read_byte;
    output[7:0] o_reg_out;
////////////////////////////////////////
    reg[7:0] o_reg_out; 
    wire o_read_byte;
    reg[7:0] byte_cnt;
////////////////////////////////////////
    reg byte_clk_r;
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
      if(!i_rst_n) 
          byte_clk_r <= 0;
      else 
          byte_clk_r <= i_byte_clk;
    end
    assign o_read_ok = (byte_cnt == (i_read_num - 8'd1))?i_byte_clk:1'd0;
    assign o_read_byte = (i_read_num == 8'd13)?byte_clk_r:1'd0;
////////////////////////////////////////
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
      if(!i_rst_n) 
          o_reg_out <= 0;
      else if (i_byte_clk) 
          o_reg_out <= 0;
      else if (byte_clk_r) 
          o_reg_out <= i_read_data;
    end
///////////////////////////////////////
////////////////////////////////////////  
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
      if(!i_rst_n) 
          byte_cnt<=0;
      else if(o_read_ok)
          byte_cnt<=0;
      else if(i_byte_clk)
          byte_cnt<=byte_cnt+8'd1;
    end 
  
endmodule
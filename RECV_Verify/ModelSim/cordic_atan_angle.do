onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -label sim:/Hilbert_tb/Group1 -group {Region: sim:/Hilbert_tb} /Hilbert_tb/i_clk_108M
add wave -noupdate -expand -label sim:/Hilbert_tb/Group1 -group {Region: sim:/Hilbert_tb} /Hilbert_tb/rst_n
add wave -noupdate -expand -label sim:/Hilbert_tb/Group1 -group {Region: sim:/Hilbert_tb} -radix decimal -radixshowbase 0 /Hilbert_tb/x
add wave -noupdate -expand -label sim:/Hilbert_tb/Group1 -group {Region: sim:/Hilbert_tb} -radix decimal -radixshowbase 0 /Hilbert_tb/y
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -radix decimal -radixshowbase 0 /Hilbert_tb/y
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -radix decimal -radixshowbase 0 /Hilbert_tb/x
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color Red -radix binary /Hilbert_tb/U_cordic/quadrant_angle
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/im_yr
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/re_xr
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} /Hilbert_tb/U_cordic/angle_zr
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/im_y0
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/re_x0
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/angle_z0
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} /Hilbert_tb/sample_clk
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color Orange -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/im_y1
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color Orange -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/re_x1
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color Orange -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/angle_z1
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color Cyan -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/im_y2
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color Cyan -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/re_x2
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color Cyan -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/angle_z2
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color Violet -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/im_y3
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color Violet -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/re_x3
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color Violet -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/angle_z3
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color {Lime Green} -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/im_y4
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color {Lime Green} -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/re_x4
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color {Lime Green} -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/angle_z4
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color {Cornflower Blue} -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/im_y5
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color {Cornflower Blue} -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/re_x5
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color {Cornflower Blue} -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/angle_z5
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color {Orange Red} -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/im_y6
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color {Orange Red} -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/re_x6
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color {Orange Red} -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/angle_z6
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color Salmon -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/im_y7
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color Salmon -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/re_x7
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color Salmon -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/angle_z7
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color Yellow -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/im_y8
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color Yellow -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/re_x8
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color Yellow -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/angle_z8
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color {Violet Red} -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/im_y9
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color {Violet Red} -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/re_x9
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color {Violet Red} -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/angle_z9
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color {Slate Blue} -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/im_y10
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color {Slate Blue} -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/re_x10
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color {Slate Blue} -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/angle_z10
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color Aquamarine -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/im_y11
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color Aquamarine -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/re_x11
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color Aquamarine -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/angle_z11
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color {Orange Red} -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/im_y12
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color {Orange Red} -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/re_x12
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color {Orange Red} -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/angle_z12
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color Orange -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/im_y13
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color Orange -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/re_x13
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color Orange -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/angle_z13
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color Yellow -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/im_y14
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color Yellow -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/re_x14
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color Yellow -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/angle_z14
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color Magenta -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/im_y15
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color Magenta -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/re_x15
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -color Magenta -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/angle_z15
add wave -noupdate -expand -label sim:/Hilbert_tb/U_cordic/Group2 -group {Region: sim:/Hilbert_tb/U_cordic} -radix decimal -radixshowbase 0 /Hilbert_tb/U_cordic/ov_angle_tan
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {170000 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ms
update
WaveRestoreZoom {115629 ps} {568021 ps}

--------------------------------------------------------------
-- Dallas/Maxim "1-Wire" interface				
--
-- Hamish Rawnsley June 2007
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_signed.all;
use ieee.std_logic_arith.conv_std_logic_vector;

entity One_Wire is
	generic (SYSCLOCK		: integer := 27000000);
	port (	resetn			: in std_logic;
			clock			: in std_logic;
			
			Address			: in std_logic_vector(3 downto 0);
			Write_Byte_in	: in std_logic_vector(7 downto 0);
			Read_Byte_out	: out std_logic_vector(7 downto 0);
			
			One_Wire_data	: inout std_logic;
			
			Busy			: out std_logic);
end One_Wire;

architecture arch of One_Wire is
	
	constant TRSTL 			: integer := ((SYSCLOCK / 1000000) *480) - 1;	
	constant TRSTH 			: integer := ((SYSCLOCK / 1000000) * 480) - 1;	
	constant TPDH 			: integer := ((SYSCLOCK / 1000000) * 60) - 1;	
	constant TPDL 			: integer := ((SYSCLOCK / 1000000) * 240) - 1;	
	constant TLOW0 			: integer := ((SYSCLOCK / 1000000) * 60) - 1;	
	constant TLOW1 			: integer := ((SYSCLOCK / 1000000) * 1) - 1;	
	constant TREC 			: integer := ((SYSCLOCK / 1000000) * 5) - 1;	
	constant TSLOT 			: integer := ((SYSCLOCK / 1000000) * 65) - 1;	
	constant TLOWR 			: integer := ((SYSCLOCK / 1000000) * 5) - 1;	
	constant TRDV 			: integer := ((SYSCLOCK / 1000000) * 14) - 1;	
	constant TRELEASE 		: integer := ((SYSCLOCK / 1000000) * 50) - 1;	
	constant TSHA1			: integer := ((SYSCLOCK / 1000) * 10) - 1;	
	
	subtype TIME_COUNT is integer range 0 to TSHA1;   
	subtype BIT_COUNT is integer range 0 to 7;
	
	type STATES is (IDLE, RESET1, RESET2, RESET3, YES_DEVICE,
					LOAD_CMD, WRITE_BEGIN, WRITE_0, WRITE_1, WRITE_WAIT,
					READ_BEGIN, READ_LOW, READ_SAMPLE, READ_WAIT, SHA_WAIT);  
		
    type reg_type is record
		Data_out_reg		: std_logic;
		Data_in_reg			: std_logic_vector(1 downto 0); 
		T_Counter			: TIME_COUNT; 	
		B_Counter			: BIT_COUNT; 	
		Write_Byte_reg		: std_logic_vector(7 downto 0);
		Read_Byte_reg		: std_logic_vector(7 downto 0);
		One_Wire_State		: STATES;		
    end record;
    
    signal 	r, 
    		rin        		: reg_type;

	signal  data_out_ena,
			T_Counter_rst,
			B_Counter_rst,
			B_Counter_ena	: std_logic;

begin
	
		
	Read_Byte_out			<= r.Read_Byte_reg;
	

	tristate_buffer : process (r, data_out_ena, One_Wire_data)
	begin
		if (data_out_ena = '1') then
			One_Wire_data	<= r.Data_out_reg;
		else	
			One_Wire_data	<= 'Z';
		end if;
		
		rin.Data_in_reg		<= r.Data_in_reg(0) & One_Wire_data;	
						
	end process tristate_buffer;

	OW_FSM : process(r, Write_Byte_in, Address)
	begin
	
		rin.Data_out_reg	<= '1';	
		data_out_ena		<= '1';
		T_Counter_rst		<= '1';
		B_Counter_rst		<= '0';
		B_Counter_ena		<= '0';
		rin.Write_Byte_reg	<= r.Write_Byte_reg;
		rin.Read_Byte_reg	<= r.Read_Byte_reg;
		Busy				<= '1'; 
	
		case (r.One_Wire_State) is
		
		when IDLE =>
			B_Counter_rst		<= '1';
			Busy				<= '0';
			if (Address(3) = '1') then 	
				case (Address(2 downto 0)) is
				when "111" =>			
					rin.One_Wire_State	<= RESET1;
				when "001" =>			
					rin.One_Wire_State	<= READ_BEGIN;
				when "011" =>			
					rin.One_Wire_State	<= LOAD_CMD;
				when "100" =>			
					rin.One_Wire_State	<= SHA_WAIT;
				when others =>
					rin.One_Wire_State	<= IDLE;
				end case;
			else
				rin.One_Wire_State	<= IDLE;
			end if;

		when RESET1 =>
			rin.Data_out_reg	<= '0';
			if (r.T_Counter = TRSTL) then
				T_Counter_rst		<= '1';
				rin.One_Wire_State	<= RESET2;
			else
				T_Counter_rst		<= '0';
				rin.One_Wire_State	<= RESET1;
			end if;
		
		when RESET2 =>
			data_out_ena		<= '0';
			if (r.T_Counter = TPDH) then
				T_Counter_rst		<= '1';
				rin.One_Wire_State	<= RESET3;
			else
				T_Counter_rst		<= '0';
				rin.One_Wire_State	<= RESET2;
			end if;
		
		when RESET3 =>
			data_out_ena		<= '0';
			if (r.T_Counter = TPDL) then
				T_Counter_rst		<= '1';
				rin.Read_Byte_reg	<= "11111111"; 
				rin.One_Wire_State	<= IDLE;
			else
				T_Counter_rst		<= '0';
				if (r.Data_in_reg(1) = '0') then
					rin.Read_Byte_reg	<= "00001111"; 
					rin.One_Wire_State	<= YES_DEVICE;
				else
					rin.Read_Byte_reg	<= "00000000"; 
					rin.One_Wire_State	<= RESET3;
				end if;
			end if;
		
		when YES_DEVICE =>
			if (r.T_Counter = TRSTH) then
				T_Counter_rst		<= '1';
				rin.One_Wire_State	<= IDLE;
			else
				T_Counter_rst		<= '0';
				rin.One_Wire_State	<= YES_DEVICE;
			end if;


		when LOAD_CMD =>
			rin.Write_Byte_reg	<= Write_Byte_in;
			rin.One_Wire_State	<= WRITE_BEGIN;
		
		when WRITE_BEGIN =>
			T_Counter_rst		<= '1';
			if (r.Write_Byte_reg(r.B_Counter) = '1') then
				rin.One_Wire_State	<= WRITE_1;
			else
				rin.One_Wire_State	<= WRITE_0;
			end if;
		
		when WRITE_0 =>
			rin.Data_out_reg	<= '0';
			if (r.T_Counter = TLOW0) then
				T_Counter_rst		<= '1';
				rin.One_Wire_State	<= WRITE_WAIT;
			else
				T_Counter_rst		<= '0';
				rin.One_Wire_State	<= WRITE_0;
			end if;
		
		when WRITE_1 =>
			rin.Data_out_reg		<= '0';
			if (r.T_Counter <= TLOW1) then
				T_Counter_rst		<= '0';
				data_out_ena		<= '1';
				rin.One_Wire_State	<= WRITE_1;
			elsif (r.T_Counter <= TSLOT) then
				T_Counter_rst		<= '0';
				data_out_ena		<= '0'; 
				rin.One_Wire_State	<= WRITE_1;
			else
				T_Counter_rst		<= '1';
				data_out_ena		<= '0'; 
				rin.One_Wire_State	<= WRITE_WAIT;
			end if;
		
		when WRITE_WAIT =>
			data_out_ena			<= '0'; 
			if (r.T_Counter = TREC) then
				if (r.B_Counter = 7) then
					T_Counter_rst		<= '1';
					B_Counter_rst		<= '1';
					B_Counter_ena		<= '0';
					rin.One_Wire_State	<= IDLE;
				else
					T_Counter_rst		<= '1';
					B_Counter_rst		<= '0';
					B_Counter_ena		<= '1';
					rin.One_Wire_State	<= WRITE_BEGIN;
				end if;
			else
				T_Counter_rst		<= '0';
				B_Counter_ena		<= '0';
				rin.One_Wire_State	<= WRITE_WAIT;
			end if;	

		when READ_BEGIN =>
			data_out_ena			<= '0'; 
			T_Counter_rst			<= '1';
			rin.Data_out_reg		<= '0';
			rin.One_Wire_State		<= READ_LOW;
					
		when READ_LOW =>
			T_Counter_rst			<= '0';
			rin.Data_out_reg		<= '0';
			if (r.T_Counter = TLOWR) then
				rin.One_Wire_State	<= READ_SAMPLE;
			else
				rin.One_Wire_State	<= READ_LOW;
			end if;
		
		when READ_SAMPLE =>
			data_out_ena			<= '0'; 
			if (r.T_Counter = TRDV) then
				rin.Read_Byte_reg	<= r.Data_in_reg(1) & r.Read_Byte_reg(7 downto 1); 
				T_Counter_rst		<= '1';
				rin.One_Wire_State	<= READ_WAIT;
			else
				T_Counter_rst		<= '0';
				rin.One_Wire_State	<= READ_SAMPLE;
			end if;
			
		when READ_WAIT =>
			data_out_ena			<= '0'; 
			if (r.T_Counter = TRELEASE) then
				if (r.B_Counter = 7) then
					T_Counter_rst		<= '1';
					B_Counter_rst		<= '1';
					B_Counter_ena		<= '0';
					rin.One_Wire_State	<= IDLE;
				else
					T_Counter_rst		<= '1';
					B_Counter_rst		<= '0';
					B_Counter_ena		<= '1';
					rin.One_Wire_State	<= READ_BEGIN;
				end if;
			else
				T_Counter_rst		<= '0';
				B_Counter_ena		<= '0';
				rin.One_Wire_State	<= READ_WAIT;
			end if;

		when SHA_WAIT =>
			T_Counter_rst			<= '0';
			data_out_ena			<= '0'; 
			if (r.T_Counter = TSHA1) then
				rin.One_Wire_State	<= IDLE;
			else
				rin.One_Wire_State	<= SHA_WAIT;
			end if;
		end case;
	
	end process OW_FSM;	
	
	
	regs : process (resetn, clock,	r, rin)
    begin
		if (resetn = '0') then
		
				r.Data_in_reg		<= "00";
				r.Data_out_reg		<= '1';
				r.T_Counter			<= 0;
				r.B_Counter			<= 0;
				r.Write_Byte_reg	<= (others => '0');
				r.Read_Byte_reg		<= (others => '0');
				r.One_Wire_State	<= IDLE;				
		
        elsif (clock'event and clock = '1') then

				r.Data_in_reg		<= rin.Data_in_reg;			
				r.Data_out_reg		<= rin.Data_out_reg;
				
				if (T_Counter_rst = '1') then
					r.T_Counter		<= 0;
				else
					r.T_Counter		<= r.T_Counter + 1;
				end if;
				
				if (B_Counter_rst = '1') then
					r.B_Counter		<= 0;
				elsif (B_Counter_ena = '1') then
					r.B_Counter		<= r.B_Counter + 1;
				else
					r.B_Counter		<= r.B_Counter;
				end if;
				
				r.Write_Byte_reg	<= rin.Write_Byte_reg;				
				r.Read_Byte_reg		<= rin.Read_Byte_reg;				
				r.One_Wire_State	<= rin.One_Wire_State;
	        			
        end if;
    end process regs;
end arch;
-------------------------------------------------------------
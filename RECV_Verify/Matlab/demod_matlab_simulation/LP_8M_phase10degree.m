function Hd = LP_8M_phase10degree
%LP_8M_PHASE0.2RAD Returns a discrete-time filter object.

%
% MATLAB Code
% Generated by MATLAB(R) 8.0 and the Signal Processing Toolbox 6.18.
%
% Generated on: 29-Mar-2017 10:13:00
%

% Chebyshev Type II Lowpass filter designed using FDESIGN.LOWPASS.

% All frequency values are in MHz.
Fs = 27.095;  % Sampling Frequency

N     = 10;  % Order
Fstop = 8;   % Stopband Frequency
Astop = 30;  % Stopband Attenuation (dB)

% Construct an FDESIGN object and call its CHEBY2 method.
h  = fdesign.lowpass('N,Fst,Ast', N, Fstop, Astop, Fs);
Hd = design(h, 'cheby2');

% [EOF]

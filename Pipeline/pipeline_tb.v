`timescale 1ns/1ps
module pipeline_tb();

    reg         clk;
    reg         rst_n;
    reg [7:0]   i_X;
    reg         start;

    wire    [7:0]   X_Pipeline;
    wire    [7:0]   X_NoPipeline;    
    wire            finished;

    parameter X_MAX = 8'd16;

    initial begin
        clk = 1'b1;
        rst_n <= 1'b0;
        #20
        rst_n <= 1'b1;
    end

    always #10 clk <= ~clk;

    always@(posedge clk or negedge rst_n)begin 
        if(rst_n == 1'b0)
            i_X <= 8'd0;
        else if(i_X == X_MAX - 1'b1)
            i_X <= 8'd0;
        else
            i_X <= i_X + 1'b1;
    end

    pipeline U_Times_pipe(
        .clk    (clk),
        .i_X    (i_X),

        .Times_P(X_Pipeline)
    );

    always@(posedge clk or negedge rst_n)begin 
        if(!rst_n)
            start <= 1'b0;
        else if(i_X == 8'd1)
            start <= 1'b1;
        else
            start <= 1'b0;
    end

    pipeline_none U_Times_N(
        .i_X     (i_X),
        .clk     (clk),
        .start   (start),

        .Times_NP(X_NoPipeline),
        .finished(finished)
);

endmodule
`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date: 2024/08/27 14:37:30
// Module Name: icmp_send
// Description: icmp组包发送端,实现ping功能.
//////////////////////////////////////////////////////////////////////////////////

module icmp_send(
	input	wire 	i_icmp_tx_clk,
	input	wire	i_icmp_tx_rst_n,

	input	wire	[15:0]	iv_icmp_tx_identity,
	input	wire	[15:0]	iv_icmp_tx_sequence,
	input	wire			i_icmp_reply_req,

	output	reg		[7:0]	ov_icmp_tx_data,
	output	reg				o_icmp_tx_vld,
	output 	wire	[15:0]	ov_icmp_tx_length,
	output	reg				o_icmp_tx_data_last
);

/*-----------------------------------------------*\
					信号和参数定义
\*-----------------------------------------------*/
	localparam	ICMP_TX_LENGTH = 16'd40;
	
	reg	[10:0]	cnt_tx_byte;
	// ICMP的发送长度为固定值40,这是通过Wireshark抓包分析得到的.
	assign	ov_icmp_tx_length = ICMP_TX_LENGTH;

	// 接收标识和序号并缓存
	reg	[15:0]	icmp_identity;	
	reg	[15:0]	icmp_sequence;
	reg	[31:0]	icmp_check_sum;

/*-----------------------------------------------*\
					发送部分:组包
\*-----------------------------------------------*/	

	// 缓存标识、序号
	always @(posedge i_icmp_tx_clk or negedge i_icmp_tx_rst_n) begin
		if(~i_icmp_tx_rst_n) begin
			icmp_identity <= 16'h0;
			icmp_sequence <= 16'h0;
		end 
		else if(i_icmp_reply_req)begin
			icmp_identity <= iv_icmp_tx_identity;
			icmp_sequence <= iv_icmp_tx_sequence;
		end
		else begin
			icmp_identity <= icmp_identity;
			icmp_sequence <= icmp_sequence;
		end
	end

	always @(posedge i_icmp_tx_clk or negedge i_icmp_tx_rst_n) begin
		if(~i_icmp_tx_rst_n)
			cnt_tx_byte <= 11'd0;
		else if(cnt_tx_byte == ICMP_TX_LENGTH - 1'b1)
			cnt_tx_byte <= 11'd0;
		else if(i_icmp_reply_req || (cnt_tx_byte != 0))
			cnt_tx_byte <= cnt_tx_byte + 1'b1;
		else
			cnt_tx_byte <= cnt_tx_byte;
	end

	always @(posedge i_icmp_tx_clk or negedge i_icmp_tx_rst_n) begin
		if(~i_icmp_tx_rst_n)
			o_icmp_tx_data_last <= 1'b0;
		else if(cnt_tx_byte == ICMP_TX_LENGTH - 1'b1)
			o_icmp_tx_data_last <= 1'b0;
		else if(i_icmp_reply_req)
			o_icmp_tx_data_last <= 1'b1;
		else
			o_icmp_tx_data_last <= o_icmp_tx_data_last;
	end

	always @(posedge i_icmp_tx_clk or negedge i_icmp_tx_rst_n) begin
		if(~i_icmp_tx_rst_n)
			o_icmp_tx_vld <= 1'b0;
		else if(i_icmp_reply_req)
			o_icmp_tx_vld <= 1'b1;
		else if(o_icmp_tx_data_last)
			o_icmp_tx_vld <= 1'b0;
		else
			o_icmp_tx_vld <= o_icmp_tx_vld;
	end

	// 计算校验和
	always @(posedge i_icmp_tx_clk or negedge i_icmp_tx_rst_n) begin
		if(~i_icmp_tx_rst_n)
			icmp_check_sum <= 32'h0;
		else if(i_icmp_reply_req)	// 两字节为单位进行求和
			icmp_check_sum <= iv_icmp_tx_identity + iv_icmp_tx_identity;
		else if(cnt_tx_byte == 11'd1)
			icmp_check_sum <= ~(icmp_check_sum[31:16] + icmp_check_sum[15:0]);
		else
			icmp_check_sum <= icmp_check_sum;
	end


	// 组包发送
	always @(posedge i_icmp_tx_clk or negedge i_icmp_tx_rst_n) begin
		if(~i_icmp_tx_rst_n)
			ov_icmp_tx_data <= 0;
		else begin
			case (cnt_tx_byte)
				0 : ov_icmp_tx_data <= 8'h00;		// 类型,0为应答,8表示请求
				1 : ov_icmp_tx_data <= 8'h00;		// 代码,默认0
				// 校验和
				2 : ov_icmp_tx_data <= icmp_check_sum[15:8];
				3 : ov_icmp_tx_data <= icmp_check_sum[ 7:0];
				// ICMP标识,与发送方保持一致
				4 : ov_icmp_tx_data <= icmp_identity[15:8];
				5 : ov_icmp_tx_data <= icmp_identity[ 7:0];
				// 序号,与发送方保持一致
				6 : ov_icmp_tx_data <= icmp_sequence[15:8];
				7 : ov_icmp_tx_data <= icmp_sequence[ 7:0];
				default : ov_icmp_tx_data <= 8'h0;
			endcase
		end
	end

endmodule

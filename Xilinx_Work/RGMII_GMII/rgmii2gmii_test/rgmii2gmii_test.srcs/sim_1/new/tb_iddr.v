`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2024/09/15 22:51:00
// Design Name: 
// Module Name: tb_iddr
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tb_iddr();
    parameter  CYCLE    =   10      ;//系统时钟周期，单位ns，默认10ns；
    parameter  RST_TIME =   10      ;//系统复位持续时间，默认10个系统时钟周期；
 
    reg                     clk     ;//系统时钟，默认100MHz；
    reg                     clk_en  ;
    reg     [3:0]           rgmii_rxd     ;

    reg						rgmii_ctl;
 
    wire    [7:0]           ov_gmii_rxd;
    wire                    o_gmii_rx_dv;
    wire					o_gmii_rx_clk;

    rgmii_rx inst_rgmii_rx(
		.i_rgmii_rx_clk (clk),
		.i_rgmii_rx_ctl (rgmii_ctl),
		.iv_rgmii_rxd   (rgmii_rxd),
		.i_rgmii_cken	(clk_en),

		.o_gmii_rx_clk  (o_gmii_rx_clk),
		.o_gmii_rx_dv   (o_gmii_rx_dv),
		.ov_gmii_rxd    (ov_gmii_rxd)
	);

 
    //生成周期为CYCLE数值的系统时钟;
    initial begin
        clk = 1;
        forever #(CYCLE/2) clk = ~clk;
    end
 
    initial begin
        #1;
        rgmii_rxd = 4'b0;clk_en = 1'b0;rgmii_ctl=1'b0;
        #(CYCLE*(10+RST_TIME));
        clk_en = 1'b1;
        #(CYCLE);
        repeat(100)begin	//产生100个双沿时钟数据。
            #(CYCLE/2);
            rgmii_rxd = ({$random} % 16);
            rgmii_ctl = ({$random} % 2);
            #(CYCLE/2);
            rgmii_rxd = ({$random} % 16);
            rgmii_ctl = ({$random} % 2);
        end
        #(CYCLE);
        clk_en = 1'b0;
    end
 
endmodule

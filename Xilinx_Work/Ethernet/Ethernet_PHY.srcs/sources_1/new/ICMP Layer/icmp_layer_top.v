`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date: 2024/08/27 23:53:29
// Module Name: icmp_layer_top
// Description: ICMP顶层模块.
// Revision 0.01 - File Created
//////////////////////////////////////////////////////////////////////////////////


module icmp_layer_top(
	input wire			i_icmp_rx_clk,   
	input wire			i_icmp_tx_clk,
	input wire			i_icmp_tx_rst_n,
	input wire			i_icmp_rx_rst_n,

	input wire			i_icmp_rx_vld,
	input wire	[7:0]	iv_icmp_rx_data,
	input wire			i_icmp_rx_last,
	input wire	[15:0]	iv_icmp_rx_length,

	output wire	[7:0]	ov_icmp_tx_data,
	output wire			o_icmp_tx_vld,
	output wire	[15:0]	ov_icmp_tx_length,
	output wire			o_icmp_tx_data_last
);

/*-----------------------------------------------*\
					模块连线
\*-----------------------------------------------*/
	
	wire			icmp_reply_req;
	wire	[15:0]	icmp_identity;
	wire	[15:0]	icmp_sequence;

/*-----------------------------------------------*\
					模块例化
\*-----------------------------------------------*/
	icmp_receive U_icmp_receive(
		.i_icmp_rx_clk     (i_icmp_rx_clk),
		.i_icmp_tx_clk     (i_icmp_tx_clk),

		.i_icmp_tx_rst_n   (i_icmp_tx_rst_n),
		.i_icmp_rx_rst_n   (i_icmp_rx_rst_n),

		.i_icmp_rx_vld     (i_icmp_rx_vld),
		.iv_icmp_rx_data   (iv_icmp_rx_data),
		.i_icmp_rx_last    (i_icmp_rx_last),
		.iv_icmp_rx_length (iv_icmp_rx_length),

		.o_icmp_reply_req  (icmp_reply_req),
		.ov_icmp_identity  (icmp_identity),
		.ov_icmp_sequence  (icmp_sequence)
	);

	icmp_send U_icmp_send(
		.i_icmp_tx_clk       (i_icmp_tx_clk),
		.i_icmp_tx_rst_n     (i_icmp_tx_rst_n),

		.iv_icmp_tx_identity (icmp_identity),
		.iv_icmp_tx_sequence (icmp_sequence),
		.i_icmp_reply_req    (icmp_reply_req),

		.ov_icmp_tx_data     (ov_icmp_tx_data),
		.o_icmp_tx_vld       (o_icmp_tx_vld),
		.ov_icmp_tx_length   (ov_icmp_tx_length),
		.o_icmp_tx_data_last (o_icmp_tx_data_last)
	);
endmodule

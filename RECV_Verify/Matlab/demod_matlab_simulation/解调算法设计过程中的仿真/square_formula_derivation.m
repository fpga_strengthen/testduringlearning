FSK_freq_table = [
    174,  174;...
    0.01,0.02;...%频偏都为+
    3,5;...
    50,30;...  
    154,155;...       
    200,  200;...
    -0.01,-0.02;...%频偏都为-
    -3,-5;...
    -50,-30;...
    -154,-155;...    
    -174,  -174;...
    -200,  -200;...
    -0.01,0.02;...%频偏为- +
    -3,5;...
    -50,30;...  
    -154,155;...  
    -174,  174;...
    -200,  200;...
    0.01,-0.02;...%频偏为+ -
    3,-5;...
    50,-30;...  
    154,-155;...   
    174,  -174;...
    200,  -200;];%频偏 单位KHz
ws = 564;      %单位KHz
for freq_num = 1:size(FSK_freq_table,1)
    close all,
    %频偏
    w0 = FSK_freq_table(freq_num,1);
    w1 = FSK_freq_table(freq_num,2);
    
%         %过渡区在后24点时t在[-T,-T/2]之间
%         t = [-2*pi/ws:0.00001:-pi/ws ];
%         sum_bit0_point_back = (t+pi./ws).^2.*(sinc((w0.*t+w0.*pi./ws)./2).^2) - 4./(w1+ws).*(t+pi./ws).*sinc((w0.*t+w0.*pi./ws)./2).*sin((w1.*t+ws.*t+2.*w1.*pi./ws)./2).*cos((w0.*t-w1.*t-ws.*t+w0.*pi./ws-2.*w1.*pi./ws)./2);%W0路平方和
%         sum_bit1_point_back = (t+2.*pi./ws).^2.*(sinc((w1.*t+2.*w1.*pi./ws)./2).^2) - 8./(w1.*(w0-ws)).*cos((w0.*t-ws.*t+w0.*pi./ws)./2).*sin((w1.*t+2.*w1.*pi./ws)./2).*sin((w1.*t-w0.*t+ws.*t-w0.*pi./ws+2.*w1.*pi./ws)./2);%W1路平方和
%         sum_bit0_minus_sum_bit1_back = sum_bit0_point_back - sum_bit1_point_back;%过渡区积分结果的做减法
%         %常数区与过渡区做减法
%         sum_bit0_minus_sum_bit1_back = (pi./ws)^2.*(sinc((w0.*pi./(2.*ws))./4).^2) - 4./((w0-ws)^2).*(cos(w0.*pi./(2.*ws))^2) - sum_bit0_minus_sum_bit1_back ;
%     
%         figure(1),
%         plot(t,sum_bit0_point_back),
%         hold on,
%         plot(t,sum_bit1_point_back,'r'),
%         hold on,
%         plot(t,sum_bit0_minus_sum_bit1_back,'k'),
    
    
    %过渡区在前24点时t在[-T/2,0]之间
    t = [-pi/ws:0.00001:0 ];
    %W0路平方和
    sum_bit0_point_front_2 = (t .* sinc(w0 ./2 .* t)).^2 ;%2次项
    sum_bit0_point_front_1 = 4./(w1+ws).*t.*sinc(w0.*t./2).*sin((w1.*t-w0.*t+ws.*t+w1.*pi./ws)./2).*cos((w1.*t+ws.*t+w1.*pi./ws)./2);%1次项
    sum_bit0_point_front = sum_bit0_point_front_2 + sum_bit0_point_front_1; %W0路平方和结果
    %W1路平方和
    sum_bit1_point_front_2 = ((t + pi./ws) .* sinc((w1 .* t + w1.*pi./ws)./2)).^2 ;%2次项
    sum_bit1_point_front_1 = - 4.*t./w1.*sinc((w0.*t-ws.*t)./2).*sin((w1.*t+w1.*pi./ws)./2).*cos((w1.*t-w0.*t+ws.*t+w1.*pi./ws)./2);%1次项
    sum_bit1_point_front = sum_bit1_point_front_2 + sum_bit1_point_front_1;%W1路平方和结果
    
    %过渡区积分结果的做减法
    sum_bit0_minus_sum_bit1_front_2 = sum_bit0_point_front_2 - sum_bit1_point_front_2;%2次项相减
    sum_bit0_minus_sum_bit1_front_1 = sum_bit0_point_front_1 - sum_bit1_point_front_1;%1次项相减
    sum_bit0_minus_sum_bit1_front = sum_bit0_point_front - sum_bit1_point_front;%过渡区相减 = sum_bit0_minus_sum_bit1_front_2 + sum_bit0_minus_sum_bit1_front_1
    
    %过渡区与常数区做减法
%     sum_bit0_minus_sum_bit1_front = sum_bit0_minus_sum_bit1_front - (4./((w1+ws)^2).*(cos(w1.*pi./(2.*ws)).^2) - (pi./ws)^2.*(sinc(w1.*pi./(2.*ws)).^2));
    
    figure(1),
%     hold on,
    plot(t,sum_bit0_point_front_2,'r'),% W0路平方和2次项
    hold on,
    plot(t,sum_bit0_point_front_1,'g'),% W0路平方和1次项
    hold on,
    plot(t,sum_bit1_point_front_2,'r'),% W1路平方和2次项
    hold on,
    plot(t,sum_bit1_point_front_1,'g'),% W1路平方和1次项
    hold on,
    plot(t,sum_bit0_minus_sum_bit1_front_2,'.r'),% W0路2次项-W1路2次项 浅蓝色
    hold on,
    plot(t,sum_bit0_minus_sum_bit1_front_1,'.g'),% W0路1次项-W1路1次项 粉色
    hold on,
    plot(t,sum_bit0_point_front,'b'),% W0路平方和结果
    hold on,
    plot(t,sum_bit1_point_front,'y'),% W1路平方和结果
    hold on,
    plot(t,sum_bit0_minus_sum_bit1_front,'k'),% 待拟合曲线
    grid on,
    legend('W0路平方和2次项','W0路平方和1次项','W1路平方和2次项','W1路平方和1次项','W0路2次项-W1路2次项','W0路1次项-W1路1次项','W0路平方和结果','W1路平方和结果','待拟合曲线');
    title(strcat('频偏：W0=' ,num2str(w0),'Khz, w1:=',num2str(w1), 'Khz 时的同步曲线'));
    
    %% 由上图可以看出极值点的偏移主要由一次项引起，所以进一步对一次项进一步拆分（sin-sin）
    % sum_bit0_point_front_1 = 4./(w1+ws).*t.*sinc(w0.*t./2).*sin((w1.*t-w0.*t+ws.*t+w1.*pi./ws)./2).*cos((w1.*t+ws.*t+w1.*pi./ws)./2);% W0路1次项
    %sum_bit1_point_front_1 = - 4.*t./w1.*sinc((w0.*t-ws.*t)./2).*sin((w1.*t+w1.*pi./ws)./2).*cos((w1.*t-w0.*t+ws.*t+w1.*pi./ws)./2);% W1路1次项
    %sum_bit0_minus_sum_bit1_front_1 = sum_bit0_point_front_1 - sum_bit1_point_front_1;%1次项相减
    
    sum_bit0_point_front_1_1 = -2./(w1+ws).*t.*sinc(w0.*t./2).*sin(w0.*t./2);% W0路1次项拆分第1项
    sum_bit0_point_front_1_2 = 2./(w1+ws).*t.*sinc(w0.*t./2).*sin((2.*w1.*t-w0.*t+2.*ws.*t+2.*w1.*pi./ws)./2);% W0路1次项拆分第2项
    sum_bit0_point_front_1 = sum_bit0_point_front_1_1 + sum_bit0_point_front_1_2;% W0路1次项
    
    sum_bit1_point_front_1_1 = - 2.*t./w1.*sinc((w0.*t-ws.*t)./2).*sin((2.*w1.*t-w0.*t+ws.*t+2.*w1.*pi./ws)./2);% W1路1次项拆分第1项
    sum_bit1_point_front_1_2 = - 2.*t./w1.*sinc((w0.*t-ws.*t)./2).*sin((w0.*t-ws.*t)./2);% W1路1次项拆分第2项
    sum_bit1_point_front_1 = sum_bit1_point_front_1_1 + sum_bit1_point_front_1_2;% W1路1次项
    
    bit0_minus_bit1_1_1 = sum_bit0_point_front_1_1 - sum_bit1_point_front_1_1;% W0路1次项拆分第1项 - W1路1次项拆分第1项
    bit0_minus_bit1_1_2 = sum_bit0_point_front_1_2 - sum_bit1_point_front_1_2;% W0路1次项拆分第1项 - W1路1次项拆分第1项
    sum_bit0_minus_sum_bit1_front_1 = bit0_minus_bit1_1_1 + bit0_minus_bit1_1_2;%1次项相减结果 = （W0路1次项拆分第1项 - W1路1次项拆分第1项） + （W0路1次项拆分第1项 - W1路1次项拆分第1项）
    
    figure(2),
    set(gcf,'outerposition',get(0,'screensize'));%默认窗口最大化
    subplot(2,1,1),
    plot(t,sum_bit0_point_front_1,'--r'),% W0路平方和1次项
    hold on,
    plot(t,sum_bit0_point_front_1_1,'--g'),
    hold on,
    plot(t,sum_bit0_point_front_1_2,'--b'),
    hold on,
    plot(t,sum_bit1_point_front_1,'r'),% W1路平方和1次项
    hold on,
    plot(t,sum_bit1_point_front_1_1,'g'),
    hold on,
    plot(t,sum_bit1_point_front_1_2,'b'),
    hold on,
    plot(t,bit0_minus_bit1_1_1,'.g'),% W0路1次项拆分第1项 - W1路1次项拆分第1项
    hold on,
    plot(t,bit0_minus_bit1_1_2,'.b'),% W0路2次项拆分第1项 - W1路1次项拆分第2项
    hold on,
    plot(t,sum_bit0_minus_sum_bit1_front_1,'*m'),% 1次项相减结果
    hold on,
    plot(t,sum_bit0_minus_sum_bit1_front,'k'),% 过渡区相减结果
    grid on,
    legend('W0路平方和1次项','W0路平方和1次项——1','W0路平方和1次项——2','W1路平方和1次项','W1路平方和1次项——1','W1路平方和1次项——2','W0路1次项拆分第1项 - W1路1次项拆分第1项','W0路1次项拆分第2项 - W1路1次项拆分第2项','1次项相减结果','过渡区相减结果');
    title(strcat('频偏：W0=' ,num2str(w0),'Khz, w1:=',num2str(w1), 'Khz 时的一次项曲线分解法1'));
    
    %% 由上图可以看出极值点的偏移主要由一次项引起，所以进一步对一次项进一步拆分（-1+cos+cos-cos）
   % sum_bit0_point_front_1 = 4./(w1+ws).*t.*sinc(w0.*t./2).*sin((w1.*t-w0.*t+ws.*t+w1.*pi./ws)./2).*cos((w1.*t+ws.*t+w1.*pi./ws)./2);% W0路1次项
    %sum_bit1_point_front_1 = - 4.*t./w1.*sinc((w0.*t-ws.*t)./2).*sin((w1.*t+w1.*pi./ws)./2).*cos((w1.*t-w0.*t+ws.*t+w1.*pi./ws)./2);% W1路1次项
    %sum_bit0_minus_sum_bit1_front_1 = sum_bit0_point_front_1 - sum_bit1_point_front_1;%1次项相减
    
    sum_bit0_point_front_1_1 = -2./(w0.*(w1+ws));% W0路1次项拆分第1项 为关于t的常数
    sum_bit0_point_front_1_2 = 2./(w0.*(w1+ws)).*cos(w0.*t);% W0路1次项拆分第2项
    sum_bit0_point_front_1_3 = 2./(w0.*(w1+ws)).*cos(w1.*t-w0.*t+ws.*t+w1.*pi./ws);% W0路1次项拆分第3项
    sum_bit0_point_front_1_4 = -2./(w0.*(w1+ws)).*cos(w1.*t+ws.*t+w1.*pi./ws);% W0路1次项拆分第4项
    sum_bit0_point_front_1 = sum_bit0_point_front_1_1 + sum_bit0_point_front_1_2 + sum_bit0_point_front_1_3 + sum_bit0_point_front_1_4;% W0路1次项
    
    sum_bit1_point_front_1_1 = -2./(w1.*(w0-ws));% W1路1次项拆分第1项 为关于t的常数
    sum_bit1_point_front_1_2 = 2./(w1.*(w0-ws)).*cos(w0.*t-ws.*t);% W1路1次项拆分第2项
    sum_bit1_point_front_1_3 = -2./(w1.*(w0-ws)).*cos(w0.*t-ws.*t-w1.*t-w1.*pi./ws);% W1路1次项拆分第3项
    sum_bit1_point_front_1_4 = 2./(w1.*(w0-ws)).*cos(w1.*t+w1.*pi./ws);% W1路1次项拆分第4项
    sum_bit1_point_front_1 = sum_bit1_point_front_1_1 + sum_bit1_point_front_1_2 + sum_bit1_point_front_1_3 + sum_bit1_point_front_1_4;% W1路1次项
    
    bit0_minus_bit1_1_1 = sum_bit0_point_front_1_1 - sum_bit1_point_front_1_1;% W0路1次项拆分第1项 - W1路1次项拆分第1项 为关于t的常数
    bit0_minus_bit1_1_2 = sum_bit0_point_front_1_2 - sum_bit1_point_front_1_2;% W0路1次项拆分第2项 - W1路1次项拆分第2项
    bit0_minus_bit1_1_3 = sum_bit0_point_front_1_3 - sum_bit1_point_front_1_3;% W0路1次项拆分第3项 - W1路1次项拆分第3项
    bit0_minus_bit1_1_4 = sum_bit0_point_front_1_4 - sum_bit1_point_front_1_4;% W0路1次项拆分第4项 - W1路1次项拆分第4项
    %1次项相减结果 = （W0路1次项拆分第1项 - W1路1次项拆分第1项） + （W0路1次项拆分第2项 - W1路1次项拆分第2项） +  （W0路1次项拆分第3项 - W1路1次项拆分第3项） +  （W0路1次项拆分第4项 - W1路1次项拆分第4项）
    sum_bit0_minus_sum_bit1_front_1 = bit0_minus_bit1_1_1 + bit0_minus_bit1_1_2 + bit0_minus_bit1_1_3 + bit0_minus_bit1_1_4;
   
    subplot(2,1,2),
    plot(t,sum_bit0_point_front_1,'--r'),% W0路平方和1次项
    hold on,
%     plot(t,sum_bit0_point_front_1_1,'--g'),
%     hold on,

plot(t,sum_bit0_point_front_1_2,'--b'),
    hold on,
    plot(t,sum_bit0_point_front_1_3,'--y'),
    hold on,
    plot(t,sum_bit0_point_front_1_4,'--c'),
    hold on,
    plot(t,sum_bit1_point_front_1,'r'),% W1路平方和1次项
    hold on,
%     plot(t,sum_bit1_point_front_1_1,'g'),
%     hold on,
    plot(t,sum_bit1_point_front_1_2,'b'),
    hold on,
    plot(t,sum_bit1_point_front_1_3,'y'),
    hold on,
    plot(t,sum_bit1_point_front_1_4,'c'),
    hold on,
%     plot(t,bit0_minus_bit1_1_1,'.g'),% W0路1次项拆分第1项 - W1路1次项拆分第1项
%     hold on,
    plot(t,bit0_minus_bit1_1_2,'.b'),% W0路2次项拆分第2项 - W1路1次项拆分第2项
    hold on,
    plot(t,bit0_minus_bit1_1_1,'.y'),% W0路1次项拆分第3项 - W1路1次项拆分第3项
    hold on,
    plot(t,bit0_minus_bit1_1_2,'.c'),% W0路2次项拆分第4项 - W1路1次项拆分第4项
    hold on,
    plot(t,sum_bit0_minus_sum_bit1_front_1,'*m'),% 1次项相减结果
    hold on,
    plot(t,sum_bit0_minus_sum_bit1_front,'k'),% 过渡区相减结果
    grid on,
    legend('W0路平方和1次项','W0路1次项—2','W0路1次项—3','W0路1次项—4','W1路平方和1次项','W1路1次项—2','W1路1次项—3','W1路1次项—4','W0路1_2 - W1路1_2','W0路1_3 - W1路1_3','W0路1-4 - W1路1_4', '1次项相减结果','过渡区相减结果');
    title(strcat('频偏：W0=' ,num2str(w0),'Khz, w1:=',num2str(w1), 'Khz 时的一次项曲线分解法2'));
    
    pause,
end
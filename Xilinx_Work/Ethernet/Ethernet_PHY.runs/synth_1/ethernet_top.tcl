# 
# Synthesis run script generated by Vivado
# 

set TIME_start [clock seconds] 
proc create_report { reportName command } {
  set status "."
  append status $reportName ".fail"
  if { [file exists $status] } {
    eval file delete [glob $status]
  }
  send_msg_id runtcl-4 info "Executing : $command"
  set retval [eval catch { $command } msg]
  if { $retval != 0 } {
    set fp [open $status w]
    close $fp
    send_msg_id runtcl-5 warning "$msg"
  }
}
set_param chipscope.maxJobs 4
set_param synth.incrementalSynthesisCache C:/Users/11976/AppData/Roaming/Xilinx/Vivado/.Xil/Vivado-24960-J_GMson/incrSyn
set_msg_config -id {Synth 8-256} -limit 10000
set_msg_config -id {Synth 8-638} -limit 10000
create_project -in_memory -part xc7vx485tffg1157-1

set_param project.singleFileAddWarning.threshold 0
set_param project.compositeFile.enableAutoGeneration 0
set_param synth.vivado.isSynthRun true
set_msg_config -source 4 -id {IP_Flow 19-2162} -severity warning -new_severity info
set_property webtalk.parent_dir D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.cache/wt [current_project]
set_property parent.project_path D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.xpr [current_project]
set_property XPM_LIBRARIES {XPM_CDC XPM_MEMORY} [current_project]
set_property default_lib xil_defaultlib [current_project]
set_property target_language Verilog [current_project]
set_property ip_output_repo d:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.cache/ip [current_project]
set_property ip_cache_permissions {read write} [current_project]
read_verilog -library xil_defaultlib {
  {D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/new/ARP Layer/arp_dynamic_list.v}
  {D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/new/ARP Layer/arp_layer_top.v}
  {D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/new/ARP Layer/arp_receive.v}
  {D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/new/ARP Layer/arp_send.v}
  D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/new/Sys_Clock/clk_rst_sync.v
  {D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/new/UDP Framework/crc32_d8.v}
  {D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/new/ICMP Layer/icmp_layer_top.v}
  {D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/new/ICMP Layer/icmp_receive.v}
  {D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/new/ICMP Layer/icmp_send.v}
  {D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/new/UDP Framework/ip_layer_top.v}
  {D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/new/UDP Framework/ip_receive.v}
  {D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/new/UDP Framework/ip_send.v}
  {D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/new/UDP Framework/mac_layer_top.v}
  {D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/new/UDP Framework/mac_receive.v}
  {D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/new/UDP Framework/mac_send.v}
  {D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/new/UDP Framework/mac_to_arp_ip.v}
  D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/new/Arbit/mux2_arbit.v
  D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/new/RGMII_GMII/rgmii_interface.v
  D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/new/RGMII_GMII/rgmii_receive.v
  D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/new/RGMII_GMII/rgmii_send.v
  D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/new/Sys_Rst_n/sync_rstn_gen.v
  {D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/new/UDP Framework/udp_layer_top.v}
  D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/new/udp_protocal_stack.v
  {D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/new/UDP Framework/udp_receive.v}
  {D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/new/UDP Framework/udp_send.v}
  D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/new/ethernet_top.v
}
read_ip -quiet D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/pll_125M_200M/pll_125M_200M.xci
set_property used_in_implementation false [get_files -all d:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/pll_125M_200M/pll_125M_200M_board.xdc]
set_property used_in_implementation false [get_files -all d:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/pll_125M_200M/pll_125M_200M.xdc]
set_property used_in_implementation false [get_files -all d:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/pll_125M_200M/pll_125M_200M_ooc.xdc]

read_ip -quiet D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/frame_fifo_w17xd16/frame_fifo_w17xd16.xci
set_property used_in_implementation false [get_files -all d:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/frame_fifo_w17xd16/frame_fifo_w17xd16.xdc]
set_property used_in_implementation false [get_files -all d:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/frame_fifo_w17xd16/frame_fifo_w17xd16_clocks.xdc]
set_property used_in_implementation false [get_files -all d:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/frame_fifo_w17xd16/frame_fifo_w17xd16_ooc.xdc]

read_ip -quiet D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/fifo_data_w9xd4096/fifo_data_w9xd4096.xci
set_property used_in_implementation false [get_files -all d:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/fifo_data_w9xd4096/fifo_data_w9xd4096.xdc]
set_property used_in_implementation false [get_files -all d:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/fifo_data_w9xd4096/fifo_data_w9xd4096_clocks.xdc]
set_property used_in_implementation false [get_files -all d:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/fifo_data_w9xd4096/fifo_data_w9xd4096_ooc.xdc]

read_ip -quiet D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/data_delay20/data_delay20.xci
set_property used_in_implementation false [get_files -all d:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/data_delay20/data_delay20_ooc.xdc]

read_ip -quiet D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/shift_ram_w16xd64/shift_ram_w16xd64.xci
set_property used_in_implementation false [get_files -all d:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/shift_ram_w16xd64/shift_ram_w16xd64_ooc.xdc]

read_ip -quiet D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/fifo_w81xd16/fifo_w81xd16.xci
set_property used_in_implementation false [get_files -all d:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/fifo_w81xd16/fifo_w81xd16.xdc]
set_property used_in_implementation false [get_files -all d:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/fifo_w81xd16/fifo_w81xd16_clocks.xdc]
set_property used_in_implementation false [get_files -all d:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/fifo_w81xd16/fifo_w81xd16_ooc.xdc]

read_ip -quiet D:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/fifo_w32xd16/fifo_w32xd16.xci
set_property used_in_implementation false [get_files -all d:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/fifo_w32xd16/fifo_w32xd16.xdc]
set_property used_in_implementation false [get_files -all d:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/fifo_w32xd16/fifo_w32xd16_clocks.xdc]
set_property used_in_implementation false [get_files -all d:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/fifo_w32xd16/fifo_w32xd16_ooc.xdc]

read_ip -quiet d:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/fifo_data_w8xd2048/fifo_data_w8xd2048.xci
set_property used_in_implementation false [get_files -all d:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/fifo_data_w8xd2048/fifo_data_w8xd2048.xdc]
set_property used_in_implementation false [get_files -all d:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/fifo_data_w8xd2048/fifo_data_w8xd2048_ooc.xdc]

read_ip -quiet d:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/fifo_ctrl_w32xd16/fifo_ctrl_w32xd16.xci
set_property used_in_implementation false [get_files -all d:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/fifo_ctrl_w32xd16/fifo_ctrl_w32xd16.xdc]
set_property used_in_implementation false [get_files -all d:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/fifo_ctrl_w32xd16/fifo_ctrl_w32xd16_ooc.xdc]

# Mark all dcp files as not used in implementation to prevent them from being
# stitched into the results of this synthesis run. Any black boxes in the
# design are intentionally left as such for best results. Dcp files will be
# stitched into the design at a later time, either when this synthesis run is
# opened, or when it is stitched into a dependent implementation run.
foreach dcp [get_files -quiet -all -filter file_type=="Design\ Checkpoint"] {
  set_property used_in_implementation false $dcp
}
set_param ips.enableIPCacheLiteLoad 1
close [open __synthesis_is_running__ w]

synth_design -top ethernet_top -part xc7vx485tffg1157-1


# disable binary constraint mode for synth run checkpoints
set_param constraints.enableBinaryConstraints false
write_checkpoint -force -noxdef ethernet_top.dcp
create_report "synth_1_synth_report_utilization_0" "report_utilization -file ethernet_top_utilization_synth.rpt -pb ethernet_top_utilization_synth.pb"
file delete __synthesis_is_running__
close [open __synthesis_is_complete__ w]

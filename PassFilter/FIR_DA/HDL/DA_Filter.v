//宏定义
`define log2    $clog2
module  DA_Filter
#(
    parameter   N_TAPS    = 11,
    parameter   BIT_WIDTH = 12
)(
    input   wire        i_clk_100M,           //时钟，100M
    input   wire        i_rst_n,              //复位，低有效
    input   wire        sample_clk_50M,       //采样时钟，50M
    input   wire signed [BIT_WIDTH-1:0]    fsk_sig,        //FSK信号，有符号数
    output  reg  signed [BIT_WIDTH-1:0]    fsk_filtered    //滤波结果
);

//滤波器系数，阶数10，通带频率10.2M，截止频率14M
    //parameter   A1  = 16'b1111_0100_1011_0000,
    //            A2  = 16'b1110_0001_1001_0000,
    //            A3  = 16'b1110_0001_1000_1010,
    //            A4  = 16'b0000_1111_0010_1110,
    //            A5  = 16'b0101_0110_0010_0000,
    //            A6  = 16'b0111_1001_0100_0000,
    //            A7  = 16'b0101_0110_0010_0000,
    //            A8  = 16'b0000_1111_0010_1110,
    //            A9  = 16'b1110_0001_1000_1010,
    //            A10 = 16'b1110_0001_1001_0000,
    //            A11 = 16'b1111_0100_1011_0000;
/////////////////////////////////////////////////

    //DA_width即ROM查找表中每一项的数据宽度
    localparam  DA_width  = BIT_WIDTH + `log2(N_TAPS) + 1;
    //localparam  DA_width  = BIT_WIDTH + $clog2(N_TAPS);
    localparam  SUM_width = BIT_WIDTH + `log2(BIT_WIDTH) + `log2(N_TAPS);

    //存储器，缓存输入数据
    reg     signed  [N_TAPS-1:0]    delay_pipeline  [BIT_WIDTH-1:0];
    //将ROM查表结果暂存（但不是寄存器类型的）
    wire    signed  [DA_width-1:0]  DA_data         [BIT_WIDTH-1:0];
    //DA结果累加求和
    wire    signed  [SUM_width-1:0] sum_DA          [BIT_WIDTH:0];
    //对其赋初值
    assign  sum_DA[0] = 0;

    integer j;
    generate
        genvar  i;
        for(i=0;i<BIT_WIDTH;i=i+1)begin:BIT_WIDTH_1bit_LUT
            always@(posedge i_clk_100M)begin 
                if(i_rst_n == 1'b0)begin 
                    for(j=0;j<N_TAPS;j=j+1)begin:bit_matrix_intial
                        delay_pipeline[i][j] <= 1'b0;
                    end
                end
                else begin 
                    if(sample_clk_50M == 1'b1)begin 
                        //按位存储每次输入
                        delay_pipeline[i][0] <= fsk_sig[i];
                        for(j=1;j<N_TAPS;j=j+1)begin:bit_matrix
                            delay_pipeline[i][j] <= delay_pipeline[i][j-1];
                        end
                    end
                end
            end
            //DA结果查表
            DA_Table U_DA_Table(
                .address(delay_pipeline[i]), 
                .clock  (   i_clk_100M    ),
                .q      (   DA_data[i]    )    
            );
            //计算求和结果，对输出的DA_data要考虑权值
            assign  sum_DA[i+1] = sum_DA[i] + (DA_data[i] << i);
        end
    endgenerate

    //溢出标志位
    wire    overflow_flag;
    //数据符号位
    wire    sign_flag;
    wire    [SUM_width-1:SUM_width-`log2(BIT_WIDTH)-`log2(N_TAPS)-1] sum_sign;
    
    //取出求和结果的所有符号位，用于判断是否会有溢出
    assign  sum_sign = sum_DA[N_TAPS][SUM_width-1:SUM_width-`log2(BIT_WIDTH)-`log2(N_TAPS)-1];
    //溢出标志位
    assign  overflow_flag = ((~|sum_sign) || (&sum_sign)) ? 1'b0 : 1'b1;
    //符号位
    assign  sign_flag = sum_DA[N_TAPS][SUM_width-1];

    //对输出的滤波器结果要做溢出处理
    always@(posedge i_clk_100M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            fsk_filtered <= 12'b0;
        else if(sample_clk_50M == 1'b1)begin 
            if(overflow_flag == 1'b1)begin      //有溢出
                if(sign_flag == 1'b1)
                    fsk_filtered <= {1'b1,{(BIT_WIDTH-1){1'b0}}};
                else
                    fsk_filtered <= {1'b0,{(BIT_WIDTH-1){1'b1}}};
            end
            else        //未发生溢出
                fsk_filtered <= sum_DA[N_TAPS][SUM_width-1:SUM_width-BIT_WIDTH];
        end
        else
            fsk_filtered <= fsk_filtered;
    end

endmodule
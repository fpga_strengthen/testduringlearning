function [ FSK_data ] = FSK_mod(src_data,freq_0,freq_1,sample_rate,BPS_data)
%本程序完成FSK连续相位调制，返回值为FSK调制信号
%   src_data：以比特向量方式输入的待调制数据
%   freq_0,freq_1：比特0和比特1的频点，单位MHz
%   sample_rate：采样速率，单位MHz
%   BPS_data：数据传输速率，单位KHz

m_f				=	16;				%16倍过采样
phai_current	=	10;				%相位累计
fs				=	sample_rate * m_f;		%过采样频率
SMP_bit			=	fix(fs/BPS_data);%每个码元采样点数目
BPS_data		=	fs / SMP_bit;	%调整后的数据传送速度，单位MHz
DUT_bit			=	1 / BPS_data;	%调整后的每个码元持续时间单位us

src_length		=	length(src_data);%信源数据长度
%% 变量准备
Ph_bit0			=	(DUT_bit * freq_0);	%比特0对应的相位的累计变化，以2*PI进行归一化
Ph_bit1			=	(DUT_bit * freq_1);	%比特1对应的相位的累计变化，以2*PI进行归一化
Ph_vector_0		=	freq_0 * (1:SMP_bit)/fs; 		%附加相位的初始化
Ph_vector_1		=	freq_1 * (1:SMP_bit)/fs; 		%附加相位的初始化

% Ph0		=	zeros(DUT_bit1);	%码元0对应的相位的初始化
% Ph1		=	zeros(DUT_bit1);	%码元1对应的相位的初始化
Ph_inst	  =	zeros(1,src_length);	%实际码元序列的相位的初始化
Ph_inst_r = zeros(1,src_length);    %实际码元序列的累加相位的初始化
phase     = zeros(1,src_length*SMP_bit);

FSK_data  =	zeros(1,fix(src_length*DUT_bit/m_f));%相位转化为余弦值

%% 构造相位向量Ph0、Ph1
Ph_inst = (1 - src_data) * Ph_bit0 +  src_data * Ph_bit1;  %每个数据比特产生的相位偏移
Ph_inst = [0,Ph_inst(1:end-1)];                            %初始相位为0
Ph_inst_r = zeros(1,src_length);

phase_deviation = 180/360;%01切换相位偏移  1-->2pi

%计算累计相位
for idx_n = 2:src_length
    
    %相位连续
        Ph_inst_r(idx_n) = sum(Ph_inst(1:idx_n)); %此处可对pi做归一化处理
    
    %相位不连续
%     if((src_data(idx_n) == 1) && (src_data(idx_n -1) == 0))
%         Ph_inst_r(idx_n) = Ph_inst_r(idx_n - 1) + Ph_inst(idx_n) - phase_deviation;
%     elseif((src_data(idx_n) == 0) && (src_data(idx_n -1) == 1))
%         Ph_inst_r(idx_n) = Ph_inst_r(idx_n - 1) + Ph_inst(idx_n) + phase_deviation;
%     else
%         Ph_inst_r(idx_n) = Ph_inst_r(idx_n - 1) + Ph_inst(idx_n);
%     end %if
    
end %for


%% 构造相位折线
phase =		kron(1-src_data, Ph_vector_0)...
    +	kron(src_data, Ph_vector_1)...
    +	kron(Ph_inst_r,ones(1,SMP_bit));

% figure(10),
% plot(unwrap(downsample(phase,m_f)));
% legend('2pi修正并下采样后的相位折线');

% phase_dsmpl=downsample(phase,m_f); %下采样恢复为ADC采样速率
FSK_data = 2047*(1+cos(downsample(phase,m_f)*2*pi));

end


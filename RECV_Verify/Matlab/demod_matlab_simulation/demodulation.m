%FSK非相干解调判决仿真

function [demodulation_result] = demodulation(fsk_data)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%FSK调制%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
f0_true = 3.951 ;
f1_true = 4.516 ;         %比特0和比特1的频点，单位MHz
sample_rate_true = 27.095;%采样速率，单位MHz
BPS_data_true = 0.56448;  %数据传输速率，单位MHz

%%%%%%%%%  %%%%%%%%%%%%%
src_data = fsk_data;

static_lenth_x_axis = length(src_data);%循环一次，FSK数据的长度
x_axis = zeros(1,static_lenth_x_axis);%x轴

%本地载波
S0 = cos( f0_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);
S1 = cos( f1_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);
S0_Q = sin(f0_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);%%移相90°
S1_Q = sin(f1_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);%%移相90°

%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%相关解调%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
value_cos0 = src_data .* S0;
value_sin0 = src_data .* S0_Q;
value_cos1 = src_data .* S1;
value_sin1 = src_data .* S1_Q;

% 相干解调过程,48点求和，将0,1求和结果对应相减得到判决数据
N = length(src_data);
for i =1:N-48
    demodulation_result(i) = ( sum(value_cos0(i:i+48)) )^2 + ( sum(value_sin0(i:i+48)) )^2 - ( sum(value_cos1(i:i+48)) )^2 - ( sum(value_sin1(i:i+48)) )^2 ;
end%for

for i=1:N-48
    if(demodulation_result(i)>0)
        demodulation_result(i) = 1;
    elseif(demodulation_result(i)<=0)
        demodulation_result(i) = 0;
    end %if
end %for

end %%funtion

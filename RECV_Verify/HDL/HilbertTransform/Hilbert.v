module Hilbert
#(
    parameter   PIPE_DEPTH = 12
)(
    input   wire        i_clk_108M,
    input   wire        i_clk_27M_en,
    input   wire        i_rst_n,
    input   wire [11:0] iv_fsk_sig,
    
    output  wire [12:0] ov_fsk_im,
    output  wire [12:0] ov_fsk_re
);

    /*
        滤波器系数：Hilbert滤波器系数为0和非零相间隔，且在频域具有对称性。
        该滤波器阶数12，采用FIR滤波器结构实现，系数满足h(-k)=-h(k),k=1,3,5.
        正频率部分的系数分别为h(1)=0.63662,h(3)=0.212207,h(5)=0.127324.
        量化为17位，并考虑符号位，设置系数字长18位。
    */
    localparam  H1 = 18'd83443, H3 = 18'd27814, H5 = 18'd16688;
    
    /*
        经验证，使用12位足以表示fsk数据和2048作差后的有符号数结果。
        输入FSK的最大值(3995)和最小值(101)减去2^12，结果是1947和-1947.
        正数部分使用12位可以表示，最高位为符号位；
        负数-1947的原码为12'b1111_1001_1011,计算出补码为12'b1000_0110_0101，未发生溢出.
    */
    // 存储器缓存数据打拍
    reg signed  [12:0]  data_pipeline   [PIPE_DEPTH-1:0];
    
    // 循环变量控制缓存
    integer i;

    always@(posedge i_clk_108M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)begin
            for(i=0;i<PIPE_DEPTH;i=i+1)
                data_pipeline[i] <= 13'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin 
            data_pipeline[0] <= iv_fsk_sig - 12'd2048;
            for(i=1;i<PIPE_DEPTH;i=i+1)
                data_pipeline[i] <= data_pipeline[i-1];
        end
        else begin 
            for(i=0;i<PIPE_DEPTH;i=i+1)
                data_pipeline[i] <= data_pipeline[i];
        end
    end

    // 数据相加，字长需要扩展1位
    reg signed  [13:0]  temp_sum1, temp_sum2, temp_sum3;
    
    always@(posedge i_clk_108M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)begin 
            temp_sum1 <= 13'd0;
            temp_sum2 <= 13'd0;
            temp_sum3 <= 13'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin 
            temp_sum1 <= data_pipeline[5] - data_pipeline[7];
            temp_sum2 <= data_pipeline[3] - data_pipeline[9];
            temp_sum3 <= data_pipeline[1] - data_pipeline[PIPE_DEPTH-1];
        end
        else begin 
            temp_sum1 <= temp_sum1;
            temp_sum2 <= temp_sum2;
            temp_sum3 <= temp_sum3;
        end
    end

    wire signed [31:0] mult_result_1, mult_result_3, mult_result_5;

    // 与滤波器系数作乘法
    mult_signed_hilbert U_mult_1(
	    .dataa (temp_sum1),
	    .datab (H1),
	    .result(mult_result_1)
    );

    mult_signed_hilbert U_mult_2(
	    .dataa (temp_sum2),
	    .datab (H3),
	    .result(mult_result_3)
    );

    mult_signed_hilbert U_mult_3(
	    .dataa (temp_sum3),
	    .datab (H5),
	    .result(mult_result_5)
    );

    // 将乘法结果求和，得到Hilbert变换后解析信号的虚部
    reg signed  [34:0]  sum_mult;

    always@(posedge i_clk_108M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            sum_mult <= 34'd0;
        else if(i_clk_27M_en == 1'b1)
            sum_mult <= mult_result_1 + mult_result_3 + mult_result_5;
        else
            sum_mult <= sum_mult;
    end

    /*
        数据溢出的判断：
        乘法器结果输出的数据位测试结果最多是[28:0]，高三位为符号位；
        所以在乘法结果求和之后扩展的3bit，继续顺延这个特点，除去最高的符号位之后额外判断剩余2位的符号位以判断是否溢出。
    */
    // 将求和结果截断到13位
    reg signed  [12:0] im_data;

    always@(posedge i_clk_108M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            im_data <= 13'd0;
        else if(i_clk_27M_en == 1'b1)begin 
            if(sum_mult[34] == 1'b1)begin               // 负数
                if(sum_mult[33:29] == 5'b11_111)        // 未发生溢出,未影响到符号位
                    im_data <= {sum_mult[34],sum_mult[28:17]};
                else                                    // 溢出按照下溢处理
                    im_data <= {sum_mult[34],{12{1'b0}}};
            end
            else begin                                  // 正数
                if(sum_mult[33:29] == 5'b00_000)        // 未发生溢出
                    im_data <= {sum_mult[34],sum_mult[28:17]};
                else                                    // 溢出按照下溢处理
                    im_data <= {sum_mult[34],{12{1'b1}}};
            end
        end
        else
            im_data <= im_data;
    end

    //reg signed  [12:0]  re_data;
    // 实部
    //always@(posedge i_clk_108M or negedge i_rst_n)begin 
    //    if(i_rst_n == 1'b0)
    //        re_data <= 13'd0;
    //    else //if(i_clk_27M_en == 1'b1)
    //        re_data <= data_pipeline[9];
    //        //re_data <= {data_pipeline[11][12],data_pipeline[11]};
    //    //else
    //    //    re_data <= re_data;
    //end

    assign  ov_fsk_im = im_data;
    //assign  ov_fsk_re = re_data;
    assign  ov_fsk_re = data_pipeline[11];
    
endmodule


%%           解调算法设计中的中间版本，已舍弃          


%FSK解调判决仿真
%目的检测频偏对判决错误的影响

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%FSK调制%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc
format long
close all
clear all

%% %标准条件下主要参数
f0_true = 3.951 ;
f1_true = 4.516 ;         %比特0和比特1的频点，单位MHz
f_mid =4.234; % (3.951+4.516) / 2
sample_rate_true = 27.095;% 标准采样速率，单位MHz
BPS_data_true = 0.56448;  % 标准数据传输速率，单位MHz
Hd = LP_6M;

%%%%%%%%% 产生相位连续的FSK调制信号进行ADC采样后作为输入 %%%%%%%%%%%%%
src_data =randint(1,100);%以比特向量方式输入的待调制数据，随机产生

%%%%%%%%% FSK数据的关键参数 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%通过控制FSK的参数产生带有偏差数据源
%A点
f0 = f_mid - 0.282 ;%
f1 = f_mid + 0.282;%比特0和比特1的频点，单位MHz
% %B点
% f0 = f_mid - 0.477 ;%
% f1 = f_mid + 0.127;%比特0和比特1的频点，单位MHz

% %C点
% f0 = f_mid - 0.43749 ;%
% f1 = f_mid + 0.0879;%比特0和比特1的频点，单位MHz

% %D点
% f0 = f_mid - 0.0879 ;%
% f1 = f_mid + 0.43749;%比特0和比特1的频点，单位MHz

sample_rate =  sample_rate_true;%采样速率，单位MHz
BPS_data = BPS_data_true;%数据传输速率，单位MHz
FSK_data  = FSK_mod(src_data,f0,f1,sample_rate,BPS_data);

%对输入数据进行定标
FSK_data = FSK_data  - 2048;%FixPointNum(FSK_data  - 2048, 12, 0);%字长12bit，精度0位小数 + FSK_data_noise
length_fsk_data = length(FSK_data);

% 构造本地载波
static_lenth_x_axis = length_fsk_data;%本地载波长度
S0 = cos( f0_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);
S1 = cos( f1_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);
S0_Q = sin(f0_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);%%移相90°
S1_Q = sin(f1_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);%%移相90°


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%相关解调%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
value_cos0 = FSK_data .* S0;
value_sin0 = FSK_data .* S0_Q;
value_cos1 = FSK_data .* S1;
value_sin1 = FSK_data .* S1_Q;

%%滤除高频信号
value_cos0 = filter(Hd,FSK_data .* S0);
value_sin0 = filter(Hd,FSK_data .* S0_Q);
value_cos1 = filter(Hd,FSK_data .* S1);
value_sin1 = filter(Hd,FSK_data .* S1_Q);
for i =1:length_fsk_data-48 %相干解调过程,24个点求和，再除以24
    sum_forward_24_point(i) =( (sum(value_cos0(i:i+23)))^2 + (sum(value_sin0(i:i+23)))^2 )./24  ;
    sum_after_24_point(i) =( (sum(value_cos0(i+24:i+47)))^2 + (sum(value_sin0(i+24:i+47)))^2 )./24 ;
end
sum_forward_mins_sum_after = (sum_forward_24_point - sum_after_24_point);

for i =1:length_fsk_data-48 %相干解调过程,48点求和，将0,1求和结果对应相减得到判决数据
    bit0(i) = ( sum(value_cos0(i:i+47)) )^2 + ( sum(value_sin0(i:i+47)) )^2  ;
    bit1(i) = ( sum(value_cos1(i:i+47)) )^2 + ( sum(value_sin1(i:i+47)) )^2  ;
end

%% 消除Ws的影响 平方后累加法
% for i =1:length_fsk_data-48 %相干解调过程,24个点求和，再除以24
%     sum_forward_24_point(i) = sum( value_cos0(i:i+23).^2)    -  sum(value_sin0(i:i+23).^2)   ;
%     sum_after_24_point(i) = sum( value_cos0(i+24:i+47).^2)   -  sum(value_sin0(i+24:i+47).^2) ;
% end
% sum_forward_mins_sum_after = (sum_forward_24_point - sum_after_24_point);

figure(1),
hold off,
plot(sum_forward_24_point,'r');
hold on,
plot(sum_after_24_point,'g');
plot(sum_forward_mins_sum_after);

%% 消除Ws的影响 hilbert法
%将与本地载波（cos路）相乘后的结果进行希尔伯特变换（只考虑2路）
% hilbert_value_cos0 = hilbert(value_cos0);  %求出理论上的解析信号

 %用实验室方法求希尔伯特变换
[ im_datain_solve ] = hilbert_trans( value_cos0 )
% im_datain 有N/2的时延，N=11；前N个和最后N个数据无效
% 数据对齐
Length_data = length(im_datain_solve);
im_datain = im_datain_solve(6:Length_data);
value_cos0 = value_cos0(1:Length_data-5);

figure(3),
plot(value_cos0),
hold on,
plot(im_datain,'r');

% 将与（cos路）相乘后的结果做希尔伯特变换后与（sin路）相乘后的结果相加
sum_value0_after_hilbert = im_datain + value_sin0(1:Length_data-5);

%将希尔伯特处理后的结果24点相加，消除高频分量
for i =1:length_fsk_data-48 %相干解调过程,24个点求和，再除以24
    sum_value0_after_hilbert(i) =( sum(sum_value0_after_hilbert(i:i+23)) )  ;
end
figure(2),
plot(sum_value0_after_hilbert);


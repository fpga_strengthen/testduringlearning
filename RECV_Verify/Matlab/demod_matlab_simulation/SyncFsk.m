%% 拐点求出后，根据拐点来修正判决时刻，以提高判决时刻的准确性，降低误码率。
function [decision_time,sync_plot,flag_47] =  SyncFsk( sync_point )
    %-------------------------------------------------------------------------------------------
    % reg [3:0]cnt_fit_dge;   //统计正确跳变沿的个数
    % reg [1:0]sync;          //同步状态机
    % reg [3:0]cnt_same;        //解调结果中有连续相同的bit个数
    % reg [6:0]cnt_48;          //判断跳变点之间的点数是否为48的倍数
    % reg sign_long;            //一个采样计数周期内，计数时刻对应周期的后半部分，为了防止将相隔几个采样点的跳变点当做同步信号的预防处理
    % reg [4:0]cnt_bit_num;     //统计两个相邻拐点之间有多少个bit
    % reg bit_long;             //相邻两个拐点之间存在多个bit数据，也即有漏掉拐点
    % reg [6:0]det_sample;      //拐点时刻对应的采样点有可能落在上一个采样周期的后部，也有可能落在下一个周期的前部，统一计算与一个采样周期的差值，
    %                            //也即绝对值处理。如....45、46、47、0、1、2、3...，如果采样时刻是45，或者2，与理论拐点的差值都是2
    % reg no_signal_flag;        //连续多次没有检测到跳变点，则认为没有信号到达，用来加快同步状态下的失步处理
    
    i_hilbert_dge = sync_point;
    %-----------------------------------parameter------------------------------------------------
    sync_window_max      = 16;     %同步窗的最大范围
    length_window1       = 3;      %第0级同步窗，中心点和采样周期都不进行修正
    length_window2       = 6;      %第1级同步窗，中心点修正1，采样周期修正1,
    length_window3       = 9;      %第2级同步窗，中心点修正2，采样周期修正2
    length_window4       = 13;     %第3级同步窗，中心点修正2，采样周期修正2
    
    half_T               = 24;     %一个bit对应采样周期的一半，一个周期为64
    max_cnt_fit_dge      = 8;      %连续8个正确的同步点表明完全同步
    T_modify             = 47;     %一个周期对应64个采样点，计数器范围为：[0~63]
    
    sync =0;
    cnt_fit_dge = 0;
    cnt_48 = 0;
    sign_long = 0;
    cnt_bit_num = 0;
    %-------------------------------------------------------------------------------------------
    for i = 1:length(i_hilbert_dge)
        %cnt_fit_dge可信度修正
        %状态机
        if( sync ==0 )
            if ( i_hilbert_dge(i) == 1 )
                cnt_fit_dge = 1;
            else
                cnt_fit_dge = cnt_fit_dge;
            end
            
            if ( cnt_fit_dge == 1 )
                sync = 1;%在未出现相同的11个bit或者连续25个bit无拐点的条件下，来了一个拐点，就跳到下一状态
            else
                sync = 0;
            end
            
        elseif( sync ==1 )%伪同步状态
            if ( i_hilbert_dge(i) == 1 )%在伪同步状态，来了一个拐点
                %cnt_fit_dge可看做可信度度量，当跳变点符合规范时，可信度增加，否则减少
                if( det_sample <= sync_window_max )
                    %跳变点落在同步窗内
                    cnt_fit_dge = cnt_fit_dge + 1;
                else%否则认为跳变沿不正确
                    cnt_fit_dge = cnt_fit_dge - 1;
                end
            else
                cnt_fit_dge = cnt_fit_dge;
            end
            
            if ( cnt_fit_dge == max_cnt_fit_dge )%??8??????????FSK??,????
                sync = 2;%当可信度增加到8时，则可以认为完全同步，跳到下一状态
            elseif ( cnt_fit_dge == 0 )
                sync = 0;%若可信度减为0，则跳回初始状态
            else
                sync = 1;
            end
            
        elseif( sync ==2 )%同步状态
            if ( i_hilbert_dge(i) == 1 )
                if( det_sample <= sync_window_max)%跳变点落在同步窗内
                    %跳变点落在同步窗内
                    if ( cnt_fit_dge >= max_cnt_fit_dge)
                        cnt_fit_dge = cnt_fit_dge;%如果同步拐点计数达到上限，则不再增加，这样方便退出同步状态
                    else
                        cnt_fit_dge = cnt_fit_dge + 1;
                    end
                else%否则认为跳变沿不正确
                    cnt_fit_dge = cnt_fit_dge - 1;
                end
            else
                cnt_fit_dge = cnt_fit_dge;
            end
            
            if ( cnt_fit_dge == 0 )%???????????0??FSK???
                sync = 0;%如果可信度减为0，则跳回初始状态，否则维持同步状态
            else
                sync = 2;
            end
            
        else
            cnt_fit_dge = cnt_fit_dge;
            sync = sync;
        end %if sync
        sync_plot(i) = sync;
        %同步窗修正
        if ( sync ==0 )%捕捉状态，只要检测到开始有拐点，将计数器清0，准备开始周期计数
            if ( i_hilbert_dge(i) == 1 )
                cnt_48 = 0;
            elseif ( cnt_48 == 47 )
                cnt_48 = 0;
            else
                cnt_48 = cnt_48 + 1 ;
            end
        elseif ( sync ==1 || sync ==2)
            if ( i_hilbert_dge(i) == 1 )%在拐点到达时
                if(det_sample <= sync_window_max)%判断同步点是否落在同步窗内  mark.&& i_slope_right == 1'b1
                    if(cnt_48 >= T_modify)%超过了一个bit
                        cnt_48 = cnt_48 - T_modify;%减掉一个bit的48个点
                    elseif(cnt_48 >= half_T)   %在一个bit的后半部分 采样周期偏小  //是否需要改成case语句，无先后顺序，减少相应的时延
                        %对同步窗进行修正，偏移的距离越大，需要修正的越多
                        if(det_sample >= length_window4)     %采样点落在区间[-16,-12]
                            cnt_48 = cnt_48 + 14;  %同步窗中心点往左偏移9个采样点
                        elseif(det_sample >= length_window3)%采样点落在区间[-16,-12]
                            cnt_48 = cnt_48 + 10;  %同步窗中心点往左偏移9个采样点
                        elseif(det_sample >= length_window2)%采样点落在区间[-12,-8]
                            cnt_48 = cnt_48 + 7;  %同步窗中心点往左偏移7个采样点
                        elseif(det_sample >= length_window1)%采样点落在区间[-8,-4]
                            cnt_48 = cnt_48 + 4;  %同步窗中心点往左偏移4个采样点
                        else
                            cnt_48 = 0;    %同步窗中心点不修正
                        end
                    elseif(cnt_48 <= half_T)  %在一个bit的后半部分 采样周期偏大
                        if(det_sample >= length_window4)     %采样点落在区间[-16,-12]
                            cnt_48 = cnt_48 - 14;  %同步窗中心点往左偏移9个采样点
                        elseif(det_sample >= length_window3)     %采样点落在区间[-16,-12]
                            cnt_48 = cnt_48 - 10;  %同步窗中心点往左偏移9个采样点
                        elseif(det_sample >= length_window2)%采样点落在区间[-12,-8]
                            cnt_48 = cnt_48 - 7;  %同步窗中心点往左偏移7个采样点
                        elseif(det_sample >= length_window1)%采样点落在区间[-8,-4]
                            cnt_48 = cnt_48 - 4;  %同步窗中心点往左偏移4个采样点
                        else
                            cnt_48 = 0;
                        end
                    else
                        cnt_48 = cnt_48;
                    end
                else  %同步点落在同步窗外
                    if(cnt_48 >= T_modify)%大于一个bit的48个点
                        cnt_48 = cnt_48 - T_modify;
                    else
                        cnt_48 = cnt_48 + 1;
                    end
                end
            elseif(cnt_48 >= T_modify)
                cnt_48 = cnt_48 - T_modify;
            else
                cnt_48 = cnt_48 + 1;
            end
        end
        %与拐点的距离
        if(cnt_48 <= half_T)
            det_sample = cnt_48;
        else
            det_sample = T_modify - cnt_48;
        end
        
        if ( cnt_48 == half_T && sync ~=0 )%当计数到24个点时拉高
            decision_time(i) = 1;
        else
            decision_time(i) = 0;
        end
        
        if ( cnt_48 == 47)%当计数到24个点时拉高
            flag_47(i) = 1;
        else
            flag_47(i) = 0;
        end
        
        if ( cnt_48 == half_T )%当计数到24个点时拉高
            sign_long = 1;
        elseif ( i_hilbert_dge(i) == 1 && (det_sample <= sync_window_max) && sign_long == 1 )
            sign_long = 0;%来了一个拐点，且跳变点落在同步窗内时，拉低
        else
            sign_long = sign_long;
        end
        
        % 计数两个拐点间有多少个bit
        if ( i_hilbert_dge(i) == 1 && (det_sample <= sync_window_max)  )
            cnt_bit_num = 0;%来了一个拐点，且落在同步窗内
        elseif ( cnt_bit_num == 31 )%计数到31则保持
            cnt_bit_num = cnt_bit_num;
        elseif ( cnt_48 == half_T )%每在一次bit中点，加一个bit
            cnt_bit_num = cnt_bit_num + 1;
        else
            cnt_bit_num = cnt_bit_num;
        end
        
        %当两个拐点间的bit数大于一定范围，则不符合编码规范，拉高错误指示
        if ( sync==0 )
            if( cnt_bit_num >= 10 )
                bit_long = 1;
            else
                bit_long = 0;
            end
        elseif ( sync==1 )
            if( cnt_bit_num >= 10 )
                bit_long = 1;
            else
                bit_long = 0;
            end
        elseif ( sync==2 )
            if( cnt_bit_num >= 25 )
                bit_long = 1;
            else
                bit_long = 0;
            end
        end
        if ( sync==2 )
            if( cnt_bit_num >= 20 )%当两拐点间的bit数大于20个，则判定此时没有数据
                no_signal_flag = 1;
            else
                no_signal_flag = 0;
            end
        end
        
    end %for
    end
//-------------------------------------------------------------------------------------------------                                                                             
//                                                                                                                                                                              
//--- ALL RIGHTS RESERVED
//--- File      : Phase_calculation.v 
//--- Auther    : 
//--- Date      : 
//--- Version   : v1.3.2.1
//--- Abstract  : 求出FSK信号的相位（0~2pi）
//
//-------------------------------------------------------------------------------------------------                                                                             
//                                                                                                                                                                              
// Description : 对输入信号进行希尔伯特变化，求出解析信号,求出相位                                                                                                                        
//                                                                                                                                                                              
//-------------------------------------------------------------------------------------------------                                                                             
//--- Modification History:
//--- Date          By          Version         Change Description 
//-------------------------------------------------------------------
//--- 2017.05.11                v.1.3.2.1                 
//--------------------------------------------------------------------
module Phase_calculation(
          i_rst_n,         //输入复位信号，低电平有效
          i_clk_81M,       //输入时钟信号，81M
          i_clk_27M_en,    //输入时钟使能，27M
          i_data,          //输入数据，用来进行希尔伯特滤波

//          o_flag_noise,    //ADC数据为噪声信号指示，用于控制点亮A排灯的条件之一，高电平时为有噪声
          o_angle_tan      //相位值（0-2pi）
          );

input i_rst_n;
input i_clk_81M;
input i_clk_27M_en;
input [11:0]i_data;

//output o_flag_noise;
output [13:0]o_angle_tan;

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//-------------------------------参数声明-------------------------

//希尔伯特滤波器参数
parameter H1   = 18'd83443;
parameter H3   = 18'd27814;
parameter H5   = 18'd16688;

//-------------------------------信号声明-------------------------
//将输入信号做成有符号数，用于希尔伯特滤波器的实现。输入数据是12位，此组寄存器定义为13位，最高位为符号位
reg signed[12:0]hilbert_datain_delay_5_n;
reg signed[12:0]hilbert_datain_delay_4_n;
reg signed[12:0]hilbert_datain_delay_3_n;
reg signed[12:0]hilbert_datain_delay_2_n;
reg signed[12:0]hilbert_datain_delay_1_n;
reg signed[12:0]hilbert_datain_delay_0;
reg signed[12:0]hilbert_datain_delay_1;
reg signed[12:0]hilbert_datain_delay_2;
reg signed[12:0]hilbert_datain_delay_3;
reg signed[12:0]hilbert_datain_delay_4;
reg signed[12:0]hilbert_datain_delay_5;

//希尔伯特滤波器的减法计算的结果
reg signed[13:0] data_add_1;
reg signed[13:0] data_add_3;
reg signed[13:0] data_add_5;

//希尔伯特滤波器的乘法计算的结果
wire signed[31:0] data_H1;
wire signed[31:0] data_H3;
wire signed[31:0] data_H5;

//希尔伯特滤波器的乘法计算的结果打一拍
reg signed[31:0] data_H1_r;
reg signed[31:0] data_H3_r;
reg signed[31:0] data_H5_r;

//希尔伯特滤波器的结果
reg signed[33:0]im_datain_solve;      

//希尔伯特滤波器的结果截短到13位
reg signed[12:0]im_datain;            

//---------通过希尔伯特变换进行幅值检测---------
reg [12:0] re_abs;
reg [12:0] im_abs;
reg [12:0] max_IQ;
reg [12:0] min_IQ;
reg [13:0]amplitude;
reg [13:0]amplitude_delay1;
reg [13:0]amplitude_delay2;
reg [13:0]amplitude_delay3;
reg [15:0]amplitude_data;
reg flag_noise;

//---------通过希尔伯特变换求arctan值---------
//相位归一化
reg [12:0]tan_numer;
reg [12:0]tan_denom;
reg [3:0]quadrant;

reg [3:0]quadrant_delay;              //对象限进行延时

wire [20:0]tan_quotient;              //第一象限的角度值

reg [13:0]angle_tan;                  //还原出的相位值（0-8192）

wire [13:0]o_angle_tan;

//------------------------------hilbert--------------------------
//将输入信号做成有符号数并延时
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      begin
        /*
        hilbert_datain_delay_5_n <= 13'd0;//mark0518 位数应是12位？
        hilbert_datain_delay_4_n <= 13'd0;
        hilbert_datain_delay_3_n <= 13'd0;
        hilbert_datain_delay_2_n <= 13'd0;
        hilbert_datain_delay_1_n <= 13'd0;
        hilbert_datain_delay_0   <= 13'd0;
        hilbert_datain_delay_1   <= 13'd0;
        hilbert_datain_delay_2   <= 13'd0;
        hilbert_datain_delay_3   <= 13'd0;
        hilbert_datain_delay_4   <= 13'd0;
        hilbert_datain_delay_5   <= 13'd0;
        */
        hilbert_datain_delay_5_n <= 13'd1;//mark0518 位数应是12位？
        hilbert_datain_delay_4_n <= 13'd1;
        hilbert_datain_delay_3_n <= 13'd1;
        hilbert_datain_delay_2_n <= 13'd1;
        hilbert_datain_delay_1_n <= 13'd1;
        hilbert_datain_delay_0   <= 13'd1;
        hilbert_datain_delay_1   <= 13'd1;
        hilbert_datain_delay_2   <= 13'd1;
        hilbert_datain_delay_3   <= 13'd1;
        hilbert_datain_delay_4   <= 13'd1;
        hilbert_datain_delay_5   <= 13'd1;
      end
    else if( i_clk_27M_en ) //27M
      begin
        hilbert_datain_delay_5_n <= i_data - 13'd2048;
        hilbert_datain_delay_4_n <= hilbert_datain_delay_5_n;
        hilbert_datain_delay_3_n <= hilbert_datain_delay_4_n;
        hilbert_datain_delay_2_n <= hilbert_datain_delay_3_n;
        hilbert_datain_delay_1_n <= hilbert_datain_delay_2_n;
        hilbert_datain_delay_0   <= hilbert_datain_delay_1_n;
        hilbert_datain_delay_1   <= hilbert_datain_delay_0;
        hilbert_datain_delay_2   <= hilbert_datain_delay_1;
        hilbert_datain_delay_3   <= hilbert_datain_delay_2;
        hilbert_datain_delay_4   <= hilbert_datain_delay_3;
        hilbert_datain_delay_5   <= hilbert_datain_delay_4;
      end
    else
      begin
        hilbert_datain_delay_5_n <= hilbert_datain_delay_5_n;
        hilbert_datain_delay_4_n <= hilbert_datain_delay_4_n;
        hilbert_datain_delay_3_n <= hilbert_datain_delay_3_n;
        hilbert_datain_delay_2_n <= hilbert_datain_delay_2_n;
        hilbert_datain_delay_1_n <= hilbert_datain_delay_1_n;
        hilbert_datain_delay_0   <= hilbert_datain_delay_0;
        hilbert_datain_delay_1   <= hilbert_datain_delay_1;
        hilbert_datain_delay_2   <= hilbert_datain_delay_2;
        hilbert_datain_delay_3   <= hilbert_datain_delay_3;
        hilbert_datain_delay_4   <= hilbert_datain_delay_4;
        hilbert_datain_delay_5   <= hilbert_datain_delay_5;
      end
  end

//将对应位相减，具体的计算公式见相关文档
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      begin
        data_add_1 <= 14'd0;
        data_add_3 <= 14'd0;
        data_add_5 <= 14'd0;
      end
    else if( i_clk_27M_en ) //27M
      begin
        data_add_1 <= hilbert_datain_delay_1 - hilbert_datain_delay_1_n;
        data_add_3 <= hilbert_datain_delay_3 - hilbert_datain_delay_3_n;
        data_add_5 <= hilbert_datain_delay_5 - hilbert_datain_delay_5_n;    
      end
    else
      begin
        data_add_1 <= data_add_1;
        data_add_3 <= data_add_3;
        data_add_5 <= data_add_5;
      end
  end
    
//相减结果与希尔伯特滤波器的参数相乘
mult_signed_hilbert mult_H1(
            .dataa( H1 ),
            .datab( data_add_1 ),
            .result( data_H1 )
            );

mult_signed_hilbert mult_H3(
            .dataa( H3 ),
            .datab( data_add_3 ),
            .result( data_H3 )
            );

mult_signed_hilbert mult_H5(
            .dataa( H5 ),
            .datab( data_add_5 ),
            .result( data_H5 )
            );

//将乘法器得到的结果延时一个单位
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )  
      begin
        data_H5_r <= 32'd0;
        data_H3_r <= 32'd0;
        data_H1_r <= 32'd0;
      end
    else if( i_clk_27M_en ) //27M
      begin
        data_H1_r <= data_H1;      
        data_H3_r <= data_H3;
        data_H5_r <= data_H5;
      end
    else
      begin
        data_H1_r <= data_H1_r;      
        data_H3_r <= data_H3_r;
        data_H5_r <= data_H5_r;
      end
  end

//希尔伯特滤波器的结果，具体的计算公式见相关文档
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      im_datain_solve <= 34'd0;
    else if( i_clk_27M_en ) //27M
      im_datain_solve <= data_H1_r + data_H3_r + data_H5_r;
    else
      im_datain_solve <= im_datain_solve;
  end

//将希尔伯特滤波器的结果截短到13位
always @ ( negedge i_rst_n or posedge i_clk_81M)//81M
begin
  if( !i_rst_n )
    im_datain <= 13'b0;    //Mark0518 仿真看截短后仍有两个符号位
  else if( i_clk_27M_en ) //27M
    begin
      if( im_datain_solve[33] == 1'b1 )
        begin
          if( ( im_datain_solve[33] && im_datain_solve[32] && im_datain_solve[31] && im_datain_solve[30] && im_datain_solve[29] ) == 1'b1 )
            im_datain <= { im_datain_solve[33] , im_datain_solve[28:17] };
          else
            im_datain <= { im_datain_solve[33] , { 12{1'b0} } };
        end
      else if( im_datain_solve[33] == 1'b0 )
        begin
          if( ( im_datain_solve[33] || im_datain_solve[32] || im_datain_solve[31] || im_datain_solve[30] || im_datain_solve[29] ) == 1'b0 )
            im_datain <= { im_datain_solve[33] , im_datain_solve[28:17] };
          else
            im_datain <= { im_datain_solve[33],{ 12{1'b1} } };
        end
      else
        im_datain <= im_datain;
    end
  else
    im_datain <= im_datain;
end

//---------通过希尔伯特变换的结果进行幅值检测滤除不符合要求的干扰信号---------
//求绝对值
always @ ( negedge i_rst_n or posedge i_clk_81M)
  begin
    if( !i_rst_n )
      re_abs <= 13'd0;
    else if( i_clk_27M_en ) //27M
      begin
        if(im_datain[12]==1'd1)   //负数
          re_abs <= ~(im_datain - 1'd1);
        else                      //结果为正
          re_abs <= im_datain;
      end
    else
      re_abs <= re_abs;
  end

always @ ( negedge i_rst_n or posedge i_clk_81M)
  begin
    if( !i_rst_n )
      im_abs <= 13'd0;
    else if( i_clk_27M_en ) //27M
      begin
        if(hilbert_datain_delay_4[12]==1'd1)//负数
          im_abs <= ~(hilbert_datain_delay_4 - 1'd1);
        else                                //结果为正
          im_abs <= hilbert_datain_delay_4;
      end
    else
      im_abs <= im_abs;  
  end                    
  
//比较大小
always @ ( negedge i_rst_n or posedge i_clk_81M)
  begin
    if( !i_rst_n )
      begin
        max_IQ <= 13'b0;
        min_IQ <= 13'b0;
      end
    else if( i_clk_27M_en ) //27M
      begin
        if( re_abs >= im_abs )begin    
          max_IQ <= re_abs;
          min_IQ <= im_abs;
        end
        else begin
          max_IQ <= im_abs;
          min_IQ <= re_abs;
        end 
      end
    else
      begin
          max_IQ <= max_IQ;
          min_IQ <= min_IQ;
      end
  end

 //幅值公式: amplitude=max（I，Q）+ min（I，Q）/2
always @(negedge i_rst_n or posedge i_clk_81M)
  begin
    if (!i_rst_n)
        amplitude <= 14'b0;
    else if( i_clk_27M_en ) //27M
        amplitude <= max_IQ + min_IQ[12:1];//理论值在0~2047
    else
        amplitude <= amplitude;
  end //always

//打拍
always @(negedge i_rst_n or posedge i_clk_81M) 
  begin
    if (!i_rst_n)
      begin 
      amplitude_delay1 <= 14'b0;
      amplitude_delay2 <= 14'b0;
      amplitude_delay3 <= 14'b0;
      end
    else if( i_clk_27M_en ) //27M
      begin
        amplitude_delay1 <= amplitude;
        amplitude_delay2 <= amplitude_delay1;
        amplitude_delay3 <= amplitude_delay2;
      end
    else
      begin
        amplitude_delay1 <= amplitude_delay1;
        amplitude_delay2 <= amplitude_delay2;
        amplitude_delay3 <= amplitude_delay3;
      end
  end //always

//取平均来平滑
always @(negedge i_rst_n or posedge i_clk_81M)
  begin
    if (!i_rst_n)
        amplitude_data <= 16'b0;
    else if( i_clk_27M_en ) //27M
        amplitude_data <= amplitude + amplitude_delay1 + amplitude_delay2 + amplitude_delay3 ;
    else
        amplitude_data <= amplitude_data;
  end //always

//通过判断amplitude_data的范围是否符合FSK幅值的大小，来过滤不符合要求的噪声信号
always @(negedge i_rst_n or posedge i_clk_81M)
  begin
    if (!i_rst_n)
        flag_noise <= 1'b0;
    else if( i_clk_27M_en ) //27M
      begin
        if(amplitude_data[15:2] <= 14'd300 )
          flag_noise <= 1'b1;
        else
          flag_noise <= 1'b0;
      end
    else
        flag_noise <= flag_noise;
  end //always

//assign o_flag_noise = flag_noise;


/*------通过希尔伯特变化，求出信号的实部与虚部，通过实部与虚部的关系，利用查表法，得到信号的相位值
通过希尔伯特变化求得的实部与虚部的值，求出的相位可能处于任一象限，为了简化查表法，将求得的相位都
归一化到第一象限，分为第一象限的上下部分和角平分线三种情况，再通过象限值将第一象限的角度还原其真实值*/
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      begin
        tan_numer <= 13'd0;           //实部
        tan_denom <= 13'd1;           //虚部
        quadrant <= 4'd0;             //象限
      end
    else if( i_clk_27M_en ) //27M
      begin
        //第一象限部分
        if ( (im_datain[12] == 1'b0) && (hilbert_datain_delay_4[12] == 1'b0) && (im_datain[11:0] < hilbert_datain_delay_4[11:0]) )
          begin 
            tan_numer <= { 1'b0 , im_datain[11:0] };
            tan_denom <= { 1'b0 , hilbert_datain_delay_4[11:0] };
            quadrant <= 4'b0000;
          end
        else if ( (im_datain[12]==1'b0) && (hilbert_datain_delay_4[12]==1'b0) && (im_datain[11:0]==hilbert_datain_delay_4[11:0]) )
          begin
            tan_numer <= {1'b0,im_datain[11:0]};
            tan_denom <= {1'b0,hilbert_datain_delay_4[11:0]};
            quadrant <= 4'b0001;
          end
        else if ( (im_datain[12]==1'b0) && (hilbert_datain_delay_4[12]==1'b0) && (im_datain[11:0]>hilbert_datain_delay_4[11:0]) )
          begin 
            tan_numer <= {1'b0,hilbert_datain_delay_4[11:0]};
            tan_denom <= {1'b0,im_datain[11:0]};
            quadrant <= 4'b0010;
          end
        //第二象限部分
        else if ( (im_datain[12]==1'b0) && (hilbert_datain_delay_4[12]==1'b1) && ({1'b0,im_datain[11:0]}>13'd4096 - {1'b0,hilbert_datain_delay_4[11:0]}) )
          begin 
            tan_numer <= 13'd4096 - {1'b0,hilbert_datain_delay_4[11:0]};
            tan_denom <= {1'b0,im_datain[11:0]};
            quadrant <= 4'b0100;
          end
        else if ( (im_datain[12]==1'b0) && (hilbert_datain_delay_4[12]==1'b1) && ({1'b0,im_datain[11:0]}==13'd4096 - {1'b0,hilbert_datain_delay_4[11:0]}) )
          begin
            tan_numer <= 13'd4096 - {1'b0,hilbert_datain_delay_4[11:0]};
            tan_denom <= {1'b0,im_datain[11:0]};
            quadrant <= 4'b0101;
          end
        else if ( (im_datain[12]==1'b0) && (hilbert_datain_delay_4[12]==1'b1) && ({1'b0,im_datain[11:0]}<13'd4096 - {1'b0,hilbert_datain_delay_4[11:0]}) )
          begin 
            tan_numer <= {1'b0,im_datain[11:0]};
            tan_denom <= 13'd4096 - {1'b0,hilbert_datain_delay_4[11:0]};
            quadrant <= 4'b0110;
          end
        //第三象限部分
        else if ( (im_datain[12]==1'b1) && (hilbert_datain_delay_4[12]==1'b1) && (im_datain[11:0]>hilbert_datain_delay_4[11:0]) )
          begin 
            tan_numer <= 13'd4096 - {1'b0,im_datain[11:0]};
            tan_denom <= 13'd4096 - {1'b0,hilbert_datain_delay_4[11:0]};
            quadrant <= 4'b1000;
          end
        else if ( (im_datain[12]==1'b1) && (hilbert_datain_delay_4[12]==1'b1) && (im_datain[11:0]==hilbert_datain_delay_4[11:0]) )
          begin
            tan_numer <= 13'd4096 - {1'b0,im_datain[11:0]};
            tan_denom <= 13'd4096 - {1'b0,hilbert_datain_delay_4[11:0]};
            quadrant <= 4'b1001;
          end
        else if ( (im_datain[12]==1'b1) && (hilbert_datain_delay_4[12]==1'b1) && (im_datain[11:0]<hilbert_datain_delay_4[11:0]) )
          begin 
            tan_numer <= 13'd4096 - {1'b0,hilbert_datain_delay_4[11:0]};
            tan_denom <= 13'd4096 - {1'b0,im_datain[11:0]};
            quadrant <= 4'b1010;
          end
        //第四象限部分
        else if ( (im_datain[12]==1'b1) && (hilbert_datain_delay_4[12]==1'b0) && (13'd4096 - {1'b0,im_datain[11:0]}>{1'b0,hilbert_datain_delay_4[11:0]}) )
          begin 
            tan_numer <= {1'b0,hilbert_datain_delay_4[11:0]};
            tan_denom <= 13'd4096 - {1'b0,im_datain[11:0]};
            quadrant <= 4'b1100;
          end
        else if ( (im_datain[12]==1'b1) && (hilbert_datain_delay_4[12]==1'b0) && (13'd4096 - {1'b0,im_datain[11:0]}=={1'b0,hilbert_datain_delay_4[11:0]}) )
          begin
            tan_numer <= {1'b0,hilbert_datain_delay_4[11:0]};
            tan_denom <= 13'd4096 - {1'b0,im_datain[11:0]};
            quadrant <= 4'b1101;
          end
        else if ( (im_datain[12]==1'b1) && (hilbert_datain_delay_4[12]==1'b0) && (13'd4096 - {1'b0,im_datain[11:0]}< {1'b0,hilbert_datain_delay_4[11:0]}) )
          begin 
            tan_numer <= 13'd4096 - {1'b0,im_datain[11:0]};
            tan_denom <= {1'b0,hilbert_datain_delay_4[11:0]};
            quadrant <= 4'b1110;
          end
      end//i_clk_27M_en
    else
      begin
        tan_numer <= tan_numer;   
        tan_denom <= tan_denom; 
        quadrant <= quadrant;                              
      end
  end//always
  
//对象限进行延时
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      quadrant_delay <= 4'd0;
    else if( i_clk_27M_en ) //27M
      quadrant_delay <= quadrant;
    else
      quadrant_delay <= quadrant_delay;
  end
  
//查表，得出相位对应到0~pi/4的角度值，先得到ROM表的对应地址
tan_divider tan_divider(
            .denom(tan_denom[11:0]),
            .numer({tan_numer[11:0],9'd0}),
            .quotient(tan_quotient),
            .remain()
            );

//通过ROM表，给出具体的相位值
(*keep*)wire [9:0]acrtan;
rom_tan uu_rom_tan(
        .address(tan_quotient[8:0]),
        .clock(i_clk_27M_en),
        .q(acrtan)
        );

//将相位值还原到0~2pi
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      angle_tan <= 14'd0;
    else if( i_clk_27M_en ) //27M
      begin
        if(!flag_noise)
          case(quadrant)//delay to quadrant 170801
            4'b0000: angle_tan <= {4'b0,acrtan };
            4'b0001: angle_tan <= 14'd1024;
            4'b0010: angle_tan <= 14'd2048 - {4'b0,acrtan};
            
            4'b0100: angle_tan <= 14'd2048 + {4'b0,acrtan};
            4'b0101: angle_tan <= 14'd3072;
            4'b0110: angle_tan <= 14'd4096 - {4'b0,acrtan};
            
            4'b1000: angle_tan <= 14'd4096 + {4'b0,acrtan};
            4'b1001: angle_tan <= 14'd5120;
            4'b1010: angle_tan <= 14'd6144 - {4'b0,acrtan};
            
            4'b1100: angle_tan <= 14'd6144 + {4'b0,acrtan};
            4'b1101: angle_tan <= 14'd7168;
            4'b1110: angle_tan <= 14'd8192 - {4'b0,acrtan};
            default: angle_tan <= angle_tan;
          endcase  
        else
          angle_tan <= angle_tan;
      end//if 27M
    else
      angle_tan <= angle_tan;
  end

//integer w_file; 
//initial w_file = $fopen("data_out.txt"); 
//always @(angle_tan)
//  begin 
//    $fdisplay(w_file,"%h",angle_tan); 
//    //$stop; 
//  end 

assign o_angle_tan = angle_tan;

endmodule
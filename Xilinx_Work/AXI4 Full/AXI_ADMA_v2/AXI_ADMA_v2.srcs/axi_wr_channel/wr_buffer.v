`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Module Name: wr_buffer
// Description: 写缓冲,主要是对用户端转换来的数据进行跨时钟域的处理.
// Revision 0.01 - File Created
//////////////////////////////////////////////////////////////////////////////////

module wr_buffer #(
	parameter	AXI_DATA_WIDTH = 128,
	parameter 	AXI_ADDR_WIDTH = 32
	)(
	input wire							i_clk,						// 用户时钟
	input wire							i_rst_n,					// 复位
	input wire							i_axi_clk,					// AXI时钟

	/*----------------------与wr_ctrl交互信号------------------------*/
	input wire							i_wr_req_en,				// 写请求使能信号
	input wire	[7:0]					iv_wr_burst_length,			// 写突发长度
	input wire	[AXI_ADDR_WIDTH-1:0]	iv_wr_data_addr,			// 写地址
	input wire	[AXI_DATA_WIDTH-1:0]	iv_wr_data_in,				// 转换字长的AXI数据
	input wire							i_wr_data_valid,			// 数据有效信号
	input wire							i_wr_data_last,				// 最后一个数据的标志

	/*--------------------------AXI写地址---------------------------*/ 
	output	reg							o_axi_aw_req_en,			// 写请求
	input	wire						i_axi_aw_ready,				// 从机发送的准备信号(握手信号)
	output	reg	[7:0]					ov_axi_aw_burst_len,		// 写地址突发长度
	output	reg	[AXI_ADDR_WIDTH-1:0]	ov_axi_aw_addr,				// 写地址

	/*--------------------------AXI写数据---------------------------*/ 
	output	reg	[AXI_DATA_WIDTH-1:0]	ov_axi_w_data,				// AXI写数据
	output	reg							o_axi_w_valid,				// 数据有效信号
	input	wire						i_axi_w_ready,				// axi_wr_master准备完成信号
	output	reg							o_axi_w_last,				// 最后一个数据的标志

	/*--------------------------FIFO调试用---------------------------*/ 
	output	reg							o_wr_data_fifo_err,			// FIFO写满标志,爆满
	output	reg							o_wr_cmd_fifo_err			// 写地址/长度FIFO写爆满
);

/*-------------------------------------------*\
			  复位信号Sync定义
\*-------------------------------------------*/
 	(* dont_touch="true" *)	reg		rstn_sync_d0;
  	(* dont_touch="true" *)	reg		rstn_sync_d1;
  	(* dont_touch="true" *)	reg		rstn_sync;

  	(* dont_touch="true" *)	reg		axi_rstn_sync_d0;
  	(* dont_touch="true" *)	reg		axi_rstn_sync_d1;
  	(* dont_touch="true" *)	reg		axi_rstn_sync;

/*-------------------------------------------*\
			   FIFO端口信号定义
\*-------------------------------------------*/
  	
	localparam 	CMD_WIDTH = AXI_ADDR_WIDTH + 8;

  	// FIFO Addr+Burst_length
  	// 缓存写地址(32bit)+突发长度(8bit)
	reg		[CMD_WIDTH-1:0]			cmd_din;
  	reg								cmd_wren;
  	wire	[CMD_WIDTH-1:0]			cmd_dout;		
  	wire							cmd_rden;		
  	wire							cmd_wrfull;	
  	wire							cmd_rdempty;
  	wire	[4:0]					cmd_wrcount;
  	wire	[4:0]					cmd_rdcount;

  	// FIFO data
  	reg								data_wren;
  	wire							data_rden;
  	wire							data_wrfull;
  	wire							data_rdempty;

/*-------------------------------------------*\
					状态机
\*-------------------------------------------*/

	reg	[2:0]	cur_status;
	reg	[2:0]	nxt_status;

	localparam  WR_IDLE    = 3'b000,
				WR_REQ     = 3'b001,
				WR_DATA_EN = 3'b010,
				WR_END     = 3'b100;

/*-------------------------------------------*\
				复位信号CDC
\*-------------------------------------------*/

	always @(posedge i_clk) begin
		rstn_sync_d0 <= i_rst_n;
		rstn_sync_d1 <= rstn_sync_d0;
		rstn_sync    <= rstn_sync_d1;
	end

	always @(posedge i_axi_clk) begin
		axi_rstn_sync_d0 <= i_rst_n;
		axi_rstn_sync_d1 <= axi_rstn_sync_d0;
		axi_rstn_sync    <= axi_rstn_sync_d1;
	end

/*-------------------------------------------*\
				缓存length和addr信息
\*-------------------------------------------*/

	// 写控制信息FIFO基于DRAM实现
	always @(posedge i_clk or negedge rstn_sync) begin
		if(~rstn_sync)begin
			cmd_din  <= {CMD_WIDTH{1'b0}};
			cmd_wren <= 1'b0;
		end
		else if(i_wr_req_en)begin
			cmd_din  <= {iv_wr_burst_length,iv_wr_data_addr};
			cmd_wren <= 1'b1;
		end
		else begin
			cmd_din  <= cmd_din;
			cmd_wren <= 1'b0;
		end
	end

/*-------------------------------------------*\
					状态机
\*-------------------------------------------*/

	always @(posedge i_axi_clk or negedge axi_rstn_sync) begin
		if(~axi_rstn_sync)
			cur_status <= WR_IDLE;
		else 
			cur_status <= nxt_status;
	end

	always @(*) begin
		if(~axi_rstn_sync)
			nxt_status <= WR_IDLE;
		else begin
			case (cur_status)
				WR_IDLE : begin
					if(~cmd_rdempty)
						nxt_status <= WR_REQ;
					else
						nxt_status <= cur_status;
				end
				WR_REQ : begin
					if(o_axi_aw_req_en && i_axi_aw_ready)
						nxt_status <= WR_DATA_EN;
					else
						nxt_status <= cur_status;
				end
				WR_DATA_EN : begin
					// valid和ready表明主从机处于正常交互状态中;此状态下检测到last信号则说明读FIFO结束.
					if(o_axi_w_valid && i_axi_w_ready && o_axi_w_last)
						nxt_status <= WR_END;
					else
						nxt_status <= cur_status;
				end
				WR_END  : nxt_status <= WR_IDLE;
				default : nxt_status <= WR_IDLE;
			endcase
		end
	end

/*-------------------------------------------*\
					发送写请求
\*-------------------------------------------*/
	
	always @(posedge i_axi_clk or negedge axi_rstn_sync) begin
		if(~axi_rstn_sync) 
			o_axi_aw_req_en <= 1'b0;
		else if(i_axi_aw_ready && o_axi_aw_req_en)
			o_axi_aw_req_en <= 1'b0;
		else if(cur_status == WR_REQ)
			o_axi_aw_req_en <= 1'b1;
		else
			o_axi_aw_req_en <= o_axi_aw_req_en;
	end

	// cmd读使能
	assign	cmd_rden = o_axi_aw_req_en && i_axi_aw_ready;

/*-------------------------------------------*\
			   	cmd fifo
\*-------------------------------------------*/
	fifo_w40xd16 wr_cmd_fifo (
	  	.rst           (~rstn_sync),        // input wire rst
	  	.wr_clk        (i_clk),             // input wire wr_clk
	  	.rd_clk        (i_axi_clk),         // input wire rd_clk
	  	.din           (cmd_din),           // input wire [39 : 0] din
	  	.wr_en         (cmd_wren),          // input wire wr_en
	  	.rd_en         (cmd_rden),          // input wire rd_en
	  	.dout          (cmd_dout),          // output wire [39 : 0] dout
	  	.full          (cmd_wrfull),        // output wire full
	  	.empty         (cmd_rdempty),       // output wire empty
	  	.rd_data_count (cmd_rdcount),  		// output wire [4 : 0] rd_data_count
	  	.wr_data_count (cmd_wrcount)  		// output wire [4 : 0] wr_data_count
	);

	always @(*) begin
		if(~axi_rstn_sync) begin
			ov_axi_aw_addr      <= 32'h0;
			ov_axi_aw_burst_len <= 8'h0;
		end 
		else begin
			ov_axi_aw_addr      <= cmd_dout[31:0];
			ov_axi_aw_burst_len <= cmd_dout[39:32];
		end
	end

/*-------------------------------------------*\
			   generate...if...
\*-------------------------------------------*/

	// 根据数据位宽例化对应的FIFO
	generate
		/*-------------------------- DATA_WIDTH=256 ---------------------------*/
		if(AXI_DATA_WIDTH == 256)begin
			// 减去的1代指last信号的1bit位宽
			localparam 	FILL_WIDTH = 288 - AXI_DATA_WIDTH - 1;

			reg		[287:0]		data_din;
			wire	[287:0]		data_dout;
			wire	[9:0]		data_wrcount;
			wire	[9:0]		data_rdcount;

			// 写数据与写使能
			always @(posedge i_clk or negedge rstn_sync) begin
				if(~rstn_sync) begin
					data_din  <= 288'h0;
					data_wren <= 1'b0;
				end 
				else begin
					data_din  <= {{FILL_WIDTH{1'b0}},i_wr_data_last,iv_wr_data_in};
					data_wren <= i_wr_data_valid;
				end
			end

			// o_axi_w_valid信号
			always @(posedge i_axi_clk or negedge axi_rstn_sync) begin
				if(~axi_rstn_sync)
					o_axi_w_valid <= 1'b0;
				else if(o_axi_aw_req_en && i_axi_aw_ready)
					o_axi_w_valid <= 1'b1;		// 当发起写请求时就拉高写有效信号
				else if(o_axi_w_valid && i_axi_w_ready && o_axi_w_last)
					o_axi_w_valid <= 1'b0;		// 最后一个数据发送完毕时就拉低
				else
					o_axi_w_valid <= o_axi_w_valid;
			end

			// FIFO读使能
			assign	data_rden = o_axi_w_valid && i_axi_w_ready && (cur_status == WR_DATA_EN);
			
			fifo_w288xd512 wr_data_fifo (
			  	.rst           (~rstn_sync),                // input wire rst
			  	.wr_clk        (i_clk),                		// input wire wr_clk
			  	.rd_clk        (i_axi_clk),               	// input wire rd_clk
			  	.din           (data_din),                  // input wire [287 : 0] din
			  	.wr_en         (data_wren),                	// input wire wr_en
			  	.rd_en         (data_rden),                	// input wire rd_en
			  	.dout          (data_dout),                	// output wire [287 : 0] dout
			  	.full          (data_wrfull),               // output wire full
			  	.empty         (data_rdempty),             	// output wire empty
			  	.rd_data_count (data_rdcount),  			// output wire [9 : 0] rd_data_count
			  	.wr_data_count (data_wrcount)  				// output wire [9 : 0] wr_data_count
			);
			
			// 输出读数据与last信号
			always @(*) begin
				if(~axi_rstn_sync)begin
					ov_axi_w_data <= {AXI_DATA_WIDTH{1'b0}};
					o_axi_w_last  <= 1'b0;
				end
				else if(data_rden)begin
					ov_axi_w_data <= data_dout[AXI_DATA_WIDTH-1:0];
					o_axi_w_last  <= data_dout[AXI_DATA_WIDTH];
				end
				else begin
					ov_axi_w_data <= {AXI_DATA_WIDTH{1'b0}};
					o_axi_w_last  <= 1'b0;
				end
			end
		end
		/*-------------------------- DATA_WIDTH=128 ---------------------------*/
		else if(AXI_DATA_WIDTH == 128)begin
			// 减去的1代指last信号的1bit位宽
			localparam 	FILL_WIDTH = 144 - AXI_DATA_WIDTH - 1;

			reg		[143:0]		data_din;
			wire	[143:0]		data_dout;
			wire	[9:0]		data_wrcount;
			wire	[9:0]		data_rdcount;

			// 写数据与写使能
			always @(posedge i_clk or negedge rstn_sync) begin
				if(~rstn_sync) begin
					data_din  <= 144'h0;
					data_wren <= 1'b0;
				end 
				else begin
					data_din  <= {{FILL_WIDTH{1'b0}},i_wr_data_last,iv_wr_data_in};
					data_wren <= i_wr_data_valid;
				end
			end

			// o_axi_w_valid信号
			always @(posedge i_axi_clk or negedge axi_rstn_sync) begin
				if(~axi_rstn_sync)
					o_axi_w_valid <= 1'b0;
				else if(o_axi_aw_req_en && i_axi_aw_ready)
					o_axi_w_valid <= 1'b1;		// 当发起写请求时就拉高写有效信号
				else if(o_axi_w_valid && i_axi_w_ready && o_axi_w_last)
					o_axi_w_valid <= 1'b0;		// 最后一个数据发送完毕时就拉低
				else
					o_axi_w_valid <= o_axi_w_valid;
			end

			// FIFO读使能
			assign	data_rden = o_axi_w_valid && i_axi_w_ready && (cur_status == WR_DATA_EN);
			
			fifo_w144xd512 wr_data_fifo (
			  	.rst           (~rstn_sync),              	// input wire rst
			  	.wr_clk        (i_clk),                		// input wire wr_clk
			  	.rd_clk        (i_axi_clk),            		// input wire rd_clk
			  	.din           (data_din),                	// input wire [143 : 0] din
			  	.wr_en         (data_wren),             	// input wire wr_en
			  	.rd_en         (data_rden),             	// input wire rd_en
			  	.dout          (data_dout),              	// output wire [143 : 0] dout
			  	.full          (data_wrfull),            	// output wire full
			  	.empty         (data_rdempty),          	// output wire empty
			  	.rd_data_count (data_rdcount),  			// output wire [9 : 0] rd_data_count
			  	.wr_data_count (data_wrcount)  				// output wire [9 : 0] wr_data_count
			);
			
			// 输出读数据与last信号
			always @(*) begin
				if(~axi_rstn_sync)begin
					ov_axi_w_data <= {AXI_DATA_WIDTH{1'b0}};
					o_axi_w_last  <= 1'b0;
				end
				else if(data_rden)begin
					ov_axi_w_data <= data_dout[AXI_DATA_WIDTH-1:0];
					o_axi_w_last  <= data_dout[AXI_DATA_WIDTH];
				end
				else begin
					ov_axi_w_data <= {AXI_DATA_WIDTH{1'b0}};
					o_axi_w_last  <= 1'b0;
				end
			end
		end
		/*-------------------------- DATA_WIDTH=64 ---------------------------*/
		else if(AXI_DATA_WIDTH == 64)begin
			// 减去的1代指last信号的1bit位宽
			localparam 	FILL_WIDTH = 72 - AXI_DATA_WIDTH - 1;

			reg		[71:0]		data_din;
			wire	[71:0]		data_dout;
			wire	[9:0]		data_wrcount;
			wire	[9:0]		data_rdcount;

			// 写数据与写使能
			always @(posedge i_clk or negedge rstn_sync) begin
				if(~rstn_sync) begin
					data_din  <= 72'h0;
					data_wren <= 1'b0;
				end 
				else begin
					data_din  <= {{FILL_WIDTH{1'b0}},i_wr_data_last,iv_wr_data_in};
					data_wren <= i_wr_data_valid;
				end
			end

			// o_axi_w_valid信号
			always @(posedge i_axi_clk or negedge axi_rstn_sync) begin
				if(~axi_rstn_sync)
					o_axi_w_valid <= 1'b0;
				else if(o_axi_aw_req_en && i_axi_aw_ready)
					o_axi_w_valid <= 1'b1;		// 当发起写请求时就拉高写有效信号
				else if(o_axi_w_valid && i_axi_w_ready && o_axi_w_last)
					o_axi_w_valid <= 1'b0;		// 最后一个数据发送完毕时就拉低
				else
					o_axi_w_valid <= o_axi_w_valid;
			end

			// FIFO读使能
			assign	data_rden = o_axi_w_valid && i_axi_w_ready && (cur_status == WR_DATA_EN);
			
			fifo_w72xd512 wr_data_fifo (
			  	.rst           (~rstn_sync),              	// input wire rst
			  	.wr_clk        (i_clk),                		// input wire wr_clk
			  	.rd_clk        (i_axi_clk),            		// input wire rd_clk
			  	.din           (data_din),                	// input wire [143 : 0] din
			  	.wr_en         (data_wren),             	// input wire wr_en
			  	.rd_en         (data_rden),             	// input wire rd_en
			  	.dout          (data_dout),              	// output wire [143 : 0] dout
			  	.full          (data_wrfull),            	// output wire full
			  	.empty         (data_rdempty),          	// output wire empty
			  	.rd_data_count (data_rdcount),  			// output wire [9 : 0] rd_data_count
			  	.wr_data_count (data_wrcount)  				// output wire [9 : 0] wr_data_count
			);
			
			// 输出读数据与last信号
			always @(*) begin
				if(~axi_rstn_sync)begin
					ov_axi_w_data <= {AXI_DATA_WIDTH{1'b0}};
					o_axi_w_last  <= 1'b0;
				end
				else if(data_rden)begin
					ov_axi_w_data <= data_dout[AXI_DATA_WIDTH-1:0];
					o_axi_w_last  <= data_dout[AXI_DATA_WIDTH];
				end
				else begin
					ov_axi_w_data <= {AXI_DATA_WIDTH{1'b0}};
					o_axi_w_last  <= 1'b0;
				end
			end
		end
	endgenerate

/*-------------------------------------------*\
			   FIFO 调试信号
\*-------------------------------------------*/

	// FIFO写满信号 : 注意是在用户时钟域下.
	always @(posedge i_clk or negedge rstn_sync) begin
		if(~rstn_sync)
			o_wr_cmd_fifo_err <= 1'b0;
		else if(cmd_wrfull && cmd_wren)
			o_wr_cmd_fifo_err <= 1'b1;
		else
			o_wr_cmd_fifo_err <= 1'b0;
	end

	always @(posedge i_clk or negedge rstn_sync) begin
		if(~rstn_sync)
			o_wr_data_fifo_err <= 1'b0;
		else if(data_wrfull && data_wren)
			o_wr_data_fifo_err <= 1'b1;
		else
			o_wr_data_fifo_err <= 1'b0;
	end

endmodule

//-------------------------------------------------------------------------------------------------                                                                             
//                                                                                                                                                                              
//--- (C) COPYRIGHT 2014 BUPT
//--- ALL RIGHTS RESERVED
//--- File      : 11bit_Crc32.v
//--- Auther    : Leexiao
//--- Date      : 2016.08.11
//--- Version   : v1.0.0
//--- Abstract  : 11bit CRC 32bit。
//-------------------------------------------------------------------------------------------------                                                                             
//                                                                                                                                                                              
//  Description :	1.input "i_data", output "o_crc33"
//--------------------------------------------------------------------------
// 模块功能:  data11bit 32位CRC校验
//            ……………………………………………………………………………
// --------------------------------------------------------------------------                                                                                                                                                                              
//-------------------------------------------------------------------------------------------------                                                                             
//--- Modification History:
//--- Date               	By           			Version               	  Change Description 
//-------------------------------------------------------------------------------------------------
//--- 2016.8	         	Leexiao            		v1.0.0	                                                                   
//---                                            
//---
//-------------------------------------------------------------------------------------------------
module Crc32_D11
    (
    i_clk_27M,//clock
    i_rst_n,  //reset
    i_data,   //data to be checked 
    i_sck,    //check data when the input signal is high level
    i_93start,//93字起始脉冲
    
    o_crc33,  //33bit 32位校验+1bit0  
    o_crcok   //检验完成指示 高有效   
    );					

input i_clk_27M;
input i_rst_n;
input [10:0]i_data;
input i_sck;
input i_93start;

output [32:0]o_crc33;
output o_crcok;
//-------------信号声明----------------
reg [6:0] countbyte;
reg done_r;
reg done_r_temp;
wire done_r_p;
reg done_r1;
reg done_r2;
reg done;
reg [31:0]checksum32;
reg [32:0] o_crc33;
reg crcok;
reg crcok_temp;
//-------------------------------------------------------------------------------------------------
//CRC校验函数
  // polynomial: (0 1 2 4 5 7 8 10 11 12 16 22 23 26 32)
  // data width: 11
  // convention: the first serial bit is D[10]
  function [31:0] Ethernet_CRC32_D11;

    input [10:0] Data;
    input [31:0] crc;
    reg [10:0] d;
    reg [31:0] c;
    reg [31:0] newcrc;
    begin
    d = Data;
    c = crc;

    newcrc[0] = d[10] ^ d[9] ^ d[6] ^ d[0] ^ c[21] ^ c[27] ^ c[30] ^ c[31];
    newcrc[1] = d[9] ^ d[7] ^ d[6] ^ d[1] ^ d[0] ^ c[21] ^ c[22] ^ c[27] ^ c[28] ^ c[30];
    newcrc[2] = d[9] ^ d[8] ^ d[7] ^ d[6] ^ d[2] ^ d[1] ^ d[0] ^ c[21] ^ c[22] ^ c[23] ^ c[27] ^ c[28] ^ c[29] ^ c[30];
    newcrc[3] = d[10] ^ d[9] ^ d[8] ^ d[7] ^ d[3] ^ d[2] ^ d[1] ^ c[22] ^ c[23] ^ c[24] ^ c[28] ^ c[29] ^ c[30] ^ c[31];
    newcrc[4] = d[8] ^ d[6] ^ d[4] ^ d[3] ^ d[2] ^ d[0] ^ c[21] ^ c[23] ^ c[24] ^ c[25] ^ c[27] ^ c[29];
    newcrc[5] = d[10] ^ d[7] ^ d[6] ^ d[5] ^ d[4] ^ d[3] ^ d[1] ^ d[0] ^ c[21] ^ c[22] ^ c[24] ^ c[25] ^ c[26] ^ c[27] ^ c[28] ^ c[31];
    newcrc[6] = d[8] ^ d[7] ^ d[6] ^ d[5] ^ d[4] ^ d[2] ^ d[1] ^ c[22] ^ c[23] ^ c[25] ^ c[26] ^ c[27] ^ c[28] ^ c[29];
    newcrc[7] = d[10] ^ d[8] ^ d[7] ^ d[5] ^ d[3] ^ d[2] ^ d[0] ^ c[21] ^ c[23] ^ c[24] ^ c[26] ^ c[28] ^ c[29] ^ c[31];
    newcrc[8] = d[10] ^ d[8] ^ d[4] ^ d[3] ^ d[1] ^ d[0] ^ c[21] ^ c[22] ^ c[24] ^ c[25] ^ c[29] ^ c[31];
    newcrc[9] = d[9] ^ d[5] ^ d[4] ^ d[2] ^ d[1] ^ c[22] ^ c[23] ^ c[25] ^ c[26] ^ c[30];
    newcrc[10] = d[9] ^ d[5] ^ d[3] ^ d[2] ^ d[0] ^ c[21] ^ c[23] ^ c[24] ^ c[26] ^ c[30];
    newcrc[11] = d[9] ^ d[4] ^ d[3] ^ d[1] ^ d[0] ^ c[0] ^ c[21] ^ c[22] ^ c[24] ^ c[25] ^ c[30];
    newcrc[12] = d[9] ^ d[6] ^ d[5] ^ d[4] ^ d[2] ^ d[1] ^ d[0] ^ c[1] ^ c[21] ^ c[22] ^ c[23] ^ c[25] ^ c[26] ^ c[27] ^ c[30];
    newcrc[13] = d[10] ^ d[7] ^ d[6] ^ d[5] ^ d[3] ^ d[2] ^ d[1] ^ c[2] ^ c[22] ^ c[23] ^ c[24] ^ c[26] ^ c[27] ^ c[28] ^ c[31];
    newcrc[14] = d[8] ^ d[7] ^ d[6] ^ d[4] ^ d[3] ^ d[2] ^ c[3] ^ c[23] ^ c[24] ^ c[25] ^ c[27] ^ c[28] ^ c[29];
    newcrc[15] = d[9] ^ d[8] ^ d[7] ^ d[5] ^ d[4] ^ d[3] ^ c[4] ^ c[24] ^ c[25] ^ c[26] ^ c[28] ^ c[29] ^ c[30];
    newcrc[16] = d[8] ^ d[5] ^ d[4] ^ d[0] ^ c[5] ^ c[21] ^ c[25] ^ c[26] ^ c[29];
    newcrc[17] = d[9] ^ d[6] ^ d[5] ^ d[1] ^ c[6] ^ c[22] ^ c[26] ^ c[27] ^ c[30];
    newcrc[18] = d[10] ^ d[7] ^ d[6] ^ d[2] ^ c[7] ^ c[23] ^ c[27] ^ c[28] ^ c[31];
    newcrc[19] = d[8] ^ d[7] ^ d[3] ^ c[8] ^ c[24] ^ c[28] ^ c[29];
    newcrc[20] = d[9] ^ d[8] ^ d[4] ^ c[9] ^ c[25] ^ c[29] ^ c[30];
    newcrc[21] = d[10] ^ d[9] ^ d[5] ^ c[10] ^ c[26] ^ c[30] ^ c[31];
    newcrc[22] = d[9] ^ d[0] ^ c[11] ^ c[21] ^ c[30];
    newcrc[23] = d[9] ^ d[6] ^ d[1] ^ d[0] ^ c[12] ^ c[21] ^ c[22] ^ c[27] ^ c[30];
    newcrc[24] = d[10] ^ d[7] ^ d[2] ^ d[1] ^ c[13] ^ c[22] ^ c[23] ^ c[28] ^ c[31];
    newcrc[25] = d[8] ^ d[3] ^ d[2] ^ c[14] ^ c[23] ^ c[24] ^ c[29];
    newcrc[26] = d[10] ^ d[6] ^ d[4] ^ d[3] ^ d[0] ^ c[15] ^ c[21] ^ c[24] ^ c[25] ^ c[27] ^ c[31];
    newcrc[27] = d[7] ^ d[5] ^ d[4] ^ d[1] ^ c[16] ^ c[22] ^ c[25] ^ c[26] ^ c[28];
    newcrc[28] = d[8] ^ d[6] ^ d[5] ^ d[2] ^ c[17] ^ c[23] ^ c[26] ^ c[27] ^ c[29];
    newcrc[29] = d[9] ^ d[7] ^ d[6] ^ d[3] ^ c[18] ^ c[24] ^ c[27] ^ c[28] ^ c[30];
    newcrc[30] = d[10] ^ d[8] ^ d[7] ^ d[4] ^ c[19] ^ c[25] ^ c[28] ^ c[29] ^ c[31];
    newcrc[31] = d[9] ^ d[8] ^ d[5] ^ c[20] ^ c[26] ^ c[29] ^ c[30];
    Ethernet_CRC32_D11 = newcrc;
  end
  endfunction

//-------------------------------------------------------------------------------------------------
//计数输入的11bit的个数  93一组
always @(negedge i_rst_n or posedge i_clk_27M)//mark
begin
    if (!i_rst_n)
        countbyte<=7'b0;
    else if (i_93start)
        countbyte<=7'b0;
    else if ( (countbyte == 7'd93) && (i_sck) )
        countbyte<=7'd1;
    else if (i_sck)
        countbyte <= countbyte+1'b1;
    else 
        countbyte <= countbyte;
end
//-------------------------------------------------------------------------------------------------
//93个11bit后，一整条1023bit的报文结束，给出结束标志
always @(negedge i_rst_n or negedge i_clk_27M)
begin
    if (!i_rst_n)  begin
        done_r <= 1'b0;
        done_r_temp <= 1'b0;
    end
    else if (countbyte == 7'd93 )  begin
        done_r <= 1'b1;
        done_r_temp <= done_r;
    end
    else  begin
        done_r <= 1'b0;
        done_r_temp <= 1'b0;
    end
end

assign done_r_p = (~done_r_temp) & (done_r);
//------------------------------------------------------------------------------//
//delay 2 clock 
always @(negedge i_rst_n or negedge i_clk_27M)
begin
    if (!i_rst_n)begin
        done_r1 <= 1'b0;
        done_r2 <= 1'b0;
        done <= 1'b0;
    end
    else begin
        done_r1 <= done_r_p;
        done_r2 <= done_r1;
        done <= done_r2;
    end
end
//-------------------------------------------------------------------------------------------------
//调用crc
always @(negedge i_rst_n or posedge i_clk_27M ) 
begin
	if(!i_rst_n)	
        checksum32 <= 32'b0;
    else if(done)   //93个以后清空
        checksum32 <= 32'b0; 
	else if( i_sck )//check data 
	    checksum32 <= Ethernet_CRC32_D11(i_data,checksum32);		
	else 
		checksum32 <= checksum32;
end	

//-------------------------------------------------------------------------------------------------
//输出CRC校验结果并在最后补一位零
always @(negedge i_rst_n or posedge i_clk_27M ) 
begin
	if(!i_rst_n)  begin	
        o_crc33 <= 33'b0;
    end
	else if( (countbyte == 7'd93) && (done_r_p) )  begin
	    o_crc33 <= {checksum32,1'b0};	
	end
	else begin
		o_crc33 <= o_crc33;
    end
end	

//-------------------------------------------------------------------------------------------------
//校验完成指示
always @(negedge i_rst_n or posedge i_clk_27M ) 
begin
	if(!i_rst_n)  begin	
        crcok <= 1'b0;
        crcok_temp <= 1'b0;
    end
	else if( (countbyte == 7'd93) && (done_r_p) )  begin
	    crcok <= 1'b1;
	    crcok_temp <= crcok;
	end
	else begin
		crcok <= 1'b0;
		crcok_temp <= 1'b0;
    end
end	

assign o_crcok = (crcok) & (~crcok_temp);
	
endmodule 
close all;
clear;
clc;

%% m序列检测
% 检测通信板报文数据何时自检成功（m序列匹配成功）

%读通信板报文数据
i_data_hex = textread('C:\Users\lenovo\Desktop\1.txt','%s');
i_data_dec = hex2dec(i_data_hex)';
i_data_bin = dec2bin(i_data_dec,48);%共 bit
i_data_bin_48 = i_data_bin - 48;

%自检m序列：101011110001001
m_series_0 =  [1 0 1 0 1 1 1 1 0 0 0 1 0 0 1];
m_series_1 =  [0 1 0 1 1 1 1 0 0 0 1 0 0 1 1];
m_series_2 =  [1 0 1 1 1 1 0 0 0 1 0 0 1 1 0];
m_series_3 =  [0 1 1 1 1 0 0 0 1 0 0 1 1 0 1];
m_series_4 =  [1 1 1 1 0 0 0 1 0 0 1 1 0 1 0];
m_series_5 =  [1 1 1 0 0 0 1 0 0 1 1 0 1 0 1];
m_series_6 =  [1 1 0 0 0 1 0 0 1 1 0 1 0 1 1];
m_series_7 =  [1 0 0 0 1 0 0 1 1 0 1 0 1 1 1];
m_series_8 =  [0 0 0 1 0 0 1 1 0 1 0 1 1 1 1];
m_series_9 =  [0 0 1 0 0 1 1 0 1 0 1 1 1 1 0];
m_series_10 = [0 1 0 0 1 1 0 1 0 1 1 1 1 0 0];
m_series_11 = [1 0 0 1 1 0 1 0 1 1 1 1 0 0 0];
m_series_12 = [0 0 1 1 0 1 0 1 1 1 1 0 0 0 1];
m_series_13 = [0 1 1 0 1 0 1 1 1 1 0 0 0 1 0];
m_series_14 = [1 1 0 1 0 1 1 1 1 0 0 0 1 0 0];
for i=1:length(i_data_bin_48)-14
    i_data_bin_temp = i_data_bin_48(i:i+14);
    if( (sum(i_data_bin_temp == m_series_0)==15)|(sum(i_data_bin_temp == m_series_1)==15)|(sum(i_data_bin_temp == m_series_2)==15)|(sum(i_data_bin_temp == m_series_3)==15)|...
        (sum(i_data_bin_temp == m_series_4)==15)|(sum(i_data_bin_temp == m_series_5)==15)|(sum(i_data_bin_temp == m_series_6)==15)|(sum(i_data_bin_temp == m_series_7)==15)|...
        (sum(i_data_bin_temp == m_series_8)==15)|(sum(i_data_bin_temp == m_series_9)==15)|(sum(i_data_bin_temp == m_series_10)==15)|(sum(i_data_bin_temp == m_series_11)==15)|...
        (sum(i_data_bin_temp == m_series_12)==15)|(sum(i_data_bin_temp == m_series_13)==15)|(sum(i_data_bin_temp == m_series_14)==15))
        M_correct(i) = 1;
    else
        M_correct(i) = 0;
    end
end
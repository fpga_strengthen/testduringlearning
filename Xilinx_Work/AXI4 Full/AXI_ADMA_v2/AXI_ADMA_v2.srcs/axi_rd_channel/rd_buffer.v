`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Module Name: rd_buffer 
// Description: 
// Revision 0.01 - File Created
//////////////////////////////////////////////////////////////////////////////////

module rd_buffer #(
	parameter   AXI_DATA_WIDTH  = 128,		// AXI数据位宽
	parameter   AXI_ADDR_WIDTH  = 32,		// AXI读地址宽度
	parameter	USER_DATA_WIDTH = 16		// 用户端数据位宽
	)(
	input		wire							i_clk,		// 用户端时钟
	input		wire							i_rst_n,	// 复位,低有效
	input 		wire							i_axi_clk,	// AXI时钟

	/*----------------------与rd_ctrl交互信号------------------------*/
	input 		wire							i_rd_req_en,				// 读请求使能信号
	input 		wire	[7:0]					iv_rd_burst_length,			// 读突发长度
	input 		wire	[AXI_ADDR_WIDTH-1:0]	iv_rd_data_addr,			// 读地址
	output 		wire							o_rd_req_ready,				// 用来指示cmd_fifo的写情况,是否有爆满

	/*----------------------与axi_rd_master交互信号-------------------*/
	output		reg								o_axi_ar_req_en,			// 读地址写请求
	input		wire							i_axi_ar_ready,				// 从机准备信号
	output		reg		[7:0]					ov_axi_ar_burst_len,		// 突发长度
	output		reg		[AXI_ADDR_WIDTH-1:0]	ov_axi_ar_addr,				// 读地址

	input		wire							i_axi_r_valid,				// 由从机读出的数据的有效信号
	input		wire	[AXI_DATA_WIDTH-1:0]	iv_axi_r_data,				// 由从机传来的数据
	input		wire							i_axi_r_last,				// 最后一个数据的标志

	/*----------------------与用户端交互的信号-------------------------*/
	output		reg								o_user_rd_valid,			// 数据有效信号
	output		reg		[USER_DATA_WIDTH-1:0]	ov_user_rd_data,			// 读出给用户端的数据
	output		reg								o_user_rd_last,				// 最后一个数据的标志

	/*------------------------FIFO读调试信号--------------------------*/
	output 		reg								o_rd_data_fifo_err,			// 读数据空读
	output 		reg								o_rd_cmd_fifo_err			// 读地址空读
);

	// 控制输出移位的计数器的最大值
	localparam	MAX_CNT_FLAG = AXI_DATA_WIDTH/USER_DATA_WIDTH - 1;
	// 地址宽度+突发长度的字长
	localparam 	CMD_WIDTH = AXI_ADDR_WIDTH + 8;
	// 控制移位输出的计数器的字长
	localparam 	WIDTH_CNT_FLAG = $clog2(MAX_CNT_FLAG);

/*-------------------------------------------*\
			  复位信号Sync定义
\*-------------------------------------------*/
 	(* dont_touch="true" *)	reg		rstn_sync_d0;
  	(* dont_touch="true" *)	reg		rstn_sync_d1;
  	(* dont_touch="true" *)	reg		rstn_sync;

  	(* dont_touch="true" *)	reg		axi_rstn_sync_d0;
  	(* dont_touch="true" *)	reg		axi_rstn_sync_d1;
  	(* dont_touch="true" *)	reg		axi_rstn_sync;

/*-------------------------------------------*\
			   FIFO端口信号定义
\*-------------------------------------------*/

  	// FIFO Addr+Burst_length
  	// 缓存写地址(32bit)+突发长度(8bit)
	reg		[CMD_WIDTH-1:0]	cmd_din;
  	reg						cmd_wren;
  	wire	[CMD_WIDTH-1:0]	cmd_dout;		
  	wire					cmd_rden;		
  	wire					cmd_wrfull;	
  	wire					cmd_rdempty;
  	wire	[4:0]			cmd_wrcount;
  	wire	[4:0]			cmd_rdcount;

  	// FIFO data
  	reg						data_wren;
  	reg						data_rden;
  	wire					data_wrfull;
  	wire					data_rdempty;
  	// Using more accurate results whichs cost more resources.
  	wire	[9:0]			data_wrcount;
	wire	[9:0]			data_rdcount;

/*-------------------------------------------*\
					其他信号
\*-------------------------------------------*/
  	
	// 在数据位宽为256bit即32个字节时,最大突发长度为4096字节
	// 4096/32=128,则写入的FIFO深度是128;
	// 此时512-128=384,即下一次的写数据长度小于384时就可以写入,不会产生爆满的问题.
  	wire 	rd_data_buf_ready;	// 此信号在generate-if语句中进行赋值.

	reg								rd_data_flag;	// 表明正在读数据中
	reg	[WIDTH_CNT_FLAG-1:0]		cnt_flag;
	reg	[AXI_DATA_WIDTH-1:0]		rd_data_fifo_out;
	reg								rd_data_fifo_last;

/*-------------------------------------------*\
					状态机
\*-------------------------------------------*/

	reg	[2:0]	cur_status;
	reg	[2:0]	nxt_status;

	localparam  RD_IDLE = 3'b000,
				RD_REQ  = 3'b001,
				RD_DATA = 3'b010,
				RD_END  = 3'b100;

/*-------------------------------------------*\
			  	assign
\*-------------------------------------------*/

	// 防止CMD FIFO写爆满
	assign	o_rd_req_ready = rstn_sync ? (cmd_wrcount <= 4'd12) : 1'b0;

/*-------------------------------------------*\
				复位信号CDC
\*-------------------------------------------*/

	always @(posedge i_clk) begin
		rstn_sync_d0 <= i_rst_n;
		rstn_sync_d1 <= rstn_sync_d0;
		rstn_sync    <= rstn_sync_d1;
	end

	always @(posedge i_axi_clk) begin
		axi_rstn_sync_d0 <= i_rst_n;
		axi_rstn_sync_d1 <= axi_rstn_sync_d0;
		axi_rstn_sync    <= axi_rstn_sync_d1;
	end

/*-------------------------------------------*\
			缓存length和addr信息,写FIFO
\*-------------------------------------------*/

	always @(posedge i_clk or negedge rstn_sync) begin
		if(~rstn_sync) begin
			cmd_wren <= 1'b0;
			cmd_din  <= {CMD_WIDTH{1'b0}};
		end 
		else if(i_rd_req_en && o_rd_req_ready)begin
			cmd_wren <= 1'b1;
			cmd_din  <= {iv_rd_burst_length,iv_rd_data_addr};
		end
		else begin
			cmd_wren <= 1'b0;
			cmd_din  <= cmd_din;
		end
	end

/*-------------------------------------------*\
					状态机
\*-------------------------------------------*/

	always @(posedge i_axi_clk or negedge axi_rstn_sync) begin
		if(~axi_rstn_sync)
			cur_status <= RD_IDLE;
		else begin
			cur_status <= nxt_status;
		end
	end

	always @(*) begin
		if(~axi_rstn_sync)
			nxt_status <= RD_IDLE;
		else begin
			case (cur_status)
				RD_IDLE : begin
					if(~cmd_rdempty && rd_data_buf_ready)
						nxt_status <= RD_REQ;
					else
						nxt_status <= cur_status;
				end
				RD_REQ : begin
					if(i_axi_ar_ready && o_axi_ar_req_en)
						nxt_status <= RD_DATA;
					else
						nxt_status <= cur_status;
				end
				RD_DATA : begin
					if(i_axi_r_valid && i_axi_r_last)
						nxt_status <= RD_END;
					else
						nxt_status <= cur_status;
				end
				RD_END  : nxt_status <= RD_IDLE;
				default : nxt_status <= RD_IDLE;
			endcase
		end
	end

/*-------------------------------------------*\
			cmd fifo读使能和读数据输出
\*-------------------------------------------*/

	// cmd fifo读使能
	assign	cmd_rden = (i_axi_ar_ready && o_axi_ar_req_en);

	always @(posedge i_axi_clk or negedge axi_rstn_sync) begin
		if(~axi_rstn_sync)
			o_axi_ar_req_en <= 1'b0;
		else if(o_axi_ar_req_en && i_axi_ar_ready)
			o_axi_ar_req_en <= 1'b0;
		else if(cur_status == RD_REQ)
			o_axi_ar_req_en <= 1'b1;
		else
			o_axi_ar_req_en <= o_axi_ar_req_en;
	end

	always @(*) begin
		if(~axi_rstn_sync)begin
			ov_axi_ar_burst_len <= 8'h0;
			ov_axi_ar_addr      <= {AXI_ADDR_WIDTH{1'b0}};
		end
		else begin
			ov_axi_ar_burst_len <= cmd_dout[CMD_WIDTH-1:AXI_ADDR_WIDTH];
			ov_axi_ar_addr      <= cmd_dout[AXI_ADDR_WIDTH-1:0];
		end
	end

/*-------------------------------------------*\
			   	cmd fifo
\*-------------------------------------------*/
	fifo_w40xd16 wr_cmd_fifo (
	  	.rst           (~rstn_sync),        // input wire rst
	  	.wr_clk        (i_clk),             // input wire wr_clk
	  	.rd_clk        (i_axi_clk),         // input wire rd_clk
	  	.din           (cmd_din),           // input wire [39 : 0] din
	  	.wr_en         (cmd_wren),          // input wire wr_en
	  	.rd_en         (cmd_rden),          // input wire rd_en
	  	.dout          (cmd_dout),          // output wire [39 : 0] dout
	  	.full          (cmd_wrfull),        // output wire full
	  	.empty         (cmd_rdempty),       // output wire empty
	  	.rd_data_count (cmd_rdcount),  		// output wire [4 : 0] rd_data_count
	  	.wr_data_count (cmd_wrcount)  		// output wire [4 : 0] wr_data_count
	);

/*-------------------------------------------*\
			generate-if配置data fifo
\*-------------------------------------------*/
	
	// data fifo读出的数据的移位使能信号
	always @(posedge i_clk or negedge rstn_sync) begin
		if(~rstn_sync)
			rd_data_flag <= 1'b0;
		else if(rd_data_flag && (cnt_flag == MAX_CNT_FLAG))
			rd_data_flag <= 1'b0;
		else if(data_rden)
			rd_data_flag <= 1'b1;
		else
			rd_data_flag <= rd_data_flag;
	end
	
	// 配置data fifo读使能
	always @(posedge i_clk or negedge rstn_sync) begin
		if(~rstn_sync) 
			data_rden <= 1'b0;
		else if(data_rden)			// 因为读出的数据还要进行128b转16b,不能连续读出,要留出转换的时间
			data_rden <= 1'b0;		// 所以读使能一次只能拉高一个时钟周期.
		else if(~data_rdempty && ~rd_data_flag)
			data_rden <= 1'b1;
		else
			data_rden <= data_rden;
	end

	// 移位计数器
	always @(posedge i_clk or negedge rstn_sync) begin
		if(~rstn_sync)
			cnt_flag <= {WIDTH_CNT_FLAG{1'b0}};
		else if(rd_data_flag)begin
			if(cnt_flag == MAX_CNT_FLAG)
				cnt_flag <= {WIDTH_CNT_FLAG{1'b0}};
			else
				cnt_flag <= cnt_flag + 1'b1;
		end else
			cnt_flag <= cnt_flag;
	end

	// generate...if...
	generate
		if(AXI_DATA_WIDTH == 256)begin
			localparam	BRAM_FULL_BITS = 288;
			// 填充的比特数,以充分利用BRAM的资源
			localparam	FILL_WIDTH = BRAM_FULL_BITS - AXI_DATA_WIDTH - 1;

			reg		[BRAM_FULL_BITS-1:0]	data_din;
			wire	[BRAM_FULL_BITS-1:0]	data_dout;

			// 将从机传来的数据暂存准备写入data_fifo中
			always @(posedge i_axi_clk or negedge axi_rstn_sync) begin
				if(~axi_rstn_sync) begin
					data_din  <= {BRAM_FULL_BITS{1'b0}};
					data_wren <= 1'b0;
				end 
				else begin
					data_din  <= {{FILL_WIDTH{1'b0}},i_axi_r_last,iv_axi_r_data};
					data_wren <= i_axi_r_valid;
				end
			end

			// 将从data fifo读出数据的data和last信号缓存后用于移位输出
			always @(posedge i_clk or negedge rstn_sync) begin
				if(~rstn_sync) 
					rd_data_fifo_out <= {AXI_DATA_WIDTH{1'b0}};
				else if(data_rden)
					rd_data_fifo_out <= data_dout[AXI_DATA_WIDTH-1:0];
				else if(rd_data_flag)
					rd_data_fifo_out <= (rd_data_fifo_out >> USER_DATA_WIDTH);
				else
					rd_data_fifo_out <= rd_data_fifo_out;
			end

			always @(posedge i_clk or negedge rstn_sync) begin
				if(~rstn_sync)
					rd_data_fifo_last <= 1'b0;
				else if(rd_data_flag && (cnt_flag == MAX_CNT_FLAG))
					rd_data_fifo_last <= 1'b0;
				else if(data_rden && data_dout[AXI_DATA_WIDTH])
					rd_data_fifo_last <= 1'b1;	// 在读出FIFO中的最后一个数据时拉高
				else
					rd_data_fifo_last <= rd_data_fifo_last;
			end

			assign	rd_data_buf_ready = rstn_sync ? (data_wrcount <= 10'd384) : 1'b0;

			// FIFO例化
			fifo_w288xd512 wr_data_fifo (
			  	.rst           (~rstn_sync),                // input wire rst
			  	.wr_clk        (i_axi_clk),                	// input wire wr_clk
			  	.rd_clk        (i_clk),               		// input wire rd_clk
			  	.din           (data_din),                  // input wire [287 : 0] din
			  	.wr_en         (data_wren),                	// input wire wr_en
			  	.rd_en         (data_rden),                	// input wire rd_en
			  	.dout          (data_dout),                	// output wire [287 : 0] dout
			  	.full          (data_wrfull),               // output wire full
			  	.empty         (data_rdempty),             	// output wire empty
			  	.rd_data_count (data_rdcount),  			// output wire [9 : 0] rd_data_count
			  	.wr_data_count (data_wrcount)  				// output wire [9 : 0] wr_data_count
			);
		end
		else if(AXI_DATA_WIDTH == 128)begin
			localparam	BRAM_FULL_BITS = 144;
			// 填充的比特数,以充分利用BRAM的资源
			localparam	FILL_WIDTH = BRAM_FULL_BITS - AXI_DATA_WIDTH - 1;

			reg		[BRAM_FULL_BITS-1:0]	data_din;
			wire	[BRAM_FULL_BITS-1:0]	data_dout;

			// 将从机传来的数据暂存准备写入data_fifo中
			always @(posedge i_axi_clk or negedge axi_rstn_sync) begin
				if(~axi_rstn_sync) begin
					data_din  <= {BRAM_FULL_BITS{1'b0}};
					data_wren <= 1'b0;
				end 
				else begin
					data_din  <= {{FILL_WIDTH{1'b0}},i_axi_r_last,iv_axi_r_data};
					data_wren <= i_axi_r_valid;
				end
			end

			// 将从data fifo读出数据的data和last信号缓存后用于移位输出
			always @(posedge i_clk or negedge rstn_sync) begin
				if(~rstn_sync) 
					rd_data_fifo_out <= {AXI_DATA_WIDTH{1'b0}};
				else if(data_rden)
					rd_data_fifo_out <= data_dout[AXI_DATA_WIDTH-1:0];
				else if(rd_data_flag)
					rd_data_fifo_out <= (rd_data_fifo_out >> USER_DATA_WIDTH);
				else
					rd_data_fifo_out <= rd_data_fifo_out;
			end

			always @(posedge i_clk or negedge rstn_sync) begin
				if(~rstn_sync)
					rd_data_fifo_last <= 1'b0;
				else if(rd_data_flag && (cnt_flag == MAX_CNT_FLAG))
					rd_data_fifo_last <= 1'b0;
				else if(data_rden && data_dout[AXI_DATA_WIDTH])
					rd_data_fifo_last <= 1'b1;	// 在读出FIFO中的最后一个数据时拉高
				else
					rd_data_fifo_last <= rd_data_fifo_last;
			end

			assign	rd_data_buf_ready = rstn_sync ? (data_wrcount <= 10'd256) : 1'b0;

			// FIFO例化
			fifo_w144xd512 wr_data_fifo (
			  	.rst           (~axi_rstn_sync),            // input wire rst
			  	.wr_clk        (i_axi_clk),                	// input wire wr_clk
			  	.rd_clk        (i_clk),               		// input wire rd_clk
			  	.din           (data_din),                  // input wire [287 : 0] din
			  	.wr_en         (data_wren),                	// input wire wr_en
			  	.rd_en         (data_rden),                	// input wire rd_en
			  	.dout          (data_dout),                	// output wire [287 : 0] dout
			  	.full          (data_wrfull),               // output wire full
			  	.empty         (data_rdempty),             	// output wire empty
			  	.rd_data_count (data_rdcount),  			// output wire [9 : 0] rd_data_count
			  	.wr_data_count (data_wrcount)  				// output wire [9 : 0] wr_data_count
			);
		end
		else if(AXI_DATA_WIDTH == 64)begin
			localparam	BRAM_FULL_BITS = 72;
			// 填充的比特数,以充分利用BRAM的资源
			localparam	FILL_WIDTH = BRAM_FULL_BITS - AXI_DATA_WIDTH - 1;

			reg		[BRAM_FULL_BITS-1:0]	data_din;
			wire	[BRAM_FULL_BITS-1:0]	data_dout;

			// 将从机传来的数据暂存准备写入data_fifo中
			always @(posedge i_axi_clk or negedge axi_rstn_sync) begin
				if(~axi_rstn_sync) begin
					data_din  <= {BRAM_FULL_BITS{1'b0}};
					data_wren <= 1'b0;
				end 
				else begin
					data_din  <= {{FILL_WIDTH{1'b0}},i_axi_r_last,iv_axi_r_data};
					data_wren <= i_axi_r_valid;
				end
			end

			// 将从data fifo读出数据的data和last信号缓存后用于移位输出
			always @(posedge i_clk or negedge rstn_sync) begin
				if(~rstn_sync) 
					rd_data_fifo_out <= {AXI_DATA_WIDTH{1'b0}};
				else if(data_rden)
					rd_data_fifo_out <= data_dout[AXI_DATA_WIDTH-1:0];
				else if(rd_data_flag)
					rd_data_fifo_out <= (rd_data_fifo_out >> USER_DATA_WIDTH);
				else
					rd_data_fifo_out <= rd_data_fifo_out;
			end

			always @(posedge i_clk or negedge rstn_sync) begin
				if(~rstn_sync)
					rd_data_fifo_last <= 1'b0;
				else if(rd_data_flag && (cnt_flag == MAX_CNT_FLAG))
					rd_data_fifo_last <= 1'b0;
				else if(data_rden && data_dout[AXI_DATA_WIDTH])
					rd_data_fifo_last <= 1'b1;	// 在读出FIFO中的最后一个数据时拉高
				else
					rd_data_fifo_last <= rd_data_fifo_last;
			end

			// 此信号是针对data fifo的
			// 由于data_fifo的数据来自从机,在AXI时钟域下,用户时钟一般慢,所以data_fifo容易写满.
			// 此信号就是在FIFO容量不足一次读触发的数据量时,控制FSM不进入读通道的写数据状态.
			// AXI4规定一次突发的字节数不能超过256
			assign	rd_data_buf_ready = rstn_sync ? (data_wrcount <= 10'd256) : 1'b0;

			// FIFO例化
			fifo_w72xd512 wr_data_fifo (
			  	.rst           (~axi_rstn_sync),            // input wire rst
			  	.wr_clk        (i_axi_clk),                	// input wire wr_clk
			  	.rd_clk        (i_clk),               		// input wire rd_clk
			  	.din           (data_din),                  // input wire [287 : 0] din
			  	.wr_en         (data_wren),                	// input wire wr_en
			  	.rd_en         (data_rden),                	// input wire rd_en
			  	.dout          (data_dout),                	// output wire [287 : 0] dout
			  	.full          (data_wrfull),               // output wire full
			  	.empty         (data_rdempty),             	// output wire empty
			  	.rd_data_count (data_rdcount),  			// output wire [9 : 0] rd_data_count
			  	.wr_data_count (data_wrcount)  				// output wire [9 : 0] wr_data_count
			);
		end
	endgenerate

/*-------------------------------------------*\
				用户端数据输出
\*-------------------------------------------*/

	// 将移位的数据输出给用户端
	always @(posedge i_clk or negedge rstn_sync) begin : proc_
		if(~rstn_sync) begin
			o_user_rd_valid <= 1'b0;
			ov_user_rd_data <= {USER_DATA_WIDTH{1'b0}};
		end 
		else if(rd_data_flag)begin	// rd_data_fifo_out是128位的,每次输出其低16位即可(一直在进行移位).
			o_user_rd_valid <= 1'b1;
			ov_user_rd_data <= rd_data_fifo_out[USER_DATA_WIDTH-1:0];
		end
		else begin		// 其他情况下直接赋0即可
			o_user_rd_valid <= 1'b0;
			ov_user_rd_data <= {USER_DATA_WIDTH{1'b0}};
		end
	end
	
	// 输出last信号
	always @(posedge i_clk or negedge rstn_sync) begin
		if(~rstn_sync)
			o_user_rd_last <= 1'b0;
		else if(rd_data_fifo_last && (cnt_flag == MAX_CNT_FLAG))
			o_user_rd_last <= 1'b1;		// 只需要拉高1个时钟周期即可
		else
			o_user_rd_last <= 1'b0;
	end

/*-------------------------------------------*\
				FIFO调试信号输出
\*-------------------------------------------*/

	always @(posedge i_clk or negedge rstn_sync) begin
		if(~rstn_sync)
			o_rd_cmd_fifo_err <= 1'b0;
		else if(cmd_wrfull && cmd_wren)
			o_rd_cmd_fifo_err <= 1'b1;
		else
			o_rd_cmd_fifo_err <= 1'b0;
	end

	always @(posedge i_clk or negedge rstn_sync) begin
		if(~rstn_sync)
			o_rd_data_fifo_err <= 1'b0;
		else if(data_wrfull && data_wren)
			o_rd_data_fifo_err <= 1'b1;
		else
			o_rd_data_fifo_err <= 1'b0;
	end

endmodule
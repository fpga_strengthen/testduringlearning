`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date: 2024/08/22 15:18:39
// Module Name: udp_send
// Tool Versions: 
// Description: 将用户发送的数据进行打包(添加UDP头部)发送.
// Revision 0.01 - File Created
//////////////////////////////////////////////////////////////////////////////////

module udp_send #(
	parameter	LOCAL_PORT  = 16'h0,	// 本地端口号
	parameter 	TARGET_PORT = 16'h0		// 目的端口号
	)(
	// 时钟和复位
	input wire			i_clk,
	input wire			i_rst_n,

	/*---------------- ip_send 模块交互信号--------------------*/
	output reg			o_udp_tx_data_vld,		// 数据有效信号
	output reg			o_udp_tx_data_last,		// UDP层数据的最后一个数据的标志
	output reg	[7:0]	ov_udp_tx_data,			// 将用户端的数据打包后发送
	output reg	[15:0]	ov_udp_tx_length,		// 帧数据长度

	/*-------------------- 用户端交互信号 --------------------*/
	input wire			i_app_tx_data_vld,		// 数据有效标志
	input wire			i_app_tx_data_last,		// 数据部分最后一个
	input wire	[7:0]	iv_app_tx_data,			// UDP层数据
	input wire	[15:0]	iv_app_tx_length,		// UDP层数据包长度

	// 用户端发数据请求信号,避免数据重叠或冲突
	output	wire		o_app_tx_req,			// 用户端发送情趣
	output	reg			o_app_tx_ready			// 用户端准备完成信号
);

/*-----------------------------------------------*\
					信号定义
\*-----------------------------------------------*/	
	
	// 以太网最小帧长
	localparam			MAX_CNT_READY = 6'd40;	// 大于28即可,即UDP头部8字节+IP头部20字节

	reg		[15:0]		app_length_lock;	// 锁存数据长度
	reg		[10:0]		cnt_tx_byte;		// 发送字节计数器
	wire	[7:0]		app_data_delay8;	// 将输入数据延迟8拍
	reg		[5:0]		cnt_ready;

	// req信号暂时这样写
	assign	o_app_tx_req = i_app_tx_data_vld;

/*-----------------------------------------------*\
				锁存数据长度信号
\*-----------------------------------------------*/	

	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n) 
			app_length_lock <= 16'h0;
		else if(i_app_tx_data_vld)
			app_length_lock <= iv_app_tx_length;
		else
			app_length_lock <= app_length_lock;
	end

/*-----------------------------------------------*\
				发送字节计数器
\*-----------------------------------------------*/	
	
	/*
		发送的数据包有最少帧长要求(46byte),除去帧头(包括UDP层),用户端最小的数据长度为46-20-8=18btye
		因此用户端发送来的数据长度小于18时,需要进行填充再发送,填充足够后再加UDP头，就将发送计数器清零;
		填充到满足18的长度后,发送时还要添加UDP头部(8byte),所以计数器还要加8,即(18-1)+8=25
		减1是因为字节计数器从0开始计数,到17时就是18字节了,再加8个字节就是25了.
	*/
	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n)
			cnt_tx_byte <= 11'd0;
		else if((app_length_lock < 'd18)&&(cnt_tx_byte == 'd25))
			cnt_tx_byte <= 11'd0;		// 不足最小帧长需要填充后，清零
		else if((app_length_lock >= 'd18)&&(cnt_tx_byte == app_length_lock + 'd7))	// 'd7 = 'd8 - 'd1
			cnt_tx_byte <= 11'd0;		// 计数到最大长度时清零,注意length_lock就是真实的长度,传来时最少是1,而计数器从0开始,所以这里加7而不是8
		else if(i_app_tx_data_vld || (cnt_tx_byte != 'd0))
			cnt_tx_byte <= cnt_tx_byte + 1'b1;
		else
			cnt_tx_byte <= cnt_tx_byte;
	end

/*-----------------------------------------------*\
					UDP长度
\*-----------------------------------------------*/	

	// 这里的长度最小是1,不是0,所以最小帧长是18,不减1
	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n)
			ov_udp_tx_length <= 'd0;
		else if(iv_app_tx_length <= 'd18)
			ov_udp_tx_length <= 'd26;	// 最小长度18字节 + UDP首部8字节
		else if(iv_app_tx_length > 'd18)
			ov_udp_tx_length <= iv_app_tx_length + 'd8;
		else
			ov_udp_tx_length <= ov_udp_tx_length;
	end

/*-----------------------------------------------*\
					UDP组包
\*-----------------------------------------------*/	

	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n)
			ov_udp_tx_data <= 8'h0;
		else begin 
			case (cnt_tx_byte)
				// 源端口号
				0 : ov_udp_tx_data <= LOCAL_PORT[15:8];
				1 : ov_udp_tx_data <= LOCAL_PORT[ 7:0];
				
				// 目的端口号
				2 : ov_udp_tx_data <= TARGET_PORT[15:8];
				3 : ov_udp_tx_data <= TARGET_PORT[ 7:0];
				
				//UDP长度
				4 : ov_udp_tx_data <= ov_udp_tx_length[15:8];
				5 : ov_udp_tx_data <= ov_udp_tx_length[ 7:0];
				
				// 首部校验和,暂时可以忽略,因为MAC层的CRC正确了这里就没什么问题.
				6 : ov_udp_tx_data <= 8'h0;
				7 : ov_udp_tx_data <= 8'h0;

				default : ov_udp_tx_data <= app_data_delay8;
			endcase
		end
	end

/*-----------------------------------------------*\
			将数据延迟8拍,留出UDP首部的时间
\*-----------------------------------------------*/	
	data_delay20 iv_data_delay8 (
	  	.A(6'd7),      				// 要延迟N拍，则输入(N-1)
	  	.D(iv_app_tx_data),      	// input wire [7 : 0] D
	  	.CLK(i_clk),  				// input wire CLK
	  	.Q(app_data_delay8)      	// output wire [7 : 0] Q
	);

/*-----------------------------------------------*\
					last信号
\*-----------------------------------------------*/	

	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n)
			o_udp_tx_data_last <= 1'b0;
		else if((app_length_lock < 'd18)&&(cnt_tx_byte == 'd25))
			o_udp_tx_data_last <= 1'b1;
		else if((app_length_lock >= 'd18)&&(cnt_tx_byte == app_length_lock + 'd7))
			o_udp_tx_data_last <= 1'b1;
		else
			o_udp_tx_data_last <= 1'b0;
	end

/*-----------------------------------------------*\
					数据有效信号
\*-----------------------------------------------*/		

	// 从数据有效开始,一直持续到last信号时拉低
	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n)
			o_udp_tx_data_vld <= 1'b0;
		else if(o_udp_tx_data_last)
			o_udp_tx_data_vld <= 1'b0;
		else if(i_app_tx_data_vld)
			o_udp_tx_data_vld <= 1'b1;
		else
			o_udp_tx_data_vld <= o_udp_tx_data_vld;
	end

/*-----------------------------------------------*\
					发送准备信号
\*-----------------------------------------------*/	

	// 主要作用是控制用户端两次发送数据之间的时间间隔(因为要空出时间添加UDP首部)
	// 添加UDP首部和IP首部需要的时间分别为8字节和20字节,所以至少累计需要28字节.
	// 而这两次添加都是在同一时钟域之下,在MAC层是有FIFO对数据进行缓冲的,时间完全足够.
	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n)
			cnt_ready <= 6'd0;
		else if(cnt_ready == MAX_CNT_READY)
			cnt_ready <= 6'd0;
		else if(i_app_tx_data_last || (cnt_ready != 0))
			cnt_ready <= cnt_ready + 1'b1;
		else
			cnt_ready <= cnt_ready;
	end

	// 准备完成信号在两段数据之间都视为无效,此时用户端不应当发数据.
	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n)
			o_app_tx_ready <= 1'b1;		// 默认是可以发送的,时间充足
		else if(cnt_ready != 0)
			o_app_tx_ready <= 1'b0;
		else
			o_app_tx_ready <= 1'b1;
	end


endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date: 2024/08/20 13:45:21
// Module Name: ip_receive
// Description: 从IP数据包中去掉IP首部,提取出IP数据部分.
//////////////////////////////////////////////////////////////////////////////////

module ip_receive #(
	parameter	LOCAL_IP_ADDR = 32'h0			// 本地IP地址,将来可以在顶层更改
	)(
	// 时钟和复位
	input wire			i_clk,					// 用户接收端时钟
	input wire			i_rst_n,

	/*---------------- mac_to_arp_ip 模块交互信号--------------------*/
	input wire			i_ip_rx_vld,			// 数据有效信号
	input wire			i_ip_rx_data_last,		// 最后一个数据的标志
	input wire	[7:0]	iv_ip_rx_data,			// 数据

	/*---------------- udp_receive 模块交互信号--------------------*/
	output	reg			o_udp_rx_vld,			// 数据有效标志
	output	reg			o_udp_rx_data_last,		// 数据部分最后一个
	output	reg	[7:0]	ov_udp_rx_data,			// UDP层数据
	output 	reg	[15:0]	ov_udp_rx_length,		// UDP层数据包长度

	/*-------------------- icmp 模块交互信号----------------------*/
	output	reg			o_icmp_rx_vld,			// 数据有效标志
	output	reg			o_icmp_rx_data_last,	// 数据部分最后一个
	output	reg	[7:0]	ov_icmp_rx_data,		// UDP层数据
	output 	reg	[15:0]	ov_icmp_rx_length		// UDP层数据包长度
);

	// IP首部中协议字节
	localparam  UDP_TYPE  = 8'd17;
	localparam  ICMP_TYPE = 8'd1;

/*-----------------------------------------------*\
					信号定义
\*-----------------------------------------------*/

	reg	[10:0]	cnt_rx_byte;			// 接收的字节总长度
	reg	[7:0]	protocal_type;			// 协议类型
	reg	[15:0]	total_length;			// 总长度
	reg	[31:0]	target_ip_addr;			// 目的IP地址

/*-----------------------------------------------*\
		获取协议类型、总长度和目的地址
\*-----------------------------------------------*/

	// 字节计数
	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n) 
			cnt_rx_byte <= 11'd0;
		else if(i_ip_rx_vld)
			cnt_rx_byte <= cnt_rx_byte + 1'b1;
		else
			cnt_rx_byte <= 11'd0;
	end

	// 接收总长度
	always @(posedge i_clk or negedge i_rst_n) begin 
		if(~i_rst_n) 
			total_length <= 16'd0;
		else  if((cnt_rx_byte == 11'd2) || (cnt_rx_byte == 11'd3))
			total_length <= {total_length[7:0],iv_ip_rx_data};
		else
			total_length <= total_length;
	end

	// 接收协议
	always @(posedge i_clk or negedge i_rst_n) begin 
		if(~i_rst_n) 
			protocal_type <= 8'b0;
		else if(cnt_rx_byte == 11'd9)
			protocal_type <= iv_ip_rx_data;
		else
			protocal_type <= protocal_type;
	end

	// 接收目的地址
	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n) 
			target_ip_addr <= 32'h0;
		else if((cnt_rx_byte >= 11'd16)&&(cnt_rx_byte <= 11'd19))
			target_ip_addr <= {target_ip_addr[23:0],iv_ip_rx_data};
		else
			target_ip_addr <= target_ip_addr;
	end

/*-----------------------------------------------*\
					输出UDP数据
\*-----------------------------------------------*/
	
	// 当协议为UDP且IP地址一致时,就接收数据部分
	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n) begin
			ov_udp_rx_data     <= 8'b0;
			o_udp_rx_vld       <= 1'b0;
			o_udp_rx_data_last <= 1'b0;
			ov_udp_rx_length    <= 16'd0;
		end
		else if((protocal_type == UDP_TYPE)&&(target_ip_addr == LOCAL_IP_ADDR)&&(cnt_rx_byte >= 11'd20))begin 
			ov_udp_rx_data     <= iv_ip_rx_data;
			o_udp_rx_vld       <= i_ip_rx_vld;
			o_udp_rx_data_last <= i_ip_rx_data_last;
			ov_udp_rx_length    <= total_length - 16'd20;	// 数据长度为总长度减去IP首部长度
		end
		else begin 
			ov_udp_rx_data     <= 8'h0;
			o_udp_rx_vld       <= 1'b0;
			o_udp_rx_data_last <= 1'b0;
			ov_udp_rx_length    <= 16'h0;
		end
	end

/*-----------------------------------------------*\
					输出ICMP数据
\*-----------------------------------------------*/

	// 当协议为ICMP且IP地址一致时,就接收数据部分
	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n) begin
			ov_udp_rx_data     <= 8'b0;
			o_udp_rx_vld       <= 1'b0;
			o_udp_rx_data_last <= 1'b0;
			ov_icmp_rx_length  <= 16'd0;
		end
		else if((protocal_type == ICMP_TYPE)&&(target_ip_addr == LOCAL_IP_ADDR)&&(cnt_rx_byte >= 11'd20))begin 
			ov_icmp_rx_data     <= iv_ip_rx_data;
			o_icmp_rx_vld       <= i_ip_rx_vld;
			o_icmp_rx_data_last <= i_ip_rx_data_last;
			ov_icmp_rx_length   <= total_length - 16'd20;	// 数据长度为总长度减去IP首部长度
		end
		else begin 
			ov_icmp_rx_data     <= 8'h0;
			o_icmp_rx_vld       <= 1'b0;
			o_icmp_rx_data_last <= 1'b0;
			ov_icmp_rx_length   <= 16'h0;
		end
	end

endmodule
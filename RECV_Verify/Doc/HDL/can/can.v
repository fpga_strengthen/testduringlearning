//+FHEADER /////////////////////////////////////////////////////////////////////////////
//  版权所有者： 
//  Copyright 2014. All Rights Reserved.
//  保密级别：  
//-------------------------------------------------------------------------------
//  文件名 :  Can.v
//  原始作者：江颖洁
//  联系方式：电子邮件: yanranzhao@sina.com
// --------------------------------------------------------------------------
// 版本升级历史：删去在顶层的一些功能，在顶层只做例化
// 版本生效日期： 
// 1.0 2014-02-1  江颖洁 第1版
// 2.0 2015-11-13 郭庆阳 第2版 
// 2020.6.8  王雪 
//---------------------------------------------------------------------------
// 关键字 :  can bus, can control, top module of can
//--------------------------------------------------------------------------
// 模块功能:  模块例化以及spi数据的整合
//            
// --------------------------------------------------------------------------
// 参数文件名：  
// -----------------------------------------------------------------------
// 重复使用问题：
// 复位策略:负跳变沿异步复位信号 i_rst_n
// 时钟域 : i_clk_27M 
// 关键时序: N/A
// 是否需要实例引用别的模块 :YES
// 可综合否 : Yes 
//-FHEADER //////////////////////////////////////////////////////////////////////
module can(
          i_clk_27M,              //27M时钟
          i_rst_n,                //复位信号低有效
          //连接到MCP2515管脚
          i_so,                   //spi输入信号，即总线上接收到的数据
          o_si,                   //spi输出信号，即发送到总线上的数据
          o_sclk,                 //spi时钟
          o_cs,                   //spi使能（低有效)
          o_can_osc,              //给到mcp2515的时钟
          o_rst_can,              //can芯片复位信号
          i_int,                  //can中断信号，低有效（有中断表示总线在被占用）
          //配置
          iv_set_id,              //初始化配置参数以及id(初始必须为64'hFFFF_FFFF_FFFF_FFFF)
                                  //开始发送和接收前必须先按需求参考配置协议完成配置
          //发送
          i_byte_ready,           //字节有效脉冲,持续一个时钟(每开始发送一帧数据拉高一次)
          iv_msg_data_in,         //状态帧数据输入,数据一直维持,
                                  //直到当前字节发送完成o_sendbyte_ok拉高,才换为下一字节
          o_sendbyte_ok,          //发完一个字节,读取下一字节信号
          o_send_frame_ok,        //发送一帧完成
          //接收         
          ov_recv_data,           //can接收字节数据
          o_databyte_en,          //高电平时数据有效
          o_databyte_clk,         //接收数据字节时钟,en为高电平时有效
          o_receive_ok_can        //接收完一帧标志
          );

    input i_clk_27M;
    input i_rst_n;
    
    input i_so;
    input i_int;
    input [63:0]iv_set_id;
    input i_byte_ready;
    input [7:0]iv_msg_data_in;
    
    output o_si;
    output o_sclk;
    output o_cs;
    output o_rst_can;
    output o_can_osc;
    output wire [7:0]ov_recv_data;
    output wire o_databyte_en;
    output wire o_databyte_clk;
    output wire o_sendbyte_ok;
    output wire o_send_frame_ok;
    output wire o_receive_ok_can;
//////////////////////////////////打两拍的处理  
    reg i_int_temp1;
    reg i_int_temp2;         
//////////////////////////////////
//定义一些各子模块输入输出的数据变量
    wire[7:0] spi_data;
    wire[7:0] spi_data_sd;
    wire[7:0] spi_data_rd;
    wire[7:0] spi_data_rst;
    wire[7:0] spi_data_mody;
    
    wire spi_tx_en;
    wire spi_tx_en_sd;
    wire spi_tx_en_rd;
    wire spi_tx_en_rst;
    wire spi_tx_en_my;
    
    wire spi_rx_en_rd;
    
    wire[7:0] send_data;
    wire[7:0] send_add;
    wire[7:0] send_data_int;
    wire[7:0] send_add_int;
    wire[7:0] send_data_fra;
    wire[7:0] send_add_fra;
    
    wire[7:0] read_add;
    wire[7:0] read_num;
    wire[7:0] read_add_int;
    wire[7:0] read_num_int;
    wire[7:0] read_add_fra;
    wire[7:0] read_num_fra;
    wire[7:0] read_add_refra;
    wire[7:0] read_num_refra; 
  
    wire send_en;
    wire read_en;  
    wire send_en_int;
    wire read_en_int;  
    wire send_en_fra;
    wire read_en_fra;
    wire read_en_refra; 
    
    wire mody_en;
    wire mody_en_fra;
    wire mody_en_refra;
  
    wire[7:0] mody_add;
    wire[7:0] mody_add_fra;
    wire[7:0] mody_add_refra;
  
    wire[7:0] mody_data;
    wire[7:0] mody_data_fra;
    wire[7:0] mody_data_refra;
  
    wire[7:0] mody_screen;
    wire[7:0] mody_screen_fra;
    wire[7:0] mody_screen_refra;
    
    wire send_msg_req;
    wire send_frame_en;
    wire read_frame_en; 
   
    wire send_reg_ok;
    wire send_ok;
    wire read_ok;
    wire read_byte;
    wire mody_ok;
    wire send_frame_ok;
    wire read_frame_ok;
    wire reset_ok;
        
    wire reset_en;
    wire i_rst_n;  
        
    wire[7:0] reg_out;
    wire[7:0] somi;
    wire byte_clk;
    wire can_init_finish;
    wire init_state_en;
    wire[7:0] msg_data_in;
   
///////////////////////////////////////////
    assign o_send_frame_ok = send_frame_ok;
    assign o_can_osc  = i_clk_27M;//主can时钟
    assign spi_data = spi_data_rst | spi_data_sd | spi_data_mody | spi_data_rd;
    assign spi_tx_en = spi_tx_en_rst | spi_tx_en_sd | spi_tx_en_my | spi_tx_en_rd;
 
    assign send_en = send_en_int | send_en_fra ;
    assign read_en = read_en_int | read_en_fra |read_en_refra ;
    assign read_add = read_add_int | read_add_fra | read_add_refra ;
    assign read_num = read_num_int | read_num_fra | read_num_refra ;
    assign send_data = send_data_int | send_data_fra ;
    assign send_add = send_add_int | send_add_fra ;
    assign mody_en = mody_en_fra | mody_en_refra ;
    assign mody_add = mody_add_fra | mody_add_refra ;
    assign mody_data = mody_data_fra | mody_data_refra;
    assign mody_screen = mody_screen_fra | mody_screen_refra ;
////////////////////////////////////////////////对来自mcp2515的信号进行处理
//片间通信要有跨时钟域的处理，中断在此处处理，而数据在can_spi中做处理
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if(!i_rst_n)begin
            i_int_temp1 <= 1'b1;
            i_int_temp2 <= 1'b1;             
        end
        else begin
            i_int_temp1 <= i_int;
            i_int_temp2 <= i_int_temp1;
        end
    end  
                             
////////////////////////////////////////////////
    can_control u_can_control(
         .i_clk_27M(i_clk_27M),
         .i_rst_n(i_rst_n),

         .i_can_init_finish(can_init_finish),
         .i_int(i_int_temp2),         
         .i_read_frame_ok(read_frame_ok),  
         .i_send_msg_en(send_msg_req), 
         .i_send_frame_ok(send_frame_ok),
         //.i_change_addr_en(o_sendbyte_ok),
         
         .o_init_state_en(init_state_en),
         .o_read_frame_en(read_frame_en),
         .o_send_frame_en(send_frame_en)
         ); 
//can初始化，包括发出复位和初始化信息并确认初始化成功
    can_init u_can_init
        (
        //time & rst
        .i_clk_27M(i_clk_27M),
        .i_rst_n(i_rst_n),
        
        .iv_set_id(iv_set_id),
        
        .i_init_state_en(init_state_en ),       //主状态在初始化的位置，执行初始化操作
        .i_send_reg_ok(send_reg_ok),    //发送完一个字节得到一个ok信号
        .i_read_ok(read_ok),            //读取完一条帧
        .i_reset_ok(reset_ok),          //复位完成
        //data in
        .i_reg_out(reg_out),            //spi输出数据
        
        //output
        .o_can_init_finish(can_init_finish),//初始化完成
        .o_send_en(send_en_int),            //初始化信息发送有效信号
        .o_read_en(read_en_int),            //初始化结果接收有效信号
        .o_mcp_rst(o_rst_can),  
        .o_reset_en(reset_en),              //复位(通过spi命令实现)有效
        
        //data out
        .o_send_add(send_add_int),          //初始化配置地址    
        .o_send_data(send_data_int),        //初始化数据内容
        .o_read_add(read_add_int),
        .o_read_num(read_num_int)
        );
        
//发送状态帧
    can_send_msg_frame  u_can_send_msg_frame
        (
        //time & rst
        .i_clk_27M(i_clk_27M),
        .i_rst_n(i_rst_n),
        
        //input control
        .i_mcp_int(i_int_temp2),                //中断
        .i_send_frame_en(send_frame_en),        //发状态使能
        .i_send_reg_ok(send_reg_ok),            //单字节发送完成信号
        .i_read_ok(read_ok),    
        .i_mody_ok(mody_ok),                    //中断清零标志
        .i_busy(spi_tx_en),
        
        //data input
        .i_reg_out(reg_out),
        .i_byte_ready(i_byte_ready),
        .i_msg_data_in(iv_msg_data_in),
        .iv_set_id(iv_set_id),
        
        //output
        .o_send_req(send_msg_req),
        .o_send_frame_ok(send_frame_ok),        //标准帧发送完成
        .o_send_en(send_en_fra),                //标准帧发送起始标志
        .o_read_en(read_en_fra),                //读发送状态
        .o_mody_en(mody_en_fra),                //位修改状态
        .o_sendbyte_ok(o_sendbyte_ok),
        
        //data out
        .o_send_add(send_add_fra),          //标准帧发送地址
        .o_send_data(send_data_fra),        //发送状态数据
        .o_read_add(read_add_fra),          //读中断地址
        .o_read_num(read_num_fra),          //读字节数
        .o_mody_add(mody_add_fra),          //修改地址
        .o_mody_data(mody_data_fra),        //修改数据
        .o_mody_screen(mody_screen_fra)     //shield some bits       
        ); 
    
//读can标准帧
    can_read_frame u_can_read_frame
        (
        //time & rst
        .i_clk_27M(i_clk_27M),
        .i_rst_n(i_rst_n),
        
        //input control
        .i_mcp_int(i_int_temp2),
        .i_read_frame_en(read_frame_en), 
        .i_read_ok(read_ok),
        .i_read_byte(read_byte),                //读取缓冲区中数据
        .i_mody_ok(mody_ok),                    //写修改完成
        
        //data input
        .i_reg_out(reg_out),             
        
        //output
        .o_read_frame_ok(read_frame_ok),
        .o_read_en(read_en_refra),
        .o_mody_en(mody_en_refra),
        
        //data out
        .ov_recv_data(ov_recv_data),
        .o_read_add(read_add_refra),
        .o_read_num(read_num_refra),
        .o_mody_add(mody_add_refra),
        .o_mody_data(mody_data_refra),
        .o_mody_screen(mody_screen_refra),
        .o_databyte_clk(o_databyte_clk),
        .o_receive_ok_can(o_receive_ok_can),
        .o_databyte_en (o_databyte_en)
        );
    
//发送一字节数据
    can_send_register u_can_send_register
        (
         //time & rst
        .i_clk_27M(i_clk_27M),
        .i_rst_n(i_rst_n),
        
        //input control
        .i_send_ok(send_ok),
        .i_send_en(send_en),
        
        //input data 
        .i_send_add(send_add),
        .i_send_data(send_data),
        
        //output control
        .o_spi_tx_en(spi_tx_en_sd),     //spi发送
        .o_send_reg_ok(send_reg_ok),    //发送完成
        
        //output data
        .o_spi_data(spi_data_sd)
        );       
//接收一字节数据
    can_read_register u_can_read_register
        (
         //time & rst
        .i_clk_27M(i_clk_27M),
        .i_rst_n(i_rst_n),
        
        //input control
        .i_send_ok(send_ok),
        .i_read_en(read_en),
        .i_read_ok(read_ok),
        //input data 
        .i_read_add(read_add),
        
        //output control
        .o_spi_tx_en(spi_tx_en_rd),
        .o_spi_rx_en(spi_rx_en_rd),
        
        //output data
        .o_spi_data(spi_data_rd)
        );
         
//接受或发送完一帧数据后对中断寄存器清零
    can_bit_mody u_can_bit_mody
         (
          //time & rst
         .i_clk_27M(i_clk_27M),
         .i_rst_n(i_rst_n),
         
         //input control
         .i_send_ok(send_ok),
         .i_mody_en(mody_en),
         
         //input data 
         .i_send_add(mody_add),
         .i_send_data(mody_data),
         .i_screen_data(mody_screen),   //shield some bits
         
         //output control
         .o_spi_tx_en(spi_tx_en_my),
         .o_mody_ok(mody_ok),
         
         //output data
         .o_spi_data(spi_data_mody)
         );
        
//spi接口底层发送控制
    can_spi u_can_spi
        (
        .clk_27M(i_clk_27M),
        .rst_n(i_rst_n),
        .mcp_simo(o_si),            //发送的指令、地址、数据信号
        .mcp_somi(i_so),        //接收到的数据信号
        .mcp_sclk(o_sclk),          //spi时钟
        .o_cs(o_cs),        
        .spi_data(spi_data),        //要发送的数据(并行)
        .spi_tx_en(spi_tx_en),  
        .spi_rx_en(spi_rx_en_rd),
        .byte_clk(byte_clk),        //发送字节完成时钟
        .somi(somi),
        .send_ok(send_ok)
        );

//spi接口底层接收处理
    can_tempstorage u_can_tempstorage
        (
        //time & rst
        .i_clk_27M(i_clk_27M),
        .i_rst_n(i_rst_n),
        
        //input control
        .i_byte_clk(byte_clk),          
        
        //data in
        .i_read_data(somi),
        .i_read_num(read_num),
        
        //output
        .o_read_ok(read_ok),
        .o_read_byte(read_byte),
        
        //data out
        .o_reg_out(reg_out)
        );
             
//复位
    can_reset u_can_reset
       (
        //time & rst
       .i_clk_27M(i_clk_27M),
       .i_rst_n(i_rst_n),
       
       //input control
       .i_send_ok(send_ok),
       .i_reset_en(reset_en),
       
       //output control
       .o_spi_tx_en(spi_tx_en_rst),
       .o_reset_ok(reset_ok),
       
       //output data
       .o_spi_data(spi_data_rst)
       );                        
       
endmodule
function [ im_datain ,hilbert_data_add_5,data_H5,im_datain_solve] = hilbert_trans_63M_collect( i_data_ADC )
    % 采集板用
    %   本函数完成Hilbert变换，输出量为输入数据对应解析信号的虚部（采集板）
    %   输入信号为ADC采样数据，字长12bit
    %   输出为对应对应解析信号的虚部 字长34bit
    % -------------------------------参数声明-------------------------
    %% 希尔伯特滤波器参数
    H1   = 83443;
    H3   = 27814;
    H5   = 16688;
    Length_data = length(i_data_ADC);
    
    %% 根据框图设计Hilbert滤波器
    
    %对数据进行打拍
    hilbert_datain_delay_5_n = Shift( i_data_ADC, 1 );
    hilbert_datain_delay_4_n = Shift( i_data_ADC, 2 );
    hilbert_datain_delay_3_n = Shift( i_data_ADC, 3 );
    hilbert_datain_delay_2_n = Shift( i_data_ADC, 4 );
    hilbert_datain_delay_1_n = Shift( i_data_ADC, 5 );
    hilbert_datain_delay_0   = Shift( i_data_ADC, 6 );
    hilbert_datain_delay_1   = Shift( i_data_ADC, 7 );
    hilbert_datain_delay_2   = Shift( i_data_ADC, 8 );
    hilbert_datain_delay_3   = Shift( i_data_ADC, 9 );
    hilbert_datain_delay_4   = Shift( i_data_ADC, 10 ); 
    hilbert_datain_delay_5   = Shift( i_data_ADC, 11 );
    
    % 减法运算后重新定标
    hilbert_data_add_1 = FixPointNum( hilbert_datain_delay_1 - hilbert_datain_delay_1_n , 15, 0);%字长15bit，精度0位小数
    hilbert_data_add_3 = FixPointNum( hilbert_datain_delay_3 - hilbert_datain_delay_3_n , 15, 0);
    hilbert_data_add_5 = FixPointNum( hilbert_datain_delay_5 - hilbert_datain_delay_5_n , 15, 0);
    
    %乘法运算后重新定标
    data_H1 = FixPointNum( (H1.*hilbert_data_add_1) , 33, 0);%字长33bit，精度0位小数
    data_H3 = FixPointNum( (H3.*hilbert_data_add_3) , 33, 0);
    data_H5 = FixPointNum( (H5.*hilbert_data_add_5) , 33, 0);
    
    %相加得到解析信号的虚部
    im_datain_solve = FixPointNum(data_H5 + data_H3 + data_H1 , 35, 0);%字长35bit，精度0位小数  
    
    im_datain = FixPointNum( im_datain_solve./2^17 , 14, 0);%字长18bit，精度0位小数  
    
    end
    
    
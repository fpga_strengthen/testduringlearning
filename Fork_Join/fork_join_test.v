`include "dump_task.v"

`timescale 1ns/1ps

`define TIMESCALE 20
module fork_join_test(
    output  reg     a,b,c,d,e,f
);
    reg     clk = 0;
    
    initial begin : clock_generator
        forever #(`TIMESCALE) clk = ~clk;
    end

    initial $monitor($time,,,"a=%b,b=%b,c=%b,d=%b,e=%b,f=%b",a,b,c,d,e,f);

    initial begin:InitAndSimEnd
        a = 0;
        b = 0;
        c = 0;
        d = 0;
        e = 0;
        f = 0;
        repeat(5) @(posedge clk);
        $finish(2);     //等待5个clk的上升沿之后仿真就会结束
    end

    //执行顺序说明
    //当开始执行时，所有的过程块会并行执行，因此仿真起始时刻，a~f均为0
    //对于always块，其中的fork-join下的语句会并行执行，但begin-end块内是顺序执行
    //因此在clk的上升沿到来后（20ns）后，再延时2ns，此时a,b,c,f均会更新
    //a = ~0 = 1, b = ~0 = 1, f = ~e = ~0 = 1, c = ~a = 0
    //即在22ns时：a=1,b=1,c=0,f=1
    //begin-end块内会顺序执行，c=~a是块内第一句，因此会和外边的几句同步执行
    //但d=~b和e=~c均在c=~a之后，因此再过2ns即24ns时刻执行d=~b，此时d=~b=~1=0，
    //此时的d相对于初始值并没有任何变化，因此monitor不会监测到也不会打印24ns时刻各变量的信息
    //如果想验证24ns时刻monitor的作用，可以将d的赋值改为1'bx，此时就会被检测到并打印
    //再过2ns之后，即26ns时刻，执行e=~c=~0=1，相对初始值e从0变为1，此时mointot会打印其信息
    //此时，即过了26ns之后，该always块内的语句才全部执行完毕；当下一次clk的上升沿到来后会继续重复
    always @(posedge clk) begin
        fork 
            #2 a = ~a;
            #2 b = ~b;

            begin 
                #2 c = ~a;
                #2 d = ~b;          //d = 1'bx
                #2 e = ~c;
            end

            #2 f = ~e;
        join
    end

endmodule
//-----------------------------------------------------------
//定义一个简单双口RAM的行为模型
//处理了一个关键问题：如何处理对同一地址的同时读写
//以下提供了两种处理方式
//1.读取原来存储的值，然后立刻写入新的值；
//2.不能读取原来存储的值，而是直接获得正在写入的值
//下面通过宏定义自动的设定了这两种方式
//-----------------------------------------------------------
module dpram#(
    parameter   ADDR_WIDTH = 6,         //地址宽度
    parameter   DATA_WIDTH = 8          //数据宽度
)(
    input   [(DATA_WIDTH-1):0]  data,
    input   [(ADDR_WIDTH-1):0]  read_addr,
    input   [(ADDR_WIDTH-1):0]  write_addr,
    input   we,
    input   clk,
    output  [(DATA_WIDTH-1):0]  q
);

    //定义存储器，作为简单双口RAM的FPGA模型
    reg [(DATA_WIDTH - 1):0]  ram [2**ADDR_WIDTH - 1:0];

    `ifdef DPRAM_OLD_DATA
        //方式1
        reg [(DATA_WIDTH - 1):0]    q_out;
        
        always@(posedge clk)begin 
            if(we)
                ram[write_addr] <= data;        //写使能有效时，写入数据
            q_out <= ram[read_addr];            //写入之后就读出
        end
        assign  q = q_out;
    `else
        //方式2
        reg [(ADDR_WIDTH - 1):0]    read_addr_reg;
        
        always@(posedge clk)begin
            if(we)  
                ram[write_addr] <= data;        //写使能有效时，写入数据
        end
        
        //这里对读地址打一拍后读取，会读以前的数据
        always@(posedge clk) read_addr_reg <= read_addr;        
        
        assign  q = ram[read_addr_reg];
    `endif

    //初始化为全0
    //注意：在FPGA中RAM的初始化语句是可综合的，用于初始RAM内容或加载初始值
    `ifdef  BLANK_RAM
        integer i;
        initial begin
            for(i = 0;i < 2**ADDR_WIDTH - 1 ;i=i+1)begin
               ram[i] = 0; 
            end
        end
    `endif

endmodule
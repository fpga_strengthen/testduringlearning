//-------------------------------------------------------------------------------------------------                                                                             
//                                                                                                                                                                              
//--- (C) COPYRIGHT 2014 BUPT
//--- ALL RIGHTS RESERVED
//--- File      : Crc16.v
//--- Auther    : Lv Yaqin
//--- Date      : 2014.10.30
//--- Version   : v1.0.0
//--- Abstract  : CRC 16bit。
//-------------------------------------------------------------------------------------------------                                                                             
//                                                                                                                                                                              
//  Description :	1.input "iv_data", output "ov_checksum16"
//--------------------------------------------------------------------------
// 模块功能:  16位CRC校验
//            ……………………………………………………………………………
// --------------------------------------------------------------------------                                                                                                                                                                             
//-------------------------------------------------------------------------------------------------                                                                             
//--- Modification History:
//--- Date               	By           			Version               	  Change Description 
//-------------------------------------------------------------------------------------------------
//--- 2014.10		         	Lv Yaqin            		v1.0.0	                                                                   
//---                                            
//---
//-------------------------------------------------------------------------------------------------   
module Crc16(
		i_clk_27M,//主时钟
		i_rst_n,//复位
		iv_data,//外部输入信号
		i_crc_ctr,//控制信号，高点平时进行校验
		i_crc_clr,//清零信号
		ov_checksum16//校验输出
		);					

input 	i_clk_27M;
input		i_rst_n;
input 	[7:0]iv_data;
input		i_crc_ctr;
input		i_crc_clr;
output 	[15:0]ov_checksum16;



function [15:0] nextCRC16_D8;//校验函数

    input [7:0] Data;
    input [15:0] crc;
    reg [7:0] d;
    reg [15:0] c;
    reg [15:0] newcrc;
  begin
    d = Data;
    c = crc;

    newcrc[0] = d[7] ^ d[6] ^ d[5] ^ d[4] ^ d[3] ^ d[2] ^ d[1] ^ d[0] ^ c[8] ^ c[9] ^ c[10] ^ c[11] ^ c[12] ^ c[13] ^ c[14] ^ c[15];
    newcrc[1] = d[7] ^ d[6] ^ d[5] ^ d[4] ^ d[3] ^ d[2] ^ d[1] ^ c[9] ^ c[10] ^ c[11] ^ c[12] ^ c[13] ^ c[14] ^ c[15];
    newcrc[2] = d[1] ^ d[0] ^ c[8] ^ c[9];
    newcrc[3] = d[2] ^ d[1] ^ c[9] ^ c[10];
    newcrc[4] = d[3] ^ d[2] ^ c[10] ^ c[11];
    newcrc[5] = d[4] ^ d[3] ^ c[11] ^ c[12];
    newcrc[6] = d[5] ^ d[4] ^ c[12] ^ c[13];
    newcrc[7] = d[6] ^ d[5] ^ c[13] ^ c[14];
    newcrc[8] = d[7] ^ d[6] ^ c[0] ^ c[14] ^ c[15];
    newcrc[9] = d[7] ^ c[1] ^ c[15];
    newcrc[10] = c[2];
    newcrc[11] = c[3];
    newcrc[12] = c[4];
    newcrc[13] = c[5];
    newcrc[14] = c[6];
    newcrc[15] = d[7] ^ d[6] ^ d[5] ^ d[4] ^ d[3] ^ d[2] ^ d[1] ^ d[0] ^ c[7] ^ c[8] ^ c[9] ^ c[10] ^ c[11] ^ c[12] ^ c[13] ^ c[14] ^ c[15];
    nextCRC16_D8 = newcrc;
  end
  endfunction


	
reg [15:0]ov_checksum16;
always @(negedge i_rst_n or posedge i_clk_27M )//CRC校验过程 
	if(!i_rst_n)	
		ov_checksum16 <= 16'b0;
	else if ( i_crc_ctr )	//i_crc_ctr高点平时进行循环校验
		ov_checksum16 <= nextCRC16_D8(iv_data,ov_checksum16);	
	else if( i_crc_clr )//高电平清零
		ov_checksum16 <= 16'd0;			
	else 
		ov_checksum16 <= ov_checksum16;//输出保持	

endmodule 
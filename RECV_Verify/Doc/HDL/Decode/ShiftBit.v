/*=======================================================================================*\
  Filename    : ShiftBit.v
  Author      : 陈献彬
  time        ：2016.07.05
  Description : 本模块根据同步码来完成移位拼接，形成正确顺序的报文。并作CRC76校验
                1.输入数据存入RAM；
                2.输入同步码iv_syncCode，根据同步码计算读缓冲区地址
                3.完成拼接后输出报文，包括开始信号、时钟、数据三部分
                4.拼接后数据进行正发码标志和解扰寄存器组初始值计算；
                5.拼接后数据进行CRC76校验。加使能信号后，对shift_ok(即CRC_ok信号)
                  以及正发码标志和解扰寄存器组初始值一起打拍后输出
                6.对数据缓冲RAM进行校验
               
  Called by   : 
  Revision History : 
  Author              Date                  Version          Description
  郭庆阳              20160817              V1.1.0  
  郭庆阳              20161014              V1.1.1           RAM地址改为0->92循环写，拼接的
                                                             整字节那做了一下修改  
  陈献彬              20160705              V1.1.2           加上对相同报文的防护                                                                
  Email       :chenxianbin_bupt@163.con
  Company     :

\*========================================================================================*/
module ShiftBit(
                i_clk_81M,
                i_rst_n,
                
                i_start,             //开始接收新数据报文标志，脉冲
                i_data_sck,         //11比特字iv_data的同步时钟
                iv_data,            //输入11比特字
                
                
                iv_syncCode,        //同步模块给出同步码
                i_syncCode_en,      //同步码有效信号
                
                o_frame_start,      //开始输出移位拼接后的报文，脉冲，比o_shiftedData_sck早一拍
                o_shiftedData_sck,  //移位拼接后的报文时钟
                ov_shiftedData,     //移位拼接后的报文                
                                
                o_shift_ok,         //移位拼接报文正确，实质是CRC76正确信号，脉冲                
                o_inversionBit,     //正反码标志，1为需要取反，与o_shift_ok对齐
                ov_ScrambleCode,    //解扰寄存器组初始值，与o_shift_ok对齐
                o_new_msg_flag      //低电平有效
                );                                   
input i_clk_81M;
input i_rst_n;
input i_data_sck;
input [10:0] iv_data;
input i_start;
input i_syncCode_en;
input [10:0] iv_syncCode;

output reg o_frame_start;
output reg o_shiftedData_sck;
output reg [10:0]ov_shiftedData;

output reg o_shift_ok;
output reg o_inversionBit;
output reg [31:0]ov_ScrambleCode;//相位值
output reg o_new_msg_flag;           //发送给transform模块的标志，标志新的报文到来
//------------------------------------------------------------------------------//
reg start_r;
reg start_p;
reg i_data_sck_r;
reg i_data_sck_p;
reg [10:0]iv_data_temp;
reg i_syncCode_en_r;
reg i_syncCode_en_p0;
reg i_syncCode_en_p1;
reg [10:0]iv_syncCode_temp;
reg [2:0]cnt_6;             //1到11的计数，将后面将要进行的工作分为6个时钟
reg wr_ram_en;
reg [6:0]wr_ram_addr;       //ram_data写地址0-92
reg [10:0]wr_ram_data;
reg rd_ram_en;
reg [6:0]rd_ram_addr;       //RAM读地址0—92 
wire [10:0]ram_out;         //ram中读出的数据
//将前后两个11比特ram_out分别寄存，用于两个相连11bit的拼接
reg [10:0]ram_out_temp1;//前11bit数据
reg [10:0]ram_out_temp2;//后11bit数据

reg [3:0]shift4;        //比特偏移量（同步码）的后四位
reg [10:0]data;         //通过比特偏移量的后四位判断应该移多少位，移位后的数据
reg [6:0]byte_cnt;      //用于计数总共发送了多少个移位后的11bit数据

reg inversionBit;
wire [43:0]multipy_data;//乘法器结果

wire [11:0]Bb;          //乘法器乘数，用于计算相位值
reg  [11:0]B;            //乘法器乘数，用于计算相位值            
reg  [2:0]Cb;            //报文固定部位的固定bit，参看报文编码说明 
reg  check_end_r0;       //输入数据到crc校验结束后需要几个时钟
reg  check_end_r1;
reg  check_end_r2;
reg  check_end_r3;
wire [74:0]crc_result;
reg  shift_ok_r;
reg  shift_ok_p;
reg  [6:0]cnt_89;        //长度为45的计数器，用于控制正确报文发送频率
//------------------------------------------------------------------------------//
always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if(!i_rst_n) begin
        start_r <= 1'b0;
        start_p <= 1'b0;
    end
    else begin
        start_r <= i_start;
        start_p <= i_start && (~start_r);
    end
end
//取沿
always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if(!i_rst_n)begin
        i_data_sck_r <= 1'b0;
        i_data_sck_p <= 1'b0;    
    end
    else begin
        i_data_sck_r <= i_data_sck;
        i_data_sck_p <= i_data_sck && (~i_data_sck_r);
    end
end
//将数据与取沿后脉冲对齐
always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if(!i_rst_n)
        iv_data_temp <= 1'b0;
    else
        iv_data_temp <= iv_data;
end
//取沿
always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if(!i_rst_n)begin
        i_syncCode_en_r  <= 1'b0;
        i_syncCode_en_p0 <= 1'b0;
        i_syncCode_en_p1 <= 1'b0;
    end
    else begin
        i_syncCode_en_r  <= i_syncCode_en;
        i_syncCode_en_p0 <= i_syncCode_en && (~i_syncCode_en_r);  
        i_syncCode_en_p1 <= i_syncCode_en_p0; 
    end
end
//将数据与取沿后脉冲对齐
always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if(!i_rst_n)
        iv_syncCode_temp <= 11'b0;
    else
        iv_syncCode_temp <= iv_syncCode;
end
//------------------------------------------------------------------------------//
//ram写地址，每个i_data_sck到来地址增1，0-92循环写 
always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if(!i_rst_n)begin
        wr_ram_en   <= 1'b0;       
        wr_ram_data <= 11'd0;
    end
    else if(start_p)begin
        wr_ram_en   <= 1'b0;        
        wr_ram_data <= 11'd0;
    end
    else if(wr_ram_addr == 7'd93) begin//第93个地址不写，直接归0
        wr_ram_en   <= 1'b0;        
        wr_ram_data <= 11'd0;
    end  
    else if(i_data_sck_p)begin
        wr_ram_en   <= 1'b1;       
        wr_ram_data <= iv_data_temp;
    end
    else begin
        wr_ram_en   <= 1'b0;        
        wr_ram_data <= wr_ram_data;
    end         
end

always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if(!i_rst_n)
        wr_ram_addr <= 7'b0;
    else if( start_p )
        wr_ram_addr <= 7'd0; 
    else if(wr_ram_addr == 7'd93)//第93个地址不写，直接归0
        wr_ram_addr <= 7'd0; 
    else if(wr_ram_en)//用该脉冲作为使能可以让地址从0->92循环写
        wr_ram_addr <= wr_ram_addr + 1'b1;
    else
        wr_ram_addr <= wr_ram_addr;
end
//------------------------------------------------------------------------------//
//1到9的计数，开始读数据后到每发送一个移位后的11bit数据需要9个时钟，数据速率为9M
always @(negedge i_rst_n or posedge i_clk_81M) //
begin
    if (!i_rst_n)
        cnt_6 <= 3'd0;
    else if(start_p)
        cnt_6 <= 3'd0;
    else if(byte_cnt == 7'd94)
        cnt_6 <= 3'd0;        
    else if(i_syncCode_en_p0)
        cnt_6 <= 3'd1; 
    else if(cnt_6 == 4'd6)
        cnt_6 <= 3'd1;   
    else if(cnt_6 != 3'd0)
        cnt_6 <= cnt_6 + 3'd1;
    else
        cnt_6 <= cnt_6;
end

always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if (!i_rst_n)
        rd_ram_en <= 1'b0; 
    else if(start_p)
        rd_ram_en <= 1'b0; 
    else if(i_syncCode_en_p0)
        rd_ram_en <= 1'b1;       
    else if(byte_cnt == 7'd94)
        rd_ram_en <= 1'b0;
    else
        rd_ram_en <= rd_ram_en;        
end
//ram_data读地址，起始地址为同步码的高7位，之后0-92内循环读，
//即93个11bit，读完正好一条完整1023bit报文
always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if (!i_rst_n)
        rd_ram_addr <= 7'd0;
    else if(start_p)
        rd_ram_addr <= 7'd0;
    else if( cnt_6 == 3'd1 )begin   //cnt_6第1个时钟读ram数据，地址实际在2处改变 
        if(i_syncCode_en_p1)        //该时刻cnt_6 = 3'd1   
            rd_ram_addr <= iv_syncCode_temp[10:4];   
        else if(rd_ram_addr == 7'd92)
            rd_ram_addr <= 7'd0;
        else 
            rd_ram_addr <= rd_ram_addr + 1'b1;
    end
    else 
        rd_ram_addr <= rd_ram_addr;
end
//寄存先后读出的两个11bit数据，移位时用
always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if (!i_rst_n)begin
        ram_out_temp1 <= 11'b0;
        ram_out_temp2 <= 11'b0;
    end
    else if(start_p)begin
        ram_out_temp1 <= 11'b0;
        ram_out_temp2 <= 11'b0;
    end
    else if(cnt_6 == 3'd4)begin    //在第4个时钟将从RAM中读出的数据存入ram_out_temp1，同步码地址    
        ram_out_temp1 <= ram_out[10:0];
        ram_out_temp2 <= ram_out_temp1;
    end    
    else begin
        ram_out_temp1 <= ram_out_temp1;
        ram_out_temp2 <= ram_out_temp2;
    end
end
//------------------------------------------------------------------------------//
//比特偏移量（同步码）的后四位
always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if (!i_rst_n)
        shift4 <= 4'd0;
    else if(start_p)
        shift4 <= 4'd0;
    else if (i_syncCode_en_p0)
        shift4 <= iv_syncCode_temp[3:0];
    else
        shift4 <= shift4;
end

//通过比特偏移量的后四位判断应该移多少位
always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if (!i_rst_n)
        data <= 11'b0;
    else if(cnt_6 == 3'd5)
        case(shift4)
            4'd0: data <= ram_out_temp2;                  
            4'd1: data <= {ram_out_temp2[9:0],ram_out_temp1[10]};                    
            4'd2: data <= {ram_out_temp2[8:0],ram_out_temp1[10:9]};                    
            4'd3: data <= {ram_out_temp2[7:0],ram_out_temp1[10:8]};                    
            4'd4: data <= {ram_out_temp2[6:0],ram_out_temp1[10:7]};                    
            4'd5: data <= {ram_out_temp2[5:0],ram_out_temp1[10:6]};                    
            4'd6: data <= {ram_out_temp2[4:0],ram_out_temp1[10:5]};                    
            4'd7: data <= {ram_out_temp2[3:0],ram_out_temp1[10:4]};                   
            4'd8: data <= {ram_out_temp2[2:0],ram_out_temp1[10:3]};                    
            4'd9: data <= {ram_out_temp2[1:0],ram_out_temp1[10:2]};                   
            4'd10:data <= {ram_out_temp2[0],ram_out_temp1[10:1]};                    
            default:data <= data;
        endcase
    else
        data <= data;
end     

//用来计数从ram中读11bit报文个数的计数器，0-94，2-94为有效数据
always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if (!i_rst_n)
        byte_cnt <= 7'b0;
    else if(start_p)
        byte_cnt <= 7'b0;    
    else if(byte_cnt == 7'd94)
        byte_cnt <= 7'b0;
    else if(cnt_6 == 3'd5)//20161014修改 
        byte_cnt <= byte_cnt + 1'd1;
    else
        byte_cnt <= byte_cnt;
end

always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if (!i_rst_n)
        o_frame_start <= 1'b0;
    else if(i_syncCode_en_p1)
        o_frame_start <= 1'b1;
    else
        o_frame_start <= 1'b0;
end

//阀门，2->94将数据与时钟放出去 输出正确顺序报文
always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if (!i_rst_n)begin
        o_shiftedData_sck <= 1'b0;
        ov_shiftedData    <=11'b0;
    end
    else if(start_p)begin
        o_shiftedData_sck <= 1'b0;
        ov_shiftedData    <=11'b0;
    end    
    else if( (byte_cnt >= 7'd2) && (byte_cnt <= 7'd94) && (cnt_6 == 3'd6) )begin
        o_shiftedData_sck <= 1'b1;
        ov_shiftedData    <= data;
    end    
    else begin 
        o_shiftedData_sck <= 1'b0;
        ov_shiftedData    <= ov_shiftedData;
    end
end
//将Ram_contrl中的先暂存下来，在SyncCRC给出同步码之后开始移位并校验
ram_data ram_data_u(
        .wraddress(wr_ram_addr),    
        .rdaddress(rd_ram_addr),   
        .wrclock(i_clk_81M),
        .rdclock(i_clk_81M),
        .data(iv_data),
        .q(ram_out),
        .rden(rd_ram_en),
        .wren(wr_ram_en)
        );
//-----------------------------随机码计算---------------------------------------//
//计算反相标志
always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if (!i_rst_n)
        Cb <= 3'b0;
    else if (byte_cnt == 7'd85 && cnt_6 == 4'd2)
        Cb <= data[10:8];
    else 
        Cb <= Cb;
end

always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if (!i_rst_n)
        inversionBit <= 1'b0;
    else 
        case (Cb)
            3'b110  : inversionBit <= 1'b1;  //fan
            3'b001  : inversionBit <= 1'b0;
            default : inversionBit <= 1'b0;
        endcase
end
//乘法器乘数，用于计算相位值
always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if (!i_rst_n)
        B <= 12'b0;
    else if(byte_cnt == 7'd85 && cnt_6 == 4'd2)
        B <= {data[7:0],4'b0};
    else if(byte_cnt == 7'd86 && cnt_6 == 4'd2)
        B <= {B[11:4],data[10:7]};
    else 
        B <= B; 
end

//乘法器乘数
assign Bb = (inversionBit)? ~B : B;
           
multiplication multiplication_u(
        .dataa(32'd2801775573),//2801775573  32'd2801775573
        .datab(Bb),// 16
        .clock(i_clk_81M),
        .result(multipy_data) //1878736208   //2280609432
);
//----------------------------CRC76计算-----------------------------------------//
//该逻辑仅做延时处理，用以crc76计算完成的花费
always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if (!i_rst_n)begin
        check_end_r0 <= 1'b0; 
        check_end_r1 <= 1'b0; 
        check_end_r2 <= 1'b0; 
        check_end_r3 <= 1'b0;
    end     
    else if (byte_cnt == 7'd94) begin
        check_end_r0 <= 1'b1; 
        check_end_r1 <= check_end_r0;     
        check_end_r2 <= check_end_r1; 
        check_end_r3 <= check_end_r2;
    end
    else begin
        check_end_r0 <= 1'b0; 
        check_end_r1 <= check_end_r0;     
        check_end_r2 <= check_end_r1; 
        check_end_r3 <= check_end_r2;//该时刻恰好是crc76计算完成的时刻
    end         
end

always @(negedge i_rst_n or posedge i_clk_81M)//mark0707
begin
    if (!i_rst_n) begin
        o_shift_ok      <= 1'b0;
        ov_ScrambleCode <= 32'b0;
        o_inversionBit  <= 1'b0; 
    end
    else if(start_p) begin//清0处理
        o_shift_ok      <= 1'b0;
        ov_ScrambleCode <= 32'b0;
        o_inversionBit  <= 1'b0; 
    end      
    else if( check_end_r3 && (crc_result == 75'd0) )begin
        o_shift_ok      <= 1'b1;
        ov_ScrambleCode <= multipy_data;
        o_inversionBit  <= inversionBit;
    end
    else if( check_end_r3 && (crc_result != 75'd0) )begin
        o_shift_ok      <= 1'b0;
        ov_ScrambleCode <= 32'b0;
        o_inversionBit  <= 1'b0;
    end
    else begin
        o_shift_ok      <= 1'b0; 
        ov_ScrambleCode <= ov_ScrambleCode;  //延长ov_ScrambleCode的持续时间
        o_inversionBit  <= 1'b0;
    end      
end
//----------------------------------------------------------------------------------------//
//相同报文滤除模块
/* 
    解调后，一段连续正确的FSK数据从第一个start（一个start对应一个11bit）开始，
    连续93个start才可能会通过crc校验有shift_ok出现（93x11=1023bit），这样 cnt_89
    在1-93之间会一直增加，增加到89后，计数器保持不变，等待shift_ok到来的那个时刻
    同时拉高给transform模块的标志，告诉transform模块新的报文到了。
    
    shift_ok到来后计数器清零，同时拉低给transform模块的标志，transform根据其下降沿发送一条报文
    因为是连续正确的fsk数据，此后的每个start都会对应一个shift_ok，因此cnt_89会在
    0-1之间跳变，不会一直增加，transform模块的标志也会一直是低电平，直到该段fsk
    结束，等待新的一段连续正确的fsk。  
*/

always @(negedge i_rst_n or posedge i_clk_81M)//shift_ok取沿
begin
    if (!i_rst_n)begin
        shift_ok_r <= 1'd0;
        shift_ok_p <= 1'd0;
    end
    else begin
        shift_ok_r <= o_shift_ok;
        shift_ok_p <= o_shift_ok && ~shift_ok_r;
    end         
end

always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if (!i_rst_n)
        cnt_89 <= 7'd0;
    else if( shift_ok_p  )//shift_ok时清零  check_end_r3 &&(crc_result == 75'd0)
        cnt_89 <= 7'd0;
    else if( cnt_89 == 7'd89 )
        cnt_89 <= cnt_89;
    else if( start_p )    //start到来时，计数器加1
        cnt_89 <= cnt_89 + 7'd1;
    else
        cnt_89 <= cnt_89;
end

always @(negedge i_rst_n or posedge i_clk_81M)
begin
    if (!i_rst_n)
        o_new_msg_flag <= 1'd0;
    else if( cnt_89 >= 7'd89 )
        o_new_msg_flag <= 1'd1;
    else
        o_new_msg_flag <= 1'd0;
end

//---------------------------------------------------------------------------//
Crc76  Crc76_check(
        .i_clk(i_clk_81M),
        .i_rst_n(i_rst_n),
        
        .i_start(o_frame_start),
        .i_data(ov_shiftedData),
        .i_data_wen(o_shiftedData_sck),
        
        .o_CRC_result(crc_result)
        );
 
////////////////////////////////////////////////////////////////////////////////        

endmodule
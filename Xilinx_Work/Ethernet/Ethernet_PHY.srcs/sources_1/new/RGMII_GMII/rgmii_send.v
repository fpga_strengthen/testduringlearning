`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2024/08/14 20:48:36
// Design Name: 
// Module Name: rgmii_send
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module rgmii_send(
	// input		wire			i_rst_n,
    
	input		wire			i_gmii_tx_clk,
	input		wire			i_gmii_tx_vld, 
	// Data outputs by only posedge,so its width turns to be 8 from 4.
	input		wire	[7:0]	iv_gmii_tx_data,
	// signals from PHY
	output		wire			o_phy_rgmii_tx_clk,
	output		wire			o_phy_rgmii_tx_ctl,
	// Data comes by two parts in both posedge and negedge.
	output		wire	[3:0]	ov_phy_rgmii_tx_data
);

	// Transfer data from single edge to double edge.
	wire			phy_rgmii_tx_ctl;
	wire	[3:0]	phy_rgmii_tx_data;

/*-----------------------------------------*\
					ODDR
\*-----------------------------------------*/
	
	// Transform gmii_tx_data and gmii_tx_vld to rgmii_tx_data and rgmii_tx_ctl.
	// i_gmii_tx_vld
	ODDR #(
     	.DDR_CLK_EDGE("SAME_EDGE"), 	// "OPPOSITE_EDGE" or "SAME_EDGE" 
      	.INIT(1'b0),    				// Initial value of Q: 1'b0 or 1'b1
      	.SRTYPE("SYNC") 				// Set/Reset type: "SYNC" or "ASYNC" 
   	) oddr_rx_rgmii_ctl (
      	.Q(phy_rgmii_tx_ctl),   		// 1-bit DDR output
      	.C(i_gmii_tx_clk),   			// 1-bit clock input
      	.CE(1), 						// 1-bit clock enable input
      	.D1(i_gmii_tx_vld), 			// 1-bit data input (positive edge)
      	.D2(i_gmii_tx_vld), 			// 1-bit data input (negative edge)
      	.R(0),   						// 1-bit reset
      	.S(0)    						// 1-bit set
   	);

   	// iv_gmii_tx_data
   	genvar	i_tx;
   	generate
   		for (i_tx = 0; i_tx < 4; i_tx = i_tx + 1) begin
   			ODDR #(
    		 	.DDR_CLK_EDGE("SAME_EDGE"), 	// "OPPOSITE_EDGE" or "SAME_EDGE" 
    		  	.INIT(1'b0),    				// Initial value of Q: 1'b0 or 1'b1
    		  	.SRTYPE("SYNC") 				// Set/Reset type: "SYNC" or "ASYNC" 
   			) oddr_tx_rgmii_tx_data (
    		  	.Q(phy_rgmii_tx_data[i_tx]),   	// 1-bit DDR output
    		  	.C(i_gmii_tx_clk),   			// 1-bit clock input
    		  	.CE(1), 						// 1-bit clock enable input
    		  	.D1(iv_gmii_tx_data[i_tx]), 	// 1-bit data input (positive edge)
    		  	.D2(iv_gmii_tx_data[i_tx + 4]), // 1-bit data input (negative edge)
    		  	.R(0),   						// 1-bit reset
    		  	.S(0)    						// 1-bit set
   			);
   		end
   	endgenerate

/*-----------------------------------------*\
					OBUF
\*-----------------------------------------*/

	// Let data from ODDR buffer one time.
	OBUF obuf_tx_rgmii_tx_clk(
      	.O(o_phy_rgmii_tx_clk),     // Buffer output (connect directly to top-level port)
      	.I(i_gmii_tx_clk)      		// Buffer input
   	);

   	OBUF obuf_tx_rgmii_tx_ctl(
      	.O(o_phy_rgmii_tx_ctl),     // Buffer output (connect directly to top-level port)
      	.I(phy_rgmii_tx_ctl)      	// Buffer input
   	);

   	genvar	j_tx;
   	generate
   		for (j_tx = 0; j_tx < 4; j_tx=j_tx+1) begin
   			OBUF obuf_tx_rgmii_data(
      			.O(ov_phy_rgmii_tx_data[j_tx]),     // Buffer output (connect directly to top-level port)
      			.I(phy_rgmii_tx_data[j_tx])      		// Buffer input
   			);
   		end
   	endgenerate

endmodule
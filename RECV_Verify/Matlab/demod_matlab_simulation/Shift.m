function [ hilbert_datain_delay ] = Shift( i_data_ADC , n )
%Shift Summary of this function goes here
%   将数据进行移位寄存
%   i_data_ADC为待移位的数据
%   n为移位几位
Length_data = length(i_data_ADC);
hilbert_datain_delay = [ zeros(1,n-1),i_data_ADC(1:Length_data-n+1)];
%定标
%hilbert_datain_delay = FixPointNum( hilbert_datain_delay, 13, 0);

end


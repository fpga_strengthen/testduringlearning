module Sync_Fsk
  (
  //----------------------------------------------
  //时钟和复位
  input        i_clk_81M,
  input        i_clk_27M_en,
  input        i_rst_n,
  
  //----------------------------------------------
  //同步点
  input        i_sync_point,      
  
  //----------------------------------------------
  //解调结果。用来判断两个相邻同步点间相同的bit数
  input        i_demod_result,    
  input        i_demod_result_sck,//81M时钟沿
  
  //解调模块以该脉冲信号为标志开始解调。
  output reg   o_demod_en,
  
  //完全同步上后，认为解调的数据有效，给出一个标志，将该标志发送给组帧模块，组帧模块根据该标志开始组帧
  output reg   o_framing_start
  );
  
  //模块常量
  parameter  sync_window_max      = 7'd16;     //同步窗的最大范围
  parameter  length_window1       = 7'd3;      //第0级同步窗，中心点和采样周期都不进行修正
  parameter  length_window2       = 7'd6;      //第1级同步窗，中心点修正1，采样周期修正1,
  parameter  length_window3       = 7'd9;      //第2级同步窗，中心点修正2，采样周期修正2
  parameter  length_window4       = 7'd13;     //第3级同步窗，中心点修正2，采样周期修正2

  parameter  half_T               = 7'd24;     //一个bit对应采样周期的一半，一个周期为48
  parameter  max_cnt_sync_point   = 4'd8;      //连续8个正确的同步点表明完全同步
  parameter  T_modify             = 6'd47;     //一个周期对应48个采样点，计数器范围为：[0~47]
  
  //各个模块变量声明
  reg        demod_result_r;    
  reg        demod_result_sck_r;
  
  reg flag_sync_point_correct;//一个48点采样计数周期内，计数时刻对应周期的后半部分，为了防止将仅相隔几个采样点的跳变点当做同步信号的预防处理

  reg [9:0]  shift_register;  
  reg [4:0]  cnt_25;
  
  reg [5:0]  cnt_48;
    
  reg [5:0]  det_sample;
  reg [3:0]  cnt_sync_point;
  
  reg [1:0]  sync_state;
  //-----------------------------------------------------------------------------------------------------
  //输入数据寄存 
  /*输入数据寄存原则
    全局时钟的跳变沿最可靠
    来自异步时钟域的输入需要寄存一次以同步化，再寄存一次以减少亚稳态带来的影响
    不需要用到跳变沿的来自同一时钟域的输入，没有必要对信号进行寄存
    需要用到跳变沿的来自同一时钟域的输入，寄存一次即可
    需要用到跳变沿的来自不同时钟域的输入，需要用到3个触发器，前两个用来同步，第3个触发器的输出和第2个的输出经过逻辑门来判断跳变沿
  */
    always@(posedge i_clk_81M or negedge i_rst_n)
    begin
        if(!i_rst_n) begin
            demod_result_r <= 1'b0;
            demod_result_sck_r <= 1'b0;
        end
        else begin
            demod_result_r <= i_demod_result;
            demod_result_sck_r <= i_demod_result_sck;
        end    
    end
  
    //相邻同步点之间理论间隔48点，实际过零点可能出现抖动，导致出现____|-|__|--|____等情况，因此对同步点进行限制
    always @ ( negedge i_rst_n or posedge i_clk_81M )
    begin
        if ( !i_rst_n )
            flag_sync_point_correct <= 1'b0;
        else if(i_clk_27M_en) begin
            if ( cnt_48 == half_T )//当计数到24个点时拉高
                flag_sync_point_correct <= 1'b1;
            else if ( i_sync_point && (det_sample <= sync_window_max) && flag_sync_point_correct )
                flag_sync_point_correct <= 1'b0;//来了一个拐点，且跳变点落在同步窗内时，拉低
            else
                flag_sync_point_correct <= flag_sync_point_correct;
        end
        else
            flag_sync_point_correct <= flag_sync_point_correct;
    end
 
  //准同步状态或者同步状态下，解调结果中出现连续10个相同的bit，说明解调结果不正确，根据编码规范，正确的报文中最多只有8个连续相同的bit
  //用一个10位的移位寄存器实现
  //????????????????????????????????????????????????????????????????????????初始值设定问题
    always@(posedge i_clk_81M or negedge i_rst_n)
    begin
        if(!i_rst_n)
            shift_register <= 10'b1;
        else if(sync_state != 2'd2)                  //未完全同步状态下，移位寄存器置1
            shift_register <= 10'b1;
        else if( demod_result_r & demod_result_sck_r)//判决结果为1，1入队
            shift_register <= {shift_register[8:0],1'b1};
        else if( !demod_result_r & demod_result_sck_r)
            shift_register <= {shift_register[8:0],1'b0};//判决结果为0，0入队
        else
            shift_register <= shift_register;
    end
  
  //两个连续有效拐点间超过25个bit，认为这两个拐点只是干扰等造成的，判定为没有fsk。
  //一个拐点到来时，det_sample的值小于同步窗的最大范围sync_window_max，即该拐点落在同步窗的有效范围内，认为该拐点有效。
    always@(posedge i_clk_81M or negedge i_rst_n)
    begin
        if(!i_rst_n)
            cnt_25 <= 5'd0;
        else if(sync_state == 2'd0)                             //未同步状态下，移位寄存器置0
            cnt_25 <= 5'd0;
        else if((det_sample <= sync_window_max) && i_sync_point && flag_sync_point_correct)//检测到一个有效拐点，计数器25清零
            cnt_25 <= 5'd0;
        else if ( cnt_25 == 5'd31 )//计数到31则保持
            cnt_25 <= cnt_25;    
        else if(demod_result_sck_r)                             //一个判决时刻代表一个bit，用来计数两个有效同步点间的bit数
            cnt_25 <= cnt_25 + 5'd1;
        else
            cnt_25 <= cnt_25;
    end  
   
    //正常数据的波特率为564.48K，采样时钟为27.095M。27.095M/564.48K = 48 所以一个bit的有效宽度为48个27.098M时钟
    //即一个同步点对应的位置应该是一个bit的结尾，即cnt_48的值应该为47
    //很显然，在一个bit的中间位置判决是最好的方式，能够有效降低误码率。所以我们做了一个计数周期为48的计数器，在计数器计数到24时判决
    //考虑到波特率的抖动以及同步点判决的误差，我们对cnt_48引入了三级同步跟踪修正的方式.
    always @( negedge i_rst_n or posedge i_clk_81M )
    begin
        if( !i_rst_n )
            cnt_48 <= 6'd0;
        else if(i_clk_27M_en) begin
            case (sync_state)
            2'd0:
                if ( i_sync_point == 1'b1 )//捕捉状态，只要检测到开始有拐点，将计数器清0，准备开始周期计数
                    cnt_48 <= 6'd0;          
                else if ( cnt_48 == 6'd47 )  
                    cnt_48 <= 6'd0;          
                else                         
                    cnt_48 <= cnt_48 + 1'd1 ;
            2'd1,
            2'd2:
                if(i_sync_point == 1'b1) begin
                    if((det_sample <= sync_window_max) && flag_sync_point_correct)begin      //判断同步点是否落在同步窗内
                        if(cnt_48 >= T_modify)            //不需要修正
                            cnt_48 <= cnt_48 - T_modify;
                        else if(cnt_48 >= half_T) begin   //采样周期偏小  是否需要改成case语句，无先后顺序，减少相应的时延 ？？？？？？
                            if(det_sample >= length_window4)  //采样点落在区间[-16,-12]
                                cnt_48 <= cnt_48 + 6'd14; //同步窗中心点往左偏移11个采样点 11 14
                            else if(det_sample >= length_window3)  //采样点落在区间[-16,-12]
                                cnt_48 <= cnt_48 + 6'd10;  //同步窗中心点往左偏移9个采样点  8 10 
                            else if(det_sample >= length_window2)//采样点落在区间[-12,-8]
                                cnt_48 <= cnt_48 + 6'd7;  //同步窗中心点往左偏移7个采样点  5 7
                            else if(det_sample >= length_window1)//采样点落在区间[-8,-4] 
                                cnt_48 <= cnt_48 + 6'd4;  //同步窗中心点往左偏移4个采样点  2 4
                            else
                                cnt_48 <= 6'd0;    //同步窗中心点不修正
                        end
                        else if(cnt_48 <= half_T) begin  //采样周期偏大
                            if(det_sample >= length_window4)     //采样点落在区间[-16,-12]
                                cnt_48 <= cnt_48 - 6'd14;  //同步窗中心点往左偏移9个采样点
                            else if(det_sample >= length_window3)     //采样点落在区间[-16,-12]
                                cnt_48 <= cnt_48 - 6'd10;  //同步窗中心点往左偏移9个采样点
                            else if(det_sample >= length_window2)//采样点落在区间[-12,-8]
                                cnt_48 <= cnt_48 - 6'd7;  //同步窗中心点往左偏移7个采样点
                            else if(det_sample >= length_window1)//采样点落在区间[-8,-4]
                                cnt_48 <= cnt_48 - 6'd4;  //同步窗中心点往左偏移4个采样点
                            else
                                cnt_48 <= 6'd0;
                        end
                        else
                            cnt_48 <= cnt_48; 
                    end
                    else begin //同步点落在同步窗外
                        if(cnt_48 >= T_modify)
                            cnt_48 <= cnt_48 - T_modify;
                        else
                            cnt_48 <= cnt_48 + 1'b1;
                    end
                end
                else if(cnt_48 >= T_modify)
                    cnt_48 <= cnt_48 - T_modify;
                else
                    cnt_48 <= cnt_48 + 1'b1;
            default:
                cnt_48 <= cnt_48;
            endcase
        end
        else
            cnt_48 <= cnt_48;
    end
  
    //采样时刻偏差的绝对值
    always @( negedge i_rst_n or posedge i_clk_81M )
    begin
        if( !i_rst_n )
            det_sample <= 6'd0;
        else if(i_clk_27M_en) begin
            if(cnt_48 <= half_T)
                det_sample <= cnt_48;
            else
                det_sample <= T_modify - cnt_48;
        end
        else
            det_sample <= det_sample;  
    end

    //-------------------------------------------------------------------------------------------------------
    //状态机跳转
    
    //状态机跳转计数器
    //reg [3:0]  cnt_sync_point;
    always@( negedge i_rst_n or posedge i_clk_81M )
    begin
    if( !i_rst_n )
        cnt_sync_point <= 4'd0;
    else if(i_clk_27M_en) begin 
        if (shift_register==10'd1023 || shift_register==10'd0)  //解调结果中出现连续10个相同的bit，说明解调结果不正确，根据编码规范，正确的报文中最多只有8个连续相同的bit
            cnt_sync_point <= 4'd0;
        else if(cnt_25 >= 5'd20)              //在同步状态下，如果连续25个bit都没有找到拐点，则说明没有信号到达，强制失步
            cnt_sync_point <= 4'd0;
        else begin
            case (sync_state)
            2'd0:
                if (i_sync_point == 1'b1)
                    cnt_sync_point <= 4'd1;
                else
                    cnt_sync_point <= cnt_sync_point;
            2'd1:
                if (i_sync_point == 1'b1) begin
                    if((det_sample <= sync_window_max) && flag_sync_point_correct)//跳变点落在同步窗内
                        cnt_sync_point <= cnt_sync_point + 1'b1;
                    else                     
                        cnt_sync_point <= cnt_sync_point - 1'b1;//否则认为跳变沿不正确
                end
                else
                    cnt_sync_point <= cnt_sync_point;
            2'd2:
                if (i_sync_point == 1'b1)begin
                    if((det_sample <= sync_window_max) && flag_sync_point_correct)begin //跳变点落在同步窗内
                        if ( cnt_sync_point >= max_cnt_sync_point)//如果同步拐点计数达到上限，则不再增加，这样方便退出同步状态
                            cnt_sync_point <= cnt_sync_point;
                        else 
                            cnt_sync_point <= cnt_sync_point + 1'b1;
                        end
                    else
                        cnt_sync_point <= cnt_sync_point - 1'b1;      //跳变点落在同步窗外，计数器减1
                else
                    cnt_sync_point <= cnt_sync_point;
                end
            default:
                cnt_sync_point <= cnt_sync_point;
            endcase
        end
    end
    else
        cnt_sync_point <= cnt_sync_point;
    end
  
  
    //状态机
    //reg [1:0]  sync_state;
    always @( negedge i_rst_n or posedge i_clk_81M )
    begin
        if ( !i_rst_n )
            sync_state <= 2'd0;
        else if(i_clk_27M_en) begin
            case( sync_state )
            2'd0:
                if ( cnt_sync_point == 4'd1 )//状态跳转计数器为1，跳转到半同步状态
                    sync_state <= 2'd1;
                else
                    sync_state <= 2'd0;
            2'd1:
                if ( cnt_sync_point == max_cnt_sync_point )//状态跳转计数器计数到最大值，跳转到完全同步状态
                    sync_state <= 2'd2;
                else if ( cnt_sync_point == 4'd0 )      //状态跳转计数器为0，直接跳转到未同步状态
                    sync_state <= 2'd0;
                else
                    sync_state <= 2'd1;
            2'd2:
                if ( cnt_sync_point == 4'd0 )           //状态跳转计数器为0，直接跳转到未同步状态
                    sync_state <= 2'd0;
                else
                    sync_state <= 2'd2;
            default:
                sync_state <= sync_state;
            endcase  
        end
        else
          sync_state <= sync_state;
    end
  
    //cnt_48为24时，给出判决时刻
    //reg        o_demod_en;
    always@(posedge i_clk_81M or negedge i_rst_n)
    begin
        if(!i_rst_n)
            o_demod_en <= 1'd0;
        else if(i_clk_27M_en ) begin
            if(cnt_48==6'd24 && (sync_state!=2'd0) )
                o_demod_en <= 1'd1;
            else
                o_demod_en <= 1'd0;
        end
        else
            o_demod_en <= o_demod_en;
    end
  
    //完全同步上后，认为解调的数据有效，给出一个标志，将该标志发送给组帧模块，组帧模块根据该标志开始组帧
    always@(posedge i_clk_81M or negedge i_rst_n)
    begin
        if(!i_rst_n)
            o_framing_start <= 1'd0;
        else if(i_clk_27M_en ) begin
            if(sync_state==2'd2)
                o_framing_start <= 1'd1;
            else 
                o_framing_start <= 1'd0;  
        end  
        else 
            o_framing_start <= o_framing_start;
    end
  
endmodule
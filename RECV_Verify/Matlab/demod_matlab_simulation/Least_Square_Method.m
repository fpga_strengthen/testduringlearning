function [hilbert_data_out,slope_forward,slope_after] =  Least_Square_Method( angle_fit )
    %function [abs_reference_numerator,abs_reference_denominator,hilbert_data_out] =  Least_Square_Method( angle_fit )
    %前后八点最小二乘法，得到交点的横坐标的分子与分母 旧版程序
    %   输入为修正后的相位
    %   输出为横坐标的分子reference_numerator与分母reference_denominator
    %%
    %取N=16个点用于之后进行8点最小二乘法
    angle_fit_delay_8_n = Shift_anglefit( angle_fit ,1 );
    angle_fit_delay_7_n = Shift_anglefit( angle_fit ,2 );
    angle_fit_delay_6_n = Shift_anglefit( angle_fit ,3 );
    angle_fit_delay_5_n = Shift_anglefit( angle_fit ,4 );
    angle_fit_delay_4_n = Shift_anglefit( angle_fit ,5 );
    angle_fit_delay_3_n = Shift_anglefit( angle_fit ,6 );
    angle_fit_delay_2_n = Shift_anglefit( angle_fit ,7 );
    angle_fit_delay_1_n = Shift_anglefit( angle_fit ,8 );
    angle_fit_delay_1   = Shift_anglefit( angle_fit ,9 );
    angle_fit_delay_2   = Shift_anglefit( angle_fit ,10 );
    angle_fit_delay_3   = Shift_anglefit( angle_fit ,11 );
    angle_fit_delay_4   = Shift_anglefit( angle_fit ,12 );
    angle_fit_delay_5   = Shift_anglefit( angle_fit ,13 );
    angle_fit_delay_6   = Shift_anglefit( angle_fit ,14 );
    angle_fit_delay_7   = Shift_anglefit( angle_fit ,15 );
    angle_fit_delay_8   = Shift_anglefit( angle_fit ,16 );
    
    %按照最小二乘法公式先进行对应位相减
    num_8_reduce_1   = angle_fit_delay_8 - angle_fit_delay_1;%用于计算拐点后直线斜率
    num_7_reduce_2   = angle_fit_delay_7 - angle_fit_delay_2;
    num_6_reduce_3   = angle_fit_delay_6 - angle_fit_delay_3 ;
    num_5_reduce_4   = angle_fit_delay_5 - angle_fit_delay_4 ;
    num_1n_reduce_8n = angle_fit_delay_1_n - angle_fit_delay_8_n ;%用于计算拐点前直线斜率
    num_2n_reduce_7n = angle_fit_delay_2_n - angle_fit_delay_7_n ;
    num_3n_reduce_6n = angle_fit_delay_3_n - angle_fit_delay_6_n ;
    num_4n_reduce_5n = angle_fit_delay_4_n - angle_fit_delay_5_n;%两个斜率用于计算拐点横坐标分母
    num_1n_reduce_1  = angle_fit_delay_1_n - angle_fit_delay_1 ;%用于计算拐点横坐标分子
    num_2n_reduce_2  = angle_fit_delay_2_n - angle_fit_delay_2 ;
    num_3n_reduce_3  = angle_fit_delay_3_n - angle_fit_delay_3 ;
    num_4n_reduce_4  = angle_fit_delay_4_n - angle_fit_delay_4 ;
    num_5n_reduce_5  = angle_fit_delay_5_n - angle_fit_delay_5 ;
    num_6n_reduce_6  = angle_fit_delay_6_n - angle_fit_delay_6 ;
    num_7n_reduce_7  = angle_fit_delay_7_n - angle_fit_delay_7 ;
    num_8n_reduce_8  = angle_fit_delay_8_n - angle_fit_delay_8 ;
    %定标
    num_8_reduce_1 = FixPointNum(num_8_reduce_1,22,0);
    num_7_reduce_2 = FixPointNum(num_7_reduce_2,22,0);
    num_6_reduce_3 = FixPointNum(num_6_reduce_3,22,0);
    num_5_reduce_4 = FixPointNum(num_5_reduce_4,22,0);
    num_1n_reduce_8n = FixPointNum(num_1n_reduce_8n,22,0);
    num_2n_reduce_7n = FixPointNum(num_2n_reduce_7n,22,0);
    num_3n_reduce_6n = FixPointNum(num_3n_reduce_6n,22,0);
    num_4n_reduce_5n = FixPointNum(num_4n_reduce_5n,22,0);
    num_1n_reduce_1 = FixPointNum(num_1n_reduce_1,22,0);
    num_2n_reduce_2 = FixPointNum(num_2n_reduce_2,22,0);
    num_3n_reduce_3 = FixPointNum(num_3n_reduce_3,22,0);
    num_4n_reduce_4 = FixPointNum(num_4n_reduce_4,22,0);
    num_5n_reduce_5 = FixPointNum(num_5n_reduce_5,22,0);
    num_6n_reduce_6 = FixPointNum(num_6n_reduce_6,22,0);
    num_7n_reduce_7 = FixPointNum(num_7n_reduce_7,22,0);
    num_8n_reduce_8 = FixPointNum(num_8n_reduce_8,22,0);
    
    %乘对应常数，用于求拐点斜率，为拐点的横坐标分母
    num_8_reduce_1_x7   = FixPointNum(num_8_reduce_1*7, 27, 0);%拐点后斜率
    num_1n_reduce_8n_x7 = FixPointNum(num_1n_reduce_8n*7, 27, 0);%拐点前斜率
    num_7_reduce_2_x5   = FixPointNum(num_7_reduce_2*5, 27, 0);%拐点后斜率
    num_2n_reduce_7n_x5 = FixPointNum(num_2n_reduce_7n*5, 27, 0);%拐点前斜率
    num_6_reduce_3_x3   = FixPointNum(num_6_reduce_3*3, 27, 0);%拐点后斜率
    num_3n_reduce_6n_x3 = FixPointNum(num_3n_reduce_6n*3, 27, 0);%拐点前斜率
    num_5_reduce_4_x1   = FixPointNum(num_5_reduce_4*1, 27, 0);%拐点后斜率
    num_4n_reduce_5n_x1 = FixPointNum(num_4n_reduce_5n*1, 27, 0);%拐点前斜率
    %乘对应常数，用于求拐点截距，为拐点的横坐标分子
    num_1n_reduce_1_x42 = FixPointNum(num_1n_reduce_1*42, 30, 0);
    num_2n_reduce_2_x33 = FixPointNum(num_2n_reduce_2*33, 30, 0);
    num_3n_reduce_3_x24 = FixPointNum(num_3n_reduce_3*24, 30, 0);
    num_4n_reduce_4_x15 = FixPointNum(num_4n_reduce_4*15, 30, 0);
    num_5n_reduce_5_x6  = FixPointNum(num_5n_reduce_5*6, 30, 0);
    num_6n_reduce_6_x3  = FixPointNum(num_6n_reduce_6*3, 30, 0);
    num_7n_reduce_7_x12 = FixPointNum(num_7n_reduce_7*12, 30, 0);
    num_8n_reduce_8_x21 = FixPointNum(num_8n_reduce_8*21, 30, 0);
    
    %前8个点的斜率
    slope_forward =num_8_reduce_1_x7 + num_7_reduce_2_x5 + num_6_reduce_3_x3 + num_5_reduce_4_x1;
    %后8个点的斜率
    slope_after = num_1n_reduce_8n_x7 + num_2n_reduce_7n_x5 + num_3n_reduce_6n_x3 + num_4n_reduce_5n_x1;
    %交点横坐标的分子
    reference_numerator = num_1n_reduce_1_x42 + num_2n_reduce_2_x33 + num_3n_reduce_3_x24 + num_4n_reduce_4_x15 + num_5n_reduce_5_x6 - num_6n_reduce_6_x3 - num_7n_reduce_7_x12 - num_8n_reduce_8_x21;
    %交点横坐标的分母
    reference_denominator = slope_after - slope_forward;
    
    
    reference_numerator_x2 = reference_numerator*2;
    reference_denominator_x3 = reference_denominator*3;
    %对分子和分母取绝对值
    abs_reference_numerator = abs(reference_numerator_x2);
    abs_reference_denominator =  abs(reference_denominator_x3);
    
    %拐点判决
    hilbert_data_out = zeros(1,length(abs_reference_denominator));
    for i= 2:(length(abs_reference_denominator))
        if((abs_reference_numerator(i) ) < (abs_reference_denominator(i)))  %拐点对应的截距交点的横坐标的值小于1
            %根据斜率前后正负的相对值来给出希尔伯特解调的结果
            if(((-slope_forward(i))<12700) && ((-slope_forward(i))>2100) && ((slope_after(i))>2100) && ((slope_after(i))<12700) )
                hilbert_data_out(i) =0;
            elseif( ((-slope_after(i))<12700) && ((-slope_after(i))>2100) && ((slope_forward(i))>2100) && ((slope_forward(i))<12700) )
                hilbert_data_out(i) = 1;
            else
                hilbert_data_out(i) = hilbert_data_out(i-1);
            end %if
        else
            hilbert_data_out(i) = hilbert_data_out(i-1);
        end %if
    end %for
    
    %%算法修改前后求得的拐点对比
    hilbert_data_out = xor([hilbert_data_out,0],[0,hilbert_data_out]);
    %画出拐点判定过程中的判决条件
    for i= 2:length(abs_reference_denominator)
      if(abs_reference_denominator(i) - abs_reference_numerator(i) > 0 )
          flag(i) = 1*10^5;
          up_freq1(i) = 12700;
          down_freq1(i) = 2100;
          up_freq2(i) = -2100;
          down_freq2(i) = -12700;  
          freq_shift_max(i) = 18943;
          freq_shift_min(i) = -16670;      
      else
          flag(i) = 0; 
          up_freq1(i) = 12700;
          down_freq1(i) = 2100;
          up_freq2(i) = -2100;
          down_freq2(i) = -12700;  
          freq_shift_max(i) = 18943;
          freq_shift_min(i) = -16670;  
      end
    end
    % %8点斜率频偏上下限
    % figure(8),
    % subplot(2,1,1),title('斜率曲线，斜率上下限'),
    % plot(slope_forward);
    % hold on,
    % plot(slope_after,'c');
    % hold on,
    % plot(up_freq1,'r');
    % hold on,
    % plot(up_freq2,'r');
    % hold on,
    % plot(down_freq1,'r');
    % hold on,
    % plot(down_freq2,'r');
    % 
    % %正常数据的8点斜率上下限
    % hold on,
    % plot(freq_shift_max,'k');
    % hold on,
    % plot(freq_shift_min,'k');
    % plot(flag,'b');
    % %交点分子分母，斜率
    % hold on,
    % plot(hilbert_data_out*3*10^4,'g'),title('拐点，前后8个点拐点斜率');
    % hold on,
    % subplot(2,1,2),title('交点分子，交点分母'),
    % plot(abs_reference_denominator,'k');
    % hold on,
    % plot(abs_reference_numerator,'c');
    % plot(flag,'g');
    end %function
    
    
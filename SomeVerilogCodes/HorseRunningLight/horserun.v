module horserun
#(
    parameter   N = 4,                          //设定灯的个数
    parameter   TIMEOUT = 32'hFFFF_FFFF         //亮灯时间
)(
    input   wire        i_clk,
    input   wire        i_rst_n,
    output  reg [N-1:0] led
);

    reg [31:0]  cnt;

    always@(posedge i_clk or negedge i_rst_n) begin
        if(i_rst_n == 1'b0)begin 
            cnt <= 32'd0;
            led <= 4'b0001;             //初始时刻只亮一个灯
        end
        else begin 
            cnt <= cnt + 1'b1;          //加满了会自动清零
            if(cnt == TIMEOUT)begin 
                led = led << 1;
            if(led == 4'b0000)
                led = 4'b0001;          //一轮完成后重头再来
            end
        end
    end

endmodule
//将吕老师在8.06发送的Check.v中CRC校验部分单独拿出来，进行仿真，验证其CRC校验部分结果是否正确

module Crc76(
         i_clk,
         i_rst_n,
         
         i_start,
         i_data,
         i_data_wen,
         
         o_CRC_result
         );
 
 
input i_clk;
input i_rst_n;

input i_start;
input [10:0]i_data;
input i_data_wen;
 
output reg [74:0] o_CRC_result;
 

/////////////////////////////////////////////////////////////////
reg [10:0]defsk_datain_r;
reg data_wen_r;
reg [74:0]lfsr;
 
////////////////////////////////////////////////////////////////
//将输入数据寄存一次
always @ ( posedge i_clk or negedge i_rst_n )
begin
    if( !i_rst_n ) begin
        defsk_datain_r <= 11'b0;
        data_wen_r <= 1'b0;
    end
    else begin
        defsk_datain_r <= i_data;
        data_wen_r <= i_data_wen;
    end
 
 end
////////////////////////////////////////////////////////////////
//CRC 
always @ ( posedge i_clk or negedge i_rst_n )
begin
    if( !i_rst_n )
        lfsr <= 75'b0;
    else if(i_start)
        lfsr <= 75'b0;
    else if( data_wen_r ) begin//cnt_6为4时做一次并行CRC除法
        lfsr[74]<= lfsr[63] ^lfsr[65] ^lfsr[66] ^lfsr[70] ^lfsr[71] ^lfsr[72] ^lfsr[73];
        lfsr[73]<= lfsr[62] ^lfsr[64] ^lfsr[65] ^lfsr[69] ^lfsr[70] ^lfsr[71] ^lfsr[72] ^lfsr[74];
        lfsr[72]<= lfsr[61] ^lfsr[64] ^lfsr[65] ^lfsr[66] ^lfsr[68] ^lfsr[69] ^lfsr[72] ^lfsr[74];
        lfsr[71]<= lfsr[60] ^lfsr[64] ^lfsr[66] ^lfsr[67] ^lfsr[68] ^lfsr[70] ^lfsr[72];
        lfsr[70]<= lfsr[59] ^lfsr[67] ^lfsr[69] ^lfsr[70] ^lfsr[72] ^lfsr[73] ^lfsr[74];
        lfsr[69]<= lfsr[58] ^lfsr[66] ^lfsr[68] ^lfsr[69] ^lfsr[71] ^lfsr[72] ^lfsr[73] ^lfsr[74];
        lfsr[68]<= lfsr[57] ^lfsr[65] ^lfsr[67] ^lfsr[68] ^lfsr[70] ^lfsr[71] ^lfsr[72] ^lfsr[73] ^lfsr[74];
        lfsr[67]<= lfsr[56] ^lfsr[64] ^lfsr[66] ^lfsr[67] ^lfsr[69] ^lfsr[70] ^lfsr[71] ^lfsr[72] ^lfsr[73] ^lfsr[74];
        lfsr[66]<= lfsr[55] ^lfsr[68] ^lfsr[69] ^lfsr[74];
        lfsr[65]<= lfsr[54] ^lfsr[67] ^lfsr[68] ^lfsr[73] ^lfsr[74];
        lfsr[64]<= lfsr[53] ^lfsr[66] ^lfsr[67] ^lfsr[72] ^lfsr[73] ;
        lfsr[63]<= lfsr[52] ^lfsr[65] ^lfsr[66] ^lfsr[71] ^lfsr[72] ^lfsr[74];
        lfsr[62]<= lfsr[51] ^lfsr[64] ^lfsr[65] ^lfsr[70] ^lfsr[71] ^lfsr[73] ^lfsr[74];
        lfsr[61]<= lfsr[50] ^lfsr[64] ^lfsr[65] ^lfsr[66] ^lfsr[69] ^lfsr[71] ^lfsr[74];
        lfsr[60]<= lfsr[49] ^lfsr[64] ^lfsr[66] ^lfsr[68] ^lfsr[71] ^lfsr[72] ^lfsr[74];
        lfsr[59]<= lfsr[48] ^lfsr[66] ^lfsr[67] ^lfsr[72] ^lfsr[74];
        lfsr[58]<= lfsr[47] ^lfsr[65] ^lfsr[66] ^lfsr[71] ^lfsr[73] ;
        lfsr[57]<= lfsr[46] ^lfsr[64] ^lfsr[65] ^lfsr[70] ^lfsr[72] ;
        lfsr[56]<= lfsr[45] ^lfsr[64] ^lfsr[65] ^lfsr[66] ^lfsr[69] ^lfsr[70] ^lfsr[72] ^lfsr[73] ;
        lfsr[55]<= lfsr[44] ^lfsr[64] ^lfsr[66] ^lfsr[68] ^lfsr[69] ^lfsr[70] ^lfsr[73] ^lfsr[74];
        lfsr[54]<= lfsr[43] ^lfsr[66] ^lfsr[67] ^lfsr[68] ^lfsr[69] ^lfsr[70] ^lfsr[71] ;
        lfsr[53]<= lfsr[42] ^lfsr[65] ^lfsr[66] ^lfsr[67] ^lfsr[68] ^lfsr[69] ^lfsr[70] ^lfsr[74];
        lfsr[52]<= lfsr[41] ^lfsr[64] ^lfsr[65] ^lfsr[66] ^lfsr[67] ^lfsr[68] ^lfsr[69] ^lfsr[73] ^lfsr[74];
        lfsr[51]<= lfsr[40] ^lfsr[64] ^lfsr[67] ^lfsr[68] ^lfsr[70] ^lfsr[71] ^lfsr[74];
        lfsr[50]<= lfsr[39] ^lfsr[65] ^lfsr[67] ^lfsr[69] ^lfsr[71] ^lfsr[72] ;
        lfsr[49]<= lfsr[38] ^lfsr[64] ^lfsr[66] ^lfsr[68] ^lfsr[70] ^lfsr[71] ^lfsr[74];
        lfsr[48]<= lfsr[37] ^lfsr[66] ^lfsr[67] ^lfsr[69] ^lfsr[71] ^lfsr[72] ^lfsr[74];
        lfsr[47]<= lfsr[36] ^lfsr[65] ^lfsr[66] ^lfsr[68] ^lfsr[70] ^lfsr[71] ^lfsr[73] ;
        lfsr[46]<= lfsr[35] ^lfsr[64] ^lfsr[65] ^lfsr[67] ^lfsr[69] ^lfsr[70] ^lfsr[72] ;
        lfsr[45]<= lfsr[34] ^lfsr[64] ^lfsr[65] ^lfsr[68] ^lfsr[69] ^lfsr[70] ^lfsr[72] ^lfsr[73] ^lfsr[74];
        lfsr[44]<= lfsr[33] ^lfsr[64] ^lfsr[65] ^lfsr[66] ^lfsr[67] ^lfsr[68] ^lfsr[69] ^lfsr[70] ^lfsr[74];
        lfsr[43]<= lfsr[32] ^lfsr[64] ^lfsr[67] ^lfsr[68] ^lfsr[69] ^lfsr[70] ^lfsr[71] ^lfsr[72] ;
        lfsr[42]<= lfsr[31] ^lfsr[65] ^lfsr[67] ^lfsr[68] ^lfsr[69] ^lfsr[72] ^lfsr[73] ^lfsr[74];
        lfsr[41]<= lfsr[30] ^lfsr[64] ^lfsr[66] ^lfsr[67] ^lfsr[68] ^lfsr[71] ^lfsr[72] ^lfsr[73] ;
        lfsr[40]<= lfsr[29] ^lfsr[67] ^lfsr[73] ^lfsr[74];
        lfsr[39]<= lfsr[28] ^lfsr[66] ^lfsr[72] ^lfsr[73] ;
        lfsr[38]<= lfsr[27] ^lfsr[65] ^lfsr[71] ^lfsr[72] ^lfsr[74];
        lfsr[37]<= lfsr[26] ^lfsr[64] ^lfsr[70] ^lfsr[71] ^lfsr[73] ;
        lfsr[36]<= lfsr[25] ^lfsr[65] ^lfsr[66] ^lfsr[69] ^lfsr[71] ^lfsr[73] ^lfsr[74];
        lfsr[35]<= lfsr[24] ^lfsr[64] ^lfsr[65] ^lfsr[68] ^lfsr[70] ^lfsr[72] ^lfsr[73] ;
        lfsr[34]<= lfsr[23] ^lfsr[64] ^lfsr[65] ^lfsr[66] ^lfsr[67] ^lfsr[69] ^lfsr[70] ^lfsr[73] ^lfsr[74];
        lfsr[33]<= lfsr[22] ^lfsr[64] ^lfsr[68] ^lfsr[69] ^lfsr[70] ^lfsr[71] ;
        lfsr[32]<= lfsr[21] ^lfsr[65] ^lfsr[66] ^lfsr[67] ^lfsr[68] ^lfsr[69] ^lfsr[71] ^lfsr[72] ^lfsr[73] ^lfsr[74];
        lfsr[31]<= lfsr[20] ^lfsr[64] ^lfsr[65] ^lfsr[66] ^lfsr[67] ^lfsr[68] ^lfsr[70] ^lfsr[71] ^lfsr[72] ^lfsr[73] ^lfsr[74];
        lfsr[30]<= lfsr[19] ^lfsr[64] ^lfsr[67] ^lfsr[69] ;
        lfsr[29]<= lfsr[18] ^lfsr[65] ^lfsr[68] ^lfsr[70] ^lfsr[71] ^lfsr[72] ^lfsr[73] ^lfsr[74];
        lfsr[28]<= lfsr[17] ^lfsr[64] ^lfsr[67] ^lfsr[69] ^lfsr[70] ^lfsr[71] ^lfsr[72] ^lfsr[73] ^lfsr[74];
        lfsr[27]<= lfsr[16] ^lfsr[65] ^lfsr[68] ^lfsr[69] ^lfsr[74];
        lfsr[26]<= lfsr[15] ^lfsr[64] ^lfsr[67] ^lfsr[68] ^lfsr[73] ^lfsr[74];
        lfsr[25]<= lfsr[14] ^lfsr[65] ^lfsr[67] ^lfsr[70] ^lfsr[71] ^lfsr[74];
        lfsr[24]<= lfsr[13] ^lfsr[64] ^lfsr[66] ^lfsr[69] ^lfsr[70] ^lfsr[73] ;
        lfsr[23]<= lfsr[12] ^lfsr[66] ^lfsr[68] ^lfsr[69] ^lfsr[70] ^lfsr[71] ^lfsr[73] ;
        lfsr[22]<= lfsr[11] ^lfsr[65] ^lfsr[67] ^lfsr[68] ^lfsr[69] ^lfsr[70] ^lfsr[72] ^lfsr[74];
        lfsr[21]<= lfsr[10] ^lfsr[64] ^lfsr[66] ^lfsr[67] ^lfsr[68] ^lfsr[69] ^lfsr[71] ^lfsr[73] ^lfsr[74];
        lfsr[20]<= lfsr[9] ^lfsr[67] ^lfsr[68] ^lfsr[71] ^lfsr[74];
        lfsr[19]<= lfsr[8] ^lfsr[66] ^lfsr[67] ^lfsr[70] ^lfsr[73] ^lfsr[74];
        lfsr[18]<= lfsr[7] ^lfsr[65] ^lfsr[66] ^lfsr[69] ^lfsr[72] ^lfsr[73] ;
        lfsr[17]<= lfsr[6] ^lfsr[64] ^lfsr[65] ^lfsr[68] ^lfsr[71] ^lfsr[72] ;
        lfsr[16]<= lfsr[5] ^lfsr[64] ^lfsr[65] ^lfsr[66] ^lfsr[67] ^lfsr[72] ^lfsr[73] ;
        lfsr[15]<= lfsr[4] ^lfsr[64] ^lfsr[70] ^lfsr[73] ^lfsr[74];
        lfsr[14]<= lfsr[3] ^lfsr[65] ^lfsr[66] ^lfsr[69] ^lfsr[70] ^lfsr[71] ;
        lfsr[13]<= lfsr[2] ^lfsr[64] ^lfsr[65] ^lfsr[68] ^lfsr[69] ^lfsr[70] ^lfsr[74];
        lfsr[12]<= lfsr[1] ^lfsr[64] ^lfsr[65] ^lfsr[66] ^lfsr[67] ^lfsr[68] ^lfsr[69] ^lfsr[70] ^lfsr[71] ^lfsr[72] ;
        lfsr[11]<= lfsr[0] ^lfsr[64] ^lfsr[67] ^lfsr[68] ^lfsr[69] ^lfsr[72] ^lfsr[73] ;
        lfsr[10]<= defsk_datain_r[10] ^lfsr[65] ^lfsr[67] ^lfsr[68] ^lfsr[70] ^lfsr[73] ;
        lfsr[ 9]<= defsk_datain_r[9] ^lfsr[64] ^lfsr[66] ^lfsr[67] ^lfsr[69] ^lfsr[72] ;
        lfsr[ 8]<= defsk_datain_r[8] ^lfsr[68] ^lfsr[70] ^lfsr[72] ^lfsr[73] ;
        lfsr[ 7]<= defsk_datain_r[7] ^lfsr[67] ^lfsr[69] ^lfsr[71] ^lfsr[72] ;
        lfsr[ 6]<= defsk_datain_r[6] ^lfsr[66] ^lfsr[68] ^lfsr[70] ^lfsr[71] ^lfsr[74];
        lfsr[ 5]<= defsk_datain_r[5] ^lfsr[65] ^lfsr[67] ^lfsr[69] ^lfsr[70] ^lfsr[73] ^lfsr[74];
        lfsr[ 4]<= defsk_datain_r[4] ^lfsr[64] ^lfsr[66] ^lfsr[68] ^lfsr[69] ^lfsr[72] ^lfsr[73] ;
        lfsr[ 3]<= defsk_datain_r[3] ^lfsr[66] ^lfsr[67] ^lfsr[68] ^lfsr[70] ^lfsr[73] ;
        lfsr[ 2]<= defsk_datain_r[2] ^lfsr[65] ^lfsr[66] ^lfsr[67] ^lfsr[69] ^lfsr[72] ;
        lfsr[ 1]<= defsk_datain_r[1] ^lfsr[64] ^lfsr[65] ^lfsr[66] ^lfsr[68] ^lfsr[71] ;
        lfsr[ 0]<= defsk_datain_r[0] ^lfsr[64] ^lfsr[66] ^lfsr[67] ^lfsr[71] ^lfsr[72] ^lfsr[73] ^lfsr[74];

    end
    else
        lfsr <= lfsr;
end //always

always @ ( posedge i_clk or negedge i_rst_n )
begin
    if( !i_rst_n )
        o_CRC_result <= 75'b0;
    else if(i_start)
        o_CRC_result <= 75'b0;
    else
        o_CRC_result <= lfsr;    
end
 
endmodule 
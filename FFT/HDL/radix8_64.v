module radix8_64
#(
    parameter   IN_W = 9,           // width of input
    parameter   OUT_W = 12          // width of output
)
(
    // input data and output data both include read part and image part.
    // the image part of input data, radix-8
    input   [IN_W-1:0]  IN_I0,IN_I1,IN_I2,IN_I3,IN_I4,IN_I5,IN_I6,IN_I7,
    // the real part of input data, radix-8
    input   [IN_W-1:0]  IN_Q0,IN_Q1,IN_Q2,IN_Q3,IN_Q4,IN_Q5,IN_Q6,IN_Q7,
    // the image part of output data, radix-8
    output  [OUT_W-1:0] OUT_I0,OUT_I1,OUT_I2,OUT_I3,OUT_I4,OUT_I5,OUT_I6,OUT_I7,
    // the read part of output data, radix-8
    output  [OUT_W-1:0] OUT_Q0,OUT_Q1,OUT_Q2,OUT_Q3,OUT_Q4,OUT_Q5,OUT_Q6,OUT_Q7
);
    // parameter define
    // the width of medium mul
    localparam  MID_MUL_W = IN_W + 2;
    // the quantization of sqrt(1/2), (2^8-1)*sqrt(1/2)≈181
    localparam  P_SQRT2   = 9'd181;

    // wire define
    wire    [IN_W+1 -1:0]   tf0,tf1,tf2,tf3;            // width is IN_W+1
    wire    [IN_W+1 -1:0]   tg0,tg1,tg2,tg3;            // width is IN_W+1
    
    wire    [IN_W+2 -1:0]   tff0,tff1,tff2,tff3;        // width is IN_W+2
    wire    [IN_W+2 -1:0]   tgg0,tgg1,tgg2,tgg3;        // width is IN_W+2

    wire    [IN_W+1 -1:0]   tp0,tp1,tp2,tp3;            // width is IN_W+1
    wire    [IN_W+1 -1:0]   tq0,tq1,tq2,tq3;            // width is IN_W+1

    wire    [IN_W+2 -1:0]   tpp0,tpp1,tpp2,tpp3;        // width is IN_W+2
    wire    [IN_W+2 -1:0]   tqq0,tqq1,tqq2,tqq3;        // width is IN_W+2


//-----------------------------8点输入的实虚部计算---------------------------------//

    // 8点输入的前四点的实部和虚部的2点FFT
    // get the MSB to extend sign bit of input data
    // real part of the first four input data
    assign  tf0 = {{{IN_I0[IN_W-1]}},IN_I0} + {{{IN_I2[IN_W-1]}},IN_I2};        // tf0 = IN_I0 + IN_I2
    assign  tf1 = {{{IN_I0[IN_W-1]}},IN_I0} - {{{IN_I2[IN_W-1]}},IN_I2};        // tf1 = IN_I0 - IN_I2
    assign  tf2 = {{{IN_I1[IN_W-1]}},IN_I1} + {{{IN_I3[IN_W-1]}},IN_I3};        // tf2 = IN_I1 + IN_I3
    assign  tf3 = {{{IN_I1[IN_W-1]}},IN_I1} - {{{IN_I3[IN_W-1]}},IN_I3};        // tf3 = IN_I1 - IN_I3

    // image part of the first four input data
    assign  tg0 = {{{IN_Q0[IN_W-1]}},IN_Q0} + {{{IN_Q2[IN_W-1]}},IN_Q2};        // tg0 = IN_Q0 + IN_Q2
    assign  tg1 = {{{IN_Q0[IN_W-1]}},IN_Q0} - {{{IN_Q2[IN_W-1]}},IN_Q2};        // tg1 = IN_Q0 - IN_Q2
    assign  tg2 = {{{IN_Q1[IN_W-1]}},IN_Q1} + {{{IN_Q3[IN_W-1]}},IN_Q3};        // tg2 = IN_Q1 + IN_Q3
    assign  tg3 = {{{IN_Q1[IN_W-1]}},IN_Q1} - {{{IN_Q3[IN_W-1]}},IN_Q3};        // tg3 = IN_Q1 - IN_Q3
    
    // 将tf0扩展符号位后，字长为IN_W+1；由于还有一次加法，所以结果的tff0再多留了一位，字长为IN_W+2
    // real
    assign  tff0 = {tf0[IN_W],tf0} + {tf2[IN_W],tf2};                           // tff0 = tf0 + tf2
    assign  tff1 = {tf1[IN_W],tf1} + {tf3[IN_W],tf3};                           // tff1 = tf1 + tf3
    assign  tff2 = {tf0[IN_W],tf0} - {tf2[IN_W],tf2};                           // tff2 = tf0 - tf2
    assign  tff3 = {tf1[IN_W],tf1} - {tf3[IN_W],tf3};                           // tff3 = tf1 - tf3
    // image
    assign  tgg0 = {tg0[IN_W],tg0} + {tg2[IN_W],tg2};                           // tgg0 = tg0 + tg2
    assign  tgg1 = {tg1[IN_W],tg1} - {tg3[IN_W],tg3};                           // tgg1 = tg1 - tg3
    assign  tgg2 = {tg0[IN_W],tg0} - {tg2[IN_W],tg2};                           // tgg2 = tg0 - tg2
    assign  tgg3 = {tg1[IN_W],tg1} + {tg3[IN_W],tg3};                           // tgg3 = tg1 + tg3

    // 8点输入的后四点的实部和虚部的2点FFT
    // 实部
    assign  tp0 = {{{IN_I4[IN_W-1]}},IN_I4} + {{{IN_I6[IN_W-1]}},IN_I6};        // tp0 = IN_I4 + IN_I6
    assign  tp1 = {{{IN_I4[IN_W-1]}},IN_I4} - {{{IN_I6[IN_W-1]}},IN_I6};        // tp1 = IN_I4 - IN_I6
    assign  tp2 = {{{IN_I5[IN_W-1]}},IN_I5} + {{{IN_I7[IN_W-1]}},IN_I7};        // tp2 = IN_I5 + IN_I7
    assign  tp3 = {{{IN_I5[IN_W-1]}},IN_I5} - {{{IN_I7[IN_W-1]}},IN_I7};        // tp3 = IN_I5 - IN_I7

    // 虚部
    assign  tq0 = {{{IN_Q4[IN_W-1]}},IN_Q4} + {{{IN_Q6[IN_W-1]}},IN_Q6};        // tq0 = IN_Q4 + IN_Q6
    assign  tq1 = {{{IN_Q4[IN_W-1]}},IN_Q4} - {{{IN_Q6[IN_W-1]}},IN_Q6};        // tq1 = IN_Q4 - IN_Q6
    assign  tq2 = {{{IN_Q5[IN_W-1]}},IN_Q5} + {{{IN_Q7[IN_W-1]}},IN_Q7};        // tq2 = IN_Q5 + IN_Q7
    assign  tq3 = {{{IN_Q5[IN_W-1]}},IN_Q5} - {{{IN_Q7[IN_W-1]}},IN_Q7};        // tq3 = IN_Q5 - IN_Q7

    // 将实虚部计算的结果求和——实部
    assign  tpp0 = {tp0[IN_W],tp0} + {tp2[IN_W],tp2};                           // tpp0 = tp0 + tp2
    assign  tpp1 = {tp1[IN_W],tp1} + {tq3[IN_W],tq3};                           // tpp1 = tp1 + tq3
    assign  tpp2 = {tp0[IN_W],tp0} - {tp2[IN_W],tp2};                           // tpp2 = tp0 - tp2
    assign  tpp3 = {tp1[IN_W],tp1} - {tq3[IN_W],tq3};                           // tpp3 = tp1 - tq3

    // 将实虚部计算的结果求和——虚部
    assign  tqq0 = {tq0[IN_W],tq0} + {tq2[IN_W],tq2};                           // tqq0 = tq0 + tq2
    assign  tqq1 = {tq1[IN_W],tq1} - {tp3[IN_W],tp3};                           // tqq1 = tq1 - tp3
    assign  tqq2 = {tq0[IN_W],tq0} - {tq2[IN_W],tq2};                           // tqq2 = tq0 - tq2
    assign  tqq3 = {tq1[IN_W],tq1} + {tp3[IN_W],tp3};                           // tqq3 = tq1 + tp3

    wire    [IN_W+3 -1:0]   mul_i1,mul_q1,mul_i3,mul_q3;    // width is IN_W+3

    // tff1和tgg1字长为IN_W+2,扩展符号位之后再求和，需要再扩展一位，字长为IN_W+3
    assign  mul_i1 = {{tff1[IN_W+1]},tff1} + {{tgg1[IN_W+1]},tgg1};                 // mul_i1 = tff1 + tgg1
    assign  mul_q1 = {{tgg1[IN_W+1]},tgg1} - {{tff1[IN_W+1]},tff1};                 // mul_q1 = tgg1 - tff1
    assign  mul_i3 = {{tff3[IN_W+1]},tff3} - {{tgg3[IN_W+1]},tgg3};                 // mul_i3 = tff3 - tgg3
    assign  mul_q3 = {{tff3[IN_W+1]},tff3} + {{tgg3[IN_W+1]},tgg3};                 // mul_q3 = tff3 + tgg3

//------------------------------------除2----------------------------------------//
    // 将求和结果截断
    wire    [MID_MUL_W-1:0] temp_mul_i1,temp_mul_q1,temp_mul_i3,temp_mul_q3;    // width is MID_MUL_W=IN_W+2
    
    // 这里截断即包含了除2操作，截去了LSB                   //IN_W+3 - MID_NUL_W = (IN_W+3) - (IN_W+2) = 1
    assign  temp_mul_i1 = mul_i1[IN_W+3 - 1 : IN_W+3 - MID_MUL_W];              // temp_mul_i1 = mul_i1 / 2
    assign  temp_mul_q1 = mul_q1[IN_W+3 - 1 : IN_W+3 - MID_MUL_W];              // temp_mul_q1 = mul_q1 / 2
    assign  temp_mul_i3 = mul_i3[IN_W+3 - 1 : IN_W+3 - MID_MUL_W];              // temp_mul_i3 = mul_i3 / 2
    assign  temp_mul_q3 = mul_q3[IN_W+3 - 1 : IN_W+3 - MID_MUL_W];              // temp_mul_q3 = mul_q3 / 2

//--------------------------------乘以sqrt(1/2)----------------------------------//
    
    // 将乘法结果与sqrt(1/2)相乘，字长加9是因为sqrt(1/2)量化后定点字长为9
    wire signed [MID_MUL_W+9 -1:0]  temp_i1,temp_q1,temp_i3,temp_q3;        // width is MID_MUL_W+9

    assign  temp_i1 = P_SQRT2 * temp_mul_i1;                                    // temp_i1 = sqrt(1/2) * temp_mul_i1
    assign  temp_q1 = P_SQRT2 * temp_mul_q1;                                    // temp_q1 = sqrt(1/2) * temp_mul_q1
    assign  temp_i3 = P_SQRT2 * temp_mul_i3;                                    // temp_i3 = sqrt(1/2) * temp_mul_i3
    assign  temp_q3 = P_SQRT2 * temp_mul_q3;                                    // temp_q3 = sqrt(1/2) * temp_mul_q3

//--------------------------------乘法结果移位-----------------------------------//

    wire    [IN_W+3 -1:0]   twi1,twq1,twi3,twq3;            // width is IN_W+3

    // 乘法结果的字长为IN_W+9=18，要截短到12位，就需要移位6次，因此截断后保留的最低位是原乘法结果的第7位
    // MID_MUL_W+9-2=(IN_W+2)+9-2=IN_W+9,  MID_MUL_W+9-2 -(IN_W+3)+1=(IN_W+2)+9-2-(IN_W+3)+1=(IN_W+9)-(IN_W+3)+1=9-3+1=7
    // 因此移位后结果是 twil[IN_W+9:7]=twil[18:7],字长为18-7+1=12bit
    assign  twi1 = temp_i1[MID_MUL_W+9-2 : MID_MUL_W+9-2 -(IN_W+3)+1];          // twi1 = temp_i1 / 64
    assign  twq1 = temp_q1[MID_MUL_W+9-2 : MID_MUL_W+9-2 -(IN_W+3)+1];          // twq1 = temp_q1 / 64
    assign  twi3 = temp_i3[MID_MUL_W+9-2 : MID_MUL_W+9-2 -(IN_W+3)+1];          // twi3 = temp_i3 / 64
    assign  twq3 = temp_q3[MID_MUL_W+9-2 : MID_MUL_W+9-2 -(IN_W+3)+1];          // twq3 = temp_q3 / 64

//-------------------------------------旋转--------------------------------------//

    wire    [IN_W+3 -1:0]   t_i0,t_i1,t_i2,t_i3,t_i4,t_i5,t_i6,t_i7;
    wire    [IN_W+3 -1:0]   t_q0,t_q1,t_q2,t_q3,t_q4,t_q5,t_q6,t_q7;

    assign  t_i0 = {tpp0[IN_W+2-1],tpp0[IN_W+2-1 : 0]} + {tff0[IN_W+2-1],tff0[IN_W+2-1 : 0]};       // t_i0 = tpp0 + tff0
    assign  t_q0 = {tqq0[IN_W+2-1],tqq0[IN_W+2-1 : 0]} + {tgg0[IN_W+2-1],tgg0[IN_W+2-1 : 0]};       // t_q0 = tqq0 + tgg0
    assign  t_i4 = {tpp0[IN_W+2-1],tpp0[IN_W+2-1 : 0]} - {tff0[IN_W+2-1],tff0[IN_W+2-1 : 0]};       // t_i4 = tpp0 - tff0
    assign  t_q4 = {tqq0[IN_W+2-1],tqq0[IN_W+2-1 : 0]} - {tgg0[IN_W+2-1],tgg0[IN_W+2-1 : 0]};       // t_q4 = tqq0 - tgg0

//------------------------------除2，用于定标一致---------------------------------//

    // 先分别扩展了符号位，然后再做加法
    assign  t_i1 = {tpp1[IN_W+2 -1],tpp1[IN_W+2 -1 : 0]} + {twi1[(IN_W+3) -1 : 0]};                 // t_i1 = tpp1 + twi1
    assign  t_q1 = {tqq1[IN_W+2 -1],tqq1[IN_W+2 -1 : 0]} + {twq1[(IN_W+3) -1 : 0]};                 // t_q1 = tqq1 + twq1

    assign  t_i5 = {tpp1[IN_W+2 -1],tpp1[IN_W+2 -1 : 0]} - {twi1[(IN_W+3) -1 : 0]};                 // t_i5 = tpp1 - twi1
    assign  t_q5 = {tqq1[IN_W+2 -1],tqq1[IN_W+2 -1 : 0]} - {twq1[(IN_W+3) -1 : 0]};                 // t_q5 = tqq1 - twq1

    assign  t_i2 = {tpp2[IN_W+2 -1],tpp2[IN_W+2 -1 : 0]} + {tgg2[IN_W+2 -1],tgg2[IN_W+2 -1 : 0]};   // t_i2 = tpp2 + tgg2
    assign  t_q2 = {tqq2[IN_W+2 -1],tqq2[IN_W+2 -1 : 0]} - {tff2[IN_W+2 -1],tff2[IN_W+2 -1 : 0]};   // t_q2 = tqq2 - tff2

    assign  t_i6 = {tpp2[IN_W+2 -1],tpp2[IN_W+2 -1 : 0]} - {tgg2[IN_W+2 -1],tgg2[IN_W+2 -1 : 0]};   // t_i6 = tpp2 - tgg2
    assign  t_q6 = {tqq2[IN_W+2 -1],tqq2[IN_W+2 -1 : 0]} + {tff2[IN_W+2 -1],tff2[IN_W+2 -1 : 0]};   // t_q6 = tqq2 + tff2

    assign  t_i3 = {tpp3[IN_W+2 -1],tpp3[IN_W+2 -1 : 0]} - {twi3[(IN_W+3) -1 : 0]};                 // t_i3 = tpp3 - twi3
    assign  t_q3 = {tqq3[IN_W+2 -1],tqq3[IN_W+2 -1 : 0]} - {twq3[(IN_W+3) -1 : 0]};                 // t_q3 = tqq3 - twq3

    assign  t_i7 = {tpp3[IN_W+2 -1],tpp3[IN_W+2 -1 : 0]} + {twi3[(IN_W+3) -1 : 0]};                 // t_i7 = tpp3 + twi3
    assign  t_q7 = {tqq3[IN_W+2 -1],tqq3[IN_W+2 -1 : 0]} + {twq3[(IN_W+3) -1 : 0]};                 // t_q7 = tqq3 + twq3

//--------------------------------输出结果赋值-----------------------------------//

    // real part
    assign  OUT_I0 = t_i0[IN_W+3 -1 : IN_W+3 - OUT_W];
    assign  OUT_I1 = t_i1[IN_W+3 -1 : IN_W+3 - OUT_W];      //9+3-1:9+3-12=11:0
    assign  OUT_I2 = t_i2[IN_W+3 -1 : IN_W+3 - OUT_W];
    assign  OUT_I3 = t_i3[IN_W+3 -1 : IN_W+3 - OUT_W];
    assign  OUT_I4 = t_i4[IN_W+3 -1 : IN_W+3 - OUT_W];
    assign  OUT_I5 = t_i5[IN_W+3 -1 : IN_W+3 - OUT_W];
    assign  OUT_I6 = t_i6[IN_W+3 -1 : IN_W+3 - OUT_W];
    assign  OUT_I7 = t_i7[IN_W+3 -1 : IN_W+3 - OUT_W];
    
    // image part
    assign  OUT_Q0 = t_q0[IN_W+3 -1 : IN_W+3 - OUT_W];
    assign  OUT_Q1 = t_q1[IN_W+3 -1 : IN_W+3 - OUT_W];
    assign  OUT_Q2 = t_q2[IN_W+3 -1 : IN_W+3 - OUT_W];
    assign  OUT_Q3 = t_q3[IN_W+3 -1 : IN_W+3 - OUT_W];
    assign  OUT_Q4 = t_q4[IN_W+3 -1 : IN_W+3 - OUT_W];
    assign  OUT_Q5 = t_q5[IN_W+3 -1 : IN_W+3 - OUT_W];
    assign  OUT_Q6 = t_q6[IN_W+3 -1 : IN_W+3 - OUT_W];
    assign  OUT_Q7 = t_q7[IN_W+3 -1 : IN_W+3 - OUT_W];

endmodule
module demod_top(
    input   wire            i_clk_108M,
    input   wire            i_clk_27M_en,
    input   wire            i_rst_n,
    input   wire    [11:0]  iv_ADC_data,
);

    wire    sync_point;

    Sync_top U_Sync_top(
        .i_clk_108M     (i_clk_108M),
        .i_clk_27M_en   (i_clk_27M_en),
        .i_rst_n        (i_rst_n),
        .iv_fsk_data    (iv_ADC_data),

        .o_sync_point   (sync_point)
    );

    Demodulation U_demodulation(
        .i_clk_108M         (i_clk_108M),
        .i_clk_27M_en       (i_clk_27M_en),
        .i_rst_n            (i_rst_n),

        .iv_ADC_data        (iv_ADC_data),
        .i_demod_en         (),

        .o_demod_result     (),
        .o_demod_result_sck ()
    );

endmodule
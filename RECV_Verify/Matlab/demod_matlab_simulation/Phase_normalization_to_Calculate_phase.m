function [ angle_tan,tan_quotient ] = Phase_normalization_to_Calculate_phase( i_data_ADC , im_datain )

%本模块实现了归一化，分子分母相除求tan值和反求arctan值三部分
%角度的转换 从(-pi/2 -- pi/2)变为(0 -- 2*pi) 并完成归一化
%输入为数据的实部i_data_ADC，和其对应的虚部im_datain
%输出为实际的arctan相位值

%%
tan_quotient = zeros(1,length(i_data_ADC));
angle_tan = zeros(1,length(i_data_ADC));
for i= 1:length(i_data_ADC)
    %%%(0 -- 2*pi)
    %(0 -- pi/4)
    if ( im_datain(i) >= 0 ) && ( i_data_ADC(i) > 0 ) && ( im_datain(i) < i_data_ADC(i))
        tan_quotient(i)  = no_FixPointNum((im_datain(i)/i_data_ADC(i)),12,11);
        angle_tan(i) = no_FixPointNum((atan(tan_quotient(i))*4/pi)*1024,11,0);
    elseif ( im_datain(i) >= 0 ) && ( i_data_ADC(i) > 0 ) && ( im_datain(i) == i_data_ADC(i))
        tan_quotient(i)  = no_FixPointNum((im_datain(i)/i_data_ADC(i)),12,11);
        angle_tan(i) = 1024;
    elseif ( im_datain(i) > 0 ) && ( i_data_ADC(i) > 0 ) && ( im_datain(i) > i_data_ADC(i) )
        tan_quotient(i)   = no_FixPointNum((i_data_ADC(i)/im_datain(i)),12,11);
        angle_tan(i) = 2048 - no_FixPointNum((atan(tan_quotient(i))*4/pi)*1024,11,0);
    %(pi/2 -- pi*3/4)
    elseif ( im_datain(i) > 0 ) && ( i_data_ADC(i) <= 0 ) && ( im_datain(i) > (-i_data_ADC(i)) )
        tan_quotient(i)  = no_FixPointNum((-i_data_ADC(i)/im_datain(i)),12,11);
        angle_tan(i) = 2048 + no_FixPointNum((atan(tan_quotient(i))*4/pi)*1024,11,0);
    elseif ( im_datain(i) > 0 ) && ( i_data_ADC(i) <= 0 ) && ( im_datain(i) == (-i_data_ADC(i)) )
        tan_quotient(i)  = no_FixPointNum((-i_data_ADC(i)/im_datain(i)),12,11);
        angle_tan(i) = 3072;
    elseif ( im_datain(i) > 0 ) && ( i_data_ADC(i) < 0 ) && ( im_datain(i) < (-i_data_ADC(i)) )
        tan_quotient(i)   = no_FixPointNum((-im_datain(i)/i_data_ADC(i)),12,11);
        angle_tan(i) = 4096 - no_FixPointNum((atan(tan_quotient(i))*4/pi)*1024,11,0);
    %(pi -- pi*5/4)
    elseif ( im_datain(i) <= 0) && ( i_data_ADC(i) < 0 ) && ( (-im_datain(i)) < (-i_data_ADC(i)) )
        tan_quotient(i)   = no_FixPointNum((im_datain(i)/i_data_ADC(i)),12,11);
        angle_tan(i) = 4096 + no_FixPointNum((atan(tan_quotient(i))*4/pi)*1024,11,0);
    elseif ( im_datain(i) <= 0) && ( i_data_ADC(i) < 0 ) && ( im_datain(i) == i_data_ADC(i) )
        tan_quotient(i)   = no_FixPointNum((im_datain(i)/i_data_ADC(i)),12,11);
        angle_tan(i) = 5120;
    elseif ( im_datain(i) < 0) && ( i_data_ADC(i) < 0 ) && ( (-im_datain(i)) > (-i_data_ADC(i)) )
        tan_quotient(i)   = no_FixPointNum((i_data_ADC(i)/im_datain(i)),12,11);
        angle_tan(i) = 6144 - no_FixPointNum((atan(tan_quotient(i))*4/pi)*1024,11,0);
    %(pi*3/2 -- pi*7/4)    
    elseif ( im_datain(i) < 0) && ( i_data_ADC(i) >= 0 ) && ( (-im_datain(i)) > (i_data_ADC(i)) )
        tan_quotient(i)   = no_FixPointNum((-i_data_ADC(i)/im_datain(i)),12,11);
        angle_tan(i) = 6144 + no_FixPointNum((atan(tan_quotient(i))*4/pi)*1024,11,0);
    elseif ( im_datain(i) < 0) && ( i_data_ADC(i) > 0 ) && ( (-im_datain(i)) == (i_data_ADC(i)) )
        tan_quotient(i)   = no_FixPointNum((-i_data_ADC(i)/im_datain(i)),12,11);
        angle_tan(i) = 7168;   
    elseif ( im_datain(i) < 0) && ( i_data_ADC(i) > 0 ) && ( (-im_datain(i)) < (i_data_ADC(i)) )
        tan_quotient(i)   = no_FixPointNum((-im_datain(i)/i_data_ADC(i)),12,11);
        angle_tan(i) = 8192 - no_FixPointNum((atan(tan_quotient(i))*4/pi)*1024,11,0);
    end

    angle_tan = no_FixPointNum( angle_tan , 15 , 0 );
end %for
% for i= 1:length(i_data_ADC)
%     %%%(0 -- 2*pi)
%     %(0 -- pi/4)
%     if ( im_datain(i) >= 0 ) && ( i_data_ADC(i) > 0 ) && ( im_datain(i) < i_data_ADC(i))
%         tan_quotient(i)  = no_FixPointNum((im_datain(i)/i_data_ADC(i)),12,11);
%         angle_tan(i) = no_FixPointNum((atan(tan_quotient(i))*4/pi)*1024,11,0);
%     elseif ( im_datain(i) >= 0 ) && ( i_data_ADC(i) > 0 ) && ( im_datain(i) == i_data_ADC(i))
%         tan_quotient(i)  = no_FixPointNum((im_datain(i)/i_data_ADC(i)),12,11);
%         angle_tan(i) = 1024;
%     elseif ( im_datain(i) > 0 ) && ( i_data_ADC(i) > 0 ) && ( im_datain(i) > i_data_ADC(i) )
%         tan_quotient(i)   = no_FixPointNum((i_data_ADC(i)/im_datain(i)),12,11);
%         angle_tan(i) = 2048 - no_FixPointNum((atan(tan_quotient(i))*4/pi)*1024,11,0);
%     %(pi/2 -- pi*3/4)
%     elseif ( im_datain(i) > 0 ) && ( i_data_ADC(i) <= 0 ) && ( im_datain(i) > (-i_data_ADC(i)) )
%         tan_quotient(i)  = no_FixPointNum((-i_data_ADC(i)/im_datain(i)),12,11);
%         angle_tan(i) = 2048 + no_FixPointNum((atan(tan_quotient(i))*4/pi)*1024,11,0);
%     elseif ( im_datain(i) > 0 ) && ( i_data_ADC(i) <= 0 ) && ( im_datain(i) == (-i_data_ADC(i)) )
%         tan_quotient(i)  = no_FixPointNum((-i_data_ADC(i)/im_datain(i)),12,11);
%         angle_tan(i) = 3072;
%     elseif ( im_datain(i) > 0 ) && ( i_data_ADC(i) < 0 ) && ( im_datain(i) < (-i_data_ADC(i)) )
%         tan_quotient(i)   = no_FixPointNum((-im_datain(i)/i_data_ADC(i)),12,11);
%         angle_tan(i) = 4096 - no_FixPointNum((atan(tan_quotient(i))*4/pi)*1024,11,0);
%     %(pi -- pi*5/4)
%     elseif ( im_datain(i) <= 0) && ( i_data_ADC(i) < 0 ) && ( (-im_datain(i)) < (-i_data_ADC(i)) )
%         tan_quotient(i)   = no_FixPointNum((im_datain(i)/i_data_ADC(i)),12,11);
%         angle_tan(i) = 4096 + no_FixPointNum((atan(tan_quotient(i))*4/pi)*1024,11,0);
%     elseif ( im_datain(i) <= 0) && ( i_data_ADC(i) < 0 ) && ( im_datain(i) == i_data_ADC(i) )
%         tan_quotient(i)   = no_FixPointNum((im_datain(i)/i_data_ADC(i)),12,11);
%         angle_tan(i) = 5120;
%     elseif ( im_datain(i) < 0) && ( i_data_ADC(i) < 0 ) && ( (-im_datain(i)) > (-i_data_ADC(i)) )
%         tan_quotient(i)   = no_FixPointNum((i_data_ADC(i)/im_datain(i)),12,11);
%         angle_tan(i) = 6144 - no_FixPointNum((atan(tan_quotient(i))*4/pi)*1024,11,0);
%     %(pi*3/2 -- pi*7/4)    
%     elseif ( im_datain(i) < 0) && ( i_data_ADC(i) >= 0 ) && ( (-im_datain(i)) > (i_data_ADC(i)) )
%         tan_quotient(i)   = no_FixPointNum((-i_data_ADC(i)/im_datain(i)),12,11);
%         angle_tan(i) = 6144 + no_FixPointNum((atan(tan_quotient(i))*4/pi)*1024,11,0);
%     elseif ( im_datain(i) < 0) && ( i_data_ADC(i) > 0 ) && ( (-im_datain(i)) == (i_data_ADC(i)) )
%         tan_quotient(i)   = no_FixPointNum((-i_data_ADC(i)/im_datain(i)),12,11);
%         angle_tan(i) = 7168;   
%     elseif ( im_datain(i) < 0) && ( i_data_ADC(i) > 0 ) && ( (-im_datain(i)) < (i_data_ADC(i)) )
%         tan_quotient(i)   = no_FixPointNum((-im_datain(i)/i_data_ADC(i)),12,11);
%         angle_tan(i) = 8192 - no_FixPointNum((atan(tan_quotient(i))*4/pi)*1024,11,0);
%     end
% 
%     angle_tan = no_FixPointNum( angle_tan , 15 , 0 );
% end %for






% for i= 1:length(i_data_ADC)
%     %%%(0 -- 2*pi)
%     %(0 -- pi/4)
%     if ( im_datain(i) >= 0 ) && ( i_data_ADC(i) > 0 ) && ( im_datain(i) < i_data_ADC(i))
%         tan_quotient(i)  = no_FixPointNum((im_datain(i)/i_data_ADC(i)),10,9);
%         angle_tan(i) = no_FixPointNum((atan(tan_quotient(i))*4/pi)*1024,10,0);
%     elseif ( im_datain(i) >= 0 ) && ( i_data_ADC(i) > 0 ) && ( im_datain(i) == i_data_ADC(i))
%         tan_quotient(i)  = no_FixPointNum((im_datain(i)/i_data_ADC(i)),10,9);
%         angle_tan(i) = 1024;
%     elseif ( im_datain(i) > 0 ) && ( i_data_ADC(i) > 0 ) && ( im_datain(i) > i_data_ADC(i) )
%         tan_quotient(i)   = no_FixPointNum((i_data_ADC(i)/im_datain(i)),10,9);
%         angle_tan(i) = 2048 - no_FixPointNum((atan(tan_quotient(i))*4/pi)*1024,10,0);
%     %(pi/2 -- pi*3/4)
%     elseif ( im_datain(i) > 0 ) && ( i_data_ADC(i) <= 0 ) && ( im_datain(i) > (-i_data_ADC(i)) )
%         tan_quotient(i)  = no_FixPointNum((-i_data_ADC(i)/im_datain(i)),10,9);
%         angle_tan(i) = 2048 + no_FixPointNum((atan(tan_quotient(i))*4/pi)*1024,10,0);
%     elseif ( im_datain(i) > 0 ) && ( i_data_ADC(i) <= 0 ) && ( im_datain(i) == (-i_data_ADC(i)) )
%         tan_quotient(i)  = no_FixPointNum((-i_data_ADC(i)/im_datain(i)),10,9);
%         angle_tan(i) = 3072;
%     elseif ( im_datain(i) > 0 ) && ( i_data_ADC(i) < 0 ) && ( im_datain(i) < (-i_data_ADC(i)) )
%         tan_quotient(i)   = no_FixPointNum((-im_datain(i)/i_data_ADC(i)),10,9);
%         angle_tan(i) = 4096 - no_FixPointNum((atan(tan_quotient(i))*4/pi)*1024,10,0);
%     %(pi -- pi*5/4)
%     elseif ( im_datain(i) <= 0) && ( i_data_ADC(i) < 0 ) && ( (-im_datain(i)) < (-i_data_ADC(i)) )
%         tan_quotient(i)   = no_FixPointNum((im_datain(i)/i_data_ADC(i)),10,9);
%         angle_tan(i) = 4096 + no_FixPointNum((atan(tan_quotient(i))*4/pi)*1024,10,0);
%     elseif ( im_datain(i) <= 0) && ( i_data_ADC(i) < 0 ) && ( im_datain(i) == i_data_ADC(i) )
%         tan_quotient(i)   = no_FixPointNum((im_datain(i)/i_data_ADC(i)),10,9);
%         angle_tan(i) = 5120;
%     elseif ( im_datain(i) < 0) && ( i_data_ADC(i) < 0 ) && ( (-im_datain(i)) > (-i_data_ADC(i)) )
%         tan_quotient(i)   = no_FixPointNum((i_data_ADC(i)/im_datain(i)),10,9);
%         angle_tan(i) = 6144 - no_FixPointNum((atan(tan_quotient(i))*4/pi)*1024,10,0);
%     %(pi*3/2 -- pi*7/4)    
%     elseif ( im_datain(i) < 0) && ( i_data_ADC(i) >= 0 ) && ( (-im_datain(i)) > (i_data_ADC(i)) )
%         tan_quotient(i)   = no_FixPointNum((-i_data_ADC(i)/im_datain(i)),10,9);
%         angle_tan(i) = 6144 + no_FixPointNum((atan(tan_quotient(i))*4/pi)*1024,10,0);
%     elseif ( im_datain(i) < 0) && ( i_data_ADC(i) > 0 ) && ( (-im_datain(i)) == (i_data_ADC(i)) )
%         tan_quotient(i)   = no_FixPointNum((-i_data_ADC(i)/im_datain(i)),10,9);
%         angle_tan(i) = 7168;   
%     elseif ( im_datain(i) < 0) && ( i_data_ADC(i) > 0 ) && ( (-im_datain(i)) < (i_data_ADC(i)) )
%         tan_quotient(i)   = no_FixPointNum((-im_datain(i)/i_data_ADC(i)),10,9);
%         angle_tan(i) = 8192 - no_FixPointNum((atan(tan_quotient(i))*4/pi)*1024,10,0);
%     end
% 
%     angle_tan = no_FixPointNum( angle_tan , 14 , 0 );
% end %for


%--------------------------------verilog原程序-----------------------------------------
% always @ ( negedge i_rst_n or posedge i_clk )
% begin
%     if ( !i_rst_n )
%         begin
%             tan_numer <= 13'd0;           //实部
%             tan_denom <= 13'd0;           //虚部
%             quadrant <= 4'd0;             //象限
%         end
%     else if( i_cke_27M ) begin
%     //第一象限部分
%         if ( (im_datain[12] == 1'b0) && (hilbert_datain_delay_4[12] == 1'b0) && (im_datain[11:0] < hilbert_datain_delay_4[11:0]) )
%             begin   
%                 tan_numer <= { 1'b0 , im_datain[11:0] };
%                 tan_denom <= { 1'b0 , hilbert_datain_delay_4[11:0] };
%                 quadrant <= 4'b0000;
%             end
%         else if ( (im_datain[12]==1'b0) && (hilbert_datain_delay_4[12]==1'b0) && (im_datain[11:0]==hilbert_datain_delay_4[11:0]) )
%             begin
%                 tan_numer <= {1'b0,im_datain[11:0]};
%                 tan_denom <= {1'b0,hilbert_datain_delay_4[11:0]};
%                 quadrant <= 4'b0001;
%             end
%         else if ( (im_datain[12]==1'b0) && (hilbert_datain_delay_4[12]==1'b0) && (im_datain[11:0]>hilbert_datain_delay_4[11:0]) )
%             begin   
%                 tan_numer <= {1'b0,hilbert_datain_delay_4[11:0]};
%                 tan_denom <= {1'b0,im_datain[11:0]};
%                 quadrant <= 4'b0010;
%             end
%     //第二象限部分
%         else if ( (im_datain[12]==1'b0) && (hilbert_datain_delay_4[12]==1'b1) && ({1'b0,im_datain[11:0]}>13'd4096 - {1'b0,hilbert_datain_delay_4[11:0]}) )
%             begin   
%                 tan_numer <= 13'd4096 - {1'b0,hilbert_datain_delay_4[11:0]};
%                 tan_denom <= {1'b0,im_datain[11:0]};
%                 quadrant <= 4'b0100;
%             end
%         else if ( (im_datain[12]==1'b0) && (hilbert_datain_delay_4[12]==1'b1) && ({1'b0,im_datain[11:0]}==13'd4096 - {1'b0,hilbert_datain_delay_4[11:0]}) )
%             begin
%                 tan_numer <= 13'd4096 - {1'b0,hilbert_datain_delay_4[11:0]};
%                 tan_denom <= {1'b0,im_datain[11:0]};
%                 quadrant <= 4'b0101;
%             end
%         else if ( (im_datain[12]==1'b0) && (hilbert_datain_delay_4[12]==1'b1) && ({1'b0,im_datain[11:0]}<13'd4096 - {1'b0,hilbert_datain_delay_4[11:0]}) )
%             begin   
%                 tan_numer <= {1'b0,im_datain[11:0]};
%                 tan_denom <= 13'd4096 - {1'b0,hilbert_datain_delay_4[11:0]};
%                 quadrant <= 4'b0110;
%             end
%     //第三象限部分
%         else if ( (im_datain[12]==1'b1) && (hilbert_datain_delay_4[12]==1'b1) && (im_datain[11:0]>hilbert_datain_delay_4[11:0]) )
%             begin   
%                 tan_numer <= 13'd4096 - {1'b0,im_datain[11:0]};
%                 tan_denom <= 13'd4096 - {1'b0,hilbert_datain_delay_4[11:0]};
%                 quadrant <= 4'b1000;
%             end
%         else if ( (im_datain[12]==1'b1) && (hilbert_datain_delay_4[12]==1'b1) && (im_datain[11:0]==hilbert_datain_delay_4[11:0]) )
%             begin
%                 tan_numer <= 13'd4096 - {1'b0,im_datain[11:0]};
%                 tan_denom <= 13'd4096 - {1'b0,hilbert_datain_delay_4[11:0]};
%                 quadrant <= 4'b1001;
%             end
%         else if ( (im_datain[12]==1'b1) && (hilbert_datain_delay_4[12]==1'b1) && (im_datain[11:0]<hilbert_datain_delay_4[11:0]) )
%             begin   
%                 tan_numer <= 13'd4096 - {1'b0,hilbert_datain_delay_4[11:0]};
%                 tan_denom <= 13'd4096 - {1'b0,im_datain[11:0]};
%                 quadrant <= 4'b1010;
%             end
%     //第四象限部分
%         else if ( (im_datain[12]==1'b1) && (hilbert_datain_delay_4[12]==1'b0) && (13'd4096 - {1'b0,im_datain[11:0]}>{1'b0,hilbert_datain_delay_4[11:0]}) )
%             begin   
%                 tan_numer <= {1'b0,hilbert_datain_delay_4[11:0]};
%                 tan_denom <= 13'd4096 - {1'b0,im_datain[11:0]};
%                 quadrant <= 4'b1100;
%             end
%         else if ( (im_datain[12]==1'b1) && (hilbert_datain_delay_4[12]==1'b0) && (13'd4096 - {1'b0,im_datain[11:0]}=={1'b0,hilbert_datain_delay_4[11:0]}) )
%             begin
%                 tan_numer <= {1'b0,hilbert_datain_delay_4[11:0]};
%                 tan_denom <= 13'd4096 - {1'b0,im_datain[11:0]};
%                 quadrant <= 4'b1101;
%             end
%         else if ( (im_datain[12]==1'b1) && (hilbert_datain_delay_4[12]==1'b0) && (13'd4096 - {1'b0,im_datain[11:0]}< {1'b0,hilbert_datain_delay_4[11:0]}) )
%             begin   
%                 tan_numer <= 13'd4096 - {1'b0,im_datain[11:0]};
%                 tan_denom <= {1'b0,hilbert_datain_delay_4[11:0]};
%                 quadrant <= 4'b1110;
%             end
%     end
%     else begin
%            tan_numer <= tan_numer;   
%            tan_denom <= tan_denom; 
%            quadrant <= quadrant;                              
%        end
% end
% //对象限进行延时
% always @ ( negedge i_rst_n or posedge i_clk )
% begin
%   if ( !i_rst_n )
%       quadrant_delay <= 4'd0;
%   else if( i_cke_27M ) begin
%       quadrant_delay <= quadrant;
%   end
%   else begin
%       quadrant_delay <= quadrant_delay;
%   end
% end
% //查表，得出相位对应到第一象限的角度值，先得到ROM表的对应地址
% tan_divider tan_divider(
%                         .denom(tan_denom[11:0]),
%                         .numer({tan_numer[11:0],9'd0}),
%                         .quotient(tan_quotient),
%                         .remain()
%                         ); 
% //通过ROM表，给出具体的相位值
% wire [9:0]acrtan;
% rom_tan uu_rom_tan(
%                 .address(tan_quotient[8:0]),
%                 .clock(i_clk),
%                 .q(acrtan)
%                 );
% //恢复相位
% always @ ( negedge i_rst_n or posedge i_clk )
% begin
%   if ( !i_rst_n )
%     angle_tan <= 14'd0;
%   else if( i_cke_27M ) begin
%       case(quadrant_delay)
%           4'b0000: angle_tan <= {4'b0,acrtan };
%           4'b0001: angle_tan <= 14'd1024;
%           4'b0010: angle_tan <= 14'd2048 - {4'b0,acrtan};          
%           4'b0100: angle_tan <= 14'd2048 + {4'b0,acrtan};
%           4'b0101: angle_tan <= 14'd3072;
%           4'b0110: angle_tan <= 14'd4096 - {4'b0,acrtan};        
%           4'b1000: angle_tan <= 14'd4096 + {4'b0,acrtan};
%           4'b1001: angle_tan <= 14'd5120;
%           4'b1010: angle_tan <= 14'd6144 - {4'b0,acrtan};
%           4'b1100: angle_tan <= 14'd6144 + {4'b0,acrtan};
%           4'b1101: angle_tan <= 14'd7168;
%           4'b1110: angle_tan <= 14'd8192 - {4'b0,acrtan};
%           default: angle_tan <= angle_tan;
%       endcase
%   end 
%   else begin
%       angle_tan <= angle_tan;
%   end
% end

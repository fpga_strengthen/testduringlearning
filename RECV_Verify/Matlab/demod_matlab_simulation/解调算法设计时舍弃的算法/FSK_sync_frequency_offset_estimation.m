%检测偏差点在1个点以内时，频偏的最大范围
%平方和法及改进版实验室Hilbert算法对比
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%FSK调制%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc
format long
close all
clear all

%% 标准条件下主要参数
f0_true = 3.951 ;
f1_true = 4.516 ;              %比特0和比特1的频点，单位MHz
f_mid = (f0_true + f1_true)/2; % 中心频率
sample_rate_true = 27.095;     % 标准采样速率，单位MHz
BPS_data_true = 0.56448;       % 标准数据传输速率，单位MHz
ws = BPS_data_true*2*pi;
sample_point = ceil(sample_rate_true/BPS_data_true); %一个周期的采样点数 48
sum_bits = 100;%比特数
Hd = LP_6M;    %低通滤波
% src_data = randint(1,sum_bits);%以比特向量方式输入的待调制数据，随机产生
src_data([1:2:sum_bits-1])=1;
src_data([2:2:sum_bits])=0;
src_data_sample =  kron(src_data,ones(1,sample_point));%将src_data采样
static_lenth_x_axis = sample_point * sum_bits + 50;

% 构造本地载波
S0 = cos( f0_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);
S1 = cos( f1_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);
S0_Q = sin(f0_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);%%移相90°
S1_Q = sin(f1_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);%%移相90°

%最小二乘法
%二次曲线拟合
N = 3; % 2N+1点最小二乘法
coeff_numerator = 3*[-N:N];           %极值点多项式分子
coeff_denominator = 8 - 2 * [-N:N].^2;%极值点多项式分母
%一次曲线拟合17点
N_17 = 8; % 2N+1点最小二乘法
coeff_slope = (1/408)*[-N_17:N_17];    %斜率
coeff_intercept = (1/17) * ones(1,2*N_17+1);%截距

ii = 0;

%% 比较特殊频率处理抵消效果
for f0 = -0.2:0.01:0.2 %频偏f0在-200K~200K之间，间隔10K连续变化
    for f1 =-0.2:0.01:0.2 %%频偏f1在-200K~200K之间，间隔10K连续变化
        %         for phase_0 = -pi : pi/5 : pi %初始相位在[-pi,pi]之间变化
        %         phase_0 = 0;
        %         w0= f0*2*pi;
        %         w1 =f1*2*pi;
        
        %产生FSK调制信号
        BPS_data = BPS_data_true;%数据传输速率，单位MHz
        sample_rate =  sample_rate_true;%采样速率，单位MHz
        FSK_data  =FSK_mod(src_data,f0_true + f0,f1_true + f1,sample_rate,BPS_data);
        %对输入数据进行定标
        FSK_data = FSK_data  - 2048;%FixPointNum(FSK_data  - 2048, 12, 0);%字长12bit，精度0位小数 + FSK_data_noise
        length_fsk_data = length(FSK_data);
        
        %% 相关解调
        % 混频并滤除高频信号
        FSK_data_S0     = filter(Hd,FSK_data .* S0(1:length_fsk_data));%filter(Hd,
        FSK_data_S0_Q   = filter(Hd,FSK_data .* S0_Q(1:length_fsk_data));%filter(Hd,
        FSK_data_S1     = filter(Hd,FSK_data .* S1(1:length_fsk_data));%filter(Hd,
        FSK_data_S1_Q   = filter(Hd,FSK_data .* S1_Q(1:length_fsk_data));% filter(Hd,
        
        %         %相关解调 忽略高频后的结果
        %         t = 1./sample_rate_true.*[1:sample_point*sum_bits];
        %         FSK_data_S0 = (1/2*2048).*((1 - src_data_sample).*cos(w0.*t + phase_0) + src_data_sample.*cos((w1+ ws).*t + phase_0));
        %         FSK_data_S0_Q = (1/2*2048).*((1 - src_data_sample).*(-sin(w0.*t + phase_0)) + src_data_sample.*(-sin((w1+ ws).*t + phase_0)));
        %         FSK_data_S1 = (1/2*2048).*((1 - src_data_sample).*cos((w0-ws).*t + phase_0) + src_data_sample.*cos(w1.*t + phase_0));
        %         FSK_data_S1_Q = (1/2*2048).*((1 - src_data_sample).*(-sin((w0-ws).*t + phase_0)) + src_data_sample.*(-sin(w1.*t + phase_0)));
        %
        %         figure(11),
        %         plot(FSK_data,'k'),
        %         plot(FSK_data_S0,'r'),
        %         %         hold on,
        %         plot(FSK_data_S0_Q,'g'),
        %         plot(FSK_data_S1,'m'),
        %         plot(FSK_data_S1_Q,'b'),
        
        %% 平方和法求同步点
        for i =1:length_fsk_data-23 %相干解调过程,24个点求和，再除以24
            sum_bit0_point(i) = ( (sum(FSK_data_S0(i:i+23)))^2 + (sum(FSK_data_S0_Q(i:i+23)))^2 )./24  ;
            sum_bit1_point(i) = ( (sum(FSK_data_S1(i:i+23)))^2 + (sum(FSK_data_S1_Q(i:i+23)))^2 )./24  ;
        end
        sum_bit0_mins_sum_bit0 = sum_bit0_point(1:end-24) - sum_bit0_point(25:end);
        sum_bit1_mins_sum_bit1 = sum_bit1_point(1:end-24) - sum_bit1_point(25:end);
        sum_bit0_mins_sum_bit1 = sum_bit0_mins_sum_bit0 - sum_bit1_mins_sum_bit1;
        
        sum_bit0_mins_sum_bit1 = [zeros(1,12),sum_bit0_mins_sum_bit1(1:end)];%第1个点减第25个点得到的是第12个点的结果，而目前将该结果存在第1个位置，所以需要移位12点才正确
        sum_bit0_mins_sum_bit1 = abs(sum_bit0_mins_sum_bit1);%去掉符号位
        
        for i = 1:length(sum_bit0_mins_sum_bit1)
            if (sum_bit0_mins_sum_bit1(i) < max(sum_bit0_mins_sum_bit1)*0.8)%下溢清零
                sum_bit0_mins_sum_bit1(i) = 0;
            else
                sum_bit0_mins_sum_bit1(i) = sum_bit0_mins_sum_bit1(i) - max(sum_bit0_mins_sum_bit1)*0.8; %减小位数
            end %if
        end %for
        
        % 求极值点坐标处理 最小二乘法的二次曲线拟合
        temp_val = toeplitz(sum_bit0_mins_sum_bit1(1:2*N+1),sum_bit0_mins_sum_bit1);
        sync_point = (coeff_numerator * temp_val) ./ ( coeff_denominator * temp_val);%详见文档极值点坐标公式推导
        
        sync_point(sync_point > 5) = 5;%极值应在[-3,3]之间，上溢处理
        sync_point(sync_point < -5) = -5;
        sync_point = (abs(sync_point)<0.6).* (sum_bit0_mins_sum_bit1>0);%对称轴在[-0.6,0.6]之间且该点不为零时，该坐标轴原点位置为所求极值点
        sync_point = sync_point(4:end); %该算法会让极值点向后移3个点
        sync_point = sync_point(1:end).*(~[0,sync_point(1:end-1)])%取上升沿作极值点
        
        %         % 画图
        %         figure(12),
        %         subplot(2,1,1),
        %         hold off,
        %         plot(sum_bit0_point,'r');
        %         hold on,
        %         plot(sum_bit1_point,'g');
        %         plot(sum_bit0_mins_sum_bit1);
        %         legend('f0混频滤波24点平方和','f1混频滤波24点平方和','相差24点后相减结果');
        %         title(strcat('FSK信号频偏：w0=' ,num2str(f0),' ,w1= ',num2str(f1), 'Mhz,'));
        %
        %         figure(12),
        %         subplot(2,1,2),
        %         hold off,
        %         hold on;
        %         plot(sum_bit0_mins_sum_bit1/max(sum_bit0_mins_sum_bit1)*8,'k');
        %         plot(sync_point.*10 ,'g-+');
        %         legend('判断折线','同步点')
        %         grid on;
        
        %% 前后17点直线拟合求拐点
        hilbert_fsk = hilbert(FSK_data);  %求出理论上的解析信号
        arctan_fsk = hilbert_fsk .* exp(-j*[1:length(hilbert_fsk)]*2*pi*4.234/sample_rate);%处理中心频率；
        arctan_fsk = angle(arctan_fsk); %求出解析信号所对应的相位
        arctan_fsk = unwrap(arctan_fsk,pi);  %对相位进行2pi的修正
        
        temp_val_forward_1 = toeplitz(arctan_fsk(1:2*N_17+1),arctan_fsk);
        slope_forward = coeff_slope * temp_val_forward_1 ;
        intercept_forward = coeff_intercept * temp_val_forward_1;%详见文档极值点坐标公式推导
        
        arctan_fsk = [arctan_fsk(1+2*N_17+1:end),zeros(1,2*N_17+1)];%移2*N_17+1个点
        temp_val_after_1 = toeplitz(arctan_fsk(1:2*N_17+1),arctan_fsk);
        slope_after = coeff_slope * temp_val_after_1 ;
        intercept_after = coeff_intercept * temp_val_after_1;
        
        intersection = (intercept_after - intercept_forward)./(slope_forward - slope_after + eps) ; %求出交点坐标
        
        intersection_slop_decisioned = zeros(1,length(intersection));
        for i=2:length(intersection)
            if(abs(intersection(i) < 0.5))
                if((slope_after(i) > 0.02689 && slope_after(i) <0.11486) && (slope_forward(i) > -0.11486 && slope_forward(i) < -0.02689))
                    intersection_slop_decisioned(i) = 2;
                elseif((slope_forward(i) > 0.02689 && slope_forward(i) <0.11486) && (slope_after(i) > -0.11486 && slope_after(i) < -0.02689))
                    intersection_slop_decisioned(i) = 0;
                else
                    intersection_slop_decisioned(i)= intersection_slop_decisioned(i-1);
                end
            else
                intersection_slop_decisioned(i) = intersection_slop_decisioned(i-1);
            end %if
        end%for
        intersection_slop_decisioned = xor( intersection_slop_decisioned(1:end-1),intersection_slop_decisioned(2:end) );
        
        %         figure(13),
        %         plot(arctan_fsk);
        %         hold on;
        %         plot(slope_forward*1000,'r' );
        %         plot(slope_after*1000,'g');
        %         plot(intersection_slop_decisioned*100,'m');
        %         legend('相位折线','前斜率*1000','后斜率*1000','同步点');
        %         title(strcat('FSK信号频偏：w0=' ,num2str(f0),' ,w1= ',num2str(f1), 'Mhz,时的相位折线和斜率'));
        %         pause,
        
        %% 17点直线拟合预测算法
        forecast_1 = slope_forward *9 + intercept_forward;
        forecast_2 = slope_forward  + forecast_1;
        difference_1 = abs(forecast_1(1:end-8) - arctan_fsk(9:end));
        difference_2 = abs(forecast_2(1:end-9) - arctan_fsk(10:end));
        
        syc_point_forcast = zeros(1,length(difference_1));
        for i = 1:length(difference_2)
        if((difference_1(i)>0.97) && (difference_2(i)>0.93))
        syc_point_forcast(i) = 1;
        else
         syc_point_forcast(i) = 0;   
        end %if
        end %for
        syc_point_forcast = syc_point_forcast(1:end) & (~[0,syc_point_forcast(1:end-1)]);   
        
%         figure(15),
%         plot(arctan_fsk);
%         hold on,
%         plot(difference_1,'r');
%         plot(difference_2,'g');
%         plot(syc_point_forcast*5,'m');
%         legend('相位折线','预测一个点与实际值的差','预测2个点与实际值的差','同步点');
%         grid on,
        
        %% 实验室现用8点求拐点
        [ slope_forward_8,intersection_slop_decisioned_8] = Least_Square_Method_theroy_1( arctan_fsk );
        intersection_slop_decisioned_8 = intersection_slop_decisioned_8(9:end);
        
        %         figure(14),
        %         plot(arctan_fsk);
        %         hold on;
        %         plot(intersection_slop_decisioned_8,'r');
        %         legend('相位折线','同步点');
        %         title(strcat('FSK信号频偏：w0=' ,num2str(f0),' ,w1= ',num2str(f1), 'Mhz,时8点Hilbert算法求得的拐点'));
       
        
        %% 判断两相邻极值点是否是48的整数倍
        %% 平方和法
        edge_position = find(sync_point == 1);%极值点的位置
        % 相邻同步点的差值
        edge_variance = mod(edge_position(2:length(edge_position)) - edge_position(1:length(edge_position)-1),48);%两极值点之间的间隔对48点取模，得到偏差量
        
        %         %与正确同步点的差值
        %         theory_position = [38 38+48];
        %         for i=2:length(edge_position)-1
        %             if ( (abs(edge_position(i-1)-theory_position(i))>24) && (abs(edge_position(i)-theory_position(i))<24) &&(abs(edge_position(i+1)-theory_position(i))>24) )%理论拐点附近有单一拐点
        %                 deviation(i) = abs(edge_position(i)-theory_position(i));
        %                 theory_position(i+1) = theory_position(i) + 48;
        %             elseif ( (abs(edge_position(i-1)-theory_position(i))>24) && (abs(edge_position(i)-theory_position(i))<24) &&(abs(edge_position(i+1)-theory_position(i))<24) )%理论拐点附近有2个拐点
        %                 deviation(i) = abs(edge_position(i)-theory_position(i));
        %                 theory_position(i+1) = theory_position(i) ;
        %             elseif ( (abs(edge_position(i-1)-theory_position(i))<24) && (abs(edge_position(i)-theory_position(i))<24) &&(abs(edge_position(i+1)-theory_position(i))<24) )%理论拐点附近有3个拐点
        %                 deviation(i) = min(deviation(i-1),abs(edge_position(i)-theory_position(i)));
        %                 theory_position(i+1) = theory_position(i) ;
        %             elseif ( (abs(edge_position(i-1)-theory_position(i))<24) && (abs(edge_position(i)-theory_position(i))<24) &&(abs(edge_position(i+1)-theory_position(i))>24) )%理论拐点附近有2个拐点
        %                 deviation(i) = min(deviation(i-1),abs(edge_position(i)-theory_position(i)));
        %                 theory_position(i+1) = theory_position(i) + 48;
        %             elseif ( abs(edge_position(i)-theory_position(i))>24)%理论拐点附近无拐点，漏判
        %                 deviation(i) = 100;
        %                 theory_position(i+1) = theory_position(i) + 48;
        %                 edge_position(i+1:end+1) = edge_position(i:end);
        %             end %if
        %         end%for
        
        %         figure(13),
        %         plot(deviation);
        %         title(strcat('FSK信号频偏：w0=' ,num2str(f0),' ,w1= ',num2str(f1), 'Mhz,时的偏差量'));
        
        ii = ii+1;
        x_axis(ii) = f0;
        y_axis(ii) = f1;
        if(isempty(edge_position))
            z_axis(ii) = 100;
        elseif (length(edge_position)<99)
            z_axis(ii) = length (find((edge_variance>1)&(edge_variance<47)))+(99-length(edge_position));
        else
            z_axis(ii) = length (find((edge_variance>1)&(edge_variance<47)));%偏差多于一个点
        end
        %         z_axis(ii) = length (find(deviation>1));%偏差多于一个点
        
        figure(1),
        c = z_axis(ii)+1;
        scatter3(x_axis(ii),y_axis(ii),z_axis(ii),30,c,'filled');
        caxis([1,100]);
        colorbar;
        hold on,
        xlabel('频偏f0(MHz)');
        ylabel('频偏f1(MHz)');
        zlabel('偏差量大于一个点的个数');
        title(strcat('平方和法FSK信号在不同频偏下统计偏移量'));
        
        %% 前后17点直线拟合求拐点
        edge_position_hilbert = find(intersection_slop_decisioned == 1);%极值点的位置
        % 相邻同步点的差值
        edge_variance_hilbert = mod(edge_position_hilbert(2:length(edge_position_hilbert)) - edge_position_hilbert(1:length(edge_position_hilbert)-1),48);%两极值点之间的间隔对48点取模，得到偏差量
        if(isempty(edge_position_hilbert))
            z_axis(ii) = 100;
        elseif (length(edge_position_hilbert)<99)
            z_axis(ii) = length (find((edge_variance_hilbert>1)&(edge_variance_hilbert<47)))+(99-length(edge_position_hilbert));
        else
            z_axis(ii) = length (find((edge_variance_hilbert>1)&(edge_variance_hilbert<47)));%偏差多于一个点
        end
        
        figure(2),
        c = z_axis(ii)+1;
        scatter3(x_axis(ii),y_axis(ii),z_axis(ii),30,c,'filled');
        caxis([1,100]);
        colorbar;
        hold on,
        xlabel('频偏f0(MHz)');
        ylabel('频偏f1(MHz)');
        zlabel('偏差量大于一个点的个数');
        title(strcat('前后17点直线拟合FSK信号在不同频偏下统计偏移量'));
        
        %% 8点拟合求拐点
        edge_position_hilbert8 = find(intersection_slop_decisioned_8 == 1);%极值点的位置
        % 相邻同步点的差值
        edge_variance_hilbert8 = mod(edge_position_hilbert8(2:length(edge_position_hilbert8)) - edge_position_hilbert8(1:length(edge_position_hilbert8)-1),48);%两极值点之间的间隔对48点取模，得到偏差量
        if(isempty(edge_position_hilbert8))
            z_axis(ii) = 100;
        elseif (length(edge_position_hilbert8)<99)
            z_axis(ii) = length (find((edge_variance_hilbert8>1)&(edge_variance_hilbert8<47)))+(99-length(edge_position_hilbert8));
        else
            z_axis(ii) = length (find((edge_variance_hilbert8>1)&(edge_variance_hilbert8<47)));%偏差多于一个点
        end
        
        figure(3),
        c = z_axis(ii)+1;
        scatter3(x_axis(ii),y_axis(ii),z_axis(ii),30,c,'filled');
        caxis([1,100]);
        colorbar;
        hold on,
        xlabel('频偏f0(MHz)');
        ylabel('频偏f1(MHz)');
        zlabel('偏差量大于一个点的个数');
        title(strcat('前后8点直线拟合FSK信号在不同频偏下统计偏移量'));
        
        %% 17点直线拟合预测算法
        edge_position_hilbert_forcast = find(syc_point_forcast == 1);%极值点的位置
        % 相邻同步点的差值
        edge_variance_hilbert_forcast = mod(edge_position_hilbert_forcast(2:length(edge_position_hilbert_forcast)) - edge_position_hilbert_forcast(1:length(edge_position_hilbert_forcast)-1),48);%两极值点之间的间隔对48点取模，得到偏差量
        if(isempty(edge_position_hilbert_forcast))
            z_axis(ii) = 100;
        elseif (length(edge_position_hilbert_forcast)<99)
            z_axis(ii) = length (find((edge_variance_hilbert_forcast>1)&(edge_variance_hilbert_forcast<47)))+(99-length(edge_position_hilbert_forcast));
        else
            z_axis(ii) = length (find((edge_variance_hilbert_forcast>1)&(edge_variance_hilbert_forcast<47)));%偏差多于一个点
        end
        
        figure(4),
        c = z_axis(ii)+1;
        scatter3(x_axis(ii),y_axis(ii),z_axis(ii),30,c,'filled');
        caxis([1,100]);
        colorbar;
        hold on,
        xlabel('频偏f0(MHz)');
        ylabel('频偏f1(MHz)');
        zlabel('偏差量大于一个点的个数');
        title(strcat('17点直线拟合预测FSK信号在不同频偏下统计偏移量'));
        
        %         end %for phase_0
    end %for w1
end %for w0

% j = find(z_axis == 0);
% figure(2),
% plot(x_axis(j),y_axis(j))
% xlabel('频偏f0(MHz)');
% ylabel('频偏f1(MHz)');
% title(strcat('FSK信号在不同频偏下统计偏移量<1的范围'));



%% ---------------------验证不同点最小二乘法的斜率公式---------------------------------------
% w0 = 2*pi*(-0.282+0.2);
% w1 = 2*pi*( 0.282+0.2);
% Ts = 1/sample_rate;
% for N = 8:2:16
%     n = [-N:N];
%     a_n = Ts./(2.*N.*(N+1).*(2*N+1)).*(w1.*(N-n).*(N-n+1).*(2*N+n+1)+w0.*(N+n).*(N+n+1).*(2*N-n+1));
%     
%     figure(4);
%     plot(n,a_n,'r');
%     hold on;
%     grid on;
%     
%     for i = 2:2*N+1
%         if( (a_n(i)<=0) && (a_n(i-1)>=0) )
%             plot(i-N-1,a_n(i),'b+');
%         end %if
%     end %for
%     
% end %for
% 
% figure(4);
% title('推导出的斜率过渡区公式');

% %%---------------------验证不同点最小二乘法过零点的截距公式---------------------------------------
% w0 = 2*pi*(-0.282);
% w1 = 2*pi*( 0.282);
% Ts = 1/sample_rate;
% for N = 4:4:16
%     n = [-N:N];
%     b_n = Ts.*(n.*(w0+w1)./2+(N^2+N+n.^2)./(2*N+1).*(w0-w1)./2);
%     figure(5);
%     plot(n,b_n,'r');
%     hold on;
%     grid on;
% end %for
% legend('N=4截距','N=8截距','N=12截距','N=16截距');

%% -----------------最小二乘法---------------------------------------
% %直线拟合17点
% N_17 = 8; % 2N+1点最小二乘法
% coeff_slope_17 = (1/408)*[-N_17:N_17];    %斜率
% coeff_intercept_17 = (1/17) * ones(1,2*N_17+1);%截距
% temp_val_17 = toeplitz(arctan_fsk(1:2*N_17+1),arctan_fsk);
% slope_17 = coeff_slope_17 * temp_val_17 ;%斜率
% intercept_17 = coeff_intercept_17 * temp_val_17;%截距
% 
% figure(2),
% plot([slope_17(N_17:end)*1000,zeros(1,N_17-1)],'r');
% hold on,

% %直线拟合33点
% N_33 = 16; % 2N+1点最小二乘法
% coeff_slope_33 = (1/2992)*[-N_33:N_33];    %斜率
% coeff_intercept_33 = (1/33) * ones(1,2*N_33+1);%截距
% temp_val_33 = toeplitz(arctan_fsk(1:2*N_33+1),arctan_fsk);
% slope_33 = coeff_slope_33 * temp_val_33 ;%斜率
% intercept_33 = coeff_intercept_33 * temp_val_33;%截距
% 
% plot([slope_33(N_33:end)*1000,zeros(1,N_33-1)],'b');
% grid on;
% legend('17点斜率','29点斜率','33点斜率');
% title('不同点数最小二乘法的到的斜率');


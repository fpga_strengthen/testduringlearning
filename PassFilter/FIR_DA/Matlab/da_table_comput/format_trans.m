%% 函数功能：将输入的二进制补码数据转化为原码并计算出对应的十进制数后输出
% 要求：输入数据为字符串形式的二进制数据，如"1100111010"，或'101010100111'
% 注：此函数一次只针对处理一个二进制补码数据，不能处理数组，所以处理矩阵数据时，请使用循环
% 如果数据在csv文件中，选中该列，并选择表示方式为文本，再重选为数字，就可以以01010...形式表示而不会转化为e指数表示
% 如果以文本转化为数字后有小数点，可以选中列后减少小数位数，只保留整数部分
% bit_width为输入的最大比特数据长度，对于当前的数据，只有等于bit_width时才考虑符号位，否则就都是正数

%% 函数实现
function signed_dec = format_trans(char_data)
        
    str_data = char(char_data);             % 将输入的字符转化为字符串

    L = length(str_data);       % 获取输入数据的位数
    Sig_bit = str_data(1);      % 获取MSB作为符号位
    
    % 先判断数据位长是否达到为有符号数的要求
    signed_dec = bin2dec(str_data);
   
    % 如果是正数，则直接可以转化为十进制
    if(Sig_bit == '0')
        signed_dec = bin2dec(str_data);
    end

    if(Sig_bit == '1')
        % 如果是负数，要先求反码、补码，再求原码
        % 对数据位再求补码(先逐位取反，转化为十进制再加1)
        for i = 2:L
            % 获取每一个比特位并求反
            bit = str_data(i);
            if(bit == '0')
                str_data(i) = '1';
            end
            if(bit == '1')
                str_data(i) = '0';
            end
        end
        % 将数据部分的反码加1得到原码
        char_data_orig = bin2dec(str_data(2:end)) + 1;
        signed_dec = -char_data_orig;
    end
end


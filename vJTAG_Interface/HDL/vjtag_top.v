module vjtag_top(
    input   wire        i_clk_50M,
    input   wire        i_rst_n,

    output  wire [3:0]   LEDs
);

    wire    ir_out_sig;
    wire    tdo_sig   ;
    wire    ir_in_sig ;
    wire    tck_sig   ;
    wire    tdi_sig   ;
    wire    virtual_state_sdr_sig;
    wire    virtual_state_udr_sig;

    reg [3:0]   cnt;
    reg         aclr_sig;

    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt <= 4'd0;
        else if(cnt == 4'd15)
            cnt <= cnt;
        else 
            cnt <= cnt + 1'b1;
    end

    always@(posedge i_clk_50M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            aclr_sig <= 1'b1;
        else if(cnt == 4'd15)
            aclr_sig <= 1'b0;
        else
            aclr_sig <= aclr_sig;
    end

    vJTAG_interface U_vJTAG_interface(
        .tck    (tck_sig),                      //所有虚拟JTAG的共享时钟.
        .tdi    (tdi_sig),                      //串接输入数据.
        .aclr   (aclr_sig),                     //异步清零.
        .ir_in  (ir_in_sig),                    //需要指定带宽，该信号通常全为0.
        .v_sdr  (virtual_state_sdr_sig),        //Indicates that v-jtag is in Shift_DR state.
        .udr    (virtual_state_udr_sig),        //Indicates that v-jtag is in Updata_DR state.

        .LEDs   (LEDs),
        .tdo    (tdo_sig)
    );

    v_jtag	v_jtag_inst(
    	.ir_out ( ir_out_sig),
    	.tdo    ( tdo_sig   ),
    	
        .ir_in  ( ir_in_sig ),
    	.tck    ( tck_sig   ),
    	.tdi    ( tdi_sig   ),
    	.virtual_state_cdr (),
    	.virtual_state_cir (),
    	.virtual_state_e1dr(),
    	.virtual_state_e2dr(),
    	.virtual_state_pdr (),
    	.virtual_state_sdr (virtual_state_sdr_sig),
    	.virtual_state_udr (virtual_state_udr_sig),
    	.virtual_state_uir ()
    );

endmodule
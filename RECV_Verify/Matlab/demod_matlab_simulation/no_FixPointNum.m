function fixPointNum = no_FixPointNum(float_num, WordLength, precision )
    % 定点化程序
    %float_num为要定点的数，带符号数
    %WordLength为定点数字长
    %precision为定点小数位数
    % tmp     =    2 ^ precision;
    % fixPointNum  = fix(float_num .* tmp);
    % 
    % fixPointNum(fixPointNum > 2^(WordLength-1)) = 2^(WordLength-1) -1;
    % fixPointNum(fixPointNum < -2^(WordLength-1)) = 1- 2^(WordLength-1);
    % 
    % %整数化为小数
    % fixPointNum = fixPointNum ./ tmp;
     fixPointNum = float_num;
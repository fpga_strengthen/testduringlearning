`timescale 1ns/1ps
module shift_reg_tb();

    parameter   taps = 8, width = 16;

    reg             clk;
    reg             rst_n;
    wire[width-1:0] d_out;

    initial begin
        clk = 1'b0;
        rst_n <= 1'b0;
        #30
        rst_n <= 1'b1;
    end

    always #10 clk = ~clk;

    shift_reg #(
        .taps (taps),
        .width(width)
    )shift_reg_inst(
        .clk    (clk),
        .rst_n  (rst_n),
        .d_in   (16'b1010_0101_1100_0011),
        .d_out  (d_out)
    );


endmodule
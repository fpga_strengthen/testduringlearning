1. Create Project Folder quickly.

File "CreateQuartusPrj.bat" helps you create a project struct quickly if you use Quartus Software.
What you need to do is only double clicking this file and it will finish creating automatically.
If you use TangDynasty Software, please double click "CreateTDPrj.bat" and wait for few seconds.


2. Quickly boot ModelSim and Simulate.

If you use this function, please ensure that you have added modelsim path into system environment variable PATH, in which you need to locate the folder "win64" eventually, that is to say where the folder modelsim.exe exists.
If you haven't configed this, you will fail to boot ModelSim Software because the system doesn't know where the exucatable file is.

First you need to put file RunModelSim.bat in foler ModelSim which was created in the first step above.
Then you have to write run.do file and put it in folder ../ModelSim.

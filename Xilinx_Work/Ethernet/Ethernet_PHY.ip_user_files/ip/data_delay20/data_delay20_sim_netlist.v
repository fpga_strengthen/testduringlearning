// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Wed Aug 21 20:43:54 2024
// Host        : J_GMson running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               d:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/data_delay20/data_delay20_sim_netlist.v
// Design      : data_delay20
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7vx485tffg1157-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "data_delay20,c_shift_ram_v12_0_13,{}" *) (* downgradeipidentifiedwarnings = "yes" *) (* x_core_info = "c_shift_ram_v12_0_13,Vivado 2019.1" *) 
(* NotValidForBitStream *)
module data_delay20
   (A,
    D,
    CLK,
    Q);
  (* x_interface_info = "xilinx.com:signal:data:1.0 a_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME a_intf, LAYERED_METADATA undef" *) input [5:0]A;
  (* x_interface_info = "xilinx.com:signal:data:1.0 d_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME d_intf, LAYERED_METADATA undef" *) input [7:0]D;
  (* x_interface_info = "xilinx.com:signal:clock:1.0 clk_intf CLK" *) (* x_interface_parameter = "XIL_INTERFACENAME clk_intf, ASSOCIATED_BUSIF q_intf:sinit_intf:sset_intf:d_intf:a_intf, ASSOCIATED_RESET SCLR, ASSOCIATED_CLKEN CE, FREQ_HZ 100000000, PHASE 0.000, INSERT_VIP 0" *) input CLK;
  (* x_interface_info = "xilinx.com:signal:data:1.0 q_intf DATA" *) (* x_interface_parameter = "XIL_INTERFACENAME q_intf, LAYERED_METADATA undef" *) output [7:0]Q;

  wire [5:0]A;
  wire CLK;
  wire [7:0]D;
  wire [7:0]Q;

  (* c_addr_width = "6" *) 
  (* c_ainit_val = "00000000" *) 
  (* c_default_data = "00000000" *) 
  (* c_depth = "36" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "1" *) 
  (* c_has_ce = "0" *) 
  (* c_has_sclr = "0" *) 
  (* c_has_sinit = "0" *) 
  (* c_has_sset = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "0" *) 
  (* c_shift_type = "1" *) 
  (* c_sinit_val = "00000000" *) 
  (* c_sync_enable = "0" *) 
  (* c_sync_priority = "1" *) 
  (* c_verbosity = "0" *) 
  (* c_width = "8" *) 
  (* c_xdevicefamily = "virtex7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  data_delay20_c_shift_ram_v12_0_13 U0
       (.A(A),
        .CE(1'b1),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule

(* C_ADDR_WIDTH = "6" *) (* C_AINIT_VAL = "00000000" *) (* C_DEFAULT_DATA = "00000000" *) 
(* C_DEPTH = "36" *) (* C_ELABORATION_DIR = "./" *) (* C_HAS_A = "1" *) 
(* C_HAS_CE = "0" *) (* C_HAS_SCLR = "0" *) (* C_HAS_SINIT = "0" *) 
(* C_HAS_SSET = "0" *) (* C_MEM_INIT_FILE = "no_coe_file_loaded" *) (* C_OPT_GOAL = "0" *) 
(* C_PARSER_TYPE = "0" *) (* C_READ_MIF = "0" *) (* C_REG_LAST_BIT = "0" *) 
(* C_SHIFT_TYPE = "1" *) (* C_SINIT_VAL = "00000000" *) (* C_SYNC_ENABLE = "0" *) 
(* C_SYNC_PRIORITY = "1" *) (* C_VERBOSITY = "0" *) (* C_WIDTH = "8" *) 
(* C_XDEVICEFAMILY = "virtex7" *) (* ORIG_REF_NAME = "c_shift_ram_v12_0_13" *) (* downgradeipidentifiedwarnings = "yes" *) 
module data_delay20_c_shift_ram_v12_0_13
   (A,
    D,
    CLK,
    CE,
    SCLR,
    SSET,
    SINIT,
    Q);
  input [5:0]A;
  input [7:0]D;
  input CLK;
  input CE;
  input SCLR;
  input SSET;
  input SINIT;
  output [7:0]Q;

  wire [5:0]A;
  wire CLK;
  wire [7:0]D;
  wire [7:0]Q;

  (* c_addr_width = "6" *) 
  (* c_ainit_val = "00000000" *) 
  (* c_default_data = "00000000" *) 
  (* c_depth = "36" *) 
  (* c_elaboration_dir = "./" *) 
  (* c_has_a = "1" *) 
  (* c_has_ce = "0" *) 
  (* c_has_sclr = "0" *) 
  (* c_has_sinit = "0" *) 
  (* c_has_sset = "0" *) 
  (* c_mem_init_file = "no_coe_file_loaded" *) 
  (* c_opt_goal = "0" *) 
  (* c_parser_type = "0" *) 
  (* c_read_mif = "0" *) 
  (* c_reg_last_bit = "0" *) 
  (* c_shift_type = "1" *) 
  (* c_sinit_val = "00000000" *) 
  (* c_sync_enable = "0" *) 
  (* c_sync_priority = "1" *) 
  (* c_verbosity = "0" *) 
  (* c_width = "8" *) 
  (* c_xdevicefamily = "virtex7" *) 
  (* downgradeipidentifiedwarnings = "yes" *) 
  data_delay20_c_shift_ram_v12_0_13_viv i_synth
       (.A(A),
        .CE(1'b0),
        .CLK(CLK),
        .D(D),
        .Q(Q),
        .SCLR(1'b0),
        .SINIT(1'b0),
        .SSET(1'b0));
endmodule
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "XILINX"
`pragma protect encrypt_agent_info = "Xilinx Encryption Tool 2019.1"
`pragma protect key_keyowner="Cadence Design Systems.", key_keyname="cds_rsa_key", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=64)
`pragma protect key_block
nn2y8yvGEAU+oxfqIjNJMT0yc3TadO2A5cZHoK3dC6l8eNK9HFYMskFicvVj1pqkc9mDJyUKrSId
CCL/HetNvg==

`pragma protect key_keyowner="Synopsys", key_keyname="SNPS-VCS-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
tNkKvQED4vWNv+vYe3nI8o8VVd25+7OXvcQGN/bznOgwwQ9zOLtv8RpNciTxGp//tcY2jCAsQOQR
SAwBc00y7wnNCTMtZFvxXOqSHEUPYTYxcXYrIMSElXg1AeD03zwd+qdTjbS69InbpBZ40pWQBX71
ctP6YeotRwK8SefTdE8=

`pragma protect key_keyowner="Aldec", key_keyname="ALDEC15_001", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
aw7jhd7UZTrADfLTZ5YsRTYTdEzrJ+N4enHI0pDja6no68W2U8xeZSGR45xVkKPWy4UGH3zDqIyg
YAP5FGIc5LcSqiGDfNdrdIvThTT5xBjpM9zvSxq6836B2yVmJhQLgvziF962Xaqbe7uQRnXQp17I
kjzsA6zhURm/05lRx/day4Cz2CbJD4D4RVpd36+ytOucw4q+sxb/dm0j/Zj3I3aD0G+lVv+6bECF
ISSRuJv19uehdbvwf+qrzqTMPcPX7L9lxubWjJXjm+4496NlRuEv4eng6rowNLkC1RQXoUtW+UVo
PyhgdGfb+nJPLDiP90hHMCg0AlZBGnoQ4bZYlw==

`pragma protect key_keyowner="ATRENTA", key_keyname="ATR-SG-2015-RSA-3", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
biYqlehfUy2MPR5IeETLexHTdJ7q51nWkAsSzWCNbkr2c3/I9AYuwIkh2pcsUMgsCkL2Lcb26gJW
D9+BY0gYxVOzcZx9DtuxM/DqEvzsBG5XW/R6eymYeJJXtW18hS5aOcO2RDf30eieTCZwOyx3cnis
herNQHaIc5PrMK7dsl2V0mRQ4Lex1+r46DNrReU4z+3VNRVfL0mX4NhB+QV2zDspLTAYf1l7nA5u
EbLi12YBXczbhvawRV6TjHxA4Ml/QzRmYMg1mfxJ+3FZmfoR+uU07NrBc1LOhonCg2DPch0TTDa/
+umVNvlER66pUotjBuU9vgjw7Dkkp+/MdqAajg==

`pragma protect key_keyowner="Xilinx", key_keyname="xilinxt_2019_02", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
VR2RqLmiSvIq90JPqVnSqgmKE5SicXJKPyTAunTpAaj1DuEXlVsRD04gzJadbWfPqE9xAoUiq9wU
PHjRKiQbrR4JC+wy8bkLpY5iW3oxOSRDZ65wXxQUvMxgGE2eFRVI/k/ZQRRYqr9mm8YdVU01dvtU
egqfPLie7vxuQB/aiN7a0E7FJ3PMFpnNM/lmZU8kUoAycwEcAu9Lvc3isAys48OIVQPaQHkMn2eB
HIwPBmpuermPsgmSnBIbbJxQ+dBWkPL0EzxPuDn7PUp+ojaG9ZnKwIfZL1me0oBJdzhp2dyAcfIo
QfkynG/j81yZHbXXctAPpAfTUjbbnye2spH36A==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VELOCE-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=128)
`pragma protect key_block
spURjF4NoZ4yjaDCJd2V8NW+Oj5ZMaUYbemUjajnsue2uyV5q0a5IBxkOcXgNdlJ2vnd44QwYWa4
lylHJkTIRWpMiAc6oKbTc0UpP1dy19psYVCAqRk/1+Ql6FdGPO88bH+fSSVGZp3FT1qBOx/xZdlI
oCEDIIhFzto+GF3j648=

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-VERIF-SIM-RSA-2", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
eMFwa46SXA6HVoFlviJtJvyKfnw7oJgLT1JrVR0eN4c/X8/spoO5r8yshZA3q3KquxGdP0kyqeP9
P1R/bA4WmHDfXN2hXErIJk8CzVEKi7D17DVfKTkvxwdjjBmDO5EYn24/k+RSr/VKfX4X0GFHpb5X
2fgSZVIPaY/ztx/tIF1U+TG7NvoQlBKjqBJyQNgDmciPzSPkSPzhpcEFL95PVLp2IwwcbtuM5PnK
SYBqbLz3Y6FcLScZwLiOUD+aFacR+bbjaJnOQ+Aap6Jy6mwXic0Vpf3KziYI6760f5HtSIaKYnEf
2emYl3n7jq3Nqev5AAxpFg7eUMHAnHMWb375Iw==

`pragma protect key_keyowner="Mentor Graphics Corporation", key_keyname="MGC-PREC-RSA", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
Cqi9GwoTKlkaEBJEWIzj9akdpbrv62ypGOOvudesgsVlneB/jEk6KGEao2ikegRYDpZTufAwirO3
Sj+DK++M3ba01HJyms/5WJ91Tfu2ERm+WS1IX21BshYF274NWlu30oEm+Ua500HBe48ubmEBAsXx
uhmsUKVFRaq6vAQ2LeQlTieqyIZu6RrcWwcAs/azEXeVuF977vOjMyeC3bnIsIesors6c8Xlc2KH
GfyZTeU6ITE1GYELFxJH4MuegWQMQKhgGHFGjNTqmBJS/+zr3NefFSCQe1/c/ev9d75UdoU4hgar
uSG1ikCbDLLreQP4Niwm3OyRTmCkTCZe+NQcJA==

`pragma protect key_keyowner="Synplicity", key_keyname="SYNP15_1", key_method="rsa"
`pragma protect encoding = (enctype="BASE64", line_length=76, bytes=256)
`pragma protect key_block
XlgM8EM42ZshzSJ45ZltzyMzpGBRxgKBvZqtpAHhe0LNiLZhuht0HqgneXLDomn0ek/lcEqDVCCH
VUNjmQLKyKPX/m28sEXpts6dwGcX1fmd8vr/vzW0DoYKD1Ei+ixb9brOesoZzXn6RAX3FLe6pghb
JGacRcItPhdtCY4CWoRKq7umPBBH7EZRuZN/ChFfdOlv/qaQ4oI1uN4+T77GT5IipBsORmdRre/a
VQgaPM+grtcVlj+1j+efJC1I8roIABNmkJaT4+vewyB4lyMRpNE7aJgvRmGePYuGT5GOf3iAQo/n
f2+vvpWfzEUfIfxJtweAKib4HUkqXPJlNswxVQ==

`pragma protect data_method = "AES128-CBC"
`pragma protect encoding = (enctype = "BASE64", line_length = 76, bytes = 16320)
`pragma protect data_block
ptiD0jtct8SaIZkWP3M4+2YuaeUG9WMLWdUcuehQDBQmioPIfYQUHGabTxGu2cxxgX5W9TI9OVHH
aFPf7R6svkoqpFFNmSov7oKwv4npbcJ7QTwDf2tNF6GdAU2lmnIXVaNDOgzVD7XnV0iP+4WuFRCg
8gMU50udb49D25mq7Gne27WvzuRKZpELgiwy+Pmw9+E245a7zDV+77JA1zKf3kq40mYRDeuiYnyw
c7PRKIcC87jbuLqSBGuS020q+JsSI5gLO+stjW+lwHF4vI1YUZjSqJ8L+erIJTuvs3RoXe7PnKZE
loha8/jmbFEkA6Aw9aQ+18bGEZ+8OQ5g6lcyW09XW9TJU6h6VuzCg8QIP+pADrR9cSY4tTGxtPyy
JcfX9HnT//7J9opMx+H4+bX9FaraUlgOn2bs4qxl9JI9hS8A0CpKAUey+mCDe4y9kSoo1UbP2IF+
FE+VIj+NecKO99SQ2F7vP+PikkPjaSR8uFPB7B9pq2iC3Qn/CJ8B5/d30kSnm6lgXveEHTVE9qzF
H82f0bDgkMCIQPbvMLFPNwS4KA+rcRMmFSf5cxQgBs54AOHGTD3eRO8RLcqcvUe+vbDbU4HYDHVW
IQvzf7j2AwE5P1JH2tNfZDApRbvNTJwEbutdXUEQlpGcCfMQGAYCbXCuAs++AF4SCMpp4JXEZifm
ix2IFbp4aHlLmsjslUUd9VLcfxEHpzfIELxqIpTI1swCMyI7sNxnb8QRELHI7LrE39gAdB2v0YUj
FP9LapomkLGqsUBInHjInIr+qMkFuQNlbam8UEKkmUtFmF7ahmqprMfPcfLDh8B6izuulzyc/q0o
t5Ou7MNd4AL9OyOcpIn6D/3UoYQQtkbGeEhFwVwUJ1K6zM1DzH1omHl2TNFfN7LswJ8G5o+rzIxt
cKlra8LrdHNofLF2RfU3NfBmAWecrIF/+sAt6gmlc3bjhHqLtmc1NUyqDv6OwfijMIFFoUnBO2Q8
xYqCAU/N+L0j1CXEiRqaRdUWWCybWkxVC3QSoUy82oomGdtaAoCrco/RFUb4raMN7VBmWIv0c5OE
E6dJHHLidAuujl8xAs/MVGzL7XgEqD4WIWxFDAkcOB9NP/hJtz1vv4NYshZMA3dZNVcqkwaJfChm
Gynyrh/jFdUsq2EnOFEDkayLXFvzJz+iW585n7ka4bzSd6UCGXScWq17B3FRlu74N1OpRjN0tFP3
AC0pMZMaqxmvYtDMJY/R63eME1Zpj1ZwpBlfDKLZLTEXTjwBLsREQuX7V3c5jk+NDDjQG/LJkKE8
5lWvdLuWuHWnRM4kXZwMjhFpG1HKbbGZRZVIHbM6TkKuWFdRtoPqFqRFtZI9wJqkZtldBVNKpt16
lrtQzedf3E2s3o3i0dLdEls6dxxBxpXbwnf+jAfox3HPaH0y/o9PzAveuB5K2fORJ+R/usDXm37O
AVk6Vf3LWc7Lvj9DDk/fZ9Bq0Z/gFMItwAkn1WbhDDewZ5C/orOB6LWQs+vQZihBw1i2XJ+Hr8m9
0ds0lbEMLQVXpKH6uFDRS4Yo31bl86glY2wzglAlWIIKn4nxX/mYcoY6DskI5NBE4E7CniXzgt4c
e+Xr9XDab/+fN39AYCADRbn6wxhaRckUIzItKqt4laenEVXBnr/CcaMWBTfTEPSsMwJKJYdNpgMs
RlmnKhM0AZ9NFvygUvBuSlo9xE9evyDCYvUvVlSXIwALvadK/DLXO0OEqgl8aJIeG5/rXFrgrNYd
JFbiP6eH0IH8/mfJddJBWH+4ar3sykG8RhI0IjmKOobtsSfHo8o5vrMLorIZ7cbGoSWqPaet9DoJ
kNTK2A21Y6iFBRExRcQzGqtjdYashjNf4raFbG5iIk53nq14yy2sQQn/64I6KSg+n8hB48UehCRd
zfLJEFk5QbU5DgoetRUsEouh10aZQZJv+Z6KOVns7nNZdYN1hDg1CUa4qw0OHZdkwRU0ScxCscFn
TPTbouCfK8r4tEDoq0/gGIfjgcveuMDV6+os7lOtYkj4kqmnUu1SN1WEYwKuUsxH2GTF0JNVDMYM
gtevT9QF5cztUdlRf8OdBv0ANefwj3ruu0IfwMdRRXNK3F2ODEq4fO89ql5g71yDVYYEPueiFlTQ
I8tjL2ns058i1Vdmjt6ydm6yQdvw9uaGiM6bSJ+wx4TGKHpzYgTGzer75JAd975GiNlmiMfBUMsC
BwoGHmfANYepIVdwo4RcW/74kpzVhIwgYqTR3MXbPB+seZKwziXJYXSEofhqi+pFthzSsiHtswCx
S65QvKti9Rh6msIt1JICQySH8alyimVPR3RlrByStFijLExRwVZEYxycrTvIVF5Ccp6YXJeOW5xU
LTKeAvdgw5ZXj2l0huMFjTy1etmTsPDWD3DxzG5nyQDMmw9Pc5doLejzUZvuWZHwKQo3pB6Wo3cn
Kt/mvJZ0URZw51doBAOUEaGAIXEBKb82k96h2V2A7yAoNLvIGQhrSFW7GTRTcB9/iXulf9SnO4n0
DArzHKTDIVxojakjHJUsY+2QPYe+HZEfagjSkfCCR2UxA9DprVTfJTJiQXr93fmH82AYv2PW82KZ
XKSc/veo+pXUJIocI3mV7Un2Uijw1II5wTycqN3XDQefEE8DzMegBpzKWM6K+l1AXOuFjd/MrwpH
D2ll+CwTagoFuh0328UBs1O91y93QrlfBsaiZDynFWKN0FunncfXtXlXN6z+9TdD/NHpmi5DQduZ
6pnj21avHp0DHqt6gb7sOoyMQqCWEv3N8Nw9BnxWart2jqW9fm01V+MQ6HZUaonq5SLydQp7wiCY
HkSnpBOom2yfIXqfD00W5kC1/xqm8SLXcCPy8NyR0FUD7vORQCcND1ZysPoI4rANzURgjxIhD9V0
FEXeIyDMMK8QwrWm9jXC7aervPFC0eQZWrASbIpk4cbpYGJJpsJ0fCV9paW0THWRCH4Pz4uU5fhN
y1710/8Mwd+fGCpFoCh8a05P9883xVncqIThzlzSKTz9fX/oMwCmVOezyLRWNsjIyXEji3ubO2HK
x9d31D/GshONlS5YKcoCwYMFxz2mC+b9UJBPbAMH7NEfacfEOvUlOSXeOIxpQz99sFbJV9ArD7rx
dfxCeHCA/vIrcWU62DtvT7GeatvLHgAztb86kcy3BlqLTlcv9dsaaNihEiO4ehkvP7pVtzk4zrkU
7JZbGNsHnId2ielupUdu7NAi47sjF8U5IZx8rD9O+j2LZZdGWW3uqx2YzxjTGGvCLaFJZglmRAy6
mVB5qD16192bf7sMsG/WM12nrWZWuyHw27smKSSMzujbE809HJBoEPR4VxoNgOW8pdy4yshan3CX
2eGK6royqoMQXqTBBGkE6DyOlmK4xwmaDivEIPFw4BxMbYdAxAYph2ncw/z2uZepL7pdSoJhqgtn
+4VPiDRQzKG+wjr2+Qfiqa2GGdxUO4xvspjK2BxkOxTWH9+kVdqZl37K9JPyums1f1bz5qdYKNSZ
vQHKn8fTNaCAMTMf6b5HlaaM6sYoKU+q8zjaJWLC/sKjCfKPJoUX2pHujkGcZk0D0udkk1bTgSmd
lxrg8qAe68B+pvzwf13dZrA9/ylONCBmpqUNqIZwa/G3gPy6oVbzVZ4qNG+ikHgpsvk+YwQSp+Tw
suap0VftKFF/SYN4Op0wboVKJeH85A5vrLoGjZiUhA4TEy5auT7MroBNVzWsea64tFDzxNIyuuOx
u/hfqUsuzx0O/0rDxLSy6eKvC2gBpS5M8dozx8wTRqobqyuHQnRmpI328GrQeIM++zkCpwFRlCj7
U8okS/4otqdSG4TKCgwvJzfG1Fgo0xIcK0fhQeORtx7UmYSD6rpaW+fjfvAZRx6ZAItTNGdwE8ID
DTLl07UryzZdjgnQmQAo7dxot7L2boAPthJajD4m2hp0MAD/SML25fusuioB894a5EE4ucTzwEiL
jYvBtvi9+AaO96w0+cd6oEXhvve0kB01YxK6K9B4pRmufaZw1WH0TnyvjPEQbGjkXbFwvjfv7Zga
W+3iIaM7NCLh/vorZ7h6zTp3RtG6NeaFe7eu3abAn73IdpjKpJSUnY/A4vUEF3B8yZN8iq6+uaOT
PV5fOvT5BCWBDnO7RMbetecHYBccdi5JYbiiHRsYc6C2iUedpIrWp5iv5bXQRGbajk6PVs72Rl2m
cjw9d+qMvnW30rIcn6VM5B58WERt8/Ax23thIv8UAcqOCCrAUlOB8tgEksYVH9FgFaUdHyq9ghfh
eOHiEQl4e8UPHQuHCwiUXHVdpMd+d85jToYxUMkBuNHgxBv9z1DX6aacQabasOKzeYCi+kaMMwZy
uyyk16LEjaN41YoL+jtAy6N9lqcuQxAZNrS0FQ7NswuD5v+qrBbkt/iiMRjtVCaSfiDCJ280LIWF
CrSE9iG3xAcPuoDTbA9A4eaBgSTMW1xSi44SdgIenvspDYJc9HOXYdISxYcYMbDLqr9J+zPtznkK
FPHMvD+dj3NNO5+Z6ZVRvaE5G6VcXmDnAbHUiIDBO1+QrUI9mggra/L0l5X6gh9jLjdgywU2yaEc
em4Yst9A61iHi2wvbvHge/qu3GIymI/s1BlQ11OtQS7F1ivegfr8RrAMfQmRlVUusbtxs4UvWvO5
2QM1Ba/hR0UjcUu6ZTlvHHwTcu1hhrcslrdIf7r0Yiav6dyE2yZMNGmG7NnFFeVXWDNrgGeWQ9tV
uwpJQoEPGfq0rcck/2Yj7RfGnnq2PAvoC6Fw+AW5J7+I1vMVIkl4kUw5XygHoyq012i3q7XbtqQQ
+gqlKMQOP2TMgcR39cJ+pFiFIOlM2OUelilm3amxQrIj8Ui2gV88flZ0Ya7uRPNVxqJAe3T/NESI
lOwL9hQm9aSD6TNniPvJ3NhUyyXL6OFMmfgzFrey/rOoaxC3Qx3lvMVD0GG8EVGtUHgVbSQvMZeL
ZbJJCTRYNzlp8iGhQlZ/ZtrbmUu5mSp0XjcH+E7cq2mPUUUPI+Zv18f1zrFtxB9WHi/OedNag2zp
V9heTpnV9QOouL1CNGEKBtUzqN7+Qc42gldLbd10EsARU3B/N3LvaDwsrD1jwDW0XWiP+gKTVBGh
qjVSt6a2pnhSCIoVP1WYi5Cm1vmHK6d+xyr1oBpVwr/MMXXv/hm3IG6raE9syiek8xGjmhuimmT3
njmO0itSkIgkldjDlvfG9Cyc3Amsrd1whAeG4qJxv0tAU6oajcVagJMfDzGMJ8xqjcRo1Gd3RJIZ
b0i/DJtJv5RuQH4/sysomhW/2q7vrhZhsvhgb39Q+FINyv6wgTCAkErh7tp9PbSl0965Yz01gqnU
5SPNdQKG3kiv8JrJ9auXS3ZeqPmbSB7BjHnY6pGtUScCNAfLr//kD23ITQEQ9QA1hSO57DWyfKS7
ZLLtN66/ke26s76aS/ZttMhmv5vpWMeRzlG6/rkL0oNqk2YsfkEOcOpda0WrEdzD7sFq+Kjg/f4J
4f4PWoNPfLTf9EOJwdApLWTd3C5FK2qdQdNHpEijnmKnO1pRiWKg2pkLWQCrTKPbrSbKxL+bgbT1
gOeQGw/I+oxPgHU9PXcBW3vTW/yQk8EaPBpsaRVDikrBmWJXEkb55VmkV+TWgpUjEA8WiVo0cpwA
NPgnb3pEENiNqIOBup7Bg+2fkoat17EN6XpjYdafgTlmDesrpfEFlwjreyMnBGl1PjsKp9Q624Nz
Bo5kFu7pII9cmOV/rHHy59MrHz1LzA1UmHPxekAJ+jx3eQDpEFCDaIiGl/7OQMLPhPLsRY6lfesS
Ww8TB7ZWWpnXsp56kIoWmOiOZFaKQ7jkTZJRLOp8GkiRnTXK1pvtEcmnPV4aRV1FovEcr0N2Kkk0
LodW4IR7zNKpJ2DHo2Bqvl9+JrxyCDqbf77NlzCRaOzMBmO5WnXqnUn2GDgRUur8OOyd/4YCQJuZ
XtCPTCvVV+OAxuihmc/devzFKAq/3sC+9vMWAt8m4jhscWfOWWvEMpv2JBcJ93NcY9ewxFNJgfbG
A8HaQ1plWRCtA77JpM3WpBkknEEDn5YjHqoU6pdi9vrrwdvdR7PjNFe+I3ai12TL4NtB/E0JI3hJ
oL9pdAPpH0vsRTKbeldigmsCwmfWDlrBnbGKC1lV1JhDGgYLye78EYyN6O7AVAmroGbQj0kXBT4V
fu4tcx80lkytrMZzKXG+Su9zHmrB4aTH0YAZqx4Km2g3Nk+JT4wsqXG7lvpQ9hbKs/jQ8ojU3rXH
pmA1RNOHvb9rSMy4pEQZQm5bYkm/10N9nEaupYjl1P3nbhxrK7iX3/oPJCkyG3Qdf4PiqHPJ4vdy
y9EskxLD2AcoFrcxA/jmJ/Y6rhk6eZpRbqA99v7iMiuPMnNL8YXxMI/F8K8NBVRwXWFYMkxH6V3+
k0ynrBdGQ/9krbp2MJ3aXANWVgKVayfR7A7R5oVnMOzBEVj9PwpEb8qec4ceXeX6tuM9krRFX50y
nsHehMvfLlev2yqkTvIbbviRgj/Wil40GEKgwHZgk1gfNgLtivpBw94Lm+/KprHi9LwBS/ALOuUV
Ge4yJbweyHsjCaBDcVNBuSEK+NG9Yjy4tNaJWVcQtcFoksvs7Yj/MdVIJSBxUjITHl7q3pPzxAaq
sHlU2dNo0UxzVntwhceFHfjM5gHyDJ00n2Hh0H4SuLgqECpMPSZh2l4mFiBUnh2+9/8fAQYTMlnf
Ou0zr4XcLBUhO2ZiZIB+S5fTbRIuK/h2UA9s6hc52SIGqre7co1tmbc3XtMT5V/1l2mVQjTfdNEZ
7ltrIwqWlTWj/xaYEVvtg4xkufwS+gHGg3UnLAMiCHFc/9/x/UtPzi4M0a6lKKtiq1a8l4b5sJR1
hStgU5AFJoBCrEWht1EZ+dMoX/BldI4afkEIubY0og66vFOPuerqVt9xrjiK8OzpvTApKddZs2T+
Ejef4DZFvt6ndc6uZirUqjsVjxbqQsoFmPVbwFuY4pf8wh9Vv8/cQ8fweom2nLDb7aRRHsw5uajm
8WeUIaVzUMNcHfwjT1EEme8VfxiirluCGE1DV8YI9BDtfPzX8AZLzpJsZaxFVWk2XTjiNvDuX/Sh
Acb+9G3AqOvjUALYfON5UKrbbOf1NcHHIKjY3PPtMqyxZeWueHDhluxEDOk75SzRnHL27A5nprz1
f0ukk+e1XdXJ/Ebw3o9loNHA0hU0CcESENvifS099VNVVaOnsSCRQVYLIfKKEk9r4PhSCvqha3Bf
LVtYHdP9dPfIb6/oDoxn+eMXO9mVxhtf3BHCBq1VSLn+fXVFXmrIA5tytTYK1c79/6vgmZeCehzK
ClLDc5itNuiPmUGT/PZIQokgtQ6s+o6GQxAhQ8Pxa5nPJWX9xQcu3G4v/PzW0c50Fr8TrwLuP6cB
67Pxdy+2F0lBdO3+iAvFdlvOTZhTWOl6gfZMaP3D0gBwa2xQpNe+Xgb9mp3P9fjxsd0O7Uu/Qazg
bMMPUbtijeVKc4jlap8I9Bvmb8OQo3UfQR+/bCriNXWx6SWnYugk7wBghqBimp3R301NNEBlOunR
l7TjVC4OdivhySRPlPXifeV/kvXELMpoR1fNw+xIzWMcaHGnQVRMTdZLvscOfdm+ggb/GRlBhhBp
gC7OtlOQQ/OjyVXXOpcEN/NBNgwSn0hRTiCAactTqBoDYlajso3naV09TdYg92W/yxRZq1vHPNhI
fAcjUPG3emMACCwDzQzfpKuXhlJr112r690Goaq0WZcNRUu0+KBm0Rk7T22Se/kSfMH0UtOuJAc7
HJ88/mWx64LTj0jYq1vsgACsL66h8QGRD5I+yBVVTzqFJi3VKqRZzZGHdgMU74xzimBmcFEWGa0Z
+D1KeQSWzwEMO6nmIc+gSxIjfsV0IHta9cDlX4PtD+rJPWpKbJsJykT71e/vybgnTv9jWGWGv/ZX
UPEM07xO3I1Xp5fVeUdhy955A5Ei0bMM+k627zmgQ0OTex/F8TCIwgDxqgmFfnCwH6gsRqE+ntdR
Jla4rOyCnKE1IcM8wpSQnCS266rInZ2U7j2LxAGBmN2Krmf/AddwxKVuOm8HFgSrYtoTOwbJp36l
3bLSgz8MUUGnZX4z4qAulnETEyQz5ANau8mBlWirhjqR2zXjiJQNa17wbZMTAb5wgkTumce8ev4Z
EMML0BAeXEEgVNZYSZM1UnBsU5FgRGglCY6yvfTgq64Yh1HuA2/ncMjEXVhv6RQkElaccNMa7gox
KlkwMC+gM+V+l/ItJGXyqA4LWhcuUXl0O12NEARwDmAaHkm9KtLnIAipsSxXdha7pm23uxA4LAPu
fwjO5bgiz8lGsTgC4/UautLial3uuQjNUS93vaHUYcZD8s/ylKRaouWPUe7kOmuQH27ahtxGNcaV
ErHzlwpz9iqyWKjNHA3BGyI8VQtjVFzFd3h3G8PlzSXbZpXrv3JciEU31b4kNNBxFtw3buPNr/fG
kU5Dup+V9+t3KZLljJLFbomD/lqYkZxkQDvZGY5cYNBwl25IdhjbtJSaMpH/KO+Ffn/NgA5iaSzG
AsFoSLzx+ynDNo33NDvyfuJuMqPQfAq+Mo4IqbNUp8D95hi2MCpqMNaOdhbcv42hCnglBEu+XuvU
RLp27ImpLY8cFmOKF2rKlyhkdBV8lk6dRuLy6YWariBOQCloGSyPlhl3YxfsXdExTBTLzr/GqDcs
fzlwW1mwTlEBbw9M6CokTJjC2RNseo7Jjme//WhIA07rV1yNQbXORz/2OBQrS3ufTOPdfm2Cj51R
Rcr5M4Qke3fBjvhUClqIYuASbxFc7E7hG+8mF7bE/9nkC9mv5HpxDSYrTuIVI+JTFkJjjwbY1eT4
/AIh9sIXU3BAMh6KGngwwpyN21t/msWoElp2HhLvqI3JWwMoseKsx+uOXSS1ylCz6LWwcH9nS7Zb
uzgYrSM4jnzIrs6EDVsFL0/QZyjjD3I4tstan9TV/bvJT5g0/v7gmdMWcDgV+dAtpNirVc5BwGoW
CkrwR+1Vlf6/PDJlj9fXOk8ZPePCI3VzACJ/Zo8koqWVbvUiyjylUE5UEw7cVpzXnLNS78vkVpxG
laM3tLgnXwLCdo/bBFhx5KEkeiuHD1fOA0HGYDktB4xcY1rlvS8tg+xT4cyqf5HnG/npoDg6U0kS
FcPNIQcn7bEnraPAAcJHiHtdivnkFkKqxyvY2+Zg36CvTFZlRE+t/xogqGooaEvaHd8lrkMBmV/X
gxe4Ur4lzlxAEQUI/F6jhuN8j6HPigIBy7YJEyLcomB9zGskEyCZABI/QXBiuz8xPAHCFLw3IMVh
Yp6GMyy6xTftLnf5XyEnxgDA5oGWgGSInid+uBAI5pfOn/TZTJCgTfmeQZWGLdvtpq0oEiCykWpb
2P9QuHk84nEIB9OqUONRnrH9fjjAGW1Vk4yvbif+q+gW2SGgP6c2pHHD7kXYMhGSe+2dlLS3wPpE
J3vOyTs7JZDWSLxyH0LmmMt9UetjloBHAwc/7uthSfxVBY9Yrl2NBBMBc5V/LQfOeEfhvF9tgitL
OaQetImEXo7bEZxQObEeyAX9yus5ghy/CTnCf3eHajN+OXga2iSLjRaYa6AicuxxxvzuggQZ3Fhn
QuzXJounH4AR0Dk19gCY29ypxVBEa9gH9HsGA3xuD5VJkGj4sTAmLJohqRKvpBINkoovQWnK+eqZ
11wZpvlFGtMj0qoEn4h7S6Fbdwghfkq84mqfcN67gY5oDX1exbOaWENQP0pPQEQNK4hhwHkG+VuS
CUCc2VbIps1BI4zHkvyni19yJwye1oSQ0dLBRoRGLgZnyfmYdglr9sqcFudRrydRCj0YdjPmPRBP
BHjmvFggOt1t7fLQdcnIAjiwag99vRGv7Raz4bFhg+Gbrp1Rfk2G5VymGZ8+TObmPqyltmbERC2l
XAgGe8gaeZWdlGCbBl+4r1F++cT1NCZlzVbYkQCdHTw6zOXwvJxo29OCLlBiwst70sC6DcBLlTCA
BLVYSIQ/6ZfWaCMA6dt61cCyPlNYPGB0m9n1K0jQTNR5SbR4z6UuPYz+jG2ghpqkptCcce55Olyf
cxeD3GAvFYaU6hPBRzkVfNgMMK/nD+B6x3u8jM0n5mJpGVdEAEnC/xTlzxP1iHH8JU4Rhn3ox3A2
VuISmmr0OLauSIJTUiSlI4RZXwJBwSwN4QLyridlGjq2u35K7ACDwSMCnv/mlozH4dw4d4SIZfxB
7HUxOL8EEvBTvw36To+YRzjkXJ24mpHx4C0mhwqfq57La4UfNF+tokEXhLc4zAMPAu1ag8Rwl349
49OpzfFe1BLhLzLOpgLqgClqkVWQ/ooK6HiRvZLAkkzJnCYut6TbJ7VwnQsdB6+WkSswKuHlCP+R
UKzAbdBqAKI88GFLfZHx9Bks8rP/tBcv/WRf1G84wadPXHj0WTqDvOZrTHCece8bGAYBTgFRGt9j
viXX3wuCOnxvYB9T0XGupUB2hJfiHtvuNbg0xdjdmAwCH5IfCvvjFHyhtK1SMiTvHXL6w65drKWM
UJa8YzXNZhCMXBoe4qn/z8qbagstGW4vwDtoeDt8SOq2eeOV/qkpL+nxvmxoJlDoROsC5GYAeRa6
Njs/fstqQ6itBS73pu3IR7ovtw0q7+NwQGAi5+jIVPJgm40attzZtjZF1I1g3HV8lhCDbKwZ4cn0
3apGUI0quAWMxdV/jGus98PDB6mdpnmwcF/KVpd2Qxk4UjIA0wru6uAmvjOQgWheP6MXW+yXfGbq
N7ngdjjRqlsm+nGXNARbd91d25ATC0Z5ksnlvBZje1bCAMjpSqKWPXMDjvNK4FSDHj/FG1QzkdMk
3NySq+pWKsh99W8yJGjyTXYWTSzTZM23sR74p/loXY0dkN4do13JE5ir+VxkVLj9qP0vzRZ7keM4
21jbA5fB1XSuzuHozEZ+dofKCCkPA/Pq0mOhb7hPskwv8gEpXMpHyvkhVZ3dcQiOathQHU5z4P5M
wrEELQG7q0ZmzzQ3UcKXW8gZ0tEHSQBqYRIHUzLqrFUJ08gtggDRWSFux3EjHG3ZjlRAQC1Ppd5a
WffnC2vtgRQpxFQ9zyEaR/wgFxk2GOW/HTxtwwyG8laim/20eFP8T1F7hpSfRWc/m84D1VhQeYQG
OojlEUVs+tpqECFD6cLm3QTmc4Z1CQE4Hw7um/s+qMbzkeuWfe+rp60hZRST2S1EXeK9i2RaQyfD
Opd4J6bqkoLOKHdnua9b6M46xL8d3aTmzgSZiy7z/s3jGQJiSSwDNjuPLdkTDuxqXcVvBs/re3Fz
uK4oXQ7FH4aHPix1nYjkaWKXcmepKVJSvuVUkDIJABnQOTb/M/ZOUhRl17RjsT8Q4CzQ9l00UqGR
QIdmCxBvEv7Fsafu2G3qjEcZOsYOgypRlQXSyaeJ+0LKX2mfKfmMjctVmVqFTQ6J/tWHlXk21/sk
Qj9C1iIJzE5KQT1I9n6b4g0e25hzTy9LPsIXTO1XyN/p0KGHP1h1H90lqaq2bN7klHTwxUrCqRDl
lTZRyzJbhTXxj+pBsCVVOJ+LK7IdIjmNwpVs81qRPCMzeTzMHdlPcm3TdNZcIDxFqCVvxeaHGAHw
3RN/hY8EnL3MKH7OqpXYTYIIpq45iRZw5Oze405cQhABZcPeDNKlWbzwDhlW7fXOBufuhjolIuhd
Nw8PdUNP8PhFze6DgGp4DK0x4mEMoDLADExI4Jy5XdnuXaL6GTlSyuBbQVIph3nUAQFTsvtmS7Wn
KQNAb+VnbhNi9fmQROs9KgvSMR456t00bWnPziabGEeJu9XjJE+RUKRIZ2NQI01XWsLWutJg5eg+
0m1h2lDycGEHImMq3WuZADTlA+bmup336WU0CSR0JkXKLEYf0uSk0gewteRw5iwPpz5IUnowKiCK
SlRTR/5mNGmRvQUi5NjpvjmAKL5jx3GWMt9bE6VY9drJSU/04nAbN1Kt6hH0fb+zdxyHcq1XWjH6
6tOxt1zIj1u8dc0Pa39WppV2G5xUqVUXjulgLM5eDrw7ffStxMBtPkKlodxPAtcyvTaMOzvyPV1I
t23hem8BwqY26eXadO37fAJVinf1tlIfCMfqhG5qFvSf3SeyVvP5bJDyMTTIkEIQaoo72A2jRVN9
hMgJw0hr6S2TWT0WEfDgp5Q2JHSwBw2LBlqOf3bAXHcpljiOt2yHm3Vd1/FiNjL9V+BGvsLiSveN
J1GVOk//db9F8Qcp/LCbwipLe2O7LeVMgkAHq2qCWw5RucdeDRP9K2/EggCZc5kMMCOv/QI/BCsB
vQ7HcQ1C4HSbVP3nl3wMucJ1iu3iz/celhx/4Q3KvlZ76OlUSKioP6ccwnWiigEP1mB/PeKtmpdz
7sYhatTXBiJ8nGEh0RKrk5R17P+wEtJ3OZdaItQRzBoTgcUN2n6QNPehLLEuf3M3wFEMqV34jKul
7zMcaWanfESaEUiy9ee9w/tqebxxDsOGZ5Xd8Dl9ZBfRcVasETNP0C50zq8jag16DPfjyXqNklaJ
VcdwGFaQKnDY1lFMXxkVyAVya2FT9VAgWl3dVc/4Gi2XSg/rui/G09xMfC+Qzte9t9oGlKlVvn6s
sLf/dBgDRmQFM2l+jJfTP9XpyUm5n/MhvguvuJCdjANR8hi9U742o+PQVzJRiOCFsNb0W3JKeX/v
B9iUvpPc2DshI+dOmDmFW5dk9J5xzBuC4Eku6AzYpJPtT0rDfk98DO7ucmQVo38HN0NywMh6zll+
CVNgCRUlXXx1ASqb8TKMum10iyrvyfWZKYFtE0+DOtpLhuyiZA6lJ/AGbIus6CarWRbvrvsCEh6c
TNCpF5BnDZRrc7/2sEhHg6yIVd8GTaBZsZXs/DGqhqBV0IfJWIUW6hHv2xKB4Tf4/Zqvn36txq1i
QjYzYhq2XWpB3BXhWEnJmAffSfuMRTYPCJdqHV8bQuRMSWmXqp0GeLZP8wKslADgbDk3BMSkxDkN
ub65W8ltbpMqXR2bcOvvHOhRGUMbytp2fb0VxfA0jLaJlkmudE29+3qSRIMiI5r5u8riTota1pTH
E1AAFB9yfY3UQAdzku6FIqjW7W0cbJCBwPZNjXix5NnwOWbQ4ToY5B7Hd1uejrbNlqgnxhnM+RrU
GmOxFwbLx0iwxQvBrt0+emWqoysBn6bKGHS+kNCV8OLPQdtN+qWwAurg0MCKee2zZOLyN18dC4I4
qVTJuvexUCaNTexDkL567uImEqKDWkcsCuGwhqyMbMJIsvDYUKiUhu6LlKLZGNxsv9idhetUadPF
l4RqjClu4CSYhmf6BAhonkMZV3HBbwz25zx54Ss6S3k9GN/d9FpaKb99Cvxdgw9xeTTGsVx2b106
SED47dmgZzv+4XmQJBv4OoN+HiSbZA6h5QV0nnd1KTHySIXq6c2yuej3qlGz7OykIW/IuxRFhFPn
dYkGCUd/c6S9ZrZJmXUH8OMI49/PZldMJUfKFsO2rjttbIAjmxZzl4tooF8mXHvmz2cFqQ54NUj7
36coYi2SsJET/UyindrQY4ADIqplRKHwq/gY7zy+D/3Tc/eieyUfps5QnZ8F4lv9H2vrWceCqX6K
ksk1bKgWkQ73XoodusIJFnQq6km7HDnuF6r2cp7PM2sQMMVQlgd946pYZS6+8xllSKlnhqF7Bmnh
eFXYZH04JF3tj/2l8wPZM7czGdNlwQImMpLTR4aaR39GsBzDZHTOCafoVNY7dHoW075tvHmdi0PZ
GIf7Zb9GbmCmVziLE77zx3ofoMLAcPeh6lLfWGBRCn2wrTQVjkOfo7QacUCCT112zqMgMM6LrJdO
xqTozraJSksp0r9OsAHHCTV85o1dgogBbIcBfi+SKlx87eXnZVgh2rBP9b6wiihgt6NJhO1xbHub
eIYch8fl3BuTFd0d61Xy/OYs152u7xk/gtdOG70n1lLr0YNPytFR8LLpz8Mrw7tPYPDGfV1mggxW
8qfuoxZh5Q8B5ieoeYs7pHwlu+BFnseMN0GiEV/RQJqSX+vW01Ppdz9Kl550YR8xyj9wxvBKsISl
elCA/56KY3g+o0pYstuhC3cqglvA/g3kwg3bafb/l2ot2O0BZltua3EdoIvNNw9UdohmQnBBDQuG
EvtyDjd2K1mx7u0uZu4nuYQVkXLGl0uUmlmhE0fJf3loSJWVb9PpmydkVz0+A1+/NPqfLlXE5AjR
TkbFT0R5kQG6KBRcKsNUt3cJkHpN1Xb1HRGxDOgXrxLanCubuWWMuc7EFUn+HZHp2Hw5UbCpdUeP
+/Y2lS23Ir2HDl03BjxN7BcLvpryeWbEangHHi8tyhB+5DbJsmKa1MCLpeC0eqdYKMmUK9C+A/+c
WqPE1W9HmGUGoCQWvPIcajcLOr5JlNP5447K3b97ArPh8yCchqalHWlUAG0FQsTjsieZcNrVkoIx
3m/wvUxCxkAU2jKjX4vXt74zu9bOPhErIuxSG0YmBtGmOC/koXN+z2tgCFBTYjnV+AEIER+XWgQE
DOYquU86J/lgrMvLAf37qCThRZzwNChfAQVPlvOSvMXZ+kdMq54PMbqkk/2rF6IkYFFv2DFfS1HP
cwcldcCxDAq21utaVwX4QV5/6k99gF6rglsNzOP8QqLz7Edcx9X0UViqNRLwMF4IjKLUG5U5FV0z
3Mi71xYJcNP7W+33cfKbJjhV15BDs1ix7A7UTWH3Bu14uEtRkBa/46RKN1B5HhSq/dSXB+cydB/I
qXg6sfnlR0cmJ6fhK7byfzTS1/b0G0qc49jjNGvpB0agQ2dRFifE2ZMYh4vxvsAzrF/R4vIoYEBg
cQbRUJ2pNqZbz6xVS5iySRHh3ZietMK5Inw5LUtSkXtkEGjJcELVy+EHmR6N+GuRmStu51AAeOia
KZ+/EM5nd/aHn9br6BIxNZWqEu1kb6IuJ2gqC9eLtGpl/zNqU9dYAVhJRpexPPSkfhmDWu4T/zuD
2G+0jf/fRfaPthgbz6HD/ocfB8qCjM6uGGPkPXGV49rPyPROLU5XlKehEgg630R7zZbsxwJuGc00
T65U2PME3CiDq7U5HsuR/KL50SAlk93X+Kci4jMB+iyygCFNBjEhpt1KVXxsEKTkt5XZQozOl0ns
KO1zEhRU4duN2c86C1AWK5axx1wXx1SExGCWw/fRdzD9I1GtK+Jdz53KBpod/OnlMITczJGwtFfB
5L8dCdebCGWKEp+RLDnt3sbY53V/3TyvbfziF+/eI2ibnSLAQG59cVHPs0NsTsih5fROmedHAeXJ
d5rIpZGtRaZJQanymrmYCqU0BMVhjqQr1udJAUGeSLV1p2jDuSjxcuHQvHU1NaT/VIRAkCdRQWpq
mTgT8QrcWPPgzscO/xRuLamhXiE8IChoIQHB9eZr4kWZrVuuvN63I1EZRtgwoW1tBLgcUoRkHQql
jjUEDPakyS9zS/gWereIhw45Gd439q1Z2Yw8JoDKbH0hLn8X/NUVUAUn0XVDxiO58eg2UPuupXMI
3QgICQCw6X9sjsy7oRdxpMtvguh8/l8bBDFo/Q2NMf0jnl69U+5/79g9r6irvE3YkIQtiOt/hpwY
BmAwpu7g9TkaAYbIA/S8TDEvL3CtJgGdgCER3+gzdXSj+mZXnp6NkLYdLarH2g8cQBzXYbIsP/6D
2JSHP6U9MxEB4O8umoJ1ee4tGG3Qn0Cu2rlKhnG7HOG3mjtL2+0gL4t3s4DHMe55hHretuBF6FaI
DvsrCa58N1HPP2CVxwyVwFdbXC58ltM1IqG8orPpu4H17cbOICjmHfnxhLaa1Z/PMSkBS1QJwys1
/KymG3tyAF6DxogBed2kNT+M9Qn2J3wKFRh+j4k+cZV6GhpniJ/Gvn/qQ3CXINY4HUpE1RNJlOq4
9JwMP6F9pQ+A8YF5+LzblRHnYr9H9wMHWvIwcI+cuBWd9UwggyCas4EO7Cs6fyq7HpGo+ZSWmepJ
85vMZ3ewCVEit9dJIc4Gg/VjGa+YvuCy4+eXaVC3RhXn87IK68RLuRF0MQE0i87drWI9Kw9dLohw
haUUlHhhVzBNquGJHnZRMr1lhT5IZaKf8ujZ6ErXUQSqUGEDyG3Y+FS0FWfXnY0a+zvYoQcYFgp4
GIicv0sQgh+aY9/KYZmkD4wT7wpHdgWyp0VSoDl7VyT4Ku3pkekkl8UsqqhfYgTPih6jaEEuxSb7
woJAAU8F+Cq/zkHVABwwJn4CY/iPUTzF1KfZU2zoL9PjtY4HJWbVyJiD1tWVqDsamYsUzWDuOMic
Po0X3tB3do1ZihKwTC5Lp2TBYDiHsmEQjhbDFa4MzauFPSXGe5uLxxqnB1mIZP4T7sgcmKVvrAh2
x/IJ6nhhyeF4KFosX7lBX3Kba192nP8tsEPJc6tlqnPh2gm3xrdHQfRqXXFTCtuCFQOKntJU+6UR
m82t0u1ZgavLREp0kB6Xw1cFQYt88qJjJeuQ18FHxkubL9iIQQVPhfxSnDHaEIZZibkOeDWIKp1k
XWgNC49otzzM6Xxsr2lD6XNMmzJhI+slkpEk5xANIZsHd3zjjrb5o0Hcf+jnYwEmC0Mt5DAllTjQ
pwu3i7r2r/K2tU0/NN9sQdnAl9WpgegCPLKfcvTqwS2TMiyo2wFF4O5NgOLx0RXjCWL3/xmIsw3O
k2cXnEpIPz8BoC996l/bvgo6exQRGqJB6+TM6eBGs+7UcSjaiCosflaKWGvzBwJpygsLEODKrnqD
hcGuAtZvdcJJp9PlYvRRFYRPAaQoYr3YFAkBYZEtQNHNifEJQMyOkvOk+vwsHk33zZfDU2oOKQL8
lsWmDq//ILcnemaTGzaMoE7Vy7WPQJ9dciCLJ9KmdWpQma3Q9LFWVNsh8A1epXEX8K4howsPdm3K
LnHAnp8FNoLn52qZdrFx5hQzwWbUsNuMInBohSp+QkAzZAUwzyzr8YNiV5JnGmzdLQopcFGDaDl4
Ti5yFDFY89MEmtvwv/wX81WfKbfesYcq9FdI7dk2VMOD52PV/gvOMGTqxUTxx4CV8RWh1hVY1Kp4
yTJKIZy8sGbPxRiZktZRbJ2jyD1aHV2SihaUbXZiEgPd+6q1hbYJfJJkScd3/U+nU7B7gYCZt9uT
9As/6Kc9u38+PaLrOvMUg6/e/qHjd4tfyCTpC2871L2XJauKJnmhjo5DC7YGAr5jsa8IT9yABUpg
GKHga62y3Ab8fatS1EgujMMF+2m8noWYynkBvMdll5ViEbTvCeOzQH8Xb44cj3qj6Ffk9y9U+lCt
qxdNC5+TtFDprhgpEu5ZnTfQjJTxPcEqrvihl6k/FobU8nCD5FjL1ahwoqsQK+USp2c4v9/WsNA5
QCVI9qRvFNyEpszw3eaoWQ3LKajoq8FxS82wH368mRaqvZu7Ot6p3L3564o8ziU5idwPYslPZdrI
KHujK8mfxVlnB+O45h2mWV1U78iPt4hEFGjXkzqK9T+LkqqNIUl/RwiVF5PLgfgxMSOjVNmlyXeC
0SL7w1GdwoAG1Dt/+cJTrpaJdzFaWJ4KuyWr/bnl8ufU7FIgij31V0BjVzeFagrvL37+c01shPkJ
AS9VpBoe7NPaVlT98wBlelhI7/HiOwT8MoRSf1U2BUvIoperxjlgnPATO9/t36GtME2ZTiV82cYY
tYlnT9MRuCrHpd1jYWzRahas5aB4As79LmHCatG2NhT7X49Hu1gjIdJJziwn7XVdTejNUUgQkliJ
knsjMp2TieQw2w5LMX0Xmesu2Hv51tw6de04KoAG+ig4HKoAOLuji7uuIEUZo3W7zyKkxWY1Y1DD
SLRMrny7cdkotXfy992cCPAbJblv+3vmrKxMGWFlZCmxp1kphbbMtzqsdckhffIaw17/R62CziiY
5rUrfE6zKo7GMmMq8rbB0f6xDzdXFpW39xj1Ga8ji8fCTkHUADl6fDApNj0p7SrWk3kLd6rft6rX
58wBpa9pt4lt0ZxI0o50SAjd0ofOQc3ojBHWhScQZZhw6vVGPuCi1rWRHtK4lgKk3F6xxBzX4VoN
bwtcKbiO8YpQdxXWc6X4uZuKXOBbXX+BifmqrYrsi1F6HJ0sRyDCN0ZA+bnIXxBCxvkrgqRSdCYf
acsiwJaJ+uvQdR8jlVrW3EYo7gk1M726Y/zgvkqecBP1d/6A6zSw6OqdbrjjFJK7m3z+CcK+QoZS
19+wa+E7bNGSMHmXBCJ6FTAAP+bdSKn1YvKEoyJ4qqcKyatv3tppdpc5Dft7UHB9OBCQc4knLaiQ
3eToM5KJtKObdm3MIYeDYgu3LxQwLHnO0lVuk/Ee+MmKlOIN992zmGUp4G21+Srpc6fXPM9Iu+yi
RHt/9XyqzAvewZqJuQt4IQ+d3G0OREc7YkP6Z+6Yiw2qBRnxm8vGdNtMBnIJbusVtaJZ0p/UA3v3
eseCceR7QmCA2nra7VPc7A+AHn4iUtJiJe4TYvZVwcn8qJHWnGpwGNQE6YiPaSwaJyh27/e9ve4z
0wE5kaMM0YIK9a2mJqdtXl6Wk0VnW7XC44EUY56OP0uZuh3BHqzy3sTUZio5R5LeM+ak737csx/Z
bcPikVnz0Q7J8l1LZIMt7cWgUGFF7aU3htuVrcnA3ICkXfz0pC5FZjx++wlFTriyo2+8ZIdQx0N0
CbjMQQLY+rmpx9wRcfVr6rTbHdNiFLpBQsfvrGvq8c+U61KttEtrzd8Df6sur8YUKJyLemJe16gJ
CZfPH3mT+3Q1zulj+ZeKog+PJ4vDPWNjkVTlhOp69u5ni/JtyDeroFahtkbF+roTui0pMk2ZJ9Qf
D0b9/umhyekQnUQCBsuUjXwFf/Q3EIZXwHK8F3Fw/CaDIXk9BtGhTPL/3Q/3Dup5nzIJLJpur23b
q0uTCPqqHoOgFxfSqYDEos+Qn96vveZ/Y5qeG2tIvm2fIc7CNKl1s2td5VEc4AlXpUIs77J0hJem
vrqd5rqW2iKytdKrAGM9FFkrAYTXsTPJFhGpbOAg/vFUY6PMojpntUmBJZThZ+oqZYO2/PKx0JM6
YfYbrfLVjDCPhn8BaLWQAOaCKfa8mkgmq6iuTOXaAM7V+QAiW3ZLBkwrqZDZjExcUIW1ch1jsPhE
M2W02+882g5lkvhLOrrWfKDdGpnHle4akoDTPoHlLK07/OjUBbj7MoneDcyK3czjQDKZLGye0vGx
44HzGGoDtUZ5riR2yaHiNzI1Lv5LMBVjL+KWenTbucqJk5wN02dbb8LjXF8T4n2Y37LzP1lEtUB5
lnlYQqdsJRuZCY7OIQQ9E1PRJIFzDdF/XGQqyq4ngmMtsqN8KV62u5LRlkIl4voOIJdNtLUNUfsN
VcL0QHglZyga6EtoxVh5FLkOOL62HC7+v0UzP82kdBFAx2ddNUlq2G3XA+6U0Ly9+XgIrqYJ94Uo
8aHVSp9KFQV4LNH8LfVvGfRe6wJ/to9nHmSBg1RAVsmbiclZGBoxEdPM59kgO/irhGOo9zjglGRb
Io8VcU413oOOy5jkYfVL0K6Li8nJsk6QxBLnSrQICDdae7XsdgrW3rhyxFMA+rOQ9XBu9ZR3BoR9
f6sy1xI2pdCwpJL93SW3PD8U8erZqJSMwmvsBarXqiBfMjBj70pKIWg/tZqTOxUn651rstjLTqWf
AmkLZFrJ1CRE3izDTJKM8DLEP2rgicq1Hxl79zySxS2HPdD+pU4FDXE1ox4ca8YR6wzH2DWumJs5
8yvH+PLJOv3uBl9iM/uFd2HiP/8TWjTOlIt0xxCWbuLYL5pzH25rYvkyiUiSzGBaBS7JviqORe9Q
h1I7QVsHtqmQ7VRZaR6QdBiIm3DjSv5RNOePZPUINMircjEmS3KEmdro4Di5pbYj1r3tudgatMEZ
m6U9aIXbc5rjBY0P1Pnkp7R5MlJ7bEX/gBde1Lr5dzkRzsz1mW86qUr+Sn/udpK60ze6cJEzuP87
YBbYvUgZ90ukAo62IPRvZRTSeDXN9TManmTCPKWcipoes1Tn9j28pj/gxk0CHUi58PeOuUMWzKvR
vKIiv+I7yYiAjmZT4QlYILYC5hhsYqCW0AIjxPYC2fKRKLSeqHJTP1KLqmEGjGllbk3v6wypYLEo
EV83X1BYCCgiJmilGwk7QgKka1vx1Q/OM21UJyXYcyAcRPGJZAp46wJQbzMwSY0BBidLrqL6TrNg
oKdA3ZxVT2fA/lJZKjDpT6SDSDXZh3XfSKasUyDpDh6v7Yok0Ba1nMwmyN2msDSQ1hasIgvNRBvE
lZ/WWT8kDqxuNdmsf45FyJQKjxRyYfbOQtqPgsXR5cPAjIU80dWMGG1zfhdOVu3C+YoLgIesorBY
xjKzmCYCCVtCpajOxlDnGdO7IA8F1qIUACtbp99lLtclCMtSmsg40F0WBRjNQzq1/iBMwQXtPXGa
9YNjCScgFyZUFlAm0aO8m0dcsLstSWwcI5tl4FrmtWMhet93IcEt3VOKaNAhzVnhSgHmhv39e6N/
2LzBuKMfLZ2dQ+Z/og8XukY6ZQSSkxFSp1J3Ny+kcvwgBQNgdvq6kqNzBU8k7U5Ya28I6+TiMCS4
sTvI6VqgVR0ls+LpvDTr0Nu5DMtwZ8GNAOrC9AI3WABF/CZ3w/7OEyJg1o58Ml1aFpT20eAui0wt
RklwWcvDFyPxkQogL9O7caO+0f4bc3OCWravIWt9ITm4UqotExx8nsJKsSan+Oyl3wJ6fwbn3MSD
hCxnB7s2rnxVqoPAazdlq2xBA3LO9s1Y9e4cijvdV8fcMNj7NvSXx9wQKXlvq5lGQtIniWO0Xl8m
5bgiurw0xIFJwgwjphc+RBasQ45dVhHVJzMY5toNlOhLLMlrJz6Z6nOs5XNiY+HP+WxbWM5JXeg2
AsADgpGC6SnNSXqIeXvAptWSjhT5tRFCRons3aTaHtSzbGB6DxxfL9X7x0eXLvzKNq9nziQCeYgf
HmiJY3l/z+lyE1dVuZhl0nb6vAjBrPb46s0mIbef/nvtklf8HbVKBX6UfWiPWdbNDyWWKS30lYie
aCVtGaPUQXIPt0cku3MqhGbBWw2EjhTn5Bo5MX/5CkGy7OW/92CLW7wLrx9xya1v1llBk4IE5qUm
8yW4cVwOA3zLt4Lt3R4oD8CW44rlTBq2yY0wcYUnnPbQNlajm/xld4QhjkYDRYXXijgjvRz74gq8
t1TaCTALdv3Blq+Ig4LrgIf5gA06WTvOFoRq3P+Eg6Bw5qHUHvJUU2CWcrulKxg4pKSxmplUAukb
E5It+/wpXLr/tNOH2L/bKfi1sS72qv1xd6j8u+vaesa/yC4BTb3pUMz9bmaF+/B1DjJR86PWiYRK
DsQJdDm+8qCojOwuXz/HLY16S8oVGGEaLZe5gwXo2TBAIfAY5eAhQuojZTENfaYngAg8Dj7hRW91
soTaa5xtWAWykCzUYmoGtHYxQCjO1Eex3GyUSEBVXIZW1Sgo9TddBlHU0HjES/Ssr4h/Ju9C+cBV
Y9jI9bccM4mlOps6DzGMNVD97gdpeiovK22HlfWEJBu5TNfIu2h+zyGysDTebsAkpgbQHKVFggmj
Jf5RYmLluGurPpOOAzESAIx8G/srJeavyyyppVvND1S+pI4vIEwBP81HMQyC5SnIfjZlauARemZx
Ko8VnKfPaKlrUkDLj7L0HiVVFKQ7onCzjF5fD24zlPW5n6II8Gz1ikwlJjqK42pmRD8ndymWIUv2
Fyur6WPYviYVmmbk4DK5yNpPUDogiXKRoW3r3Oi4Zhoqu3ZSCpA9WWZP1sS2iebBUeWyFtNBOYFv
miiR1om1J/octHIJh3a8AdRntVwJ82kUaybU9tcX4gWbIR1KWJzVZ/xJ/DErHUZ3ZRzTyHUqBiSG
mztAUAQU/9qg5W78KVcq17Fo
`pragma protect end_protected
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

endmodule
`endif

/*=======================================================================================*\
  Filename    : Decode.v
  Author      : 陈献彬
  time        ：2016.08.12
  Description : 译码模块顶层
                              
               
  Called by   : 
  Revision History :
  Author              Date                  Version          Description
  陈献彬              20160812              V1.1.0             
  Email       : 
  Company     :

\*========================================================================================*/
module Decode(
        i_clk_81M,
        i_rst_n,
        
        iv_data_fsk,   //解调后的数据，11bit一组
        i_data_fsk_sck,//iv_data_fsk的字节同步时钟
        
        i_rd_msg_en,       //译码后，报文发送模块给出的读使能
        i_rd_msg_sck,      //译码后，报文发送模块给出的读时钟
        ov_msg_data,       //发送模块读出的数据，8bit一组
        o_msg_trans_ready,
        
        i_rd_telegram_en,
        i_rd_telegram_sck,
        ov_telegram_data,
        o_telegram_trans_ready,
                   
        o_new_msg_flag,
        o_decode_ok    //查表正确
        );            
input i_clk_81M;
input i_rst_n;
input [10:0]iv_data_fsk;
input i_data_fsk_sck;

input i_rd_msg_en;
input i_rd_msg_sck;
output [7:0]ov_msg_data; 
output o_msg_trans_ready;

input i_rd_telegram_en;
input i_rd_telegram_sck;
output [7:0]ov_telegram_data;
output o_telegram_trans_ready;

output o_new_msg_flag;
output o_decode_ok;
//--------------------------------------
wire [10:0]data_fsk;
wire data_fsk_sck;
wire ram_contl_frame_start;
wire Syncdata_sck;
wire [10:0]Syncdata;
wire frame_shift_bit_start;
wire shiftedData_sck;
wire [10:0]shiftedData;
wire o_shift_ok;
wire inversionBit;
wire [31:0]ScrambleCode;
wire o_new_msg_flag;
wire frame_10bit_start;
wire [9:0]descramed_data;
wire descramed_data_sck;
wire check_error;
wire crc_error;
wire frame_first_word_start;
wire [9:0]descramed_data_first_word;
wire descramed_data_sck_first_word;
//------------------------------------------------------------------------------//

RamContrl RamContrl_u(
        .i_clk_81M(i_clk_81M),
        .i_rst_n(i_rst_n),
                       
        .i_data_sck(i_data_fsk_sck),//iv_data_fsk同步时钟，81M取沿 
        .iv_data_fsk(iv_data_fsk),  //11bit数据，一个iv_data_fsk大概持续11X48个27M时钟
                                    //即维持11x48x3个81M时钟
                                     
        .o_data_clk(data_fsk_sck),  //ov_data_fsk同步时钟
        .ov_data_fsk(data_fsk),     //输出数据    
                    
        .o_frame_start(ram_contl_frame_start) //输出1帧（93*11bit)的开始，脉冲                 
        );


SyncCRC SyncCRC_u(
        .i_clk_81M(i_clk_81M),
        .rst_n(i_rst_n),
        
        .i_data_fsk(data_fsk),    //11bit数据输入
        .i_data_sck(data_fsk_sck),//i_data_fsk同步时钟，一个11bit有一个i_data_sck与之对应
        .i_data_start(ram_contl_frame_start),//一帧数据开始指示位
        
        .o_SyncCode_clk(Syncdata_sck),//76位crc校验正确，与同步码对齐
        .ov_SyncCode(Syncdata)        //同步码
        );


ShiftBit ShiftBit_u(
        .i_clk_81M(i_clk_81M),
        .i_rst_n(i_rst_n),       
        
        .i_start(ram_contl_frame_start),        //开始接收新数据报文标志，脉冲
        .i_data_sck(data_fsk_sck),              //11比特字iv_data的同步时钟
        .iv_data(data_fsk),                     //输入11比特字
                
        .iv_syncCode(Syncdata),                 //同步模块给出同步码
        .i_syncCode_en(Syncdata_sck),           //同步码有效信号
        
        .o_frame_start(frame_shift_bit_start),  //开始输出移位拼接后的报文，脉冲，比o_shiftedData_sck早一拍
        .o_shiftedData_sck(shiftedData_sck),    //移位拼接后的报文时钟
        .ov_shiftedData(shiftedData),           //移位拼接后的报文                
                       
        .o_shift_ok(o_shift_ok),                //移位拼接报文正确，实质是CRC76正确信号，脉冲      
        .o_inversionBit(inversionBit),          //正反码标志，1为需要取，与o_shift_ok对齐
        .ov_ScrambleCode(ScrambleCode),         //解扰寄存器组初始值，与o_shift_ok对齐
        .o_new_msg_flag(o_new_msg_flag)
        );            


Bit11ToBit10 Bit11ToBit10_u(
        .i_clk_81M(i_clk_81M),
        .i_rstn(i_rst_n),
        
        .i_data_start(frame_shift_bit_start),
        .iv_data_shifted(shiftedData),
        .i_data_shifted_sck(shiftedData_sck),
        .i_shift_ok_en(o_shift_ok),
        
        .i_ph_flag(inversionBit),
        .iv_pn_data(ScrambleCode),
        
        .o_data_start(frame_10bit_start),
        .ov_descramed_data(descramed_data),
        .o_descramed_data_sck(descramed_data_sck),
        .o_check_error(check_error),
        .o_crc_error(crc_error)
        );
    
assign o_decode_ok = frame_10bit_start;  //11bit查表符合规范

FirstWord FirstWord_u(
        .i_clk_81M(i_clk_81M),
        .i_rstn(i_rst_n),
        
        .i_data_start(frame_10bit_start),
        .iv_descramed_data(descramed_data),
        .i_descramed_data_sck(descramed_data_sck),
        .i_crc_error(crc_error),
        
        .o_data_start(frame_first_word_start),
        .ov_descramed_data(descramed_data_first_word),
        .o_descramed_data_sck(descramed_data_sck_first_word)
        );
        
TransformTelegram TransformTelegram_u(
        .i_clk_81M(i_clk_81M),
        .i_rst_n(i_rst_n), 
        //与shift_bit模块接口
        .i_shift_ok(o_shift_ok), 
        .i_new_msg_flag(o_new_msg_flag),
        .i_data_start(frame_shift_bit_start),
        .i_data_shift_sck(shiftedData_sck),
        .iv_data_shift(shiftedData),              
        //与组帧模块接口
        .i_telegram_transport_en(i_rd_telegram_en),
        .i_telegram_data_sck(i_rd_telegram_sck),
        .ov_telegram_data(ov_telegram_data),       
        .o_telegram_trans_ready(o_telegram_trans_ready)        
        );        
                                 
TransformMsg TransformMsg_u(
        .i_clk_81M(i_clk_81M),
        .i_rst_n(i_rst_n),  
        //新报文指示信号
        .i_new_msg_flag(o_new_msg_flag),     //新报文指示信号，低电平有效                     
        //与FistWord接口
        .i_start(frame_first_word_start),
        .i_check_err(check_error),
        .i_read_data_sck(descramed_data_sck_first_word),
        .iv_read_data(descramed_data_first_word),
        //对外部接口                
        .i_transport_en(i_rd_msg_en),
        .i_out_data_sck(i_rd_msg_sck),
        .ov_out_data(ov_msg_data),
        .o_transform_ready(o_msg_trans_ready)                                             
        );

endmodule 
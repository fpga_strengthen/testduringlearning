`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date: 2024/08/16 14:14:03
// Module Name: mac_receive
// Create Author : J.GMson
// Description: 从MAC层的数据包中去除MAC首部，解析出数据包交给IP层。
// 	主要完成三个功能：
//		1. 完成对前导码、SFD、目的MAC地址的校验；
//		2. 完成CRC校验；
//		3. 完成校验之后，输出有效数据。
//////////////////////////////////////////////////////////////////////////////////

module mac_receive#(
	parameter	LOCAL_MAC_ADDR = 48'hFF_FF_FF_FF_FF_FF,		// local MAC address
	parameter	CRC_CHECK_EN   = 1'b1						// CRC check enable
	)(
	input wire			i_clk,					// 用户端接收时钟
	input wire			i_phy_rx_clk,			// phy芯片提供的时钟

	input wire			i_phy_rx_rst_n,			// phy接收端复位
	input wire			i_rst_n, 				// 用户端复位信号

	/*---------------- rgmii_receive 模块交互信号--------------------*/
	input wire			i_gmii_rx_data_vld,		// 数据有效信号
	input wire	[7:0]	iv_gmii_rx_data,		// MAC数据包数据(单沿)

	/*---------------- mac_to_arp_ip 模块交互信号--------------------*/
	output reg			o_mac_rx_data_vld,		// 数据有效信号
	output reg			o_mac_rx_data_last,		// MAC层数据的最后一个数据的标志
	output reg	[7:0]	ov_mac_rx_data,			// 从MAC数据包解析出MAC层数据
	output reg	[15:0]	ov_mac_rx_frame_type,	// 帧类型

	/*---------------- mac_to_arp_ip 模块交互信号--------------------*/
	input wire	[31:0]	iv_rx_CRC_data,			// 收到的CRC数据
	output reg			o_rx_CRC_din_vld,		// CRC校验使能信号
	output reg	[7:0]	ov_rx_CRC_din,			// 一个字节的CRC
	output reg			o_rx_CRC_done			// 一帧数据的CRC结束
);

/*-----------------------------------------------*\
					状态机信号定义
\*-----------------------------------------------*/	
	reg 	[2:0]	cur_status;
	reg 	[2:0]	nxt_status;

	localparam  RD_IDLE = 3'b000,
				RD_PRE  = 3'b001,
				RD_DATA = 3'b010,
				RD_END  = 3'b100;

/*-----------------------------------------------*\
				输入数据信号打拍
\*-----------------------------------------------*/
	// 将vld信号打五拍,从而给出last信号,得到数据部分的界定符
	reg			gmii_rx_data_vld_d0;
	reg			gmii_rx_data_vld_d1;
	reg			gmii_rx_data_vld_d2;
	reg			gmii_rx_data_vld_d3;
	reg			gmii_rx_data_vld_d4;

	reg	[7:0]	gmii_rx_data_d0;
	reg	[7:0]	gmii_rx_data_d1;
	reg	[7:0]	gmii_rx_data_d2;
	reg	[7:0]	gmii_rx_data_d3;
	reg	[7:0]	gmii_rx_data_d4;

	wire		rx_data_last;		// 下降沿,表示MAC层的最后一个数据
	reg			rx_data_last_d0;
	reg			rx_data_last_d1;
	reg			rx_data_last_d2;
	reg			rx_data_last_d3;
	reg			rx_data_last_d4;
	reg			rx_data_last_d5;
	reg			rx_data_last_d6;
	
/*-----------------------------------------------*\
				定义FIFO端口信号
\*-----------------------------------------------*/

	// frame fifo
	reg				frame_wren;		// 帧数据写使能
	reg		[16:0]	frame_din;		// 1bit CRC + 2byte Type
	wire	[16:0]	frame_dout;		// 读出的数据
	wire			frame_rden;		// 读使能
	wire			frame_wrfull;	// FIFO写满标志
	wire			frame_rdempty;	// FIFO读空
	wire	[3:0]	frame_wrcount;
	wire	[3:0]	frame_rdcount;

	// data fifo
	reg				data_wren;		// 帧数据写使能
	reg		[8:0]	data_din;		// 1bit CRC + 2byte Type
	reg				data_rden;		// 读使能
	wire	[8:0]	data_dout;		// 读出的数据
	wire			data_wrfull;	// FIFO写满标志
	wire			data_rdempty;	// FIFO读空
	wire	[11:0]	data_wrcount;
	wire	[11:0]	data_rdcount;

	reg 			frame_CRC_result;	// CRC校验结果：1正确,0错误

/*-----------------------------------------------*\
				定义MAC其他相关信号
\*-----------------------------------------------*/
	
	// 前导码
	localparam  PREAMBLE = 56'h55_55_55_55_55_55_55;

	reg 	[55:0]	rx_preamble;			// 前导码, 7 Byte
	reg 			rx_preamble_check;		// 前导码校验信号
	reg 	[47:0]	rx_target_mac;			// 目的MAC地址, 6 Byte
	reg 			rx_target_mac_check;	// 目的MAC校验信号
	reg 			rx_SFD_check;			// 帧起始校验
	reg  			rx_CRC_check;			// CRC校验

	reg 	[15:0]	rx_frame_type;			// 帧类型

	reg 			rx_wr_data_valid;		// 写数据有效信号

/*-----------------------------------------------*\
				数据和使能信号打拍
\*-----------------------------------------------*/

	// vld
	always @(posedge i_phy_rx_clk or negedge i_phy_rx_rst_n) begin
		if(~i_phy_rx_rst_n) begin
			gmii_rx_data_vld_d0 <= 1'b0;
			gmii_rx_data_vld_d1 <= 1'b0;
			gmii_rx_data_vld_d2 <= 1'b0;
			gmii_rx_data_vld_d3 <= 1'b0;
			gmii_rx_data_vld_d4 <= 1'b0;
			
		end 
		else begin
			gmii_rx_data_vld_d0 <= i_gmii_rx_data_vld;
			gmii_rx_data_vld_d1 <= gmii_rx_data_vld_d0;
			gmii_rx_data_vld_d2 <= gmii_rx_data_vld_d1;
			gmii_rx_data_vld_d3 <= gmii_rx_data_vld_d2;
			gmii_rx_data_vld_d4 <= gmii_rx_data_vld_d3;
		end
	end

	// 取vld信号的下跳沿
	assign	rx_data_last = gmii_rx_data_vld_d0 & (~i_gmii_rx_data_vld);

	// data
	// 打5拍,倒数低5拍是为了取到vld下降沿得到last信号与最后一个数据同步.
	// 后边四拍有效的vld信号是与CRC32的4个字节同步的.
	always @(posedge i_phy_rx_clk or negedge i_phy_rx_rst_n) begin
		if(~i_phy_rx_rst_n) begin
			gmii_rx_data_d0 <= 8'b0;
			gmii_rx_data_d1 <= 8'b0;
			gmii_rx_data_d2 <= 8'b0;
			gmii_rx_data_d3 <= 8'b0;
			gmii_rx_data_d4 <= 8'b0;
		end 
		else begin
			gmii_rx_data_d0 <= iv_gmii_rx_data;
			gmii_rx_data_d1 <= gmii_rx_data_d0;
			gmii_rx_data_d2 <= gmii_rx_data_d1;
			gmii_rx_data_d3 <= gmii_rx_data_d2;
			gmii_rx_data_d4 <= gmii_rx_data_d3;
		end
	end

	// data_last_symbol
	always @(posedge i_phy_rx_clk or negedge i_phy_rx_rst_n) begin
		if(~i_phy_rx_rst_n)begin
			rx_data_last_d0 <= 1'b0;
			rx_data_last_d1 <= 1'b0;
			rx_data_last_d2 <= 1'b0;
			rx_data_last_d3 <= 1'b0;
			rx_data_last_d4 <= 1'b0;
			rx_data_last_d5 <= 1'b0;
			rx_data_last_d6 <= 1'b0;
		end
		else begin 
			rx_data_last_d0 <= rx_data_last;
			rx_data_last_d1 <= rx_data_last_d0;
			rx_data_last_d2 <= rx_data_last_d1;
			rx_data_last_d3 <= rx_data_last_d2;
			rx_data_last_d4 <= rx_data_last_d3;
			rx_data_last_d5 <= rx_data_last_d4;
			rx_data_last_d6 <= rx_data_last_d5;
		end
	end

/*-----------------------------------------------*\
				对输入数据字节计数
\*-----------------------------------------------*/

	// 数据部分最多1500字节,其他部分26字节,11位足够计数
	reg 	[10:0]	cnt_rx_byte;

	always @(posedge i_phy_rx_clk or negedge i_phy_rx_rst_n) begin
		if(~i_phy_rx_rst_n)
			cnt_rx_byte <= 11'd0;
		else if(gmii_rx_data_vld_d4)
			cnt_rx_byte <= cnt_rx_byte + 1'b1;
		else
			cnt_rx_byte <= 11'd0;
	end

/*-----------------------------------------------*\
				前导码接收和校验
\*-----------------------------------------------*/

	// 注意只在计数器对应范围内才会进行移位寄存;寄存完了后就会保持不变了.
	// 缓存前导码
	always @(posedge i_phy_rx_clk or negedge i_phy_rx_rst_n) begin
		if(~i_phy_rx_rst_n)
			rx_preamble <= 56'b0;
		else if(gmii_rx_data_vld_d4 &&(cnt_rx_byte < 11'd7))
			rx_preamble <= {rx_preamble[47:0], gmii_rx_data_d4};
		else
			rx_preamble <= rx_preamble;
	end
	
	// 只要前导码正确,此校验信号就会一直拉高(因为前导码会一直保持),而非仅仅持续一个时钟周期.
	// 前导码校验
	always @(posedge i_phy_rx_clk or negedge i_phy_rx_rst_n) begin
		if(~i_phy_rx_rst_n)
			rx_preamble_check <= 1'b0;
		else if(rx_preamble == PREAMBLE)
			rx_preamble_check <= 1'b1;
		else
			rx_preamble_check <= 1'b0;
	end

/*-----------------------------------------------*\
				目的MAC接收和校验
\*-----------------------------------------------*/

	// 接收
	always @(posedge i_phy_rx_clk or negedge i_phy_rx_rst_n)begin
		if(~i_phy_rx_rst_n)
			rx_target_mac <= 48'b0;
		else if(gmii_rx_data_vld_d4 && (cnt_rx_byte < 11'd14) && (cnt_rx_byte > 11'd7))
			rx_target_mac <= {rx_target_mac[39:0], gmii_rx_data_d4};
		else
			rx_target_mac <= rx_target_mac;
	end

	// 校验
	always @(posedge i_phy_rx_clk or negedge i_phy_rx_rst_n) begin
		if(~i_phy_rx_rst_n)
			rx_target_mac_check <= 1'b0;
		else if(rx_target_mac == LOCAL_MAC_ADDR)
			rx_target_mac_check <= 1'b1;
		else
			rx_target_mac_check <= 1'b0;
	end

/*-----------------------------------------------*\
					SFD校验
\*-----------------------------------------------*/

	always @(posedge i_phy_rx_clk or negedge i_phy_rx_rst_n) begin
		if(~i_phy_rx_rst_n) begin
			rx_SFD_check <= 1'b0;
		end 
		else if(cnt_rx_byte == 'd7)begin
			if(gmii_rx_data_d4 == 8'hD5)
				rx_SFD_check <= 1'b1;
			else
				rx_SFD_check <= 1'b0;
		end
		else
			rx_SFD_check <= rx_SFD_check;
	end

/*-----------------------------------------------*\
					CRC校验
\*-----------------------------------------------*/

	generate
		if(CRC_CHECK_EN == 1'b0)begin
			always @(posedge i_phy_rx_clk)begin
				rx_CRC_check <= 1'b1;
			end
		end
		else if(CRC_CHECK_EN == 1'b1)begin 

			(* dont_touch="true" *)reg 			crc_en;		// 作用是把需要待校验的数据标记出来
			(* dont_touch="true" *)reg 	[31:0]	rx_CRC;		// 缓存收到的CRC

			always @(posedge i_phy_rx_clk) begin 
				if(rx_data_last_d0 || rx_data_last_d1 || rx_data_last_d2 || rx_data_last_d3)
					// 与MAC层数据包其他部分先发送高字节相反,CRC会先发送低字节
					rx_CRC <= {gmii_rx_data_d4, rx_CRC[31:8]};		
				else
					rx_CRC <= rx_CRC;
			end

			// 参考MAC层数据包帧格式 : 从目的MAC地址(第9个字节)开始,到MAC层数据结束,都是需要校验的.
			always @(posedge i_phy_rx_clk) begin
				if(cnt_rx_byte == 11'd7)
					crc_en <= 1'b1;		// 时序逻辑,在等于8、即第9个字节时,crc_en才会拉高
				else if(rx_data_last)
					crc_en <= 1'b0;
				else
					crc_en <= crc_en;
			end

			// 这一段的数据和vld信号是对齐的,因为cnt_rx_byte是以vld_d4作为计数使能的
			always @(posedge i_phy_rx_clk) begin 
				o_rx_CRC_din_vld <= crc_en;
				ov_rx_CRC_din	 <= gmii_rx_data_d4;
				o_rx_CRC_done	 <= rx_data_last_d6;
			end

			// 判断计算的CRC和收到的CRC是否一致
			always @(posedge i_phy_rx_clk) begin
				if(rx_data_last_d5)begin
					if(rx_CRC == iv_rx_CRC_data)
						rx_CRC_check <= 1'b1;		// 只需要在d5时刻进行判断
					else
						rx_CRC_check <= 1'b0;
				end 
				else 
					rx_CRC_check <= rx_CRC_check;
			end
		end
	endgenerate
	
/*-----------------------------------------------*\
			将帧类型、CRC校验和写入帧FIFO
\*-----------------------------------------------*/
	// frame fifo
	// 存储1bit的CRC校验结果和帧类型的两个字节

	// 接收帧类型字节
	always @(posedge i_phy_rx_clk) begin
		if(~i_phy_rx_rst_n)
			rx_frame_type <= 16'b0;
		else if(gmii_rx_data_vld_d4 && (cnt_rx_byte == 11'd20 || cnt_rx_byte == 11'd21))
			rx_frame_type <= {rx_frame_type[7:0], gmii_rx_data_d4};
		else
			rx_frame_type <= rx_frame_type;
	end

	// 合并得到要存入FIFO的数据
	// 在d3时计算、在d4时得到原数据中的CRC值;在d5时刻判断收到的CRC与计算的是否一致,给出CRC校验成功标志;
	// 在d6时根据CRC校验结果以及MAC首部各部分的校验结果共同决定是否将数据写入FIFO中.
	always @(posedge i_phy_rx_clk or negedge i_phy_rx_rst_n) begin
		if(~i_phy_rx_rst_n) begin
			frame_din  <= 17'b0;
			frame_wren <= 1'b0;
		end 
		else if(rx_preamble_check && rx_target_mac_check && rx_SFD_check && rx_data_last_d6)begin
			frame_din  <= {rx_CRC_check, rx_frame_type};
			frame_wren <= 1'b1;
		end
		else begin
			frame_din  <= frame_din;
			frame_wren <= 1'b0;
		end
	end

/*-----------------------------------------------*\
		将收到的MAC层数据写入FIFO(去除了MAC首部)
\*-----------------------------------------------*/

	always @(posedge i_phy_rx_clk or negedge i_phy_rx_rst_n) begin
		if(~i_phy_rx_rst_n) 
			rx_wr_data_valid <= 1'b0;
		else if(rx_preamble_check && rx_target_mac_check && rx_SFD_check && (cnt_rx_byte == 11'd21))
			rx_wr_data_valid <= 1'b1;	// 在MAC首部边界时就将数据写入FIFO
		else if(rx_data_last)			// cnt_rx_byte是按照vld_d4进行计数的,所以和gmii_rx_data_d4是同步的.
			rx_wr_data_valid <= 1'b0;
		else
			rx_wr_data_valid <= rx_wr_data_valid;
	end

	// 数据写FIFO
	always @(posedge i_phy_rx_clk or posedge i_phy_rx_rst_n) begin
		if(~i_phy_rx_rst_n) begin
			data_wren <= 1'b0;
			data_din  <= 9'b0;
		end 
		else begin
			data_wren <= rx_wr_data_valid;
			data_din  <= {rx_data_last,gmii_rx_data_d4};
		end
	end

/*-----------------------------------------------*\
				状态机控制读FIFO
\*-----------------------------------------------*/

	always@(posedge i_clk or negedge i_rst_n)begin 
		if(~i_rst_n)
			cur_status <= RD_IDLE;
		else
			cur_status <= nxt_status;
	end

	always@(*)begin 
		if(~i_rst_n)
			nxt_status <= RD_IDLE;
		else begin 
			case (cur_status)
				RD_IDLE : begin 
					if(~frame_rdempty)
						nxt_status <= RD_PRE;
					else
						nxt_status <= cur_status;
				end
				RD_PRE  : nxt_status <= RD_DATA;
				RD_DATA : begin 
					if(data_rden && data_dout[8])
						nxt_status <= RD_END;
					else
						nxt_status <= cur_status;
				end
				RD_END  : nxt_status <= RD_IDLE;
			default : nxt_status <= RD_IDLE;
			endcase
		end
	end

/*-----------------------------------------------*\
					FIFO例化
\*-----------------------------------------------*/

	frame_fifo_w17xd16 rx_frame_fifo(
  		.rst           (~i_phy_rx_rst_n),           // input wire rst
  		.wr_clk        (i_phy_rx_clk),              // input wire wr_clk
  		.rd_clk        (i_clk),                		// input wire rd_clk
  		.din           (frame_din),                 // input wire [16 : 0] din
  		.wr_en         (frame_wren),                // input wire wr_en
  		.rd_en         (frame_rden),                // input wire rd_en
  		.dout          (frame_dout),                // output wire [16 : 0] dout
  		.full          (frame_wrfull),              // output wire full
  		.empty         (frame_rdempty),             // output wire empty
  		.rd_data_count (frame_rdcount),  			// output wire [3 : 0] rd_data_count
  		.wr_data_count (frame_wrcount)  			// output wire [3 : 0] wr_data_count
	);

	fifo_data_w9xd4096 rx_data_fifo(
	 	.rst           (~i_phy_rx_rst_n),           // input wire rst
	 	.wr_clk        (i_phy_rx_clk),              // input wire wr_clk
	 	.rd_clk        (i_clk),                		// input wire rd_clk
	 	.din           (data_din),                  // input wire [8 : 0] din
	 	.wr_en         (data_wren),                 // input wire wr_en
	 	.rd_en         (data_rden),                 // input wire rd_en
	 	.dout          (data_dout),                 // output wire [8 : 0] dout
	 	.full          (data_wrfull),               // output wire full
	 	.empty         (data_rdempty),              // output wire empty
	 	.rd_data_count (data_rdcount),  			// output wire [11 : 0] rd_data_count
	 	.wr_data_count (data_wrcount)  				// output wire [11 : 0] wr_data_count
	);

/*-----------------------------------------------*\
			从帧FIFO中读出帧类型和CRC校验结果
\*-----------------------------------------------*/

	// 帧FIFO读使能
	assign	frame_rden = (cur_status == RD_PRE);

	// 输出FIFO的读出的帧类型与数据结束标志
	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n) begin
			ov_mac_rx_frame_type <= 16'b0;
			frame_CRC_result     <= 1'b0;
		end 
		else if(frame_rden)begin
			ov_mac_rx_frame_type <= frame_dout[15:0];
			frame_CRC_result     <= frame_dout[16];
		end
		else begin 
			ov_mac_rx_frame_type <= ov_mac_rx_frame_type;
			frame_CRC_result     <= frame_CRC_result;
		end
	end

/*-----------------------------------------------*\
				从data FIFO中读出数据
\*-----------------------------------------------*/

	// 读出的逻辑和帧FIFO是一样的
	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n)
			data_rden <= 1'b0;
		else if(cur_status == RD_PRE)
			data_rden <= 1'b1;
		else if(data_rden && data_dout[8] && (cur_status == RD_DATA))
			data_rden <= 1'b0;		// data_dout[8]表示读出了MAC数据部分的最后一个字节
		else
			data_rden <= data_rden;
	end

	// 输出数据
	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n)begin 
			o_mac_rx_data_vld  <= 1'b0;		// 数据有效信号
			o_mac_rx_data_last <= 1'b0;		// MAC层数据的最后一个数据的标志
			ov_mac_rx_data	   <= 8'b0;		// 数据部分
		end
		else if(data_rden && frame_CRC_result)begin 
			o_mac_rx_data_vld  <= 1'b1;	
			o_mac_rx_data_last <= data_dout[8];
			ov_mac_rx_data	   <= data_dout[7:0];
		end
		else begin 
			o_mac_rx_data_vld  <= 1'b0;
			o_mac_rx_data_last <= 1'b0;
			ov_mac_rx_data	   <= 8'b0;
		end
	end

endmodule
//给出N阶滤波器，N个位组成的查找表。所有位数都使用相同的表
module DA_ROM 
#(
    parameter   N_taps = 5,
    parameter   ROM_WIDTH = 18
)(
    input       [N_taps-1:0]    addr,
    output reg  [ROM_WIDTH-1:0] data
);

    always@(addr)begin 
        case (addr)
            5'b00000 : data = 18'b00_0000_0000_0000_0000; 
            5'b00001 : data = 18'b00_0011_0000_1010_0000;
            5'b11111 : data = 18'b01_1001_1101_0101_0000;
            default  : data = 18'b01_1001_1101_0101_0000;
        endcase
    end

endmodule
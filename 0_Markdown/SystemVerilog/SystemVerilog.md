# 1. 验证平台的组件介绍

如图所示，是验证组件，其主要成分分别为：

<img src="./pics/1.png" style="zoom:70%;float:left" >

**Stimulus**：可以理解为对DUT所有输入的一个激励；

**Monitor**：从DUT的pin上面采样数据，并且将接收到的数据进行封装，或者将接收到的数据直接判断正确与否；

**Scoreboard**：将DUT输出的结果与期望的结果做对比，检查来判断输出是否正确，期望结构来自RM；

**Coverage**：覆盖率是衡量那些规格特征已经被测试过的指标；

**Formal**：利用数学手段检查等价或者一致性的方法，有些是工具检查，有些需要一些新的语言支持；

**Assertion**：特指SVA，对特定时序做检查的验证手段，仿真器时刻追踪被激活的断言；

**Reference Model**：将规格描述翻译成code算法，将规格期望的结果输出用以对比DUT的输出；

**Driver**：将Stimulus驱动到DUT的输入pin，如果Stimulus是抽象级别的封装，则需要转换为接口级别的信息。



# 2. 数据类型

> 概述

- Verilog没有严格区分信号的类型，变量和线网类型均是**四值逻辑（0,1,x,z）**；

  其中，**x**在实际电路中是没有，它代表的是信号在一个不确定的状态；**z**则代表高阻态，代表悬空。

  Verilog中的变量类型其实就是reg，只能使用过程赋值。

- SV添加了许多新的**数据类型**，并且SV将硬件信号区分为**“类型”**和**“数据类型”**：

  - **类型**即表示该信号为变量（variables，关键字是var）或者线网类型（nets，关键字是wire）；
  - 对于**线网类型**赋值只能使用**连续**赋值语句assign；
  - 对于**变量类型**赋值可以使用连续赋值、或者过程赋值；
  - **数据类型**则表示该数据是**四值逻辑（logic）还是二值逻辑（bit）**。

- 在SV中，数据类型可以分为线网和变量。线网的赋值设定与Verilog要求相同，只能用assign，而不能出现在过程块中；

  相比于线网驱动的限制，变量（var）类型的驱动要求就没那么多，例如`logic [3:0] a`，该变量默认类型为`var`，可以连续赋值或过程赋值。

- 所以从以下三个方面考虑：

  - 是类型还是数据类型？
  - 对于数据类型，是四值逻辑还是二值逻辑？
  - 对于数据，是有符号数还是无符号数？

- 简而言之，可以在Tesetbench（module）中大量使用logic类型变量，而很少使用wire。

  什么时候使用wire呢？当多于一个驱动源的时候（因为logic是针对单一驱动源的），或者设计模块端口是双向（inout）的时候。
  
  <img src="./pics/2.svg" style="zoom:87%;float:left" >

如下是一段代码示例：

```verilog
module tb;
    wire	w1;
    logic	r1;		// logic (var)
    assign	w1 = 1'b1;
    assign	r1 = 1'b0;
    
    wire	logic	w2;
    var		logic	r2;
    assign	w2 = 1'b1;
    assign	r2 = 1'b0;
    initial begin
		#1ns;
        $display("w1 = %b, r1 = %b", w1, r1);
        $display("w2 = %b, r2 = %b", w2, r2);
    end
endmodule
```



> 四值逻辑数据类型

- reg不一定会被总和为register，它只是用原来与线网类型相对表示存储数据的变量；

- 在SV中，可以直接使用logic（数据类型）来达到通用的存储硬件数据的目的；

- logic虽然只是表示数据类型，而在声明时，它**默认会表示变量类型**，用户也可以显示声明其类型：

  ```verilog
  var		logic	[63:0]	addr;
  wire	logic	[63:0]	data;	
  ```

  

> 二值逻辑数据类型

- SV添加了二值逻辑数据类型，用来对在RTL更高抽象级的模型建模，例如系统级或者事务级模型，这些二值逻辑数据类型包括：
  - **bit**：a 1-bit 2-state integer
  - **byte**：an 8-bit 2-state integer，similar to a C char
  - **shortint**：a 16-bit 2-state integer，similar to a C short
  - **int**：a 32-bit 2-state integer，similar to a C int
  - **longint**：a 64-bit 2-state integer，similar to a C longlong
- 在RTL级别，X和Z都是用来在硬件级别建模使用，Z用来表示未连接或者三态的设计逻辑，但是在系统级或者事务级，Z和X很少使用；
- 类似于logic类型默认为变量类型，bit类型默认也为变量类型。



> 有符号和无符号类型

- logic或者bit构成的向量是无符号类型；
- integer、byte、shortint、int、longint为有符号类型；
- 可以在有符号类型后添加`unsigned`来表示无符号类型。



将数据类型按照不同的标准可分别进行划分：

<img src="pics/3.png" style="zoom:67%;float:left" >



> 仿真行为

- 四值逻辑变量如reg、logic或者integet等，在仿真开始时的初值为X；
- 二值逻辑变量例如bit等，在仿真开始时的初值为0；
- 如果四值逻辑与二值逻辑的数据类型之间发生了默认转换，那么Z和X将转换为0；
- 二值逻辑也可以用来实现可综合电路，只是**由于没有X和Z值，可能会出现仿真行为同综合电路结果不一致的情况**。
- **赋值的混用**：二值逻辑可以直接赋值给四值逻辑，在语法上不会出现问题；四值逻辑到二值逻辑也不会语法出错，但是会出现上边的问题。



> 其他类型

- SV中添加void类型来表示空类型，经常用在函数定义时表示不会返回数值，同C语言的void使用方法；
- SV添加shortreal表示32位单精度浮点类型，同C语言的float；而Verilog的real类型表示双精度浮点类型，同C语言的double。



# 3. 自定义类型

## 3.1 `typedef`概述

- 通过用户自定义类型，以往Verilog的代码可以通过更少的代码来表示更多的功能；
- 用户自定义类型使得代码自身的可读性更好；
- 通过**typedef**来创建用户自定义类型；

- SV提供了特性使得用户可以构建更高抽象层的数据类型；

- 类似于C，用户可以利用已有的数据类型来定义新的数据类型，一旦定义了新的数据类型，则此类型的变量可以声明：

  ```verilog
  typedef int unsigned uint;
  unit	a,b;
  ```

  为了使代码更易读和维护，通常对于自定义的类型，都习惯添加`_t`的后缀来表示它是一个自定义类型（type）。



## 3.2 通过`enum`来创建枚举类型

- 枚举类型提供方法来描述抽象变量合法值范围，其每一个值都需要提供一个用户自定义的名字；

- 下面的枚举类型RGB可以拥有red、green和blue的三个数值：

  ```verilog
  enum	{red,green,blue}	RGB;
  ```

  RGB的初始值在这里就是red，【注意】<font color="red">RGB在这里是一个变量</font>。

- Verilog语言自身不提供枚举类型，因此为了提供类似于枚举类型可实现的便利，

  采用了`parameter`常量来表示可取的值范围，或者使用``define`来定义各个合法值对应的宏名称。

- 默认的枚举类型是int，即32位的二值逻辑数据类型；

- 为了能更准确的描述硬件，SV允许指明枚举类型的数据类型，例如：

  ```verilog
  enum	bit		{TRUE,FALSE}			Boolean;
  enum	logic	{WAITE, LOAD, READY}	state;
  ```

  如果枚举类型并没有伴随着`typedef`，那么该枚举类型指的则是一个匿名枚举类型。

- 如果一个枚举类型数值被赋值，那么所赋的值必须符合其数据类型；

  ```verilog
  enum	logic	[2:0]	{WAITE = 3'b001, LOAD = 3'b010, READY = 3'b100}	state;
  ```

- 如果枚举类型是四值逻辑数据类型，那么对枚举值赋为X或者Z也是合法的。

- 枚举类型赋值时则相对严格，例如下边的例子中，赋值操作符的左右两侧应该尽量为相同的枚举类型

  ```verilog
  typedef	enum	{WAITE, LOAD, READY}	states_t;
  states_t	state, next_state;
  int	foo;
  
  // legal : 因为这里是枚举类型之间的相互赋值
  state = next_state;
  // legal : 由于1是int,所以相加时state会隐性的转化为int和1相加,而foo是int型
  foo = state + 1;
  // illegal : foo是int,相加结果为int;而state是enum类型,enum型可以直接赋值给int型,但int不能直接赋给enum型
  state = foo + 1;
  // illegal : 相加时state会由于1而转化为int,但state是enum型,而int不能直接赋给enum型
  state = state + 1;
  // illegal : ++运算符暗含+1运算,然后同上
  state++;
  // illegal : +运算会把两个枚举类型都隐式的转换为int型然后相加,而int型不能赋给enum型
  next_state += state;	
  ```

  以上问题即注意：**枚举类型可以转化为整型，但整型不能隐式的转化为枚举类型**。

如下是一个枚举类型的示例：

```verilog
package	chip_types;
    // typedef在这里意思是把从enum开始到instr_t之前的这部分整体作为一个缺省类型并将其命名为instr_t
    // 这样会将instr_t放到package中
    typedef	enum  {FETCH,WRITE,ADD,SUB, MULT, DIV, SHIFT, NOP}	instr_t;
endpackage

// 从package中将instr_t这个类型导出
import	chip_types::*;	// import package definitions into 

module controller(
	output	logic	read, write,
    input	instr_t	instruction,	// 导出后就可以使用instr_t
    input	wire	clock,resetN
);
    // enum在这里没有使用typedef,直接使用了下面这个缺省类型来声明了两个变量
    // 一般情况下,会习惯先用typedef定义这个类型State_t,然后再用State_t去声明这两个变量
    enum	{WAITE,LOAD,STORE}	State, NextState;
    // 个人理解这样相对于parameter的好处是:对于一个State,它被限定在了这个缺省类型中,不会出现Verilog中的x值
    
    always_ff@(posedge clock, negedge resetN)
        if(!resetN)
            State <= WAITE;
    	else
            State <= NextState;
    
    always_comb begin
        case(State)
            WAITE : NextState = LOAD;
            LOAD  : NextState = STORE;
            STORE : NextState = WAITE;
        endcase
    end
    
    always_comb begin
        read = 0; write = 0;
        if(State == LOAD && instruction == FETCH)
            read = 1;
        else if(State == STORE && instruction == WRITE)
            write = 1;
    end
    
endmodule
```



## 3.3 通过`struct`来创建结构体类型

- 设计或者验证的数据经常会有逻辑相关的数据信号组，例如一个总线协议的所有控制信号，或者在一个状态控制器中用到的所有的信号；

- Verilog语言中没有方便的特性可以将相关的信号收集整理到一个信号组中；

- SV中添加了同C一样的结构体`struct`，而结构体的成员可以是任何变量类型，包括自定义类型或者其他常量类型：

  ```verilog
  struct {
      int			a,b;		// 32-bit variables
      opcode_t	opcode;		// user-defined type
      logic[23:0]	address;	// 24-bit variable
      bit			error;		// 1-bit 2-state var
  } Instruction_Word;
  ```

  在上边的示例中，`Instruction_Word`之前的部分是一个结构体类型，而`Instruction_Word`则是一个结构体变量。

- 正是由于结构体是变量的合集，因此结构体类型的变量也可以用来索引到其内部的变量，索引方式同C语言一致：

  ```verilog
  <structure_name>.<variable>
  Instruction_Word.address = 32'hF000001E;
  ```

  对结构体内部变量索引的方式就是通过一个`.`来进行。

- 结构体类型默认也是变量类型，用户也可以显示声明其为`var`或者`wire`类型。

- 类似于枚举类型，结构体类型也可以伴随着`typedef`来实现自定义结构体类型：

  ```verilog
  typedef	struct {			// structure definition
      logic	[31:0]	a,b;
      logic	[7:0]	opcode;
      logic	[23:0]	address;
  } instruction_word_t;
  instruction_word_t	IW;		// structure allocation
  ```

  下面是结构体变量不同赋值方式的示例：

  ```verilog
  // 1.通过索引其各个成员做依次的成员赋值
  always@(posedge clock, negedge resetN)begin 
      if(!resetN)begin
         	IW.a = 100;		// reference structure member
          IW.b = 5;
          IW.opcode = 8'hFF;
          IW.address = 0;
      end
      else begin
          ...
      end
  end
  
  // 2.也可以通过单引号'和花括号{}来实现整体赋初值
  IW = '{100, 3, 8'hFF, 0};
  IW = '{address:0, opcode:8'hFF, a:100, b:5};
  ```



# 4. 字符串类型

## 4.1 概述

- Verilog语言对于字符串的处理手段非常有限；

- SV引入了**string**类型用来容纳可变长度的字符串；
- 字符串类型变量的存储单元为**byte**类型；
- 字符串类型变量长度为**N**时，其字符成员索引值为**0~N-1**；
- 不同于C函数，字符串结尾没有”空字符“，即`null`字符`\0`;
- 字符串的内存是动态分配的，用户无需担心内存空间管理。



## 4.2 字符串操作

> 字符串类型的常见使用方式

```verilog
typedef	logic [15:0]	r_t;		// 定义一个type
r_t		r;							// 使用自定义的type定义一个variable r
integer	i = 1;						// 定义一个四值变量i并赋值1
string	b = "";						// 定义一个空字符串b,等价于 string b;
string	a = {"Hi",b};				// 定义一个字符串a,其最小单位是一个字节

r = r_t'(a);		// 将字符串a进行类型转换
b = string'(r);		// 将r转化为字符串类型

b = "Hi";			// 对字符串b赋值
b = {5{"Hi"}};		// 相当于复制扩展,b="HiHiHiHiHi"
a = {i{"Hi"}};		// i=1,所以a={1{"Hi"}}="Hi"
a = {i{b}};			// i=1,所以a={1{b}}=b
a = {a,b};			// 拼接运算符,a={a,b}={b,b}
a = {"Hi",b};		// 拼接运算符,a={"Hi",{5{"Hi"}}}={6{"Hi"}}
b = {"Hi",""};		// 拼接运算符,b={"Hi"},""是一个空字符串

a = "Hello";
// a[0]是字符串a的最左边的一个字符,但是a[0]是8位,意思是a的第0个字符,它以一个字节为一个基本单位
a[0] = "h";			// 此时 a="hello"	
a[0] = "hello";		// 此时用字符串"hello"的最右边的一位"o"去替换掉a的最低位,即a="oello"
```



> 字符串内建方法

- **str.len()**：返回字符串的长度

- **str.put(i, c)**：将第`i`个字符替换为字符`c`，等同于**str[i]=c**

- **str.getc(i)**：返回第`i`个字符

- **str.substr(i, j)**：将从第`i`个字符到第`j`个字符的字符串返回

- **str.{atoi(), atohex(), atooct, atobin}**：将字符串当作十进制、十六进制、八进制或者二进制数据来对待

  ```verilog
  str = "123";
  int i = str.atoi();		// assigns 123 to i. 
  ```

  其中`atoi`的`i`表示`integer`，即十进制整数。



# 5. v0实验

本实验主要是对`router.v`文件进行编译、仿真，使用工具为VCS。

> Vim编辑器

在当前目录下使用`gvim &`命令可以打开Vim的GUI界面，同时由于NREDTree的作用，以文件树形式显示。

其中`&`的作用是在打开Vim的同时保持当前Terminal不被关闭，这样就可以在Vim中编辑的同时也在terminal中操作。



> VCS编译指令

如下所示（并不完全显示所有指令），并在右侧分别解释参数意义：

```shell
vcs	-full64				# 64位机
	-debug_access+all	# 打开调试模式,如果有错误会打印出来相关信息
	-sverilog			# 代码语言为SystemVerilog,可以兼容Verilog
	-timescale=1ns/1ps	# 指定时间精度,这样就不用在每个module分别指定,因为如果tb和module的不一致会报错,这样可以减少麻烦
	router.v			# 设计的Verilog文件
	tb.sv				# SystemVerilog编写的Testbench文件
	-top	tb			# 指定顶层模块为tb.sv
```

在使用该命令编译后，在本地会生成几个文件，其中包含可执行文件`simv`，执行该文件就可以打开VCS准备仿真，指令为：

```shell
./simv -gui &			# -gui的意思是打开图形化界面,方便查看波形
```



# 6. 设计特性

## 6.1 面向可综合设计的主要优化

- 添加接口（interface）从而将通信和协议检查进一步封装；

- 添加类似C语言的数据类型，例如int、byte；

- 添加用户自定义类型，枚举类型，结构体类型；

- 添加类型转换（<font color="red">$cast(T, S)</font>或者<font color="red">\'( )</font>）;

- 添加包（package）从而使得多个设计之间可以共享公共类型和方法；

- 添加方便的赋值操作符和运算操作符，例如++、+=、===；

- 添加**priority**和**unique case**语句；

- 添加**组合逻辑语句块always_comb、锁存逻辑语句块always_latch**和**时序逻辑always_ff**过程语句块，

  通过以上细分的方式，降低了Verilog always语句块容易混淆的方式，也使得EDA软件工具可以更准确的检查设计的实现意图。



## 6.2 过程语句块的细分

> **always_comb**：组合逻辑

- always_comb可以自动嵌入敏感列表；

- always_comb可以禁止共享变量，即赋值左侧的变量无法被另一个过程块所赋值；

- 软件工具会检查该过程块，如果其表示的不是组合逻辑，那么会发出警告：

  ```verilog
  always@(a,en)	if(en) y = a;	// Verilog
  always_comb		if(en) y = a;	// SystemVerilog
  ```

  在上例中，第一行由于缺else分支，会被综合出Latch；在第二行中，也是缺少else分支，但是由于是always_comb，所以会给出警告。

- always_comb在仿真0时刻会自动触发一次，无论在0时刻与否有敏感信号列表中的信号发生变化。

- `Verilog@*`的敏感列表声明方式不同于always_comb：

  - `@*`不要求可综合的建模要求，但always_comb则会限制其他过程块对同一变量进行赋值；
  - `@*`的敏感列表可能不完全，例如如果一个过程块调用一个函数，那么`@*`则只会将该函数的形式参数自动声明到敏感列表，而不会将该函数展开，比如一个函数没有形式参数，但内部有参数，此时敏感列表不会将内部的这些参数加进来；
  - 【重要】always_comb则将被调用函数中可能参与运算的其他信号也声明到敏感列表中。



> **always_latch**：锁存逻辑

- always_latch表示锁存逻辑，且自动插入敏感列表；

- EDA工具会检查always_llatch过程块是否真正实现了锁存逻辑：

  ```verilog
  always_latch begin		// latch the ready input
      if(!resetN)
          enable <= 1'b0;
      else if(ready)
          enable <= 1'b1;
      else if(overflow)
          enable <= 1'b0;
  end
  ```



> **always_ff**：时序逻辑

- always_ff用来表示时序逻辑；
- 敏感列表必须指明posedge或者negedge（综合要求），从而使得EDA工具实现同步或者异步的复位逻辑；
- EDA工具也会验明always_ff过程语句块是否实现了时序逻辑。



## 6.3 赋值操作符

- 基本的运算符和C是类似的，运算逻辑和Verilog是一致的。

- Verilog没有简单的放大可以对向量填充1，SV可以通过`'0`，`'1`，`'z`和`'x`来分别填充0，1，x和z。通过这种方法，代码会根据向量的宽度自动填充，这提高了代码的复用性；

- SV在比较数据时，可通过`==?`来进行通配比较；

  在比较操作的右侧操作数，如果在某些位置有X或者Z，那么它表示的是在该位置上会与左侧操作数的相同位置的任何值相匹配：

  ```verilog
  logic [7:0]	opcode;
  if(opcode ==? 8'b11011???)		// mask out low bits
      ...
  ```

  最关键和常用的主要为下表：

  |  a   |  b   | a == b  | a === b |
  | :--: | :--: | :-----: | :-----: |
  | 0000 | 0000 |  True   |  Talse  |
  | 0000 | 0001 |  False  |  False  |
  | 010Z | 0101 | unknown |  False  |
  | 010Z | 010Z | unknown |  True   |
  | 010X | 010Z | unknown |  Flase  |
  | 010X | 010X | unknown |  True   |

- SV添加了`inside`操作符，用来检查数值是否在一系列值的集合当中：

  ```verilog
  logic	[2:0]	a;
  // 下面的if语句的多个条件判断可以变得更为简化
  if((a == 3'b001) || (a == 3'b010) || (a == 3'b100)) ...
  // 可以改写为
  if(a inside {3'b001,3'b010,3'b100}) ...
  ```



## 6.4 增强的case语句

- 仿真和综合可以将case语句做不同的翻译，Verilog定义case语句在执行时按照优先级，而综合编译器则会优化case语句中多余的逻辑；
- 为了保持仿真与综合的一致性，SV提供了`unique`和`priority`的声明，结合case，casex和casez来进一步实现cases对应的硬件电路；



> **unique case**

- `unique case`要求每次case选择必须且只能满足一条case选项；

- `unique case`不能有重叠选项，即多个满足条件的选项，比如下面就会报错：

  ```verilog
  logic	[2:0]	request;
  always_comb			// compilation error below with unique case
      unique	casez(request)	// ?代表任意值,所以如果?是1时三个选项就完全重合了
          3'b1?? : slave_grant = 1;
          3'b?1? : slave_grant = 1;
          3'b??1 : slave_grant = 1;
      endcase
  ```

- `unique case`可以并行执行，并且case选项必须完备，而case是顺序执行的，这是因为case下边的分支有可能重叠，所以顺序即指定了优先级。



> **priority case**

- `priority case`表示**必须至少**有一个case选项满足要求；
- 如果有多个case选项满足时，第一个满足的分支将会被执行；
- `priority case`的逻辑同`if-else`的逻辑一致；
- `unique`和`priority`的声明也可以结合`if-else`条件语句使用；
- `unique`和`priority`使得**仿真行为同设计者希望实现的综合电路保持一致**。



# 7. 接口`interface`

## 7.1 概述

- SV在Verilog语言基础上扩展了接口(`interface`)；
- 接口提供了一种新型的**对抽象级建模的方式**；
- 接口的使用可以**简化建模和验证大型复杂的设计**；
- Verilog是通过模块之间进行端口连接来完成模块间通信的；
- 对于大型的设计，通过端口进行连接时将会让硬件集成变得非常乏味和容易出错。



> 接口的优势

- SV添加了新的抽象端口类型interface；
- interface允许多个信号被整合到一起用来表示一个单一的抽象端口；
- 多个模块因此可以使用同一个interface，继而避免分散的多个端口信号连接。

【注意】接口的使用，在语法规则上，和module几乎是一致的，怎么使用module就可以怎么使用接口。



## 7.2 接口的简单使用

> 接口的内容

- 接口不单单可以包含变量或者线网，它还可以封装模块之间通信的协议；
- 此外接口中还可以嵌入与协议有关的断言检查、功能覆盖率收集等模块；
- 接口不同于模块的地方在于，**接口不允许包含设计层次，即接口无法例化module，但是接口可以例化接口**；
- 接口中可以进一步声明modport来约束不同模块连接时的信号方向。



> 接口的声明

- 接口的定义同模块定义类似；
- 接口也可以有端口，例如外部接入的时钟或者复位信号；
- 接口内部可以声明所有的变量或者线网类型。

如下面例子所示：

```verilog
/******************* Interface Definition ****************************/
interface	main_bus(
    input	logic	clock,resetN,test_mode		// 接口也可以有端口
);
    // 接口内部可以声明变量
    wire	[15:0]	data;
    wire	[15:0]	addresss;
    logic	[ 7:0]	slave_instruction;
    logic			slave_request;
    logic			bus_grant;
    logic			slave_ready;
endinterface

/******************** Top-Level Netlist *****************************/
module top(input logic clock, resetN,test_mode);
    logic	[15:0]	program_address,jump_address;    
    logic	[7：0]  instruction,next_instruction;
    
    main_bus bus(
        .clock(clock),
        .resetN(resetN),
        .test_mode(test_mode)
    );
    ...
endmodule
```



> 接口的例化

- 接口的例化方式同模块例化一致；

- 模块的端口如果声明为input、output或者inout，那么在例化时可以不连接；

- 模块的端口如果声明为interface，那么在例化时则必须连接到一个接口实例，或者另外一个接口端口；

- 如果一个模块拥有一个接口类型端口，那么要索引该接口中的信号，需要通过以下方式进行：

  ```verilog
  // <port_name>.<internal_interface_signal_name>  即通过一个.的方式进行索引
  always @(posedge bus.clock, negedge bus.resetN)
      ...
  ```




> 接口的定义和使用

- 在接口的端口列表中只需要定义时钟、复位等公共信号，或者不定义任何端口信号，转而在变量列表中定义各个需要跟DUT和TB连接的logic变量（推荐）；
- 接口也可以依靠参数化方式提高复用性；
- 接口在例化时，同module的例化方式一样；
- 对于有对应interface的DUT和TB组件，在其例化时，也只需要传递匹配的interface变量名即可完成interface的变量传递。





## 7.3 modport

- 接口中的变量或者线网信号，对于连接到该接口的不同模块则可能具备着不同的连线方向；
- 接口引入了modport作为module port的缩写，表示不同的模块看到同一组信号时的视角（连接方向）；
- 在接口中声明modport，需要指明modport中各个信号的方向。

```verilog
// 一开始所有的信号都在interface中进行声明,但是不声明方向
interface	chip_bus(input logic clock,resetN);
    logic			interrupt_request,grant,ready;
    logic	[31:0]	address;
    wire	[63:0]	data;
    
    // 方向在modport中进行声明,下面的信号都在interface中声明过了,位宽也不需要再次声明
    // modport不可以对interface没有声明过的信号进行声明
    // modport的作用就是一点,指定下来连接到不同design的信号的方向
    modport	master(
        input	interrupt_request,		// 该信号对于master是输入
        input	address,
        output	grant,ready,
        inout	data,
        input	clock,resetN);
    
    modport	slave(
        output	interrupt_request,		// 该信号对于slave是输出
        output	address,
        input	grant,ready,
        inout	data,
        input	clock,resetN);
```

- 当一个模块在例化时，可以选择连接到interface端口中具体的某一个modport，这种方式可以降低方向连接错误的可能，进而避免信号多驱动的情况。



## 7.4 接口在验证中的应用

- 利用接口，可以将测试平台同DUT连接在一起：

  如下图所示，需要例化三个部分，分别是test、接口和arbit

  <img src="pics/4.svg" style="zoom: 140%;">

  ```verilog
  module top;
      bit	clk;
      always #50 clk = ~clk;
      
      // 例化arbit的接口
      arb_if	arbif(clk);
      // 例化arbit的design,因为使用了port,所以直接和interface对应的对接即可
      arb_with_port	a1(.grant	(arbif.grant),		// .port(ifc.signal)
                         .request	(arbif.request),
                         .rst		(arbif.rst),
                         .clk		(arbif.clk)
      				);
      // test的接口是interface,所以例化testbench的module,然后将interface传递过来即可
      test_with_ifc	t1(arbif);
      
  endmodule
  ```

  由此可知，<font color="red">对于一个TB环境的组成，需要例化三个部分，分别是DUT、Interface和Testbench</font>。



> 总结

- 接口对于设计复用非常有利；
- 接口减少了模块之间错误连接的可能性；
- 如果要添加新的信号，只需要在接口中声明，而不必在模块中声明；
- 由于接口将有关信号都集合在一起，因此在使用这些信号时需要多添加一个层次（接口实例名）；
- 接口往往会将有关的信号集合在一起，这意味着对于拥有多组不相关信号的设计而言，它可能需要有多个接口实例才能完成与其他模块的连接。



## 7.5 概念区分

>  结构体和接口之间的联系和差别有哪些地方？

- 概念的区别

  - 结构体是一种将不同类型的**数据**组合在一起的数据类型，允许用户定义一个包含多个成员变量的自定义数据类型；
  - 接口用于封装一组相关的**信号、任务和函数**，主要用于模块之间的通信，它定义了模块之间的连接规范和通信协议。

- 联系

  - 结构体和数据都提供了一种将多个相关的数据或信号组和在一起的方式。例如在一个复杂的通信模块设计中，可以使用**结构体**来**组和发送和接收的数据格式**，同时使用**接口**来**定义模块之间的通信信号**，这样可以更好的管理模块间的数据交互；

  - 在模块中的应用是可以配合的：结构体可以作为接口中信号的数据类型，或者作为模块内部存储数据的一种方式。

    例如，接口中的一个信号可以是结构体类型，这样可以方便的传递一组相关的数据，比如下例：

    ```verilog
    typedef struct {			// 在C环境下,可以在花括号前加一个名称作为结构体的真名,而花括号后则是结构体的别名
        bit	[3:0]	cmd_type;
        bit	[31:0]	address;
    } command_struct;			// 定义一个结构体类型,只不过给结构体类型定义了一个别名为command_struct
    
    struct {
        bit	[3:0]	cmd_type;
        bit	[31:0]	address;
    } command_struct;			// 定义一个结构体类型
    
    // 上边两种定义对下面的cmd的影响是一样的,command_struct都是结构体类型的名称
    interface	storage_inerface;
        logic			clk;
        command_struct	cmd;	// 接口中的信号可以是结构体类型
    endinterface
    ```

- 差别

  - 用途重点不同

    - 结构体：侧重于数据的组织和存储，是一种纯粹的数据类型，用于将不同类型的数据打包在一起，方便在模块内部进行数据处理和传递；
    - 接口：重点在于模块之间的通信，定义了模块之间的信号连接规范、通信协议和操作这些信号的方法。

  - 作用范围和可见性

    - 结构体：结构体的作用范围通常是在定义它的模块内部或者在一个特定的代码块中。它主要是作为一种本地的数据组织方式，除非通过参数传递等方式，否则外部模块一般无法直接访问结构体的成员。
    - 接口：接口的作用是跨越多个模块的，多个模块可以通过实例化接口来连接和通信。接口信号在连接的模块之间是可见的，并且可以通过接口中定义的任务和函数来操作这些信号。

  - 信号特性和功能关联

    - 结构体：结构体成员主要是数据变量，没有内置的信号特性（如时钟驱动、同步等），主要用于存储静态或动态的数据，不涉及模块间的信号驱动和同步等问题；

    - 接口中的信号具有硬件信号的特性，如时钟驱动、复位控制等。

      接口可以有端口，端口的信号有方向；接口内部信号通过modport定义方向，modport只定义**接口内**信号的**方向**，不能定义位宽。



> 模块和接口之间的相近的地方有哪些？又有哪些差别？

- 相近之处

  模块和接口在内部语法上是类似的，简单来说，模块内可写的在接口内也基本都可以；都是层次化设计的重要组成部分；

  模块和接口都具有一定的可复用性，但【注意】模块可以调用模块和接口，接口可以调用接口，但不可以调用模块。

- 差别之处

  - 功能重点不同

    - 模块用于实现具体的硬件功能，如数据处理、存储、控制等；

    - 接口则用于定义模块之间的连接规范（类型、位宽、方向等），不包含具体的功能实现逻辑。

  - 信号处理方式不同

    - 模块有自己的输入和输出端口，这些端口的信号方向是明确的；

    - 接口中的信号通常是共享的，没有严格的输入输出之分，默认双向，除非使用modport指定，主要用于模块之间的信息交互。

      例如，在一个共享内存接口中，数据信号可能由处理器模块写入，同时由内存模块读出，接口本身并不限制信号只能作为输入或输出，而是提供了一个信号的集合，供连接的模块根据具体的通信协议来使用。

  - 内部实现细节与外部可见性

    - 模块内部的实现细节在模块外部通常不可见，只有模块的端口信号对外可见，有助于保护模块的独立性和可维护性；
    - 接口的所有信号对于连接的模块来说都是可见的，并且接口通常会定义这些信号的属性和操作方式，没有像模块那样实现内部隐藏。



> 将DUT，Interface和TB在顶层连接起来时，要完成哪些步骤？（以下源自Lab1实验）

- 进入目录的directory；

- 接口文件

  - 创建SystemVerilog接口文件，在接口中声明一个由clock驱动的clocking block，把接口内的信号放到时钟块中，并指明方向；

  - 如果可以的话，对输入和输出添加skew，即考虑了时序问题：

  ```verilog
  default input #1ns output #1ns;
  ```

  - 创建一个`modport`用来连接`test program`。在`modport`的参数列表中，应该有上一步创建的时钟块参数和其他潜在的异步信号（如复位）。

- 测试程序文件`Test Program File`

  - 声明一个带有参数的`program block`，这些参数会连接到接口block中的`modport`，在program block中可以实现具体的代码。

- Create SystemVerilog Test Harness File

  - 这是测试的顶层文件，包括时钟和设计模块的例化；
  - 首先例化之前的接口（假设例化为`top_io`），并在接口的端口连接时钟等必要的信号（如果设计接口时有时钟的话）；
  - 例化测试用的`test program`，通过接口实例进行I/O的连接；
  - 通过接口和DUT进行连接，具体方式为`top_io.<port_signal>`；
  - 添加时间精度``timescale`，在initial块中指定时间格式如`$timeformat(-9,1,"ns",10)`。

- 通过VCS编译和仿真：

  ```shell
  vcs -sverilog router_test_top.sv test.sv router_io.sv ../../rtl/router.v
  ```

  编译完成后会产生`simv`文件，执行该文件可以看到在`program block`中要实现的内容。



## 7.6 clocking block

> 接口中的clocking

- 硬件和软件的连接可以通过灵活的interface来实现，也可以通过modport来进一步限定信号的传输方向，避免端口连接的错误；
- 可以在接口中声明clocking和采样的时钟信号，用来做信号的同步和采样；
- clocking块基于**时钟周期**对信号进行驱动或者采样的方式，使得tb不再苦恼于如何准确及时的对信号驱动或者采样，即$\Delta Cycle$问题，消除了信号竞争问题。



如下通过一个例子说明时钟块的简单定义：

```verilog
clocking bus @(posedge clock1);			// 指出clocking默认事件为clock1的上升沿
    default	input #10ns output #2ns;	// 这里类似于模拟硬件的建立和保持时间,并非物理意义的准确
    input	data,ready,enable;
    output	negedge	ack;
    input	#1step	addr;
endclocking
```

- 第一行定义了一个`clocking`块（名称为`bus`），由clock1的**上升沿来驱动和采样**；

- 第二行指出了在clocking块中所有的信号，

  默认情况下会在clocking事件（clock1的上升沿）的**前10ns**对其进行**输入采样**，在事件的**后2ns**对其进行**输出驱动**；

- 第三行声明了要对其采样的三个输入信号，这三个信号作为输入，它们的采样事件即**采用了默认输入事件**（clock1上升沿的前10ns）；

- 第四行声明了要驱动的**ack**信号，而驱动该事件的信号是**时钟clock1的下降沿**，即覆盖了原有的默认输出事件（clock1上升沿后的2ns）；

- 第五行的**addr**，也采用了**自身定义的采样事件，即clock1上升沿前的1step**。

  这里的**1step**会使得采样发生在clock1上升沿的上一个时间片采样区域，即可以保证采样到的数据是上一个时钟周期的数据。

- 上面的时钟块所表示的采样时序如下所示：

  <img src="pics/5.png" style="zoom:75%">

  输入信号的采样，会在时钟事件前的10ns做采样，所以，规定的"input clocking_skew"中"clocking_skew"（时钟偏移量）采取的是负值，即相对clocking事件的前10ns；而输出驱动采用的是正值，会在clocking事件后的2ns时刻做出驱动。



> clocking块的定义

- clocking块不但可以定义在interface中，也可以定义在module和program中；

- clocking中列举的信号**不是自己定义的**，而是**应该由interface或者其他声明clocking的模块定义的**；

- clocking在声明完名字之后，应该**伴随着定义默认的采样事件**，即**`default input/output event`**。

  如果没有定义，则会默认的在clocking采样事件前的1step对输入进行采样，在采样事件后的`#0`对输出进行驱动。

  其中，`#0`表示一个时钟片，相当于很多个$\Delta Cycle$，它一定大于一个$\Delta Cycle$；

- 除了定义默认的采样和驱动事件，也可以在定义信号方向时，用新的采样事件对默认事件做覆盖；

- 在interface中可以定义**多个**clocking，并且同一个变量在不同clocking中**可以**声明不同方向。



> 采样和数据驱动

- 为了避免可能的采样竞争问题，应该在验证环境的驱动环节就添加固定延迟，使得在仿真波形中更容易体现出时钟与被驱动信号之间的时序前后关系，同时这样也便于DUT的准确处理和别的准确采样；
- 如果TB在采样从DUT送出的数据，在时钟与被驱动信号之间存在$\Delta Cycle$时，应该考虑在时钟采样沿的更早时间段去模拟建立时间要求采样，这种方法也可以避免由于$\Delta Cycle$问题带来的采样竞争问题；
- 当把clocking运用到interface中，用来声明各个接口与时钟的采样和驱动关系后，可以大大提高数据驱动和采样的准确性，从根本上消除采样竞争的可能性。



# 8. 验证环境结构

## 8.1 验证环境结构

> 概述

- **测试环境（Testbench）**是整个验证系统的总称；
- 它包括验证结构中的**各个组件、组件之间的连接关系、测试平台的配置和控制**；
- 从更系统的意义来讲，它还包括**编译仿真**的流程、**结果分析**报告和**覆盖率**检查等；
- 从侠义上讲，主要关注验证平台的**结构和组件**部分，他们可以产生设计所需要的各种**输入**，也会在此基础上进行**设计功能**的检查。



> 测试平台结构图

如下图所示，是一个经典的测试平台结构图：

<img src="pics/6.svg" style="zoom:120%">

- Monitor要对Stimulator产生的给DUT的激励进行观测，可以通过DUT的端口观测，通过**接口**实现；

- DUT虽然直接和Checker相连，但是仍然要通过接口通信。

  有一个规定，即在Testbench中，**所有组件的连接都是通过接口实现**。

- 以下是几点说明：

  - 各个组件之间是相互独立的，不但是模块/组件的独立，功能也要独立；
  - 验证组件和设计之间也需要连接；
  - 验证组件之间也需要进行通信；
  - 验证环境也需要时钟和复位信号的驱动。



## 8.2 验证环境组件

> 激励发生器

- **Stimulator**（激励发生器）是验证环境的重要组件，在一些场合中，它也被称为**driver**（驱动器）、**BFM**（bus function model，总线功能模型），**behaviroal**（行为模型）或者**generator**（发生器）。
- 主要职责是模拟与DUT相邻设计的接口协议，只需要关注于如何模拟接口信号，使其能够以真实的接口协议来发送激励给DUT。
- Stimulator不应该违反协议，但**不拘束于真实的硬件行为**，还可以给出更多丰富的、只要协议允许的激励场景。
- 比真实硬件行为更丰富的激励，会使得模块级的验证更加充分，因为它不但验证过了硬件普通的接口协议情景，还模拟出更多复杂的、在更高系统级别无法产生出来的场景。
- Stimulator的**接口主要是同DUT之间连接**，此外也应该有时钟和复位的输入，确保生成的数据同DUT的接口一侧是同步的关系。
- 较精细的Stimulator还可以有**其他的配置接口用来控制接口的数据生成**，比如数据的数量、时间、长度等。
- Stimulator也可以有**存储接口数据生成历史**的功能，这可以用来在仿真运行时或者结束后查看接口数据，方便统计或者调试。
- 从Stimulator同DUT的连接关系来看，可以将其进一步分为两种：**initiator(发起器)和responder(响应器)**。



> 监测器**Monitor**

- 主要功能是用来**观察DUT的边界或者内部信号，并且经过打包整理传送给其他验证平台的组件**，例如checker（比较器）。

- 从监测信号的层次来划分monitor的功能，它们可以分为观察DUT**边界信号**和观察DUT**内部信号**：

  - **观察DUT边界信号**

    - 对于系统信号如时钟，可以监测其频率变化；

    - 对于总线信号，可以监测总线的传输类型和数据内容，以及观察总线时序是否符合协议。

  - **观察DUT内部信号**

    从灰盒验证的手段来看，往往需要探视DUT内部信号，用来指导Stimulator的激励发送，或者完成覆盖率收集，又或者完成内部功能的检查：

    - 如果没有特殊的需要，应采取灰/黑盒验证的策略（而非白盒）；

    - 观察的内部信号应尽量少，且应当是表示状态的信号。

      不建议采集中间变量信号的原因在于，这些信号的时序、逻辑甚至留存性都不稳定，这种不稳定对于验证环境的收敛是有害的。

    - 可以通过接口信息计算的，就尽量少去监测内部信号，因为这种方式也**有悖于假定设计有缺陷的验证思想**。我们观测到的内部信号在被环境采纳之前也有必要确认它们的逻辑正确性，这一要求可以通过**动态检查或断言触发**的方式来实现。

- 【易错】Monitor从interface获取数据，而不应该从Stimulator获取激励，因为interface更可靠。



> 比较器Checker

- 无论是从实现难度，还是从维护人力上来讲，checker（比较器）都应当是最需要时间投入的验证组件了。

- checker肩负了模拟**设计行为和功能检查**的任务。

- **缓存从各个monitor收集到的数据**。

- 将DUT输入接口侧的数据汇集给内置的**reference model（参考模型）**。Reference model在这里扮演了模拟硬件功能的角色，也是需要较多精力维护的部分，因为验证者需要在熟悉硬件功能的情况下实现该模型，而又不应该参考真实硬件的逻辑。

- 通过数据比较的方法，检查实际收集到的DUT输出端口数据是否同reference model产生的期望数据一致。

- 对于设计内部的关键功能模块，也有相**对应的线程进行独立的检查**。

- 在检查过程中，可以将检查成功的信息统一纳入到**检查报告**中，便于仿真后的追溯。

  如果检查失败，也可以采取暂停仿真同时报告错误信息的方式，进行在线调试。

- 以前的比较方式分为两种：

  - **线上比较（online check）**：在仿真时收集数据在线比较，并且实时报告；
  - **线下比较（offline check）**：将仿真时收集到的数据记录在文档中，在仿真结束后，通过脚本或者其他手段，进行数据比较。

  但随着设计功能愈加复杂，靠验证者每次进行繁琐检查的方式缺少可靠性。

  于是将checker添加到验证环境中，需要它分析DUT的边界激励，理解数据的输入，并且按照硬件功能来预测输出的数据内容。

  - 这种**预测**的过程，发生在reference model中，有时也将其称为predictor；

  - reference model也会**内置一些缓存**，分别存放从**DUT输入端观察到的数据**，以及经过功能转换的数据，

    同时**checker**也有其它缓存来存放**从DUT输出端采集到的数据**。

- 比较器实现建议（经验之谈，以后看）

  - 对于复杂的系统验证，倾向于集中管理stimulator和checker，因为它们两者都需要主动给出激励或者判断结果，需要较多的协调处理；
  - 而monitor则相对更独立，它只是作为监测方，将其观察的数据一字不落的交给checker即可，至于checker怎么使用，mointor并不关心；
  - 因此，stimulator和monitor是一一对应，通常将它们进一步封装在agent单元组件中，而checker则最终集群搁置在中心话的位置。



# 9. 任务和函数

## 9.1 区别

- function不会消耗**仿真**时间，而task则**可能**会消耗仿真时间。这就导致function无法调用task，而task可以调用function；
- 一个可以返回数据的function只能返回一个单一数值，而任务或者void function不会返回数值；
- 一个可以返回数据的function可以作为一个表达式中的操作数，而该操作数的值即function的返回值。



## 9.2 函数function

- 函数的首要目的在于为运算表达式提供返回值，这样既便于简化原有的代码，也便于大型代码的维护；

- `void`函数不会返回数值；

- 类似于task，函数的参数列表方向也可以声明为`input`、`output`、`inout`和`ref`：

  ```verilog
  // 以下给出函数的两种参数列表定义方式
  function	logic	[15:0]	myfunc1(int x, int y);
      ...
  endfunction
  // 返回值是logic类型,位宽定义为[15:0],返回值名与函数名同名
  function	logic	[15:0]	myfunc2;
      input	int	x;
      input 	int	y;	// 如果y是输出,这里可以写output
      ...
  endfunction
  ```

- function的返回值同C类似，也可以使用`return`返回，和函数名返回的区别在于return会立即返回，而函数名返回后会继续执行后续代码。

  ```verilog
  // 由于没有定义输出数据类型,所以默认为logic类型
  function	[15:0]	myfunc1(input [7:0]	x,y);
      myfunc1 = x*y-1;	// 方式1:使用函数名返回
  endfunction
  
  function	[15:0]	myfunc2(input [7:0]	x,y);
      return	x*y-1;		// 方式2:使用return返回
  endfunction
  
  // automatic是表示生命周期的关键字
  function automatic void myfunc3(input [7:0] x,y, 
                   				output [15:0] z);
      z = x*y-1;			// 方式3:声明了z为输出,所以无需使用上面两种方式
  endfunction
  ```

  如图所示，是两种方式的对比测试：

  <img src="pics/7.png" style="zoom:90%;"  >

  通过上图可以看到对于不同的定义方式，调用方式和返回值的赋值方式都不同。

- 如果调用具有返回值的函数，但是又不使用该返回值时，建议为其添加`void'()`进行类型转换：

  ```verilog
  void'(some_function());		// 将函数返回值类型转换为void
  ```



## 9.3 任务task

- 任务的定义可以指定参数，`input`、`output`、`inout`、`ref`皆可;

  【注意】`inout`和`ref`的区别：inout和input、output一样都会发生参数的拷贝，但是`ref`不会，它是真实的将参数引用过来。

- 任务可以消耗仿真时间；

- 任务可以调用其他任务或者函数；

- 注意`task`没有返回值，没有`void`，在task关键词和任务名之间没有关于返回值的任何东西；

- 任务没有返回值，但是任务内可以有`return`关键字，当执行到return时，task内程序的执行会直接由此结束。



## 9.4 参数传递

- input参数方向在方法调用时，属于值传递。即传递过程中，参数变量的值会经过拷贝，赋值于形式参数；
- 同样的，output、inout也会在传入或者传出的时候发生值传递的过程；
- 值传递的过程只发生在方法的调用时和返回时；
- `ref`参数在传递时不会发生值拷贝，而是将变量“指针”传递到方法中，在方法内部对该参数的操作将会同时影响外部变量；
- 如果为了避免外部传入的ref参数会被方法修改，则可以添加`const`修饰符，来表示变量是只读变量；
- 如果端口没有定义变量默认值，在调用时必须赋值，赋值方式和module的端口例化连接类似。如果有默认值，可以不给值，赋值会采用默认值。
- 函数和任务的参数列表都可以为空，函数和任务的参数可以有默认值。
- 当在一个module内，不同的线程同时调用同一个task时，要在task之后添加`automatic`关键字，这样才能为两个子线程开辟两个内存空间。



# 10. 定长数组

## 10.1 分类

数组分为组合型数组和非组合型数组两种，在Verilog中常见的数组就是非组合型数组。

### 10.1.1 非组合型（unpacked）

- 对于Verilog，数组常会被用来做数据存储，例如：

  ```verilog
  reg	[15:0]	RAM	[0:4095];		// memory array
  ```

  SV将Verilog这种声明数组的方式称之为**非组合型声明**，即数组中的成员之间存储数据都是互相独立的；

- Verilog也不会指定软件去如何存储数组中的成员：

  ```verilog
  wire [7:0]	table [3:0];
  ```

  非组合型数组的数据组织方式如下图所示：

  <img src="pics/9.svg" style="zoom:150%">

- SV保留了非组合型的数组声明方式，并且扩展了允许的类型，包括`event`,`logic`,`bit`,`byte`,`int`,`longint`,`shortreal`和`real`类型；

- SV也保留了Verilog索引非组合型数组或者数组片段的能力，这种方式为数组以及数组片段的拷贝带来了方便：

  ```verilog
  int a1	[7:0][1023:0];	// unpacked array
  int a2	[1:8][1:1024];	// unpacked array
  a2 = a1;				// copy an entire array
  a2[3] = a1[0];			// copy a slice of an array
  ```

  在上面以`a2`为例，从左到右对应维度从高到低，即`[1:8]`的是数组的高维度，`[1:1024]`是数组的低维度。

  高维度在该例的意思就是数组中有8个数据，低维度在这里意思是每个数据是1024位。

- 声明**非组合型**数组的方式，以下两种皆可：

  ```verilog
  logic	[31:0]	data	[1024];
  logic	[31:0]	data	[0:1023];
  ```




### 10.1.2 组合型（packed）

- SV将Verilog的向量作为组合型数组声明方式

  ```verilog
  wire [3:0]	select;
  reg  [63:0]	data;		// 64-bit packed array
  ```

- SV也进一步允许**多维**组合型数组的声明

  ```verilog
  logic [3:0][7:0] data;		// 2-D packed array ： 4*8bit
  ```

  注意组合型与非组合型数组的区别，组合型数组**维度**的声明在数组名的**左边**，而非组合型的在右边。

  但与非组合型的共同点仍然是：数组定义从左到右对应维度的从高到低。

- 组合型数组会进一步规范数据的存储方式，而不需要关心编译器或者操作系统的区别

  <img src="pics/8.svg" style="zoom:170%">

- 关于存储所占空间的说明

  这里声明两种定义方式：

  ```verilog
  logic	[3:0][7:0]	data1;		// 占用2个Word空间
  bit		[3:0][7:0]	data2;		// 占用1个Word空间
  ```

  这里存储空间通过`Word`（字）来衡量。在软件中，一个字是32位。对于data2，总位数是4*8=32位，由于`bit`是二值类型，所以需要一个字的空间。

  对于data1而言，看似也是32位，但是`logic`是四值类型，相比于`bit`类型还多了`x,z`，因此软件对其建模还需要多一倍的空间，即两个字的空间。

  因此总结来说，同样位宽的定义下，四值类型需要的存储空间是二值类型的一倍。



> 组合型的其他应用

- 组合型除了可以运用的数组声明，也可以用来定义结构体的存储方式：

  ```verilog
  // 声明一个结构体类型data_word
  typedef struct packed {
      logic [7:0]	 crc;
      logic [63:0] data;
  } data_word;
  // 结构体类型变量
  data_word [7:0] darray;		// darray是一个72*8的数组
  ```

  **注意添加了`packed`关键字，这样数据就是连续存储**，就可以**方便寻址**了，相当于定义了`logic [71:0] vec2`这样一个72位的向量。

- 组合型数组和其他数组片段也可以灵活选择，用来拷贝和赋值等：

  ```verilog
  logic [3:0][7:0]	data;			// 2-D packed array
  wire  [31:0]	   	out = data;		// whole array,由于都是32bit,所以可以直接赋值
  wire   sign = data[3][7];			// bit-select
  wire  [3:0] nib = data[0][3:0];		// part-select:取出第0个数据的低四位
  
  byte 	high_byte;
  assign	hign_byte = data[3];		// 8-bit slice
  
  logic	[15:0]	word;
  assign	word = data[1:0];			// 取出data的第0、1个数据,合起来是16bit,所以可以赋给word
  ```

  

  > 复杂多维数组的识别方法

  对于一个比较复杂的多维数组的定义，识别其维度和宽度的方式和下面类似，先看右边（在内部后先看左边，再看右边），再看左边：
  $$
  \rm
  int\quad
  \overbrace{
  \underbrace{[1:0]}_{step\ 5}
  \underbrace{[2:0]}_{step\ 6}
  }^{step\ 4}
  \quad array \quad
  \overbrace{
  \underbrace{[3:0]}_{step\ 2}\underbrace{[4:0]}_{step\ 3}
  }^{step\ 1}\quad;
  $$
  因此该数组是一个$(4\times5)\times(2\times4)$维度的数组。



## 10.2 数组操作

### 10.2.1 初始化

> 组合型数组

组合型数组的初始化同向量初始化一致：

```verilog
logic	[3:0][7:0]	a = 32'h0;			// vector assignment
logic	[3:0][7:0]	b = {16'hz,16'h0};	// concatenate operator
logic	[3:0][7:0]	c = {16{2'b01}};	// replicate operator
```



> 非组合型数组

- 非组合型数组初始化时，需要通过**`'{}`**来对数组的**每一个维度**进行赋值，一定注意赋值不要忘记**单引号**。

  ```verilog
  // 这里数据位宽没有明确写出,默认与int一致(32bit),数组维度为2*4
  int	d [0:1][0:3] = '{'{7,3,0,5}, '{2,0,1,6}};
  // d[0][0] = 7, d[0][1] = 3, d[0][2] = 0, d[0][3] = 5
  // d[1][0] = 2, d[1][1] = 0, d[1][2] = 1, d[1][3] = 6
  ```

- 非组合型数组在初始化时，也可以类似结构体初始化，通过**`'{}'`**和**`default`**关键字即可以完成

  ```verilog
  int a1	[0:7][0:1023] = '{default : 8'h55}';
  ```

  上面将数组的8*1024个元素全部赋为默认值`8'h55`。



### 10.2.2 赋值

> 组合型（packed）数组

```verilog
// 组合型数组的赋值相比非组合型要简单很多
logic [1:0][1:0][7:0] a;		// 3-D packed array,维度2*2*8
a[1][1][0] = 1'b0;				// assign to one bit
a = 32'hF1A3C5E7;				// assign to full array
a[1][0][3:0] = 4'hF;			// assign to a part slice
a[0] = 16'hFACE;				// assign to a slice
a = {16'bz,16'b0};				// assign concatenation
```



> 非组合型（unpacked）数组

非组合型数组的数据成员或者数组本身均可以为其赋值：

```verilog
byte a [0:3][0:3];
a[1][0] = 8'h5;	// assign to one element
a[3] = '{'hF, 'hA, 'hC, 'hE};	// assign list of value to slice of the array
```

其中赋值的`a[3]`指数组高维度的部分：
$$
\rm 
byte\quad a \underbrace{[0:3]}_{a[3] = '\{'hF,\ 'hA,\ 'hC,\ 'hE\};}[0:3];
$$
【注意】非组合型数组不可以使用一个向量进行整体赋值，因为向量和组合型数组背后的存储逻辑都是连续存储，而非组合型数组是不连续存储的。



### 10.2.3 拷贝

> 组合型数组

- 对于组合型数组，由于数组会被视为向量，因此当赋值左右两侧操作数的大小和维度不相同时，也可以做赋值；

- 如果当**尺寸**不相同时，则会通过**截取或者扩展**右侧操作数的方式来对左侧操作数赋值：

  ```verilog
  bit   [1:0][15:0]	a;		// 32-bit 2-state vector
  logic [3:0][7:0]	b;		// 32-bit 4-state vector
  logic [15:0]		c;		// 16-bit 4-state vector
  logic [39:0]		d;		// 40-bit 4-state vector
  b = a;		// assign 32-bit array to 32-bit array, no problem
  c = a;		// 高16bit会被截掉丢弃
  d = a;		// 高8位会进行补零填充
  ```

  

> 非组合型数组

- 对于非组合型数组，在发生数组间拷贝时，则要求**左右两侧操作数的维度和大小必须严格一致**；

  ```verilog
  logic	[31:0]	a	[2:0][9:0];
  logic	[0:31]	b	[1:3][1:10];
  assign a = b;		// assign unpacked arrary to unpacked array with the same dimension
  ```

- 非组合型数组无法直接赋值给组合型数组，同样，组合型数组也无法直接赋值给非组合型数组。



### 10.2.4 foreach循环结构

- SV添加`foreach`循环来对一维或者多维数组进行循环索引，而不需要指定该数组的维度大小：

  ```verilog
  int	sum	[1:8][1:3];
  foreach( sum[i,j] )
      sum[i][j] = i + j;		// initialize array
  ```

- `foreach`循环结构中的变量**无需声明**；

- `foreach`循环结构中的变量是只读的（只能访问，不能修改），其**作用域只在此循环结构中**。



### 10.2.5 系统函数

这里的系统函数是SV中针对数组操作的。

- **`$dimensions(array_name)`**用来返回数组的**维度**；

- **`$left(array_name,dimension)`**返回指定维度的最左索引值（MSB）：

  ```verilog
  logic	[1:2][7:0]	word	[0:3][4:1];
  // $left(word, 1) will return 0
  // $left(word, 2) will return 4
  // $left(word, 3) will return 1
  // $left(word, 4) will return 7
  ```

  注意第二个参数1，2，3，4代指的是数组的维度$D_i$：
  $$
  \rm
  logic\quad
  \underbrace{[1:2]}_{D_3}
  \underbrace{[7:0]}_{D_4}
  \quad word\quad
  \underbrace{[0:3]}_{D_1}
  \underbrace{[4:1]}_{D_2}
  ;
  $$

- 与**`left()`**类似的，还有`${right, low, high}(array_name, dimension)`；

- **`$size(array_name,dimension)`**可以返回指定维度的尺寸大小（**常用**）；

  该系统函数对于定长数组或者动态数组、队列、关联数组都可用，但函数<font color="red">**`size()`**方法对于定长数组不可用</font>。

- **`$increment(array_name, dimension)`**，如果指定维度的最左索引值大于或等于最右索引值，则返回`1`，否则返回`-1`；

- **`$bits(expression)`**可以用来返回数组存储的比特数目：

  ```verilog
  wire	[3:0][7:0]	a	[0:15];
  $bits(a); 			// 返回512
  
  struct packed {
      byte 			tag;
      logic	[31:0]	addr;
  } b;
  $bits(b);			// 返回40
  ```

  

# 11. 动态数组

- 与之前的定长数组相比，SV还提供了才可以重新确定大小的动态数组；

- 动态数组在声明时需要使用**`[]`**，这表示不会在编译时为其指定尺寸，而是在仿真运行时来确定；

- 动态数组一开始为空，而需要使用**`new[]`**来为其分配空间，注意这里是**中括号`[]`**，和之后的类与方法中使用的**`new()`**的圆括号要区分开。

  ```verilog
  int	dyn[],d2[];				// Declare dynamic arrays
  
  initial begin 
      dyn = new[5];			// Allocate 5 elements
      foreach(dyn[j])			// Initialize the elements
          dyn[j] = j;
      d2 = dyn;				// Copy a dynamic array
      d2[0] = 5;				// Modify the copy
      $display(dyn[0],d2[0]);	// See both values (0 & 5)
      dyn = new[20](dyn);		// 分配20个数据并且将dyn的原有数据复制后放到新分配的数据前端
      dyn = new[100];			// Allocate 100 new ints, Old values are lost.
      dyn.delete();			// Way1 to Delete all elements
      dyn = '{};				// Way2 to Delete all elements
      dyn = new[0];			// Way3 to Delete all elements
  end
  ```

- 内建方法**`size()`**可以返回动态数组的大小；

- 内建方法**`delete()`**可以清空动态数组，使其尺寸变为0；

- 动态数组在声明时也可以完成其初始化：

  ```verilog
  bit [7:0]	mask [] = {8'b0000_0000, 8'b0000_0001, 8'b0000_0011,
                         8'b0000_0111, 8'b0000_1111, 8'b0001_1111,
                         8'b0011_1111, 8'b0111_1111, 8'b1111_1111};
  ```



# 12. 队列

> 基本内容

- SV引入了队列类型，它结合了数组和链表；
- 可以在队列的**任何位置**添加或者删除数据成员；
- 可以通过**索引**来访问队列的任何一个成员；
- 通过**`[$]`**来声明队列，队列的索引值从`0`到`$`；
- **（重要）**可以通过内建方法**`push_back(val)`**、**` push_front(val)`**、**` pop_back()`**和**`pop_front()`**来顺序添加或者移出并且获得数据成员；
- 可以通过**`insert(pos, val)`**来在指定位置pos插入数据成员val；
- 可以通过**`delete()`**来删除所有数据成员。



> 示例说明

```verilog
int j = 1,
	q2[$] = {3,4},		// 队列的赋值不能使用单引号'
	q[$] = {0,2,3};		// 赋值

initial begin 
    q.insert(1,j);		// 在索引为1处插入元素j,注意队列的下标从0开始,因此插入后队列变为 {0,1,2,3}
    q.delete(1);		// 删除索引为1的元素,删除后队列变为 {0,2,3}
    
    // These operations are fast
    q.push_front(6);	// 从队列的前端插入6,队列变为 {6,0,2,3}
    j = q.pop_back();	// 取出队列的最后边的一个数据,队列变为 {6,0,2}, j=3
    q.push_back(8);		// 向队列的尾部插入8,队列变为 {6,0,2,8}
    j = q.pop_front();	// 取出队列头部的数据,队列变为 {0,2,8}, j=6
    foreach(q[i])
        $display(q[i]);	// 打印整个队列
    q.delete();			// 删除队列,即清空,此时队列变为 {}
end
```

注意队列的**`delete()`**方法与动态数组不同，队列可以传参删除指定索引的数据，但是动态数组的delete方法不能传任何参数。



如果想对两个队列进行互相操作，使用拼接运算符，不易出错。另外，队列的`insert()`方法不支持直接插入队列。

```verilog
int j = 1, q2[$] = {3,4}, q[$] = {0,2,5};
// 下面使用了拼接运算符来实现队列的删除、插入和拼接等操作.
initial begin 
    q = {q[0], j, q[1:$]};		// {0, 1, 2,5}  	Insert 1 before 2
    q = {q[0:2], q2, q[3:$]};	// {0,1,2, 3,4, 5}	Insert queue in q, 实际就是3个队列的拼接
    q = {q[0], q[2:$]};			// {0, 2,3,4,5}		删除索引为1的数据
    
    // These operations are fast.
    q = {6,q};					// {6, 0,2,3,4,5}	Insert at front
    
    j = q[$];					// j = 5			pop_back()
    q = q[0:$-1];				// {6,0,2,3,4}		equivalent
    
    q = {q,8};					// {6,0,2,3,4,8}	Insert at back
    
    j = q[0];					// j = 6			pop_front()
    q = q[1:$];					// {0,2,3,4,8}		equivalent
    
    q = {};						// { }				Delete contents
end
```



# 13. 关联数组

- 关联数组（Associate Array），也称为Hash，或者类似Python中的字典；
- 关联数组的索引除了整型以外，还可以为字符串或其他任何类型，散列存储的数据成员也可以为任意类型且不连续。

```verilog
module tb;
    initial begin 
        bit	[31:0]	mem	[int unsigned];
        int	unsigned	data, addr;
        
        repeat(5)begin 
            // 随机化约束,限制addr[31:8]和addr[1:0]为0,且data在1~10范围内.
            std::randomize(addr,data) with {addr[31:8] == 0; addr[1:0] == 0; data inside {[1:10]}; };
            $display("address : 'h%0x, data : 'h%0x", addr, data);
            mem[addr] = data;
            
            foreach(mem[idx])
                $display("mem address : 'h%0x, data : 'h%0x", idx, mem[idx]);
        end
    end
endmodule
```

如果直接访问一个关联数组中并不存在的索引，可能也不会进行报错，这样相当于间接的告诉数组可能会有这样一个索引空间存在。

下面是分别通过`foreach`和`first`、`next`方法来对关联数组进行索引遍历的实例：

```verilog
byte	assoc[byte], idx = 1;

initial begin 
	// Initialize widely scatered value
    do begin 
        assoc[idx] = idx;	// idx = 8'b0000_0001
        idx = idx << 1;		// idx是8位的,每次左移,这样assoc中就是存储了离散的索引值:1,2,4,6,8,16,32,64
    end while (idx != 0);
    
    // 通过forech遍历所有的索引值
    foreach(assoc[i])
        $display("assoc[%h] = %h", i, assoc[i]);
    
    // 通过函数方法来遍历所有的索引值
    if(assoc.first(idx))		// Get first index
        do
            $display("assoc[%h] = %h", idx, assoc[idx]);
        while(assoc.next(idx));	// Get next index
    
    // Find and delete the first element
    void'(assoc.first(idx));
    void'(assoc.delete(idx));		// 和队列类似,关联数组也可以指定idx删除
    $display("The array now has %d elements", assoc.num());
end
```



# 14. 数组的方法

## 14.1 缩减方法

- 基本的数组缩减方法是把一个数组缩减成一个值；

- 最常用的缩减方法是`sum`，它对数组中的所有元素求和：

  ```verilog
  byte b[$] = {2,3,4,5};
  int	 w;
  w = b.sum();		// 14  = 2+3+4+5
  w = b.product();	// 120 = 2*3*4*5
  w = b.and();		// 0000_0000 = 2 & 3 & 4 & 5;
  ```

- 其他的数组缩减方法还有`product`（积）、`and`（与），`or`（或）和`xor`（异或）；



## 14.2 定位方法

- 对于非合并数组，可以使用数组定位方法，其**返回值将是一个队列**而非一个数据成员：

  ```verilog
  int	f[6] = '{1,6,2,6,8,6};		// Fixed-size array
  int	d[]  = '{2,4,6,8,10};		// Dynamic array
  int	q[$] = 	{1,3,5,7},tq[$];	// Queue
  
  tq = q.min();		// {1}
  tq = d.max();		// {10}
  tq = f.unique();	// {1,6,2,8}
  ```

- 使用`foreach`也可以实现数组的搜索，但使用`find...with...`查找满足条件的数据成员时，更为方便。

  ```verilog
  int d[] = '{9,1,8,3,4,4}, tq[$];
  
  // Find all elements greater than 3
  tq = d.find with (item > 3);		// {9,8,4,4}
  // Equivalent code
  tq.delete();
  foreach(d[i])
      if(d[i] > 3)
          tq.push_back(d[i]);
  
  tq = d.find_index with (item > 3);			// {0,2,4,5}  这里存疑,待测试
  tq = d.find_first with (item > 99);			// {} - none found
  tq = d.find_first_index with (item == 8);	// {2} d[2]=8
  tq = d.find_last_with (item == 4);			// {4}
  tq = d.find_last_index with (item == 4);	// {5} d[5]=4
  ```

  

## 14.3 排序方法

可以通过排序方法改变数组中元素的顺序，可以对它们进行逆向、乱序等排列：

```verilog
int d[] = 		   '{9,1,8,3,4,4};

d.reverse();	// '{4,4,3,8,1,9}	将原数组倒序排列
d.sort();		// '{1,3,4,4,8,9}	将数组中的元素按照从小到大的顺序排列
d.rsort();		// '{9,8,4,4,3,1}	将数组中的元素按照从大到小的顺序排列
d.shuffle();	// '{9,4,3,8,1,4}	将数组中的元素乱序排列
```



# 15. 类（Class）

以下内容从类的三要素分别介绍，类的三要素包括：**类的封装、类的继承和类的多态（虚方法）**。



## 15.1 类的封装

### 15.1.1 类的概述

- 类是一种可以包含数据和方法（function,task）的类型；

  例如一个数据包，可能被定义为一个类，类中可以包含指令、地址、队列ID、时间戳和数据等成员。

- `packet`这个类可以在其中对这些数据做初始化，设置指令，读取该类的状态以及检查队列ID；

- 每一个`packet`类例化的具体对象，其数据成员都可能不相同，

  然而`packet`类作为描述这些数据的抽象类型，将其对应的数据成员和操作这些数据成员的方法都定义在其中。

```verilog
class Packet;
    // data or class properties
    // 这部分定义数据
    bit	[3:0]	command;
    bit	[40:0]	address;
    bit	[4:0]	master_id;
    integer		time_requested;
   	integer		time_issued;
    integer		status;
    
    typedef	enum {ERR_OVERFLOW=10,ERR_UNDERFLOW=1123}	PACKET_TYPE;
    const	integer	buffer_size = 100;
    const	integer	header_size;
   
    // 这部分定义了类的方法，由函数和任务组成
    // initialization
    function new();
        command = 4'd0;
        address = 41'b0;
        master_id   = 5'bx;
        header_size = 10;
    endfunction
    
    // methods
    // public access entry points
    task clean();
        command = 0;
        address = 0;
        master_id = 5'bx;
    endtask
    
    task issue_request(int delay);
        // send request to bus
    endtask
    
    function	integer	current_status();
        current_status = status;
    endfunction
    
endclass
```



> OOP术语

- 类（class）：包含成员变量和成员方法；
- 对象（object）：类在例化后的实例；
- 句柄（handle）：指向对象的指针；
- 原型（prototype）：程序的声明部分，包含程序名、返回类型和参数列表。



### 15.1.2 构建函数

- SV采用了像Java一样空间自动开辟和回收的手段；

  因此SV的类在定义时，只需要定义构建函数（constructor），而不需要定义析构函数（destructor）；

- 类在定义时，需要定义构建函数，如果未定义，则系统会自动帮助定义一个空的构建函数（没有形式参数，函数体亦为空）。

- 对象在创建时，需要先声明再例化，同时进行亦可：

  ```verilog
  class Packet;
      integer	command;
      function new();
          command = IDLE;
      endfunction
  endclass
  
  Packet	p = new;
  ```

  - 注意与module例化不同，上例的`p`不是类的例化，它称为**句柄**，只有使用了`new()`函数才会**开辟空间**存放变量、数据、method等，**才能访问类内部**。

  - 开辟空间在C语言中称为构建函数。另外，注意上边类中的函数**没有返回值**，连`void`都没有。

  - 当创建了实际的对象以后，对象头部的指针会被交给`new()`函数作为返回值，该返回值就是创建的对象的头部的指针/句柄。

  - 当句柄没有赋值时，其值为`null`，表示句柄为空。

  在如下一段代码，分别解释类和模块、结构体在执行上的差异：

  ```verilog
  class packet_c;			// 定义类
      integer command;
  endclass
  
  module packet_m ();		// 定义模块
      integer command;
  
  endmodule
  
  typedef struct {		// 定义结构体类型
      integer command;
  } packet_s;
  
  module tb;
      packet_m    m1();		// 模块例化（第一次）
      packet_m    m2();		// 模块例化（第二次）
  
      packet_c    c1 = new();		// 类的例化（第一次）
  
      packet_s    s1 = '{1};		// 结构体例化（第一次）
      
      initial begin : int_proc1
          packet_s s2 = '{2};		// 结构体例化（第二次）
          packet_c c2 = new();	// 类的例化（第二次）
      end
  
  endmodule
  ```

  在**仅编译、没有run**的情况下，**结构体变量**s1和s2都被进行了**静态赋值**，而类虽然使用了new()进行实例化，但此时c1、c2都是空的`null`：

  <img src="pics/10.png">

  在这里，**静态的意思是在仿真开始之前，已经为其分配好了空间**（至于是什么值并不关心）。

  【特别要注意】上边的`c1,c2`无论是在tb还是initial的域中，在仿真没有开始时，它的value都是`null`，因此**由类创建的对象一定是动态的**，对象一定是在仿真开始后才会创建，体现在上图就是仿真前一直为空`null`，仿真开始后（下图）句柄才会指向具体的对象。

  而在运行后（即使执行`run 0ns`），c1和c2也会由空类型变为实例化的**对象**：

  <img src="pics/11.png">

  其中，对于c1的value而言，`@packet_c@1`的`@packet_c`表示c1这个句柄指向的对象是`packet_c`这个类，`@1`表示例化的第一个实例。

  
  
  当对类进行了**多次例化**时，如下所示：
  
  ```verilog
  // 顺延上一部分代码,修改类并添加参数
  class packet_c;
      function new(int init_val = -1);
          integer command = init_val;
      endfunction
  endclass
  
  module tb2;
      initial begin
          packet_c c1;
          for(int i=1;i<=100;i++)begin	// 例化100次
              c1 = new(i);
        end
      end
endmodule
  ```

  由于多次例化的对象都是c1，所以c1的句柄只会指向最后一次例化的对象，（不同于C/C++）对于SV，之前例化的对象都会被自动丢弃：

  <img src="pics/12.png">

  通过上图可知packet_c这个类被例化了100次，在QuestaSim的菜单栏`View`下选择如下选项可以将指定的对象已有的实例化全部添加进来观察：
  
  <img src="pics/13.png">
  
  这样，通过在对应类的例化代码旁边添加`breakpoint`就可以观察到每次添加到工作区的类的变化情况。
  
  
  
- 对象的分辨

  在下例中，是只有一个对象的，**使用了几个`new()`就有几个对象**，并且由于`new()`的使用，才使得c1可以访问类内部的方法/变量。

  ```verilog
  module tb3;
      packet_c c1,c2;
      initial begin :init_proc
          c1 = new(10);		// 使用new()开辟了空间
          $display("c1.command = %0d",c1.command);	// 开辟了空间才可以访问内部方法/变量
          c2 = c1;			// 赋值的是句柄/指针,没有创建新的对象,所以c2和c1指向同一个对象
          $display("c2.command = %0d",c2.command);
      end
  endmodule
  ```



### 15.1.3 静态和动态

对于类而言，默认其中所有的变量和方法都是**动态**的，即只有在开始仿真后才会有值。静态则是指在仿真开始之前就有初始值了。

在类中，通过添加`static`关键字，可以将变量/方法声明为静态，如：

```verilog
class packet_c;
    int			address;
    static	int	data = 1;
endclass
```

这样即使仿真没有开始，data也会有默认值，由于是`int`（二值类型），所以默认值为`32'h0`。

在上面定义的前提下，在外部可以通过一定的方法来访问该静态变量：

```verilog
module tb3;
    packet_c c1;		// 声明类
    
    initial begin : init_proc
        // 访问方式1:通过双冒号“：：”
        $display("packet_c status data is %0d", packet_c::data);	
        // 访问方式2
        $display("c1.data = %0d", c1.data);
    end
    
endmodule
```

- 双冒号`::`的访问方式，它表示的是**域**的概念，即在`packet_c`这个域下索引该变量；

- 第二种访问方式即使没有通过`new()`进行空间分配，也可以访问静态变量`data`，因为该变量可以认为不在对象而是在这个类中，它在固定的内存中，在仿真前中后的任何一个阶段都不会被释放。此时在第二行定义的`c1`的value仍然为`null`，但不影响静态变量`data`的值已经存在且可以访问。

- `data`真正的存放空间不在任何一个对象中，如果在对象中，它存放的空间就是各自独立的了。所以大家是在共享这个变量。

- 对于第8行的具体的说明：

  在编译的过程中，c1的句柄指向packet_c这个类，而data是静态变量，在接下来的仿真过程中找data这个变量时，不管c1有没有指向对象（并不care），因为真正要找到的变量放在这个类中。此时是否例化对象没有关系，因为这个变量不在任何一个对象中，而是在类的这个域中。



### 15.1.4 this

- `this`是用来明确索引**当前所在对象**的成员（变量/参数/方法）；

- `this`只可以用来在类的**非静态成员**、约束和覆盖组中使用；

- `this`的使用可以明确所指向变量的作用域：

  ```verilog
  class Demo;
      integer	x;
      function new (integer x);
          this.x = x;
      endfunction
  endclass
  ```

  如果没有使用`this`，可能会不明确第4行的`x`到底指向的是函数参数中的x还是这个类的成员变量x；

  事实上，对`x`的检索也是就近原则，所以没有`this`时，`x`都会指向函数形式参数x；

  而通过`this.x`后，就指定了这个`x`只能指向当前类的非静态成员（即第2行定义的）`x`，此外也可以使得代码意思更明确。

  如果函数的形式参数是y，那此时无论是否使用`this`都不会有区分指向的问题，所以不适用`this`也是完全可以的，但使用了会更清晰，方便阅读。

  ```verilog
  class Demo;
      integer	x;
      function new (integer y);
          this.x = y;		// 形式参数为y
      endfunction
  endclass
  ```



### 15.1.5 赋值和拷贝

- 声明变量（句柄）和创建对象是两个过程，也可以一步完成；

  ```verilog
  packet p1;
  p1 = new();
  ```

- 如果将p1赋值给另外一个变量p2，那么依然只有一个对象，只是指向这个对象的句柄有p1和p2；

  如果想让p2也创建一个新的对象，对p2使用`new()`即可，在下边说明。

- 以下这种方式表示p1和p2代表两个不同的对象：

  在创建p2时，将从p1**拷贝其成员变量**例如integer、string和句柄等，该种拷贝方式称为**浅拷贝**（shallow copy）：

  ```verilog
  Packet p1;
  Packet p2;
  p1 = new();		// 加不加括号都可以
  p2 = new p1;	// 在创建p2时同时将p1拷贝到p2
  ```

  > 浅拷贝存在的问题

  浅拷贝时，拷贝除句柄以外的都是没什么问题的，但对于句柄而言，拷贝的新句柄和之前的句柄指向的是同一个对象。

  浅拷贝时，对于句柄的拷贝就是句柄的值，对象并不会增加，也就是没有改变指针的指向。

  对于深拷贝而言，不仅仅拷贝句柄的值，还会拷贝句柄所指向的对象，相当于重新创建了一个对象，所以深拷贝时，两边的空间是完全独立隔开的。

  <img src="pics/14.svg" style="zoom:150%">

  上图大致是深浅拷贝的对比，将上边的拷贝到下边，浅拷贝拷贝指针的值，但指向的还是原对象，深拷贝则会拷贝（创建）一个新的对象并指向它。

  如果不使用`new()`进行浅拷贝而又要进行拷贝，则使用`p_nxt.<var/method> = p_cur.<var/method>`即可，将要拷贝的内容逐个列出即可。

  【注意】如果要进行深拷贝，必须要写自定义函数来进行，对于SV没有别的方法。



### 15.1.6 数据的隐藏和封装

> 数据的隐藏

- 类的成员（变量/方法）默认情况下，即是公共属性的，表示对于类自身和外部均可以访问该成员；

- 对于成员的限定，如果使用`local`，则**只有该类**可以访问此成员，而**子类或者外部均无法访问**；

- 对于成员的限定，如果使用`protected`，则表示该类和其子类（即继承）均可以访问此成员，而外部无法访问。

  ```verilog
  class Packet;
      local 	integer i;
      function integer compare(Packet other);
          compare = (this.i == other.i);
      endfunction
  endclass
  ```

  在刚开始学习时，不要进行数据的隐藏，会导致不必要的麻烦。
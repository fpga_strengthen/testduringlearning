/*========================================================================================*\
 Filename : syncCRC.v
 Author : leexiao
 time ：2016.08.17
 Description : 此模块主要完成报文同步
               1.输入的待同步的11bit字，同时计算CRC76和CRC12；
               2.当CRC76正确时，输出报文同步码。
               将计算的同步码发送给同步模块shift_bit.v，进行报文移位形成正确顺序的报文。
 
 Called by : 15001052282
 Revision History :
 Email : 
 Company :

\*========================================================================================*/
module SyncCRC(
                i_clk_81M,
                rst_n,
 
                i_data_fsk, //11bit数据输入
                i_data_sck, //i_data_fsk同步时钟，一个11bit有一个i_data_sck与之对应
                i_data_start, //一帧数据开始指示位
 
                o_SyncCode_clk, //报文同步后的同步时钟
                ov_SyncCode //同步后的报文
               );
input i_clk_81M;
input rst_n;

input [10:0]i_data_fsk;
input i_data_sck;
input i_data_start;

output reg o_SyncCode_clk;
output wire[10:0]ov_SyncCode;
//------------------------------------------------------------------------------//
reg [10:0]i_data_fsk_r;
reg i_data_sck_r;
reg i_data_sck_p;
reg [9:0] lfsr_syn;
reg [74:0] lfsr_CRC;
reg [6:0] countbyte;
reg CRC76_OK;
reg CRC76_OK_r;
reg CRC76_OK_p; 
reg [9:0]SyncCode_address;
//------------------------------------------------------------------------------//
//输入数据缓存到本地
always @(negedge rst_n or negedge i_clk_81M)
begin
    if (!rst_n)
        i_data_fsk_r <= 11'b0;
    else 
        i_data_fsk_r <= i_data_fsk;
end
    
//对i_data_sck取沿
always @(negedge rst_n or negedge i_clk_81M)
begin
    if (!rst_n)  begin
        i_data_sck_r <= 1'b0;
        i_data_sck_p <= 1'b0;
    end
    else begin
        i_data_sck_r <= i_data_sck;
        i_data_sck_p <= i_data_sck && (~i_data_sck_r);
    end
end

//----------------------------------同步--------------------------------------------//
//对输入数据进行计算，最后的计算结果得到查表地址，用于查找偏移量
always @(negedge rst_n or negedge i_clk_81M)
begin
    if (!rst_n)
        lfsr_syn<=10'b0;
    else if (i_data_start)//一帧开始时清空重新开始计算
        lfsr_syn<=10'b0;
    else if( i_data_sck_p ) begin //每来一个11bit进行一次计算
        lfsr_syn[9] <= i_data_fsk_r[9] ^i_data_fsk_r[10] ^lfsr_syn[0] ^lfsr_syn[6] ^lfsr_syn[8] ;
        lfsr_syn[8] <= i_data_fsk_r[8] ^lfsr_syn[0] ^lfsr_syn[5] ^lfsr_syn[6] ^lfsr_syn[7] ^lfsr_syn[8] ;
        lfsr_syn[7] <= i_data_fsk_r[7] ^i_data_fsk_r[10] ^lfsr_syn[4] ^lfsr_syn[5] ^lfsr_syn[6] ^lfsr_syn[7] ^lfsr_syn[9] ;
        lfsr_syn[6] <= i_data_fsk_r[6] ^i_data_fsk_r[10] ^lfsr_syn[0] ^lfsr_syn[3] ^lfsr_syn[4] ^lfsr_syn[5] ;
        lfsr_syn[5] <= i_data_fsk_r[5] ^lfsr_syn[0] ^lfsr_syn[2] ^lfsr_syn[3] ^lfsr_syn[4] ^lfsr_syn[6] ^lfsr_syn[8] ^lfsr_syn[9];
        lfsr_syn[4] <= i_data_fsk_r[4] ^i_data_fsk_r[10] ^lfsr_syn[1] ^lfsr_syn[2] ^lfsr_syn[3] ^lfsr_syn[5] ^lfsr_syn[7] ^lfsr_syn[8] ;
        lfsr_syn[3] <= i_data_fsk_r[3] ^i_data_fsk_r[10] ^lfsr_syn[1] ^lfsr_syn[2] ^lfsr_syn[4] ^lfsr_syn[7] ^lfsr_syn[8] ;
        lfsr_syn[2] <= i_data_fsk_r[2] ^i_data_fsk_r[10] ^lfsr_syn[1] ^lfsr_syn[3] ^lfsr_syn[7] ^lfsr_syn[8] ;
        lfsr_syn[1] <= i_data_fsk_r[1] ^i_data_fsk_r[10] ^lfsr_syn[2] ^lfsr_syn[7] ^lfsr_syn[8] ^lfsr_syn[9] ;
        lfsr_syn[0] <= i_data_fsk_r[0] ^i_data_fsk_r[10] ^lfsr_syn[0] ^lfsr_syn[1] ^lfsr_syn[7] ^lfsr_syn[9] ;
    end
    else
        lfsr_syn <= lfsr_syn;
end

//----------------------------------CRC76--------------------------------------------//
//并行CRC校验，93个不同相位crc校验依次进行（具体说明参见说明文档）
always @ ( posedge i_clk_81M or negedge rst_n )
begin
    if( !rst_n )
        lfsr_CRC <= 75'b0;
    else if( i_data_start )
        lfsr_CRC <= 75'b0;
    else if( i_data_sck_p )  begin//每来一个11bit数据做一次并行CRC除法
        lfsr_CRC[74]<= lfsr_CRC[63] ^lfsr_CRC[65] ^lfsr_CRC[66] ^lfsr_CRC[70] ^lfsr_CRC[71] ^lfsr_CRC[72] ^lfsr_CRC[73];
        lfsr_CRC[73]<= lfsr_CRC[62] ^lfsr_CRC[64] ^lfsr_CRC[65] ^lfsr_CRC[69] ^lfsr_CRC[70] ^lfsr_CRC[71] ^lfsr_CRC[72] ^lfsr_CRC[74];
        lfsr_CRC[72]<= lfsr_CRC[61] ^lfsr_CRC[64] ^lfsr_CRC[65] ^lfsr_CRC[66] ^lfsr_CRC[68] ^lfsr_CRC[69] ^lfsr_CRC[72] ^lfsr_CRC[74];
        lfsr_CRC[71]<= lfsr_CRC[60] ^lfsr_CRC[64] ^lfsr_CRC[66] ^lfsr_CRC[67] ^lfsr_CRC[68] ^lfsr_CRC[70] ^lfsr_CRC[72];
        lfsr_CRC[70]<= lfsr_CRC[59] ^lfsr_CRC[67] ^lfsr_CRC[69] ^lfsr_CRC[70] ^lfsr_CRC[72] ^lfsr_CRC[73] ^lfsr_CRC[74];
        lfsr_CRC[69]<= lfsr_CRC[58] ^lfsr_CRC[66] ^lfsr_CRC[68] ^lfsr_CRC[69] ^lfsr_CRC[71] ^lfsr_CRC[72] ^lfsr_CRC[73] ^lfsr_CRC[74];
        lfsr_CRC[68]<= lfsr_CRC[57] ^lfsr_CRC[65] ^lfsr_CRC[67] ^lfsr_CRC[68] ^lfsr_CRC[70] ^lfsr_CRC[71] ^lfsr_CRC[72] ^lfsr_CRC[73] ^lfsr_CRC[74];
        lfsr_CRC[67]<= lfsr_CRC[56] ^lfsr_CRC[64] ^lfsr_CRC[66] ^lfsr_CRC[67] ^lfsr_CRC[69] ^lfsr_CRC[70] ^lfsr_CRC[71] ^lfsr_CRC[72] ^lfsr_CRC[73] ^lfsr_CRC[74];
        lfsr_CRC[66]<= lfsr_CRC[55] ^lfsr_CRC[68] ^lfsr_CRC[69] ^lfsr_CRC[74];
        lfsr_CRC[65]<= lfsr_CRC[54] ^lfsr_CRC[67] ^lfsr_CRC[68] ^lfsr_CRC[73] ^lfsr_CRC[74];
        lfsr_CRC[64]<= lfsr_CRC[53] ^lfsr_CRC[66] ^lfsr_CRC[67] ^lfsr_CRC[72] ^lfsr_CRC[73] ;
        lfsr_CRC[63]<= lfsr_CRC[52] ^lfsr_CRC[65] ^lfsr_CRC[66] ^lfsr_CRC[71] ^lfsr_CRC[72] ^lfsr_CRC[74];
        lfsr_CRC[62]<= lfsr_CRC[51] ^lfsr_CRC[64] ^lfsr_CRC[65] ^lfsr_CRC[70] ^lfsr_CRC[71] ^lfsr_CRC[73] ^lfsr_CRC[74];
        lfsr_CRC[61]<= lfsr_CRC[50] ^lfsr_CRC[64] ^lfsr_CRC[65] ^lfsr_CRC[66] ^lfsr_CRC[69] ^lfsr_CRC[71] ^lfsr_CRC[74];
        lfsr_CRC[60]<= lfsr_CRC[49] ^lfsr_CRC[64] ^lfsr_CRC[66] ^lfsr_CRC[68] ^lfsr_CRC[71] ^lfsr_CRC[72] ^lfsr_CRC[74];
        lfsr_CRC[59]<= lfsr_CRC[48] ^lfsr_CRC[66] ^lfsr_CRC[67] ^lfsr_CRC[72] ^lfsr_CRC[74];
        lfsr_CRC[58]<= lfsr_CRC[47] ^lfsr_CRC[65] ^lfsr_CRC[66] ^lfsr_CRC[71] ^lfsr_CRC[73] ;
        lfsr_CRC[57]<= lfsr_CRC[46] ^lfsr_CRC[64] ^lfsr_CRC[65] ^lfsr_CRC[70] ^lfsr_CRC[72] ;
        lfsr_CRC[56]<= lfsr_CRC[45] ^lfsr_CRC[64] ^lfsr_CRC[65] ^lfsr_CRC[66] ^lfsr_CRC[69] ^lfsr_CRC[70] ^lfsr_CRC[72] ^lfsr_CRC[73] ;
        lfsr_CRC[55]<= lfsr_CRC[44] ^lfsr_CRC[64] ^lfsr_CRC[66] ^lfsr_CRC[68] ^lfsr_CRC[69] ^lfsr_CRC[70] ^lfsr_CRC[73] ^lfsr_CRC[74];
        lfsr_CRC[54]<= lfsr_CRC[43] ^lfsr_CRC[66] ^lfsr_CRC[67] ^lfsr_CRC[68] ^lfsr_CRC[69] ^lfsr_CRC[70] ^lfsr_CRC[71] ;
        lfsr_CRC[53]<= lfsr_CRC[42] ^lfsr_CRC[65] ^lfsr_CRC[66] ^lfsr_CRC[67] ^lfsr_CRC[68] ^lfsr_CRC[69] ^lfsr_CRC[70] ^lfsr_CRC[74];
        lfsr_CRC[52]<= lfsr_CRC[41] ^lfsr_CRC[64] ^lfsr_CRC[65] ^lfsr_CRC[66] ^lfsr_CRC[67] ^lfsr_CRC[68] ^lfsr_CRC[69] ^lfsr_CRC[73] ^lfsr_CRC[74];
        lfsr_CRC[51]<= lfsr_CRC[40] ^lfsr_CRC[64] ^lfsr_CRC[67] ^lfsr_CRC[68] ^lfsr_CRC[70] ^lfsr_CRC[71] ^lfsr_CRC[74];
        lfsr_CRC[50]<= lfsr_CRC[39] ^lfsr_CRC[65] ^lfsr_CRC[67] ^lfsr_CRC[69] ^lfsr_CRC[71] ^lfsr_CRC[72] ;
        lfsr_CRC[49]<= lfsr_CRC[38] ^lfsr_CRC[64] ^lfsr_CRC[66] ^lfsr_CRC[68] ^lfsr_CRC[70] ^lfsr_CRC[71] ^lfsr_CRC[74];
        lfsr_CRC[48]<= lfsr_CRC[37] ^lfsr_CRC[66] ^lfsr_CRC[67] ^lfsr_CRC[69] ^lfsr_CRC[71] ^lfsr_CRC[72] ^lfsr_CRC[74];
        lfsr_CRC[47]<= lfsr_CRC[36] ^lfsr_CRC[65] ^lfsr_CRC[66] ^lfsr_CRC[68] ^lfsr_CRC[70] ^lfsr_CRC[71] ^lfsr_CRC[73] ;
        lfsr_CRC[46]<= lfsr_CRC[35] ^lfsr_CRC[64] ^lfsr_CRC[65] ^lfsr_CRC[67] ^lfsr_CRC[69] ^lfsr_CRC[70] ^lfsr_CRC[72] ;
        lfsr_CRC[45]<= lfsr_CRC[34] ^lfsr_CRC[64] ^lfsr_CRC[65] ^lfsr_CRC[68] ^lfsr_CRC[69] ^lfsr_CRC[70] ^lfsr_CRC[72] ^lfsr_CRC[73] ^lfsr_CRC[74];
        lfsr_CRC[44]<= lfsr_CRC[33] ^lfsr_CRC[64] ^lfsr_CRC[65] ^lfsr_CRC[66] ^lfsr_CRC[67] ^lfsr_CRC[68] ^lfsr_CRC[69] ^lfsr_CRC[70] ^lfsr_CRC[74];
        lfsr_CRC[43]<= lfsr_CRC[32] ^lfsr_CRC[64] ^lfsr_CRC[67] ^lfsr_CRC[68] ^lfsr_CRC[69] ^lfsr_CRC[70] ^lfsr_CRC[71] ^lfsr_CRC[72] ;
        lfsr_CRC[42]<= lfsr_CRC[31] ^lfsr_CRC[65] ^lfsr_CRC[67] ^lfsr_CRC[68] ^lfsr_CRC[69] ^lfsr_CRC[72] ^lfsr_CRC[73] ^lfsr_CRC[74];
        lfsr_CRC[41]<= lfsr_CRC[30] ^lfsr_CRC[64] ^lfsr_CRC[66] ^lfsr_CRC[67] ^lfsr_CRC[68] ^lfsr_CRC[71] ^lfsr_CRC[72] ^lfsr_CRC[73] ;
        lfsr_CRC[40]<= lfsr_CRC[29] ^lfsr_CRC[67] ^lfsr_CRC[73] ^lfsr_CRC[74];
        lfsr_CRC[39]<= lfsr_CRC[28] ^lfsr_CRC[66] ^lfsr_CRC[72] ^lfsr_CRC[73] ;
        lfsr_CRC[38]<= lfsr_CRC[27] ^lfsr_CRC[65] ^lfsr_CRC[71] ^lfsr_CRC[72] ^lfsr_CRC[74];
        lfsr_CRC[37]<= lfsr_CRC[26] ^lfsr_CRC[64] ^lfsr_CRC[70] ^lfsr_CRC[71] ^lfsr_CRC[73] ;
        lfsr_CRC[36]<= lfsr_CRC[25] ^lfsr_CRC[65] ^lfsr_CRC[66] ^lfsr_CRC[69] ^lfsr_CRC[71] ^lfsr_CRC[73] ^lfsr_CRC[74];
        lfsr_CRC[35]<= lfsr_CRC[24] ^lfsr_CRC[64] ^lfsr_CRC[65] ^lfsr_CRC[68] ^lfsr_CRC[70] ^lfsr_CRC[72] ^lfsr_CRC[73] ;
        lfsr_CRC[34]<= lfsr_CRC[23] ^lfsr_CRC[64] ^lfsr_CRC[65] ^lfsr_CRC[66] ^lfsr_CRC[67] ^lfsr_CRC[69] ^lfsr_CRC[70] ^lfsr_CRC[73] ^lfsr_CRC[74];
        lfsr_CRC[33]<= lfsr_CRC[22] ^lfsr_CRC[64] ^lfsr_CRC[68] ^lfsr_CRC[69] ^lfsr_CRC[70] ^lfsr_CRC[71] ;
        lfsr_CRC[32]<= lfsr_CRC[21] ^lfsr_CRC[65] ^lfsr_CRC[66] ^lfsr_CRC[67] ^lfsr_CRC[68] ^lfsr_CRC[69] ^lfsr_CRC[71] ^lfsr_CRC[72] ^lfsr_CRC[73] ^lfsr_CRC[74];
        lfsr_CRC[31]<= lfsr_CRC[20] ^lfsr_CRC[64] ^lfsr_CRC[65] ^lfsr_CRC[66] ^lfsr_CRC[67] ^lfsr_CRC[68] ^lfsr_CRC[70] ^lfsr_CRC[71] ^lfsr_CRC[72] ^lfsr_CRC[73] ^lfsr_CRC[74];
        lfsr_CRC[30]<= lfsr_CRC[19] ^lfsr_CRC[64] ^lfsr_CRC[67] ^lfsr_CRC[69] ;
        lfsr_CRC[29]<= lfsr_CRC[18] ^lfsr_CRC[65] ^lfsr_CRC[68] ^lfsr_CRC[70] ^lfsr_CRC[71] ^lfsr_CRC[72] ^lfsr_CRC[73] ^lfsr_CRC[74];
        lfsr_CRC[28]<= lfsr_CRC[17] ^lfsr_CRC[64] ^lfsr_CRC[67] ^lfsr_CRC[69] ^lfsr_CRC[70] ^lfsr_CRC[71] ^lfsr_CRC[72] ^lfsr_CRC[73] ^lfsr_CRC[74];
        lfsr_CRC[27]<= lfsr_CRC[16] ^lfsr_CRC[65] ^lfsr_CRC[68] ^lfsr_CRC[69] ^lfsr_CRC[74];
        lfsr_CRC[26]<= lfsr_CRC[15] ^lfsr_CRC[64] ^lfsr_CRC[67] ^lfsr_CRC[68] ^lfsr_CRC[73] ^lfsr_CRC[74];
        lfsr_CRC[25]<= lfsr_CRC[14] ^lfsr_CRC[65] ^lfsr_CRC[67] ^lfsr_CRC[70] ^lfsr_CRC[71] ^lfsr_CRC[74];
        lfsr_CRC[24]<= lfsr_CRC[13] ^lfsr_CRC[64] ^lfsr_CRC[66] ^lfsr_CRC[69] ^lfsr_CRC[70] ^lfsr_CRC[73] ;
        lfsr_CRC[23]<= lfsr_CRC[12] ^lfsr_CRC[66] ^lfsr_CRC[68] ^lfsr_CRC[69] ^lfsr_CRC[70] ^lfsr_CRC[71] ^lfsr_CRC[73] ;
        lfsr_CRC[22]<= lfsr_CRC[11] ^lfsr_CRC[65] ^lfsr_CRC[67] ^lfsr_CRC[68] ^lfsr_CRC[69] ^lfsr_CRC[70] ^lfsr_CRC[72] ^lfsr_CRC[74];
        lfsr_CRC[21]<= lfsr_CRC[10] ^lfsr_CRC[64] ^lfsr_CRC[66] ^lfsr_CRC[67] ^lfsr_CRC[68] ^lfsr_CRC[69] ^lfsr_CRC[71] ^lfsr_CRC[73] ^lfsr_CRC[74];
        lfsr_CRC[20]<= lfsr_CRC[9] ^lfsr_CRC[67] ^lfsr_CRC[68] ^lfsr_CRC[71] ^lfsr_CRC[74];
        lfsr_CRC[19]<= lfsr_CRC[8] ^lfsr_CRC[66] ^lfsr_CRC[67] ^lfsr_CRC[70] ^lfsr_CRC[73] ^lfsr_CRC[74];
        lfsr_CRC[18]<= lfsr_CRC[7] ^lfsr_CRC[65] ^lfsr_CRC[66] ^lfsr_CRC[69] ^lfsr_CRC[72] ^lfsr_CRC[73] ;
        lfsr_CRC[17]<= lfsr_CRC[6] ^lfsr_CRC[64] ^lfsr_CRC[65] ^lfsr_CRC[68] ^lfsr_CRC[71] ^lfsr_CRC[72] ;
        lfsr_CRC[16]<= lfsr_CRC[5] ^lfsr_CRC[64] ^lfsr_CRC[65] ^lfsr_CRC[66] ^lfsr_CRC[67] ^lfsr_CRC[72] ^lfsr_CRC[73] ;
        lfsr_CRC[15]<= lfsr_CRC[4] ^lfsr_CRC[64] ^lfsr_CRC[70] ^lfsr_CRC[73] ^lfsr_CRC[74];
        lfsr_CRC[14]<= lfsr_CRC[3] ^lfsr_CRC[65] ^lfsr_CRC[66] ^lfsr_CRC[69] ^lfsr_CRC[70] ^lfsr_CRC[71] ;
        lfsr_CRC[13]<= lfsr_CRC[2] ^lfsr_CRC[64] ^lfsr_CRC[65] ^lfsr_CRC[68] ^lfsr_CRC[69] ^lfsr_CRC[70] ^lfsr_CRC[74];
        lfsr_CRC[12]<= lfsr_CRC[1] ^lfsr_CRC[64] ^lfsr_CRC[65] ^lfsr_CRC[66] ^lfsr_CRC[67] ^lfsr_CRC[68] ^lfsr_CRC[69] ^lfsr_CRC[70] ^lfsr_CRC[71] ^lfsr_CRC[72] ;
        lfsr_CRC[11]<= lfsr_CRC[0] ^lfsr_CRC[64] ^lfsr_CRC[67] ^lfsr_CRC[68] ^lfsr_CRC[69] ^lfsr_CRC[72] ^lfsr_CRC[73] ;
        lfsr_CRC[10]<= i_data_fsk_r[10] ^lfsr_CRC[65] ^lfsr_CRC[67] ^lfsr_CRC[68] ^lfsr_CRC[70] ^lfsr_CRC[73] ;
        lfsr_CRC[ 9]<= i_data_fsk_r[9] ^lfsr_CRC[64] ^lfsr_CRC[66] ^lfsr_CRC[67] ^lfsr_CRC[69] ^lfsr_CRC[72] ;
        lfsr_CRC[ 8]<= i_data_fsk_r[8] ^lfsr_CRC[68] ^lfsr_CRC[70] ^lfsr_CRC[72] ^lfsr_CRC[73] ;
        lfsr_CRC[ 7]<= i_data_fsk_r[7] ^lfsr_CRC[67] ^lfsr_CRC[69] ^lfsr_CRC[71] ^lfsr_CRC[72] ;
        lfsr_CRC[ 6]<= i_data_fsk_r[6] ^lfsr_CRC[66] ^lfsr_CRC[68] ^lfsr_CRC[70] ^lfsr_CRC[71] ^lfsr_CRC[74];
        lfsr_CRC[ 5]<= i_data_fsk_r[5] ^lfsr_CRC[65] ^lfsr_CRC[67] ^lfsr_CRC[69] ^lfsr_CRC[70] ^lfsr_CRC[73] ^lfsr_CRC[74];
        lfsr_CRC[ 4]<= i_data_fsk_r[4] ^lfsr_CRC[64] ^lfsr_CRC[66] ^lfsr_CRC[68] ^lfsr_CRC[69] ^lfsr_CRC[72] ^lfsr_CRC[73] ;
        lfsr_CRC[ 3]<= i_data_fsk_r[3] ^lfsr_CRC[66] ^lfsr_CRC[67] ^lfsr_CRC[68] ^lfsr_CRC[70] ^lfsr_CRC[73] ;
        lfsr_CRC[ 2]<= i_data_fsk_r[2] ^lfsr_CRC[65] ^lfsr_CRC[66] ^lfsr_CRC[67] ^lfsr_CRC[69] ^lfsr_CRC[72] ;
        lfsr_CRC[ 1]<= i_data_fsk_r[1] ^lfsr_CRC[64] ^lfsr_CRC[65] ^lfsr_CRC[66] ^lfsr_CRC[68] ^lfsr_CRC[71] ;
        lfsr_CRC[ 0]<= i_data_fsk_r[0] ^lfsr_CRC[64] ^lfsr_CRC[66] ^lfsr_CRC[67] ^lfsr_CRC[71] ^lfsr_CRC[72] ^lfsr_CRC[73] ^lfsr_CRC[74];
    end
    else
        lfsr_CRC <= lfsr_CRC;
end      

//------------------------------------------------------------------------------//
//计数输入的11bit的个数
always @(negedge rst_n or posedge i_clk_81M)//mark
begin
    if (!rst_n)
        countbyte<=7'b0;
    else if (i_data_start)//一帧开始时清空重新开始计算
        countbyte<=7'b0;
    else if (i_data_sck_p)//每来一个11bit进行加1个
        countbyte <= countbyte+1'b1;
    else
        countbyte <= countbyte;
end

//判断CRC76检验是否正确 高电平有效
always @(negedge rst_n or negedge i_clk_81M)
begin
  if (!rst_n)  begin
      CRC76_OK <= 1'b0;
      CRC76_OK_r <=1'b0;
      CRC76_OK_p <=1'b0;
  end
  else if ( (countbyte == 7'd93) && (!lfsr_CRC) )  begin 
      CRC76_OK <= 1'b1;
      CRC76_OK_r <=CRC76_OK;
      CRC76_OK_p <= CRC76_OK&&(~CRC76_OK_r);
  end
  else begin
      CRC76_OK <= 1'b0;
      CRC76_OK_r <=1'b0;
      CRC76_OK_p <= 1'b0;
  end
end

//将CRC76_OK_p延时一拍生成o_SyncCode_clk，与ov_SyncCode对齐
always @(negedge rst_n or negedge i_clk_81M)
begin
  if (!rst_n)
      o_SyncCode_clk <= 1'b0;
  else 
      o_SyncCode_clk <= CRC76_OK_p;
end

//93个11bit后，CRC76检验正确 则通过SyncCode_result给出计算的查表地址
always @(negedge rst_n or negedge i_clk_81M)
begin
 if (!rst_n)
   SyncCode_address <= 10'b0;
 else if (i_data_start)  
   SyncCode_address <= 10'b0;
 else if (CRC76_OK)
   SyncCode_address <= lfsr_syn;
 else 
   SyncCode_address <= SyncCode_address;
end

//查表得到报文偏移量
long_sync_result u1(
        .address(SyncCode_address),
        .clock(i_clk_81M),
        .q(ov_SyncCode)//ROM数据比地址变化晚1个时钟输出
        );

endmodule
//+FHEADER //////////////////////////////////////////////////////////////////////
//  版权所有者： 
//  Copyright 2014. All Rights Reserved.
//  保密级别：  
//---------------------------------------------------------------------------
//  文件名 :  can_reset.v
//  原始作者：丁云冰
//  联系方式：
// --------------------------------------------------------------------------
// 版本升级历史：  去掉状态机，加入发送控制器进行发送字节的控制
//                 
// 版本生效日期：  作者    版本
// 1.0 2014-01-13  丁云冰  第1版  
// 2.0 2015-10-28  郭庆阳  第2板  
//---------------------------------------------------------------------------
// 关键字 :   
//---------------------------------------------------------------------------
// 模块功能: reset  mcp2515 chip
//               
// --------------------------------------------------------------------------
// 参数文件名：  
// --------------------------------------------------------------------------
// 重复使用问题：
// 复位策略:负跳变沿异步复位信号 i_rst_n
// 时钟域 : i_clk_27M 
// 关键时序: N/A
// 是否需要实例引用别的模块 : No
// 可综合否 : Yes 
//-FHEADER //////////////////////////////////////////////////////////////////////
module can_reset
       (
         //time & rst
        i_clk_27M,
        i_rst_n,
        
        //input control
        i_send_ok,         //脉冲信号，向寄存器发送数据完成
        i_reset_en,        //电平信号
        
        //output control
        o_spi_tx_en,       //电平信号，spi发送使能
        o_reset_ok,        //脉冲信号，复位完成
        
        //output data
        o_spi_data         //复位数据
        );
    input i_clk_27M;
    input i_rst_n;
    input i_send_ok;
    input i_reset_en;
    
    output o_spi_tx_en;
    output o_reset_ok;
    output[7:0] o_spi_data; 
//////////////////////////////////////////
  `include "parameter.v"
////////////////////////////////////////// 
    reg[1:0] ctrl_cnt;   
    reg o_spi_tx_en;
    reg o_reset_ok;
    reg[7:0] o_spi_data;
////////////////////////////////////////////
//取沿i_reset_en 修改2019.9.12
    reg reset_en_temp1;
    reg reset_en_p;
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if(!i_rst_n)begin
            reset_en_temp1 <= 1'b0;
            reset_en_p <= 1'b0;
        end
        else begin
            reset_en_temp1 <= i_reset_en;
            reset_en_p <= i_reset_en && (~reset_en_temp1);
        end
    end
    
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if(!i_rst_n)
            ctrl_cnt <= 2'd0;
        else if(ctrl_cnt == 2'd2)
            ctrl_cnt <= 2'd0;
        else if(i_send_ok && (ctrl_cnt != 2'd0))
            ctrl_cnt <= ctrl_cnt + 2'd1;
        else if(reset_en_p)
            ctrl_cnt <= 2'd1;
        else
            ctrl_cnt <= ctrl_cnt;
    end

    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if(!i_rst_n) begin 
            o_spi_tx_en <= 0;
            o_spi_data <= 0;
            o_reset_ok <= 0;
        end  
        else if(ctrl_cnt == 2'd1) begin
            o_spi_tx_en <= 1;
            o_spi_data <= `RESET_CMD;
            o_reset_ok <= 0; 
        end
        else if(ctrl_cnt == 2'd2) begin
            o_spi_tx_en <= 0;
            o_spi_data <= 0;
            o_reset_ok <= 1'd1;
        end
        else begin
            o_spi_tx_en <= 0;
            o_spi_data <= 0;
            o_reset_ok <= 0;
        end
    end
 
endmodule

//-------------------------------------------------------------------------------------------------                                                                             
//                                                                                                                                                                              
//--- ALL RIGHTS RESERVED
//--- File      : BtmInnerBusForFsk.v 
//--- Auther    : JInyuan Chen  Jinyuan_whw@hotmail.com
//--- Date      : 2009.5.12
//--- Version   : v2.0
//--- Abstract  : Inner Bus Control Module of BTM
//
//-------------------------------------------------------------------------------------------------                                                                             
//                                                                                                                                                                              
// Description : 通过FIFO设置缓冲区，滤掉短于12个byte的信号。
//               1.接收解调模块framing_start信号，取上跳沿开始组帧。接收字节数计数器清空
//               2.清空FIFO，并向FIFO写入FF FF
//               3.接收Framing模块传过来的字节数据和使能信号（该使能信号比framing_start晚7*48=336拍）                                                                                                                                                              
//               4.当接收字节数到14时，如果framing_start信号高，则将传输标志置高，开始传输
//                                    如果framing_start信号低，则不开启传输 
//               5.停止传输条件：a)framing_start拉低，同时FIFO清空后；b)当接收字节数>12800字节
//               6.(注)相邻的_temp文件是原同名程序，采用状态机和移位寄存器方式，边存边发；现为FIFO缓存，待一定数量后再发。                                                                                                                                                         
//-------------------------------------------------------------------------------------------------                                                                             
//--- Modification History:
//--- Date          By          Version         Change Description 
//-------------------------------------------------------------------
//--- 2009.5.12    Jinyuan Chen      v1.0       
//--- 2009.6.15    Jinyuan Chen      v2.0        ctrl once to send per balise 
//--- 2009.6.20    Jinyuan Chen      v3.0        (FFFF || FFFFD)-->to throw   
//--- 2009.7.07    Jinyuan Chen      v4.0        460800    Baud 
//--- 2009.8.02    Jinyuan Chen      v5.0        NoFIFO   27095K/2/16= 864.7k Baud   >  564/8*11=77.5K
//--- 2009.8.16    Jinyuan Chen      v6.0        FSK
//--- 2011.8.8     Dan Cui           v6.1        计数两个8bit使能之间的时间间隔 3ms
//--- 2024.3.3     MuHaitao          v6.2        Change state ctrl to buffer of FIFO.
//--------------------------------------------------------------------
module BtmInnerBusForFsk(
            i_clk_27M,             // 
            i_rst_n,               //--reset
            
            i_framing_start,       //--defsk data by using this clk    宽度27M(bit_en,8个bit一个byte)
            
            iv_defsk_data8bit,     //--defsk data input 8bits
            i_defsk_wen,           //--defsk data by using this clk 
            
            o_uart_data,           //--UART output             
            o_uart_fsk_wen
            
            );

    input i_clk_27M;
    input i_rst_n;
    input [7:0]iv_defsk_data8bit;
    input i_framing_start;
    input i_defsk_wen;
    output o_uart_data;
    output o_uart_fsk_wen;         

    //--------------------------------------信号声明----------------------------------------------//

    parameter   cntTx_max = 12800;    //接收FSK上限长度
    parameter   MAX_FIFO_BYTE_NUM = 7'd120;

    reg  [13:0]cntTx;             //12800 byte
    reg  [6:0] cnt_byte;      // 对接收的字节计数
    wire uart_tx_done;       
    wire o_uart_data;
    reg  o_uart_fsk_wen;          //输出使能信号 
    reg  framing_start_r;
    reg  framing_start_p;
    reg  defsk_wen_p;
    reg  defsk_wen_r;
    reg  [2:0]  cnt_8;    
    reg  aclr; 
    reg  wrclk;
    reg  [7:0]  byte_deFSK;       //写入fifo的数据   
    wire [7:0]  defsk_data_fifo;  //传入Uart的数据
    reg  state_trans;
    reg  rdreq;
    reg  state_trans_p;
    reg  state_trans_r;
    wire read_FIFO_en;    
    wire [7:0] rdusedw;
    reg  [2:0]  cnt_FIFO_req;
    
    //----------------------------信号预处理-------------------------------
    //---------------对framing_start信号取上跳沿---------------------------
    always @ ( negedge i_rst_n or posedge i_clk_27M)   //使能打拍
    begin 
        if( !i_rst_n) begin
            framing_start_r  <= 1'd0;
            framing_start_p  <= 1'd0;
        end
        else begin
            framing_start_r  <= i_framing_start;
            framing_start_p  <= i_framing_start&(~framing_start_r);
        end
    end 
    
    //---------------对i_defsk_wen信号取上跳沿---------------------------
    always @ ( negedge i_rst_n or posedge i_clk_27M)   //使能打拍
    begin 
        if( !i_rst_n) begin
            defsk_wen_r  <= 1'd0;
            defsk_wen_p  <= 1'd0;
        end
        else begin
            defsk_wen_r  <= i_defsk_wen;
            defsk_wen_p  <= i_defsk_wen & (~defsk_wen_r);
        end
    end 

    /******************************** 27M分频(48*8=384)分频 ******************************/

    parameter   MAX_CNT_48 = 6'd48;
    
    reg [5:0]   cnt_48;
    reg [2:0]   cnt_div8;
    
    wire    cnt_48_flag;
    reg     byte_local_sck;       // 字节数据的本地sck信号
    
    assign  cnt_48_flag = (cnt_48 == MAX_CNT_48 - 1'b1);
    //assign  byte_local_sck = (cnt_div8 == 3'd7);
    
    // 27M作48分频 -> 564.48kbps
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if(i_rst_n == 1'b0) 
            cnt_48 <= 6'd0;
        else if(cnt_48_flag == 1'b1)
            cnt_48 <= 6'd0;
        else
            cnt_48 <= cnt_48 + 1'b1;
    end

    // 564.48k作8分频 -> 70.56kBps
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin 
        if(i_rst_n == 1'b0)
            cnt_div8 <= 3'd0;
        else if((cnt_div8 == 3'd7)&&(cnt_48_flag == 1'b1))
            cnt_div8 <= 3'd0;
        else if(cnt_48_flag == 1'b1)
            cnt_div8 <= cnt_div8 + 1'b1;
        else
            cnt_div8 <= cnt_div8;
    end

    always@(posedge i_clk_27M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)
            byte_local_sck <= 1'b0;
        else if((cnt_div8 == 3'd7)&&(cnt_48_flag == 1'b1))
            byte_local_sck <= 1'b1;
        else
            byte_local_sck <= 1'b0;
    end

    //----------------------------- 对无数据的情况进行检测----------------------------

    reg [1:0]   cnt_no_byte;
    wire        lose_flag;
    reg         lose_flag_r,lose_flag_p;          // 只要丢失2个字节以上就会拉高
    reg         byte_loss_flag;     // 该丢失信号控制读写FIFO

    assign  lose_flag = (cnt_no_byte >= 2'd3);

    // 取上升沿
    always@(posedge i_clk_27M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)begin 
            lose_flag_r <= 1'b0;
            lose_flag_p <= 1'b0;
        end
        else begin 
            lose_flag_r <= lose_flag;
            lose_flag_p <= lose_flag & (~lose_flag_r);
        end
    end

    reg [1:0]   cnt_loss_flag;

    always@(posedge i_clk_27M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            cnt_loss_flag <= 2'd0;
        else if(cnt_byte == 7'd127)
            cnt_loss_flag <= 2'd0;
        else if(lose_flag_p == 1'b1)begin
            if(cnt_loss_flag > 2'd1)
                cnt_loss_flag <= 2'd2;
            else
                cnt_loss_flag <= cnt_loss_flag + 2'd1;
        end
        else 
            cnt_loss_flag <= cnt_loss_flag;
    end

    always@(posedge i_clk_27M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            byte_loss_flag <= 1'b0;
        else if(lose_flag_p == 1'b1)begin 
            if(cnt_byte <= MAX_FIFO_BYTE_NUM)
                byte_loss_flag <= 1'b1;         // 情况1：当接收少于120个字节且丢失较多字节时拉高
            else begin      // 如果在前120个已经丢失过，flag拉高并一直保持到该条报文发送结束；如果没有cnt_loss_flag的控制，会错误的把flag又拉低
                if(cnt_loss_flag == 2'd0)           
                    byte_loss_flag <= 1'b0;         // 情况2：当接收超过120个字节时忽略丢失信号
                else
                    byte_loss_flag <= 1'b1;
            end
        end
        else
            byte_loss_flag <= byte_loss_flag;   // 保持。如果属于情况1，将直接忽略该条报文的后续数据，不再写入
    end
    
    
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if(i_rst_n == 1'b0)
            cnt_no_byte <= 2'd0;
        else if(defsk_wen_p == 1'b1)
            cnt_no_byte <= 2'd0;                    // 只要有数据就将该计数器清空
        else if(lose_flag == 1'b1)
            cnt_no_byte <= cnt_no_byte;             // 当超过3个字节时保持
        else if(byte_local_sck == 1'b1)
            cnt_no_byte <= cnt_no_byte + 1'b1;      // 满足两个字节的时间间隔但仍然没有字节有效信号就视为没有数据
        else
            cnt_no_byte <= cnt_no_byte;
    end
    
    //------------------------------  有效字节计数器 ---------------------------------/

    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if(i_rst_n == 1'b0)
            cnt_byte <= 7'd0;
        else if(framing_start_p == 1'b1)
            cnt_byte <= 7'd0;
        else if(defsk_wen_p == 1'b1)
            cnt_byte <= cnt_byte + 1'b1;
        else
            cnt_byte <= cnt_byte;
    end

    /********************************************************************************/
    /*****************************  向FIFO写入数据      ******************************/
    /********************************************************************************/

    //准备写入FF FF
    //计数器0~7；cnt_8 = 1时清FIFO，cnt_8 == 5和7时写FF
    always @ ( negedge i_rst_n or posedge i_clk_27M)   //数据打拍
    begin 
        if( !i_rst_n)
            cnt_8  <= 3'd0;
        else if((framing_start_p == 1'b1)&&(cnt_8 == 3'd0)) //新的帧标志有效开始标志
            cnt_8  <= 3'd1;
        else if (cnt_8 != 3'd0)
            cnt_8  <= cnt_8 + 3'd1;
        else
            cnt_8  <= cnt_8;
    end
    
    //清空FIFO
    always @ ( negedge i_rst_n or posedge i_clk_27M)   //数据打拍
    begin 
        if( !i_rst_n)
            aclr  <= 1'b0;
        else if((cnt_8 == 3'd1) && (state_trans == 1'b0))   //不满足传输条件且framing_start重新拉高则清空FIFO（因为framing拉高时就会将cnt_8置1)
            aclr  <= 1'b1;
        //else if((cnt_byte == 7'd127)&&(byte_loss_flag == 1'b1))      // mark 2024.02.27
        //    aclr <= 1'b1;
        else
            aclr <= 1'b0;
    end

    // 写时钟这里需要添加：当上一条报文拖尾到下一条，并且该报文丢了较多的字节时，关闭当前报文的写时钟，不再写入，否则当前清空也会将上一条报文清掉。
    //写FIFO时钟，cnt_8 == 5和7时写FF，其余写接收解调后字节
    always @ ( negedge i_rst_n or posedge i_clk_27M)   //数据打拍
    begin 
        if( !i_rst_n)
            wrclk <= 1'b0;
        else if(cnt_8 == 3'd5||cnt_8 == 3'd7)   // 写帧头FF FF的时钟
            wrclk <= 1'b1;
        //else if(byte_loss_flag == 1'b1)
        //    wrclk <= 1'b0;
        else if (defsk_wen_p == 1'b1)           // 解调数据字节对应的时钟
            wrclk <= 1'b1;
        else
            wrclk <= 1'b0;
    end 
    
    //准备写入FIFO的数据
    always @ ( negedge i_rst_n or posedge i_clk_27M)   //数据打拍
    begin 
        if( !i_rst_n)
            byte_deFSK  <= 8'd0;
        else if(framing_start_p == 1'b1)    // 在每次接收新的数据帧时，先写入帧头FF
            byte_deFSK  <= 8'hFF;
        else if(defsk_wen_p == 1'b1)
            byte_deFSK  <= iv_defsk_data8bit;
        else
            byte_deFSK  <= byte_deFSK;
    end 
    
    //统计写入到FIFO，待传输的字节数
    always @ ( negedge i_rst_n or posedge i_clk_27M)   //写入的信号todo:对齐
    begin 
        if( !i_rst_n)
            cntTx <= 14'h0; 
        else if (framing_start_p == 1'b1)
            cntTx <= 14'h2;
        else if(defsk_wen_p == 1'b1)  
            cntTx <= cntTx + 14'h1; 
        else if(framing_start_r == 1'b0)
            cntTx <= 14'h0;       
        else 
            cntTx <= cntTx;     
    end

    /********************************************************************************/
    /*****************************  从FIFO读数据并发给串口***************************/
    /********************************************************************************/
    
    //处理发数据的状态
    always@(negedge i_rst_n or posedge i_clk_27M)   
    begin 
        if( !i_rst_n)
            state_trans <= 1'b0;
        //else if(cntTx >=  14'd14)//根据仿真修改
        else if(rdusedw >= 7'd14)      // 8'd14
            state_trans <= 1'b1;
        else if((rdusedw == 7'd0) && (framing_start_r == 1'b0))
        //停止传输条件：framing_start为低电平，并且FIFO中的数读空
            state_trans <= 1'b0;
        else
            state_trans <= state_trans;
    end
    
    //对发数据标志位取沿
    always @ ( negedge i_rst_n or posedge i_clk_27M)   
    begin
        if( !i_rst_n)
        begin
            state_trans_r <= 1'b0;
            state_trans_p <= 1'b1;
        end
        else
        begin
            state_trans_r <= state_trans;
            state_trans_p <= ~state_trans_r & state_trans;
        end
    end

    //处理读FIFO_req信号和时钟宽度
    assign read_FIFO_en = (state_trans == 1'b1) && (uart_tx_done == 1'b1) && (rdusedw != 7'd0);

    //满足读数据条件时开始计数，并维持在最大值
    always @ ( negedge i_rst_n or posedge i_clk_27M)   
    begin 
        if( !i_rst_n)
            cnt_FIFO_req <= 3'b0;
        else if(read_FIFO_en == 1'b0)
            cnt_FIFO_req <= 3'b0;
        else if(state_trans == 1'b1)begin                    //传输状态时，才需要计数
            if(cnt_FIFO_req >= 3'd7)                //维持在最大值
                cnt_FIFO_req <= 3'd7;
            else
                cnt_FIFO_req <= cnt_FIFO_req + 3'b1;
            end
        else
            cnt_FIFO_req <= cnt_FIFO_req;
    end

    //处理读FIFO时钟
    always @ ( negedge i_rst_n or posedge i_clk_27M)    
    begin 
        if( !i_rst_n)
            rdreq <= 1'b0;
        else if(state_trans_p ==  1'b1)
            rdreq <= 1'b1;
        else if((read_FIFO_en == 1'b1) && (cnt_FIFO_req == 3'd1)) //确保只有1拍
            rdreq <= 1'b1;          // mark_mht(理解):每8个bit读1个字节
        else
            rdreq <= 1'b0;
    end

    //----------------------------- fifo -------------------------

    wire [6:0]  wrusedw;

    //FIFO深度127
    Bus_FIFO U_Bus_FIFO ( 
        .aclr(aclr),                     
        .wrclk(wrclk),                   
        .wrreq(framing_start_r),
        .data(byte_deFSK),
        .rdclk(i_clk_27M),               
        .rdreq(rdreq),                   
        
        .q(defsk_data_fifo),
        .rdusedw(rdusedw)
    );  
        
    //----------------------------------------o_uart_fsk_wen---------------------------------------//
    always @ ( negedge i_rst_n or posedge i_clk_27M) 
      begin
        if ( !i_rst_n )
            o_uart_fsk_wen <= 1'b0;
        else if( state_trans )                   //传输过程中为1
            o_uart_fsk_wen <= 1'b1;
        else
            o_uart_fsk_wen <= 1'b0; 
      end

    //---------------------------   UartTx    ------------------------------------
    UartTx uu_UartTx
      (
      .CLK ( i_clk_27M ), //need change?    
      .RESETn(i_rst_n),
      .start(rdreq),    
      .data(defsk_data_fifo),
      .TIN(o_uart_data), 
      .done(uart_tx_done)  //空闲时为1
      ); 
         
endmodule                        




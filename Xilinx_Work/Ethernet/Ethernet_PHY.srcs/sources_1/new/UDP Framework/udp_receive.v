`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2024/08/22 15:18:28
// Design Name: 
// Module Name: udp_receive
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 接收IP层数据并去掉IP首部，提取出IP层数据。
//				这个模块没有做首部校验和的检查，因为在MAC层的CRC正确时,后边基本不会出错,所以不用做校验.
// Revision:
//////////////////////////////////////////////////////////////////////////////////


module udp_receive #(
	parameter	LOCAL_PORT = 16'h0		// 本地端口号
	)(
	input 	wire			i_clk,
	input	wire			i_rst_n,

	/*---------------- ip_receive 模块交互信号--------------------*/
	input	wire			i_udp_rx_vld,			// 数据有效标志
	input	wire			i_udp_rx_data_last,		// 数据部分最后一个
	input	wire	[7:0]	iv_udp_rx_data,			// UDP层数据
	input 	wire	[15:0]	iv_udp_rx_length,		// UDP层数据包长度

	/*----------------与用户端的交互信号--------------------*/
	output	reg				o_app_rx_data_vld,
	output	reg				o_app_rx_data_last,
	output	reg		[7:0]	ov_app_rx_data,
	output	reg		[15:0]	ov_app_rx_length
);

/*-----------------------------------------------*\
					信号定义
\*-----------------------------------------------*/	
	
	reg	[10:0]	cnt_rx_byte;		// 接收字节计数器
	reg	[15:0]	target_port;		// 目的端口号

/*-----------------------------------------------*\
					接收字节计数
\*-----------------------------------------------*/	

	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n)
			cnt_rx_byte <= 11'd0;
		else if(i_udp_rx_vld)
			cnt_rx_byte <= cnt_rx_byte + 1'b1;
		else
			cnt_rx_byte <= 11'd0;
	end

/*-----------------------------------------------*\
					提取目的端口号
\*-----------------------------------------------*/	

	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n)
			target_port <= 16'h0;
		else if((cnt_rx_byte == 'd2)||(cnt_rx_byte == 'd3))
			target_port <= {target_port[7:0],iv_udp_rx_data};
		else
			target_port <= target_port;
	end

/*-----------------------------------------------*\
			输出数据、有效信号和长度
\*-----------------------------------------------*/	

	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n) begin
			o_app_rx_data_vld  <= 1'b0;
			o_app_rx_data_last <= 1'b0;
			ov_app_rx_data     <= 8'h0;
			ov_app_rx_length   <= 16'h0;
		end 
		else if((target_port == LOCAL_PORT)&&(cnt_rx_byte >= 'd8))begin
			o_app_rx_data_vld  <= i_udp_rx_vld;
			o_app_rx_data_last <= i_udp_rx_data_last;
			ov_app_rx_data     <= iv_udp_rx_data;
			ov_app_rx_length   <= iv_udp_rx_length - 'd8;
		end
		else begin
			o_app_rx_data_vld  <= 1'b0;
			o_app_rx_data_last <= 1'b0;
			ov_app_rx_data     <= 8'h0;
			ov_app_rx_length   <= 16'h0;
		end
	end

endmodule

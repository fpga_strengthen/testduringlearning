onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -label sim:/Sync_tb/Group1 -group {Region: sim:/Sync_tb} /Sync_tb/i_clk_100M
add wave -noupdate -expand -label sim:/Sync_tb/Group1 -group {Region: sim:/Sync_tb} /Sync_tb/rst_n
add wave -noupdate -expand -label sim:/Sync_tb/Group1 -group {Region: sim:/Sync_tb} -height 15 -max 983.00000000000011 -min -927.0 -radix decimal -radixshowbase 0 /Sync_tb/fsk_im
add wave -noupdate -expand -label sim:/Sync_tb/Group1 -group {Region: sim:/Sync_tb} -height 15 -max 973.0 -min -973.0 -radix decimal -radixshowbase 0 /Sync_tb/fsk_re
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/im_yr
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/re_xr
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} {/Sync_tb/U_cordic/x_sign[0]}
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} /Sync_tb/U_cordic/angle_zr
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/im_y0
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/re_x0
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} {/Sync_tb/U_cordic/x_sign[1]}
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/angle_z0
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} /Sync_tb/sample_clk
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -color Orange -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/im_y1
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -color Orange -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/re_x1
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} {/Sync_tb/U_cordic/x_sign[2]}
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -color Orange -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/angle_z1
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -color Cyan -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/im_y2
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -color Cyan -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/re_x2
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} {/Sync_tb/U_cordic/x_sign[3]}
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -color Cyan -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/angle_z2
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -color Violet -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/im_y3
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -color Violet -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/re_x3
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} {/Sync_tb/U_cordic/x_sign[4]}
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -color Violet -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/angle_z3
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -color {Lime Green} -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/im_y4
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -color {Lime Green} -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/re_x4
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} {/Sync_tb/U_cordic/x_sign[5]}
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -color {Lime Green} -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/angle_z4
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -color {Cornflower Blue} -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/im_y5
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -color {Cornflower Blue} -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/re_x5
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} {/Sync_tb/U_cordic/x_sign[6]}
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -color {Cornflower Blue} -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/angle_z5
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -color {Orange Red} -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/im_y6
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -color {Orange Red} -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/re_x6
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} {/Sync_tb/U_cordic/x_sign[7]}
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -color {Orange Red} -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/angle_z6
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -color Salmon -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/im_y7
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -color Salmon -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/re_x7
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} {/Sync_tb/U_cordic/x_sign[8]}
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -color Salmon -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/angle_z7
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -color Yellow -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/im_y8
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -color Yellow -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/re_x8
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} {/Sync_tb/U_cordic/x_sign[9]}
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -color Yellow -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/angle_z8
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -color {Violet Red} -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/im_y9
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -color {Violet Red} -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/re_x9
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} /Sync_tb/U_cordic/angle_z9
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} {/Sync_tb/U_cordic/x_sign[10]}
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -color Pink -itemcolor Pink -radix decimal -radixshowbase 0 /Sync_tb/fsk_im
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -color Pink -itemcolor Pink -radix decimal -radixshowbase 0 /Sync_tb/fsk_re
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} /Sync_tb/sample_clk
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -height 15 -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/angle_tan
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/ov_angle_tan
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {553251 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ms
update
WaveRestoreZoom {522895 ps} {1025111 ps}

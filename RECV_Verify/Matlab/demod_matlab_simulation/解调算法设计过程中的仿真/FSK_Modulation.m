function [ data_ADC ] = FSK_Modulation( )
    %产生相位连续的FSK调制信号，进行16倍过采样得到ADC采样数据
    %   Detailed explanation goes here
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%FSK调制%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    clc
    format long
    close all
    clear all
    
    %%
    m_f				=	16;				%16倍过采样
    src_length		=	20;			%信源数据长度
    sample_rate		=	27.095;		%采样速率，单位MHz
    phai_current	=	0;				%相位累计
    pi				=	3.1415926;
    f0				=	 3.757;%3.951;			%对应比特0的频率，单位MHz -->3.757   3.757; < fL < 4.14652;
    f1				=	 4.711;%4.711;			%对应比特1的频率，单位MHz --> 4.32148; < fH < 4.711;
    f0_true			=	3.951;			%对应比特0的频率，单位MHz
    f1_true			=	4.516;			%对应比特1的频率，单位MHz
    BPS_data		=	0.56448*(1-0.05);		%数据传送速度，单位MHz
    fs				=	sample_rate * m_f;		%过采样频率
    SMP_bit			=	fix(fs/BPS_data);%每个码元采样点数目
    BPS_data		=	fs / SMP_bit;	%调整后的数据传送速度，单位MHz
    DUT_bit			=	1 / BPS_data;	%调整后的每个码元持续时间单位us
    
    S0 = cos( f0_true/sample_rate * (1:fix(sample_rate/0.56448)) * 2*pi);
    S1 = cos( f1_true/sample_rate * (1:fix(sample_rate/0.56448)) * 2*pi);
    S0_Q = sin(f0_true/sample_rate * (1:fix(sample_rate/0.56448)) * 2*pi);%%移相90°
    S1_Q = sin(f1_true/sample_rate * (1:fix(sample_rate/0.56448)) * 2*pi);%%移相90°
    
    Ph_bit0			=	(DUT_bit * f0);	%比特0对应的相位的累计变化，以2*PI进行归一化
    Ph_bit1			=	(DUT_bit * f1);	%比特1对应的相位的累计变化，以2*PI进行归一化
    Ph_vector_0		=	f0 * (1:SMP_bit)/fs; 		%附加相位的初始化
    Ph_vector_1		=	f1 * (1:SMP_bit)/fs; 		%附加相位的初始化
    
    Ph_inst	  =	zeros(1,src_length);	%实际码元序列的相位的初始化
    Ph_inst_r = zeros(1,src_length);    %实际码元序列的累加相位的初始化
    phase     = zeros(1,src_length*SMP_bit);
    Ph_sample =	zeros(1,fix(src_length*DUT_bit/m_f));%16倍抽样的相位的初始化
    Ph_cos	  =	zeros(1,fix(src_length*DUT_bit/m_f));%相位转化为余弦值
    
    PTR_smp =   1;	%初始的采样相位
    
    %%产生信源
    src_data= randi(1,src_length);	%信源比特0/1
    
    Ph_inst = (1 - src_data) * Ph_bit0 +  src_data * Ph_bit1;%每个数据比特会产生的相位偏移
    Ph_inst=[0,Ph_inst(1:end-1)];
    %%构造相位向量Ph0、Ph1
    Ph_inst_r(1) = 0;
    for idx_n = 2:src_length
        Ph_inst_r(idx_n) = sum(Ph_inst(1:idx_n));
    end
    
    %相位函数的计算为比特0和比特1的相位，以及每个比特初始相位，及当前累计相位偏差
    phase =		kron(1-src_data, Ph_vector_0)...
        +	kron(src_data, Ph_vector_1)...
        +	kron(Ph_inst_r,ones(1,SMP_bit));%将本比特的初始相位分配在每个采样点上
    
    phase_dsmpl=downsample(phase,m_f);
    %Ph_inst是实际的码元对应的相位矩阵
    
    Ph_cos = fix(awgn(cos(phase_dsmpl*2*pi),20)*2^10)/(2^10);
    
    for i=1:length(Ph_cos)
        if (Ph_cos(i)>1)
            Ph_cos(i) = 1;
        elseif(Ph_cos(i)<-1)
            Ph_cos(i) = -1;
        end %if
    end %for
    
    data_ADC = (Ph_cos+1)*2048;
    
    end
    
    
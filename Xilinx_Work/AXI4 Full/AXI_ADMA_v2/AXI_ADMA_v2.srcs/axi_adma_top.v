`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Module Name: axi_adma_top
// Description: 
// Revision 0.01 - File Created
//////////////////////////////////////////////////////////////////////////////////


module axi_adma_top #(
	parameter   USER_WR_DATA_WIDTH = 16,				// 用户写数据位宽
	parameter 	USER_RD_DATA_WIDTH = 16,				// 用户读数据位宽
	parameter   AXI_DATA_WIDTH     = 128,				// 转换到的AXI数据位宽
	parameter   AXI_ADDR_WIDTH     = 32,				// AXI写地址宽度
	parameter   WR_BURST_LENGTH    = 4096,				// 最大写突发长度,以字节为单位
	parameter   RD_BURST_LENGTH    = 4096
 	)(
 	input 	wire								i_user_wr_clk,			// 用户写时钟
 	input 	wire								i_user_rd_clk,			// 用户读时钟
	input 	wire								i_rst_n,				// 复位
	input 	wire								i_axi_clk,				// AXI时钟
	
	input 	wire								i_ddr_init_done,		// DDR3初始化完成信号
	// v2模式下添加的用户接口信号
	input 	wire                              	i_user_wr_mode,            // 模式选择,设置0为v1版本功能,设置1为v2版本功能
    input   wire   	[AXI_ADDR_WIDTH-1:0]        iv_user_wr_addr,        // 写地址
    input   wire   	[11:0]                      iv_user_wr_length,      // 写长度,地址偏移量,最大为4096
    input   wire                              	i_user_wr_last,         // 用户端最后一个数据标志

    // v2版本新添加的用户端口信号
	input   wire								i_user_rd_mode,			// 用户读模式设置:0表示v1版本,1表示可以设定读地址的v2版本
	input   wire	[AXI_ADDR_WIDTH-1:0]		iv_user_rd_addr,
	input   wire	[12:0]						iv_user_rd_length,

	/*--------------------------用户端信号-----------------------------*/
	input 	wire	[USER_WR_DATA_WIDTH-1:0]	iv_user_wr_data,		// 用户数据
	input 	wire								i_user_wr_en,			// 数据同步有效信号
	input 	wire	[AXI_ADDR_WIDTH-1:0]		iv_user_wr_base_addr,	// 用户写数据的基地址
	input 	wire	[AXI_ADDR_WIDTH-1:0]		iv_user_wr_end_addr,	//用户写数据的结束地址

	input   wire								i_user_rd_req,			// 数据同步有效信号
	input   wire	[AXI_ADDR_WIDTH-1:0]		iv_user_rd_base_addr,	// 用户读数据的基地址
	input   wire	[AXI_ADDR_WIDTH-1:0]		iv_user_rd_end_addr,	// 用户读数据的结束地址
	output  wire								o_rd_req_busy,			// 用户请求忙标志,此时当前模块无法处理新请求

	// 输出给用户端
	output	wire								o_user_rd_valid,		// 数据有效信号
	output	wire	[USER_WR_DATA_WIDTH-1:0]	ov_user_rd_data,		// 读出给用户端的数据
	output	wire								o_user_rd_last,			// 最后一个数据的标志

	/*--------------------------AXI端口信号----------------------------*/
	//****************写通道*****************//
	/*---------写通道写地址--------*/ 
	output	wire								o_m_axi_awvalid,		// 有效信号，表明此通道的地址控制信号有效
	input	wire								i_m_axi_awready,		// 表明“从”已经准备好,可以接收地址和对应的控制信号
	output	wire	[AXI_DATA_WIDTH-1:0]		ov_m_axi_awaddr,		// 写地址，给出一次写突发传输的写地址
	output	wire	[3:0]						ov_m_axi_awid,			// 写地址ID，用来标志一组写信号
	output	wire	[7:0]						ov_m_axi_awlen,			// 突发长度，给出突发传输的次数
	output	wire	[1:0]						ov_m_axi_awburst,		// 突发类型
	output	wire	[2:0]						ov_m_axi_awsize,		// 突发大小，给出每次突发传输的字节数
	output	wire	[2:0]						ov_m_axi_awprot,		// 保护类型，表明一次传输的特权级及安全等级
	output	wire	[3:0]						ov_m_axi_awqos,			// 质量服务 QoS
	output	wire								o_m_axi_awlock,			// 总线锁信号，可提供操作的原子性
	output	wire	[3:0]						ov_m_axi_awcache,		// 内存类型，表明一次传输是怎样通过系统的

	/*--------写通道写数据---------*/ 
	output	wire								o_m_axi_wvalid,			// 写有效，表明此次写有效
	input	wire								i_m_axi_wready,			// 表明从机可以接收写数据
	output	wire	[AXI_DATA_WIDTH-1:0]		ov_m_axi_wdata,			// 写数据
	output	wire	[AXI_DATA_WIDTH/8-1:0]		ov_m_axi_wstrb,			// 写数据有效的字节线，用来表明每8bits数据是有效的
	output	wire								o_m_axi_wlast,			// 表明此次传输是最后一个突发传输

	/*--------写通道响应通道-------*/ 
	input	wire 	[3:0]						iv_m_axi_bid,			// 写响应 IDTAG
	input	wire 	[1:0]						iv_m_axi_bresp,			// 写响应，表明写传输的状态
	input	wire 								i_m_axi_bvalid,			// 写响应有效
	output 	wire								o_m_axi_bready,			// 表明主机能够接收写响应


	//****************读通道*****************//
	/*---------读通道读数据----------*/ 
	input	wire	[3:0]						iv_m_axi_rid,			// 读 IDtag
	input	wire	[AXI_DATA_WIDTH-1:0]		iv_m_axi_rdata,			// 读数据
	input	wire	[1:0]						iv_m_axi_rresp,			// 读响应，表明读传输的状态
	input	wire								i_m_axi_rlast,			// 表明读突发的最后一次传输
	input	wire								i_m_axi_rvalid,			// 表明此通道信号有效
	output	wire								o_m_axi_rready,			// 表明主机能够接收读数据和响应信息

	/*-----读通道地址与控制信号------*/
	output	wire	[3:0]						ov_m_axi_arid,			// 读地址ID，用来标志一组写信号
	output	wire	[AXI_ADDR_WIDTH-1:0]		ov_m_axi_araddr,		// 读地址，给出一次写突发传输的读地址
	output	wire	[7:0]						ov_m_axi_arlen,			// 突发长度，给出突发传输的次数
	output	wire	[2:0]						ov_m_axi_arsize,		// 突发大小，给出每次突发传输的字节数
	output	wire	[1:0]						ov_m_axi_arburst,		// 突发类型
	output	wire								o_m_axi_arlock,			// 总线锁信号，可提供操作的原子性
	output	wire	[3:0]						ov_m_axi_arcache,		// 内存类型，表明一次传输是怎样通过系统的
	output	wire	[2:0]						ov_m_axi_arprot,		// 保护类型，表明一次传输的特权级及安全等级
	output	wire	[3:0]						ov_m_axi_arqos,			// 质量服务 QOS
	output	wire								o_m_axi_arvalid,		// 有效信号，表明此通道的地址控制信号有效
	input	wire								i_m_axi_arready			// 表明“从”可以接收地址和对应的控制信号
);

	axi_wr_channel_top #(
			.USER_WR_DATA_WIDTH (USER_WR_DATA_WIDTH),
			.AXI_DATA_WIDTH     (AXI_DATA_WIDTH),
			.AXI_ADDR_WIDTH     (AXI_ADDR_WIDTH),
			.WR_BURST_LENGTH    (WR_BURST_LENGTH)
		) U_axi_wr_channel_top (
			.i_user_wr_clk        (i_user_wr_clk),
			.i_rst_n              (i_rst_n),
			.i_axi_clk            (i_axi_clk),

			.i_ddr_init_done      (i_ddr_init_done),
			.i_user_wr_mode       (i_user_wr_mode),
			.iv_user_wr_data      (iv_user_wr_data),
			.i_user_wr_en         (i_user_wr_en),
			.iv_user_wr_base_addr (iv_user_wr_base_addr),
			.iv_user_wr_end_addr  (iv_user_wr_end_addr),

			.iv_user_wr_addr      (iv_user_wr_addr),
			.iv_user_wr_length    (iv_user_wr_length),
			.i_user_wr_last       (i_user_wr_last),

			.o_m_axi_awvalid      (o_m_axi_awvalid),
			.i_m_axi_awready      (i_m_axi_awready),
			.ov_m_axi_awaddr      (ov_m_axi_awaddr),
			.ov_m_axi_awid        (ov_m_axi_awid),
			.ov_m_axi_awlen       (ov_m_axi_awlen),
			.ov_m_axi_awburst     (ov_m_axi_awburst),
			.ov_m_axi_awsize      (ov_m_axi_awsize),
			.ov_m_axi_awprot      (ov_m_axi_awprot),
			.ov_m_axi_awqos       (ov_m_axi_awqos),
			.o_m_axi_awlock       (o_m_axi_awlock),
			.ov_m_axi_awcache     (ov_m_axi_awcache),
			.o_m_axi_wvalid       (o_m_axi_wvalid),
			.i_m_axi_wready       (i_m_axi_wready),
			.ov_m_axi_wdata       (ov_m_axi_wdata),
			.ov_m_axi_wstrb       (ov_m_axi_wstrb),
			.o_m_axi_wlast        (o_m_axi_wlast),

			.iv_m_axi_bid         (iv_m_axi_bid),
			.iv_m_axi_bresp       (iv_m_axi_bresp),
			.i_m_axi_bvalid       (i_m_axi_bvalid),
			.o_m_axi_bready       (o_m_axi_bready)
	);

	axi_rd_channel_top #(
			.USER_RD_DATA_WIDTH (USER_RD_DATA_WIDTH),
			.AXI_DATA_WIDTH     (AXI_DATA_WIDTH),
			.AXI_ADDR_WIDTH     (AXI_ADDR_WIDTH),
			.RD_BURST_LENGTH    (RD_BURST_LENGTH)
		) U_axi_rd_channel_top (
			.i_user_rd_clk        (i_user_rd_clk),
			.i_rst_n              (i_rst_n),
			.i_axi_clk            (i_axi_clk),

			.i_ddr_init_done      (i_ddr_init_done),

			.i_user_rd_req        (i_user_rd_req),
			.iv_user_rd_base_addr (iv_user_rd_base_addr),
			.iv_user_rd_end_addr  (iv_user_rd_end_addr),
			.o_rd_req_busy        (o_rd_req_busy),
			
			.i_user_rd_mode       (i_user_rd_mode),
			.iv_user_rd_addr      (iv_user_rd_addr),
			.iv_user_rd_length    (iv_user_rd_length),

			.o_user_rd_valid      (o_user_rd_valid),
			.ov_user_rd_data      (ov_user_rd_data),
			.o_user_rd_last       (o_user_rd_last),

			.iv_m_axi_rid         (iv_m_axi_rid),
			.iv_m_axi_rdata       (iv_m_axi_rdata),
			.iv_m_axi_rresp       (iv_m_axi_rresp),
			.i_m_axi_rlast        (i_m_axi_rlast),
			.i_m_axi_rvalid       (i_m_axi_rvalid),
			.o_m_axi_rready       (o_m_axi_rready),
			.ov_m_axi_arid        (ov_m_axi_arid),
			.ov_m_axi_araddr      (ov_m_axi_araddr),
			.ov_m_axi_arlen       (ov_m_axi_arlen),
			.ov_m_axi_arsize      (ov_m_axi_arsize),
			.ov_m_axi_arburst     (ov_m_axi_arburst),
			.o_m_axi_arlock       (o_m_axi_arlock),
			.ov_m_axi_arcache     (ov_m_axi_arcache),
			.ov_m_axi_arprot      (ov_m_axi_arprot),
			.ov_m_axi_arqos       (ov_m_axi_arqos),
			.o_m_axi_arvalid      (o_m_axi_arvalid),
			.i_m_axi_arready      (i_m_axi_arready)
	);

endmodule
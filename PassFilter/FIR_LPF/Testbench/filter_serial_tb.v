`timescale 1ns/1ps
module filter_serial_tb();

    reg                 clk;
    reg                 rst_n;
    reg         [1:0]   cnt;
    reg signed  [7:0]   acc_final;
    wire        [7:0]   filter_out;
    
    initial begin
        clk = 1'b1;
        rst_n <= 1'b0;
        #40
        rst_n <= 1'b1;
    end

    always #10 clk = ~clk;

    always@(posedge clk or negedge rst_n)begin 
        if(rst_n == 1'b0)
            cnt <= 2'd0;
        else
            cnt <= cnt + 1'b1;
    end

    always@(posedge clk or negedge rst_n)begin 
        if(rst_n == 1'b0)
            acc_final <= 8'b0;
        else if(cnt == 2'd3)
            acc_final <= ($random % 100);
        else
            acc_final <= acc_final;
    end

    assign  filter_out = (acc_final >>> 3);


endmodule
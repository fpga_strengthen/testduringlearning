/*=======================================================================================*\
  Filename    : Bit11ToBit10.v
  Author      : 陈献彬
  time        ：2016.07.05
  Description : 报文移位拼接完成后，需要完成查表和解扰运算。本模块根据移位拼接模块给出的报文，
                解扰运算的初始值以及正反向标志对报文进行查表和解扰。
               
  Called by   : 
  Revision History : 
  Author              Date                  Version          Description
  陈献彬              20160817              V1.1.0                
  Email       : chenxianbin_bupt@163.con
  Company     :

\*========================================================================================*/
module Bit11ToBit10(
           i_clk_81M,
           i_rstn,
           
           i_data_start,
           iv_data_shifted,
           i_data_shifted_sck,
           i_shift_ok_en,
           
           i_ph_flag,
           iv_pn_data,
           
           o_data_start,
           ov_descramed_data,
           o_descramed_data_sck,
           o_check_error,
           o_crc_error
            );
                      
input i_clk_81M;
input i_rstn;
           
input i_data_start;    //帧开始标志，每个11bit都会产生一个93x11bit的帧
input [10:0]iv_data_shifted;  //同步后的11bit数据
input i_data_shifted_sck;     //11bit字节同步时钟
input i_shift_ok_en;          //同步后76位crc校验正确标志
           
input i_ph_flag;              //是否取反标志
input [31:0]iv_pn_data;       //解扰时用到

output o_data_start;          //该模块给出的帧开始标志，比ov_descramed_data提前几个时钟
output [9:0]ov_descramed_data;//解扰后的10bit数据
output o_descramed_data_sck;  //解扰后10bit数据的字节同步时钟
output o_check_error;         //11bit查表时，查出不符合编码规范的报文
output o_crc_error;           //crc校验错误

//---------------------------------------------------------------------------------------------------------------//
reg [10:0]data_shifted;
reg data_shifted_sck_r1;
reg data_shifted_sck_p;
reg data_shifted_sck_p_r;
reg [6:0]wt_add;  //1023itram写地址，93x11
reg wt_en;        //写使能
reg shift_ok_en_r;
reg shift_ok_en_p;
wire [32:0]data_in_crc_result_before;
wire data_in_crc_ok_before;
reg [3:0]cnt_10;
reg [6:0]rd_add;//读地址
reg rd_en;      //读使能
reg rd_en_r1;    //读使能寄存2次，用于读出crc校验的字节同步时钟
reg rd_en_r2;    //读使能寄存2次，用于读出crc校验的字节同步时钟
wire [10:0]data_out_11bit;
//wire [10:0]data_out_test;  //RAM坏道测试 delete  
wire [32:0]data_in_crc_result_after;
wire data_in_crc_ok_after;
reg o_crc_error;
wire [10:0]data_ph;
wire [10:0]data_10;//查表得到的数据，低10有效，查表后的报文最高位均应该为0，最高位为1的报文是非法报文，应当滤除
reg decode_error;//译码错误标志
reg [9:0]data_cyclic_shift;//查表后数据循环移位,用于解扰运算
reg [31:0]pn;   //h(x) = x32+x31+x30+x29+x27+x25+1;    2801775573
reg [9:0]write_data_reg;//对out值进行位拼接
reg [9:0]write_data1;
reg [9:0]data_first;
reg data_descramed_sck;  //字节同步时钟
reg [9:0]data_descramed; //解扰后与同步时钟对齐的数据
reg o_data_start; //数据发送之前给出的帧开始标志
reg out;
//------------------------------------------------------------------------------//
//首先将同步后的1023bit数据存到本地ram，以下模块为控制ram写操作的代码
always@( posedge i_clk_81M or negedge i_rstn )
begin
    if( !i_rstn )
        data_shifted <= 11'd0;
    else if ( data_shifted_sck_p )
        data_shifted <= iv_data_shifted;
    else
        data_shifted <= data_shifted;
end

//将iv_data_shifted的字节同步时钟i_data_shifted_sck取沿并延时几个时钟，方便后续时序处理
always@( posedge i_clk_81M or negedge i_rstn )
begin
    if( !i_rstn )begin
        data_shifted_sck_r1  <= 1'b0;
        data_shifted_sck_p   <= 1'b0;
        data_shifted_sck_p_r <= 1'b0;
    end
    else begin
        data_shifted_sck_r1  <= i_data_shifted_sck;
        data_shifted_sck_p   <= ~data_shifted_sck_r1 & i_data_shifted_sck;
        data_shifted_sck_p_r <= data_shifted_sck_p;
    end
  
end

//------------------------------------------------------------------------------//
always@( posedge i_clk_81M or negedge i_rstn )
begin
    if( !i_rstn )
        wt_add <= 7'd0;
    else if( i_data_start )
        wt_add <= 7'd0;
    else if( wt_add == 7'd92 && data_shifted_sck_p_r ) //93个11bit写完
        wt_add <= 7'd0;
    else if( data_shifted_sck_p_r )
        wt_add <= wt_add + 7'd1;
    else
        wt_add <= wt_add;  
end
                      
always@( posedge i_clk_81M or negedge i_rstn )
begin
    if( !i_rstn )
        wt_en <= 1'b0;
    else if( data_shifted_sck_p )
        wt_en <= 1'b1;
    else
        wt_en <= 1'b0; 
end                      

//-----------------------------------------------------------------------------//  
//输入数据进行crc校验
Crc32_D11 Crc32_D11_before
    (
    .i_clk_27M( i_clk_81M ),//clock
    .i_rst_n( i_rstn ),  //reset
    .i_data( data_shifted ),   //data to be checked 
    .i_sck( data_shifted_sck_p_r ),    //check data when the input signal is high level
    .i_93start( i_data_start ),//93字起始脉冲
    
    .o_crc33( data_in_crc_result_before),  //33bit 32位校验+1bit0  
    .o_crcok( data_in_crc_ok_before )   //检验完成指示 高有效  
    );

//------------------------------------------------------------------------------//

//输入数据写ram完成后且同步模块给出crc76校验正确标志，证明输入数据正确，开始读出数据进行后续处理

//-----------------------------------------------------------------------------//
//对i_shift_ok_en取沿
always@( posedge i_clk_81M or negedge i_rstn )
begin
    if( !i_rstn )begin
        shift_ok_en_r <= 1'd0;
        shift_ok_en_p <= 1'd0;
    end
    else begin
        shift_ok_en_r <= i_shift_ok_en;
        shift_ok_en_p <= i_shift_ok_en && ~shift_ok_en_r ;
    end
end
//------------------------------------------------------------------------------//
//长度为10的计数器，用来控制读数据和读出后数据的处理,将后续的分为10个时钟处理
always@( posedge i_clk_81M or negedge i_rstn )
begin
    if( !i_rstn )
        cnt_10 <= 4'd0;
    else if( i_data_start )  //帧开始标志，清零计数器
        cnt_10 <= 4'd0;
    else if( shift_ok_en_p ) //同步模块76位crc计算正确，开始读数据，计数器开始循环计数
        cnt_10 <= 4'd1;
    else if( rd_add ==7'd0 && cnt_10 == 4'd10 )// 读数据结束（cnt_10 == 4'd4是为了控制好时序，画出时序图可理解清晰），清零计数器
        cnt_10 <= 4'd0;
    else if( cnt_10 == 4'd10 )
        cnt_10 <= 4'd1;
    else if( cnt_10 )
        cnt_10 <= cnt_10 + 4'd1;
    else
        cnt_10 <= cnt_10;  
end 
 
//------------------------------------------------------------------------------//
//产生读使能和读地址
always@( posedge i_clk_81M or negedge i_rstn )
begin
    if( !i_rstn ) begin
        rd_en    <= 1'b0;
        rd_en_r1 <= 1'b0;
        rd_en_r2 <= 1'b0;
    end
    else if( cnt_10 == 4'd1 ) begin
        rd_en    <= 1'b1;
        rd_en_r1 <= rd_en;
        rd_en_r2 <= rd_en_r1;
    end
    else begin
        rd_en    <= 1'b0; 
        rd_en_r1 <= rd_en;
        rd_en_r2 <= rd_en_r1;
    end
end  

always@( posedge i_clk_81M or negedge i_rstn )
begin
    if( !i_rstn )
        rd_add <= 7'd0;
    else if( i_data_start )
        rd_add <= 7'd0;
    else if( (rd_add == 7'd92) && (cnt_10 == 4'd2) ) //读数完成，读地址清零 
        rd_add <= 7'd0;   
    else if( cnt_10 == 4'd2 )
        rd_add <= rd_add + 7'd1;
    else
        rd_add <= rd_add;  
end

//------------------------------------------------------------------------------//
ram_out U_ram_out(
    .wraddress(wt_add),
    .rdaddress(rd_add),
    .wrclock(i_clk_81M),
    .rdclock(i_clk_81M),
    .data(data_shifted),
    .q(data_out_11bit),
    .rden(rd_en),
    .wren(wt_en)
    );
//RAM坏道测试 can delete  
//assign data_out_test = (rd_en_r2 && rd_add == 7'd76 ) ? 11'd0 : data_out_11bit;
        
//------------------------------------------------------------------------------//  
//输出数据进行crc校验
Crc32_D11 Crc32_D11_after
    (
    .i_clk_27M( i_clk_81M ),//clock
    .i_rst_n( i_rstn ),  //reset
    .i_data( data_out_11bit ),   //data to be checked 
    //.i_data( data_out_test ),   //RAM坏道测试 can delete  
    .i_sck( rd_en_r2 ),    //check data when the input signal is high level mark 注意时序
    .i_93start( i_data_start ),//93字起始脉冲
    
    .o_crc33( data_in_crc_result_after),  //33bit 32位校验+1bit0  
    .o_crcok( data_in_crc_ok_after )   //检验完成指示 高有效  
    );
    

always @(negedge i_rstn or posedge i_clk_81M)   
begin
  if(!i_rstn)
      o_crc_error <= 1'b0;
  else if ( i_data_start )
      o_crc_error <= 1'b0;
  else if( data_in_crc_ok_after && (data_in_crc_result_after != data_in_crc_result_before) ) 
      o_crc_error <= 1'b1;
  else
      o_crc_error <= 1'b0;
end    

//------------------------------------------------------------------------------//
//11bit查表
reg ph_flag;  //i_ph_flag寄存，上位模块（shift_bit）给出脉冲，本模块需要维持一定时间的电平
always @(negedge i_rstn or posedge i_clk_81M)   
begin
  if(!i_rstn)
      ph_flag <= 1'b0;
  else if( i_data_start )
      ph_flag <= 1'b0;
  else if( i_ph_flag )
      ph_flag <= 1'b1;
  else
      ph_flag <= ph_flag;
end
                
assign data_ph = (ph_flag == 1'b0)? data_out_11bit[10:0] : ~data_out_11bit[10:0];  //数据是否需要反相

bit11to10table  bit11to10table1(
    .address(data_ph),
    .clock(i_clk_81M),
    .q(data_10)
    );
//------------------------------------------------------------------------------//
//判断查表得到的数据是否符合编码规范

always @(negedge i_rstn or posedge i_clk_81M)   
begin
  if(!i_rstn)
      decode_error <= 1'b0;
  else if(data_10[10] == 1'b1 && cnt_10 == 4'd7 ) //查表得到的每个11bit都要校验，通过cnt_10和rd_add控制 
      decode_error <= 1'b1;
  else
      decode_error <= 1'b0;
end

assign o_check_error = decode_error;
//------------------------------------------------------------------------------//
//解扰运算
always @(negedge i_rstn or posedge i_clk_81M)   
begin
    if(!i_rstn)
        data_cyclic_shift <= 10'b0;
    else if( i_data_start )
        data_cyclic_shift <= 10'd0;
    else if(cnt_10 == 4'd5)  
        data_cyclic_shift <= data_10[9:0];
    else
        data_cyclic_shift <= {data_cyclic_shift[8:0],data_cyclic_shift[9]};
end

always @(negedge i_rstn or posedge i_clk_81M)//计算out值   
begin
    if(!i_rstn) begin
        pn <= 32'b0;
        out <= 1'b0;
    end
    else if( i_data_start ) begin
        pn <= 32'b0;
        out <= 1'b0;
    end
    else if(cnt_10 == 4'd5 && rd_add == 7'd1) begin
        pn <= iv_pn_data;
        out <= out;
    end
    else begin
        pn <= {pn[30:0],1'b0}^(32'hEA000001&{32{data_cyclic_shift[9]}});  //h(x) 串行解扰过程
        out <= (data_cyclic_shift[9]^pn[31]);
    end
end

always @(negedge i_rstn or posedge i_clk_81M)   
begin
    if(!i_rstn)
        write_data_reg <= 10'b0;
    else 
        write_data_reg <= {write_data_reg[8:0],out};   
end
    

always @(negedge i_rstn or posedge i_clk_81M)   
begin
    if(!i_rstn)
        write_data1 <= 10'b0;
    else if( i_data_start )
        write_data1 <= 10'b0;
    else if(cnt_10 == 4'd7)//画出时序图后，得cnt_10为7时取出的拼接数据正好是解扰完成数据
        write_data1 <= write_data_reg;
    else
        write_data1 <= write_data1;//扰码后数据
end

//------------------------------------------------------------------------------//
//第一个10byte特殊处理，
always @(negedge i_rstn or posedge i_clk_81M)   
begin
    if(!i_rstn)
        data_first <= 10'b0;
    else if( i_data_start )
        data_first <= 10'b0;
    else if( (7'd2 == rd_add) && (cnt_10 == 4'd8))
        data_first <= write_data1;
    else if( (7'd3 <= rd_add) && (rd_add <= 7'd84) && (cnt_10 == 4'd8)) //
        data_first <= {1'b1,data_first} - write_data1 ;  //依次减掉后面82个10bit，最后得到第一个10bit数据
    else
        data_first <= data_first;
end
//------------------------------------------------------------------------------//
//数据拼接完成后，需要将解扰后的正确数据写入ram中。
//这个过程由下一个模块完成，本模块给出数据和数据同步时钟即可。
always @(negedge i_rstn or posedge i_clk_81M)   
begin
    if(!i_rstn)
        data_descramed_sck <= 1'b0;
    else if((7'd4 <= rd_add) && (rd_add <= 7'd86) && (cnt_10 == 4'd4)) //mark 注明幻数含义
        data_descramed_sck <= 1'b1;
    else
        data_descramed_sck <= 1'b0;
end

assign o_descramed_data_sck = data_descramed_sck;

/*只取前83个11bit（报文部分，以后的字节为各种校验码），第一个10bit特殊处理，用后82个10bit的和截短后作为第一个10bit
  画出时序图，得到写数据和数据同步时钟
*/
always @(negedge i_rstn or posedge i_clk_81M)   
begin
    if(!i_rstn)
        data_descramed <= 10'b0;
    else if( i_data_start )
        data_descramed <= 10'b0;
    else if( (rd_add == 7'd85) && (cnt_10 == 4'd7)) //读地址到85时，第一个10bit数据计算结束
        data_descramed <= data_first;
    else if( (7'd3 <= rd_add) && (rd_add <= 7'd84) && (cnt_10 == 4'd8) ) //3-84 共82个10bit，依次发送出来
        data_descramed <= write_data1;
    else
        data_descramed <= data_descramed;
end

assign ov_descramed_data = data_descramed;

always@( posedge i_clk_81M or negedge i_rstn )
begin
      if( !i_rstn )
          o_data_start <= 1'b0;
      else if ( (7'd3 == rd_add)  && (cnt_10 == 4'd1) )
          o_data_start <= 1'b1;
      else
          o_data_start <= 1'b0; 
end
                 
endmodule 
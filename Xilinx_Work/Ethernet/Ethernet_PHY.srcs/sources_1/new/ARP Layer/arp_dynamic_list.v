`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date: 2024/08/26 22:37:13
// Module Name: arp_dynamic_list
// Description: 缓存收到的IP和MAC地址,更新ARP动态链表.
//////////////////////////////////////////////////////////////////////////////////

module arp_dynamic_list(
	input	wire			i_clk,
	input	wire			i_rst_n,

	input	wire			i_wr_arp_en,
	input	wire	[31:0]	iv_wr_ip_addr,
	input	wire 	[47:0]	iv_wr_mac_addr,

	input	wire			i_rd_arp_en,
	input	wire 	[31:0]	iv_rd_ip_addr,	// 输入IP地址查询MAC地址
	output	reg		[47:0]	ov_rd_mac_addr,
	output 	reg				o_rd_mac_addr_vld
);
	
	localparam	MEM_SIZE = 4;	// 存储器深度

	// 定义存储器作为ip和mac地址的缓冲区
	reg	[31:0]	ip_addr_buffer	[MEM_SIZE-1:0];
	reg	[47:0]	mac_addr_buffer	[MEM_SIZE-1:0];

	reg	[2:0]	buffer_pointer;		// 定义一个buffer指针,用于对buffer进行访问和更新

/*-----------------------------------------------*\
				更新ARP动态链表
\*-----------------------------------------------*/	
	
	// 将收到的IP地址与本地IP地址比对,并存储对应的MAC地址.
	// 缓冲区存储深度为4条IP地址,当一条IP在本地不存在时,存储并更新本地buffer且将指针自加1.
	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n) begin		// buffer初始化
			buffer_pointer     <=  3'd0;		// 数组地址指针
			// IP缓冲区
			ip_addr_buffer[0]  <= 32'h0;		
			ip_addr_buffer[1]  <= 32'h0;
			ip_addr_buffer[2]  <= 32'h0;
			ip_addr_buffer[3]  <= 32'h0;
			// MAC地址缓冲区
			mac_addr_buffer[0] <= 48'h0;		
			mac_addr_buffer[1] <= 48'h0;
			mac_addr_buffer[2] <= 48'h0;
			mac_addr_buffer[3] <= 48'h0;
		end 
		else if(i_wr_arp_en)begin
			// 根据输入IP地址先在本地查询是否已有此IP;如没有则更新buffer,并缓存对应MAC地址.
			case (iv_wr_ip_addr)
				ip_addr_buffer[0] : mac_addr_buffer[0] <= iv_wr_mac_addr;
				ip_addr_buffer[1] : mac_addr_buffer[1] <= iv_wr_mac_addr;
				ip_addr_buffer[2] : mac_addr_buffer[2] <= iv_wr_mac_addr;
				ip_addr_buffer[3] : mac_addr_buffer[3] <= iv_wr_mac_addr;
				default : begin 
					ip_addr_buffer[buffer_pointer]  <= iv_wr_ip_addr;
					mac_addr_buffer[buffer_pointer] <= iv_wr_mac_addr;
					buffer_pointer                  <= (buffer_pointer == MEM_SIZE-1) ? 3'd0 : (buffer_pointer + 1'b1);
				end
			endcase
		end 	// 时序逻辑下,为了简便起见,不写最后一个else分支了,程序会默认是保持.
	end

/*-----------------------------------------------*\
						查表
\*-----------------------------------------------*/	

	// 根据收到的IP查询buffer中的MAC地址
	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n) begin
			ov_rd_mac_addr <= 48'h0;
			o_rd_mac_addr_vld <= 1'b0;
		end 
		else if(i_rd_arp_en)begin
			// 读使能有效时,就输出MAC地址查表的标志信号
			o_rd_mac_addr_vld <= 1'b1;
			case(iv_rd_ip_addr)
				ip_addr_buffer[0] : ov_rd_mac_addr <= mac_addr_buffer[0];
				ip_addr_buffer[1] : ov_rd_mac_addr <= mac_addr_buffer[1];
				ip_addr_buffer[2] : ov_rd_mac_addr <= mac_addr_buffer[2];
				ip_addr_buffer[3] : ov_rd_mac_addr <= mac_addr_buffer[3];
				default : ov_rd_mac_addr <= 48'h0;
			endcase
		end
		else begin 
			o_rd_mac_addr_vld <= 1'b0;
			ov_rd_mac_addr    <= 48'h0;
		end
	end

endmodule

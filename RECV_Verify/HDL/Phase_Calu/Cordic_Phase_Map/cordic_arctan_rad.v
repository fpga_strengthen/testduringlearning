// 通过Cordic的10级流水迭代计算arctan(y/x)值
// 该模块欲计算的相位以弧度值表示，已完成且通过了验证.
module cordic_arctan_rad(
    input   wire    i_clk_108M,
    input   wire    i_clk_27M_en,
    input   wire    i_rst_n,
    input   wire    signed  [12:0]  iv_im_data,
    input   wire    signed  [12:0]  iv_re_data,
    // 输出相位为14bit，范围0~8192,最大情况下的8192需要14bit
    output  wire            [13:0]  ov_angle_tan
);

/*
    设pi=4096,2pi=8912,即一个周期的角度为8192，量化为14位.
    角度值θ满足：tan(θi)=2^(-i),则θi=arctan(2^(-i)),θi∈[0,2pi]~[0,8192].
    这里的角度值均以弧度表示,并依照[0,2pi]~[0,8192]的规则进行表示.
    采用10级流水进行计算,迭代计算的常数角度值angle_i 参考Matlab程序.
    对于满足tan(θi)=2^(-i)的θi,这里以弧度表示,则θi=arctan(2^(-i))/pi.
    举例：360°~2pi~8192, 则45°~pi/4~1024,换算的关系为对于弧度表示的θ有：θ/2pi*8192.
    这里的θ是满足tan(αi)=2^(-i)的αi转化为弧度后的值映射到[0,8192]上,i=0~9.
*/

    // 以弧度表示
    parameter   angle_0  = 11'd1024,            // i=0,对应45°
                angle_1  = 11'd605,             // i=1
                angle_2  = 11'd319,             // i=2
                angle_3  = 11'd162,             // i=3
                angle_4  = 11'd81,              // i=4
                angle_5  = 11'd41,              // i=5
                angle_6  = 11'd20,              // i=6
                angle_7  = 11'd10,              // i=7
                angle_8  = 11'd5,               // i=8
                angle_9  = 11'd3;               // i=9

    // 迭代次数     
    parameter   NUM_Pipeline = 'd10;

    // 因为要迭代10次，若进行全精度计算，就需要扩展10bit
    reg signed  [22:0]  im_yr,  re_xr;
    reg signed  [22:0]  im_y0,  re_x0;
    reg signed  [22:0]  im_y1,  re_x1;
    reg signed  [22:0]  im_y2,  re_x2;
    reg signed  [22:0]  im_y3,  re_x3;
    reg signed  [22:0]  im_y4,  re_x4;
    reg signed  [22:0]  im_y5,  re_x5;
    reg signed  [22:0]  im_y6,  re_x6;
    reg signed  [22:0]  im_y7,  re_x7;
    reg signed  [22:0]  im_y8,  re_x8;
    // reg signed  [22:0]  im_y9,  re_x9;   // 最后一次的im_y9实际用不上
    reg signed  [22:0]  re_x9;

    // 对于所有的angle_i,即使全部进行加法,结果最大为2270,加上符号位,需要13bit足矣
    reg signed  [12:0]  angle_zr;    
    reg signed  [12:0]  angle_z0;    
    reg signed  [12:0]  angle_z1;    
    reg signed  [12:0]  angle_z2;    
    reg signed  [12:0]  angle_z3;    
    reg signed  [12:0]  angle_z4;    
    reg signed  [12:0]  angle_z5;    
    reg signed  [12:0]  angle_z6;    
    reg signed  [12:0]  angle_z7;    
    reg signed  [12:0]  angle_z8;    
    reg signed  [12:0]  angle_z9;    

//--------------------------- 象限判断 ----------------------------//
 
    // 按照当前的计算逻辑，二三象限的坐标会按照坐标原点进行对称.
    // 所以当x<0即第二三象限时,要对相位进行180°的补偿
    // 当x=0时分两种情况:
    //      (1)y>0,则相位为90°;y<0,则相位为270°
    //      (2)y=0,则相位为0°.
    //  当16次迭代完成时,re_x15的值应当为原始向量模长的1.67倍;如果re_x15=0,说明原始坐标在原点,相位为0.
    reg [0:0]   x_sign  [NUM_Pipeline:0];
    reg [0:0]   y_sign  [NUM_Pipeline:0];

    integer i;
    // 对输入的实部数据符号位进行缓存,用于迭代结束后相位补偿的判断依据.
    always@(posedge i_clk_108M or negedge i_rst_n)
    begin : qudrant_x_leq_0
        if(i_rst_n == 1'b0)begin 
            for(i=0;i<NUM_Pipeline+1;i=i+1)begin:quadrant_init
                x_sign[i] <= 1'b0;
                y_sign[i] <= 1'b0;
            end
        end
        else if(i_clk_27M_en == 1'b1)begin 
            x_sign[0] <= iv_re_data[12];
            y_sign[0] <= iv_im_data[12];
            for(i=1;i<NUM_Pipeline+1;i=i+1)begin:quadrant_record
                x_sign[i] <= x_sign[i-1];
                y_sign[i] <= y_sign[i-1];
            end
        end
    end

// ------------------------ 确定坐标象限---------------------------//
    
    // 如果是二三象限，超过了Cordic算法计算的角度范围,所以需要按照原点对称，计算后再进行相位的补偿
    always@(posedge i_clk_108M or negedge i_rst_n)
    begin 
        if(i_rst_n == 1'b0)begin 
            re_xr <= 23'd0;
            im_yr <= 23'd0;
            angle_zr <= 13'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin
            if((iv_im_data == 13'd0)&&(iv_re_data == 13'd0))begin 
                re_xr <= 23'd0;
                im_yr <= 23'd0;
                angle_zr <= 13'd0;
            end
            else if(iv_re_data[12] == 1'b1)begin                // 二三象限,x<0
                re_xr <= ((~iv_re_data + 1'b1) <<< NUM_Pipeline);         // 取相反数，并左移10位
                im_yr <= ((~iv_im_data + 1'b1) <<< NUM_Pipeline);         // 该计算方式对正负数均成立
                angle_zr <= 13'd0;
            end
            else begin          // 一四象限,x>0,保持不变,直接进行移位操作
                re_xr <= (iv_re_data <<< 10);
                im_yr <= (iv_im_data <<< 10);
                angle_zr <= 13'd0;
            end
        end
    end

//------------------------ 10级流水迭代计算 ---------------------------//
    
    // 第1次迭代
    always@(posedge i_clk_108M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)begin 
            re_x0 <= 23'd0;
            im_y0 <= 23'd0;
            angle_z0 <= 13'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin
            if(im_yr[22] == 1'b1)begin         // y<0
                re_x0 <= re_xr - im_yr;
                im_y0 <= im_yr + re_xr;
                angle_z0 <= angle_zr - angle_0;
            end
            else begin              // y>0 
                re_x0 <= re_xr + im_yr;
                im_y0 <= im_yr - re_xr;
                angle_z0 <= angle_zr + angle_0;
            end
        end
    end

    // 第2次迭代
    always@(posedge i_clk_108M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)begin
            re_x1 <= 23'd0;
            im_y1 <= 23'd0;
            angle_z1 <= 13'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin
            if(im_y0[22] == 1'b1)begin
                re_x1 <= re_x0 - (im_y0 >>> 1);
                im_y1 <= im_y0 + (re_x0 >>> 1);
                angle_z1 <= angle_z0 - angle_1;
            end
            else begin              // y>0
                re_x1 <= re_x0 + (im_y0 >>> 1);
                im_y1 <= im_y0 - (re_x0 >>> 1);
                angle_z1 <= angle_z0 + angle_1;
            end
        end
    end

    // 第3次迭代
    always@(posedge i_clk_108M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)begin
            re_x2 <= 23'd0;
            im_y2 <= 23'd0;
            angle_z2 <= 13'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin
            if(im_y1[22] == 1'b1)begin
                re_x2 <= re_x1 - (im_y1 >>> 2);
                im_y2 <= im_y1 + (re_x1 >>> 2);
                angle_z2 <= angle_z1 - angle_2;
            end
            else begin              // y>0
                re_x2 <= re_x1 + (im_y1 >>> 2);
                im_y2 <= im_y1 - (re_x1 >>> 2);
                angle_z2 <= angle_z1 + angle_2;
            end
        end
    end

    // 第4次迭代
    always@(posedge i_clk_108M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)begin
            re_x3    <= 23'd0;
            im_y3    <= 23'd0;
            angle_z3 <= 13'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin
            if(im_y2[22] == 1'b1)begin
                re_x3    <= re_x2 - (im_y2 >>> 3);
                im_y3    <= im_y2 + (re_x2 >>> 3);
                angle_z3 <= angle_z2 - angle_3;
            end
            else begin              // y>0
                re_x3    <= re_x2 + (im_y2 >>> 3);
                im_y3    <= im_y2 - (re_x2 >>> 3);
                angle_z3 <= angle_z2 + angle_3;
            end
        end
    end

    // 第5次迭代
    always@(posedge i_clk_108M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)begin
            re_x4    <= 23'd0;
            im_y4    <= 23'd0;
            angle_z4 <= 13'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin
            if(im_y3[22] == 1'b1)begin
                re_x4    <= re_x3 - (im_y3 >>> 4);
                im_y4    <= im_y3 + (re_x3 >>> 4);
                angle_z4 <= angle_z3 - angle_4;
            end
            else begin              // y>0
                re_x4    <= re_x3 + (im_y3 >>> 4);
                im_y4    <= im_y3 - (re_x3 >>> 4);
                angle_z4 <= angle_z3 + angle_4;
            end
        end
    end

    // 第6次迭代
    always@(posedge i_clk_108M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)begin
            re_x5    <= 23'd0;
            im_y5    <= 23'd0;
            angle_z5 <= 13'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin
            if(im_y4[22] == 1'b1)begin
                re_x5    <= re_x4 - (im_y4 >>> 5);
                im_y5    <= im_y4 + (re_x4 >>> 5);
                angle_z5 <= angle_z4 - angle_5;
            end
            else begin              // y>0
                re_x5    <= re_x4 + (im_y4 >>> 5);
                im_y5    <= im_y4 - (re_x4 >>> 5);
                angle_z5 <= angle_z4 + angle_5;
            end
        end
    end

    // 第7次迭代
    always@(posedge i_clk_108M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)begin
            re_x6    <= 23'd0;
            im_y6    <= 23'd0;
            angle_z6 <= 13'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin
            if(im_y5[22] == 1'b1)begin
                re_x6    <= re_x5 - (im_y5 >>> 6);
                im_y6    <= im_y5 + (re_x5 >>> 6);
                angle_z6 <= angle_z5 - angle_6;
            end
            else begin              // y>0
                re_x6    <= re_x5 + (im_y5 >>> 6);
                im_y6    <= im_y5 - (re_x5 >>> 6);
                angle_z6 <= angle_z5 + angle_6;
            end
        end
    end

    // 第8次迭代
    always@(posedge i_clk_108M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)begin
            re_x7    <= 23'd0;
            im_y7    <= 23'd0;
            angle_z7 <= 13'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin
            if(im_y6[22] == 1'b1)begin
                re_x7    <= re_x6 - (im_y6 >>> 7);
                im_y7    <= im_y6 + (re_x6 >>> 7);
                angle_z7 <= angle_z6 - angle_7;
            end
            else begin              // y>0
                re_x7    <= re_x6 + (im_y6 >>> 7);
                im_y7    <= im_y6 - (re_x6 >>> 7);
                angle_z7 <= angle_z6 + angle_7;
            end
        end
    end

    // 第9次迭代
    always@(posedge i_clk_108M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)begin
            re_x8    <= 23'd0;
            im_y8    <= 23'd0;
            angle_z8 <= 13'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin
            if(im_y7[22] == 1'b1)begin
                re_x8    <= re_x7 - (im_y7 >>> 8);
                im_y8    <= im_y7 + (re_x7 >>> 8);
                angle_z8 <= angle_z7 - angle_8;
            end
            else begin              // y>0
                re_x8    <= re_x7 + (im_y7 >>> 8);
                im_y8    <= im_y7 - (re_x7 >>> 8);
                angle_z8 <= angle_z7 + angle_8;
            end
        end
    end

    // 第10次迭代
    always@(posedge i_clk_108M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)begin
            re_x9    <= 23'd0;
            // im_y9    <= 23'd0;
            angle_z9 <= 13'd0;
        end
        else if(i_clk_27M_en == 1'b1)begin
            if(im_y8[22] == 1'b1)begin
                re_x9    <= re_x8 - (im_y8 >>> 9);
                // im_y9    <= im_y8 + (re_x8 >>> 9);
                angle_z9 <= angle_z8 - angle_9;
            end
            else begin              // y>0
                re_x9    <= re_x8 + (im_y8 >>> 9);
                // im_y9    <= im_y8 - (re_x8 >>> 9);
                angle_z9 <= angle_z8 + angle_9;
            end
        end
    end

//-------------------------- 修正相位值 -----------------------------//

/*  (x_sign,y_sign)           象限和相位补偿
            2'b00       :   第一象限，不需要修正
            2'b10       :   第二象限，需要180°的修正
            2'b11       :   第三象限，需要180°的修正
            2'b01       :   第四象限，需要360°的修正        
        
        对于计算出的有符号数相位值，采取直接补零操作可以得到正确的相位,个人觉得原因在于:
        对于一个补码表示的负数,补码A+原码B+1=最高符号位的权值所对应的无符号数C,
        因此如果直接在数据前面补零,以上所有的数据(不包含符号位)都会被当作无符号数对待,故A=C-B-1.
        比如,设A=-1024=13'b1_1100_0000_0000(补码),反码为13'b1_0011_1111_1111,加1为13'b1_0100_0000_0000(原码).
        {1'b0,A} = {1'b0,13'b1_1100_0000_0000} = 2^13 - 'b0011_1111_1111 - 1 = 8192-1023-1=7168.
        但直接采取补零对实虚部均为0的情况会出现误判,所以需要额外处理.
*/
    reg [13:0]  angle_tan;

    // 对计算的相位进行补偿
    always@(posedge i_clk_108M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            angle_tan <= 13'd0;
        else if(i_clk_27M_en == 1'b1)begin 
            if(re_x9 == 23'd0)                             // 实部为0而虚部不为0,则相位为90°;如果迭代完成时实部仍为0,则该向量模长为0,无意义,相位为0.
                angle_tan <= 13'd0;
            else begin
                case({x_sign[NUM_Pipeline],y_sign[NUM_Pipeline]})
                    2'b00 : angle_tan <= {angle_z9[12],angle_z9};                   // 第一象限，无需进行相位补偿
                    2'b10 : angle_tan <= {angle_z9[12],angle_z9} + 14'd4096;        // 第二象限，进行180°的相位补偿
                    2'b11 : angle_tan <= {angle_z9[12],angle_z9} + 14'd4096;        // 第三象限，进行180°的相位补偿
                    2'b01 : angle_tan <= {1'b0,angle_z9};                           // 第四象限，进行360°的相位补偿
                    default : angle_tan <= angle_tan;                               // 其他情况下，保持当前相位不变
                endcase
            end
        end
        else
            angle_tan <= angle_tan;
    end

    assign  ov_angle_tan = angle_tan;

endmodule
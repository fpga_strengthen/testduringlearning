`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////

// Create Date: 2024/08/22 15:19:01
// Module Name: udp_layer_top
// Description: UDP顶层
//////////////////////////////////////////////////////////////////////////////////


module udp_layer_top#(
	parameter	LOCAL_PORT  = 16'h0,	// 本地端口号
	parameter 	TARGET_PORT = 16'h0		// 目的端口号
	)(
	// 时钟
	input wire			i_app_tx_clk,
	input wire			i_app_rx_clk,
	// 复位
	input wire			i_app_rx_rst_n,
	input wire			i_app_tx_rst_n,

	//---------------- UDP端 ----------------//
	input wire			i_udp_rx_vld,			// 数据有效标志
	input wire			i_udp_rx_data_last,		// 数据部分最后一个
	input wire	[7:0]	iv_udp_rx_data,			// UDP层数据
	input wire	[15:0]	iv_udp_rx_length,		// UDP层数据包长度

	output wire			o_udp_tx_data_vld,		// 数据有效信号
	output wire			o_udp_tx_data_last,		// UDP层数据的最后一个数据的标志
	output wire	[7:0]	ov_udp_tx_data,			// 将用户端的数据打包后发送
	output wire	[15:0]	ov_udp_tx_length,		// 帧数据长度

	//---------------- 用户端 ----------------//
	input wire			i_app_tx_data_vld,		// 数据有效标志
	input wire			i_app_tx_data_last,		// 数据部分最后一个
	input wire	[7:0]	iv_app_tx_data,			// UDP层数据
	input wire	[15:0]	iv_app_tx_length,		// UDP层数据包长度

	output wire			o_app_rx_data_vld,
	output wire			o_app_rx_data_last,
	output wire	[7:0]	ov_app_rx_data,
	output wire	[15:0]	ov_app_rx_length,

	output wire			o_app_tx_req,			// 用户端发送情趣
	output wire			o_app_tx_ready			// 用户端准备完成信号
);

	udp_receive #(
		.LOCAL_PORT (LOCAL_PORT)
	) U_udp_rx (
		.i_clk              (i_app_rx_clk),
		.i_rst_n            (i_app_rx_rst_n),
		.i_udp_rx_vld       (i_udp_rx_vld),
		.i_udp_rx_data_last (i_udp_rx_data_last),
		.iv_udp_rx_data     (iv_udp_rx_data),
		.iv_udp_rx_length   (iv_udp_rx_length),

		.o_app_rx_data_vld  (o_app_rx_data_vld),
		.o_app_rx_data_last (o_app_rx_data_last),
		.ov_app_rx_data     (ov_app_rx_data),
		.ov_app_rx_length   (ov_app_rx_length)
	);


	udp_send #(
		.LOCAL_PORT  (LOCAL_PORT),
		.TARGET_PORT (TARGET_PORT)
	) U_udp_tx (
		.i_clk              (i_app_tx_clk),
		.i_rst_n            (i_app_tx_rst_n),

		.o_udp_tx_data_vld  (o_udp_tx_data_vld),
		.o_udp_tx_data_last (o_udp_tx_data_last),
		.ov_udp_tx_data     (ov_udp_tx_data),
		.ov_udp_tx_length   (ov_udp_tx_length),

		.i_app_tx_data_vld  (i_app_tx_data_vld),
		.i_app_tx_data_last (i_app_tx_data_last),
		.iv_app_tx_data     (iv_app_tx_data),
		.iv_app_tx_length   (iv_app_tx_length),
		
		.o_app_tx_req       (o_app_tx_req),
		.o_app_tx_ready     (o_app_tx_ready)
	);


endmodule

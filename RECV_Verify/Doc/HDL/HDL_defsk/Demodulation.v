module Demodulation
  (
  //----------------------------------------------
  //时钟和复位
  input        i_clk_81M,
  input        i_clk_27M_en,
  input        i_rst_n,
  
  input [11:0] i_ADC_data,
  input        i_demod_en,
  
  output reg   o_demod_result,
  output reg   o_demod_result_sck//81M时钟沿
  );
  
  //------------------------------------------变量声明-------------------------------------------
  reg [6:0]cnt_ctrl;
  reg [6:0]addr_wt;
  reg [6:0]addr_rd;
  wire [11:0] datain_used;
  reg [5:0] addr_rd_rom;
  wire [18:0] bit0_sin;
  wire [18:0] bit0_cos;
  wire [18:0] bit1_sin;
  wire [18:0] bit1_cos;
  wire [29:0] num_bit0_sin;
  reg signed[52:0] sum_bit0_sin;
  reg signed [17:0] sum_bit0_sin_use;
  wire [29:0] num_bit0_cos;
  reg signed[52:0]sum_bit0_cos;
  reg signed [17:0]sum_bit0_cos_use;
  wire [29:0]num_bit1_sin;
  reg signed[52:0]sum_bit1_sin;
  reg signed [17:0]sum_bit1_sin_use;
  wire [29:0]num_bit1_cos;
  reg signed[52:0]sum_bit1_cos;
  reg signed [17:0]sum_bit1_cos_use;
  wire [35:0]bit0_sin_square;
  wire [35:0]bit0_cos_square;
  wire [35:0]bit1_sin_square;
  wire [35:0]bit1_cos_square;
  reg [36:0] sum_bit0_square;
  reg [36:0] sum_bit1_square;
  
 
  
  //------------------------------------------模块主体-------------------------------------------
  //控制解调的整个流程，采用流水线的工作方式，整个解调过程需要进行一系列的顺序运算，通过cnt_ctrl的值来控制进行相应操作
  
  //i_demod_en取沿
  reg demod_en_r1;
  reg demod_en_r2;
  reg demod_en_p;
   always @ ( negedge i_rst_n or posedge i_clk_81M )
  begin
    if ( !i_rst_n ) begin
        demod_en_r1 <= 1'd0;
        demod_en_r2 <= 1'd0;
        demod_en_p <= 1'd0;
    end
    else begin
        demod_en_r1 <= i_demod_en;
        demod_en_r2 <= demod_en_r1;
        demod_en_p <= demod_en_r1 & ~demod_en_r2;
    end
  end
  
  always @ ( negedge i_rst_n or posedge i_clk_81M )
  begin
    if ( !i_rst_n )
        cnt_ctrl <= 7'd0;
    else if(demod_en_p)
        cnt_ctrl <= 7'd1;
    else if (cnt_ctrl == 7'd52)
        cnt_ctrl <= 7'd0;
    else if (cnt_ctrl)
        cnt_ctrl <= cnt_ctrl + 1'b1;
    else
        cnt_ctrl <= cnt_ctrl;
  end
  
  
  //ram写地址，ADC采样数据以27M采样速率写入ram
  //reg [5:0]  addr_wt;
  always @ ( negedge i_rst_n or posedge i_clk_81M )
  begin
    if ( !i_rst_n )
        addr_wt <= 7'd0;
    else if(i_clk_27M_en) begin
        if ( addr_wt==7'd80 )
            addr_wt <= 7'd0;
        else
            addr_wt <= addr_wt + 1'b1;
    end
    else
        addr_wt <= addr_wt;
  end
  //在cnt_ctrl控制下，读取ram中的数据进行解调
  //reg [5:0]  addr_rd;     
  //读ram数据的初始地址应该重新考虑？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？
  always @ (negedge i_rst_n or posedge i_clk_81M)
  begin
    if ( !i_rst_n )
        addr_rd <= 7'd0;
    else if (cnt_ctrl==7'd0) begin
        if (addr_wt==7'd80)
            addr_rd <= 7'd0;
        else
            addr_rd <= addr_wt + 1'b1;
    end
    else if ( addr_rd==7'd80 )
        addr_rd <= 7'd0;
    else
        addr_rd <= addr_rd + 1'b1;
  end
  
  //存储ADC采样数据的RAM
  ram_datain_store uu_ram_datain_store(
                   .data(i_ADC_data),
                   .rdaddress(addr_rd),
                   .rdclock(i_clk_81M),
                   .wraddress(addr_wt),
                   .wrclock(~i_clk_27M_en),
                   .wren(1'b1),
                   .q(datain_used)
                   );
  //rom读地址
  always @ (negedge i_rst_n or posedge i_clk_81M)
    if ( !i_rst_n ) 
      addr_rd_rom <= 6'd0;
    else if ( cnt_ctrl==7'd0 )
      addr_rd_rom <= 6'd0;
    else if ( addr_rd_rom==6'd47 )
      addr_rd_rom <= 6'd0;
    else
      addr_rd_rom <= addr_rd_rom + 1'b1;
  
  //---------------------------------------------------------
  //存储本地载波的ROM 
  //rom中的数据长度为什么要用19位？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？
  rom_bit0_sin uu_rom_bit0_sin(
                  .address(addr_rd_rom),
                  .clock(i_clk_81M),
                  .q(bit0_sin)
                  );
  
  rom_bit0_cos uu_rom_bit0_cos(
                  .address(addr_rd_rom),
                  .clock(i_clk_81M),
                  .q(bit0_cos)
                  );
  
  rom_bit1_sin uu_rom_bit1_sin(
                  .address(addr_rd_rom),
                  .clock(i_clk_81M),
                  .q(bit1_sin)
                  );
  
  rom_bit1_cos uu_rom_bit1_cos(
                  .address(addr_rd_rom),
                  .clock(i_clk_81M),
                  .q(bit1_cos)
                  );
                  
                  
  //------------------------------------------------------------------
  //乘法器 sin(bit0) x data
  mult_filter uu_mult_filter_bit0_sin(
                    .dataa(bit0_sin[17:0]),
                    .datab(datain_used),
                    .result(num_bit0_sin)
                    );
  
  //sin(bit0)*data的结果48点求和
  always @ ( negedge i_rst_n or posedge i_clk_81M )
  begin
    if ( !i_rst_n ) 
        sum_bit0_sin <= 37'd0;
    else if ( cnt_ctrl==7'd0 )
        sum_bit0_sin <= 37'd0;
    else if ( cnt_ctrl>=7'd2 && cnt_ctrl<=7'd49 ) begin
        if ( bit0_sin[18]==1'b0 )
            sum_bit0_sin <= sum_bit0_sin + num_bit0_sin;
        else
            sum_bit0_sin <= sum_bit0_sin - num_bit0_sin;
    end
    else
      sum_bit0_sin <= sum_bit0_sin;
  end
  //48点求和结果截短
  //为什么截短19位而不是18位？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？？
  always @ ( negedge i_rst_n or posedge i_clk_81M)
  begin
    if ( !i_rst_n )
        sum_bit0_sin_use <= 18'b0;
    else
        sum_bit0_sin_use <= {sum_bit0_sin[36],sum_bit0_sin[35:19]};
  end
  
  
  
  //-----------------------------------------------------------------------
  //乘法器 cos(bit0) * data
  mult_filter uu_mult_filter_bit0_cos(
                    .dataa(bit0_cos[17:0]),
                    .datab(datain_used),
                    .result(num_bit0_cos)
                    );
  //cos(bit0)*data的结果48点求和
  always @ ( negedge i_rst_n or posedge i_clk_81M ) 
  begin
    if ( !i_rst_n ) 
        sum_bit0_cos <= 37'd0;
    else if ( cnt_ctrl==7'd0 )
        sum_bit0_cos <= 37'd0;
    else if ( cnt_ctrl>=7'd2 && cnt_ctrl<=7'd49 ) begin
        if ( bit0_cos[18]==1'b0 )
            sum_bit0_cos <= sum_bit0_cos + num_bit0_cos;
        else
            sum_bit0_cos <= sum_bit0_cos - num_bit0_cos;  
    end
    else
        sum_bit0_cos <= sum_bit0_cos;
  end
  //48点求和结果截短
  always @ ( negedge i_rst_n or posedge i_clk_81M)
  begin
    if ( !i_rst_n )
        sum_bit0_cos_use <= 18'b0;
    else
        sum_bit0_cos_use <= {sum_bit0_cos[36],sum_bit0_cos[35:19]};
  end
  
  //-----------------------------------------------------------------------
  //乘法器 sin(bit1) * data
  mult_filter uu_mult_filter_bit1_sin(
                    .dataa(bit1_sin[17:0]),
                    .datab(datain_used),
                    .result(num_bit1_sin)
                    );
  //sin(bit1)*data的结果48点求和
  always @ ( negedge i_rst_n or posedge i_clk_81M )
  begin
    if ( !i_rst_n ) 
        sum_bit1_sin <= 37'd0;
    else if ( cnt_ctrl==7'd0 )
        sum_bit1_sin <= 37'd0;
    else if ( cnt_ctrl>=7'd2 && cnt_ctrl<=7'd49 )
        if ( bit1_sin[18]==1'b0 )
            sum_bit1_sin <= sum_bit1_sin + num_bit1_sin;
        else
            sum_bit1_sin <= sum_bit1_sin - num_bit1_sin;
    else
        sum_bit1_sin <= sum_bit1_sin;
  end
   
  //48点求和结果截短
  always @ ( negedge i_rst_n or posedge i_clk_81M)
  begin
    if ( !i_rst_n )
        sum_bit1_sin_use <= 18'b0;
    else
        sum_bit1_sin_use <= {sum_bit1_sin[36],sum_bit1_sin[35:19]};
  end
  
  //-----------------------------------------------------------------------
  //乘法器 cos(bit1) * data
  mult_filter uu_mult_filter_bit1_cos(
                    .dataa(bit1_cos[17:0]),
                    .datab(datain_used),
                    .result(num_bit1_cos)
                    );
  //cos(bit1)*data的结果48点求和
  always @ ( negedge i_rst_n or posedge i_clk_81M )
  begin
    if ( !i_rst_n ) 
        sum_bit1_cos <= 37'd0;
    else if ( cnt_ctrl==7'd0 )
        sum_bit1_cos <= 37'd0;
    else if ( cnt_ctrl>=7'd2 && cnt_ctrl<=7'd49 ) begin
        if ( bit1_cos[18]==1'b0 )
            sum_bit1_cos <= sum_bit1_cos + num_bit1_cos;
        else
            sum_bit1_cos <= sum_bit1_cos - num_bit1_cos;
    end
    else
        sum_bit1_cos <= sum_bit1_cos;
  end

   //48点求和结果截短
  always @ ( negedge i_rst_n or posedge i_clk_81M)
  begin
    if ( !i_rst_n )
        sum_bit1_cos_use <= 18'b0;
    else
        sum_bit1_cos_use <= {sum_bit1_cos[36],sum_bit1_cos[35:19]};
  end
  
  
  //截短后的48点求和结果平方
  mult_square uu_mult_square_bit0_sin(
                    .dataa(sum_bit0_sin_use),
                    .datab(sum_bit0_sin_use),
                    .result(bit0_sin_square)
                    );
  
  mult_square uu_mult_square_bit0_cos(
                    .dataa(sum_bit0_cos_use),
                    .datab(sum_bit0_cos_use),
                    .result(bit0_cos_square)
                    );
  
  mult_square uu_mult_square_bit1_sin(
                    .dataa(sum_bit1_sin_use),
                    .datab(sum_bit1_sin_use),
                    .result(bit1_sin_square)
                    );
  
  mult_square uu_mult_square_bit1_cos(
                    .dataa(sum_bit1_cos_use),
                    .datab(sum_bit1_cos_use),
                    .result(bit1_cos_square)
                    );
  
  //对应平方结果求和
  always @ ( negedge i_rst_n or posedge i_clk_81M )
    if ( !i_rst_n )
      sum_bit0_square <= 37'd0;
    else if ( cnt_ctrl==7'd50 )
      sum_bit0_square <= bit0_cos_square + bit0_sin_square;
    else
      sum_bit0_square <= sum_bit0_square;
  
  always @ ( negedge i_rst_n or posedge i_clk_81M )
    if ( !i_rst_n )
      sum_bit1_square <= 37'd0;
    else if ( cnt_ctrl==7'd50 )
      sum_bit1_square <= bit1_cos_square + bit1_sin_square;
    else
      sum_bit1_square <= sum_bit1_square;
  
  //将相干解调结果判决为0，1bit
  always @ ( negedge i_rst_n or posedge i_clk_81M )
  begin
    if ( !i_rst_n ) begin
      o_demod_result <= 1'b0;
      o_demod_result_sck <= 1'b0;
    end
    else if ( cnt_ctrl==7'd51 ) begin
      o_demod_result <= ( sum_bit0_square > sum_bit1_square )?1'b0:1'b1;
      o_demod_result_sck <= 1'b1;
    end
    else begin
      o_demod_result <= o_demod_result;
      o_demod_result_sck <= 1'd0;
    end
  end
  endmodule
    
    
    
    
    
    
  
module data_signed();

    reg     clk;
    reg     rst_n;

    reg [7:0]   data_mem [255:0];

    initial begin
        clk = 1'b0;
        rst_n <= 1'b0;
        #30
        rst_n <= 1'b1;
    end

    always #10 clk = ~clk;



endmodule
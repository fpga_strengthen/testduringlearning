--------------------------------------------------------------
-- 32 bit micro for SHA-1 Algorithm
--
-- Hamish Rawnsley June 2007
--------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_signed.all;
use ieee.std_logic_arith.conv_std_logic_vector;

entity small_micro_32 is
	generic (SECRET_KEY		: std_logic_vector(63 downto 0) := X"5555555555555555");
	port (	
			resetn			: in std_logic;
			clock			: in std_logic;
			
			OW_Command		: out std_logic_vector(3 downto 0);
			OW_Write_Byte	: out std_logic_vector(7 downto 0);
			RNG_Byte		: in std_logic_vector(7 downto 0);
			OW_Read_Byte	: in std_logic_vector(7 downto 0);
			OW_Busy			: in std_logic;
			OW_No_Device	: out std_logic;
			Enable			: out std_logic
			);
end small_micro_32;

architecture arch of small_micro_32 is

	constant PROGRAM_ITER : integer := 79; 
		
	component program_mem
	port(
		address			: IN STD_LOGIC_VECTOR (7 DOWNTO 0);
		clock			: IN STD_LOGIC;
		data			: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		wren			: IN STD_LOGIC;
		q				: OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
	);
	end component;
	
	component Adder
	port(
		dataa			: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		datab			: IN STD_LOGIC_VECTOR (31 DOWNTO 0);
		result			: OUT STD_LOGIC_VECTOR (31 DOWNTO 0)
	);
	end component;
	
	type States			is (FETCH, WAIT_S, DECODE_OP, GET, GET_Wt, GET_Kt, LOAD, LOAD_Wt, ADD,
							SHIFT, COMPARE, STOP, FUNCT_T, FUNCT_XOR, 
							OW_RESET, NO_DEVICE, OW_WRITE, OW_READ, OW_WAIT, T_INCR);		
	subtype pc_type 	is integer range 0 to 255;
	subtype iter_type 	is integer range 0 to 127;     
		
    type reg_type is record
		Instr_Reg		: std_logic_vector(15 downto 0);
		Reg1			: std_logic_vector(31 downto 0);
		Reg2			: std_logic_vector(31 downto 0);
		Reg3			: std_logic_vector(31 downto 0);
		Enable_reg		: std_logic_vector(4 downto 0);
		P_Counter		: pc_type;
		T_Counter		: iter_type;
		FSM				: States;		    	
    end record;
    
    signal 	r, rIn      : reg_type;

	signal  PC_rst, 
			PC_ena,
			PC_jump,
			TC_ena,
			Mem_wren	: std_logic;
			
	signal  Mem_datain,
			Mem_dataout,
			Add_result  : std_logic_vector(31 downto 0);
			
	signal  T_vector	: std_logic_vector(6 downto 0);
	signal	Mem_addr,
			Wt_addr_0,
			Wt_addr_14,
			Wt_addr_8,
			Wt_addr_3	: std_logic_vector(7 downto 0);	
	signal  Mem_space	: std_logic_vector(7 downto 0);
	signal  Prog_space	: std_logic_vector(3 downto 0);
	signal  Op_code		: std_logic_vector(3 downto 0);
	signal  Prog_addr	: std_logic_vector(7 downto 0);
	signal  PC_offset	: pc_type; 
	
begin
	
	Mem_space			<= r.Instr_Reg(11 downto 4); 	
	Prog_space			<= r.Instr_Reg(3 downto 0); 	
	Op_code				<= r.Instr_Reg(15 downto 12); 	
	PC_offset			<= r.P_Counter + 32;
	Prog_addr 			<= conv_std_logic_vector(PC_offset, 8); 
	T_vector			<= conv_std_logic_vector(r.T_Counter, 7);
	Wt_addr_0			<= "0001" & T_vector(3 downto 0);
	Wt_addr_14			<= "0001" & (T_vector(3 downto 0) + "0010");
	Wt_addr_8			<= "0001" & (T_vector(3 downto 0) + "1000");
	Wt_addr_3			<= "0001" & (T_vector(3 downto 0) + "1101");
		
	
	Enable				<= r.Enable_reg(4) AND r.Enable_reg(3) AND r.Enable_reg(2) AND r.Enable_reg(1) AND r.Enable_reg(0);
			
	
	Mem_inst : program_mem
	port map (
		address			=> Mem_addr,
		clock			=> clock,
		data			=> Mem_datain,
		wren			=> Mem_wren,
		q				=> Mem_dataout
		);
		
	Reg_add : Adder
	port map(
		dataa			=> r.Reg1,
		datab			=> r.Reg2,
		result			=> Add_result
	);
	
	
	main : process (r, Add_result, Mem_dataout, Mem_space, 
					Prog_space, Op_code, Prog_addr, RNG_Byte,
					Wt_addr_0, Wt_addr_14, Wt_addr_8, Wt_addr_3,
					OW_Read_Byte, OW_Busy)
	begin
		PC_ena			<= '0';
		PC_rst			<= '0';
		PC_jump			<= '0';
		TC_ena			<= '0';
		Mem_addr 		<= (others => '0');
		rIn.Instr_Reg	<= r.Instr_Reg;
		rIn.Reg1		<= r.Reg1;
		rIn.Reg2		<= r.Reg2;
		rIn.Reg3		<= r.Reg3;
		rIn.Enable_reg	<= r.Enable_reg;
		rIn.FSM			<= r.FSM;
		Mem_wren		<= '0';
		Mem_datain		<= (others => '0');
		OW_Command		<= (others => '0');
		OW_Write_Byte	<= (others => '0');
		OW_No_Device	<= '0';
		case (r.FSM) is
			when FETCH =>
				Mem_addr 				<= Prog_addr;
				rIn.Instr_Reg			<= Mem_dataout(15 downto 0);
				rIn.FSM					<= WAIT_S;	
			
			when WAIT_S => 
				Mem_addr 				<= Prog_addr;
				rIn.Instr_Reg			<= Mem_dataout(15 downto 0);
				rIn.FSM					<= DECODE_OP;			
			
			when DECODE_OP =>
				case (Op_code) is
					when X"1" => 
						rIn.FSM			<= GET;
						Mem_addr 		<= Mem_space;
					when X"2" => 
						rIn.FSM			<= LOAD;
						Mem_addr 		<= Mem_space;
					when X"3" => 
						rIn.FSM			<= ADD;
					when X"4" => 
						rIn.FSM			<= SHIFT;
					when X"5" => 
						rIn.Enable_reg	<= r.Enable_reg(3 downto 0) & '0';
						rIn.FSM			<= COMPARE;
					when X"6" => 
						rIn.FSM			<= STOP;
					when X"7" => 
						rIn.FSM			<= FUNCT_T;
					when X"8" => 
						rIn.FSM			<= GET_Wt;
						if (Mem_space = X"14") then
							Mem_addr	<= Wt_addr_14;
						elsif (Mem_space = X"08") then
							Mem_addr	<= Wt_addr_8;
						elsif (Mem_space = X"03") then
							Mem_addr	<= Wt_addr_3;
						else
							Mem_addr	<= Wt_addr_0;
						end if;
					when X"9" => 
						rIn.FSM			<= FUNCT_XOR;
					when X"A" => 
						rIn.FSM			<= LOAD_Wt;	
						Mem_addr 		<= Wt_addr_0;
					when X"B" => 
						rIn.FSM			<= GET_Kt;
						if (r.T_Counter < 20) then
							Mem_addr 	<= "00000101";
						elsif (r.T_Counter < 40) then
							Mem_addr 	<= "00000110";
						elsif (r.T_Counter < 60) then
							Mem_addr 	<= "00000111";
						else	
							Mem_addr 	<= "00001000";
						end if;
					when X"C" => 
						if (Mem_space = X"20") then
							OW_Command	<= "1100";
							rIn.FSM		<= OW_WAIT; 
						else
							OW_Command	<= "1111";
							rIn.FSM		<= OW_RESET;
						end if;
						
					when X"D" => 
						OW_Command		<= "1011";
						case (Prog_space) is
							when X"1" => 
								OW_Write_Byte	<= r.Reg1(7 downto 0);
							when X"2" => 
								OW_Write_Byte	<= r.Reg1(15 downto 8);
							when X"3" => 
								OW_Write_Byte	<= r.Reg1(23 downto 16);
							when X"4" => 
								OW_Write_Byte	<= r.Reg1(31 downto 24);
							when others => 
								OW_Write_Byte	<= r.Instr_Reg(11 downto 4);
						end case;
						rIn.FSM			<= OW_WRITE;
					
					when X"E" => 	
						OW_Command		<= "1001";
						rIn.FSM			<= OW_READ;
										
					when X"F" => 
						rIn.FSM			<= T_INCR;
					when others =>
						rIn.FSM			<= FETCH;
						PC_ena			<= '1';
				end case;
				
			when GET =>
				Mem_addr 				<= Mem_space;
				case (Prog_space) is
					when X"1" => 
						rIn.Reg1		<= Mem_dataout;
					when X"2" => 
						rIn.Reg2		<= Mem_dataout;
					when X"3" => 
						rIn.Reg3		<= Mem_dataout;
					when X"A" => 
						rIn.Reg1		<= SECRET_KEY(7 downto 0) & SECRET_KEY(15 downto 8) & SECRET_KEY(23 downto 16) & SECRET_KEY(31 downto 24);
					when X"B" => 
						rIn.Reg1		<= SECRET_KEY(39 downto 32) & SECRET_KEY(47 downto 40) & SECRET_KEY(55 downto 48) & SECRET_KEY(63 downto 56);
					when X"C" => 
						rIn.Reg1		<= SECRET_KEY(39 downto 32) & SECRET_KEY(47 downto 40) & SECRET_KEY(55 downto 48) & X"80";
					when X"9" => 
						rIn.Reg1		<= SECRET_KEY(23 downto 16) & SECRET_KEY(31 downto 24) & X"FFFF";
					when X"D" => 
						rIn.Reg1(31 downto 24)	<= RNG_Byte;
					when X"E" => 
						rIn.Reg1(23 downto 16)	<= RNG_Byte;
					when X"F" => 
						rIn.Reg1(15 downto 8)	<= RNG_Byte;
					when others =>
						rIn.Reg1		<= r.Reg1;
						rIn.Reg2		<= r.Reg2;
						rIn.Reg3		<= r.Reg3;
				end case;
				rIn.FSM					<= FETCH;
				PC_ena					<= '1';
				
			when GET_Kt =>
				if (r.T_Counter < 20) then
					Mem_addr 	<= "00000101";
				elsif (r.T_Counter < 40) then
					Mem_addr 	<= "00000110";
				elsif (r.T_Counter < 60) then
					Mem_addr 	<= "00000111";
				else	
					Mem_addr 	<= "00001000";
				end if;
				rIn.Reg2				<= Mem_dataout;
				rIn.FSM					<= FETCH;
				PC_ena					<= '1';
				
			when GET_Wt =>
				if (Mem_space = X"14") then
					Mem_addr			<= Wt_addr_14;
				elsif (Mem_space = X"08") then
					Mem_addr			<= Wt_addr_8;
				elsif (Mem_space = X"03") then
					Mem_addr			<= Wt_addr_3;
				else
					Mem_addr			<= Wt_addr_0;
				end if;
				if (Prog_space = X"2") then
					rIn.Reg2			<= Mem_dataout;
				else 
					rIn.Reg3			<= Mem_dataout;
				end if;
				if (r.T_Counter < 16) then
					PC_jump				<= '1';
				else
					PC_ena				<= '1';
				end if;
				rIn.FSM					<= FETCH;
				
			when LOAD =>
				Mem_addr 				<= Mem_space;
				Mem_wren				<= '1';
				case (Prog_space) is
					when X"1" => 
						Mem_datain		<= r.Reg1;
					when X"2" => 
						Mem_datain		<= r.Reg2;
					when X"3" => 
						Mem_datain		<= r.Reg3;
					when X"A" => 
						Mem_datain		<= SECRET_KEY(31 downto 0);
					when X"B" => 
						Mem_datain		<= SECRET_KEY(63 downto 32);
					when others =>
						Mem_datain		<= (others => '0');
				end case;				
				rIn.FSM					<= FETCH;
				PC_ena					<= '1';
			
			when LOAD_Wt =>
				Mem_addr 				<= Wt_addr_0;
				Mem_wren				<= '1';
				Mem_datain				<= r.Reg2;				
				rIn.FSM					<= FETCH;
				PC_ena					<= '1';
				
			when ADD =>
				rIn.Reg1				<= Add_result;
				rIn.FSM					<= FETCH;
				PC_ena					<= '1';
			
			when SHIFT =>
				case (Prog_space) is
					when X"1" => 
						rIn.Reg2		<= r.Reg2(30 downto 0) & r.Reg2(31);
					when X"2" => 
						rIn.Reg2		<= r.Reg2(26 downto 0) & r.Reg2(31 downto 27);
					when X"3" => 
						rIn.Reg2		<= r.Reg2(1 downto 0) & r.Reg2(31 downto 2);
					when others =>
						rIn.Reg2		<= r.Reg2;
				end case;
				rIn.FSM					<= FETCH;
				PC_ena					<= '1';
			
			when COMPARE =>
				if (r.Reg1 = r.Reg2) then
					rIn.Enable_reg(0)	<= '1'; 
				else
					rIn.Enable_reg(0)	<= '0'; 
				end if;
				rIn.FSM					<= FETCH;
				PC_ena					<= '1';	
			
			when STOP =>
				rIn.FSM					<= STOP;
			
			when FUNCT_T =>
				if (r.T_Counter < 20) then
					rIn.Reg1 			<= (r.Reg1 AND r.Reg2) OR ((NOT(r.Reg1)) AND r.Reg3);
				elsif (r.T_Counter < 40) then
					rIn.Reg1 			<= (r.Reg1 XOR r.Reg2) XOR r.Reg3;
				elsif (r.T_Counter < 60) then
					rIn.Reg1 			<= (r.Reg1 AND (r.Reg2 OR r.Reg3)) OR (r.Reg2 AND r.Reg3);
				else	
					rIn.Reg1 			<= (r.Reg1 XOR r.Reg2) XOR r.Reg3;
				end if;
				rIn.FSM					<= FETCH;
				PC_ena					<= '1';
				
			when FUNCT_XOR =>
				rIn.Reg2				<= (r.Reg2 XOR r.Reg3);
				rIn.FSM					<= FETCH;
				PC_ena					<= '1';
				
			when OW_RESET =>
				if (OW_Busy = '1') then
					rIn.FSM				<= OW_RESET;
				else
					if (OW_Read_Byte = "00001111") then
						PC_ena			<= '1';
						rIn.FSM			<= FETCH;	
					else
						rIn.FSM			<= NO_DEVICE;
					end if;
				end if;
				
			when NO_DEVICE =>
				OW_No_Device			<= '1';
				rIn.FSM					<= NO_DEVICE;	
				
			when OW_WRITE =>
				case (Prog_space) is
					when X"1" => 
						OW_Write_Byte	<= r.Reg1(7 downto 0);
					when X"2" => 
						OW_Write_Byte	<= r.Reg1(15 downto 8);
					when X"3" => 
						OW_Write_Byte	<= r.Reg1(23 downto 16);
					when X"4" => 
						OW_Write_Byte	<= r.Reg1(31 downto 24);
					when others => 
						OW_Write_Byte	<= r.Instr_Reg(11 downto 4);
				end case;
				if (OW_Busy = '1') then
					rIn.FSM				<= OW_WRITE;
				else
					PC_ena				<= '1';
					rIn.FSM				<= FETCH;	
				end if;
			
			when OW_READ =>
				if (OW_Busy = '1') then
					rIn.FSM				<= OW_READ;
				else
					case (Prog_space) is
					when X"1" => 
						rIn.Reg1(7 downto 0)	<= OW_Read_Byte;
					when X"2" => 
						rIn.Reg1(15 downto 8)	<= OW_Read_Byte;
					when X"3" => 
						rIn.Reg1(23 downto 16)	<= OW_Read_Byte;
					when X"4" => 
						rIn.Reg1(31 downto 24)	<= OW_Read_Byte;
					when others =>
						rIn.Reg1		<= r.Reg1;
					end case;
					PC_ena				<= '1';
					rIn.FSM				<= FETCH;	
				end if;
				
			when OW_WAIT =>
				if (OW_Busy = '1') then
					rIn.FSM				<= OW_WAIT;
				else
					PC_ena				<= '1';
					rIn.FSM				<= FETCH;	
				end if;									
				
			when T_INCR =>
				if (r.T_Counter >= PROGRAM_ITER) then
					PC_rst				<= '0'; 
					PC_ena				<= '1';
				else
					PC_rst				<= '1'; 
					PC_ena				<= '0';
				end if;
				rIn.FSM					<= FETCH;					
				TC_ena					<= '1';						
			
		end case;		
						
	end process main;
	
	
	regs : process (resetn, clock, r, rIn)
    begin
		if (resetn = '0') then
		
				r.Instr_Reg				<= (others => '0');
				r.Reg1					<= (others => '0');
				r.Reg2					<= (others => '0');
				r.Reg3					<= (others => '0');
				r.Enable_reg			<= (others => '0');
				r.P_Counter				<= 0;
				r.T_Counter				<= 0;
				r.FSM					<= FETCH;
		
        elsif (clock'event and clock = '1') then

				r.FSM					<= rIn.FSM;
				r.Instr_Reg				<= rIn.Instr_Reg;
				r.Reg1					<= rIn.Reg1;
				r.Reg2					<= rIn.Reg2;
				r.Reg3					<= rIn.Reg3;
				r.Enable_reg			<= rIn.Enable_reg;
				
				if (PC_rst = '1') then
					r.P_Counter			<= 98; 
				elsif (PC_jump = '1') then
					r.P_Counter			<= r.P_Counter + 9;				
				elsif (PC_ena = '1') then
					r.P_Counter			<= r.P_Counter + 1;
				else
					r.P_Counter			<= r.P_Counter;			
				end if;
				
				if (TC_ena = '1') then
					r.T_Counter			<= r.T_Counter + 1;
				else
					r.T_Counter			<= r.T_Counter;			
				end if;
	        			
        end if;
    end process regs;
end arch;
-------------------------------------------------------------
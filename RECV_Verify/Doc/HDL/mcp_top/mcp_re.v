//============================================================
//
//--- ALL RIGHTS RESERVED
//--- File		: mcp_re.v
//--- Auther	: smartbai
//--- Data		: 2020-11-18
//--- Version	: v1.0.0.0
//--- Abstract	: 处理CAN接收到的一帧数据
//                1. 目前只判断是否为版本请求帧
//                2. 如需可添加其它判断
//------------------------------------------------------------
//Description	:
//------------------------------------------------------------
//--- Modification History:
//--- Date		By		Version		Change Description
//------------------------------------------------------------
//
//============================================================

module mcp_re (
    i_clk,          //27MHz
    i_rst_n,

    //CAN RX
    i_can_wren,
    i_can_wrclk,
    i_can_reFrameEnd,
    iv_can_wrdata,

    o_tx_type_en       //发送版本号使能, 脉冲信号
);
    input i_clk;          //27MHz
    input i_rst_n;

    //CAN RX
    input i_can_wren;
    input i_can_wrclk;
    input i_can_reFrameEnd;
    input [7:0] iv_can_wrdata;

    output reg o_tx_type_en;       //发送版本号使能, 脉冲信号

//---------------------------------------------------------//
localparam REDATASIZE = 13*8;

//-----------------------内部信号声明-----------------------//
//can接收一帧数据位13字节,参考datasheet P29
reg [REDATASIZE-1:0] re_data;

//---------------------------------------------------------//
//缓存一帧数据
always @(posedge i_clk or negedge i_rst_n) begin
    if(~i_rst_n)
        re_data <= 104'd0;
    else if(i_can_wrclk)
        re_data <= {re_data[(REDATASIZE-8)-1:0], iv_can_wrdata};
    else
        re_data <= re_data;
end

//---------------------------------------------------------//
//等待一帧接收完毕后, 对比本地数据，判断是否为版本请求帧, 若是则使能版本发送使能
always @(posedge i_clk or negedge i_rst_n) begin
    if(~i_rst_n)
        o_tx_type_en <= 1'b0;
    else if(i_can_reFrameEnd)
        o_tx_type_en <= 1'b1;
        // if(re_data[63:0] == 63'b)       //需要修改为具体的值
        //     o_tx_type_en <= 1'b1;
        // else
        //     o_tx_type_en <= 1'b0;
    else
        o_tx_type_en <= 1'b0;
end

endmodule
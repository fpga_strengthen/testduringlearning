//
////---2011.8.15    DanCui          ?????????????????3s??400ms
////---------------------------------------------------------------------------------------------------------
//module shortcheck_new(
//            i_clk_27M,            //--27.095M 
//            i_rst_n,               
//            i_data_bit_defsk,
//            i_clk_defsk,
//            
//            o_m_ready_p
//            );
//            
//input i_clk_27M;
//input i_rst_n;
//input i_data_bit_defsk;
//input i_clk_defsk;
//
//output o_m_ready_p;
//
//////////////////////////////////////////////??1bit???????////////////////////////////////////////////
//wire i_clk_defsk_p;
//reg i_clk_defsk_temp1;
//reg i_clk_defsk_temp2;
//always @(  negedge i_rst_n or posedge i_clk_27M  )
//  if( !i_rst_n )
//    begin
//      i_clk_defsk_temp1 <= 1'b0;
//      i_clk_defsk_temp2 <= 1'b0;
//    end
//  else
//    begin
//      i_clk_defsk_temp1 <= i_clk_defsk ;
//      i_clk_defsk_temp2 <= i_clk_defsk_temp1 ;
//    end
//assign i_clk_defsk_p = i_clk_defsk_temp1 && (!i_clk_defsk_temp2);
//
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////?15bit??????????/////////////////////////////////////////
//reg [14:0]data_temp;  //15bit?????
//reg [4:0]addr_data_temp;
////reg comp_doing; //15bit??????????rom?????
//reg clk_comp; //???15bit????????????????????????
//
//always @(  negedge i_rst_n or posedge i_clk_27M  )
//  if ( !i_rst_n )
//    data_temp <= 15'd0;
//  else if( i_clk_defsk_p ) 
//    data_temp <= {data_temp[13:0],i_data_bit_defsk};
//  else
//    data_temp <= data_temp;
//    
//always @(  negedge i_rst_n or posedge i_clk_27M  )
//  if ( !i_rst_n )
//    addr_data_temp <= 5'd0;
//  else if( i_clk_defsk_p )
//    if( addr_data_temp == 5'd29 ) 
//      addr_data_temp <= 5'd0;
//    else 
//      addr_data_temp <= addr_data_temp + 5'd1;
//  else
//    addr_data_temp <= addr_data_temp;
//    
//always @(  negedge i_rst_n or posedge i_clk_27M  )
//  if ( !i_rst_n )
//    clk_comp <= 1'b0;
//  else if( addr_data_temp == 5'd15 )
//    clk_comp <= 1'b1;
//  else if( addr_data_temp == 5'd0 )
//    clk_comp <= 1'b0;
//  else
//    clk_comp <= clk_comp ;
//    
///////////////////////////////////////////////clk_comp????????///////////////////////////////////////////////
//wire comp_doing_p;
//wire comp_doing_np;
//reg comp_doing_temp1;
//reg comp_doing_temp2;
//always @( negedge i_rst_n or posedge i_clk_27M )
//  if( !i_rst_n )
//    begin
//      comp_doing_temp1 <= 1'b0;
//      comp_doing_temp2 <= 1'b0;
//    end
//  else 
//    begin
//      comp_doing_temp1 <= clk_comp;
//      comp_doing_temp2 <= comp_doing_temp1;
//    end
//
//assign comp_doing_p = comp_doing_temp1 && (!comp_doing_temp2);
//assign comp_doing_np = comp_doing_temp2 && (!comp_doing_temp1);
//    
///////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////???/////////////////////////////////////////////////////////////
//
//parameter IDLE  = 3'd0,
//      COMP_1  = 3'd1,
//      COMP_2  = 3'd2,
//      COMP_3  = 3'd3,
//      WAIT  = 3'd4;
//
//reg [2:0]state;
//
//reg [3:0]addr_rom;
//wire [14:0]rom_data;
//wire [14:0]rom_data_delay;
//
//reg comp1_done;
//reg comp1_ok;
//reg comp2_ok;
//reg comp3_ok;
//
//reg [26:0]wait_send ;
//
//always @( negedge i_rst_n or posedge i_clk_27M )
//  if( !i_rst_n )
//    state <= 3'd0;
//  else
//    case( state )
//      IDLE:
//        if( comp_doing_p || comp_doing_np )
//          state <= COMP_1;
//        else
//          state <= state;
//          
//      COMP_1:
//        if( ( comp_doing_p || comp_doing_np ) && comp1_ok )
//          state <= COMP_2;
//        else if( comp1_done && (!comp1_ok) )
//          state <= IDLE ;
//        else
//          state <= state;
//        
//      COMP_2:
//        if( ( comp_doing_p || comp_doing_np ) && comp2_ok )
//          state <= COMP_3 ;
//        else if( ( rom_data_delay == data_temp ) || comp2_ok )
//          state <= COMP_2;
//        else
//          state <= IDLE ;
//        
//      COMP_3:
//        if( rom_data_delay == data_temp )
//          state <= WAIT ;
//        else
//          state <= IDLE ;
//      
//      WAIT:
////        if( wait_send == 27'd127007812 )//2011.8.15
//        if( wait_send == 27'd10838000 )
//          state <= IDLE ;
//        else
//          state <= state;
//          
//      default:
//        state <= state;
//    endcase
//
///////////////////////////////////////////////??400ms???/////////////////////////////////////
//always @( negedge i_rst_n or posedge i_clk_27M )
//  if( !i_rst_n )
//    wait_send <= 27'd0;
//  else
//    case(state)
//      WAIT:
////        if( wait_send == 27'd127007812 )
//        if( wait_send == 27'd10838000 ) //2011.8.15
//          wait_send <=  27'd0;
//        else
//          wait_send <= wait_send + 27'd1;
//      default:
//        wait_send <= 27'd0;
//    endcase
//
//
//////////////////////////////////////////////????///////////////////////////////////////////////
//always @( negedge i_rst_n or posedge i_clk_27M )
//  if( !i_rst_n )
//    addr_rom <= 4'd0 ;
//  else
//    case( state )
//      IDLE:
//        addr_rom <= 4'd0 ;    
//      COMP_1:
//        if( comp_doing_p || comp_doing_np )
//          addr_rom <= addr_rom ;
//        else if( rom_data == data_temp )
//          addr_rom <= addr_rom ;
//        else if( addr_rom >= 4'd14 )
////        else if( comp1_done || comp1_ok )
//          addr_rom <= addr_rom ;
//        else
//          addr_rom <= addr_rom + 4'd1;
//          
//      COMP_2:
//        if( (rom_data_delay == data_temp) || comp2_ok )
//          addr_rom <= addr_rom ;
//        else
//          addr_rom <= addr_rom ;
//      
//      COMP_3:
//        if( (rom_data_delay == data_temp)  || comp3_ok  )
//          addr_rom <= addr_rom ;
//        else
//          addr_rom <= addr_rom ;
//          
//      WAIT:
//        addr_rom <= 4'd0 ;  
//          
//      default:
//        addr_rom <= addr_rom ;
//    endcase
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////// comp1_done ////////////////////////////////////////////////////////////////
//always @( negedge i_rst_n or posedge i_clk_27M )
//  if( !i_rst_n )   
//    comp1_done <= 1'b0;
//  else
//    case( state )
//      IDLE:
//        comp1_done <= 1'b0;
//      COMP_1:
//        if( comp1_ok || addr_rom >= 4'd14 )
//          comp1_done <= 1'b1 ;
//        else
//          comp1_done <= comp1_done;
//      WAIT:
//        comp1_done <= 1'b0;
//      default:
//        comp1_done <= comp1_done;
//    endcase
//    
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//always @( negedge i_rst_n or posedge i_clk_27M )
//  if( !i_rst_n )
//    begin
//      comp1_ok <= 1'b0 ;
//      comp2_ok <= 1'b0 ; 
//      comp3_ok <= 1'b0 ;
//    end
//  else
//    case( state )
//      IDLE:
//        begin
//          comp1_ok <= 1'b0 ;
//          comp2_ok <= 1'b0 ; 
//          comp3_ok <= 1'b0 ;
//        end 
//        
//      COMP_1:
//        if( rom_data == data_temp )
//          begin
//            comp1_ok <= 1'b1 ;
//            comp2_ok <= comp2_ok ; 
//            comp3_ok <= comp3_ok ;
//          end 
//        else
//          begin
//            comp1_ok <= comp1_ok ;
//            comp2_ok <= comp2_ok ; 
//            comp3_ok <= comp3_ok ;
//          end 
//          
//      COMP_2:
//        if( rom_data_delay == data_temp )
//          begin
//            comp1_ok <= comp1_ok ;
//            comp2_ok <= 1'b1 ; 
//            comp3_ok <= comp3_ok ;
//          end 
//        else
//          begin
//            comp1_ok <= comp1_ok ;
//            comp2_ok <= comp2_ok ; 
//            comp3_ok <= comp3_ok ;
//          end 
//            
//      COMP_3:
//        if( rom_data_delay == data_temp )
//          begin
//            comp1_ok <= comp1_ok ;
//            comp2_ok <= comp2_ok ; 
//            comp3_ok <= 1'b1 ;
//          end 
//        else
//          begin
//            comp1_ok <= comp1_ok ;
//            comp2_ok <= comp2_ok ; 
//            comp3_ok <= comp3_ok ;
//          end 
//          
//      WAIT:
//        begin
//          comp1_ok <= 1'b0 ;
//          comp2_ok <= 1'b0 ; 
//          comp3_ok <= 1'b0 ;
//        end 
//        
//      default:
//        begin
//          comp1_ok <= comp1_ok ;
//          comp2_ok <= comp2_ok ; 
//          comp3_ok <= comp3_ok ;
//        end 
//      
//    endcase
///////////////////////////////////////////////////////////////////////////////////////////////////////////
//reg m_ready_temp1 ;
//reg m_ready_temp2 ;
//wire o_m_ready_p ;
//always @( negedge i_rst_n or posedge i_clk_27M )
//  if( !i_rst_n )
//    begin
//      m_ready_temp1 <= 1'b0 ;
//      m_ready_temp2 <= 1'b0 ;
//    end
//  else
//    begin
//      m_ready_temp1 <= comp3_ok ;
//      m_ready_temp2 <= m_ready_temp1 ;
//    end
//assign o_m_ready_p = m_ready_temp1 && (!m_ready_temp2);
//
//rom_cycle15 uu_rom_cycle15(
//              .address( addr_rom ),
//              .clock( i_clk_27M ),
//              .q( rom_data )
//              );
//
//reg [3:0]addr_rom_delay;
//always @( negedge i_rst_n or posedge i_clk_27M )
//  if( !i_rst_n )
//    addr_rom_delay <= 1'd0;
//  else 
//    case(state)
//      IDLE:
//        addr_rom_delay <= 4'd0 ;
//      COMP_1:
//        if( (rom_data == data_temp) || comp1_ok )
//          addr_rom_delay <= addr_rom_delay ;
//        else
//          addr_rom_delay <= addr_rom ;
//      
//      WAIT:
//        addr_rom_delay <= 4'd0 ;
//        
//      default:
//        addr_rom_delay <= addr_rom_delay ;
//    endcase
//    
//rom_cycle15 uu_rom_cycle15_delay(
//              .address( addr_rom_delay ),
//              .clock( i_clk_27M ),
//              .q( rom_data_delay )
//              );
//
//
//endmodule
//

module shortcheck_new(
            i_clk_27M,            //--27.095M 
            i_rst_n,               
            i_data_bit_defsk,
            i_clk_defsk,
            
            o_m_ready_p
            );
            
input i_clk_27M;
input i_rst_n;
input i_data_bit_defsk;
input i_clk_defsk;

output o_m_ready_p;

//----------------------------------------�ź�����------------------------------------------------//
parameter IDLE  = 2'd0,
          COMP  = 2'd1,
          WAIT  = 2'd2;
          
wire i_clk_defsk_p;
reg i_clk_defsk_temp1;
reg i_clk_defsk_temp2;
reg [1:0]state;
reg m_ready;
reg [23:0]cnt_wait; 
reg [44:0]data_temp;
reg [14:0]data_sample;
reg [14:0]msg_r1;
reg [14:0]msg_r2;
reg [14:0]msg_r3;
reg m_ready_temp1 ;
reg m_ready_temp2 ;
wire o_m_ready_p ;  

////////////////////////////////////////////??1bit???????////////////////////////////////////////////
always @(  negedge i_rst_n or posedge i_clk_27M  )
  if( !i_rst_n )
    begin
      i_clk_defsk_temp1 <= 1'b0;
      i_clk_defsk_temp2 <= 1'b0;
    end
  else
    begin
      i_clk_defsk_temp1 <= i_clk_defsk ;
      i_clk_defsk_temp2 <= i_clk_defsk_temp1 ;
    end
assign i_clk_defsk_p = i_clk_defsk_temp1 & ( ~i_clk_defsk_temp2 );

//////////////////////////////////////////////////////////////
always @(  negedge i_rst_n or posedge i_clk_27M  )
  if( !i_rst_n )
    state <= IDLE;
  else
    case( state )
      IDLE:
        if( i_clk_defsk_p )
          state <= COMP;
        else
          state <= state;
      COMP:
        if( m_ready )
          state <= WAIT;
        else
          state <= state;
      WAIT:
        if( cnt_wait >= 24'd13547500) //wait 500ms
          state <= IDLE;
        else
          state <= state;
      default:
        state <= IDLE;
    endcase

//??500ms
//reg [23:0]cnt_wait;   
always @(  negedge i_rst_n or posedge i_clk_27M  )
  if( !i_rst_n )
    cnt_wait <= 24'd0;
  else
    case( state ) 
      WAIT:
        if( cnt_wait >= 24'd13547500) //wait 500ms
          cnt_wait <= cnt_wait;
        else
          cnt_wait <= cnt_wait + 24'd1;
      default:
        cnt_wait <= 24'd0;
    endcase

//-----------------------------------------------------------------------------------//
always @(  negedge i_rst_n or posedge i_clk_27M  )
  if( !i_rst_n )
    data_temp <= 45'd0;
  else if( i_clk_defsk_p )
    data_temp <= {data_temp[43:0],i_data_bit_defsk};
  else
    data_temp <= data_temp;
    
//---------------------------------------------------------------------------------//
always @(  negedge i_rst_n or posedge i_clk_27M  )
  if( !i_rst_n )
    data_sample <= 15'b101011110001001;
  else 
    case( state )
      IDLE:
        data_sample <= 15'b101011110001001;
      COMP:
        data_sample <= {data_sample[13:0],data_sample[14]};
      WAIT:
        data_sample <= data_sample;
      default:
        data_sample <= data_sample;
    endcase
    
//------------------------------------------------------------------------------------------------------------//          
always @(  negedge i_rst_n or posedge i_clk_27M  )
  if( !i_rst_n )
    begin
      msg_r1 <= 15'd0;
      msg_r2 <= 15'd0;
      msg_r3 <= 15'd0;
    end
  else
    case( state )
      IDLE:
        begin
          msg_r1 <= 15'd0;
          msg_r2 <= 15'd0;
          msg_r3 <= 15'd0;
        end
      COMP:
        if( i_clk_defsk_p )
          begin
            msg_r1 <= data_temp[44:30];
            msg_r2 <= data_temp[29:15];
            msg_r3 <= data_temp[14:0];
          end
        else
          begin
            msg_r1 <= msg_r1;
            msg_r2 <= msg_r2;
            msg_r3 <= msg_r3;
          end
      WAIT:
        begin
          msg_r1 <= msg_r1;
          msg_r2 <= msg_r2;
          msg_r3 <= msg_r3;
        end
      default:
        begin
          msg_r1 <= 15'd0;
          msg_r2 <= 15'd0;
          msg_r3 <= 15'd0;
        end
    endcase

//reg m_ready;
always @(  negedge i_rst_n or posedge i_clk_27M  )
  if( !i_rst_n )
    m_ready <= 1'b0;
  else 
    case( state )
      IDLE:
        m_ready <= 1'b0;
      COMP:
        if ( ( msg_r1 == msg_r2 )&( msg_r2 == msg_r3 )&( msg_r1 == data_sample ) )
          m_ready <= 1'b1;
        else
          m_ready <= 1'b0;
      WAIT:
        m_ready <= 1'b0;
      default:
        m_ready <= 1'b0;
    endcase
    
/////////////////////////////////////////////////////////////////////////////////////////////////////////
always @( negedge i_rst_n or posedge i_clk_27M )
  if( !i_rst_n )
    begin
      m_ready_temp1 <= 1'b0 ;
      m_ready_temp2 <= 1'b0 ;
    end
  else
    begin
      m_ready_temp1 <= m_ready ;
      m_ready_temp2 <= m_ready_temp1 ;
    end
    
assign o_m_ready_p = m_ready_temp1 & (~m_ready_temp2);  

endmodule 

/*=======================================================================================*\
  Filename    : RamContrl.v
  Author      : leexiao
  Description : 循环保存和发送当前字（11比特）
                1.保持收到的11比特
                2.从下一字节开始循环发送全部报文：93个
                3.清下一个字，以备保存数据
                4.读数据，验证已清
                
          
  Called by   : 15001052282
  Revision History  : 
  Author             Date               Version              Description 
  GuoQingyang        20160817           V1.1.0               修改一些处理逻辑                        
  Email       ：
  Company     :

\*========================================================================================*/      
module RamContrl(
                i_clk_81M,
                i_rst_n,
                               
                i_data_sck,     //iv_data_fsk同步时钟，81M取沿 
                iv_data_fsk,    //11bit数据，一个iv_data_fsk大概持续11X48个27M时钟
                                //即维持11x48x3个81M时钟
                                             
                o_data_clk,     //ov_data_fsk同步时钟
                ov_data_fsk,    //输出数据    
                            
                o_frame_start   //输出1帧（93*11bit)的开始，脉冲                 
                );
        
input i_clk_81M;
input i_rst_n;

input i_data_sck;
input [10:0]iv_data_fsk;

output reg o_data_clk;
output reg [10:0]ov_data_fsk;

output reg o_frame_start;
//--------------------------------------------------------------------------------------------//
//变量声明
reg i_data_sck_r1;
reg i_data_sck_r2;
reg i_data_sck_p0;
reg i_data_sck_p1;
reg [6:0]wr_ram_addr; //RAM写地址1-93
reg wr_ram_en;
reg [10:0] wr_ram_data;
reg [3:0]cnt_4; //1-4
reg rd_ram_en;
reg [6:0]rd_ram_addr;
wire [10:0]ramout;
reg read_ok; //读完一圈指示
reg clr_check_en;
reg clr_check_en_r1;
reg clr_check_en_r2;
reg clr_check_en_r3;

reg frame_start;
reg frame_start_r;
reg ram_check_ok; //清空后的数据读出来为0
reg ram_check_err;//清空后的数据读出来不为0
//--------------------------------------------------------------------------------------------//
//对i_data_sck取沿 做跨时钟域处理
always @( posedge i_clk_81M or negedge i_rst_n )
begin
    if( !i_rst_n )  begin
        i_data_sck_r1 <= 1'b0;
        i_data_sck_r2 <= 1'b0;
        i_data_sck_p0 <= 1'b0;
        i_data_sck_p1 <= 1'b0;
    end
    else begin
        i_data_sck_r1 <= i_data_sck;
        i_data_sck_r2 <= i_data_sck_r1; 
        i_data_sck_p0 <= i_data_sck_r1 & (~i_data_sck_r2);
        i_data_sck_p1 <= i_data_sck_p0;
    end
end //always

//--------写RAM部分------------------------------------------------------------------------// 
//RAM写使能比i_data_sck_p0/read_ok延后1拍，使之与wr_ram_addr对齐
//RAM写数据
always @( posedge i_clk_81M or negedge i_rst_n )
begin
    if( !i_rst_n ) begin
        wr_ram_data <= 1'b0;
        wr_ram_en <= 1'b0;
    end
    else if(i_data_sck_p0)  begin
        wr_ram_data <= iv_data_fsk;
        wr_ram_en <= 1'b1;
    end
    else if(read_ok)begin //读完一轮后要写一个0,清空
        wr_ram_data <= 11'b0;
        wr_ram_en <= 1'b1;
    end
    else begin
        wr_ram_data <= wr_ram_data;
        wr_ram_en <= 1'b0;
    end
end //always
//ram写地址,每收一个字就存一个
always @( posedge i_clk_81M or negedge i_rst_n )
begin
    if( !i_rst_n )
        wr_ram_addr <= 7'd0;
    else if(read_ok)  begin//读完一轮以后，将下一个地址清空
        if(wr_ram_addr == 7'd93 )
            wr_ram_addr <= 7'd1;
        else
            wr_ram_addr <= wr_ram_addr + 1'b1;
    end
    else if( (wr_ram_addr != 7'd0) && (i_data_sck_p0) )//新的一帧帧数据来的时候写第一个11bit数据，同步时钟拉高
        wr_ram_addr <= wr_ram_addr;//这里选择不用wr_ram_addr <= wr_ram_addr + 7'd1，因为前次已经清0ram时已加1
    else if(i_data_sck_p0)//第一个数据来，则地址记为1
        wr_ram_addr <= 7'd1;           
    else
        wr_ram_addr <= wr_ram_addr ;
end //always

//---------------读RAM部分程序--------------------------------------------------------------// 
//cnt_4计数区间为 1～4
//cnt_4 == 4'd1--> 给出RAM读使能
always @( posedge i_clk_81M or negedge i_rst_n )
begin
    if( !i_rst_n )
        cnt_4 <= 3'b0;  
    else if(i_data_sck_p1) //写入一个11bit后开始计数,开始从1->93读ram往外发数据
        cnt_4 <= 3'd1;   
    else if(cnt_4 == 3'd4)begin
        if(rd_ram_addr == 7'd93)//读地址等于93，读完一轮，停止计数
            cnt_4 <= 3'b0;
        else                    //其它情况，从1～4计数
            cnt_4 <= 3'b1;
    end
    else if(cnt_4 != 3'd0)      //计数器大于1时开始1～4循环计数
        cnt_4 <= cnt_4 + 3'b1;
    else
        cnt_4 <= cnt_4;
end //always

// ram读使能
always @( posedge i_clk_81M or negedge i_rst_n )
begin
    if( !i_rst_n )
        rd_ram_en <= 1'b0;
    else if(cnt_4 == 3'd1) 
        rd_ram_en <= 1'b1; 
    else if(clr_check_en) 
        rd_ram_en <= 1'b1;  
    else
        rd_ram_en <= 1'b0;
end

//ram读地址
//RAM读地址1-93
always @( posedge i_clk_81M or negedge i_rst_n )
begin
    if( !i_rst_n )
        rd_ram_addr <= 7'd0;    
    else if(clr_check_en)//读完一圈以后，读下一个数据，检测其是否为0
        rd_ram_addr <= wr_ram_addr;    
    else if(i_data_sck_p0) //每帧清0读ram的地址
        rd_ram_addr <= 7'b0;
    else if(cnt_4 == 3'd1 )
        rd_ram_addr <= rd_ram_addr + 7'b1;
    else
        rd_ram_addr <= rd_ram_addr;
end

always @( posedge i_clk_81M or negedge i_rst_n )
begin
    if( !i_rst_n ) begin
        frame_start   <= 1'b0; 
        frame_start_r <= 1'b0; 
        o_frame_start <= 1'b0;
    end    
    else if( (rd_ram_addr ==7'b1) && (cnt_4 != 3'b0) ) begin
        frame_start   <= 1'b1; 
        frame_start_r <= frame_start; 
        o_frame_start <= frame_start && (~frame_start_r);//比第一字节的o_data_clk早1拍
    end
    else begin
        frame_start   <= 1'b0; 
        frame_start_r <= 1'b0; 
        o_frame_start <= 1'b0;
    end
end

//读出数据的字节时钟，上跳沿有效
always @( posedge i_clk_81M or negedge i_rst_n )
begin
    if( !i_rst_n )begin
        o_data_clk <= 1'b0;
        ov_data_fsk <= 1'b0;
    end        
    else if(cnt_4 == 3'd4)begin
        o_data_clk  <= 1'b1;
        ov_data_fsk <= ramout;
    end
    else begin 
        o_data_clk  <= 1'b0;
        ov_data_fsk <= ov_data_fsk;
    end
end

always @( posedge i_clk_81M or negedge i_rst_n )
begin
    if( !i_rst_n )
        read_ok <= 1'b0;       
    else if((cnt_4 == 3'd4) && (rd_ram_addr == 7'd93))
        read_ok <= 1'b1;
    else 
        read_ok <= 1'b0;                     
end //always

//---------------检测RAM部分程序--------------------------------------------------------------// 
//检验清0的使能
always @( posedge i_clk_81M or negedge i_rst_n )
begin
    if( !i_rst_n )begin
        clr_check_en    <= 1'b0;
        clr_check_en_r1 <= 1'b0;
        clr_check_en_r2 <= 1'b0;
        clr_check_en_r3 <= 1'b0;
    end
    else begin
        clr_check_en    <= read_ok;
        clr_check_en_r1 <= clr_check_en;
        clr_check_en_r2 <= clr_check_en_r1;
        clr_check_en_r3 <= clr_check_en_r2;
    end    
end//always

always @( posedge i_clk_81M or negedge i_rst_n )
begin
    if( !i_rst_n ) begin
        ram_check_ok  <= 1'b0;
        ram_check_err <= 1'b0;
    end
    else if( clr_check_en_r3 && (ramout == 11'b0))begin
        ram_check_ok  <= 1'b1;
        ram_check_err <= 1'b0;
    end    
    else if( clr_check_en_r3 && (ramout != 11'b0))begin  
        ram_check_ok  <= 1'b0;
        ram_check_err <= 1'b1;
    end
    else begin
        ram_check_ok  <= 1'b0;
        ram_check_err <= 1'b0;
    end
end
////////////////////////////////////////
//地址为：1-93
////////////////////////////////////////

ram_ram_contrl URam_ram_contrl(
    .data(wr_ram_data),
    .rdaddress(rd_ram_addr),
    .rdclock(i_clk_81M),
    .rden(rd_ram_en),
    .wraddress(wr_ram_addr),
    .wrclock(i_clk_81M ),
    .wren(wr_ram_en),
    .q(ramout)
    );

   
////////////////////////////////////////////////////////////////////////////////

endmodule 
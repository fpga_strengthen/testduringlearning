`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date: 2024/08/26 19:18:06
// Module Name: arp_receive
// Description: 检测MAC层发来的是数据包还是请求包,并将检测到的源地址和IP写入ARP动态链表中.
// Revision 0.01 - File Created
//////////////////////////////////////////////////////////////////////////////////

module arp_receive#(
	parameter	LOCAL_IP_ADDR = 32'hFF_FF_FF_FF
	)(
	// 时钟
	input wire			i_app_rx_clk,
	input wire			i_app_tx_clk,
	//复位
	input wire			i_app_rx_rst_n,
	input wire			i_app_tx_rst_n,

	// MAC层接收的数据
	input wire			i_arp_rx_data_vld,		// 数据有效信号
	input wire			i_arp_rx_data_last,		// MAC层数据的最后一个数据的标志
	input wire	[7:0]	iv_arp_rx_data,			// 从MAC数据包解析出MAC层数据

	// 输出接收到的MAC地址和IP地址
	output reg			o_arp_rx_src_vld,
	output reg	[7:0]	ov_arp_rx_ip_addr,	
	output reg	[7:0]	ov_arp_rx_mac_addr,

	output reg			o_arp_reply_active
);

/*-----------------------------------------------*\
					变量定义
\*-----------------------------------------------*/	
	
	reg		[5:0]		cnt_rx_byte;				// ARP数据包共包含28+18=46字节(对应MAC层数据部分的最小长度)
	reg		[15:0]		opcode;						// 操作码

	reg		[47:0]		source_mac_addr;			// 源MAC地址		
	reg		[31:0]		source_ip_addr;				// 源IP地址

	reg		[31:0]		target_ip_addr;				// 目的IP地址
	reg					target_ip_check;			// 目的IP校验结果

	reg					reply_active;				// 应答有效信号

	reg		[80:0]		arp_din;					// 将地址打包写入FIFO
	reg 				arp_wren;					// FIFO写使能
	wire				arp_wrfull;					// 写满

	wire				arp_rdempty;				// 读空
	wire	[3:0]		arp_wrcount;				// 写计数
	wire	[3:0]		arp_rdcount;				// 读技术
	wire				arp_rden;					// FIFO读使能

	wire	[80:0]		arp_dout;					// 由FIFO读出的地址

/*-----------------------------------------------*\
					assign
\*-----------------------------------------------*/	

	// FIFO读使能
	assign	arp_rden = ~arp_rdempty;

/*-----------------------------------------------*\
				接收字节计数
\*-----------------------------------------------*/	

	always @(posedge i_app_rx_clk or negedge i_app_rx_rst_n) begin 
		if(~i_app_rx_rst_n)
			cnt_rx_byte <= 6'd0;
		else if(i_arp_rx_data_last)
			cnt_rx_byte <= 6'd0;
		else if(i_arp_rx_data_vld)
			cnt_rx_byte <= cnt_rx_byte + 1'b1;
		else
			cnt_rx_byte <= cnt_rx_byte;
	end

/*-----------------------------------------------*\
					接收操作码
\*-----------------------------------------------*/

	always @(posedge i_app_rx_clk or negedge i_app_rx_rst_n) begin
		if(~i_app_rx_rst_n)
			opcode <= 16'h0;
		else if((cnt_rx_byte == 6'd6) || (cnt_rx_byte == 6'd7))
			opcode <= {opcode[7:0],iv_arp_rx_data};
		else
			opcode <= opcode;
	end

/*-----------------------------------------------*\
				接收源MAC和IP地址
\*-----------------------------------------------*/

	always @(posedge i_app_rx_clk or negedge i_app_rx_rst_n) begin
		if(~i_app_rx_rst_n) 
			source_mac_addr <= 48'h0;
		else if((cnt_rx_byte >= 6'd8) && (cnt_rx_byte <= 6'd13))
			source_mac_addr <= {source_mac_addr[39:0],iv_arp_rx_data};
		else
			source_mac_addr <= source_mac_addr;
	end

	always @(posedge i_app_rx_clk or negedge i_app_rx_rst_n) begin
		if(~i_app_rx_rst_n)
			source_ip_addr <= 32'h0;
		else if(cnt_rx_byte >= 6'd14 && (cnt_rx_byte <= 6'd17))
			source_ip_addr <= {source_ip_addr[23:0],iv_arp_rx_data};
		else
			source_ip_addr <= source_ip_addr;
	end

/*-----------------------------------------------*\
					接收IP地址
\*-----------------------------------------------*/

	always @(posedge i_app_rx_clk or negedge i_app_rx_rst_n) begin
		if(~i_app_rx_rst_n) 
			target_ip_addr <= 32'h0;
		else if((cnt_rx_byte >= 6'd24) && (cnt_rx_byte <= 6'd27))
			target_ip_addr <= {target_ip_addr[23:0],iv_arp_rx_data};
		else
			target_ip_addr <= target_ip_addr;
	end

/*-----------------------------------------------*\
					地址校验
\*-----------------------------------------------*/

	// 对比接收到的目的IP是否与板子的IP相同
	always @(posedge i_app_rx_clk or negedge i_app_rx_rst_n) begin
		if(~i_app_rx_rst_n)
			target_ip_check <= 1'b0;
		else if(target_ip_addr == LOCAL_IP_ADDR)
			target_ip_check <= 1'b1;
		else if(target_ip_check != LOCAL_IP_ADDR)
			target_ip_check <= 1'b0;	// 当进行下一次传输时,没有这个清零操作就会导致IP校验一直误判为正确.
		else
			target_ip_check <= target_ip_check;
	end

/*-----------------------------------------------*\
					应答激活信号
\*-----------------------------------------------*/

	always @(posedge i_app_rx_clk or negedge i_app_rx_rst_n) begin
		if(~i_app_rx_rst_n)
			reply_active <= 1'b0;
		else if(target_ip_check && (opcode == 16'd2))
			reply_active <= 1'b1;
		else if(~(target_ip_check && (opcode == 16'd2)))
			reply_active <= 1'b0;
		else
			reply_active <= reply_active;
	end

/*-----------------------------------------------*\
			  将源IP、MAC地址跨时钟域
\*-----------------------------------------------*/
	
	always @(posedge i_app_rx_clk or negedge i_app_rx_rst_n) begin
		if(~i_app_rx_rst_n)begin
			arp_wren <= 1'b0;
			arp_din  <= 81'd0;
		end
		else if(i_arp_rx_data_last)begin
			arp_wren <= 1'b1;
			arp_din  <= {reply_active,source_mac_addr,source_ip_addr};
		end
		else begin 
			arp_wren <= 1'b0;
			arp_din  <= 81'd0;
		end
	end

/*-----------------------------------------------*\
			  			FIFO例化
\*-----------------------------------------------*/
	
	fifo_w81xd16 rx_fifo_w81xd16(
	  	.rst           (~i_app_rx_rst_n),       // input wire rst
	  	.wr_clk        (i_app_rx_clk),         	// input wire wr_clk
	  	.rd_clk        (i_app_tx_clk),          // input wire rd_clk
	  	.din           (arp_din),               // input wire [80 : 0] din
	  	.wr_en         (arp_wren),              // input wire wr_en
	  	.rd_en         (arp_rden),             	// input wire rd_en
	  	.dout          (arp_dout),              // output wire [80 : 0] dout
	  	.full          (arp_wrfull),            // output wire full
	  	.empty         (arp_rdempty),           // output wire empty
	  	.rd_data_count (arp_wrcount),  			// output wire [3 : 0] rd_data_count
	  	.wr_data_count (arp_rdcount)  			// output wire [3 : 0] wr_data_count
	);

/*-----------------------------------------------*\
		将跨时钟域的数据在发送端时钟下打拍输出
\*-----------------------------------------------*/

	always @(posedge i_app_tx_clk or negedge i_app_tx_rst_n) begin
		if(~i_app_tx_rst_n) begin
			o_arp_rx_src_vld   <= 1'b0;
			o_arp_reply_active <= 1'b0;
			ov_arp_rx_mac_addr <= 48'b0;
			ov_arp_rx_ip_addr  <= 32'b0;
		end 
		else if(arp_rden)begin
			o_arp_rx_src_vld   <= 1'b1;
			o_arp_reply_active <= arp_dout[80];
			ov_arp_rx_mac_addr <= arp_dout[79:32];
			ov_arp_rx_ip_addr  <= arp_dout[31:0];
		end
		else begin 
			o_arp_rx_src_vld   <= 1'b0;
			o_arp_reply_active <= 1'b0;
			ov_arp_rx_mac_addr <= 48'b0;
			ov_arp_rx_ip_addr  <= 32'b0;
		end
	end

endmodule

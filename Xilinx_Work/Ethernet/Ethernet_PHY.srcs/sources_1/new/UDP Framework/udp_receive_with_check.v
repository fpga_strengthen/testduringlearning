`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2024/08/22 15:18:28
// Design Name: 
// Module Name: udp_receive
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 接收IP层数据并去掉IP首部，提取出IP层数据。
//				这个模块包含UDP首部校验.
// Revision:	
//////////////////////////////////////////////////////////////////////////////////


module udp_receive_with_check #(
	parameter	LOCAL_PORT = 16'h0		// 本地端口号
	)(
	input 	wire			i_clk,
	input	wire			i_rst_n,

	/*---------------- ip_receive 模块交互信号--------------------*/
	input	wire			i_udp_rx_vld,			// 数据有效标志
	input	wire			i_udp_rx_data_last,		// 数据部分最后一个
	input	wire	[7:0]	iv_udp_rx_data,			// UDP层数据
	input 	wire	[15:0]	iv_udp_rx_length,		// UDP层数据包长度

	/*----------------与用户端的交互信号--------------------*/
	output	reg				o_app_rx_data_vld,
	output	reg				o_app_rx_data_last,
	output	reg		[7:0]	ov_app_rx_data,
	output	reg		[15:0]	ov_app_rx_length
);

/*-----------------------------------------------*\
					信号定义
\*-----------------------------------------------*/	
	
	reg	[10:0]	cnt_rx_byte;		// 接收字节计数器
	reg	[15:0]	src_port;			// 源端口号
	reg	[15:0]	target_port;		// 目的端口号
	reg	[15:0]	udp_length;			// UDP长度

	// 计算校验和
	reg	[15:0]	head_check_sum_recv;// 接收到的UDP首部校验和
	reg	[31:0]	head_check_sum_tmp;	// 计算的UDP首部校验和长字节
	reg	[15:0]	head_check_sum;		// 首部校验和短字节

/*-----------------------------------------------*\
					接收字节计数
\*-----------------------------------------------*/	

	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n)
			cnt_rx_byte <= 11'd0;
		else if(i_udp_rx_vld)
			cnt_rx_byte <= cnt_rx_byte + 1'b1;
		else
			cnt_rx_byte <= 11'd0;
	end

/*-----------------------------------------------*\
					提取源端口号
\*-----------------------------------------------*/	

	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n)
			src_port <= 16'h0;
		else if(i_udp_rx_vld && ((cnt_rx_byte == 'd0) || (cnt_rx_byte == 'd1)))
			src_port <= {src_port[7:0],iv_udp_rx_data};
		else
			src_port <= src_port;
	end

/*-----------------------------------------------*\
					提取目的端口号
\*-----------------------------------------------*/	

	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n)
			target_port <= 16'h0;
		else if((cnt_rx_byte == 'd2)||(cnt_rx_byte == 'd3))
			target_port <= {target_port[7:0],iv_udp_rx_data};
		else
			target_port <= target_port;
	end

/*-----------------------------------------------*\
					提取UDP长度
\*-----------------------------------------------*/	

	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n)
			udp_length <= 16'h0;
		else if((cnt_rx_byte == 'd4 )|| (cnt_rx_byte == 'd5))
			udp_length <= {udp_length[7:0],iv_udp_rx_data};
		else
			udp_length <= udp_length;
	end

/*-----------------------------------------------*\
				UDP首部校验和检查
\*-----------------------------------------------*/	

	// 提取UDP首部校验和
	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n)
			head_check_sum_recv <= 'h0;
		else if((cnt_rx_byte == 'd6) || (cnt_rx_byte == 'd7))
			head_check_sum_recv <= {head_check_sum_recv[7:0],iv_udp_rx_data};
		else
			head_check_sum_recv <= head_check_sum_recv;
	end

	// 计算UDP首部校验和step1
	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n)
			head_check_sum_tmp <= 32'h0;
		else if(cnt_rx_byte == 'd6)
			head_check_sum_tmp <= src_port + target_port + udp_length;
		else
			head_check_sum_tmp <= head_check_sum_tmp;
	end

	// 计算UDP首部校验和step2
	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n)
			head_check_sum <= 16'h0;
		else if(cnt_rx_byte == 'd7)
			head_check_sum <= ~(head_check_sum_tmp[31:16] + head_check_sum_tmp[15:0]);
		else
			head_check_sum <= head_check_sum;
	end

/*-----------------------------------------------*\
			输出数据、有效信号和长度
\*-----------------------------------------------*/	

	// 校验正确时,就接收后续数据
	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n)
			ov_app_rx_data <= 8'h0;
		else if(i_udp_rx_data_last)
			ov_app_rx_data <= 8'h0;
		else if((head_check_sum == head_check_sum_recv)&&(cnt_rx_byte >= 'd8))
			ov_app_rx_data <= iv_udp_rx_data;
		else
			ov_app_rx_data <= ov_app_rx_data;
	end

	// 数据有效信号	
	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n) 
			o_app_rx_data_vld <= 1'b0;
		else if(o_app_rx_data_last)
			o_app_rx_data_vld <= 1'b0;
		else if(cnt_rx_byte >= 'd8)
			o_app_rx_data_vld <= 1'b1;
		else
			o_app_rx_data_vld <= o_app_rx_data_vld;
	end

	// 数据结束标志
	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n)
			o_app_rx_data_last <= 1'b0;
		else
			o_app_rx_data_last <= i_udp_rx_data_last;
	end

	// 输出数据长度
	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n)
			ov_app_rx_length <= 16'h0;
		else
			ov_app_rx_length <= iv_udp_rx_length - 'd8;
	end

endmodule

//-------------------------------------------------------------------------------------------------                                                                             
//                                                                                                                                                                              
//--- ALL RIGHTS RESERVED
//--- File      : ZCP_judgment.v 
//--- Auther    : 
//--- Date      : 
//--- Version   : v1.3.2.1
//--- Abstract  : 通过斜率求得同步点
//
//-------------------------------------------------------------------------------------------------                                                                             
//                                                                                                                                                                              
// Description : 求出斜率的过零点对斜率进行修正后得到同步点
//                                                                                                                                                            
//                                                                                                                                                                              
//-------------------------------------------------------------------------------------------------                                                                             
//--- Modification History:
//--- Date          By          Version         Change Description 
//-------------------------------------------------------------------
//--- 2017.05.11           
//--------------------------------------------------------------------
module ZCP_judgment(
          i_rst_n,         //输入复位信号，低电平有效
          i_clk_81M,       //输入时钟信号，81M
          i_clk_27M_en,    //输入时钟使能，27M
          i_slope,         //输入数据，用来进行过零点判决

          o_sync_point     //同步点
          );

input i_rst_n;
input i_clk_81M;
input i_clk_27M_en;
input [25:0]i_slope;

output o_sync_point;

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//----------斜率对频偏的修正后进行过零点判决得到同步点----------
reg [3:0]sum_wt_addr;                 //写地址  共存9个值用于求平均
reg [3:0]sum_rd_addr;                 //读地址  
wire signed[25:0]slope_delay_9;        //ram输出
reg signed[28:0]slope_sum_8;          //对连续8点斜率求和 （24+3）

reg[5:0]sum_delay_wt_addr;            //写地址  将9点求和结果延时用于求平均
reg[5:0]sum_delay_rd_addr;            //读地址
wire signed[28:0]slope_sum_8_delay43;  //ram输出

reg signed[29:0]slope_sum_8_add_delay;//斜率8点和与延时后的结果对应相加的结果，用于求斜率平均值
reg signed[25:0]mid_freq;           //斜率平均值

reg [4:0]delay16_wt_addr;             //写地址  共存17个值 延时用于过零点判决
reg [4:0]delay16_rd_addr;             //读地址  
wire signed[25:0]slope_delay_25;       //ram输出

reg signed[25:0]slope_delay_26;       //ram输出打一拍

reg slope_right;                      //未修正的斜率所处范围正确指示

reg signed[25:0]slope_correction;     //斜率修正量
reg signed[26:0]slope_delay_25_correction/*synthesis noprune*/;//修正后的斜率
reg signed[26:0]slope_delay_26_correction;//修正后的斜率

reg sync_point;                       //同步点

reg sync_point_r;                         //取沿

wire o_sync_point;


//----------对斜率进行修正并进行过零点判决得到同步点---------------
//将斜率存入RAM，并以先入先出的方式读出数据，共9个点
//写地址
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      sum_wt_addr <= 4'd0;
    else if( i_clk_27M_en ) //27M
      begin
        if ( sum_wt_addr==4'd8 )
          sum_wt_addr <= 4'd0;
        else
          sum_wt_addr <= sum_wt_addr + 1'b1;
      end//i_clk_27M_en
    else
      sum_wt_addr <= sum_wt_addr;
  end

//读地址
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      sum_rd_addr <= 4'd0;
    else if( i_clk_27M_en ) //27M
      begin
        if ( sum_wt_addr==4'd6 )//mark 时序图是5
            sum_rd_addr <= 4'd0;
        else if ( sum_rd_addr==4'd8 )
          sum_rd_addr <= 4'd0;
        else
          sum_rd_addr <= sum_rd_addr + 1'b1;
      end
    else
      sum_rd_addr <= sum_rd_addr;
  end  
  
ram_sum_8 u_ram_sum_8(
                  .clock(i_clk_27M_en),
                  .data(i_slope),
                  .rdaddress(sum_rd_addr),
                  .wraddress(sum_wt_addr),
                  .wren(1'b1),
                  .q(slope_delay_9)
                  );

//对斜率求8点的和
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      slope_sum_8 <= 29'd0;
    else if(i_clk_27M_en)//27M
      slope_sum_8 <= slope_sum_8 + i_slope - slope_delay_9;
    else
      slope_sum_8 <= slope_sum_8;
  end
  
//将斜率8点和的结果存入RAM，并以先入先出的方式读出数据，共44个点
//写地址
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      sum_delay_wt_addr <= 6'd0;
    else if(i_clk_27M_en)//27M
      begin
        if ( sum_delay_wt_addr==6'd43 )
          sum_delay_wt_addr <= 6'd0;
        else
          sum_delay_wt_addr <= sum_delay_wt_addr + 1'b1;
      end
    else
      sum_delay_wt_addr <= sum_delay_wt_addr;
  end

//读地址
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      sum_delay_rd_addr <= 6'd0;
    else if(i_clk_27M_en)//27M
      begin 
        if ( sum_delay_wt_addr==6'd41 )//mark 时序图是40
            sum_delay_rd_addr <= 6'd0;
        else if ( sum_delay_rd_addr==6'd43 )
          sum_delay_rd_addr <= 6'd0;
        else
          sum_delay_rd_addr <= sum_delay_rd_addr + 1'b1;
      end
    else
      sum_delay_rd_addr <= sum_delay_rd_addr;
  end  
  
ram_sum8_delay43 u_ram_sum8_delay43(
                  .clock(i_clk_27M_en),
                  .data(slope_sum_8),
                  .rdaddress(sum_delay_rd_addr),
                  .wraddress(sum_delay_wt_addr),
                  .wren(1'b1),
                  .q(slope_sum_8_delay43)
                  );    
                  
//斜率8点和与延时后的结果对应相加
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      slope_sum_8_add_delay <= 30'd0;
    else if(i_clk_27M_en)//27M
      slope_sum_8_add_delay <= slope_sum_8 + slope_sum_8_delay43;
    else
      slope_sum_8_add_delay <= slope_sum_8_add_delay;
  end
    
//斜率必须处在某个区间内才认为是有效的同步点 ，给出同步点有效的标志 具体范围推导见文档  
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      slope_right <= 1'd0;
    else if(i_clk_27M_en) begin//27M      // 除以8
        if( (slope_sum_8[28:3]>=26'd35592 && slope_sum_8[28:3]<=26'd439089) && (slope_sum_8_delay43[28:3]>=26'd66706620 && slope_sum_8_delay43[28:3]<=26'd67056896) )
            slope_right <= 1'd1;
        else if( (slope_sum_8_delay43[28:3]>=26'd35592 && slope_sum_8_delay43[28:3]<=26'd439089) && (slope_sum_8[28:3]>=26'd66706620 && slope_sum_8[28:3]<=26'd67056896) )
            slope_right <= 1'd1;
        else
            slope_right <= 1'd0;
    end
    else
        slope_right <= slope_right;
  end   
  
 
//求斜率的平均值
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      mid_freq <= 26'd0;
    else if(i_clk_27M_en)//27M 
      mid_freq <= slope_sum_8_add_delay[29:4];  // 除以16
    else
      mid_freq <= mid_freq;
  end
  
//将ram_sum_8读出斜率延时结果存入RAM，并以先入先出的方式读出数据，共17个点
//写地址
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      delay16_wt_addr <= 5'd0;
    else if(i_clk_27M_en)//27M 
      begin
        if ( delay16_wt_addr==5'd16 )
          delay16_wt_addr <= 5'd0;
        else
          delay16_wt_addr <= delay16_wt_addr + 1'b1;
      end
    else
      delay16_wt_addr <= delay16_wt_addr;
  end

//读地址
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      delay16_rd_addr <= 5'd0;
    else if(i_clk_27M_en)//27M 
      begin
        if ( delay16_wt_addr==5'd13 )//mark 时序图是13
            delay16_rd_addr <= 5'd0;
        else if ( delay16_rd_addr==5'd16 )
          delay16_rd_addr <= 5'd0;
        else
          delay16_rd_addr <= delay16_rd_addr + 1'b1;
      end
    else
      delay16_rd_addr <= delay16_rd_addr;
  end  
  
ram_delay_16 u_ram_delay_16(
                  .clock(i_clk_27M_en),
                  .data(slope_delay_9),
                  .rdaddress(delay16_rd_addr),
                  .wraddress(delay16_wt_addr),
                  .wren(1'b1),
                  .q(slope_delay_25)
                  );  

//将slope_delay_25延时1个时钟用于过零判决
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      slope_delay_26 <= 26'd0;
    else if(i_clk_27M_en)//27M 
      slope_delay_26 <= slope_delay_25;
    else
      slope_delay_26 <= slope_delay_26;
  end

//根据估算的w0和w1的值求中心频率偏移量，用来后续同步点的判决
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      slope_correction <= 26'd0;
    else if(i_clk_27M_en)//27M 
      begin
        if( ( slope_delay_26[25] ) ^ (slope_delay_25[25] )  )  // 过零判决
            slope_correction = mid_freq;
        else
            slope_correction = slope_correction;
      end
    else
      slope_correction = slope_correction;
  end

//对斜率进行修正
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
        slope_delay_25_correction <= 27'd0;
    else if(i_clk_27M_en)//27M 
        slope_delay_25_correction <= slope_delay_25 - slope_correction;
    else
        slope_delay_25_correction <= slope_delay_25_correction;
  end
  
//将slope_delay_25_correction延时1个时钟用于过零判决
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      slope_delay_26_correction <= 27'd0;
    else if(i_clk_27M_en)//27M 
      slope_delay_26_correction <= slope_delay_25_correction;
    else
      slope_delay_26_correction <= slope_delay_26_correction;
  end

//对修正后的斜率进行过零点判决，判断其是否是同步点
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if  (!i_rst_n )
      sync_point <= 1'd0;
    else if(i_clk_27M_en)//27M 
      begin
        if( (slope_delay_26_correction[26])^(slope_delay_25_correction[26]) && slope_right  ) //过零判决
          sync_point <= 1'b1;
        else
          sync_point <= 1'b0;
      end
    else
      sync_point <= sync_point;
  end      
  
 //取沿
 always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if  (!i_rst_n )
      sync_point_r <= 1'd0;
    else if(i_clk_27M_en)//27M 
      begin
          sync_point_r <= sync_point;
      end
    else
      sync_point_r <= sync_point_r;
  end     
  
  assign o_sync_point = sync_point & (~sync_point_r);

endmodule
% 利用MATLAB生成mif文件，DA位数设置为12，单极性器件
% 正弦波的数据位宽设置为12位，无符号型
% 产生的信号频率为12.5kHz

f1 = 12.5e3;                % 信号的频率
p1 = 0;                     % 信号的相位

fs = 4e6;                   % 采样频率
N  = fs/f1;                 % 采样点数

t = [0:1/fs:(N-1)/fs];      %#ok<NBRAK> %采样时刻

A = 2^11-1;                 % 幅值
ADC = 2^11-1;               % 直流分量

% 生成信号,长度为一个周期
s = A * sin(2*pi*f1*t) + ADC;
s1 = s;
plot(s)

% 保存在mif文件中，之后存在ROM中重复读取即可生成连续的正弦波
fid = fopen('da_8k_0.mif','wt');
fprintf(fid,'%s\n','WIDTH=12;');
fprintf(fid,'%s\n\n','DEPTH=320;');                     % 深度
fprintf(fid,'%s\n','ADDRESS_RADIX=UNS;');               % 地址格式
fprintf(fid,'%s\n\n','DATA_RADIX=UNS;');                % 数据格式
fprintf(fid,'%s\t','CONTENT');                          % 地址
fprintf(fid,'%s\n','BEGIN');                            % 地址:数据 ，起始符
for i=1:N
    s2 = round(s(i));
    if s2 < 0
        s2 = 0;
    end
    fprintf(fid,'\t%g\t',i-1);          % 地址从0开始
    fprintf(fid,'%s\t',':');
    fprintf(fid,'%d',s2);
    fprintf(fid,'%s\n',';');
end
fprintf(fid,'%s\n','END;');             % 结束符
%FSK解调判决仿真
%目的检测频偏对判决错误的影响

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%FSK调制%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc
format long
close all
clear all

f0_true = 3.951 ;
f1_true = 4.516 ;         %比特0和比特1的频点，单位MHz
sample_rate_true = 27.095;%采样速率，单位MHz
BPS_data_true = 0.56448;  %数据传输速率，单位MHz

static_lenth_fsk = 100 ;%fsk的bit个数
j_lenth = 100 ;         %j的长度，用来控制循环测次数。同时根据该变量控制偏差的值（频偏，速率偏移，干扰频率）
static_lenth_x_axis = 4794;%循环一次，FSK数据的长度
x_axis = zeros(1,j_lenth*static_lenth_x_axis);%x轴

%本地载波
S0 = cos( f0_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);
S1 = cos( f1_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);
S0_Q = sin(f0_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);%%移相90°
S1_Q = sin(f1_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);%%移相90°




%Y轴，4条曲线，分别代表采样点偏差不同值（+1，-1，+2，-2）
y_axis_1p = zeros(1,j_lenth);%*static_lenth_x_axis);
y_axis_1n = zeros(1,j_lenth);%*static_lenth_x_axis);
y_axis_2p = zeros(1,j_lenth);%*static_lenth_x_axis);
y_axis_2n = zeros(1,j_lenth);%*static_lenth_x_axis);

for j=1:j_lenth%for循环，每次循环都产生需要验证的偏移量（频偏，数据速率偏移）



%%%%%%%%% 产生相位连续的FSK调制信号进行ADC采样后作为输入 %%%%%%%%%%%%%
src_data =randint(1,100);%以比特向量方式输入的待调制数据，随机产生

%%%%%%%%% FSK数据的关键参数 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%通过控制FSK的参数产生带有偏差数据源
f0 = f0_true+ 3*(j-j_lenth/2)/100 ;%+ 3*(j-j_lenth/2)/100
f1 = f1_true ;%比特0和比特1的频点，单位MHz
sample_rate = sample_rate_true;%采样速率，单位MHz
BPS_data = BPS_data_true;%数据传输速率，单位MHz
FSK_data  = FSK_mod(src_data,f0,f1,sample_rate,BPS_data);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%干扰信号
f0_noise = 3.951   ;
f1_noise = 4.516 ;%比特0和比特1的频点，单位MHz
FSK_data_noise  = FSK_mod(src_data,f1_noise,f1_noise,sample_rate,BPS_data)/10;

%对输入数据进行定标
FSK_data = FixPointNum(FSK_data  - 2048, 12, 0);%字长12bit，精度0位小数 + FSK_data_noise
N = length(FSK_data);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%x轴
for i=1:N
x_axis((j-1)*N+i) = j;
end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%相关解调%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
value_cos0 = FSK_data .* S0;
value_sin0 = FSK_data .* S0_Q;
value_cos1 = FSK_data .* S1;
value_sin1 = FSK_data .* S1_Q;

for i =1:N-48 %相干解调过程,48点求和，将0,1求和结果对应相减得到判决数据
    x(i) = ( sum(value_cos0(i:i+48)) )^2 + ( sum(value_sin0(i:i+48)) )^2 - ( sum(value_cos1(i:i+48)) )^2 - ( sum(value_sin1(i:i+48)) )^2 ;
end
%% 判决
for i=1:N-48
if(x(i)>0)
   value_x_sum0_minus_sum1(i) = 1;
elseif(x(i)<=0)
   value_x_sum0_minus_sum1(i) = 0; 
end %if
end %%for

%% 判决后的数据求拐点，同时判断拐点处是否是48的整数倍
edge = zeros(1,N-49);
for i=1:N-49
   edge(i) =  xor( value_x_sum0_minus_sum1(i),value_x_sum0_minus_sum1(i+1) );
end
%沿儿的位置
edge_position = find(edge == 1);

r = mod(edge_position(2:length(edge_position)) - edge_position(1:length(edge_position)-1),48);

y_axis_1p(j)  = length (find(r==1));
y_axis_2p(j)  = length (find(r==2));
y_axis_3p(j)  = length (find(r==3));
y_axis_1n(j)  = -length (find(r==47));
y_axis_2n(j)  = -length (find(r==46));
y_axis_3n(j)  = -length (find(r==45));
end

figure(1),
bar((1:j_lenth)-j_lenth/2, y_axis_1p,0.2,'y' );
hold on,
bar( (1:j_lenth)+0.2-j_lenth/2,y_axis_2p,0.2,'g' );
hold on,
bar( (1:j_lenth)+0.4-j_lenth/2,y_axis_1n,0.2,'r' );
hold on,
bar((1:j_lenth)+0.6-j_lenth/2,y_axis_2n,0.2,'k' );
xlabel('3.951频偏(x30K)');
ylabel('概率值');
title('黄色是+1；绿色是+2；红色是-1；黑色是-2');




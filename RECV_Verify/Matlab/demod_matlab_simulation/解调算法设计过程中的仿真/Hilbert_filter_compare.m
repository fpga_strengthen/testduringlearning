%比较几种Hilbert滤波器的性能，以符合Hilbert累加法求同步点的设计需求

clc
format long
close all
clear


%% 参数
window = ones(1,21); %矩形窗
nfft = 480;
sample_rate =27095;%KHz
%时窗法参数
N = 10;%取值范围在10~30之间
n = [-N:1:N];

%% 时窗法1 时窗函数：w_n_1=w（n△）=1/2*(1+cos(pi*n/N))
w_n_1 = 1/2*(1+cos(pi*n/N));
h_n_1 = 2./(pi .* n).*w_n_1;
h_n_1(1:2:end) = 0;%修正后的Hilbert滤波因子

%画出Hilbert变换滤波因子h_n的功率谱
[Pxx1,f1] = periodogram(h_n_1,window,nfft,sample_rate); %直接法求功率谱
Pxx1 = 10*log10(Pxx1);
Fl_200k_1 = find( (f1 < 230) & (f1 > 170) );%找到200kHz的点
Fh_564k_1 = find((f1 < 580) & (f1 > 560));%找到564kHz的点

figure(1), subplot(3,2,1)
plot(f1,Pxx1);
grid on
text(f1(Fl_200k_1),Pxx1(Fl_200k_1),'o','color','r');%标注200KHz的位置 
text(f1(Fl_200k_1+1),Pxx1(Fl_200k_1+1),['(',num2str(f1(Fl_200k_1)),'kHz,',num2str(Pxx1(Fl_200k_1)),'dB)'],'color','r');
text(f1(Fh_564k_1),Pxx1(Fh_564k_1),'^','color','g');%标注564KHz的位置 
text(f1(Fh_564k_1+1),Pxx1(Fh_564k_1+1),['(',num2str(f1(Fh_564k_1)),'kHz,',num2str(Pxx1(Fh_564k_1)),'dB)'],'color','k');
title('时窗法1滤波因子h_n_1的功率谱');

%% 时窗法2 时窗函数： w_n_2=w（n△）=0.56+0.44*cos(pi*n/N)
w_n_2 = 0.56+0.44*cos(pi*n/N);
h_n_2 = 2./(pi.*n).*w_n_2;
h_n_2(1:2:end) = 0;%修正后的Hilbert滤波因子

%画出Hilbert变换滤波因子h_n的功率谱
[Pxx2,f2]=periodogram(h_n_2,window,nfft,sample_rate); %直接法求功率谱
Pxx2 = 10*log10(Pxx2);
Fl_200k_2 = find( (f2 < 230) & (f2 > 170) );%找到200kHz的点
Fh_564k_2 = find((f2 < 580) & (f2 > 560));%找到564kHz的点
figure(1), subplot(3,2,2)
plot(f2,Pxx2);
grid on
text(f2(Fl_200k_2),Pxx2(Fl_200k_2),'o','color','r');%标注200KHz的位置 
text(f2(Fl_200k_2+1),Pxx2(Fl_200k_2+1),['(',num2str(f2(Fl_200k_2)),'kHz,',num2str(Pxx2(Fl_200k_2)),'dB)'],'color','r');
text(f2(Fh_564k_2),Pxx2(Fh_564k_2),'^','color','b');%标注200KHz的位置 
text(f2(Fh_564k_2+1),Pxx2(Fh_564k_2+1),['(',num2str(f2(Fh_564k_2)),'kHz,',num2str(Pxx2(Fh_564k_2)),'dB)'],'color','k');
title('时窗法2滤波因子h_n_2的功率谱')

%% 时窗法3 时窗函数： w_n_2=w（n△）=exp(-X*N^2) B=[0.1,0.2] X=-lnB/N^2
B = 0.15;
X = -log(B)/N^2;
w_n_3 = exp(-X*N^2);
h_n_3 = 2./(pi.*n).*w_n_3;
h_n_3(1:2:end) = 0;%修正后的Hilbert滤波因子
%画出Hilbert变换滤波因子h_n的功率谱
[Pxx3,f3]=periodogram(h_n_3,window,nfft,sample_rate); %直接法求功率谱
Pxx3 = 10*log10(Pxx3);
Fl_200k_3 = find( (f3 < 230) & (f3 > 170) );%找到200kHz的点
Fh_564k_3 = find((f3 < 580) & (f3 > 560));%找到564kHz的点
figure(1), subplot(3,2,3)
plot(f3,Pxx3);
grid on
text(f3(Fl_200k_3),Pxx3(Fl_200k_3),'o','color','r');%标注200KHz的位置 
text(f3(Fl_200k_3+1),Pxx3(Fl_200k_3+1),['(',num2str(f3(Fl_200k_3)),'kHz,',num2str(Pxx3(Fl_200k_3)),'dB)'],'color','r');
text(f3(Fh_564k_3),Pxx3(Fh_564k_3),'^','color','b');%标注200KHz的位置 
text(f3(Fh_564k_3+1),Pxx3(Fh_564k_3+1),['(',num2str(f3(Fh_564k_3)),'kHz,',num2str(Pxx3(Fh_564k_3)),'dB)'],'color','k');
title('时窗法3滤波因子h_n_3的功率谱')

%% 镶边法
N1 = 30;%取值范围在[30,60]之间
n1 = [-N1:1:N1];
f1 = 200;  %单位kHz
f2 = 500;
f3 = 1000;
f4 = 1200;
delta = 1/sample_rate;%抽样时间间隔
g_n = (4/(f4-f3)^2).*sin(pi.*(f3+f4).*n1.*delta).*sin(pi.*(f4-f3)./2.*n1.*delta).^2./(pi.*n1.*delta).^3 - (4/(f2-f1)^2).*sin(pi.*(f1+f2).*n1.*delta).*sin(pi.*(f2-f1)./2.*n1.*delta).^2./(pi.*n1.*delta).^3 ;
%n1=0时g_n无值，所以在该点求极限代替
syms n_syms;
g_n(N1+1)=limit((4/(f4-f3)^2).*sin(pi.*(f3+f4).*n_syms.*delta).*sin(pi.*(f4-f3)./2.*n_syms.*delta).^2./(pi.*n_syms.*delta).^3 - (4/(f2-f1)^2).*sin(pi.*(f1+f2).*n_syms.*delta).*sin(pi.*(f2-f1)./2.*n_syms.*delta).^2./(pi.*n_syms.*delta).^3)
for i =1:1:length(n1)
    if rem(n1(i),2)==0
        n1_odd(i) = Inf;
    else
        n1_odd(i) = n1(i);
    end %if
end %for
hn = 2./(pi.*n1_odd);
h_n_4 = conv(g_n,hn);
h_n_4 = h_n_4((2*N1-9):(2*N1+11));
%画出Hilbert变换滤波因子h_n的功率谱
[Pxx4,f4]=periodogram(h_n_4,window,nfft,sample_rate); %直接法求功率谱
Pxx4 = 10*log10(Pxx4);
Fl_200k_4 = find( (f4 < 230) & (f4 > 170) );%找到200kHz的点
Fh_564k_4 = find((f4 < 580) & (f4 > 560));%找到564kHz的点
figure(1), subplot(3,2,4)
plot(f4,Pxx4);
grid on
text(f4(Fl_200k_4),Pxx4(Fl_200k_4),'o','color','r');%标注200KHz的位置 
text(f4(Fl_200k_4+1),Pxx4(Fl_200k_4+1),['(',num2str(f4(Fl_200k_4)),'kHz,',num2str(Pxx4(Fl_200k_4)),'dB)'],'color','r');
text(f4(Fh_564k_4),Pxx4(Fh_564k_4),'^','color','b');%标注200KHz的位置 
text(f4(Fh_564k_4+1),Pxx4(Fh_564k_4+1),['(',num2str(f4(Fh_564k_4)),'kHz,',num2str(Pxx4(Fh_564k_4)),'dB)'],'color','k');
title('镶边法滤波因子h_n_4的功率谱')

%% 实验室程序实现的Hilbert滤波器系数
h_n_5 = [-0.127324 0 -0.212207 0 -0.63662 0 0.63662 0 0.212207 0 0.127324];
%画出Hilbert变换滤波因子h_n的功率谱
window1 = ones(1,length(h_n_5));
[Pxx5,f5]=periodogram(h_n_5,window1,nfft,sample_rate); %直接法求功率谱
Pxx5 = 10*log10(Pxx5);
Fl_200k_5 = find( (f5 < 230) & (f5 > 170) );%找到200kHz的点
Fh_564k_5 = find((f5 < 580) & (f5 > 560));%找到564kHz的点
figure(1),  subplot(3,2,5)
plot(f5,Pxx5);
grid on
text(f5(Fl_200k_5),Pxx5(Fl_200k_5),'o','color','r');%标注200KHz的位置 
text(f5(Fl_200k_5+1),Pxx5(Fl_200k_5+1),['(',num2str(f5(Fl_200k_5)),'kHz,',num2str(Pxx5(Fl_200k_5)),'dB)'],'color','r');
text(f5(Fh_564k_5),Pxx5(Fh_564k_5),'^','color','b');%标注200KHz的位置 
text(f5(Fh_564k_5+1),Pxx5(Fh_564k_5+1),['(',num2str(f5(Fh_564k_5)),'kHz,',num2str(Pxx5(Fh_564k_5)),'dB)'],'color','k');
title('实验室所使用的Hilbert滤波因子h_n_5的功率谱')

%% 产生不同频率的单频信号输入
freq_table = [1,5,10,50,100,200,250,400,500,800,1000,2000,2500,3000,3500,4000,5000];%单位kHz

for freq_num = 1:length(freq_table)
    %% 测试不同频率测试信号
    re_x = cos(2*pi*freq_table(freq_num)*[1:480]/ sample_rate);
    im_x = sin(2*pi*freq_table(freq_num)*[1:480]/ sample_rate);
    
    %% 时窗法1 时窗函数：w_n_1=w（n△）=1/2*(1+cos(pi*n/N))
    im_datain_solve1 = conv(h_n_1,re_x);
    im_datain_solve1 = im_datain_solve1(10:end)
    figure(2),subplot(3,2,1)
    hold off;
    plot(re_x,'r');
    hold on;
    plot(im_x,'b-.');
    plot(im_datain_solve1,'k');
    title(['时窗法1：信号频率' ,num2str(freq_table(freq_num)), 'khz'])
    legend('原同相信号','原正交信号','求得正交信号')
    
    %% 时窗法2 时窗函数： w_n_2=w（n△）=0.56+0.44*cos(pi*n/N)
    im_datain_solve2 = conv(h_n_2,re_x);
    im_datain_solve2 = im_datain_solve2(10:end)
    figure(2),subplot(3,2,2),
    hold off;
    plot(re_x,'r');
    hold on;
    plot(im_x,'b-.');
    plot(im_datain_solve2,'k');
    title(['时窗法2：信号频率' ,num2str(freq_table(freq_num)), 'khz'])
    legend('原同相信号','原正交信号','求得正交信号')
    
    
    %% 时窗法3 时窗函数： w_n_2=w（n△）=exp(-X*N^2) B=[0.1,0.2] X=-lnB/N^2
    im_datain_solve3 = conv(h_n_3,re_x);
    im_datain_solve3 = im_datain_solve3(10:end);
    figure(2),subplot(3,2,3),
    hold off;
    plot(re_x,'r');
    hold on;
    plot(im_x,'b-.');
    plot(im_datain_solve3,'k');
    title(['时窗法3：信号频率' ,num2str(freq_table(freq_num)), 'khz'])
    legend('原同相信号','原正交信号','求得正交信号')
    
    %% 镶边法
    im_datain_solve4 = conv(h_n_4,re_x);
    im_datain_solve4 = im_datain_solve4(10:end);
    figure(2),subplot(3,2,4),
    hold off;
    plot(re_x,'r');
    hold on;
    plot(im_x,'b-.');
    plot(im_datain_solve4,'k');
    title(['镶边法：信号频率' ,num2str(freq_table(freq_num)), 'khz'])
    legend('原同相信号','原正交信号','求得正交信号')
   
    %% 实验室程序实现的Hilbert滤波
    [ im_datain_solve5 ] = hilbert_trans( re_x );
    im_datain_solve5 = im_datain_solve5(6:end);
    figure(2),subplot(3,2,5),
    hold off;
    plot(re_x,'r');
    hold on;
    plot(im_x,'b-.');
    plot(im_datain_solve5,'k');
    title(['实验室现用程序：信号频率' ,num2str(freq_table(freq_num)), 'khz'])
    legend('原同相信号','原正交信号','求得正交信号')
    
    
    pause
    cla reset
end

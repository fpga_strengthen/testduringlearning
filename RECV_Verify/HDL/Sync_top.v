module Sync_top(
    input   wire        i_clk_108M,
    input   wire        i_clk_27M_en,
    input   wire        i_rst_n,
    input   wire [11:0] iv_fsk_data,
    input   wire        i_demod_result,
    input   wire        i_demod_result_sck,

    output  wire        o_demod_en,
    output  wire        o_framing_start
);

    // 模块间连线
    wire    [12:0]      fsk_im;
    wire    [12:0]      fsk_re;
    wire    [13:0]      angle_atan;
    wire    [25:0]      slope;
    wire                sync_point;

    // 希尔伯特变换计算实虚部
    Hilbert U_Hilbert(
        .i_clk_108M         (i_clk_108M),
        .i_clk_27M_en       (i_clk_27M_en),
        .i_rst_n            (i_rst_n),
        .iv_fsk_sig         (iv_fsk_data),

        .ov_fsk_im          (fsk_im),
        .ov_fsk_re          (fsk_re)
    );

    // Cordic算法计算相位
    cordic_arctan_rad U_cordic(
        .i_clk_108M         (i_clk_108M),
        .i_clk_27M_en       (i_clk_27M_en),
        .i_rst_n            (i_rst_n),
        .iv_im_data         (fsk_im),
        .iv_re_data         (fsk_re),

        .ov_angle_tan       (angle_atan)
    );

    // 最小二乘法拟合相位直线的斜率
    slope_calu U_slope_calu(
        .i_clk_108M         (i_clk_108M),
        .i_clk_27M_en       (i_clk_27M_en),
        .i_rst_n            (i_rst_n),
        .iv_phase           (angle_atan),

        .ov_slope           (slope)   
    );

    // 根据两端折线的中间部分判断过零点
    ZCP_judge U_ZCP_judge(
        .i_clk_108M         (i_clk_108M),
        .i_clk_27M_en       (i_clk_27M_en),
        .i_rst_n            (i_rst_n),
        .iv_slope           (slope),

        .o_sync_point       (sync_point)
    );

    Sync_FSK U_Sync_FSK(
        .i_clk_108M         (i_clk_108M),
        .i_clk_27M_en       (i_clk_27M_en),
        .i_rst_n            (i_rst_n),
        .i_sync_point       (sync_point),           // 同步点
        .i_demod_result     (i_demod_result),       // 解调结果
        .i_demod_result_sck (i_demod_result_sck),   // 同步有效信号

        .o_demod_en         (o_demod_en),             // 解调使能信号
        .o_framing_start    (o_framing_start)         // 起始组帧标志
    );
    

endmodule
// 根据实虚部计算出相位
module phase_calu(
    input   wire    i_clk_108M,
    input   wire    i_rst_n,
    input   wire    i_clk_27M_en,
    input   wire    signed  [12:0]  iv_im_data,
    input   wire    signed  [12:0]  iv_re_data,

    output  wire    [15:0]  angle_tan
);

//----------------------------------- 噪声判断标志 ------------------------------//
    
    // 计算绝对值是为了估算幅值再对幅值进行均值平滑，与噪声门限值比较以判断噪声
    // 先对实虚部数据计算绝对值
    reg [12:0]  abs_im_data;
    reg [12:0]  abs_re_data;

    /*
        求绝对值后范围应在-4096~4095，如果超过该范围则有溢出。
        正数 ： 绝对值等于原码；
        负数 ： 绝对值两种计算方法
            (1)先取反，再加1，即补码的补码等于原码，但不同之处在于符号位也包含在取反范围内。
            (2)先减1，再对所有位取反，即求补码的逆向操作，但符号位也包括在内。
        对于负数，如果除符号位之外全0，则该负数为-4096，求绝对值后会溢出为8192，防溢出为4095。
    */
    
    // 虚部求绝对值
    always@(posedge i_clk_108M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)
            abs_im_data <= 13'd0;
        else if(iv_im_data[12] == 1'b1)begin        // 负数
            if(iv_im_data[11:0] == 12'h000)
                abs_im_data <= {1'b0,{12{1'b1}}};   // 防溢出
            else                                    // 未溢出
                abs_im_data <= ~iv_im_data + 1'b1;  // 按照方式1求绝对值
        end
        else                                        // 正数
            abs_im_data <= iv_im_data;
    end

    // 实部求绝对值
    always@(posedge i_clk_108M or negedge i_rst_n)begin
        if(i_rst_n == 1'b0)
            abs_re_data <= 13'd0;
        else if(iv_re_data[12] == 1'b1)begin
            if(iv_re_data[11:0] == 12'h000)
                abs_re_data <= {1'b0,{12{1'b1}}}; 
            else
                abs_re_data <= ~iv_re_data + 1'b1;
        end
        else
            abs_re_data <= iv_re_data;
    end

    // 求绝对值之后，为无符号数，最高位为0，可以舍弃
    wire    [11:0]  re_abs,im_abs;

    assign  re_abs = abs_re_data[11:0];
    assign  im_abs = abs_im_data[11:0];

//----------------------------------- 计算相位信息 ------------------------------//

    // quadrant[3:2]的00~11依次表示一二三四象限， quadrant[1]表示是否在象限中间pi/4上， quadrant[0]表示在象限的前/后位置
    reg [3:0]   quadrant;       // 象限信息

    // 将相位信息归一化到0~pi/4
    reg [11:0]  tan_numer;
    reg [11:0]  tan_denom;

    // 只考虑数据位，不考虑符号位，注意负数部分是补码
    wire    [12:0]  re_data_part;
    wire    [12:0]  im_data_part;

    // 做成wire信号更简洁
    /* 用4096做减法的原因：
        数据全部归一化到第一象限即求绝对值，负数最小是4096，
        在补码表示的条件下，不考虑符号位，13bit表示的最小数据是-4096，补码为13'd1_0000_0000_0000
        不考虑符号位时，补码的数据位是全0，因此要用4096减去数据位，即相当于对-4096求绝对值为4096.
    */
    assign  re_data_part = (iv_re_data[12] == 1'b1) ? (13'd4096 - {1'b0, iv_re_data[11:0]}) : iv_re_data;
    assign  im_data_part = (iv_im_data[12] == 1'b1) ? (13'd4096 - {1'b0, iv_im_data[11:0]}) : iv_im_data;

    // 相位计算仍然使用的是有符号的实虚部 tan(theta)=y/x
    // 由于要归一化到0~pi/4，所以必须调整为将计算的实虚部调整到 x < y
    always@(posedge i_clk_108M or negedge i_rst_n)
    begin
        if(i_rst_n == 1'b0)begin 
            tan_numer <= 12'd0;             // 被除数，即分子
            tan_denom <= 12'd1;             // 除数，即分母
            quadrant  <= 4'b0;              // 象限
        end
        else if(i_clk_27M_en == 1'b1)begin 
            // 第一象限,实虚部都是正数
            if((iv_re_data[12] == 1'b0)&&(iv_im_data[12] == 1'b0))begin 
                if(re_data_part[11:0] > im_data_part[11:0])begin        // 
                    tan_numer <= im_data_part[11:0];
                    tan_denom <= re_data_part[11:0];
                    quadrant  <= 4'b00_0_0;         // 象限前半部分
                end
                else if(re_data_part[11:0] == im_data_part[11:0])begin 
                    tan_numer <= re_data_part[11:0];
                    tan_denom <= im_data_part[11:0];
                    quadrant  <= 4'b00_0_1;         // 在象限的角平分线上(视作在前半部分,quadrant[1]=0)
                end
                else begin          // iv_re_data[11:0] < iv_im_data[11:0]
                    tan_numer <= re_data_part[11:0];
                    tan_denom <= im_data_part[11:0];
                    quadrant  <= 4'b00_1_0;         // 象限的后半部分
                end
            end
            // 第二象限，实部负，虚部正
            else if((iv_re_data[12] == 1'b1)&&(iv_im_data[12] == 1'b0))begin 
                if(re_data_part[11:0] < im_data_part[11:0])begin 
                    tan_numer <= re_data_part[11:0];
                    tan_denom <= im_data_part[11:0];
                    quadrant  <= 4'b01_0_0;
                end
                else if(re_data_part[11:0] == im_data_part[11:0])begin 
                    tan_numer <= re_data_part[11:0];
                    tan_denom <= im_data_part[11:0];
                    quadrant  <= 4'b01_0_1;
                end
                else begin          // re_data_part[11:0] > im_data_part[11:0]
                    tan_numer <= im_data_part[11:0];
                    tan_denom <= re_data_part[11:0];
                    quadrant  <= 4'b01_1_0;
                end
            end
            // 第三象限，实部负，虚部负
            else if((iv_re_data[12] == 1'b1)&&(iv_im_data[12] == 1'b1))begin
                if(re_data_part[11:0] > im_data_part[11:0])begin
                    tan_numer <= im_data_part[11:0];
                    tan_denom <= re_data_part[11:0];
                    quadrant  <= 4'b10_0_0;
                end
                else if(re_data_part[11:0] == im_data_part[11:0])begin 
                    tan_numer <= re_data_part[11:0];
                    tan_denom <= im_data_part[11:0];
                    quadrant  <= 4'b10_0_1;
                end
                else begin          // re_data_part[11:0] < im_data_part[11:0]
                    tan_numer <= re_data_part[11:0];
                    tan_denom <= im_data_part[11:0];
                    quadrant  <= 4'b10_1_0;
                end
            end
            // 第四象限，实部正，虚部负
            else if((iv_re_data[12] == 1'b0)&&(iv_im_data[12] == 1'b1))begin
                if(re_data_part[11:0] < im_data_part[11:0])begin
                    tan_numer <= re_data_part[11:0];
                    tan_denom <= im_data_part[11:0];
                    quadrant  <= 4'b11_0_0;
                end
                else if(re_data_part[11:0] == im_data_part[11:0])begin
                    tan_numer <= re_data_part[11:0];
                    tan_denom <= im_data_part[11:0];
                    quadrant  <= 4'b11_0_1;
                end
                else begin          // re_data_part[11:0] > im_data_part[11:0]
                    tan_numer <= im_data_part[11:0];
                    tan_denom <= re_data_part[11:0];
                    quadrant  <= 4'b11_1_0;
                end
            end
            else begin 
                tan_numer <= tan_numer;                        
                tan_denom <= tan_denom;                        
                quadrant  <= quadrant;                            
            end
        end
    end

    wire    [20:0]  tan_quotient;
    wire    [11:0]  tan_remain;
    
    // 通过除法器计算arctan(y/x)，计算的结果是归一化的
    // 分子和0拼接相当于扩大2^9倍,最后商的主要数据部分在低9位
    // 为了减小查找表的规模，这里将除法结果(即tan值)的精度少1bit。
    // 由于2pi~8192,所以pi/4~1024,根据tan值得到10位的角度theta.
    // 但是利用除法器计算会有分母为0时无意义的情况，以后考虑用Cordic算法代替计算.
    
    tan_divider U_tan_divider(
	    .denom   (tan_denom),            // 分母
	    .numer   ({tan_numer,9'b0}),     // 分子         
	    .quotient(tan_quotient),         // 商
	    .remain  (tan_remain)            // 余数
    );

    wire    [8:0]   arctan;
    
    // 查表得到arctan值
    rom_tan U_rom_tan(
	    .address(tan_quotient[8:0]),
	    .clock  (i_clk_108M),
	    .q      (arctan)
    );



endmodule
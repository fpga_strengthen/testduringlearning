clear
% 根据每一次的DA输入来得到相应的查表结果，与滤波器输入数据的字长无关
% 滤波器系数补码
filter_coeff_bin = [1111010010110000,1110000110010000,1110000110001010,0000111100101110,0101011000100000,...
                    0111100101000000,...
                    0101011000100000,0000111100101110,1110000110001010,1110000110010000,1111010010110000];
% 滤波器长度，亦即输入DA字长，取决于滤波器阶数
N_TAPS = length(filter_coeff_bin);

% 输入DA，非滤波数据，范围从 11'b0000_0000_000 ~ 11'b1111_1111_111
da_in = 0:(2^N_TAPS-1);

% 补码转十进制表示，便于计算
filter_coeff_dec = zeros(1,N_TAPS);
% 调用自定义函数实现
for i=1:length(filter_coeff_bin)
    filter_coeff_dec(i) = format_trans(num2str(filter_coeff_bin(i)));
end

% 将输入的数据范围
filter_in_trans = dec2bin(da_in);
% da制表结果保存(十进制)，用于写入ROM
DA_table_dec = zeros(1,2^N_TAPS);
sum = 0;

% ROM表格规模:2^N_TAPS
for i=1:2^N_TAPS
    for j=1:N_TAPS
        sum = sum + str2double(filter_in_trans(i,j)) * filter_coeff_dec(j);
    end
    DA_table_dec(i) = sum;
    sum = 0;        % 计算完成后要清零，不影响下一次计算
end

% 将DA_Table的十进制转化为二进制
DA_Table_Bin = zeros(1,2^N_TAPS);
for i=1:length(DA_Table_Bin)
   x = dec2bin(DA_table_dec(i));
   l = length(x);
   if l <= 16
       DA_Table_Bin(i) = str2num(x);
   end
   if l > 16
       DA_Table_Bin(i) = str2num(x(16:end));
   end
end

% 将DA结果写入文件
fid = fopen('DA_Table.mif','w+');
fprintf(fid,'%s\n','WIDTH=17;');
fprintf(fid,'%s\n','DEPTH=2048;');

fprintf(fid,'\n%s\n','ADDRESS_RADIX=UNS;');
fprintf(fid,'\n%s\n','DATA_RADIX=BIN;');
fprintf(fid,'\n');
fprintf(fid,'%s\n','CONTENT BEGIN');
for i=1:2^N_TAPS
    fprintf(fid,'\t\t\t%d%s%d%s\n',i-1,':',DA_Table_Bin(i),';');
end
fprintf(fid,'%s','END;');



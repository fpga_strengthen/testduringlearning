-- Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
-- Date        : Sat Aug 24 17:22:58 2024
-- Host        : J_GMson running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               d:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/shift_ram_w16xd64/shift_ram_w16xd64_stub.vhdl
-- Design      : shift_ram_w16xd64
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7vx485tffg1157-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity shift_ram_w16xd64 is
  Port ( 
    A : in STD_LOGIC_VECTOR ( 5 downto 0 );
    D : in STD_LOGIC_VECTOR ( 15 downto 0 );
    CLK : in STD_LOGIC;
    Q : out STD_LOGIC_VECTOR ( 15 downto 0 )
  );

end shift_ram_w16xd64;

architecture stub of shift_ram_w16xd64 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "A[5:0],D[15:0],CLK,Q[15:0]";
attribute x_core_info : string;
attribute x_core_info of stub : architecture is "c_shift_ram_v12_0_13,Vivado 2019.1";
begin
end;

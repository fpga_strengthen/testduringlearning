onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib fifo_data_w8xd2048_opt

do {wave.do}

view wave
view structure
view signals

do {fifo_data_w8xd2048.udo}

run -all

quit -force

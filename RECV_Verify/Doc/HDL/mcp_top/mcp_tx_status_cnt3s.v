//============================================================
//
//--- ALL RIGHTS RESERVED
//--- File		: mcp_tx_status_cnt3s.v
//--- Auther	: smartbai
//--- Data		: 2020-11-20
//--- Version	: v1.0.0.0
//--- Abstract	: 
//
//------------------------------------------------------------
//Description	:
//------------------------------------------------------------
//--- Modification History:
//--- Date		By		Version		Change Description
//------------------------------------------------------------
//
//============================================================

module mcp_tx_status_cnt3s (
    i_clk,
    i_rst_n,

    i_ant_err,
    i_ant_nor,
    i_msg_framing,
    o_tx_status_en
);
    input i_clk;
    input i_rst_n;

    input i_ant_err;
    input i_ant_nor;
    input i_msg_framing;
    output o_tx_status_en;


//--------------------------------------------------------//
// 对输入状态信号打拍或取沿
    reg ant_err_r;
    reg ant_err_p;
    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n) begin
            ant_err_r <= 1'b0;
            ant_err_p <= 1'b0;
        end
        else begin
            ant_err_r <= i_ant_err;
            ant_err_p <= i_ant_err && (~ant_err_r);
        end
    end

    reg ant_nor_r;
    reg ant_nor_p;
    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n) begin
            ant_nor_r <= 1'b0;
            ant_nor_p <= 1'b0;
        end
        else begin
            ant_nor_r <= i_ant_nor;
            ant_nor_p <= i_ant_nor && (~ant_nor_r);
        end
    end

    reg msg_framing_r1;
    reg msg_framing_r2;
    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n) begin
            msg_framing_r1 <= 1'b0;
            msg_framing_r2 <= 1'b0;
        end
        else begin
            msg_framing_r1 <= i_msg_framing;
            msg_framing_r2 <= msg_framing_r1;
        end
    end

//--------------------------------------------------------------------------------------------//


reg tx_status_en;
always @(posedge i_clk or negedge i_rst_n) begin
    if(~i_rst_n)
        tx_status_en <= 1'b0;
    else if(msg_framing_r2)
        tx_status_en <= 1'b0;
    else if(ant_err_p || ant_nor_p)
        tx_status_en <= 1'b1;
    else
        tx_status_en <= 1'b0;
end

assign o_tx_status_en = tx_status_en;

endmodule
`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date: 2024/08/14 20:50:20
// Module Name: rgmii_interface
// Description: 
// Revision 0.01 - File Created
//////////////////////////////////////////////////////////////////////////////////

module rgmii_interface(
	// system reset
	// input		wire			i_rst_n,

	//------------------ Rx -------------------------\\
	// Reference clk for delay
	input		wire			i_delay_refclk,		// 200MHz
	// signals from PHY
	input		wire			i_phy_rgmii_rx_clk,
	input		wire			i_phy_rgmii_rx_ctl,
	// Data comes by two parts in both posedge and negedge.
	input		wire	[3:0]	iv_phy_rgmii_rx_data,

	output		wire			o_gmii_rx_clk,
	output		wire			o_gmii_rx_vld,
	output		wire			o_gmii_rx_error,
	// Data outputs by only posedge,so its width turns to be 8 from 4.
	output		wire	[7:0]	ov_gmii_rx_data,
	
	//------------------- Tx ------------------------\\
	input		wire			i_gmii_tx_clk,
	input		wire			i_gmii_tx_vld, 
	// Data outputs by only posedge,so its width turns to be 8 from 4.
	input		wire	[7:0]	iv_gmii_tx_data,
	// signals from PHY
	output		wire			o_phy_rgmii_tx_clk,
	output		wire			o_phy_rgmii_tx_ctl,
	// Data comes by two parts in both posedge and negedge.
	output		wire	[3:0]	ov_phy_rgmii_tx_data
);

	rgmii_receive U_rgmi_receive(
		// reset
		// .i_rst_n				(i_rst_n),
		// Reference clk for delay
		.i_delay_refclk			(i_delay_refclk),		// 200MHz
		// signals from PHY
		.i_phy_rgmii_rx_clk		(i_phy_rgmii_rx_clk),
		.i_phy_rgmii_rx_ctl		(i_phy_rgmii_rx_ctl),
		// Data comes by two parts in both posedge and negedge.
		.iv_phy_rgmii_rx_data	(iv_phy_rgmii_rx_data),

		.o_gmii_rx_clk			(o_gmii_rx_clk),
		.o_gmii_rx_vld			(o_gmii_rx_vld),
		.o_gmii_rx_error		(o_gmii_rx_error),
		// Data outputs by only posedge,so its width turns to be 8 from 4.
		.ov_gmii_rx_data		(ov_gmii_rx_data)
    );

    rgmii_send U_rgmii_send(
		// .i_rst_n              (i_rst_n),
		.i_gmii_tx_clk        (i_gmii_tx_clk),
		.i_gmii_tx_vld        (i_gmii_tx_vld),
		.iv_gmii_tx_data      (iv_gmii_tx_data),

		.o_phy_rgmii_tx_clk   (o_phy_rgmii_tx_clk),
		.o_phy_rgmii_tx_ctl   (o_phy_rgmii_tx_ctl),
		.ov_phy_rgmii_tx_data (ov_phy_rgmii_tx_data)
	);

endmodule

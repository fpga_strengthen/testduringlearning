module light(
        i_rst_n,
        i_clk_27M,

        i_m_ready,
        lamp1_1,  //有FSK信号
        lamp1_2,  //有FSK信号
        lamp2_1,  //译出报文
        lamp2_2,  //
        lamp3_1,  //译出自检
        lamp3_2,  //译出自检
        lamp4_1,  //天线异常
        lamp4_2,  //天线异常

        o_led_clk,
        o_led_data
    );

input i_rst_n;
input i_clk_27M;
input i_m_ready;
input lamp1_1;
input lamp1_2;
input lamp2_1;
input lamp2_2;
input lamp3_1;
input lamp3_2;
input lamp4_1;
input lamp4_2;
output  o_led_clk;
output  o_led_data;

//--------------------------------信号声明------------------------------------------//
reg lamp1_r1;
reg lamp1_r2;
reg lamp1_p;

reg [9:0]cnt_pulse_num;
reg [5:0]cnt_15;

reg m_ready_r1;
reg m_ready_r2;
reg m_ready_p;

reg lamp1_contr/*synthesis noprune*/;
reg lamp1_temp1;
reg lamp1_temp2;
wire lamp11_p;
reg lamp2_temp1;
reg lamp2_temp2;
wire lamp12_p;
reg lamp3_temp1;
reg lamp3_temp2;
wire lamp21_p;
reg lamp4_temp1;
reg lamp4_temp2;
wire lamp22_p;
reg lamp5_temp1;
reg lamp5_temp2;
wire lamp31_p;
reg lamp6_temp1;
reg lamp6_temp2;
wire lamp32_p;
reg lamp7_temp1;
reg lamp7_temp2;
wire lamp41_p;
reg lamp8_temp1;
reg lamp8_temp2;
wire lamp42_p;

reg [4:0]cnt_24;

reg clk_564k;//时钟分频,输入时钟的24分频
reg clk_564k1;
reg clk_564k2;
wire clk882k_p;

reg  [24:0]cnt_time1_1;//led1_1用来计时500ms
reg  [24:0]cnt_time1_2;
reg  [24:0]cnt_time2_1;
reg  [24:0]cnt_time2_2;
reg  [24:0]cnt_time3_1;
reg  [24:0]cnt_time3_2;

reg led1_1;
reg led1_2;
reg led2_1;
reg led2_2;
reg led3_1;
reg led3_2;
reg led4_1;
reg led4_2;

reg [7:0] led;
reg [10:0] cnt9;
reg light_ctrl;
wire o_led_clk;
reg o_led_data;

//*********单独处理lamp1和lamp2的点灯信号********//
//lamp1 和 lamp2表示收到fsk信号。用（o_sync_ok & defsk_wen8）脉冲点亮。正常的点灯信号（o_sync_ok & defsk_wen8）是一系列有规律的脉冲。
//当有干扰存在时，偶尔会出现几个脉冲将两个灯点亮。这种现象是错误的，为了解决这种错误，让（o_sync_ok & defsk_wen8）计数一定数目后再点灯。
//lamp1 和 lamp2的点灯控制信号相同，控制信号为脉冲信号。将控制信号脉冲计数一定数目后作为新的点灯信号

// 跨时钟域处理 lamp1_1(81M)
reg lamp1_1_r;
always @(posedge i_clk_27M or posedge lamp1_1)
begin
    if(lamp1_1)
        lamp1_1_r <= 1'b1;
    else
        lamp1_1_r <= 1'b0;
end

//将lamp1 和 lamp2的点灯控制信号寄存
always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n) begin
        lamp1_r1 <= 1'b0;
        lamp1_r2 <= 1'b0;
        lamp1_p <= 1'b0;
    end
    else begin
        lamp1_r1 <= lamp1_1_r;
        lamp1_r2 <= lamp1_r1;
        lamp1_p <= lamp1_r1 & (~lamp1_r2);
    end

//计数两个脉冲之间的脉冲数目,正常情况下，两个脉冲间隔为48*8
//超过48*15个时钟没有计数脉冲，认为没有脉冲
always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n)
        cnt_pulse_num <= 10'd0;
    else if(lamp1_p)
        cnt_pulse_num <= 10'd0;
    else if(cnt_pulse_num>=10'd720)
        cnt_pulse_num <= cnt_pulse_num;
    else
        cnt_pulse_num <= cnt_pulse_num + 10'd1;

//长度为8的计数器。将点灯脉冲计数超过15以后再点灯。
always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n)
        cnt_15 <= 6'd0;
    else if(cnt_pulse_num>=10'd720)
        cnt_15 <= 6'd0;
    else if(cnt_15>=6'd15)
        cnt_15 <= cnt_15;
    else if(lamp1_p)
        cnt_15 <= cnt_15 + 6'd1;
    else
        cnt_15 <= cnt_15;

//i_m_ready取沿，自检信号来的时候点亮A灯，作为因cnt_8太大而漏掉自检点灯信号的补充
always @(negedge i_rst_n or posedge i_clk_27M) begin
    if(!i_rst_n) begin
        m_ready_r1 <= 1'b0;
        m_ready_r2 <= 1'b0;
        m_ready_p <= 1'b0;
    end
    else begin
        m_ready_r1 <= i_m_ready;
        m_ready_r2 <= m_ready_r1;
        m_ready_p <= m_ready_r1 & (~m_ready_r2);
    end
end

//控制A排灯点灯的控制信号
always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n)
        lamp1_contr <= 1'd0;
    else if(m_ready_p)
        lamp1_contr <= 1'd1;
    else if(cnt_15>=6'd15)
        lamp1_contr <= lamp1_1_r;
    else
        lamp1_contr <= 1'd0;

always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n) begin
        lamp1_temp1 <= 1'b0;
        lamp1_temp2 <= 1'b0;
    end
    else begin
        lamp1_temp1 <= lamp1_contr;
        lamp1_temp2 <= lamp1_temp1;
    end

assign  lamp11_p = ~lamp1_temp2 && lamp1_temp1;

always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n) begin
        lamp2_temp1 <= 1'b0;
        lamp2_temp2 <= 1'b0;
    end
    else begin
        lamp2_temp1 <= lamp1_contr;
        lamp2_temp2 <= lamp2_temp1;
    end

assign  lamp12_p = ~lamp2_temp2 && lamp2_temp1;

// 跨时钟域处理 lamp2_1(81M)
reg lamp2_1_r;
always @(posedge i_clk_27M or posedge lamp2_1)
begin
    if(lamp2_1)
        lamp2_1_r <= 1'b1;
    else
        lamp2_1_r <= 1'b0;
end

always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n) begin
        lamp3_temp1 <= 1'b0;
        lamp3_temp2 <= 1'b0;
    end
    else begin
        lamp3_temp1 <= lamp2_1_r;
        lamp3_temp2 <= lamp3_temp1;
    end

assign  lamp21_p = ~lamp3_temp2 && lamp3_temp1;

always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n) begin
        lamp4_temp1 <= 1'b0;
        lamp4_temp2 <= 1'b0;
    end
    else begin
        lamp4_temp1 <= lamp2_2;
        lamp4_temp2 <= lamp4_temp1;
    end

assign  lamp22_p = ~lamp4_temp2 && lamp4_temp1;

always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n) begin
        lamp5_temp1 <= 1'b0;
        lamp5_temp2 <= 1'b0;
    end
    else begin
        lamp5_temp1 <= lamp3_1;
        lamp5_temp2 <= lamp5_temp1;
    end

assign  lamp31_p = ~lamp5_temp2 && lamp5_temp1;

always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n) begin
        lamp6_temp1 <= 1'b0;
        lamp6_temp2 <= 1'b0;
    end
    else begin
        lamp6_temp1 <= lamp3_2;
        lamp6_temp2 <= lamp6_temp1;
    end

assign  lamp32_p = ~lamp6_temp2 && lamp6_temp1;

always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n) begin
        lamp7_temp1 <= 1'b0;
        lamp7_temp2 <= 1'b0;
    end
    else begin
        lamp7_temp1 <= lamp4_1;
        lamp7_temp2 <= lamp7_temp1;
    end

assign  lamp41_p = ~lamp7_temp2 && lamp7_temp1;

always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n) begin
        lamp8_temp1 <= 1'b0;
        lamp8_temp2 <= 1'b0;
    end
    else begin
        lamp8_temp1 <= lamp4_2;
        lamp8_temp2 <= lamp8_temp1;
    end

assign  lamp42_p = ~lamp8_temp2 && lamp8_temp1;

always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n)
        cnt_24 <= 5'd0;
    else if(cnt_24 == 5'd23)
        cnt_24 <= 5'd0;
    else
        cnt_24 <= cnt_24 + 5'd1;

always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n)
        clk_564k <= 1'b0;
    else if(cnt_24 == 5'd12)
        clk_564k <= ~clk_564k;

always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n) begin
        clk_564k1 <= 1'b0;
        clk_564k2 <= 1'b0;
    end
    else begin
        clk_564k1 <= clk_564k;
        clk_564k2 <= clk_564k1;
    end

assign clk882k_p = clk_564k1&(~clk_564k2);

//将每一个灯的状态分别记录下来,单独为每一个灯设定一个计时器
always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n)
        cnt_time1_1 <= 25'd0;
    else if( cnt_time1_1[23] == 1'b1 )
        cnt_time1_1 <= 25'd0;
    else if( lamp11_p == 1'b1)
        cnt_time1_1 <= 25'd1;
    else if( cnt_time1_1 )
        cnt_time1_1 <= cnt_time1_1 + 1'b1;
    else
        cnt_time1_1 <= cnt_time1_1;

always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n)
        cnt_time1_2 <= 25'd0;
    else if( cnt_time1_2[23] == 1'b1 )
        cnt_time1_2 <= 25'd0;
    else if( lamp12_p == 1'b1)
        cnt_time1_2 <= cnt_time1_2 + 1'b1;
    else if( cnt_time1_2 )
        cnt_time1_2 <= cnt_time1_2 + 1'b1;
    else
        cnt_time1_2 <= cnt_time1_2;

always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n)
        cnt_time2_1 <= 25'd0;
    else if( cnt_time2_1[23] == 1'b1 )
        cnt_time2_1 <= 25'd0;
    else if( lamp21_p == 1'b1)
        cnt_time2_1 <= cnt_time2_1 + 1'b1;
    else if( cnt_time2_1 )
        cnt_time2_1 <= cnt_time2_1 + 1'b1;
    else
        cnt_time2_1 <= cnt_time2_1;

always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n)
        cnt_time2_2 <= 25'd0;
    else if( cnt_time2_2[23] == 1'b1 )
        cnt_time2_2 <= 25'd0;
    else if( lamp22_p == 1'b1)
        cnt_time2_2 <= cnt_time2_2 + 1'b1;
    else if( cnt_time2_2 )
        cnt_time2_2 <= cnt_time2_2 + 1'b1;
    else
        cnt_time2_2 <= cnt_time2_2;

always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n)
        cnt_time3_1 <= 25'd0;
    else if( cnt_time3_1[23] == 1'b1 )
        cnt_time3_1 <= 25'd0;
    else if( lamp31_p == 1'b1)
        cnt_time3_1 <= cnt_time3_1 + 1'b1;
    else if( cnt_time3_1 )
        cnt_time3_1 <= cnt_time3_1 + 1'b1;
    else
        cnt_time3_1 <= cnt_time3_1;

always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n)
        cnt_time3_2 <= 25'd0;
    else if( cnt_time3_2[23] == 1'b1 )
        cnt_time3_2 <= 25'd0;
    else if( lamp32_p == 1'b1)
        cnt_time3_2 <= cnt_time3_2 + 1'b1;
    else if( cnt_time3_2 )
        cnt_time3_2 <= cnt_time3_2 + 1'b1;
    else
        cnt_time3_2 <= cnt_time3_2;

///////////以下为点亮灯的程序，16个状态如果有一个变化就重新刷寄存器，重新点灯/////////////

always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n)
        led1_1 <= 1'b0;
    else if (25'd1<=(cnt_time1_1) )
        led1_1 <= 1'b1;
    else
        led1_1 <= 1'b0;

always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n)
        led1_2 <= 1'b0;
    else if (cnt_time1_2)
        led1_2 <= 1'b1;
    else
        led1_2 <= 1'b0;

always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n)
        led2_1 <= 1'b0;
    else if(cnt_time2_1)
        led2_1 <= 1'b1;
    else
        led2_1 <= 1'b0;

always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n)
        led2_2 <= 1'b0;
    else if(cnt_time2_2)
        led2_2 <= 1'b1;
    else
        led2_2 <= 1'b0;

always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n)
        led3_1 <= 1'b0;
    else if(cnt_time3_1)
        led3_1 <= 1'b1;
    else
        led3_1 <= 1'b0;

always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n)
        led3_2 <= 1'b0;
    else if(cnt_time3_2)
        led3_2 <= 1'b1;
    else
        led3_2 <= 1'b0;

always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n)
        led4_1 <= 1'b0;
    else if(lamp41_p == 1'b1)
        led4_1 <= 1'b1;
    else if(lamp21_p  || lamp12_p)
        led4_1 <= 1'b0;
    else
        led4_1 <= led4_1;

always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n)
        led4_2 <= 1'b0;
    else if(lamp42_p == 1'b1)
        led4_2 <= 1'b1;
    else if(lamp12_p ||  lamp21_p)
        led4_2 <= 1'b0;
    else
        led4_2 <= led4_2;

always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n)
        led <= 8'b0;
    else
        led <={~led2_1,~led2_1,~led1_1,~led1_1,~led4_1,~led4_2,~led3_1,~led3_2};

////////74芯片时钟的控制信号///////
always @(negedge i_rst_n or posedge i_clk_27M )
    if(!i_rst_n)
        cnt9 <= 11'd0;
    else if(clk882k_p)
        cnt9 <= cnt9 + 1'b1;
    else
        cnt9 <= cnt9;

always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n)
        light_ctrl <= 1'b0;
    else if( ( cnt9 >= 11'd1 ) && ( cnt9 <= 11'd8 )  )
        light_ctrl <= 1'b1;
    else
        light_ctrl <= 1'b0;

////给74芯片的时钟//////////
assign o_led_clk = light_ctrl & clk882k_p ;

///////刷新移位寄存器//////////////////
always @(negedge i_rst_n or posedge i_clk_27M)
    if(!i_rst_n)
        o_led_data <= 1'b0;
    else begin
        case (cnt9)
            10'd1:
                o_led_data <= led[7];
            10'd2:
                o_led_data <= led[6];
            10'd3:
                o_led_data <= led[5];
            10'd4:
                o_led_data <= led[4];
            10'd5:
                o_led_data <= led[3];
            10'd6:
                o_led_data <= led[2];
            10'd7:
                o_led_data <= led[1];
            10'd8:
                o_led_data <= led[0];
            default:
                o_led_data <= o_led_data;
        endcase
    end

endmodule

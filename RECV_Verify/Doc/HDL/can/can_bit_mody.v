//+FHEADER //////////////////////////////////////////////////////////////////////
//  版权所有者： 
//  Copyright 2014. All Rights Reserved.
//  保密级别：  
//---------------------------------------------------------------------------
//  文件名 :  can_bit_mody.v
//  原始作者：丁云冰
//  联系方式：
// --------------------------------------------------------------------------
// 版本升级历史：  2.0主要把原来版本的状态机去掉，换成了控制计数器进行控制状态
//                 流转
// 版本生效日期：  作者    版本
// 1.0 2014-02-21  丁云冰  第1版  
// 2.0 2015-10-17  郭庆阳  第2版   
//---------------------------------------------------------------------------
// 关键字 :   can_spi control， 位修改
//---------------------------------------------------------------------------
// 模块功能: modify some bits of MCP2515 CANINIF register
//           通过spi指令利用屏蔽位和屏蔽数据对中断寄存器完成复位，eg：   
//           original data: 11111111
//           screen   byte: 10000001
//           data         : 00000000
//           result       : 01111110
// --------------------------------------------------------------------------
// 参数文件名：  
// --------------------------------------------------------------------------
// 重复使用问题：
// 复位策略:负跳变沿异步复位信号 i_rst_n
// 时钟域 : i_clk_27M 
// 关键时序: N/A
// 是否需要实例引用别的模块 : No
// 可综合否 : Yes 
//-FHEADER //////////////////////////////////////////////////////////////////////
module can_bit_mody
       (
          //time & rst
         i_clk_27M,
         i_rst_n,
         
         //input control
         i_send_ok,       //can_spi has worked finish,脉冲信号
         i_mody_en,       //this module work enable，高电平
         
         //input data 
         i_send_add,     //address of mcp2515 register need to mody
         i_send_data,    //data need to change to mcp2515 register
         i_screen_data,  //shield some bits
         
         //output control
         o_spi_tx_en,   //can_spi module send enable
         o_mody_ok,     // this module has work finish，脉冲信号
         
         //output data
         o_spi_data     //data send to can_spi module
         );
    input i_clk_27M;
    input i_rst_n;
    input i_send_ok;
    input i_mody_en;
    input[7:0] i_send_add; 
    input[7:0] i_send_data;
    input[7:0] i_screen_data; 
    
    output o_spi_tx_en;
    output o_mody_ok;
    output[7:0] o_spi_data;
  
//////////////////////////////////////////
  `include "parameter.v"
//////////////////////////////////////////
//output register
    reg mody_en_r;
    reg mody_en_p;
    
    reg[2:0] ctrl_cnt;
    reg o_spi_tx_en; 
    
    reg o_mody_ok;
    reg[7:0] o_spi_data;

/*************控制计数器计数*****************/
/*  该计数器用来控制向spi发送指令的顺序     */
/********************************************/

    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if(!i_rst_n) begin
            mody_en_r <= 0;
            mody_en_p <= 0;
        end
        else begin
            mody_en_r <= i_mody_en;
            mody_en_p <= i_mody_en && (~mody_en_r);
        end
    end
    
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if( !i_rst_n )
            ctrl_cnt <= 3'd0;
        else if( ctrl_cnt == 3'd5)
            ctrl_cnt <= 3'd0;
        else if( mody_en_p )
            ctrl_cnt <= 3'd1;
        else if( (ctrl_cnt != 3'd0) && i_send_ok ) 
            ctrl_cnt <= ctrl_cnt + 3'd1;
        else
            ctrl_cnt <= ctrl_cnt;
    end
    
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if(!i_rst_n) begin
            o_spi_tx_en <= 0;
            o_spi_data  <= 0;
            o_mody_ok   <= 0;
        end
        else begin
            case(ctrl_cnt)
            3'd1: begin
                o_spi_tx_en <= 1'b1;
                o_spi_data  <= `MODY_BIT;
            end
            3'd2: begin
                o_spi_tx_en <= 1'b1;
                o_spi_data  <= i_send_add;
            end
            3'd3: begin
                o_spi_tx_en <= 1'b1;
                o_spi_data  <= i_screen_data;
            end
            3'd4: begin
                o_spi_tx_en <= 1'b1;
                o_spi_data  <= i_send_data;
            end
            3'd5: begin
                o_spi_tx_en <= 0;
                o_spi_data <= 0;
                o_mody_ok <= 1'd1;
            end
            default: begin
                o_spi_tx_en <= 0;
                o_spi_data  <= 0;
                o_mody_ok   <= 0;
            end
            endcase
        end
    end
        
endmodule
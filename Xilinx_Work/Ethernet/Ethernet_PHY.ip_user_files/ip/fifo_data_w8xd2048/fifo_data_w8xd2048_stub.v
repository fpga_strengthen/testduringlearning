// Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
// Date        : Wed Aug 28 18:29:46 2024
// Host        : J_GMson running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               d:/My_FPGA_Work/Xilinx_Work/Ethernet/Ethernet_PHY.srcs/sources_1/ip/fifo_data_w8xd2048/fifo_data_w8xd2048_stub.v
// Design      : fifo_data_w8xd2048
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7vx485tffg1157-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "fifo_generator_v13_2_4,Vivado 2019.1" *)
module fifo_data_w8xd2048(clk, srst, din, wr_en, rd_en, dout, full, empty)
/* synthesis syn_black_box black_box_pad_pin="clk,srst,din[8:0],wr_en,rd_en,dout[8:0],full,empty" */;
  input clk;
  input srst;
  input [8:0]din;
  input wr_en;
  input rd_en;
  output [8:0]dout;
  output full;
  output empty;
endmodule

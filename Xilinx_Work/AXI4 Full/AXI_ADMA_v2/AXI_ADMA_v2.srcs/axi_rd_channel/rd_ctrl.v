`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Module Name: rd_ctrl
// Description: 
// Revision 0.01 - File Created
//////////////////////////////////////////////////////////////////////////////////

module rd_ctrl #(
	parameter   AXI_DATA_WIDTH     = 128,				// AXI数据位宽
	parameter   AXI_ADDR_WIDTH     = 32,				// AXI读地址宽度
	parameter   RD_BURST_LENGTH    = 4096				// 最大读突发长度,以字节为单位
	)(
	input  wire								i_clk,					// 用户读时钟
	input  wire								i_rst_n,				// 复位

	input  wire								i_ddr_init_done,		// DDR3初始化完成信号

	/*--------------------------用户端信号-----------------------------*/
	input  wire								i_user_rd_req,			// 数据同步有效信号
	input  wire	[AXI_ADDR_WIDTH-1:0]		iv_user_rd_base_addr,	// 用户读数据的基地址
	input  wire	[AXI_ADDR_WIDTH-1:0]		iv_user_rd_end_addr,	// 用户读数据的结束地址
	output wire								o_rd_req_busy,			// 用户请求忙标志,此时当前模块无法处理新请求

	// v2版本新添加的用户端口信号
	input  wire								i_user_rd_mode,			// 用户读模式设置:0表示v1版本,1表示可以设定读地址的v2版本
	input  wire	[AXI_ADDR_WIDTH-1:0]		iv_user_rd_addr,
	input  wire	[12:0]						iv_user_rd_length,

	/*----------------------与rd_buffer交互信号------------------------*/
	output reg								o_rd_req_en,			// 读请求使能信号
	output reg 	[7:0]						ov_rd_burst_length,		// 读突发长度
	output reg	[AXI_ADDR_WIDTH-1:0]		ov_rd_data_addr,		// 读地址
	input  wire								i_rd_req_ready			// 用来指示cmd_fifo的写情况,是否有爆满
);

	// 状态机定义
	reg  [1:0]	cur_status;
	reg  [1:0]	nxt_status;

	localparam	RD_IDLE = 2'b00,		// 空闲状态
				RD_REQ  = 2'b01,		// 读请求状态
				RD_END  = 2'b10;		// 读地址发送完成

	// 最大突发长度:指当前数据宽度在指定的突发长度下需要突发的次数.
	localparam	MAX_BURST_LENGTH = RD_BURST_LENGTH/(AXI_DATA_WIDTH/8) - 1;

	// 复位信号打拍,确保和时钟同步
	(* dont_touch="true" *)reg		rst_n_r0;
	(* dont_touch="true" *)reg		rst_n_r1;
   	(* dont_touch="true" *)reg		rst_n;

   	// DDR初始化信号打拍
   	reg		ddr_init_done_d0;
   	reg		ddr_init_done_d1;
   	reg		ddr_wr_en;

   	// 对请求信号打拍取沿
   	reg		user_rd_req_d0;
   	reg		user_rd_req_d1;
   	wire	user_rd_req_p;
   	reg		rd_req_trigger;

   	reg 	o_err_4k_border;
	reg 	o_err_user_addr;

/*-------------------------------------------------*\
						CDC
\*-------------------------------------------------*/	

	// rst_n
	always @(posedge i_clk) begin
  		rst_n_r0 <= i_rst_n;
  		rst_n_r1 <= rst_n_r0;
  		rst_n    <= rst_n_r1;
  	end

  	// DDR init_done
  	always @(posedge i_clk or negedge rst_n) begin
  		if(~rst_n) begin
  			ddr_init_done_d0 <= 1'b0;
  			ddr_init_done_d1 <= 1'b0;
  			ddr_wr_en 		 <= 1'b0;
  		end else begin
  			ddr_init_done_d0 <= i_ddr_init_done;
  			ddr_init_done_d1 <= ddr_init_done_d0;
  			ddr_wr_en		 <= ddr_init_done_d1;
  		end
  	end

  	// 请求信号打2拍
  	always @(posedge i_clk or negedge rst_n) begin
  		if(~rst_n) begin
  			user_rd_req_d0 <= 1'b0;
  			user_rd_req_d1 <= 1'b0;
  		end else begin
  			user_rd_req_d0 <= i_user_rd_req;
  			user_rd_req_d1 <= user_rd_req_d0;
  		end
  	end

  	// 在写使能有效时对req_p打拍
  	always @(posedge i_clk or negedge rst_n) begin
  		if(~rst_n)
  			rd_req_trigger <= 1'b0;
  		else if(ddr_wr_en)
  			rd_req_trigger <= user_rd_req_p;
  		else
  			rd_req_trigger <= rd_req_trigger;
  	end

/*-------------------------------------------------*\
					 assign
\*-------------------------------------------------*/
	
	assign	o_rd_req_busy = (cur_status != RD_IDLE);
	// 取请求信号的上升沿
  	assign	user_rd_req_p = user_rd_req_d0 & (~user_rd_req_d1);

/*-------------------------------------------------*\
						状态机
\*-------------------------------------------------*/	

	always @(posedge i_clk or negedge rst_n) begin
		if(~rst_n)
			cur_status <= RD_IDLE;
		else 
			cur_status <= nxt_status;
	end

	always @(*) begin
		if(~rst_n) begin
			nxt_status <= RD_IDLE;
		end else begin
			case (cur_status)
				RD_IDLE : begin
					if(rd_req_trigger)
						nxt_status <= RD_REQ;
					else
						nxt_status <= cur_status;
				end
				RD_REQ : begin
					if(o_rd_req_en && i_rd_req_ready)
						nxt_status <= RD_END;
					else
						nxt_status <= cur_status;
				end
				RD_END  : nxt_status <= RD_IDLE;
				default : nxt_status <= RD_IDLE;
			endcase
		end
	end

/*-------------------------------------------------*\
				发送读请求信号给rd_buffer
\*-------------------------------------------------*/
	
	always @(posedge i_clk or negedge rst_n) begin
		if(~rst_n)
			o_rd_req_en <= 1'b0;
		else if(o_rd_req_en && i_rd_req_ready)
		 	o_rd_req_en <= 1'b0;
		else if(cur_status == RD_REQ)
			o_rd_req_en <= 1'b1;
		else
			o_rd_req_en <= o_rd_req_en;
	end

/*-------------------------------------------------*\
					  发送读地址
\*-------------------------------------------------*/

	always @(posedge i_clk or negedge rst_n) begin
		if(~rst_n) 
			ov_rd_data_addr <= iv_user_rd_base_addr;
		else if(i_user_rd_mode && rd_req_trigger)
			ov_rd_data_addr <= iv_user_rd_addr;
		else if(o_rd_req_en && i_rd_req_ready && (~i_user_rd_mode))begin
			if((ov_rd_data_addr >= iv_user_rd_end_addr - RD_BURST_LENGTH))
				ov_rd_data_addr <= iv_user_rd_base_addr;	// 当即将写满时就重新回到基地址
			else						// 收到写请求且未写满时就将突发首地址增加.
				ov_rd_data_addr <= ov_rd_data_addr + MAX_BURST_LENGTH;
		end
		else
			ov_rd_data_addr <= ov_rd_data_addr;
	end

/*-------------------------------------------------*\
					读触发长度
\*-------------------------------------------------*/
	
	always @(posedge i_clk or negedge rst_n) begin
		if(~rst_n)
			ov_rd_burst_length <= 13'h0;
		else if(rd_req_trigger && i_user_rd_mode)
			ov_rd_burst_length <= iv_user_rd_length/(AXI_DATA_WIDTH/8) - 1'b1;
		else if(~i_user_rd_mode)
			ov_rd_burst_length <= MAX_BURST_LENGTH;
		else
			ov_rd_burst_length <= ov_rd_burst_length;
	end

/*-------------------------------------------------*\
                    调试信号
\*-------------------------------------------------*/

    // 检测用户发送的地址是否会产生4k边界问题
    always @(posedge i_clk or negedge rst_n) begin
        if(~rst_n)
            o_err_4k_border <= 1'b0;
        else if(user_rd_req_d0 && (iv_user_rd_addr + iv_user_rd_length[11:0] > 13'd4096))
            o_err_4k_border <= 1'b1;  // 取低12位就减去了4096,查看相对长度会产生4k边界问题.
        else
            o_err_4k_border <= 1'b0;
    end

    // 检查用户输入的地址是否规范
    // 如果AXI数据位宽128bit,对应16字节,那输入的地址最好是16的倍数.
    // 突发传输的数据量以字节为单位,1个128bit的AXI数据是16字节;如果突发地址不是16的倍数,
    // 那意味着突发地址可能在某个128bitAXI数据中的某个字节开始,这样就将从这个数据开始的AXI数据破坏掉了.
    always @(posedge i_clk or negedge rst_n) begin
        if(~rst_n) 
            o_err_user_addr <= 1'b0;
        else if(user_rd_req_d0 && (iv_user_rd_addr[$clog2(AXI_DATA_WIDTH/8)-1:0]))
            o_err_user_addr <= 1'b1;
        else
            o_err_user_addr <= 1'b0;
    end

endmodule

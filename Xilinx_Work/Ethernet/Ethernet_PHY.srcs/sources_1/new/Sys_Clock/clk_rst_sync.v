`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2024/08/14 21:18:51
// Design Name: 
// Module Name: clk_rst_sync
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module clk_rst_sync(
	input		wire		i_clk_50M,

	output		wire		o_clk_125M,
	output		wire		o_clk_200M,
	output		wire		o_sys_rst_n
);

	wire		clk_125M;
	wire		clk_200M;
	wire		clk_locked;
	reg	[8:0]	cnt_rst_init;
	reg			reset;

	// Pll uses to generate clks whose frequency is 125MHz and 200MHz respectively.
	pll_125M_200M  U_pll_clk(
		// Clock in ports
  		.clk_in1	(i_clk_50M),
  		// Clock out ports
  		.clk_out1	(clk_125M),
  		.clk_out2	(clk_200M),
  		// Status and control signals
  		.reset		(1'b0),		// This is high valid.
  		.locked		(clk_locked)
 	);

 	always @(posedge clk_125M or negedge clk_locked) begin
 		if(~clk_locked) begin
 			cnt_rst_init <= 9'd0;
 			reset 		 <= 1'b1;
 		end 
		else if(cnt_rst_init <= 9'd500)begin
			cnt_rst_init <= cnt_rst_init + 1'b1;
			reset		 <= 1'b1;
		end
 		else begin
 			cnt_rst_init <= cnt_rst_init;
 			reset        <= 1'b0;
 		end
 	end

 	assign	o_sys_rst_n = ~reset;
 	assign	o_clk_125M  = clk_125M;
 	assign	o_clk_200M  = clk_200M;

endmodule

//-------------------------------------------------------------------------------------------------                                                                             
//                                                                                                                                                                              
//--- ALL RIGHTS RESERVED
//--- File      : DecodeTop.v
//--- Auther    : JInyuan Chen  yuan Jinyuan_whw@hotmail.com
//--- Date      : 2010.5.19
//--- Version   : v2.0
//--- Abstract  : Inner Bus Control Module of BTM
//
//-------------------------------------------------------------------------------------------------                                                                             
//                                                                                                                                                                              
//Description:  UART使用485实现, 只用来发送fsk数据, 没有用到接收
//              CAN接收查询版本请求,发送版本+状态信息 \ 发送104+128报文信息
//                                                                                                                                                                              
//-------------------------------------------------------------------------------------------------                                                                             
//--- Modification History:
//--- Date          By          Version         Change Description 
//-------------------------------------------------------------------
//--- 2010.5.19    Ying Zhou       v2.0       
//--- 2011.8.1    Dan Cui      v3.0 ????????????????27M????????????
//---                   ?????????Match?????????????????????fsk???????????????????ARM??
//---                   FSK????14M???????921600
//---                   ???????RAM???????
//---2011.8.8   Dan Cui         FSK?????????AD9224???????????
//---2011.8.15    DanCui          ?????????????????3s??400ms
//--------------------------------------------------------------------

//--------------------------------------------------------------------

module decode_top(
    i_clk_27M,          //PIN_22  27.095M
    i_rst_n,            //PIN_87  复位信号
    i_adc_otr,
    iv_adc_fsk,         //ADC采样数据

    io_one_wire,        //PIN_104   保密芯片I/O

    i_can_int,          //PIN_58    can通用中断
    // i_int_rxb0,      //PIN_51    can接收缓冲区0满中断标志
    // i_int_rxb1,      //PIN_50    can接收缓冲区1满中断标志
    o_can_rstn,         //PIN_69    can复位控制

    i_spi_so,           //PIN_64    SPI输入信号, CAN控制器
    o_spi_si,           //PIN_60    SPI输出，CS为低时有效
    o_spi_clk,          //PIN_59
    o_spi_csn,          //PIN_68

    o_led_clk,          //PIN_100   点灯时钟
    o_led_data,         //PIN_101   控制点灯信号

    // i_uart_rx,       //PIN_76    not use
    o_uart_fsk_en,      //PIN_72    UART使能信号，测试用
    o_uart_fsk,         //PIN_71    UART透传的fsk数据

    o_adc_clk27M        //PIN_144   ADC时钟
);
           

input i_clk_27M;
input i_rst_n;
input i_adc_otr;
input [11:0] iv_adc_fsk;
inout io_one_wire;

input i_spi_so;
input i_can_int;

output wire o_can_rstn;
output wire o_spi_si;
output wire o_spi_clk;
output wire o_spi_csn;

output wire o_led_clk;
output wire o_led_data;

output wire o_uart_fsk_en;
output wire o_uart_fsk;

output wire o_adc_clk27M; 

//----------------信号声明------------------//
wire clk_27M;
wire clk_54M;
wire clk_81M;
wire clk_27M_en;
wire sys_rst_n;

wire m_ready;

//加密芯片
wire  safe_enable;

//天线
wire  sign_nofsk;//天线异常标志
wire  tx_state;//天线正常

//点灯
wire  lamp_A;

//同步解调
wire [10:0]data_11_bit;
wire data_11_sck;
wire [7:0]bus_data_out8;
wire defsk_wen8;
wire data_bit_self;
wire data_sck_self;
//wire ADC_noise_flag;
wire framing_start;

//译码
wire msg_rd_en_dec;
wire msg_rd_sck_dec;
wire msg_trans_ready_dec;
wire [7:0]msg_data_dec;

wire telegram_rd_en_dec;
wire telegram_rd_sck_dec;
wire telegram_trans_ready_dec;
wire [7:0]telegram_data_dec;

wire decode_ok;

//组帧模块
wire msg_end;
wire msg_en;
wire [7:0] msg_data;

//CAN模块
// can rx
wire can_wrclk;
wire can_wren;
wire [7:0] can_wrdata;
wire can_reFrameEnd;

// can tx
wire tx_byte_en;
wire [7:0] tx_byte;
wire can_send_byte_ok;
wire can_send_frame_ok;

//---------------------------------------------------------------------------------------------------//
//加密芯片
IFF0  U_IFFO(
    .clock_in(clk_27M),
    .reset_in(sys_rst_n),
    .one_wire_b(io_one_wire),
    .enable_out(safe_enable)
);

//---------------------------------------------------------------------------------------------------//
//时钟
SynClk U_SynClk(
    .i_clk_27M(i_clk_27M),
    .i_rst_n(i_rst_n), 

    .o_clk_27M( clk_27M ),
    .o_clk_27M_en(clk_27M_en),
    .o_clk_54M( clk_54M ),
    .o_clk_81M( clk_81M ),
    .o_sys_rst_n( sys_rst_n )
);

//ADC芯片时钟
assign o_adc_clk27M = clk_27M;

//----------------------------------------------------------------------------------------------------//
//同步解调
// wire flag_noise;
demod_top U_DemodTop(
    .i_rst_n(sys_rst_n),
    .i_clk_27M_en(clk_27M_en),         //27M时钟使能
    .i_clk_81M(clk_81M),

    .i_ADC_data(iv_adc_fsk),           //ADC采样数据

    .ov_data_11bit(data_11_bit),       //解调结果11bit一组打包，给译码模块
    .o_data_11bit_sck(data_11_sck),    //11bit数据的sck同步时钟

    .ov_data_8bit(bus_data_out8),      //解调结果8bit一组打包，给fsk发送模块
    .o_data_8bit_sck(defsk_wen8),   //8bit数据的sck同步时钟

    .o_data_1bit( data_bit_self ),    //解调结果1bit一组发出，给自检模块
    .o_data_1bit_sck( data_sck_self ),//1bit数据的sck同步时钟

    // .o_flag_noise(flag_noise),
    .o_framing_start(framing_start)
);

//--------------------------------------------------------------------------------------------//
//译码模块
Decode U_Decode(
    .i_clk_81M(clk_81M),
    .i_rst_n(sys_rst_n),
    
    .iv_data_fsk(data_11_bit),          //解调后的数据，11bit一组
    .i_data_fsk_sck(data_11_sck),       //iv_data_fsk的字节同步时钟
    
    .i_rd_msg_en(msg_rd_en_dec),        //译码后，报文发送模块给出的读使能
    .i_rd_msg_sck(msg_rd_sck_dec),      //译码后，报文发送模块给出的读时钟
    .ov_msg_data(msg_data_dec),         //发送模块读出的数据，8bit一组
    .o_msg_trans_ready(msg_trans_ready_dec),
    
    .i_rd_telegram_en(telegram_rd_en_dec),
    .i_rd_telegram_sck(telegram_rd_sck_dec),
    .ov_telegram_data(telegram_data_dec),
    .o_telegram_trans_ready(telegram_trans_ready_dec),
    
    .o_new_msg_flag(),                  //o_new_msg_flag
    .o_decode_ok(decode_ok)             //查表正确
);

//---------------------------------组帧模块-----------------------------------//    
//组帧模块
MsgFrameNew U_MsgFrameNew(
    .i_clk_81M(clk_81M),
    .i_rst_n(sys_rst_n),

    .i_msg_trans_ready(msg_trans_ready_dec),//译码模块准备完成的白标志，维持一段时间的高电平
    .iv_msg_data(msg_data_dec),             //从译码模块读出的译码完成的数据，8bit一组共832bit（最后两个bit为无效0，有效数据为830）同时，还有4x8bit的crc校验码跟在尾部        
    .o_rd_msg_en(msg_rd_en_dec),            //本模块主动向译码模块读数据的使能信号  
    .o_rd_msg_sck(msg_rd_sck_dec),          //本模块主动向译码模块读数据的字节同步时钟

    .i_telegram_trans_ready(telegram_trans_ready_dec),
    .iv_telegram_data(telegram_data_dec),
    .o_rd_telegram_en(telegram_rd_en_dec),
    .o_rd_telegram_sck(telegram_rd_sck_dec),
    
    .o_tx_end(msg_end),
    .o_tx_en(msg_en),
    .o_tx_data(msg_data)
);

//------------------------------------------------------------------------------------------------------------------//
shortcheck_new U_Shortcheck_new(
    .i_clk_27M( clk_27M ),                  //--27.095M 
    .i_rst_n( sys_rst_n ),               
    .i_data_bit_defsk( data_bit_self ),     //解调得到的1bit报文
    .i_clk_defsk( data_sck_self ),          //报文使能信号

    .o_m_ready_p( m_ready )                 //自检通过
);

//------------------------------------------??FSK??????7M------------------------------------------------------//
//BtmInnerBusForFsk U_BtmInnerBusForFsk(
//    .i_clk_27M( clk_27M ),                  //--27.095MHz
//    // .clk_uart( clk_uart_p ),
//    .i_rst_n( sys_rst_n ),
//
//    .iv_defsk_data8bit(bus_data_out8),      //--defsk data input 8bits
//    .i_defsk_wen(defsk_wen8),               //--defsk data by using this clk 
//
//    .o_uart_fsk_wen(o_uart_fsk_en),         //输出使能信号
//    .o_uart_data(o_uart_fsk)                //--UART output   o_uart_fsk
//);
BtmInnerBusForFsk U_BtmInnerBusForFsk(
    .i_clk_27M        (clk_27M),        
    .i_rst_n          (sys_rst_n),          
    .i_framing_start  (framing_start),  
    .iv_defsk_data8bit(bus_data_out8),
    .i_defsk_wen      (defsk_wen8),      
    
    .o_uart_data      (o_uart_fsk),      
    .o_uart_fsk_wen   (o_uart_fsk_en)
);
//-----------------------------------------------------------------------------------------------------------//
//用来判断天线状态，如果超过5S没有收到自检或者报文则报天线异常，如果有收到自检或者报文则通过CAN报天线正常
nofsk U_Nofsk(
    .i_rst_n(sys_rst_n),
    .i_clk_27M(clk_27M),
    .i_fsk_come(m_ready),   //有自检信号到来,用m_ready_p
    .i_data_come(decode_ok),//有报文到来,用RAM存完报文的使能表示

    .o_ant_nor(tx_state),   //天线正常信号
    .o_ant_err(sign_nofsk)  //天线异常，3S钟没有收到任何信号        
);
          
//-----------------------------------------------------------------------------------------------------------//
    //CAN Interface
    wire [6:0] frame_id;
    wire [63:0] set_id;
    assign set_id[63:60] = 4'b1000;     //波特率设置   4'b1000
    assign set_id[59] = 1'b0;           //发送和接收保持一致，使用标准帧或者扩展帧(1是扩展帧, 0是标准帧)

    //发送帧ID, 也就是对方接受ID
    assign set_id[58:55] = 4'b1001;
    //assign set_id[54:48] = frame_id;     // 0x01~0x1E
    assign set_id[54:48] = 7'b000_0000;     //----------------可变
    assign set_id[47:30] = 5'h00000;    //扩展帧发送ID

    //接收ID, 也就是本地
    assign set_id[29:26] = 4'b0110;
    assign set_id[25:19] = 7'b000_0000;
    assign set_id[18:1]  = 5'h00000;    //扩展帧接收ID

    assign set_id[0]     = 1'b1;        //表示接收是否滤波, 1表示滤波, 0表示不滤波

can U_Can(
    .i_clk_27M(clk_27M),
    .i_rst_n(sys_rst_n),

    .i_so(i_spi_so),                //spi输入信号，即总线上接收到的数据
    .i_int(i_can_int),              //can中断信号，低有效（有中断表示总线在被占用）                 
    .iv_set_id(set_id),

    .o_si(o_spi_si),                //spi输出信号，即发送到总线上的数据
    .o_sclk(o_spi_clk),             //spi时钟
    .o_cs(o_spi_csn),               //spi使能(低有效)           
    .o_rst_can(o_can_rstn),         //can(mcp2515)复位信号
    .o_can_osc(),                   //o_can1_clk
    //TX
    .i_byte_ready(tx_byte_en),     //字节有效脉冲
    .iv_msg_data_in(tx_byte),      //状态帧数据输入
    //RX
    .o_databyte_en(can_wren),
    .ov_recv_data(can_wrdata),      //spi输出转并行数据
    .o_databyte_clk(can_wrclk),     //接收数据变化标志，每一条接收到的can标准帧在8位数据接收过程中对应给出8个o_databyte_clk信号
    .o_receive_ok_can(can_reFrameEnd),
    //TX
    .o_sendbyte_ok(can_send_byte_ok),   //给状态帧数据输出模块的地址变化信号，地址加一则发送下一字节的状态帧数据（对应iv_msg_data_in）
    .o_send_frame_ok(can_send_frame_ok)
);

//处理can接收数据和控制发送数据
mcp_top  u_mcp_top (
    .i_clk                   ( clk_27M              ),
    .i_clk_81M               ( clk_81M              ),
    .i_rst_n                 ( sys_rst_n            ),
    .i_ant_err               ( sign_nofsk           ),
    .i_ant_nor               ( tx_state             ),
    .i_safe_enable           ( safe_enable          ),
    .i_msg_end               ( msg_end              ),
    .i_msg_en                (  1'b0                ),        // msg_en      // 关闭发报文,只发送状态信息
    .iv_msg_data             ( msg_data             ),
    .i_can_wren              ( can_wren             ),
    .i_can_wrclk             ( can_wrclk            ),
    .i_can_reFrameEnd        ( can_reFrameEnd       ),
    .iv_can_wrdata           ( can_wrdata           ),
    .i_tx_byte_ok            ( can_send_byte_ok     ),
    .i_frame_ok              ( can_send_frame_ok    ),

    .ov_frame_id             ( frame_id             ),
    .o_tx_byte_en            ( tx_byte_en           ),
    .ov_tx_byte              ( tx_byte              )
);

//-----------------------------------------------------------------------------------------------------------//
//接收报文：当同步上且不是噪声时用8bit报文使能点亮a灯
    // assign lamp_A = (~ADC_noise_flag) & framing_start & defsk_wen8; //A排灯控制使能20171106
    assign lamp_A = framing_start & defsk_wen8 ;//& (~flag_noise);
  
light U_light(
    .i_rst_n(sys_rst_n),
    .i_clk_27M(clk_27M),

    /* 修改了点灯模块 mark2016.10.09
    .lamp1_1(defsk_wen8),//有FSK信号
    .lamp1_2(decode_ok),//译出报文
    .lamp2_1(m_ready),//译出自检
    .lamp2_2(),//预留
    .lamp3_1(can_send_ok),//CAN发送
    .lamp3_2(can_receive_ok),//CAN接收
    .lamp4_1(sign_nofsk),//天线异常
    .lamp4_2(sign_nofsk),//天线异常
    */

    /*mark 2017.02.28
    .lamp1_1(defsk_wen8),//有FSK信号
    .lamp1_2(defsk_wen8),//有FSK信号
    */
    .i_m_ready(m_ready),
    //            .lamp1_1( framing_start & defsk_wen8 ),//有FSK信号20171106
    //            .lamp1_2( framing_start & defsk_wen8 ),//有FSK信号
    .lamp1_1(lamp_A),
    .lamp1_2(lamp_A),
    .lamp2_1(decode_ok), //译出报文
    .lamp2_2(),//预留
    .lamp3_1(m_ready),   //译出自检
    .lamp3_2(m_ready),   //译出自检
    .lamp4_1(sign_nofsk),//天线异常
    .lamp4_2(sign_nofsk),//天线异常

    .o_led_clk(o_led_clk),
    .o_led_data(o_led_data)
);

endmodule

module DAROM_bit1(
    input   wire        clk,
    output  wire [11:0] sin_rom_bit1
);
    
    parameter   COUNTER_LOAD_12k_INIT = 9'd320;

    wire        cout_sig;
    wire [8:0]  addr;

    //调用计数控制IP
    //该计数器装载初值，会实现向下计数
    counter_ctrl U_Counetr_ctrl(
	    .clock  (clk),
	    .data   (COUNTER_LOAD_12k_INIT - 1),
	    .sload  (cout_sig),

	    .cout   (cout_sig),
	    .q      (addr)
    );

    //调用ROM，根据地址查表输出正弦信号
    DAROM_12_5k U_DAROM_12k(
	    .address(addr),
	    .clock  (clk),

	    .q      (sin_rom_bit1)
    );



endmodule
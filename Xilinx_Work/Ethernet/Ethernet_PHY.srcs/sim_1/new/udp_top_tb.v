`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date: 2024/08/23 15:33:24
// Module Name: udp_top_tb
// Tool Versions: 
// Description: 对UDP环进行仿真.
//////////////////////////////////////////////////////////////////////////////////

module udp_top_tb();

	// 参数定义
	// 由于做回环实验,所以源与目的的地址、端口号都是一样的.
	parameter   LOCAL_MAC_ADDR  = {8'h0,8'h1,8'h2,8'h3,8'h4,8'h5};
	parameter   TARGET_MAC_ADDR = {8'h0,8'h1,8'h2,8'h3,8'h4,8'h5};
	parameter   LOCAL_IP_ADDR   = {8'hAA,8'hBB,8'hCC,8'hDD};
	parameter   TARGET_IP_ADDR  = {8'hAA,8'hBB,8'hCC,8'hDD};
	parameter   LOCAL_PORT      = 16'h8060;
	parameter   TARGET_PORT     = 16'h8060;

	parameter 	USER_CLK_PERIOD = 20;
	parameter 	PHY_CLK_PERIOD  = 8;

/*-----------------------------------------------*\
				激励、模块输出
\*-----------------------------------------------*/	
	
	reg				i_phy_rx_clk;
	reg				i_phy_tx_clk;
	reg				i_rst_n;

	wire			i_gmii_rx_data_vld;
	wire	[7:0]	iv_gmii_rx_data;

	wire			o_gmii_tx_data_vld;
	wire	[7:0]	ov_gmii_tx_data;

	wire			o_app_rx_data_vld;
	wire			o_app_rx_data_last;
	wire	[7:0]	ov_app_rx_data;
	wire	[15:0]	ov_app_rx_length;

	reg				i_app_tx_data_vld;
	reg				i_app_tx_data_last;
	reg	[7:0]		iv_app_tx_data;
	reg	[15:0]		iv_app_tx_length;

	wire			o_app_tx_ready;


/*-----------------------------------------------*\
					时钟激励产生
\*-----------------------------------------------*/	

	reg		phy_clk_125M;			// PHY时钟,125MHz
	reg		user_clk;				// 用户时钟

	// 时钟初始化
	initial begin
		phy_clk_125M = 1'b0;
		user_clk     = 1'b0;
	end

	// 复位
	initial begin
		i_rst_n <= 1'b0;
		#(1000)
		i_rst_n <= 1'b1;
	end

	always #(PHY_CLK_PERIOD/2)  phy_clk_125M = ~phy_clk_125M;      	// PHY时钟    
	always #(USER_CLK_PERIOD/2) user_clk     = ~user_clk;      		// 用户端时钟


/*-----------------------------------------------*\
					搭建回环
\*-----------------------------------------------*/	

	assign  i_gmii_rx_data_vld = o_gmii_tx_data_vld;
	assign  iv_gmii_rx_data    = ov_gmii_tx_data;

/*-----------------------------------------------*\
					启动任务
\*-----------------------------------------------*/	

	initial begin
		#(20000)
		udp_loop_test('d18);

		// udp_loop_test('d1024);
	end

/*-----------------------------------------------*\
				定义任务产生激励
\*-----------------------------------------------*/	

	task udp_loop_test(
		input	[15:0]	length
	);
		begin
			#3000
			wait(o_app_tx_ready);		// 等待数据帧头部间隔足够的状态下才能发送
			i_app_tx_data_vld  <= 1'b0;
			i_app_tx_data_last <= 1'b0;
			iv_app_tx_length   <= length;
			// repeat(N)的重复执行次数实际为(N-1)次.
			repeat(length) @(posedge user_clk)begin 
				i_app_tx_data_vld <= 1'b1;
			end
			i_app_tx_data_vld  <= 1'b1;
			i_app_tx_data_last <= 1'b1;
			#(USER_CLK_PERIOD)
			i_app_tx_data_vld  <= 1'b0;
			i_app_tx_data_last <= 1'b0;
		end
	endtask


	always @(posedge user_clk or negedge i_rst_n) begin
		if(~i_rst_n)
			iv_app_tx_data <= 8'h0;
		else if(i_app_tx_data_vld)
			iv_app_tx_data <= iv_app_tx_data + 1'b1;
		else
			iv_app_tx_data <= iv_app_tx_data;
	end


/*-----------------------------------------------*\
					顶层例化
\*-----------------------------------------------*/	
	udp_protocal_stack #(
		.LOCAL_MAC_ADDR  (LOCAL_MAC_ADDR),
		.TARGET_MAC_ADDR (TARGET_MAC_ADDR),
		.LOCAL_IP_ADDR   (LOCAL_IP_ADDR),
		.TARGET_IP_ADDR  (TARGET_IP_ADDR),
		.LOCAL_PORT      (LOCAL_PORT),
		.TARGET_PORT     (TARGET_PORT)
	) U_udp_protocal_stack (
		// rx和tx使用相同的时钟
		.i_phy_rx_clk       (phy_clk_125M),
		.i_phy_tx_clk       (phy_clk_125M),

		.i_rst_n            (i_rst_n),

		// 将GMII接口的输入输出连在一起做回环,就要求两者要在相同的时钟下.
		.i_gmii_rx_data_vld (i_gmii_rx_data_vld),
		.iv_gmii_rx_data    (iv_gmii_rx_data),

		.o_gmii_tx_data_vld (o_gmii_tx_data_vld),
		.ov_gmii_tx_data    (ov_gmii_tx_data),

		.i_app_rx_clk       (user_clk),

		.o_app_rx_data_vld  (o_app_rx_data_vld),
		.o_app_rx_data_last (o_app_rx_data_last),
		.ov_app_rx_data     (ov_app_rx_data),
		.ov_app_rx_length   (ov_app_rx_length),

		.i_app_tx_clk       (user_clk),

		.i_app_tx_data_vld  (i_app_tx_data_vld),
		.i_app_tx_data_last (i_app_tx_data_last),
		.iv_app_tx_data     (iv_app_tx_data),
		.iv_app_tx_length   (iv_app_tx_length),

		.o_app_tx_ready     (o_app_tx_ready)
	);

endmodule

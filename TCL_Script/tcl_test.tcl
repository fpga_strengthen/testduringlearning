# --------------------------create a project-------------------------------//
proc create_project {}{
    global  project_name;               # the name of project built
    global  work_root;                  # 进入工作目录的根目录下
    cd ${work_root}
        if{project exists $project_name}{
            project_open $project_name;         # 打开工程
        }else {
            project_new $project_name;          # 新建工程
        }
}

# 设定当前的目标器件
set_global_assignment   -name   FAMILY  "Cyclone IV"
set_global_assignment   -name   DEVICE  EP4CE10FI7C8N
set_global_assignment   -name   TOP_LEVEL_ENTITY    <name of top_level module>

# 设置文件库和搜索路径
set_global_assignment   -name   SEARCH_PATH     ../../lib

# 添加文件,最后一个参数是文件名
set_global_assignment   -name   VERILOG_FILE    COREUART.v

# 设置I/O引脚定义
set_global_assignment   PIN_A11 -to clk_in
set_global_assignment   PIN_N21 -to RESET_N

# 其他约束文件以及在线调试文件
set_global_assignment   -name   ENABLE_SIGNALTAP    ON
set_global_assignment   -name   SIGNALTAP_FILE      stp2.stp
set_global_assignment   -name   SDC_FILE            smbus.sdc

# ------------------------run Synthsis commands------------------------//

# 于Quartus而言，设定好工程后，所有实现命令可以浓缩为如下命令
execute_flow -compile

# 综合后的时序结果可以如下TCL函数完成，执行的命令为get_fmax_from_report
proc    get_fmax_from_report{}{
    global      project_name;
    load_report $project_name;

    set fmax_panel_name "Timing Analyzer Summary";
    for each panel_name [get_report_panel_names]{
        if{[string match "* $fmax_panel_name*" "$panel_name"]}{
            set fmax_row [get_report_panel_row "$panel_name" -row1];
        }
    }
    set actual_fmax [lindex $fmax_row3];
    unload_report $project_name;
    return  $actual_fmax;
}

# 如果时序不满足，可以执行 ::quartus::timing 时序验证包命令，获得完整的时序报告，具体包括如下命令
# 估算仿真时间并且报告时序验证，只能在quartus_tan中执行
create_timing_netlist
report_timing
delete_timing_netlist
//-------------------------------------------------------------------------------------------------                                                                             
//                                                                                                                                                                              
//--- ALL RIGHTS RESERVED
//--- File      : Slope_calculation.v 
//--- Auther    : 
//--- Date      : 
//--- Version   : v1.3.2.2
//--- Abstract  : 求相位折线的斜率
//
//-------------------------------------------------------------------------------------------------                                                                             
//                                                                                                                                                                              
// Description : 对相位折线进行曲线拟合，求得斜率
//                                                                                                                                                            
//                                                                                                                                                                              
//-------------------------------------------------------------------------------------------------                                                                             
//--- Modification History:
//--- Date          By          Version         Change Description 
//-------------------------------------------------------------------
//--- 2017.06.20              修改斜率最小二乘法计算方式，改为直接移位寄存           
//--------------------------------------------------------------------

module Slope_calculation(
          i_rst_n,         //输入复位信号，低电平有效
          i_clk_81M,       //输入时钟信号，81M
          i_clk_27M_en,    //输入时钟使能，27M
          i_angle_tan,     //输入原始相位，修正后进行最小二乘法求斜率

          o_slope          //斜率
          );

input i_rst_n;
input i_clk_81M;
input i_clk_27M_en;
input [13:0]i_angle_tan;

output [25:0]o_slope;

/////////////////////////////////////////////////////////////////////////////////////////////////////////
//-------------------------------信号声明-------------------------
//---------相位补偿---------
reg [13:0]angle_tan;                  //将输入数据打一拍（0-8192）
reg [13:0]angle_tan_delay;            //打一拍用于进行2pi修正（0-8192）

reg signed[16:0]angle_sum;            //相位修正量 +—4*8192 实际除溢出点外，在+—2*8192之间

//---------相位修正并延时---------
reg signed[16:0]angle_fit;            //修正后的相位 —2*8192~+3*8192
reg signed[16:0]angle_fit_delay_14  ;  //对相位进行延时29点，用于之后最小二乘法
reg signed[16:0]angle_fit_delay_13  ;
reg signed[16:0]angle_fit_delay_12  ;
reg signed[16:0]angle_fit_delay_11  ;
reg signed[16:0]angle_fit_delay_10  ;
reg signed[16:0]angle_fit_delay_9   ;
reg signed[16:0]angle_fit_delay_8   ;
reg signed[16:0]angle_fit_delay_7   ;
reg signed[16:0]angle_fit_delay_6   ;
reg signed[16:0]angle_fit_delay_5   ;
reg signed[16:0]angle_fit_delay_4   ;
reg signed[16:0]angle_fit_delay_3   ;
reg signed[16:0]angle_fit_delay_2   ;
reg signed[16:0]angle_fit_delay_1   ;
reg signed[16:0]angle_fit_delay_0   ;
reg signed[16:0]angle_fit_delay_1_n ;
reg signed[16:0]angle_fit_delay_2_n ;
reg signed[16:0]angle_fit_delay_3_n ;
reg signed[16:0]angle_fit_delay_4_n ;
reg signed[16:0]angle_fit_delay_5_n ;
reg signed[16:0]angle_fit_delay_6_n ;
reg signed[16:0]angle_fit_delay_7_n ;
reg signed[16:0]angle_fit_delay_8_n ;
reg signed[16:0]angle_fit_delay_9_n ;
reg signed[16:0]angle_fit_delay_10_n;
reg signed[16:0]angle_fit_delay_11_n;
reg signed[16:0]angle_fit_delay_12_n;
reg signed[16:0]angle_fit_delay_13_n;
reg signed[16:0]angle_fit_delay_14_n;

//---------最小二乘法求斜率---------
//对应位相减
reg signed[17:0]num_14_reduce_14n; 
reg signed[17:0]num_13_reduce_13n;
reg signed[17:0]num_12_reduce_12n;
reg signed[17:0]num_11_reduce_11n;
reg signed[17:0]num_10_reduce_10n;
reg signed[17:0]num_9_reduce_9n  ;
reg signed[17:0]num_8_reduce_8n  ;
reg signed[17:0]num_7_reduce_7n  ;
reg signed[17:0]num_6_reduce_6n  ;
reg signed[17:0]num_5_reduce_5n  ;
reg signed[17:0]num_4_reduce_4n  ;
reg signed[17:0]num_3_reduce_3n  ;
reg signed[17:0]num_2_reduce_2n  ;
reg signed[17:0]num_1_reduce_1n  ;

//乘对应权值
reg signed[21:0]num_14_reduce_14n_x14;
reg signed[21:0]num_13_reduce_13n_x13;
reg signed[21:0]num_12_reduce_12n_x12;
reg signed[21:0]num_11_reduce_11n_x11;
reg signed[21:0]num_10_reduce_10n_x10;
reg signed[21:0]num_9_reduce_9n_x9   ;  
reg signed[21:0]num_8_reduce_8n_x8   ;  
reg signed[21:0]num_7_reduce_7n_x7   ;  
reg signed[21:0]num_6_reduce_6n_x6   ;  
reg signed[21:0]num_5_reduce_5n_x5   ;  
reg signed[21:0]num_4_reduce_4n_x4   ;  
reg signed[21:0]num_3_reduce_3n_x3   ;  
reg signed[21:0]num_2_reduce_2n_x2   ;  
reg signed[21:0]num_1_reduce_1n_x1   ;  

//将各值相加
reg signed[22:0]num_14_add_13;
reg signed[22:0]num_12_add_11;
reg signed[22:0]num_10_add_9;
reg signed[22:0]num_8_add_7;
reg signed[22:0]num_6_add_5;
reg signed[22:0]num_4_add_3;
reg signed[22:0]num_2_add_1;

reg signed[23:0]num_14_add_to_11;
reg signed[23:0]num_10_add_to_7 ;
reg signed[23:0]num_6_add_to_3  ;
reg signed[23:0]num_2_add_to_1  ;

reg signed[25:0]slope;                //斜率结果 

wire signed[25:0]o_slope;
 
//---------------------------------------------------------------------------------------------
//对输入数据相位打一拍
always @ ( negedge i_rst_n or posedge i_clk_81M )
  begin
    if( !i_rst_n )
      angle_tan <= 14'd0;
    else if( i_clk_27M_en ) //27M
      angle_tan <= i_angle_tan;
    else
      angle_tan <= angle_tan;
  end
//对相位延时一拍，为后面做2pi修正做准备
always @ ( negedge i_rst_n or posedge i_clk_81M )
  begin
    if( !i_rst_n )
      angle_tan_delay <= 14'd0;
    else if( i_clk_27M_en ) //27M
      angle_tan_delay <= angle_tan;
    else
      angle_tan_delay <= angle_tan_delay;
  end
    
/*--------将求得的相位进行中心频率的处理，即减去4.234M，3.951与4.516减去4.234M得到正负282.28K
的两个信号，其相位曲线是一个对称的值，通过求出曲线的拐点，可以求出01bit的切换点，从而求得同步点---*/
//修正累积量
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if( !i_rst_n )
      angle_sum <= 17'd0;
    else if( i_clk_27M_en ) //27M
      begin
        if ( (angle_sum > 17'd16384) &&(angle_sum[16]==1'b0) )//相位修正累积量超过+2*8192 上溢处理 恢复初值
          begin
            if ( angle_tan + 14'd4096 <= angle_tan_delay )//对2pi进行修正
              angle_sum <= 17'd6912;//+8192-1280
            else
              angle_sum <= 17'd0 - 17'd1280;// -1280;
          end     
        else if( (angle_sum  < 17'd114688) &&(angle_sum[16]==1'b1) )//相位修正累积量超过-2*8192 下溢处理 恢复初值
          begin
            if ( angle_tan + 14'd4096 <= angle_tan_delay )//对2pi进行修正
              angle_sum <= 17'd6912;//+8192-1280
            else
              angle_sum <= 17'd0 - 17'd1280;//-1280;
          end
        else //相位修正累积量在+-2*8192之间时
          begin
            if ( angle_tan + 14'd4096 <= angle_tan_delay )//对2pi进行修正
              angle_sum <= angle_sum + 17'd6912;//+8192-1280
            else
              angle_sum <= angle_sum - 17'd1280;// - 1280;
          end        
      end//i_clk_27M_en   
    else
      angle_sum <= angle_sum;
  end
  
  
/*对求得的相位折线进行曲线拟合，求其斜率（即减去中心频率之后的频率值，分别对应正负282K），
根据仿真，曲线拟合选取29点最小二乘法，具体最小二乘法的推导参考详细的文档说明*/
//将修正量叠加到初始相位值上，得到修正后的相位折线
always @ ( negedge i_rst_n or posedge i_clk_81M )
  begin
    if ( !i_rst_n )
        begin
          angle_fit            <=  17'd0;
          angle_fit_delay_14   <=  17'd0;
          angle_fit_delay_13   <=  17'd0;
          angle_fit_delay_12   <=  17'd0;
          angle_fit_delay_11   <=  17'd0;
          angle_fit_delay_10   <=  17'd0;
          angle_fit_delay_9    <=  17'd0;
          angle_fit_delay_8    <=  17'd0;
          angle_fit_delay_7    <=  17'd0;
          angle_fit_delay_6    <=  17'd0;
          angle_fit_delay_5    <=  17'd0;
          angle_fit_delay_4    <=  17'd0;
          angle_fit_delay_3    <=  17'd0;
          angle_fit_delay_2    <=  17'd0;
          angle_fit_delay_1    <=  17'd0;
          angle_fit_delay_0    <=  17'd0;
          angle_fit_delay_1_n  <=  17'd0;
          angle_fit_delay_2_n  <=  17'd0;
          angle_fit_delay_3_n  <=  17'd0;
          angle_fit_delay_4_n  <=  17'd0;
          angle_fit_delay_5_n  <=  17'd0;
          angle_fit_delay_6_n  <=  17'd0;
          angle_fit_delay_7_n  <=  17'd0;
          angle_fit_delay_8_n  <=  17'd0;
          angle_fit_delay_9_n  <=  17'd0;
          angle_fit_delay_10_n <=  17'd0;
          angle_fit_delay_11_n <=  17'd0;
          angle_fit_delay_12_n <=  17'd0;
          angle_fit_delay_13_n <=  17'd0;
          angle_fit_delay_14_n <=  17'd0;
        end //rst_n
    else if(i_clk_27M_en)//27M 
      begin
        if ( ((angle_sum>17'd16384)&&(angle_sum[16]==1'b0)) || ((angle_sum<17'd114688)&&(angle_sum[16]==1'b1)) ) //溢出时
          begin
            angle_fit            <= {3'b0,angle_tan_delay};
            angle_fit_delay_14   <= angle_fit            - angle_sum;
            angle_fit_delay_13   <= angle_fit_delay_14   - angle_sum;
            angle_fit_delay_12   <= angle_fit_delay_13   - angle_sum;
            angle_fit_delay_11   <= angle_fit_delay_12   - angle_sum;
            angle_fit_delay_10   <= angle_fit_delay_11   - angle_sum;
            angle_fit_delay_9    <= angle_fit_delay_10   - angle_sum;
            angle_fit_delay_8    <= angle_fit_delay_9    - angle_sum;
            angle_fit_delay_7    <= angle_fit_delay_8    - angle_sum;
            angle_fit_delay_6    <= angle_fit_delay_7    - angle_sum;
            angle_fit_delay_5    <= angle_fit_delay_6    - angle_sum;
            angle_fit_delay_4    <= angle_fit_delay_5    - angle_sum;
            angle_fit_delay_3    <= angle_fit_delay_4    - angle_sum;
            angle_fit_delay_2    <= angle_fit_delay_3    - angle_sum;
            angle_fit_delay_1    <= angle_fit_delay_2    - angle_sum;
            angle_fit_delay_0    <= angle_fit_delay_1    - angle_sum;
            angle_fit_delay_1_n  <= angle_fit_delay_0    - angle_sum;
            angle_fit_delay_2_n  <= angle_fit_delay_1_n  - angle_sum;
            angle_fit_delay_3_n  <= angle_fit_delay_2_n  - angle_sum;
            angle_fit_delay_4_n  <= angle_fit_delay_3_n  - angle_sum;
            angle_fit_delay_5_n  <= angle_fit_delay_4_n  - angle_sum;
            angle_fit_delay_6_n  <= angle_fit_delay_5_n  - angle_sum;
            angle_fit_delay_7_n  <= angle_fit_delay_6_n  - angle_sum;
            angle_fit_delay_8_n  <= angle_fit_delay_7_n  - angle_sum;
            angle_fit_delay_9_n  <= angle_fit_delay_8_n  - angle_sum;
            angle_fit_delay_10_n <= angle_fit_delay_9_n  - angle_sum;
            angle_fit_delay_11_n <= angle_fit_delay_10_n - angle_sum;
            angle_fit_delay_12_n <= angle_fit_delay_11_n - angle_sum;
            angle_fit_delay_13_n <= angle_fit_delay_12_n - angle_sum;
            angle_fit_delay_14_n <= angle_fit_delay_13_n - angle_sum;
          end
        else 
          begin
              angle_fit            <= {3'b0,angle_tan_delay} + angle_sum;
              angle_fit_delay_14   <= angle_fit           ;
              angle_fit_delay_13   <= angle_fit_delay_14  ;
              angle_fit_delay_12   <= angle_fit_delay_13  ;
              angle_fit_delay_11   <= angle_fit_delay_12  ;
              angle_fit_delay_10   <= angle_fit_delay_11  ;
              angle_fit_delay_9    <= angle_fit_delay_10  ;
              angle_fit_delay_8    <= angle_fit_delay_9   ;
              angle_fit_delay_7    <= angle_fit_delay_8   ;
              angle_fit_delay_6    <= angle_fit_delay_7   ;
              angle_fit_delay_5    <= angle_fit_delay_6   ;
              angle_fit_delay_4    <= angle_fit_delay_5   ;
              angle_fit_delay_3    <= angle_fit_delay_4   ;
              angle_fit_delay_2    <= angle_fit_delay_3   ;
              angle_fit_delay_1    <= angle_fit_delay_2   ;
              angle_fit_delay_0    <= angle_fit_delay_1   ;      
              angle_fit_delay_1_n  <= angle_fit_delay_0   ;
              angle_fit_delay_2_n  <= angle_fit_delay_1_n ;
              angle_fit_delay_3_n  <= angle_fit_delay_2_n ;
              angle_fit_delay_4_n  <= angle_fit_delay_3_n ;
              angle_fit_delay_5_n  <= angle_fit_delay_4_n ;
              angle_fit_delay_6_n  <= angle_fit_delay_5_n ;
              angle_fit_delay_7_n  <= angle_fit_delay_6_n ;
              angle_fit_delay_8_n  <= angle_fit_delay_7_n ;
              angle_fit_delay_9_n  <= angle_fit_delay_8_n ;
              angle_fit_delay_10_n <= angle_fit_delay_9_n ;
              angle_fit_delay_11_n <= angle_fit_delay_10_n;
              angle_fit_delay_12_n <= angle_fit_delay_11_n ;
              angle_fit_delay_13_n <= angle_fit_delay_12_n ;
              angle_fit_delay_14_n <= angle_fit_delay_13_n ;
          end
      end  //i_clk_27M_en
    else
      begin
        angle_fit            <= angle_fit ; 
        angle_fit_delay_14   <= angle_fit_delay_14  ; 
        angle_fit_delay_13   <= angle_fit_delay_13  ; 
        angle_fit_delay_12   <= angle_fit_delay_12  ; 
        angle_fit_delay_11   <= angle_fit_delay_11  ; 
        angle_fit_delay_10   <= angle_fit_delay_10  ; 
        angle_fit_delay_9    <= angle_fit_delay_9   ; 
        angle_fit_delay_8    <= angle_fit_delay_8   ; 
        angle_fit_delay_7    <= angle_fit_delay_7   ; 
        angle_fit_delay_6    <= angle_fit_delay_6   ; 
        angle_fit_delay_5    <= angle_fit_delay_5   ; 
        angle_fit_delay_4    <= angle_fit_delay_4   ; 
        angle_fit_delay_3    <= angle_fit_delay_3   ; 
        angle_fit_delay_2    <= angle_fit_delay_2   ; 
        angle_fit_delay_1    <= angle_fit_delay_1   ; 
        angle_fit_delay_0    <= angle_fit_delay_0   ; 
        angle_fit_delay_1_n  <= angle_fit_delay_1_n ; 
        angle_fit_delay_2_n  <= angle_fit_delay_2_n ; 
        angle_fit_delay_3_n  <= angle_fit_delay_3_n ; 
        angle_fit_delay_4_n  <= angle_fit_delay_4_n ; 
        angle_fit_delay_5_n  <= angle_fit_delay_5_n ; 
        angle_fit_delay_6_n  <= angle_fit_delay_6_n ; 
        angle_fit_delay_7_n  <= angle_fit_delay_7_n ; 
        angle_fit_delay_8_n  <= angle_fit_delay_8_n ; 
        angle_fit_delay_9_n  <= angle_fit_delay_9_n ; 
        angle_fit_delay_10_n <= angle_fit_delay_10_n; 
        angle_fit_delay_11_n <= angle_fit_delay_11_n; 
        angle_fit_delay_12_n <= angle_fit_delay_12_n; 
        angle_fit_delay_13_n <= angle_fit_delay_13_n; 
        angle_fit_delay_14_n <= angle_fit_delay_14_n; 
      end 
  end//always 
      
//--------按照最小二乘法的公式，计算斜率 具体查看相关文档-----------
//对应位相减
always @ ( negedge i_rst_n or posedge i_clk_81M )
  begin
    if ( !i_rst_n )
      begin
          num_14_reduce_14n <= 18'd0;
          num_13_reduce_13n <= 18'd0;
          num_12_reduce_12n <= 18'd0;
          num_11_reduce_11n <= 18'd0;
          num_10_reduce_10n <= 18'd0;
          num_9_reduce_9n   <= 18'd0;
          num_8_reduce_8n   <= 18'd0;
          num_7_reduce_7n   <= 18'd0;
          num_6_reduce_6n   <= 18'd0;
          num_5_reduce_5n   <= 18'd0;
          num_4_reduce_4n   <= 18'd0;
          num_3_reduce_3n   <= 18'd0;
          num_2_reduce_2n   <= 18'd0;
          num_1_reduce_1n   <= 18'd0;
      end
    else if(i_clk_27M_en)//27M   
      begin
          num_14_reduce_14n <= angle_fit_delay_14 - angle_fit_delay_14_n;
          num_13_reduce_13n <= angle_fit_delay_13 - angle_fit_delay_13_n;
          num_12_reduce_12n <= angle_fit_delay_12 - angle_fit_delay_12_n;
          num_11_reduce_11n <= angle_fit_delay_11 - angle_fit_delay_11_n;
          num_10_reduce_10n <= angle_fit_delay_10 - angle_fit_delay_10_n;
          num_9_reduce_9n   <= angle_fit_delay_9  - angle_fit_delay_9_n ;
          num_8_reduce_8n   <= angle_fit_delay_8  - angle_fit_delay_8_n ;
          num_7_reduce_7n   <= angle_fit_delay_7  - angle_fit_delay_7_n ;
          num_6_reduce_6n   <= angle_fit_delay_6  - angle_fit_delay_6_n ;
          num_5_reduce_5n   <= angle_fit_delay_5  - angle_fit_delay_5_n ;
          num_4_reduce_4n   <= angle_fit_delay_4  - angle_fit_delay_4_n ;
          num_3_reduce_3n   <= angle_fit_delay_3  - angle_fit_delay_3_n ;
          num_2_reduce_2n   <= angle_fit_delay_2  - angle_fit_delay_2_n ;
          num_1_reduce_1n   <= angle_fit_delay_1  - angle_fit_delay_1_n ;
      end
    else
      begin
        num_14_reduce_14n <= num_14_reduce_14n;
        num_13_reduce_13n <= num_13_reduce_13n;
        num_12_reduce_12n <= num_12_reduce_12n;
        num_11_reduce_11n <= num_11_reduce_11n;
        num_10_reduce_10n <= num_10_reduce_10n;
        num_9_reduce_9n   <= num_9_reduce_9n  ;
        num_8_reduce_8n   <= num_8_reduce_8n  ;
        num_7_reduce_7n   <= num_7_reduce_7n  ;
        num_6_reduce_6n   <= num_6_reduce_6n  ;
        num_5_reduce_5n   <= num_5_reduce_5n  ;
        num_4_reduce_4n   <= num_4_reduce_4n  ;
        num_3_reduce_3n   <= num_3_reduce_3n  ;
        num_2_reduce_2n   <= num_2_reduce_2n  ;
        num_1_reduce_1n   <= num_1_reduce_1n  ;
      end
  end//always

//乘以对应权值
//num_14_reduce_14n*14
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      num_14_reduce_14n_x14 <= 22'd0;    
    else if( i_clk_27M_en ) //27M 
      num_14_reduce_14n_x14 <= {num_14_reduce_14n,4'b0} - {{3{num_14_reduce_14n[17]}},num_14_reduce_14n,1'b0};
    else
      num_14_reduce_14n_x14 <= num_14_reduce_14n_x14;
  end
  
//num_13_reduce_13n*13
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      num_13_reduce_13n_x13 <= 22'd0;    
    else if( i_clk_27M_en ) //27M 
      num_13_reduce_13n_x13 <= {{num_13_reduce_13n[17]},num_13_reduce_13n,3'b0} + {{2{num_13_reduce_13n[17]}},num_13_reduce_13n,2'b0} + {{4{num_13_reduce_13n[17]}},num_13_reduce_13n};
    else
      num_13_reduce_13n_x13 <= num_13_reduce_13n_x13;
  end
  
//num_12_reduce_12n*12
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      num_12_reduce_12n_x12 <= 22'd0;    
    else if( i_clk_27M_en ) //27M 
      num_12_reduce_12n_x12 <= {{num_12_reduce_12n[17]},num_12_reduce_12n,3'b0} + {{2{num_12_reduce_12n[17]}},num_12_reduce_12n,2'b0};
    else
      num_12_reduce_12n_x12 <= num_12_reduce_12n_x12;
  end
  
//num_11_reduce_11n*11
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      num_11_reduce_11n_x11 <= 22'd0;    
    else if( i_clk_27M_en ) //27M 
      num_11_reduce_11n_x11 <= {{num_11_reduce_11n[17]},num_11_reduce_11n,3'b0} + {{3{num_11_reduce_11n[17]}},num_11_reduce_11n,1'b0} + {{4{num_11_reduce_11n[17]}},num_11_reduce_11n};
    else
      num_11_reduce_11n_x11 <= num_11_reduce_11n_x11;
  end
  
//num_10_reduce_10n*10
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      num_10_reduce_10n_x10 <= 22'd0;    
    else if( i_clk_27M_en ) //27M 
      num_10_reduce_10n_x10 <= {{num_10_reduce_10n[17]},num_10_reduce_10n,3'b0} + {{3{num_10_reduce_10n[17]}},num_10_reduce_10n,1'b0};
    else
      num_10_reduce_10n_x10 <= num_10_reduce_10n_x10;
  end
    
//num_9_reduce_9n*9
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      num_9_reduce_9n_x9 <= 22'd0;    
    else if( i_clk_27M_en ) //27M 
      num_9_reduce_9n_x9 <= {{num_9_reduce_9n[17]},num_9_reduce_9n,3'b0} + {{4{num_9_reduce_9n[17]}},num_9_reduce_9n};
    else
      num_9_reduce_9n_x9 <= num_9_reduce_9n_x9;
  end
  
//num_8_reduce_8n*8
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      num_8_reduce_8n_x8 <= 22'd0;    
    else if( i_clk_27M_en ) //27M 
      num_8_reduce_8n_x8 <= {{num_8_reduce_8n[17]},num_8_reduce_8n,3'b0};
    else
      num_8_reduce_8n_x8 <= num_8_reduce_8n_x8;
  end
  
//num_7_reduce_7n*7
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      num_7_reduce_7n_x7 <= 22'd0;    
    else if( i_clk_27M_en ) //27M 
      num_7_reduce_7n_x7 <= {{num_7_reduce_7n[17]},num_7_reduce_7n,3'b0} - {{4{num_7_reduce_7n[17]}},num_7_reduce_7n};
    else
      num_7_reduce_7n_x7 <= num_7_reduce_7n_x7;
  end
  
//num_6_reduce_6n*6
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      num_6_reduce_6n_x6 <= 22'd0;    
    else if( i_clk_27M_en ) //27M 
      num_6_reduce_6n_x6 <= {{2{num_6_reduce_6n[17]}},num_6_reduce_6n,2'b0} + {{3{num_6_reduce_6n[17]}},num_6_reduce_6n,1'b0};
    else
      num_6_reduce_6n_x6 <= num_6_reduce_6n_x6;
  end
  
//num_5_reduce_5n*5
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      num_5_reduce_5n_x5 <= 22'd0;    
    else if( i_clk_27M_en ) //27M 
      num_5_reduce_5n_x5 <= {{2{num_5_reduce_5n[17]}},num_5_reduce_5n,2'b0} + {{4{num_5_reduce_5n[17]}},num_5_reduce_5n};
    else
      num_5_reduce_5n_x5 <= num_5_reduce_5n_x5;
  end
  
//num_4_reduce_4n*4
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      num_4_reduce_4n_x4 <= 22'd0;    
    else if( i_clk_27M_en ) //27M 
      num_4_reduce_4n_x4 <= {{2{num_4_reduce_4n[17]}},num_4_reduce_4n,2'b0};
    else
      num_4_reduce_4n_x4 <= num_4_reduce_4n_x4;
  end
  
//num_3_reduce_3n*3
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      num_3_reduce_3n_x3 <= 22'd0;    
    else if( i_clk_27M_en ) //27M 
      num_3_reduce_3n_x3 <= {{3{num_3_reduce_3n[17]}},num_3_reduce_3n,1'b0} + {{4{num_3_reduce_3n[17]}},num_3_reduce_3n};
    else
      num_3_reduce_3n_x3 <= num_3_reduce_3n_x3;
  end
  
//num_2_reduce_2n*2
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      num_2_reduce_2n_x2 <= 22'd0;    
    else if( i_clk_27M_en ) //27M 
      num_2_reduce_2n_x2 <= {{3{num_2_reduce_2n[17]}},num_2_reduce_2n,1'b0};
    else
      num_2_reduce_2n_x2 <= num_2_reduce_2n_x2;
  end
  
//num_1_reduce_1n*1
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      num_1_reduce_1n_x1 <= 22'd0;    
    else if( i_clk_27M_en ) //27M 
      num_1_reduce_1n_x1 <= {{4{num_1_reduce_1n[17]}},num_1_reduce_1n};
    else
      num_1_reduce_1n_x1 <= num_1_reduce_1n_x1;
  end
       
//相加
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      num_14_add_13 <= 23'd0;
    else if( i_clk_27M_en ) //27M 
      num_14_add_13 <= num_14_reduce_14n_x14 + num_13_reduce_13n_x13;
    else
      num_14_add_13 <= num_14_add_13;
  end 

always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      num_12_add_11 <= 23'd0;
    else if( i_clk_27M_en ) //27M 
      num_12_add_11 <= num_12_reduce_12n_x12 + num_11_reduce_11n_x11;
    else
      num_12_add_11 <= num_12_add_11;
  end 

always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      num_10_add_9 <= 23'd0;
    else if( i_clk_27M_en ) //27M 
      num_10_add_9 <= num_10_reduce_10n_x10 + num_9_reduce_9n_x9;
    else
      num_10_add_9 <= num_10_add_9;
  end 

always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      num_8_add_7 <= 23'd0;
    else if( i_clk_27M_en ) //27M 
      num_8_add_7 <= num_8_reduce_8n_x8 + num_7_reduce_7n_x7;
    else
      num_8_add_7 <= num_8_add_7;
  end 

always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      num_6_add_5 <= 23'd0;
    else if( i_clk_27M_en ) //27M 
      num_6_add_5 <= num_6_reduce_6n_x6 + num_5_reduce_5n_x5;
    else
      num_6_add_5 <= num_6_add_5;
  end 

always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      num_4_add_3 <= 23'd0;
    else if( i_clk_27M_en ) //27M 
      num_4_add_3 <= num_4_reduce_4n_x4 + num_3_reduce_3n_x3;
    else
      num_4_add_3 <= num_4_add_3;
  end 

always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      num_2_add_1 <= 23'd0;
    else if( i_clk_27M_en ) //27M 
      num_2_add_1 <= num_2_reduce_2n_x2 + num_1_reduce_1n_x1;
    else
      num_2_add_1 <= num_2_add_1;
  end 
  
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      num_14_add_to_11 <= 24'd0;
    else if( i_clk_27M_en ) //27M 
      num_14_add_to_11 <= num_14_add_13 + num_12_add_11;
    else
      num_14_add_to_11 <= num_14_add_to_11;
  end
  
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      num_10_add_to_7 <= 24'd0;
    else if( i_clk_27M_en ) //27M 
      num_10_add_to_7 <= num_10_add_9 + num_8_add_7;
    else
      num_10_add_to_7 <= num_10_add_to_7;
  end  
  
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      num_6_add_to_3 <= 24'd0;
    else if( i_clk_27M_en ) //27M 
      num_6_add_to_3 <= num_6_add_5 + num_4_add_3;
    else
      num_6_add_to_3 <= num_6_add_to_3;
  end  

always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      num_2_add_to_1 <= 24'd0;
    else if( i_clk_27M_en ) //27M 
      num_2_add_to_1 <= num_2_add_1;
    else
      num_2_add_to_1 <= num_2_add_to_1;
  end 
  
always @ ( negedge i_rst_n or posedge i_clk_81M )//81M
  begin
    if ( !i_rst_n )
      slope <= 26'd0;
    else if( i_clk_27M_en ) //27M 
      slope <= num_14_add_to_11 + num_10_add_to_7 + num_6_add_to_3 + num_2_add_to_1;
    else
      slope <= slope;
  end  
  
assign o_slope = slope;

endmodule
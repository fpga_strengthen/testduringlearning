/*=======================================================================================*\
  Filename    : FirstWord.v
  Author      : 陈献彬
  time        ：2016.07.05
  Description : 解扰完成后，83x10bit的报文顺序不正确，为2-83-1（10bit一组）的状态。
                本模块主要完成对报文顺序的调整。形成正确顺序的报文后，10bit一组发送给下一个模块
                进行10bit-8bit的转换。
               
  Called by   : 
  Revision History : 
  Author              Date                  Version          Description
  陈献彬              20160822               V1.0.0
  Email       : chenxianbin_bupt@163.con
  Company     :

\*========================================================================================*/
module FirstWord(
        i_clk_81M,
        i_rstn,
        
        i_data_start,
        iv_descramed_data,
        i_descramed_data_sck,
        i_crc_error,
        
        o_data_start,
        ov_descramed_data,
        o_descramed_data_sck         
        );
                         
input i_clk_81M;
input i_rstn;
input i_data_start;  //帧开始标志。
input [9:0]iv_descramed_data;//解扰后的数据
input i_descramed_data_sck;  //解扰后数据字节同步时钟
input i_crc_error;           //crc校验错误

output o_data_start;//帧开始的标志，比第一个发送的ov_descramed_data提前几个时钟
output [9:0]ov_descramed_data;//正常顺序的830bit报文
output o_descramed_data_sck;  //字节同步时钟
//---------------------------------------------------------------------------//
reg [9:0]descramed_data_r;//数据寄存
reg descramed_data_sck_r; //字节同步时钟延时，用于crc校验字节同步时钟
reg crc_error;
reg [6:0] wt_add;
reg wt_en;
reg [9:0]data_temp;
wire [39:0]crc_result_before;
wire crc_ok_before;
reg [2:0]cnt_3;//长度为6的计数器，用于控制ram的读地址和读使能的产生
reg [6:0]rd_add;//读地址
reg rd_en;  //读使能
reg o_descramed_data_sck;//报文字节同步时钟
reg [9:0]ov_descramed_data;//报文
reg o_data_start;//数据开始发送标志
//--------------------------------------------------------------------------//
//输入数据寄存.共83x10个bit，10bit一组。第2-83个10bit依次到，最后一个到的是第1个10bit
always@( posedge i_clk_81M or negedge i_rstn )
begin
    if( !i_rstn )
        descramed_data_r <= 10'd0;
    else if( i_descramed_data_sck )
        descramed_data_r <= iv_descramed_data;
    else
        descramed_data_r <= descramed_data_r;
end 

//crc校验错误，给出错误标志，模块停止工作，拉高电平
always@( posedge i_clk_81M or negedge i_rstn )
begin
    if( !i_rstn )
        crc_error <= 1'd0;
    else if( i_data_start )
        crc_error <= 1'd0;
    else if(i_crc_error)
        crc_error <= 1'd1;
    else
        crc_error <= crc_error;
end 

always@( posedge i_clk_81M or negedge i_rstn )
begin
    if( !i_rstn )
        descramed_data_sck_r <= 1'd0;
    else if( wt_add == 7'd83 && i_descramed_data_sck ) //第一个10bit不参与crc校验，去掉其同步时钟（不读入crc模块）
        descramed_data_sck_r <= 1'd0;
    else
        descramed_data_sck_r <= i_descramed_data_sck;
end 

//--------------------------------------------------------------------------//
//将输入数据存入ram。同时进行crc校验

//ram写地址，只将2-83写入ram中.为了方便ram读写控制，我们依旧将第一个10bit写入ram中。
//同时将第一个10bit暂存，发送的时候，从寄存器读取第一个10bit而不是从ram中。既然不需要ram来存取第一个10bit
//该10bit也就不用参与crc校验
always@( posedge i_clk_81M or negedge i_rstn )
begin
    if( !i_rstn )
        wt_add <= 7'd0;  
    else if( i_data_start )
        wt_add <= 7'd1;  //有帧开始标志，地址置1，这样第一个i_descramed_data_sck到来对应的写地址就是2了
    else if( wt_add == 7'd83 && i_descramed_data_sck )
        wt_add <= 7'd1;
    else if( i_descramed_data_sck )
        wt_add <= wt_add + 7'd1;
    else
        wt_add <= wt_add;
end 

//ram写使能
always@( posedge i_clk_81M or negedge i_rstn )
begin
    if( !i_rstn )
        wt_en <= 1'd0;
    else
        wt_en <= i_descramed_data_sck;
end 

//第一个10bit暂存
always@( posedge i_clk_81M or negedge i_rstn )
begin
    if( !i_rstn )
        data_temp <= 10'd0; 
    else if( wt_add == 7'd1 && wt_en )
        data_temp <= descramed_data_r;
end 

//-------------------------------------------------------------------//
//数据写入ram前进行crc校验（只对2-83进行crc）
Crc32_D10 Crc32_D10_before
    (
    .i_clk_27M(i_clk_81M),//clock
    .i_rst_n(i_rstn),  //reset
    .i_data(descramed_data_r),   //data to be checked 
    .i_sck(descramed_data_sck_r),    //check data when the input signal is high level
    .i_CRC_start(i_data_start),//93字起始脉冲
    
    .o_crc33(crc_result_before),  //33bit 32位校验+8bit0  
    .o_crcok(crc_ok_before)   //检验完成指示 高有效 
    );
//--------------------------------------------------------------------//
wire [9:0]data10bit;
ram_first_word_out U_ram_out(
        .wraddress(wt_add),
        .rdaddress(rd_add),
        .wrclock(i_clk_81M),
        .rdclock(i_clk_81M),
        .data(descramed_data_r),
        .q(data10bit),
        .rden(rd_en),
        .wren(wt_en)
        );
//--------------------------------------------------------------------//
//数据写ram完成后，开始读数据并向下一个模块发送
always@( posedge i_clk_81M or negedge i_rstn )
begin
    if( !i_rstn )
        cnt_3 <= 3'd0;
    else if(i_data_start)
        cnt_3 <= 3'd0;
    else if( crc_error )//crc校验错误，模块不进行读操作
        cnt_3 <= 3'd0;
    else if( wt_add == 7'd1 && wt_en )//830bit接收完毕，开始读数据
        cnt_3 <= 3'd1;
    else if ( rd_add == 7'd88 && cnt_3 ==3'd1 )//830bit读完，cnt_3清零
        cnt_3 <= 3'd0; 
    else if( cnt_3 == 3'd6 )//mark0901   else if( cnt_3 == 3'd3 ) 
        cnt_3 <= 3'd1;
    else if ( cnt_3 )
        cnt_3 <= cnt_3 + 3'd1;
    else
        cnt_3 <= cnt_3;
end 

always@( posedge i_clk_81M or negedge i_rstn )
begin
    if( !i_rstn )
        rd_add <= 7'd0;
    else if(i_data_start)
        rd_add <= 7'd0;
    else if( rd_add == 7'd88 && cnt_3 == 2'd1 )  //830bit + 4x10bitcrc校验码读完，读地址清零
        rd_add <= 7'd0;
    else if( cnt_3 == 2'd1 )
        rd_add <= rd_add + 7'd1;
    else
        rd_add <= rd_add;
end 

always@( posedge i_clk_81M or negedge i_rstn )
begin
      if( !i_rstn )
          rd_en <= 1'd0;
      else if ( 7'd2<= rd_add && rd_add <=7'd83 && cnt_3 ==2'd2  ) //只给出读取2-83的数据的使能，后面虽然有读地址（84-87），但是没有读使能
          rd_en <= 1'd1;
      else
          rd_en <= 1'd0;
end
//---------------------------------------------------------------//
//1-83正确顺序的10bit一组报文
always@( posedge i_clk_81M or negedge i_rstn )
begin
      if( !i_rstn )
          ov_descramed_data <= 10'd0;
      else if ( rd_add == 7'd1 && cnt_3 == 2'd3 )begin //第一个10bit
          ov_descramed_data <= data_temp;
          o_descramed_data_sck <= 1'd1;
       end
       else if ( rd_add == 7'd85 && cnt_3 == 2'd2 )begin //第一个10bit crc
          ov_descramed_data <= crc_result_before[39:30];
          o_descramed_data_sck <= 1'b1;
       end
       else if ( rd_add == 7'd86 && cnt_3 == 2'd2 )begin //第2个10bit crc
          ov_descramed_data <= crc_result_before[29:20];
          o_descramed_data_sck <= 1'b1;
       end
       else if ( rd_add == 7'd87 && cnt_3 == 2'd2 )begin //第3个10bit crc
          ov_descramed_data <= crc_result_before[19:10];
          o_descramed_data_sck <= 1'd1;
      end
       else if ( rd_add == 7'd88 && cnt_3 == 2'd2 )begin //第4个10bit crc
          ov_descramed_data <= crc_result_before[9:0];
          o_descramed_data_sck <= 1'd1; 
      end
      else if (  7'd3 <= rd_add && rd_add <= 7'd84  && cnt_3 == 2'd2 )begin// 读取第2-83个10bit
          ov_descramed_data <= data10bit;
          o_descramed_data_sck <= 1'd1; 
      end
      else begin
          ov_descramed_data <= ov_descramed_data;
          o_descramed_data_sck <= 1'd0;
      end
end

always@( posedge i_clk_81M or negedge i_rstn )
begin
      if( !i_rstn )
          o_data_start <= 1'd0;
      else if (rd_add == 7'd1 && cnt_3 == 2'd2)
          o_data_start <= 1'd1;
      else
          o_data_start <= 1'd0; 
end

endmodule    
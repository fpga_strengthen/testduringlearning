//----------------------------------------------
//ROM的简单模型
//FPGA中实际上并没有专用的ROM资源，实现ROM的思路是对RAM赋予初值并保持该初值
//----------------------------------------------
module single_port_rom #(
    parameter   DATA_WIDTH = 8,
    parameter   ADDR_WIDTH = 8
)(
    input   [(ADDR_WIDTH-1):0]      addr,
    input   clk,

    output  reg [(DATA_WIDTH-1):0]  q       //rom只有数据输出，没有数据输入
);
    //ROM定义
    reg [DATA_WIDTH-1:0]    rom [2**ADDR_WIDTH-1:0];

    //识别ROM中的内容
    initial
        $readmemb("single_port_rom_init.txt",rom);
    
    always@(posedge clk)begin 
        q <= rom[addr];                     //ROM输出
    end

endmodule
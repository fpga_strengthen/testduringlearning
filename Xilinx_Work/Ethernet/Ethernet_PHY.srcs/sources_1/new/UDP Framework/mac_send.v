`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date: 2024/08/16 15:45:51
// Design Name: 
// Module Name: mac_send
// Description: 主要有以下功能：
//				1. 对IP层发送的数据进行打包,组成MAC数据包;
//				2. 跨时钟域:将用户时钟域的数据转化到PHY芯片时钟域下;
//////////////////////////////////////////////////////////////////////////////////

module mac_send #(
	parameter	TARGET_MAC_ADDR = 48'hFF_FF_FF_FF_FF_FF,
	parameter	LOCAL_MAC_ADDR = 48'hFF_FF_FF_FF_FF_FF
	)(
	// 时钟和复位
	input wire			i_clk,			// 用户时钟
	input wire			i_rst_n,		// 复位信号,低有效
	input wire			i_phy_tx_clk,	// 由PLL得到的125M时钟
	input wire			i_phy_rst_n,	// 复位

	/*---------------- rgmii_send 模块交互信号--------------------*/
	output reg 			o_gmii_tx_data_vld,		// 数据有效信号
	output reg 	[7:0]	ov_gmii_tx_data,		// MAC数据包数据(单沿)

	/*------------------- ip_send 模块交互信号 -------------------*/
	input wire			i_mac_tx_data_vld,		// 数据有效信号
	input wire			i_mac_tx_data_last,		// MAC层数据的最后一个数据的标志
	input wire	[7:0]	iv_mac_tx_data,			// 从MAC数据包解析出MAC层数据
	input wire	[15:0]	iv_mac_tx_frame_type,	// 帧类型
	input wire	[15:0]	iv_mac_tx_length,		// 接收数据长度

	/*---------------- tx_crc32_d8 模块交互信号--------------------*/
	input wire	[31:0]	iv_tx_CRC_data,			// 收到的CRC数据
	output reg			o_tx_CRC_din_vld,		// CRC校验使能信号
	output wire	[7:0]	ov_tx_CRC_din,			// 要做CRC的数据
	output reg			o_tx_CRC_done,			// 四个字节的CRC发送完成

	/*----------------ARP动态链表查找的MAC地址--------------------*/
	input wire			i_rd_arp_list_mac_vld,	// 数据有效信号
	input wire	[47:0]	iv_rd_arp_list_mac		// MAC地址
);

/*-----------------------------------------------*\
					状态机信号定义
\*-----------------------------------------------*/	
	reg 	[2:0]	cur_status;
	reg 	[2:0]	nxt_status;

	localparam  TX_IDLE = 3'b000,
				TX_PRE  = 3'b001,
				TX_DATA = 3'b010,
				TX_END  = 3'b100;

/*-----------------------------------------------*\
				定义FIFO端口信号
\*-----------------------------------------------*/

	// frame fifo
	reg				frame_wren;		// 帧数据写使能
	reg		[63:0]	frame_din;		// 1bit CRC + 2byte Type
	wire	[63:0]	frame_dout;		// 读出的数据
	wire			frame_rden;		// 读使能
	wire			frame_wrfull;	// FIFO写满标志
	wire			frame_rdempty;	// FIFO读空
	wire	[3:0]	frame_wrcount;
	wire	[3:0]	frame_rdcount;

	// data fifo
	reg				data_wren;		// 帧数据写使能
	reg		[8:0]	data_din;		// 1bit CRC + 2byte Type
	reg				data_rden;		// 读使能
	wire	[8:0]	data_dout;		// 读出的数据
	wire			data_wrfull;	// FIFO写满标志
	wire			data_rdempty;	// FIFO读空
	wire	[11:0]	data_wrcount;
	wire	[11:0]	data_rdcount;

/*-----------------------------------------------*\
					定义其他信号
\*-----------------------------------------------*/

	reg				crc_data_en;		// CRC字节使能有效信号
	reg		[2:0]	cnt_crc_byte;		// CRC字节计数器

	reg 	[10:0]	cnt_tx_byte;		// 对MAC数据包字节计数
	
	reg  	[7:0]	send_data;			// 待发送数据
	reg 			send_data_en;		// 数据同步有效信号
	reg 			send_data_last;		// 最后一个数据的标志

	reg  	[15:0]	mac_type;			// 两个字节的帧类型

	reg 	[7:0]	crc_data_8bit;

	reg 	[47:0]	send_target_mac;	// 要发送的目标MAC地址
	reg 	[47:0]	send_target_mac_cdc;

/*-----------------------------------------------*\
				更新目标MAC地址
\*-----------------------------------------------*/

	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n) 
			send_target_mac <= TARGET_MAC_ADDR;	// 复位时就给初始的目标MAC地址
		else if(i_rd_arp_list_mac_vld)
			send_target_mac <= iv_rd_arp_list_mac;	// 查询arp动态链表的目标MAC地址
		else if(i_mac_tx_data_vld && (iv_mac_tx_frame_type == 16'h0086))
			send_target_mac <= 48'hFF_FF_FF_FF_FF_FF;	// 广播地址
		else
			send_target_mac <= send_target_mac;
	end

/*-----------------------------------------------*\
				将MAC有效数据写入FIFO
\*-----------------------------------------------*/

	// 将帧类型数据写入FIFO
	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n) begin
			frame_din  <= 64'b0;
			frame_wren <= 1'b0;
		end 
		else begin
			frame_din  <= {send_target_mac,iv_mac_tx_frame_type};
			frame_wren <= i_mac_tx_data_last;
		end
	end

	// 将MAC有效数据写入FIFO
	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n)begin
			data_din  <= 9'b0;
			data_wren <= 1'b0;
		end
		else begin
			data_din  <= {i_mac_tx_data_last,iv_mac_tx_data};
			data_wren <= i_mac_tx_data_vld;
		end
	end

/*-----------------------------------------------*\
				状态机控制读FIFO
\*-----------------------------------------------*/

	always@(posedge i_phy_tx_clk or negedge i_phy_rst_n)begin 
		if(~i_phy_rst_n)
			cur_status <= TX_IDLE;
		else
			cur_status <= nxt_status;
	end

	always@(*)begin 
		if(~i_phy_rst_n)
			nxt_status <= TX_IDLE;
		else begin 
			case (cur_status)
				TX_IDLE : begin
					if(~frame_rdempty)
						nxt_status <= TX_PRE;
					else
						nxt_status <= cur_status;
				end
				TX_PRE : nxt_status <= TX_DATA;
				TX_DATA : begin 
					if(crc_data_en && (cnt_crc_byte == 3'd3))
						nxt_status <= TX_END;
					else
						nxt_status <= cur_status;
				end
				TX_END : nxt_status <= cur_status;
			default : nxt_status <= TX_IDLE;
			endcase
		end
	end

/*-----------------------------------------------*\
					FIFO例化
\*-----------------------------------------------*/

	// 特别注意FIFO的复位为高有效.
	// 此FIFO已经进行了修改,实际输入数据位宽为64位,为MAC地址长度+帧类型长度之和.
	frame_fifo_w17xd16 tx_frame_fifo(
  		.rst           (~i_rst_n),            		// input wire rst
  		.wr_clk        (i_clk),              		// input wire wr_clk
  		.rd_clk        (i_phy_tx_clk),              // input wire rd_clk
  		.din           (frame_din),                 // input wire [16 : 0] din
  		.wr_en         (frame_wren),                // input wire wr_en
  		.rd_en         (frame_rden),                // input wire rd_en
  		.dout          (frame_dout),                // output wire [16 : 0] dout
  		.full          (frame_wrfull),              // output wire full
  		.empty         (frame_rdempty),             // output wire empty
  		.rd_data_count (frame_rdcount),  			// output wire [3 : 0] rd_data_count
  		.wr_data_count (frame_wrcount)  			// output wire [3 : 0] wr_data_count
	);

	fifo_data_w9xd4096 tx_data_fifo(
	 	.rst           (~i_rst_n),            		// input wire rst
	 	.wr_clk        (i_clk),              		// input wire wr_clk
	 	.rd_clk        (i_phy_tx_clk),             	// input wire rd_clk
	 	.din           (data_din),                  // input wire [8 : 0] din
	 	.wr_en         (data_wren),                 // input wire wr_en
	 	.rd_en         (data_rden),                 // input wire rd_en
	 	.dout          (data_dout),                 // output wire [8 : 0] dout
	 	.full          (data_wrfull),               // output wire full
	 	.empty         (data_rdempty),              // output wire empty
	 	.rd_data_count (data_rdcount),  			// output wire [11 : 0] rd_data_count
	 	.wr_data_count (data_wrcount)  				// output wire [11 : 0] wr_data_count
	);

/*-----------------------------------------------*\
				对接收数据计数
\*-----------------------------------------------*/

	// 数据FIFO写入几个数据后才是非空(近似状态),之后才会进入写数据状态
	always @(posedge i_phy_tx_clk or negedge i_phy_rst_n) begin
		if(~i_phy_rst_n)
			cnt_tx_byte <= 11'd0;
		else if(cur_status == TX_DATA)
			cnt_tx_byte <= cnt_tx_byte + 1'b1;
		else		// 只在TX_DATA状态进行计数
			cnt_tx_byte <= 11'd0;
	end

/*-----------------------------------------------*\
				帧FIFO读使能
\*-----------------------------------------------*/
	
	// 读使能
	assign	frame_rden = (cur_status == TX_PRE);

	// 将由帧FIFO读出的帧类型寄存
	always @(posedge i_phy_tx_clk or negedge i_phy_rst_n)begin
		if(~i_phy_rst_n)begin 
			mac_type            <= 16'b0;
			send_target_mac_cdc <= 48'h0;
		end
		else if(frame_rden)begin
			mac_type            <= frame_dout[15:0];
			send_target_mac_cdc <= frame_dout[63:16];
		end
		else begin
			mac_type            <= mac_type;
			send_target_mac_cdc <= send_target_mac_cdc;
		end
	end

/*-----------------------------------------------*\
				data fifo读数据
\*-----------------------------------------------*/

	// 读使能
	// 在进入读状态之后,MAC首部发送完才会开始发送数据
	// 所以不能单纯使用FSM的状态来作为data读使能的依据
	always @(posedge i_phy_tx_clk or negedge i_phy_rst_n) begin
		if(~i_phy_rst_n)
			data_rden <= 1'b0; 
		else if((cur_status == TX_DATA)&&(cnt_tx_byte == 11'd21))
			data_rden <= 1'b1;
		else if(data_rden && data_dout[8])
			data_rden <= 1'b0;		// data_dout[8]表示last信号
		else
			data_rden <= data_rden;
	end

	// 最后一个数据的标志
	always @(posedge i_phy_tx_clk or negedge i_phy_rst_n) begin
		if(~i_phy_rst_n)
			send_data_last <= 1'b0;
		else if(data_rden && data_dout[8])
			send_data_last <= 1'b1;
		else
			send_data_last <= 1'b0;
	end

	// 数据发送使能
	always @(posedge i_phy_tx_clk or negedge i_phy_rst_n) begin
		if(~i_phy_rst_n) 
			send_data_en <= 1'b0;
		else if((cur_status == TX_DATA)&&(cnt_tx_byte == 11'd0))
			send_data_en <= 1'b1;
		else if(send_data_last)
			send_data_en <= 1'b0;
		else
			send_data_en <= send_data_en;
	end

/*-----------------------------------------------*\
					CRC
\*-----------------------------------------------*/

	// 要做CRC的数据同步有效信号
	always @(posedge i_phy_tx_clk or negedge i_phy_rst_n) begin
		if(~i_phy_rst_n)
			o_tx_CRC_din_vld <= 1'b0;
		else if(cnt_tx_byte == 11'd8)
			o_tx_CRC_din_vld <= 1'b1;
		else if(send_data_last)
			o_tx_CRC_din_vld <= 1'b0;
		else
			o_tx_CRC_din_vld <= o_tx_CRC_din_vld;
	end

	// 要做CRC的数据
	// 由于有din_vld信号的同步控制,所以send_data直接赋给输出即可
	// 使用组合逻辑是因为vld和data都是通过计数器控制的,如果使用时序逻辑,data会比vld晚一拍
	assign	ov_tx_CRC_din = send_data;

	always @(posedge i_phy_tx_clk or negedge i_phy_rst_n) begin
		if(~i_phy_rst_n)
			o_tx_CRC_done <= 1'b0;
		else if(crc_data_en && cnt_crc_byte == 3'd3)
			o_tx_CRC_done <= 1'b1;		// 当CRC计数够4时,就表明四个字节的CRC计算完毕,CRC校验完成
		else
			o_tx_CRC_done <= 1'b0;
	end

	// crc_data_en : 与最后四个字节CRC同步的使能信号
	always @(posedge i_phy_tx_clk or negedge i_phy_rst_n) begin
		if(~i_phy_rst_n)
			crc_data_en <= 1'b0;
		else if(crc_data_en && cnt_crc_byte == 3'd3)
			crc_data_en <= 1'b0;
		else if(send_data_last)
			crc_data_en <= 1'b1;
		else
			crc_data_en <= crc_data_en;
	end

	// 对四个字节的CRC计数
	always @(posedge i_phy_tx_clk or negedge i_phy_rst_n) begin
		if(~i_phy_rst_n)
			cnt_crc_byte <= 3'd0;
		else if(crc_data_en && cnt_crc_byte == 3'd3)
			cnt_crc_byte <= 3'd0;
		else if(crc_data_en)
			cnt_crc_byte <= cnt_crc_byte + 1'b1;
		else
			cnt_crc_byte <= cnt_crc_byte;
	end

	// 将输入的32位CRC数据转化为8位输出,注意CRC与MAC层其他数据相反,先发送低位
	// 使用组合逻辑是为了避免延迟一拍
	always @(*)begin
		if(~i_phy_rst_n)
			crc_data_8bit <= 8'b0;
		else begin 
			case (cnt_crc_byte)
				3'd0    : crc_data_8bit <= iv_tx_CRC_data[7:0];
				3'd1    : crc_data_8bit <= iv_tx_CRC_data[15:8];
				3'd2    : crc_data_8bit <= iv_tx_CRC_data[23:16];
				3'd3    : crc_data_8bit <= iv_tx_CRC_data[31:24];
				default : crc_data_8bit <= 8'b0;
			endcase
		end
	end

/*-----------------------------------------------*\
				MAC层的数据包组包
\*-----------------------------------------------*/

	// 由于数据很多,所以使用case语句比较简洁
	always @(posedge i_phy_tx_clk or negedge i_phy_rst_n) begin
		if(~i_phy_rst_n)
			send_data <= 8'b0;
		else begin 	// 什么时候开始发送数据、即字节计数器何时计数已经通过状态机进行控制了
			case (cnt_tx_byte)
				// 前7个字节为前导码
				0,1,2,3,4,5,6 : send_data <= 8'h55;			
				// SFD，帧起始标志
				7  : send_data <= 8'hD5;					
				// 目的MAC地址,先发送高位,后发送低位
				8  : send_data <= send_target_mac_cdc[47:40];	
				9  : send_data <= send_target_mac_cdc[39:32];
				10 : send_data <= send_target_mac_cdc[31:24];
				11 : send_data <= send_target_mac_cdc[23:16];
				12 : send_data <= send_target_mac_cdc[15:8];
				13 : send_data <= send_target_mac_cdc[7:0];
				// 源MAC地址,先发送高位,后发送低位
				14 : send_data <= LOCAL_MAC_ADDR[47:40];	
				15 : send_data <= LOCAL_MAC_ADDR[39:32];
				16 : send_data <= LOCAL_MAC_ADDR[31:24];
				17 : send_data <= LOCAL_MAC_ADDR[23:16];
				18 : send_data <= LOCAL_MAC_ADDR[15:8];
				19 : send_data <= LOCAL_MAC_ADDR[7:0];
				// 帧类型,两个字节
				20 : send_data <= mac_type[15:8];
				21 : send_data <= mac_type[7:0];
				// MAC层数据
				default : send_data <= data_dout[7:0];		// 发送数据和CRC
			endcase 
		end
	end

/*-----------------------------------------------*\
					组MAC包
\*-----------------------------------------------*/

	// 数据使能信号
	always @(posedge i_phy_tx_clk or negedge i_phy_rst_n) begin
		if(~i_phy_rst_n)
			o_gmii_tx_data_vld <= 1'b0;
		else		// 这两个使能信号不会同时拉高的
			o_gmii_tx_data_vld <= (send_data_en | crc_data_en);
	end

	// 数据
	always @(posedge i_phy_tx_clk or negedge i_phy_rst_n) begin
		if(~i_phy_rst_n)
			ov_gmii_tx_data <= 8'b0;
		else if(send_data_en)
			ov_gmii_tx_data <= send_data;
		else if(crc_data_en)
			ov_gmii_tx_data <= crc_data_8bit;
		else
			ov_gmii_tx_data <= ov_gmii_tx_data;
	end

endmodule

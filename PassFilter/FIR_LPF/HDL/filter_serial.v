module filter_serial(
    input   clk,
    input   clk_enable,
    input   rst_n,
    input   signed  [15:0]  filter_in,          //sfix16_En15  signed_fixed_16bit
    output  signed  [15:0]  filter_out
);
    parameter   signed  [15:0]  coeff1 = 16'b1110_1110_1011_1001;
    parameter   signed  [15:0]  coeff2 = 16'b0100_1000_1011_1111;
    parameter   signed  [15:0]  coeff3 = 16'b0111_0001_1011_1010;
    parameter   signed  [15:0]  coeff4 = 16'b0111_0001_1011_1010;
    parameter   signed  [15:0]  coeff5 = 16'b0100_1000_1011_1111;
    parameter   signed  [15:0]  coeff6 = 16'b1110_1110_1011_1001;

    //信号，用于选择当前是输出滤波器结果还是锁存输入，还是进行中间计算
    reg [2:0]   cur_count;

    //cur_count为增量计数器
    always@(posedge clk)begin
        if(rst_n == 1'b0)
            cur_count <= 3'b101;        //同步复位
        else if(clk_enable == 1'b1)begin 
            if(cur_count == 3'b101)
                cur_count <= 3'b000;
            else
                cur_count <= cur_count + 1'b1;
        end
        else
            cur_count <= cur_count;
    end

    //控制信号，指示是否输出或者是否开始第一次乘加
    wire    FIRST_SUM_STAGE;
    //当该信号有效时，表示一轮乘加操作已经完成，可以进行输出
    wire    OUTPUT_STAGE;

    assign  FIRST_SUM_STAGE = ((cur_count == 3'b101)&&(clk_enable == 1'b1)) ? 1'b1 : 1'b0;
    assign  OUTPUT_STAGE = ((cur_count == 3'b000)&&(clk_enable == 1'b1)) ? 1'b1 : 1'b0;

    //存储FIR滤波器中间数据
    reg signed  [15:0]  delay_pipeline [0:5];

    integer i;

    always@(posedge clk)begin
        if(rst_n == 1'b0)begin 
            for(i=0;i<=5;i=i+1)
                delay_pipeline[i] <= 0;
        end
        else if(FIRST_SUM_STAGE == 1'b1)begin 
            delay_pipeline[0] <= filter_in;
            for(i=1;i<=5;i=i+1)
                delay_pipeline[i] <= delay_pipeline[i-1];
           //delay_pipeline[1] <= delay_pipeline[0];
           //delay_pipeline[2] <= delay_pipeline[1];
           //delay_pipeline[3] <= delay_pipeline[2];
           //delay_pipeline[4] <= delay_pipeline[3];
           //delay_pipeline[5] <= delay_pipeline[4];
        end
    end

    //用于选择哪一个存储数据进行乘法运算
    wire    [15:0]  input_mux_1;
    wire    [15:0]  product_mux_1;

    assign  input_mux_1 =   (cur_count == 3'b000) ? delay_pipeline[0] :
                            (cur_count == 3'b001) ? delay_pipeline[1] :
                            (cur_count == 3'b010) ? delay_pipeline[2] :
                            (cur_count == 3'b011) ? delay_pipeline[3] :
                            (cur_count == 3'b100) ? delay_pipeline[4] : delay_pipeline[5];

    assign  product_mux_1 = (cur_count == 3'b000) ? coeff1 :
                            (cur_count == 3'b001) ? coeff2 :
                            (cur_count == 3'b010) ? coeff3 :
                            (cur_count == 3'b011) ? coeff4 :
                            (cur_count == 3'b100) ? coeff5 : coeff6;

    //乘积
    wire    [31:0]  mul_temp;

    //每一次数据与系数的乘法，实际会多产生一个符号位，最后要去掉
    assign  mul_temp = input_mux_1 * product_mux_1;

    //累加器中的输入输出与中间累加结果
    wire    signed  [32:0]  acc_sum_1;
    wire    signed  [32:0]  acc_in_1;
    reg     signed  [32:0]  acc_out_1;

    //对乘法结果进行累加
    //（个人理解）这里对乘法结果作符号位扩展是为了使加法式子两端所有数据保持相同的位长
    assign  acc_sum_1 = {mul_temp[31],mul_temp} + acc_out_1;

    //累加器的输入端
    //特别解释：每当一轮乘加操作完成、到输出阶段时，要将累加结果清空（不能再继续累加），
    //         以开始新一轮的累加操作，所以直接更新为当前最新的乘法结果
    //当该轮的乘加操作未完成时，就持续接收累加结果，并将累加结果进行缓存（缓存到acc_out_1）
    assign  acc_in_1 = (OUTPUT_STAGE == 1'b1) ? {mul_temp[31],mul_temp} : acc_sum_1;

    //缓存累加结果
    always@(posedge clk)begin 
        if(rst_n == 1'b0)
            acc_out_1 <= 0;
        else if(clk_enable == 1'b1)
            acc_out_1 <= acc_in_1;
        else
            acc_out_1 <= acc_out_1;
    end

    reg signed  [32:0]  acc_final;

    //到输出阶段时，将缓存的累加结果进行输出
    always@(posedge clk)begin
        if(rst_n == 1'b0)
            acc_final <= 0;
        else if(OUTPUT_STAGE == 1'b1)
            acc_final <= acc_out_1;
        else    
            acc_final <= acc_final;
    end

    //根据仿真测试，移位运算符>>>的移位结果与数据的正负有关，
    //移位后空位会填补符号位：当数据为正时，右移后空位补零；当数据为负时，右移后左边空位补1
    //对滤波器输出四舍五入
    //如果是正数，由于正数的原码、反码和补码一致，所以直接取满；
    //如果是负数，而负数以补码表示，所以符号位保持，数据位全部取0
    //若acc_final[16]=0,则下式中加法等同于 acc_final[31:0] + 16'b0111_1111_1111_1111
    //若acc_final[16]=1,则下式中加法等同于 acc_final[31:0] + 16'b1000_0000_0000_0000
    assign  filter_out = (acc_final[31:0] + {acc_final[16],{15{~acc_final[16]}}}) >>> 16;

endmodule
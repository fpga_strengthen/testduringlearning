close all;
clear;
clc;

%% 幅值检测算法测试

%fsk数据
ADC_data = textread('D:\lab_project\BTM\测试\单通道测试文档\0103数据\未改电阻\九阶滤波器单通道接收板功放上电断4M_adc.txt','%s');
i_data_ADC =  char(ADC_data) ;
i_data_ADC = str2num(i_data_ADC)'- 2048;

 
% Hilbert变换 并将数据对齐
[im_datain0] = hilbert_trans(i_data_ADC);
Length_data = length(im_datain0);
im_datain = im_datain0(11:Length_data-1);
i_data_ADC = i_data_ADC(6:Length_data-6);

%取绝对值
im_datain_abs = abs(im_datain);
i_data_ADC_abs = abs(i_data_ADC);

%幅值检测算法
am = max(im_datain_abs,i_data_ADC_abs) + min(im_datain_abs,i_data_ADC_abs)./2;

%取平均
am_average = fix( ( am([1:end-3]) + am([2:end-2]) + am([3:end-1]) + am([4:end]) )./4 );

%求am_average的统计量
am_average_mean = mean(am_average);
% am_average_mean = am_average_mean*ones(1,length(am_average));

%% 噪声数据
noise = textread('D:\lab_project\BTM\测试\data\新版接收板程序数据\1102应答器极限幅值测试\干扰测试_adc.txt','%s');
noise =  char(noise) ;
noise = str2num(noise)'- 2048;

% Hilbert变换 并将数据对齐
[im_noise] = hilbert_trans(noise);
Length_im_noise = length(im_noise);
im_noise = im_noise(11:Length_im_noise-1);
noise = noise(6:Length_im_noise-6);

%取绝对值
im_noise_abs = abs(im_noise);
noise_abs = abs(noise);

%幅值检测算法
am_noise = max(im_noise_abs,noise_abs) + min(im_noise_abs,noise_abs)./2;

%取平均
am_noise_average = fix( ( am_noise([1:end-3]) + am_noise([2:end-2]) + am_noise([3:end-1]) + am_noise([4:end]) )./4 );

%求am_average的均值用于画图
am_noise_average_mean = mean(am_noise_average);
% am_noise_average_mean = am_noise_average_mean*ones(1,length(am_noise_average));
am_noise_average_max = max(am_noise_average);
am_noise_average_min = min(am_noise_average);
am_noise_average_var = var(am_noise_average);

%% 画图
figure(1),
plot(i_data_ADC,'g');
hold on,
plot(am_average,'r');
% % plot(am_average_mean,'c');
% plot(noise,'b');
% plot(am_noise_average,'m');
% plot(am_noise_average_mean,'y');
% legend('报文数据','报文幅值','报文幅值的均值','噪声','噪声幅值','噪声幅值的均值');
legend('报文数据','报文幅值','噪声','噪声幅值');
title(['报文幅值的均值=',num2str(am_average_mean),';噪声幅值的均值=',num2str(am_noise_average_mean)]);

% figure(2),
% [f, xc] = ecdf(am_noise_average); % 调用ecdf函数计算xc处的经验分布函数值f
% ecdfhist(f, xc); % 绘制频率直方图
% xlabel('噪声幅值');
% ylabel('频率');
% title(['噪声频率直方图：','噪声幅值的均值=',num2str(am_noise_average_mean),... 
%        ';噪声幅值的方差=',num2str(am_noise_average_var),';噪声幅值的最大值=',num2str(am_noise_average_max),';噪声幅值的最小值=',num2str(am_noise_average_min)]);

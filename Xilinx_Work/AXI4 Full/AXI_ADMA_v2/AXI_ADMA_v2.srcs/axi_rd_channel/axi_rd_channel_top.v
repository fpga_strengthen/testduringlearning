`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Module Name: axi_rd_channel_top
// Description: 
// Revision 0.01 - File Created
//////////////////////////////////////////////////////////////////////////////////

module axi_rd_channel_top #(
	parameter   USER_RD_DATA_WIDTH = 16,		// 用户数据位宽
	parameter   AXI_DATA_WIDTH     = 128,		// 转换到的AXI数据位宽
	parameter   AXI_ADDR_WIDTH     = 32,		// AXI写地址宽度
	parameter   RD_BURST_LENGTH    = 4096		// 最大读突发长度,以字节为单位
	)(
	input 	wire							i_user_rd_clk,			// 用户写时钟
	input 	wire							i_rst_n,				// 复位
	input 	wire							i_axi_clk,				// AXI时钟
	
	input 	wire							i_ddr_init_done,		// DDR3初始化完成信号

	/*--------------------------用户端信号-----------------------------*/
	input   wire							i_user_rd_req,			// 数据同步有效信号
	input   wire	[AXI_ADDR_WIDTH-1:0]	iv_user_rd_base_addr,	// 用户读数据的基地址
	input   wire	[AXI_ADDR_WIDTH-1:0]	iv_user_rd_end_addr,	// 用户读数据的结束地址
	output  wire							o_rd_req_busy,			// 用户请求忙标志,此时当前模块无法处理新请求

	// v2版本新添加的用户端口信号
	input   wire							i_user_rd_mode,			// 用户读模式设置:0表示v1版本,1表示可以设定读地址的v2版本
	input   wire	[AXI_ADDR_WIDTH-1:0]	iv_user_rd_addr,
	input   wire	[12:0]					iv_user_rd_length,

	// 输出给用户端
	output	wire							o_user_rd_valid,		// 数据有效信号
	output	wire	[USER_RD_DATA_WIDTH-1:0]ov_user_rd_data,		// 读出给用户端的数据
	output	wire							o_user_rd_last,			// 最后一个数据的标志

	/*-----------------------与从机的交互信号----------------------*/
	// 读通道读数据
	input	wire	[3:0]					iv_m_axi_rid,			// 读 IDtag
	input	wire	[AXI_DATA_WIDTH-1:0]	iv_m_axi_rdata,			// 读数据
	input	wire	[1:0]					iv_m_axi_rresp,			// 读响应，表明读传输的状态
	input	wire							i_m_axi_rlast,			// 表明读突发的最后一次传输
	input	wire							i_m_axi_rvalid,			// 表明此通道信号有效
	output	wire							o_m_axi_rready,			// 表明主机能够接收读数据和响应信息

	// 读通道地址与控制信号
	output	wire	[3:0]					ov_m_axi_arid,			// 读地址 ID，用来标志一组写信号
	output	wire	[AXI_ADDR_WIDTH-1:0]	ov_m_axi_araddr,		// 读地址，给出一次写突发传输的读地址
	output	wire	[7:0]					ov_m_axi_arlen,			// 突发长度，给出突发传输的次数
	output	wire	[2:0]					ov_m_axi_arsize,		// 突发大小，给出每次突发传输的字节数
	output	wire	[1:0]					ov_m_axi_arburst,		// 突发类型
	output	wire							o_m_axi_arlock,			// 总线锁信号，可提供操作的原子性
	output	wire	[3:0]					ov_m_axi_arcache,		// 内存类型，表明一次传输是怎样通过系统的
	output	wire	[2:0]					ov_m_axi_arprot,		// 保护类型，表明一次传输的特权级及安全等级
	output	wire	[3:0]					ov_m_axi_arqos,			// 质量服务 QOS
	output	wire							o_m_axi_arvalid,		// 有效信号，表明此通道的地址控制信号有效
	input	wire							i_m_axi_arready			// 表明“从”可以接收地址和对应的控制信号
);

	wire							rd_req_en;
	wire	[7:0]					rd_burst_length;
	wire	[AXI_ADDR_WIDTH-1:0]	rd_data_addr;
	wire							rd_req_ready;

	wire							axi_ar_req_en;
	wire							axi_ar_ready;
	wire	[7:0]					axi_ar_burst_len;
	wire	[AXI_ADDR_WIDTH-1:0]	axi_ar_addr;

	wire							axi_r_valid;
	wire	[AXI_DATA_WIDTH-1:0]	axi_r_data;
	wire							axi_r_last;

	// FIFO调试信号
	wire							rd_cmd_fifo_err;
	wire							rd_data_fifo_err;

	rd_ctrl #(
		.AXI_DATA_WIDTH  (AXI_DATA_WIDTH),
		.AXI_ADDR_WIDTH  (AXI_ADDR_WIDTH),
		.RD_BURST_LENGTH (RD_BURST_LENGTH)
	) u0 (
		.i_clk                (i_user_rd_clk),
		.i_rst_n              (i_rst_n),

		.i_ddr_init_done      (i_ddr_init_done),

		.i_user_rd_req        (i_user_rd_req),
		.iv_user_rd_base_addr (iv_user_rd_base_addr),
		.iv_user_rd_end_addr  (iv_user_rd_end_addr),

		.i_user_rd_mode       (i_user_rd_mode),
		.iv_user_rd_addr      (iv_user_rd_addr),
		.iv_user_rd_length    (iv_user_rd_length),

		.o_rd_req_busy        (o_rd_req_busy),

		.o_rd_req_en          (rd_req_en),
		.ov_rd_burst_length   (rd_burst_length),
		.ov_rd_data_addr      (rd_data_addr),
		.i_rd_req_ready       (rd_req_ready)
	);

	rd_buffer #(
		.AXI_DATA_WIDTH  (AXI_DATA_WIDTH),
		.AXI_ADDR_WIDTH  (AXI_ADDR_WIDTH),
		.USER_DATA_WIDTH (USER_RD_DATA_WIDTH)
	) u1 (
		.i_clk               (i_user_rd_clk),
		.i_rst_n             (i_rst_n),
		.i_axi_clk           (i_axi_clk),

		.i_rd_req_en         (rd_req_en),
		.iv_rd_burst_length  (rd_burst_length),
		.iv_rd_data_addr     (rd_data_addr),

		.o_rd_req_ready      (rd_req_ready),

		.o_axi_ar_req_en     (axi_ar_req_en),
		.i_axi_ar_ready      (axi_ar_ready),
		.ov_axi_ar_burst_len (axi_ar_burst_len),
		.ov_axi_ar_addr      (axi_ar_addr),

		.i_axi_r_valid       (axi_r_valid),
		.iv_axi_r_data       (axi_r_data),
		.i_axi_r_last        (axi_r_last),

		.o_user_rd_valid     (o_user_rd_valid),
		.ov_user_rd_data     (ov_user_rd_data),
		.o_user_rd_last      (o_user_rd_last),

		.o_rd_data_fifo_err  (rd_data_fifo_err),
		.o_rd_cmd_fifo_err   (rd_cmd_fifo_err)
	);


	axi_rd_master #(
		.AXI_DATA_WIDTH (AXI_DATA_WIDTH),
		.AXI_ADDR_WIDTH (AXI_ADDR_WIDTH)
	) u2 (
		.i_rst_n             (i_rst_n),
		.i_axi_clk           (i_axi_clk),

		.i_axi_ar_req_en     (axi_ar_req_en),
		.o_axi_ar_ready      (axi_ar_ready),
		.iv_axi_ar_burst_len (axi_ar_burst_len),
		.iv_axi_ar_addr      (axi_ar_addr),

		.o_axi_r_valid       (axi_r_valid),
		.ov_axi_r_data       (axi_r_data),
		.o_axi_r_last        (axi_r_last),

		.iv_m_axi_rid        (iv_m_axi_rid),
		.iv_m_axi_rdata      (iv_m_axi_rdata),
		.iv_m_axi_rresp      (iv_m_axi_rresp),
		.i_m_axi_rlast       (i_m_axi_rlast),
		.i_m_axi_rvalid      (i_m_axi_rvalid),
		.o_m_axi_rready      (o_m_axi_rready),

		.ov_m_axi_arid       (ov_m_axi_arid),
		.ov_m_axi_araddr     (ov_m_axi_araddr),
		.ov_m_axi_arlen      (ov_m_axi_arlen),
		.ov_m_axi_arsize     (ov_m_axi_arsize),
		.ov_m_axi_arburst    (ov_m_axi_arburst),
		.o_m_axi_arlock      (o_m_axi_arlock),
		.ov_m_axi_arcache    (ov_m_axi_arcache),
		.ov_m_axi_arprot     (ov_m_axi_arprot),
		.ov_m_axi_arqos      (ov_m_axi_arqos),
		.o_m_axi_arvalid     (o_m_axi_arvalid),
		.i_m_axi_arready     (i_m_axi_arready)
	);

endmodule

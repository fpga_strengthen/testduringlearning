`timescale 1ns/1ps

// Description
// 产生2FSK信号，两个子载波频率分别为5M（比特0）和10MHz（比特1）
// (1)考虑包络，进一步判断溢出问题;
// (2)计算相位.
module Sync_tb();

//------------------------------------ 信号定义与模块连线 ----------------------------------//

    // 时钟和采样点相关参数
    parameter   CLK_PERIOD = 9.227;
    // 设置的比特数，设置500个Bit
    parameter   Length_Data_Bit = 500;
    // 27.095M/564.48k=48,设定每个bit的采样点数均为48
    parameter   Sample_Per_bit = 48;
    // 50M速率分别进行采样
    // 如果想增加每个周期的点数让波形变得好看，就要提高采样速率
    // 采样点数是48，采样频率是27.095M，则27.095/48 * 7 = 3.951M，即在采样点为48的情况下3.951M（比特0）的波形周期数为7；同理4.516M（比特1）的周期数是8
    parameter   Sample_bit0_Periods = 7, Sample_bit1_Periods = 8;
    // 添加高频分量，以验证滤波器算法；采样点数和比特0、1设置一致，都是50点；50M/50 * 25 = 25M
    //parameter   Sample_High_Freq = 25;
    // 设定包络的最值
    parameter   Max_amp = 1.0, Min_amp = 0.5;

    logic           i_clk_108M;         // 时钟，108M
    logic           sample_clk;         // 采样时钟，27.095M
    logic           rst_n;              // 复位，低有效
    int             i;
    
    // 包络部分
    real    k;                              // 包络的二次项系数
    real    amp [Length_Data_Bit-1:0];      // 按照bit来设定包络
    // 存储初始化bit，设定一个位宽为1、深度为报文长度的存储器,即基带信号
    logic           data_bit    [Length_Data_Bit-1:0];
    logic   [8:0]   cnt_baseband_bit;           // 对基带信号的比特进行计数
    logic   [7:0]   cnt_sample_bit;             // 每个bit的采样点计数器
    // 存储三角函数的浮点数计算结果，存储的数据是浮点数，数据个数为该bit的采样点数
    real            fsk_bit_0   [Sample_Per_bit-1:0];
    real            fsk_bit_1   [Sample_Per_bit-1:0];
    // 暂存三角函数计算结果
    real            tmp_fsk;

    // 生成的的定点2FSK信号
    logic  signed   [11:0]   fsk_sig_fp;
    // 将定点的2FSK信号变为无符号数
    logic           [11:0]   iv_fsk_sig;
    
    // 同步点
    logic   sync_point;

//----------------------------------- 初始化数据 -----------------------------------//
    
    // 时钟初始化
    initial begin
        i_clk_108M = 1'b1;
        rst_n <= 1'b0;
        sample_clk <= 1'b0;
        #(CLK_PERIOD * 3);
        rst_n <= 1'b1;
        iv_fsk_sig = 12'd0;
    end

    // 时钟100M，采样速率设为50M
    always #(CLK_PERIOD/2) i_clk_108M = ~i_clk_108M;

    // 采样时钟
    initial begin 
        forever begin
            // 产生27M的采样时钟
            @(posedge i_clk_108M)  sample_clk = 1;
            @(posedge i_clk_108M)  sample_clk = 0;
            @(posedge i_clk_108M)  sample_clk = 0;
            @(posedge i_clk_108M)  sample_clk = 0;
        end
    end

    // 初始化基带信号
    initial begin 
        for(i=0;i<Length_Data_Bit;i++)
        begin:baseband_bit_init
            data_bit[i] = {$random} % 2;
        end
    end

    // 初始化比特0，1的调制信号，产生标准FSK信号
    // s(t) = sin(2*pi*f*t)
    initial begin:fsk_bit0_modulation
        for(i=0;i<=Sample_Per_bit;i++)begin
            tmp_fsk = $sin(6.283185307179586 * i * Sample_bit0_Periods / Sample_Per_bit);
            fsk_bit_0[i] = tmp_fsk;
            tmp_fsk = $sin(6.283185307179586 * i * Sample_bit1_Periods / Sample_Per_bit);
            fsk_bit_1[i] = tmp_fsk;
        end
    end

    // 初始化包络信号
    initial begin 
        for(i=0;i<Length_Data_Bit;i++)begin
            k = -4.0/(Length_Data_Bit * Length_Data_Bit) * (Max_amp -Min_amp);
            amp[i] = k * ((i - Length_Data_Bit/2.0)*(i - Length_Data_Bit/2.0)) + Max_amp;
        end
    end

//---------------------------------- 打印象限信息 ----------------------------------//
    // 象限信息的ASCII码表示，一个字符需要8位
    // logic   [NUM_WORD*8-1:0]    scope;
    // ASCII码显示的字符数
    // parameter   NUM_WORD = 'd7;
    /*
    assign    quadrant = U_Phase_calu.quadrant;
    // 打印象限信息
    always@(*)begin 
        case(quadrant)
            // 第一象限
            4'b0000 : scope = "1_front";
            4'b0001 : scope = "1_midle";
            4'b0010 : scope = "1_back";
            // 第二象限
            4'b0100 : scope = "2_front";
            4'b0101 : scope = "2_midle";
            4'b0110 : scope = "2_back";
            // 第三象限
            4'b1000 : scope = "3_front";
            4'b1001 : scope = "3_midle";
            4'b1010 : scope = "3_back";
            // 第四象限
            4'b1100 : scope = "4_front";
            4'b1101 : scope = "4_midle";
            4'b1110 : scope = "4_back";
            default : scope = "default";
        endcase
    end
*/

//------------------------------- 产生带包络的FSK信号 ------------------------------//
    // 产生FSK信号采样
    // 初始化比特0、1的调制信号时就已经计算出了两个载波频率的与采样点对应的幅度表，下来根据输入的基带信号对应取出
    // 提前计算的两个表中分别存储着比特0、1各自对应的50个采样点的幅度信息
    initial begin 
        forever begin
            if(rst_n == 1'b0)begin
                tmp_fsk    = 0;
                fsk_sig_fp = 0;
            end
            else begin
                for(cnt_baseband_bit=0;cnt_baseband_bit < Length_Data_Bit;cnt_baseband_bit++)
                begin : random_bit
                    //将每个比特对应的所有采样点幅值查表取出
                    for(cnt_sample_bit = 0;cnt_sample_bit < Sample_Per_bit;cnt_sample_bit++)
                    begin : points_per_bit
                        @(posedge sample_clk)       //在采样时钟有效时才可以执行，否则就会一次性计算完毕
                        if(data_bit[cnt_baseband_bit] == 1'b1)
                            tmp_fsk = fsk_bit_0[cnt_sample_bit];
                        else
                            tmp_fsk = fsk_bit_1[cnt_sample_bit];

                        // 将计算的浮点数做定点化处理
                        // 注意这里是有符号数，最高位是符号位，如果2^11不减1会导致溢出，从+2047变为-2048
                        fsk_sig_fp = amp[cnt_baseband_bit] * tmp_fsk * (2**11 - 1);
                        //if(fsk_sig_fp < -2048)
                        //    fsk_sig_fp = 0;
                        //if(fsk_sig_fp > 2049)
                        //    fsk_sig_fp = 2048;
                        iv_fsk_sig = fsk_sig_fp + (2**11);      // 加直流
                    end
                end
            end
        end
    end

//------------------ test cordic -------------------//
    /*    
    logic   signed  [12:0]  x;
    logic   signed  [12:0]  y;
    logic   finish;

    initial begin
        x = 'b0;
        y = 'b0;
        #100
        @(posedge i_clk_108M);
        #10 @(posedge i_clk_108M) x = 1024;     y = 1024;
        repeat(3)@(posedge i_clk_108M);
        #10 @(posedge i_clk_108M) x = 13'd6;    y = 0;
        repeat(3)@(posedge i_clk_108M);
        #10 @(posedge i_clk_108M) x = 13'd0;    y = 173;        // tan(60°)=sqrt(3)=y/x
        repeat(3)@(posedge i_clk_108M);
        #10 @(posedge i_clk_108M) x = -100;     y = -100;
        repeat(3)@(posedge i_clk_108M);
        #10 @(posedge i_clk_108M) x = 100;    y = -100;
        repeat(3)@(posedge i_clk_108M);
        #10 @(posedge i_clk_108M) x = 0;    y = 0;
    end
*/
//--------------------- end of test ------------------//

//------------------ Top Module ----------------------//
    
    Sync_top U_Sync_top(
        .i_clk_108M  (i_clk_108M),
        .i_clk_27M_en(sample_clk),
        .i_rst_n     (rst_n),
        .iv_fsk_data (iv_fsk_sig),

        .o_sync_point(sync_point)
    );

endmodule
module DAROM_SinCos(
    input   wire        clk,
    input   wire        baseband,
    output  wire [11:0] ov_sin_cos
);
    
    wire    [11:0]  sin_rom_bit1;
    wire    [11:0]  sin_rom_bit0;

    //8k
    DAROM_bit0 U_DAROM_8k(
        .clk            (clk),
        .sin_rom_bit0   (sin_rom_bit0)
    );

    //12.5k
    DAROM_bit1 U_DAROM_12k(
        .clk            (clk),
        .sin_rom_bit1   (sin_rom_bit1)
    );

    //根据基带信号输入的0、1来选择不同的频率
    assign  ov_sin_cos = (baseband == 1'b1) ? sin_rom_bit1 : sin_rom_bit0;
    //assign  ov_sin_cos = sin_rom_bit0;

endmodule
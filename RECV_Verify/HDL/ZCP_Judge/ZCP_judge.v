//--------------------------------------------------------------
// 文件名： ZCP_judge.v
// Author：J.GMson
// RAM仿真需要的库文件：altera_mf.v
//--------------------------------------------------------------
// 求出斜率的过零点进行修正后得到同步点
module ZCP_judge(
    input   wire    i_clk_108M,
    input   wire    i_clk_27M_en,
    input   wire    i_rst_n,
    input   wire    [25:0]  iv_slope,

    output  wire    o_sync_point
);

//---------------------------------------------------//
    
    //斜率要对频偏进行修正后才能判决以得到同步点

//-----------------------RAM 计算8点斜率和-----------------------//
    
    // RAM的读写地址
    reg [3:0]   wr_addr_8p;     // 8点求均值
    reg [3:0]   rd_addr_8p;     // 8p : 8 points
    // RAM采用边读边写的机制,存9个点的斜率
    // wire    signed  [25:0]  slope_delay_9;
    // 8点斜率值求和
    reg     signed  [28:0]  sum_slope_8p;

    // 写地址控制，具体参考时序图
    always@(posedge i_clk_108M or negedge i_rst_n)
    begin 
        if(i_rst_n == 1'b0)
            wr_addr_8p <= 4'd0;
        else if(i_clk_27M_en == 1'b1)begin 
            if(wr_addr_8p == 4'd8)
                wr_addr_8p <= 4'd0;
            else
                wr_addr_8p <= wr_addr_8p + 4'd1;
        end
        else
            wr_addr_8p <= wr_addr_8p;
    end

    // 读地址控制
    always@(posedge i_clk_108M or negedge i_rst_n)
    begin 
        if(i_rst_n == 1'b0)
            rd_addr_8p <= 4'd0;
        else if(i_clk_27M_en == 1'b1)begin 
            if(wr_addr_8p == 4'd6)
                rd_addr_8p <= 4'd0;
            else if(rd_addr_8p == 4'd8)
                rd_addr_8p <= 4'd0;
            else
                rd_addr_8p <= rd_addr_8p + 4'd1;
        end
        else
            rd_addr_8p <= rd_addr_8p;
    end

    wire signed [25:0]  slope_delay_8p;
    
    // 调用双端口RAM实现连续读写，将单个斜率点延迟8点.
    ram_sum_8p U_ram_sum_8p(
	    .clock    (i_clk_27M_en),
	    .data     (iv_slope),
	    .rdaddress(rd_addr_8p),
	    .wraddress(wr_addr_8p),
	    .wren     (1'b1),
	    .q        (slope_delay_8p)
    );

    // 8点求和
    // RAM深度是9,是为了延时8点后求和的时序要求。具体可以参考时序图文档.
    always@(posedge i_clk_108M or negedge i_rst_n)
    begin 
        if(i_rst_n == 1'b0)
            sum_slope_8p <= 29'd0;
        else if(i_clk_27M_en == 1'b1)
            sum_slope_8p <= sum_slope_8p + iv_slope - slope_delay_8p;
        else
            sum_slope_8p <= sum_slope_8p;
    end

//---------------------- 将斜率的8点和再存入RAM实现43点延时 ------------------------//

    // 这部分要分别在48点的前8点和最后8点计算水平斜率值,将16点斜率值之和进行平均
    // 读写地址声明
    reg [5:0]   wr_addr_43p;
    reg [5:0]   rd_addr_43p;

    // 将8点之和延时43点后的斜率值
    wire signed  [28:0]  slope_sum8p_delay_43p;

    // 写地址
    always@(posedge i_clk_108M or negedge i_rst_n)
    begin 
        if(i_rst_n == 1'b0)
            wr_addr_43p <= 6'd0;
        else if(i_clk_27M_en == 1'b1)begin 
            if(wr_addr_43p == 6'd43)
                wr_addr_43p <= 6'd0;
            else
                wr_addr_43p <= wr_addr_43p + 6'd1;
        end
        else
            wr_addr_43p <= wr_addr_43p;
    end

    // 读地址
    always@(posedge i_clk_108M or negedge i_rst_n)
    begin 
        if(i_rst_n == 1'b0)
            rd_addr_43p <= 6'd0;
        else if(i_clk_27M_en == 1'b1)begin 
            if(wr_addr_43p == 6'd41)
                rd_addr_43p <= 6'd0;
            else if(rd_addr_43p == 6'd43)
                rd_addr_43p <= 6'd0;
            else
                rd_addr_43p <= rd_addr_43p + 6'd1;
        end
        else
            rd_addr_43p <= rd_addr_43p;
    end


    // 将8点斜率之和写入RAM延时43个周期,具体原因参考时序图
    ram_slope_delay_43p U_RAM_delay43p(
	    .clock    (i_clk_27M_en),
	    .data     (sum_slope_8p),
	    .rdaddress(rd_addr_43p),
	    .wraddress(wr_addr_43p),
	    .wren     (1'b1),
	    .q        (slope_sum8p_delay_43p)
    );

//------------------------------ 拐点区间判断标志 ---------------------------//
    
    // 计算的首尾8点斜率均值必须要满足一定的范围才可以,具体说明参考Markdown文件说明.
    // 当斜率值在符合要求的范围之内时,给出一个标志信号
    // 这个均值的要求是根据频偏和中心频率偏移的要求定出来的
    reg     slope_right;

    // 这里要对首尾8点同时判断,从而确定出现了w0和w1和拐点区间
    // 数据转换方法：-26'd51969 -> 26'b1111_1111_1111_1111_0011_0100_1111_1111 -> (截取低26位)26'b11_1111_1111_0011_0100_1111_1111(Bin) -> 26'd67_056_895
    always@(posedge i_clk_108M or negedge i_rst_n)
    begin 
        if(i_rst_n == 1'b0)
            slope_right <= 1'b0;
        else if(((sum_slope_8p[28:3] <= 26'd439089)&&(sum_slope_8p[28:3] >= 26'd35593))&&((slope_sum8p_delay_43p[28:3] <= 26'd67_056_896)&&(slope_sum8p_delay_43p[28:3] >= 26'd66_706_620)))
            slope_right <= 1'b1;        // 比特顺序先1后0时,等价于 (35593 <= f1 <= 439089)&&(-402244 <= f0 <= -51969)
        else if(((sum_slope_8p[28:3] >= 26'd66_706_620)&&(sum_slope_8p[28:3] <= 26'd67_056_896))&&((slope_sum8p_delay_43p[28:3] >= 26'd35593)&&(slope_sum8p_delay_43p[28:3] <= 26'd439089)))
            slope_right <= 1'b1;        // 比特顺序先0后1时,等价于 (35593 <= f0 <= 439089)&&(-402244 <= f1 <= -51969)
        else
            slope_right <= 1'b0;
    end

//---------------------- 将延时8点的斜率再存入RAM实现17点延时 ------------------------//

    // 进行17点延时是为了在中间点进行判决,一直延时到中间点,第26点应该为过零点
    reg [4:0]   wr_addr_17p;
    reg [4:0]   rd_addr_17p;

    // 写地址
    always@(posedge i_clk_108M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            wr_addr_17p <= 5'd0;
        else if(i_clk_27M_en == 1'b1)begin 
            if(wr_addr_17p == 5'd16)
                wr_addr_17p <= 5'd0;
            else
                wr_addr_17p <= wr_addr_17p + 5'd1;
        end
        else
            wr_addr_17p <= wr_addr_17p;
    end
    
    // 读地址
    always@(posedge i_clk_108M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            rd_addr_17p <= 5'd0;
        else if(i_clk_27M_en == 1'b1)begin 
            if(wr_addr_17p == 5'd13)
                rd_addr_17p <= 5'd0;
            else if(rd_addr_17p == 5'd16)
                rd_addr_17p <= 5'd0;
            else
                rd_addr_17p <= rd_addr_17p + 5'd1;
        end
        else
            rd_addr_17p <= rd_addr_17p;
    end

    // 先延时8点，再延时17点，共25点
    wire    [25:0]  slope_delay_25p;
    // 将前面延时了8点的斜率再延时17点
    ram_slope_delay17 U_ram_delay17(
	    .clock    (i_clk_27M_en),
	    .data     (slope_delay_8p),
	    .rdaddress(rd_addr_17p),
	    .wraddress(wr_addr_17p),
	    .wren     (1'b1),
	    .q        (slope_delay_25p)
    );

//-------------------------- 根据首尾16点均值计算修正量 -----------------------//
    
    // 将延时43后的8点斜率之和与延时前相加
    // 得到16点和再求均值,作为中心判决点频率的修正量
    reg signed  [29:0]  sum_slope_16p;

    always@(posedge i_clk_108M or negedge i_rst_n)
    begin 
        if(i_rst_n == 1'b0)
            sum_slope_16p <= 30'd0;
        else if(i_clk_27M_en == 1'b1)
            sum_slope_16p <= sum_slope_8p + slope_sum8p_delay_43p;
        else
            sum_slope_16p <= sum_slope_16p;
    end
    
    // 将16点和求均值作为中心点的频率修正量(偏移量)
    reg signed  [25:0]  mid_freq;

    always@(posedge i_clk_108M or negedge i_rst_n)
    begin 
        if(i_rst_n == 1'b0)
            mid_freq <= 26'd0;
        else if(i_clk_27M_en == 1'b1)
            mid_freq <= sum_slope_16p[29:4];        // 截掉低4位,相当于除16
        else
            mid_freq <= mid_freq;
    end

//--------------------------- 修正前的过零判决--------------------------//

    // 将第25点打拍后相邻两点过零判决
    reg signed  [25:0]  slope_delay_26p;

    always@(posedge i_clk_108M or negedge i_rst_n)
    begin 
        if(i_rst_n == 1'b0)
            slope_delay_26p <= 26'd0;
        else if(i_clk_27M_en == 1'b1)
            slope_delay_26p <= slope_delay_25p;
        else
            slope_delay_26p <= slope_delay_26p;
    end

    // 判断是否过零点;如是,则寄存修正量
    reg signed  [25:0]  slope_correction;

    always@(posedge i_clk_108M or negedge i_rst_n)
    begin 
        if(i_rst_n == 1'b0)
            slope_correction <= 26'd0;
        else if(i_clk_27M_en == 1'b1)begin 
            if((slope_delay_25p[25] ^ slope_delay_26p[25]) == 1'b1)
                slope_correction = mid_freq;       // 中间两点异号
            else
                slope_correction = slope_correction;
        end
        else
            slope_correction = slope_correction;
    end

//-------------------------- 将修正量叠加到中间点上 ----------------------------//

    reg signed  [26:0]  slope_delay25p_correction;

    always@(posedge i_clk_108M or negedge i_rst_n)
    begin 
        if(i_rst_n == 1'b0)
            slope_delay25p_correction <= 27'd0;
        else if(i_clk_27M_en == 1'b1)
            slope_delay25p_correction <= slope_delay_25p - slope_correction;
        else
            slope_delay25p_correction <= slope_delay25p_correction;
    end

    // 将修正后的频率再打一拍判断过零点
    reg signed  [26:0]  slope_delay25p_r;

    always@(posedge i_clk_108M or negedge i_rst_n)begin 
        if(i_rst_n == 1'b0)
            slope_delay25p_r <= 27'd0;
        else if(i_clk_27M_en == 1'b1)
            slope_delay25p_r <= slope_delay25p_correction;
        else
            slope_delay25p_r <= slope_delay25p_r;
    end

//-------------------------------- 过零判决 -----------------------------------//
    
    // 判断过零点
    reg sync_point;

    always@(posedge i_clk_108M or negedge i_rst_n)
    begin 
        if(i_rst_n == 1'b0)
            sync_point <= 1'b0;
        else if(i_clk_27M_en == 1'b1)begin 
            if(((slope_delay25p_correction[26]) ^ (slope_delay25p_r[26]) == 1'b1)&&(slope_right == 1'b1))
                sync_point <= 1'b1;
            else
                sync_point <= 1'b0;
        end
        else
            sync_point <= 1'b0;
    end

//----------------------------- 对同步信号取沿 -------------------------------//

    // 可能会有持续拉高的情况,在找到同步点之后,只需要一个脉冲来表征即可
    reg sync_point_r;
    reg sync_point_p;

    always@(posedge i_clk_108M or negedge i_rst_n)
    begin
        if(i_rst_n == 1'b0)begin 
            sync_point_r <= 1'b0;
            sync_point_p <= 1'b0;
        end
        else begin
            sync_point_r <= sync_point;
            sync_point_p <= (sync_point == 1'b1 )&&(sync_point_r == 1'b0);
        end
    end

    assign  o_sync_point = sync_point_p;

endmodule
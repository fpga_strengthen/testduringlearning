`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date: 2024/08/19 20:36:16
// Module Name: crc32_d8
// Description:  完成CRC校验.
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module crc32_d8(
	input wire			i_clk,
	input wire			i_rst_n,
	input wire	[7:0]	iv_crc_din,
	input wire			i_crc_din_vld,
	input wire			i_crc_done,

	output wire	[31:0]	ov_crc_dout
);

	// 反转输入
	wire	[7:0]	crc_din_r;
	reg		[31:0]	crc_dout_r;
	wire	[31:0]	crc_temp;
	
	// 对输入数据进行反转
	assign  crc_din_r = {iv_crc_din[0],iv_crc_din[1],iv_crc_din[2],iv_crc_din[3],
						iv_crc_din[4],iv_crc_din[5],iv_crc_din[6],iv_crc_din[7]};

	// 组合逻辑计算CRC32
	assign		crc_temp[0]  = crc_din_r[6] ^ crc_din_r[0] ^ crc_dout_r[24] ^ crc_dout_r[30];
   	assign		crc_temp[1]  = crc_din_r[7] ^ crc_din_r[6] ^ crc_din_r[1] ^ crc_din_r[0] ^ crc_dout_r[24] ^ crc_dout_r[25] ^ crc_dout_r[30] ^ crc_dout_r[31];
   	assign		crc_temp[2]  = crc_din_r[7] ^ crc_din_r[6] ^ crc_din_r[2] ^ crc_din_r[1] ^ crc_din_r[0] ^ crc_dout_r[24] ^ crc_dout_r[25] ^ crc_dout_r[26] ^ crc_dout_r[30] ^ crc_temp[31];
   	assign		crc_temp[3]  = crc_din_r[7] ^ crc_din_r[3] ^ crc_din_r[2] ^ crc_din_r[1] ^ crc_dout_r[25] ^ crc_dout_r[26] ^ crc_dout_r[27] ^ crc_dout_r[31];
   	assign		crc_temp[4]  = crc_din_r[6] ^ crc_din_r[4] ^ crc_din_r[3] ^ crc_din_r[2] ^ crc_din_r[0] ^ crc_dout_r[24] ^ crc_dout_r[26] ^ crc_dout_r[27] ^ crc_dout_r[28] ^ crc_temp[30];
   	assign		crc_temp[5]  = crc_din_r[7] ^ crc_din_r[6] ^ crc_din_r[5] ^ crc_din_r[4] ^ crc_din_r[3] ^ crc_din_r[1] ^ crc_din_r[0] ^ crc_dout_r[24] ^ crc_dout_r[25] ^ crc_temp[27] ^ crc_temp[28] ^ crc_temp[29] ^ crc_temp[30] ^ crc_temp[31];
   	assign		crc_temp[6]  = crc_din_r[7] ^ crc_din_r[6] ^ crc_din_r[5] ^ crc_din_r[4] ^ crc_din_r[2] ^ crc_din_r[1] ^ crc_dout_r[25] ^ crc_dout_r[26] ^ crc_dout_r[28] ^ crc_temp[29] ^ crc_temp[30] ^ crc_temp[31];
   	assign		crc_temp[7]  = crc_din_r[7] ^ crc_din_r[5] ^ crc_din_r[3] ^ crc_din_r[2] ^ crc_din_r[0] ^ crc_dout_r[24] ^ crc_dout_r[26] ^ crc_dout_r[27] ^ crc_dout_r[29] ^ crc_temp[31];
   	assign		crc_temp[8]  = crc_din_r[4] ^ crc_din_r[3] ^ crc_din_r[1] ^ crc_din_r[0] ^ crc_dout_r[0] ^ crc_dout_r[24] ^ crc_dout_r[25] ^ crc_dout_r[27] ^ crc_dout_r[28];
   	assign		crc_temp[9]  = crc_din_r[5] ^ crc_din_r[4] ^ crc_din_r[2] ^ crc_din_r[1] ^ crc_dout_r[1] ^ crc_dout_r[25] ^ crc_dout_r[26] ^ crc_dout_r[28] ^ crc_dout_r[29];
   	assign		crc_temp[10] = crc_din_r[5] ^ crc_din_r[3] ^ crc_din_r[2] ^ crc_din_r[0] ^ crc_dout_r[2] ^ crc_dout_r[24] ^ crc_dout_r[26] ^ crc_dout_r[27] ^ crc_dout_r[29];
   	assign		crc_temp[11] = crc_din_r[4] ^ crc_din_r[3] ^ crc_din_r[1] ^ crc_din_r[0] ^ crc_dout_r[3] ^ crc_dout_r[24] ^ crc_dout_r[25] ^ crc_dout_r[27] ^ crc_dout_r[28];
   	assign		crc_temp[12] = crc_din_r[6] ^ crc_din_r[5] ^ crc_din_r[4] ^ crc_din_r[2] ^ crc_din_r[1] ^ crc_din_r[0] ^ crc_dout_r[4] ^ crc_dout_r[24] ^ crc_dout_r[25] ^ crc_temp[26] ^ crc_temp[28] ^ crc_temp[29] ^ crc_temp[30];
   	assign		crc_temp[13] = crc_din_r[7] ^ crc_din_r[6] ^ crc_din_r[5] ^ crc_din_r[3] ^ crc_din_r[2] ^ crc_din_r[1] ^ crc_dout_r[5] ^ crc_dout_r[25] ^ crc_dout_r[26] ^ crc_temp[27] ^ crc_temp[29] ^ crc_temp[30] ^ crc_temp[31];
   	assign		crc_temp[14] = crc_din_r[7] ^ crc_din_r[6] ^ crc_din_r[4] ^ crc_din_r[3] ^ crc_din_r[2] ^ crc_dout_r[6] ^ crc_dout_r[26] ^ crc_dout_r[27] ^ crc_dout_r[28] ^ crc_temp[30] ^ crc_temp[31];
   	assign		crc_temp[15] = crc_din_r[7] ^ crc_din_r[5] ^ crc_din_r[4] ^ crc_din_r[3] ^ crc_dout_r[7] ^ crc_dout_r[27] ^ crc_dout_r[28] ^ crc_dout_r[29] ^ crc_dout_r[31];
   	assign		crc_temp[16] = crc_din_r[5] ^ crc_din_r[4] ^ crc_din_r[0] ^ crc_dout_r[8] ^ crc_dout_r[24] ^ crc_dout_r[28] ^ crc_dout_r[29];
   	assign		crc_temp[17] = crc_din_r[6] ^ crc_din_r[5] ^ crc_din_r[1] ^ crc_dout_r[9] ^ crc_dout_r[25] ^ crc_dout_r[29] ^ crc_dout_r[30];
   	assign		crc_temp[18] = crc_din_r[7] ^ crc_din_r[6] ^ crc_din_r[2] ^ crc_dout_r[10] ^ crc_dout_r[26] ^ crc_dout_r[30] ^ crc_dout_r[31];
   	assign		crc_temp[19] = crc_din_r[7] ^ crc_din_r[3] ^ crc_dout_r[11] ^ crc_dout_r[27] ^ crc_dout_r[31];
   	assign		crc_temp[20] = crc_din_r[4] ^ crc_dout_r[12] ^ crc_dout_r[28];
   	assign		crc_temp[21] = crc_din_r[5] ^ crc_dout_r[13] ^ crc_dout_r[29];
   	assign		crc_temp[22] = crc_din_r[0] ^ crc_dout_r[14] ^ crc_dout_r[24];
   	assign		crc_temp[23] = crc_din_r[6] ^ crc_din_r[1] ^ crc_din_r[0] ^ crc_dout_r[15] ^ crc_dout_r[24] ^ crc_dout_r[25] ^ crc_dout_r[30];
   	assign		crc_temp[24] = crc_din_r[7] ^ crc_din_r[2] ^ crc_din_r[1] ^ crc_dout_r[16] ^ crc_dout_r[25] ^ crc_dout_r[26] ^ crc_dout_r[31];
   	assign		crc_temp[25] = crc_din_r[3] ^ crc_din_r[2] ^ crc_dout_r[17] ^ crc_dout_r[26] ^ crc_dout_r[27];
   	assign		crc_temp[26] = crc_din_r[6] ^ crc_din_r[4] ^ crc_din_r[3] ^ crc_din_r[0] ^ crc_dout_r[18] ^ crc_dout_r[24] ^ crc_dout_r[27] ^ crc_dout_r[28] ^ crc_dout_r[30];
   	assign		crc_temp[27] = crc_din_r[7] ^ crc_din_r[5] ^ crc_din_r[4] ^ crc_din_r[1] ^ crc_dout_r[19] ^ crc_dout_r[25] ^ crc_dout_r[28] ^ crc_dout_r[29] ^ crc_dout_r[31];
   	assign		crc_temp[28] = crc_din_r[6] ^ crc_din_r[5] ^ crc_din_r[2] ^ crc_dout_r[20] ^ crc_dout_r[26] ^ crc_dout_r[29] ^ crc_dout_r[30];
   	assign		crc_temp[29] = crc_din_r[7] ^ crc_din_r[6] ^ crc_din_r[3] ^ crc_dout_r[21] ^ crc_dout_r[27] ^ crc_dout_r[30] ^ crc_dout_r[31];
   	assign		crc_temp[30] = crc_din_r[7] ^ crc_din_r[4] ^ crc_dout_r[22] ^ crc_dout_r[28] ^ crc_dout_r[31];
   	assign		crc_temp[31] = crc_din_r[5] ^ crc_dout_r[23] ^ crc_dout_r[29];

   	// CRC初始化与打拍输出
	always @(posedge i_clk) begin
		if(~i_rst_n)
			crc_dout_r <= {32{1'b1}};		// 初始化为全1: 32'hFF_FF_FF_FF
		else if(i_crc_done)
			crc_dout_r <= {32{1'b1}};
		else if(i_crc_din_vld)
			crc_dout_r <= crc_temp;			// 打拍输出
		else
			crc_dout_r <= crc_dout_r;
	end

	assign	ov_crc_dout = ~{crc_dout_r[0] , crc_dout_r[1] , crc_dout_r[2] , crc_dout_r[3] , crc_dout_r[4] , crc_dout_r[5],
							crc_dout_r[6] , crc_dout_r[7] , crc_dout_r[8] , crc_dout_r[9] , crc_dout_r[10], crc_dout_r[11],
							crc_dout_r[12], crc_dout_r[13], crc_dout_r[14], crc_dout_r[15], crc_dout_r[16], crc_dout_r[17],
							crc_dout_r[18], crc_dout_r[19], crc_dout_r[20], crc_dout_r[21], crc_dout_r[22], crc_dout_r[23],
							crc_dout_r[24], crc_dout_r[25], crc_dout_r[26], crc_dout_r[27], crc_dout_r[28], crc_dout_r[29],
							crc_dout_r[30], crc_dout_r[31]};

endmodule

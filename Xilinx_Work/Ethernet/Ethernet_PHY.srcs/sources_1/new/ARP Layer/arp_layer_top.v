`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date: 2024/08/28 00:00:20
// Module Name: arp_layer_top
// Description: 
//  ARP接收模块接收收到的IP和MAC地址并跨时钟域,缓存到动态链表中.
//	当收到IP层发送模块发送来的IP地址时,可以在本地动态链表中查询对应的MAC地址并交给MAC Send模块.
// Revision 0.01 - File Created
//////////////////////////////////////////////////////////////////////////////////


module arp_layer_top #(
	parameter	LOCAL_MAC_ADDR = 48'hFF_FF_FF_FF_FF_FF,
	parameter	LOCAL_IP_ADDR  = {8'd192,8'd168,8'd85,8'd176}
	)(
	input 	wire			i_app_rx_clk,
	input 	wire			i_app_tx_clk,
	//复位
	input 	wire			i_app_rx_rst_n,
	input 	wire			i_app_tx_rst_n,

	input 	wire			i_arp_rx_data_vld,		// 数据有效信号
	input 	wire			i_arp_rx_data_last,		// MAC层数据的最后一个数据的标志
	input 	wire	[7:0]	iv_arp_rx_data,			// 从MAC数据包解析出MAC层数据

	output	wire	[15:0]	ov_arp_tx_length,
	output 	wire			o_arp_tx_data_vld,
	output	wire	[7:0]	ov_arp_tx_data,
	output	wire			o_arp_tx_data_last,

	input	wire			i_rd_arp_list_en,
	input	wire	[31:0]	iv_rd_arp_list_ip,
	output 	wire	[47:0]	ov_rd_arp_list_mac,
	output	wire			o_rd_arp_list_mac_vld
);

	wire	[31:0]	arp_rx_ip_addr;
	wire	[47:0]	arp_rx_mac_addr;
	wire			arp_reply_active;
	wire			arp_rx_src_vld;

	arp_receive #(
		.LOCAL_IP_ADDR (LOCAL_IP_ADDR)
	) U_arp_receive (
		.i_app_rx_clk       (i_app_rx_clk),
		.i_app_tx_clk       (i_app_tx_clk),

		.i_app_rx_rst_n     (i_app_rx_rst_n),
		.i_app_tx_rst_n     (i_app_tx_rst_n),

		.i_arp_rx_data_vld  (i_arp_rx_data_vld),
		.i_arp_rx_data_last (i_arp_rx_data_last),
		.iv_arp_rx_data     (iv_arp_rx_data),

		.o_arp_rx_src_vld   (arp_rx_src_vld),
		.ov_arp_rx_ip_addr  (arp_rx_ip_addr),
		.ov_arp_rx_mac_addr (arp_rx_mac_addr),
		.o_arp_reply_active (arp_reply_active)
	);

	arp_send #(
		.LOCAL_MAC_ADDR (LOCAL_MAC_ADDR),
		.LOCAL_IP_ADDR  (LOCAL_IP_ADDR)
	) U_arp_send (
		.i_app_tx_clk       (i_app_tx_clk),
		.i_app_tx_rst_n     (i_app_tx_rst_n),

		.iv_arp_tx_ip_addr  (arp_rx_ip_addr),
		.iv_arp_tx_mac_addr (arp_rx_mac_addr),
		.i_arp_reply_active (arp_reply_active),

		.ov_arp_tx_length   (ov_arp_tx_length),
		.o_arp_tx_data_vld  (o_arp_tx_data_vld),
		.ov_arp_tx_data     (ov_arp_tx_data),
		.o_arp_tx_data_last (o_arp_tx_data_last)
	);

	arp_dynamic_list U_arp_dynamic_list(
		.i_clk             (i_app_tx_clk),
		.i_rst_n           (i_app_tx_rst_n),

		.i_wr_arp_en       (arp_rx_src_vld),
		.iv_wr_ip_addr     (arp_rx_ip_addr),
		.iv_wr_mac_addr    (arp_rx_mac_addr),

		.i_rd_arp_en       (i_rd_arp_list_en),
		.iv_rd_ip_addr     (iv_rd_arp_list_ip),

		.ov_rd_mac_addr    (ov_rd_arp_list_mac),
		.o_rd_mac_addr_vld (o_rd_arp_list_mac_vld)
	);

endmodule

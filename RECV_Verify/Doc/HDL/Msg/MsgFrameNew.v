/*=======================================================================================*\
  Filename    : msg_frame.v
  Author      : 陈献彬
  time        ：2016.07.05
  Description : 本模块主要完成组帧向2C8发送的功能，同时要注意跨时钟域处理            
               
  Called by   : 
  Revision History : 
  Author              Date                  Version          Description
  郭庆阳              20161111              V1.1.0           增加1024bit的同步数据
  郭庆阳              20161119              V1.2.0       crc32校验错误之后将crc16生成结果取反
  郭庆阳              20161215              V1.3.0          将本模块的主时钟由27M换成81M                                                                                                                   
  Email       :chenxianbin_bupt@163.con
  Company     :

\*========================================================================================*/
module MsgFrameNew(
    i_clk_81M,
    i_rst_n,

    //译码后的报文接口          
    i_msg_trans_ready,   //译码模块报文准备完成的白标志，维持一段时间的高电平
    iv_msg_data,         /*从译码模块读出的译码完成的数据，8bit一组共832bit（最后两个bit
                            为无效0，有效数据为830）同时，还有4x8bit的crc校验码跟在尾部*/                                                                
    o_rd_msg_en,         //本模块主动向译码模块读报文的使能信号
    o_rd_msg_sck,        //本模块主动向译码模块读报文的字节同步时钟
    //同步后的数据接口
    i_telegram_trans_ready,//译码模块同步数据准备完成标志，维持一段时间高电平
    iv_telegram_data,      //同步完成的数据（1024bit）           
    o_rd_telegram_en,    //本模块主动向译码模块读同步数据的使能信号
    o_rd_telegram_sck,   //本模块主动向译码模块读同步数据的字节同步时钟
    // 写到 CAN 发送 FIFO
    o_tx_end,
    o_tx_en,
    o_tx_data
    //EP2C8 SPI接口                     
    // o_spi_cs,            //spi使能
    // o_spi_clk,           //发送给spi接收端使用的时钟
    // o_spi_data           //spi发送数据
);

input i_clk_81M;
input i_rst_n;

input i_msg_trans_ready;
input [7:0]iv_msg_data;
output reg o_rd_msg_en;
output reg o_rd_msg_sck;

input i_telegram_trans_ready;
input [7:0]iv_telegram_data;
output reg o_rd_telegram_en;
output reg o_rd_telegram_sck;

output o_tx_end;
output o_tx_en;
output [7:0] o_tx_data;
// output o_spi_cs;
// output o_spi_clk;
// output o_spi_data;
//------------------------------------------------------------//
//crc校验的函数
`include "../Crc/Crc32_D8_Gene.v"
//------------------------------------------------------------//

reg telegram_trans_ready_r1;
reg telegram_trans_ready_p;
reg msg_trans_ready_r1;
reg msg_trans_ready_p;
reg [1:0]cnt_ready;

reg [31:0]crc32_rcv;    //从transform读取的crc校验结果
reg [31:0]LFSR_CRC32_D8;//本模块自己计算的crc32
reg crc_compare_result; //比较错误时拉高
reg crc_ctr;
reg crc_clr;
wire [15:0]checksum16;

reg [7:0]cnt_40;        //字节计数器，主要用于产生spi发送字节同步时钟
reg [7:0]cnt_Byte;      //计数发送了多少个字节
// reg spi_en;       //帧结束的标志
reg tx_end;
reg tx_en;
reg spi_sck;            //spi发送每个字节时的开始脉冲
reg [7:0] spi_data;      //组帧后的数据

// wire spi_tx_ok;
reg spi_tx_ok;

//-----------------------------------------------------------------------------//
// 临时
// assign spi_tx_ok = spi_sck;
assign o_tx_en = tx_en;
assign o_tx_data = spi_data;
assign o_tx_end = tx_end;

always @(posedge i_clk_81M or negedge i_rst_n) begin
    if(!i_rst_n)
        tx_en <= 1'b0;
    else
        tx_en <= spi_sck;
end

//----------------------------------------------------------------------------//
//对i_telegram_trans_ready取沿 
always@( posedge i_clk_81M or negedge i_rst_n )
begin
    if( !i_rst_n )begin
        telegram_trans_ready_r1 <= 1'b0;        
        telegram_trans_ready_p  <= 1'b0;
    end
    else begin
        telegram_trans_ready_r1 <= i_telegram_trans_ready;        
        telegram_trans_ready_p  <= i_telegram_trans_ready & ~telegram_trans_ready_r1;      
    end    
end

//对i_msg_trans_ready取沿
always@( posedge i_clk_81M or negedge i_rst_n )
begin
    if( !i_rst_n )begin
        msg_trans_ready_r1 <= 1'b0;
        msg_trans_ready_p  <= 1'b0;
    end
    else begin
        msg_trans_ready_r1 <= i_msg_trans_ready;
        msg_trans_ready_p  <= i_msg_trans_ready & ~msg_trans_ready_r1;      
    end    
end

always@( posedge i_clk_81M or negedge i_rst_n )
begin
    if( !i_rst_n )
        cnt_ready <= 2'd0;
    else if(cnt_ready == 2'd2)
        cnt_ready <= 2'd0;
    else if(telegram_trans_ready_p)//一定是它先来，否则就是异常
        cnt_ready <= 2'd1;
    else if(msg_trans_ready_p && cnt_ready == 2'd1)
        cnt_ready <= 2'd2;
    else
        cnt_ready <= cnt_ready;
end

// /*------------------------------------------------------------------*\ 
//    一条正常的报文帧的格式为:（数字为16进制）共239字节

//     0     1         2~3         4~107  108~235     236~237       238
//    C0 + 类型 + 长度（2个字节） + Byte（E8h字节） + CRC码（2字节） + C0
//                               (104+128)232个有效数据
//    C0 +  01  +    00 + E8      + Byte（E8h字节） + CRC码（2字节） + C0
// \*------------------------------------------------------------------*/
/*------------------------------------------------------------------*\ 
   更新(20201119): 一条正常的报文帧的格式为:（数字为16进制）共240字节(can接口发送)

    0            1~5      6~109        110~237     238~239
   SEQ_NUM(1B) + 预留 + Data(104B) +  Data(128B) + CRC码(2B)
                                232个有效数据
     1~255     +  00(5B) + Data(104B)  + Data(128B) + CRC码(2B)
\*------------------------------------------------------------------*/
always @(posedge i_clk_81M or negedge i_rst_n) begin
    if(~i_rst_n)
        spi_tx_ok <= 1'b0;
    else if(cnt_40 == 6'd40)
        spi_tx_ok <= 1'b1;
    else
        spi_tx_ok <= 1'b0;
end

always@( posedge i_clk_81M or negedge i_rst_n )
begin
    if( !i_rst_n )
        cnt_Byte <= 8'd0;
    else if( spi_tx_ok && cnt_Byte == 8'd240 )  //240字节发送完毕，计数器清零
        cnt_Byte <= 8'd0;
    else if( (cnt_ready == 2'd2) )
        cnt_Byte <= 8'd1;
    else if( spi_tx_ok )
        cnt_Byte <= cnt_Byte + 8'd1;
    else
        cnt_Byte <= cnt_Byte;     
end 

//取上层模块数据的计数器 我们上层模块读数据时，上层模块不会马上把数据给出来
//所以，需要用该计数器控制采样数据
always@( posedge i_clk_81M or negedge i_rst_n )
begin
    if( !i_rst_n ) 
        cnt_40 <= 6'd0;
    else if(spi_tx_ok && cnt_Byte == 8'd240) //一帧报文发送完毕，清0
        cnt_40 <= 6'd0;
    else if(cnt_40 == 6'd40)
        cnt_40 <= 6'd0;
    else if( (cnt_ready == 2'd2) )
        cnt_40 <= 6'd1;
    else if(spi_tx_ok)
        cnt_40 <= 6'd1;
    else if(cnt_40)
        cnt_40 <= cnt_40 + 6'd1;
    else 
        cnt_40 <= cnt_40;     
end

//spi的字节开始脉冲信号
always@( posedge i_clk_81M or negedge i_rst_n )
begin
    if( !i_rst_n )
        spi_sck <= 1'd0;
    else if( (cnt_Byte >= 8'd1 && cnt_Byte <= 8'd240) && cnt_40 == 8'd7 )   //在cnt_40 == 8'd7时采样
        spi_sck <= 1'd1;
    else
        spi_sck <= 1'd0;
end

//240B数据写入FIFO完成信号
always @(posedge i_clk_81M or negedge i_rst_n) begin
    if(!i_rst_n)
        tx_end <= 1'b0;
    else if(spi_tx_ok && cnt_Byte == 8'd240)
        tx_end <= 1'b1;
    else
        tx_end <= 1'b0;
end

//产生报文首部的SEQ_NUM: 应答器报文逻辑编号, 接收板每成功译出一条应答器报文则加一, 取值范围为1~255
reg [7:0] seq_num;
always @(posedge i_clk_81M or negedge i_rst_n) begin
    if(!i_rst_n)
        seq_num <= 8'd0;
    else if((cnt_ready == 2'd2) && (&seq_num == 1'b1))
        seq_num <= 8'd1;
    else if(cnt_ready == 2'd2)
        seq_num <= seq_num + 1'b1;
    else
        seq_num <= seq_num;
end

//------------------------------------读报文-----------------------------------//
//产生从译码模块读报文的读使能和读时钟
always@( posedge i_clk_81M or negedge i_rst_n )
begin
    if( !i_rst_n )
        o_rd_msg_en <= 1'b0;
    else if( cnt_ready == 2'd2 )//检测到译码模块ready信号,或者检测到检错重传信号，读使能拉高
        o_rd_msg_en <= 1'b1;
    else if( cnt_Byte== 8'd111 )//报文读完即拉低
        o_rd_msg_en <= 1'b0;
    else
        o_rd_msg_en <= o_rd_msg_en;
end
//从transform读数据的同步时钟，读出的数据会比o_rd_msg_sck延后5个81M时钟
always@( posedge i_clk_81M or negedge i_rst_n )
begin
    if( !i_rst_n ) 
        o_rd_msg_sck <= 1'd0;
    else if( cnt_40 == 8'd1 && cnt_Byte >= 8'd7 && cnt_Byte <= 8'd110 )//在发送当前字节时，要做好读下1字节的工作
        o_rd_msg_sck <= 1'd1;
    else if( ( cnt_40 == 8'd7 || cnt_40 == 8'd14 || cnt_40 == 8'd21 || cnt_40 == 8'd28 ) && (cnt_Byte== 8'd110) )//读crc32校验结果,108时读的是报文的crc，236时读的是同步后数据的crc
        o_rd_msg_sck <= 1'd1;
    else
        o_rd_msg_sck <= 1'd0;
end 
//-------------------------------------------------------------------//
//对输入数据crc校验，根据校验值判断输入数据正确与否
always @(negedge i_rst_n or posedge i_clk_81M) 
begin
    if (!i_rst_n)
        LFSR_CRC32_D8 <= 32'b0;
    else if( msg_trans_ready_p )//当新一组报文到达时，或者检测到检错重传信号，将寄存器组清空
        LFSR_CRC32_D8 <= 32'b0;
    else if( cnt_Byte == 8'd110 && cnt_40 == 8'd40 )//为计算同步数据crc32预先清0
        LFSR_CRC32_D8 <= 32'b0;
    else if( cnt_Byte >= 8'd7 && cnt_Byte <= 8'd110 && spi_sck ) //104个字节进行crc校验，幻数不方便详细说明，参见时序图即可理解
        LFSR_CRC32_D8 <= getCRC32_D8(iv_msg_data,LFSR_CRC32_D8);
    else if( cnt_Byte >= 8'd111 && cnt_Byte <= 8'd238 && spi_sck ) //128个字节进行crc校验
        LFSR_CRC32_D8 <= getCRC32_D8(iv_telegram_data,LFSR_CRC32_D8);
    else 
        LFSR_CRC32_D8 <= LFSR_CRC32_D8;     
end 

always @(negedge i_rst_n or posedge i_clk_81M) 
begin
    if (!i_rst_n)
        crc32_rcv <= 32'b0; 
    else if( msg_trans_ready_p )
        crc32_rcv <= 32'b0;
    else if( cnt_40 == 8'd14 && (cnt_Byte == 8'd110) )
        crc32_rcv[31:24] <= iv_msg_data;  
    else if( cnt_40 == 8'd21 && (cnt_Byte == 8'd110) )
        crc32_rcv[23:16] <= iv_msg_data; 
    else if( cnt_40 == 8'd28 && (cnt_Byte == 8'd110) )
        crc32_rcv[15:8] <= iv_msg_data; 
    else if( cnt_40 == 8'd35 && (cnt_Byte == 8'd110) )
        crc32_rcv[7:0] <= iv_msg_data; 
    else if(cnt_Byte == 8'd112)//接收同步数据的CRC前清0
        crc32_rcv <= 32'b0;
    else if( cnt_40 == 8'd14 && (cnt_Byte == 8'd238) )
        crc32_rcv[31:24] <= iv_telegram_data;  
    else if( cnt_40 == 8'd21 && (cnt_Byte == 8'd238) )
        crc32_rcv[23:16] <= iv_telegram_data; 
    else if( cnt_40 == 8'd28 && (cnt_Byte == 8'd238) )
        crc32_rcv[15:8] <= iv_telegram_data; 
    else if( cnt_40 == 8'd35 && (cnt_Byte == 8'd238) )
        crc32_rcv[7:0] <= iv_telegram_data; 
    else
        crc32_rcv <= crc32_rcv;
end

always @(negedge i_rst_n or posedge i_clk_81M) 
begin
    if (!i_rst_n)
        crc_compare_result <= 1'd0;
    else if( msg_trans_ready_p )//读取新的报文，或者检测到检错重传信号时，置0
        crc_compare_result <= 1'd0;
    else if( cnt_Byte == 8'd110 && cnt_40 == 8'd40 && LFSR_CRC32_D8 != crc32_rcv )//发送来的校验码和本地计算的校验码比对，对比错误置高
        crc_compare_result <= 1'd1;
    else if( cnt_Byte == 8'd238 && cnt_40 == 8'd40 && LFSR_CRC32_D8 != crc32_rcv )//发送来的校验码和本地计算的校验码比对，对比错误置高
        crc_compare_result <= 1'd1;
    else 
        crc_compare_result <= crc_compare_result;
end 

//----------------------------------读同步数据--------------------------------//

//产生从译码模块读同步数据的读使能
always@( posedge i_clk_81M or negedge i_rst_n )
begin
    if( !i_rst_n )
        o_rd_telegram_en <= 1'b0;
    else if(cnt_40 == 8'd30 && cnt_Byte== 8'd110)//在读完报文数据开始读同步数据
        o_rd_telegram_en <= 1'b1;
    else if(cnt_Byte== 8'd239) //整个帧发送完毕，读使能拉低
        o_rd_telegram_en <= 1'b0;
    else
        o_rd_telegram_en <= o_rd_telegram_en;
end

//读时钟
 always@( posedge i_clk_81M or negedge i_rst_n )
begin
    if( !i_rst_n ) 
        o_rd_telegram_sck <= 1'd0;
    else if( cnt_40 == 8'd1 && cnt_Byte >= 8'd111 && cnt_Byte <= 8'd238)
        o_rd_telegram_sck <= 1'd1;
    else if( ( cnt_40 == 8'd7 || cnt_40 == 8'd14 || cnt_40 == 8'd21 || cnt_40 == 8'd28 ) && (cnt_Byte== 8'd238) )//读crc32校验结果,238时读的是同步后数据的crc
        o_rd_telegram_sck <= 1'd1;
    else
        o_rd_telegram_sck <= 1'd0;
end   
//-----------------------------------------------------------------------------//
//对组帧数据crc校验, 校验内容为0-237字节
always@( posedge i_clk_81M or negedge i_rst_n )
begin
    if( !i_rst_n )begin
        crc_ctr <= 1'd0;
        crc_clr <= 1'd0;
    end
    else if( cnt_ready == 2'd2 )begin //读取新的报文，或者检测到检错重传信号时，置0
        crc_ctr <= 1'd0;
        crc_clr <= 1'd0;
    end
    else if( 8'd1 <= cnt_Byte && cnt_Byte <= 8'd238 && spi_sck )begin//校验 类型 + 长度（2个字节） + Byte（232字节）
        crc_ctr <= 1'd1;
        crc_clr <= 1'd0;
    end
    else if( cnt_Byte == 8'd240 && spi_sck)begin//校验结束且校验码发送完毕，校验结果清零
        crc_ctr <= 1'd0;
        crc_clr <= 1'd1;
    end
    else begin
        crc_ctr <= 1'd0;
        crc_clr <= 1'd0;
    end
end

Crc16 u_Crc16(
        .i_clk_27M(i_clk_81M),//主时钟
        .i_rst_n(i_rst_n),//复位
        .iv_data(spi_data),//外部输入信号
        .i_crc_ctr(crc_ctr),//控制信号，高点平时进行校验
        .i_crc_clr(crc_clr),//清零信号
        .ov_checksum16(checksum16)//校验输出
        );                  
//---------------------------------------------------------------//
//对数据进行组帧
always@( posedge i_clk_81M or negedge i_rst_n )
begin
    if( !i_rst_n )
        spi_data <= 8'd0;
    else if( cnt_Byte == 8'd1 && spi_sck )//1~255
        spi_data <= seq_num;
    else if( 8'd2 <= cnt_Byte && cnt_Byte <= 8'd6 && spi_sck )//预留
        spi_data <= 8'd0;
    else if( 8'd7 <= cnt_Byte && cnt_Byte <= 8'd110 && spi_sck )//104字节报文
        spi_data <= iv_msg_data;
    else if( 8'd111 <= cnt_Byte && cnt_Byte <= 8'd238 && spi_sck )//128字节同步后的源数据
        spi_data <= iv_telegram_data;
    else if( cnt_Byte == 8'd239 && spi_sck && crc_compare_result )//若crc32比对错误将CRC16生成结果取反
        spi_data <= ~checksum16[15:8];
    else if( cnt_Byte == 8'd240 && spi_sck && crc_compare_result )//若crc32比对错误将CRC16生成结果取反
        spi_data <= ~checksum16[7:0];
    else if( cnt_Byte == 8'd239 && spi_sck )//16比特CRC高字节
        spi_data <= checksum16[15:8];
    else if( cnt_Byte == 8'd240 && spi_sck )//16比特CRC低字节
        spi_data <= checksum16[7:0];
    else
        spi_data <= spi_data;     
end 


endmodule
%FSK解调判决仿真
%目的检测频偏对判决错误的影响

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%FSK调制%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc
format long
close all
clear all

%% %标准条件下主要参数
f0_true = 3.951 ;
f1_true = 4.516 ;         %比特0和比特1的频点，单位MHz
f_mid =4.234; % (3.951+4.516) / 2
sample_rate_true = 27.095;% 标准采样速率，单位MHz
BPS_data_true = 0.56448;  % 标准数据传输速率，单位MHz
sum_bits = 100;%比特数
Hd = LP_6M;
% FSK数据的关键参数
FSK_freq_table = [  4.51624,    3.95176;...
    4.4964832,  3.9320032;...
    4.5359968,  3.9715168;...
    4.34124,    3.77676;...
    4.3214832,  3.7570032;...
    4.3609968,  3.7965168;...
    4.69124,    4.12676;...
    4.6714832,  4.1070032;...
    4.7109968,  4.1465168];%单位MHz

%%%%%%%%% 产生相位连续的FSK调制信号进行ADC采样后作为输入 %%%%%%%%%%%%%
src_data =randint(1,sum_bits);%以比特向量方式输入的待调制数据，随机产生
static_lenth_x_axis = 48 * sum_bits + 50;

% 构造本地载波
S0 = cos( f0_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);
S1 = cos( f1_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);
S0_Q = sin(f0_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);%%移相90°
S1_Q = sin(f1_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);%%移相90°

%% 比较特殊频率处理抵消效果
for freq_num = 1:size(FSK_freq_table,1)
    %产生FSK调制信号
    BPS_data = BPS_data_true;%数据传输速率，单位MHz
    sample_rate =  sample_rate_true;%采样速率，单位MHz
    FSK_data  = FSK_mod(src_data,FSK_freq_table(freq_num,2),FSK_freq_table(freq_num,1),sample_rate,BPS_data);
    
    %对输入数据进行定标
    FSK_data = FSK_data  - 2048;%FixPointNum(FSK_data  - 2048, 12, 0);%字长12bit，精度0位小数 + FSK_data_noise
    length_fsk_data = length(FSK_data);
    
    %% 相关解调后求平方和法求同步点
    %------------混频并滤除高频信号-----------------------
    FSK_data_S0     = filter(Hd,FSK_data .* S0(1:length_fsk_data));
    FSK_data_S0_Q   = filter(Hd,FSK_data .* S0_Q(1:length_fsk_data));
    FSK_data_S1     = filter(Hd,FSK_data .* S1(1:length_fsk_data));
    FSK_data_S1_Q   = filter(Hd,FSK_data .* S1_Q(1:length_fsk_data));
    
    %------------相干解调过程,24个点求和，再除以24--------------
    for i =1:length_fsk_data-23
        sum_bit0_point(i) = ( (sum(FSK_data_S0(i:i+23)))^2 + (sum(FSK_data_S0_Q(i:i+23)))^2 )./24  ;
        sum_bit1_point(i) = ( (sum(FSK_data_S1(i:i+23)))^2 + (sum(FSK_data_S1_Q(i:i+23)))^2 )./24  ;
    end %for
    
    sum_bit0_mins_sum_bit0 = sum_bit0_point(1:end-24) - sum_bit0_point(25:end);
    sum_bit1_mins_sum_bit1 = sum_bit1_point(1:end-24) - sum_bit1_point(25:end);
    sum_bit0_mins_sum_bit1 = sum_bit0_mins_sum_bit0 - sum_bit1_mins_sum_bit1;
    
    
    %% -----------求同步点--------------------------
%     %曲线拟合法求同步点
%     x = [1:1:length(sum_bit0_mins_sum_bit1)];
%     y = sum_bit0_mins_sum_bit1;
%     % x = -10:0.2:10;
%     % y = 5*exp(-((x)/4).^2)+randn(size(x))*0.1; 
%     %f = fittype('a*exp(-((x-b)/c)^2)');
%     for ii = 1:length(x)
%         x_5 = x(ii:ii+4);
%         y_5 = y(ii:ii+4);
%         % [cfun,gof] = fit(x_5(:),y_5(:),f);
%         % yy = cfun.a*exp(-((x_5-cfun.b)/cfun.c).^2);
%         % hold on;plot(x(ii:ii+4),yy,'r','LineWidth',2);
%         A = polyfit(x_5,y_5,2);%最小二乘法二次曲线拟合
%         yy = polyval(A,x_5);
%         yy_diff1 = diff(yy)*10;
%         yy_diff2 = diff(yy_diff1)*10;
%         if((yy_diff1 == 0) & (yy_diff2 ~= 0))
%             sync_p(i) = 1;
%         else
%             sync_p(i) = 0;
%         end
%         figure(2),
%         plot(x(ii:ii+4),y_5,'r')
%         hold on,
%         plot(x(ii:ii+4),yy,'g');
%         hold on,
%         plot(x(ii:ii+3),yy_diff1,'b');
%         plot(x(ii:ii+2),yy_diff2,'m');
%         legend('待拟合数据','拟合曲线','拟合曲线的1阶导','拟合曲线的2阶导');
%     end
%     plot(sync_p,'k');
    
%     a0 = [1 1 1];
%     options = optimset('lsqnonlin');
%     options.TolX = 0.01;
%     options.Display = 'off';
%     for i = 1:length(x) - 4
%         x_5 = x(i:i+4);
%         y_5 = y(i:i+4);
%         a = lsqnonlin(@twoexps3,a0,[],[],options,x_5,y_5);
%         y_set = a(1).*(x_5.^2) + a(2).*x_5 + a(3);
%         a1=num2str(a(1));
%         a2=num2str(a(2));
%         a3=num2str(a(3));
%         char_y_set = ['yset=',a1,'*(x^2 ) + ',a2,'*x + ',a3];
%         disp(['估计方程',char_y_set]);
%         figure(1),
%         plot(x(i:i+4),y_5,'r');
%         hold on,
%         plot(x(i:i+4),y_set+1,'g');
%         hold on,
%         plot(x(i:i+3),diff(y_set)*10,'b');
%         plot(x(i:i+2),diff(diff(y_set)*10)*10,'m');
%         % pause;
%         % close;
%     end
    
    
    
    
    % 通过比较相邻几个点的大小求极大值和极小值，从而判断出同步点
    for i =2:length(sum_bit0_mins_sum_bit1)-1
        if(  (sum_bit0_mins_sum_bit1(i) > 10e5) && (sum_bit0_mins_sum_bit1(i) > sum_bit0_mins_sum_bit1(i+1)) && (sum_bit0_mins_sum_bit1(i) > sum_bit0_mins_sum_bit1(i-1)) )
            sync_point(i) = 1  ;
        elseif( (sum_bit0_mins_sum_bit1(i) < -10e5) && (sum_bit0_mins_sum_bit1(i) < sum_bit0_mins_sum_bit1(i+1)) && (sum_bit0_mins_sum_bit1(i) < sum_bit0_mins_sum_bit1(i-1)) )
            sync_point(i) = 1  ;
        else
            sync_point(i) = 0  ;
        end %if
    end %for
    
    %% 实验室算法求拐点
    % --------------------------------------------------------
    %理论上的Hilbert变换
    hilbert_fsk = hilbert(FSK_data);  %求出理论上的解析信号
    
    %----------------相位归一化后求arctan值----------------------------------------
    %理论相位
    atan_theroy = atan2(imag(hilbert_fsk),real(hilbert_fsk));
    
    %------------------对中心频率和2pi进行修正--------------------------------------
    arctan_fsk = hilbert_fsk .* exp(-j*[1:length(hilbert_fsk)]*2*pi*4.234/27.095);%处理中心频率；
    arctan_fsk = angle(arctan_fsk); %求出解析信号所对应的相位
    arctan_fsk = unwrap(arctan_fsk,pi);  %对相位进行2pi的修正
    
    %-----------------最小二乘法---------------------------------------
    %用理论计算的相位进行理论上的最小二乘法
    [ intersection_slop_decisioned,slope_forward_theroy] = Least_Square_Method_theroy_1( arctan_fsk )
    
    %% 画图
    figure(1),
    hold off,
    plot(sum_bit0_point(13:end),'r');    hold on,
    plot(sum_bit1_point(13:end),'g');
    plot(sum_bit0_mins_sum_bit1);
    plot(sync_point*30e6,'m');
    plot(intersection_slop_decisioned(12:end)*10e6,'k');
    legend('f0混频滤波24点平方和','f1混频滤波24点平方和','相差24点后相减结果','相干解调求出的同步点','希尔伯特折线法求出的同步点');
    title(strcat('FSK信号频点' ,num2str(FSK_freq_table(freq_num,2)-3.951),' , ',num2str(FSK_freq_table(freq_num,1)-4.516), 'Mhz'));
    
    pause;
end

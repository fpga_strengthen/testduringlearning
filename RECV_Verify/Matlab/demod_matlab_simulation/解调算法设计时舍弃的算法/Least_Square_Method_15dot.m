function [ hilbert_data_out_15dot ] = Least_Square_Method_15dot( angle_fit )
%15点的最小二乘法，复用中间的第八个点，得到交点的横坐标的分子与分母
%   输入为修正后的相位
%   输出为拐点
%%
%取N=15个点用于之后进行8点最小二乘法
y15 = Shift_anglefit( angle_fit ,1 );
y14 = Shift_anglefit( angle_fit ,2 );
y13 = Shift_anglefit( angle_fit ,3 );
y12 = Shift_anglefit( angle_fit ,4 );
y11 = Shift_anglefit( angle_fit ,5 );
y10 = Shift_anglefit( angle_fit ,6 );
y9 = Shift_anglefit( angle_fit ,7 );
y8 = Shift_anglefit( angle_fit ,8 );
y7   = Shift_anglefit( angle_fit ,9 );
y6   = Shift_anglefit( angle_fit ,10 );
y5   = Shift_anglefit( angle_fit ,11 );
y4   = Shift_anglefit( angle_fit ,12 );
y3   = Shift_anglefit( angle_fit ,13 );
y2   = Shift_anglefit( angle_fit ,14 );
y1   = Shift_anglefit( angle_fit ,15 );

%%
%乘对应常数，用于求拐点斜率，为拐点的横坐标分母
%前斜率
num_8_reduce_1_x7   = FixPointNum((y8-y1)*7, 27, 0);
num_7_reduce_2_x5   = FixPointNum((y7-y2)*5, 27, 0);
num_6_reduce_3_x3   = FixPointNum((y6-y3)*3, 27, 0);
num_5_reduce_4_x1   = FixPointNum((y5-y4)*1, 27, 0);
slope_forward =FixPointNum(num_8_reduce_1_x7 + num_7_reduce_2_x5 + num_6_reduce_3_x3 + num_5_reduce_4_x1, 32, 0);

%后斜率
num_15_reduce_8_x7 = FixPointNum((y15-y8)*7, 27, 0);
num_14_reduce_9_x5 = FixPointNum((y14-y9)*5, 27, 0);
num_13_reduce_10_x3 = FixPointNum((y13-y10)*3, 27, 0);
num_12_reduce_11_x1 = FixPointNum((y12-y11)*1, 27, 0);
slope_after = FixPointNum(num_15_reduce_8_x7 + num_14_reduce_9_x5 + num_13_reduce_10_x3 + num_12_reduce_11_x1, 32, 0);

%交点横坐标的分母
reference_denominator = FixPointNum(slope_after - slope_forward, 33, 0);

%%
%乘对应常数，用于求拐点截距，为拐点的横坐标分子
num_7_reduce_9_x33 = FixPointNum((y7-y9)*33, 30, 0);
num_6_reduce_10_x24 = FixPointNum((y6-y10)*24, 30, 0);
num_5_reduce_11_x15 = FixPointNum((y5-y11)*15, 30, 0);
num_4_reduce_12_x6  = FixPointNum((y4-y12)*6, 30, 0);
num_3_reduce_13_x3  = FixPointNum((y3-y13)*3, 30, 0);
num_2_reduce_14_x12 = FixPointNum((y2-y14)*12, 30, 0);
num_1_reduce_15_x21 = FixPointNum((y1-y15)*21, 30, 0);
%交点横坐标的分子
reference_numerator = FixPointNum(num_7_reduce_9_x33 + num_6_reduce_10_x24 + num_5_reduce_11_x15 + num_4_reduce_12_x6 - num_3_reduce_13_x3 - num_2_reduce_14_x12 - num_1_reduce_15_x21, 33, 0);

%%
reference_numerator_x2 = FixPointNum(reference_numerator*2,34, 0);
reference_denominator_x3 = FixPointNum(reference_numerator*3,34, 0);
%对分子和分母取绝对值
abs_reference_numerator = FixPointNum(abs(reference_numerator_x2),35, 0);
abs_reference_denominator =  FixPointNum(abs(reference_denominator_x3),35, 0);

%根据前后斜率的值，判断对应的采样点是否是拐点
hilbert_data_out_15dot = zeros(1,length(abs_reference_denominator));
for i= 2:length(abs_reference_denominator)
    if((abs_reference_numerator(i) ) < (abs_reference_denominator(i)))  %拐点对应的截距交点的横坐标的值小于1
        %根据斜率前后正负的相对值来给出希尔伯特解调的结果
        if( (-slope_forward(i))<12700 && (-slope_forward(i))>2100 && slope_after(i)>2100 && slope_after(i)<12700 )
            hilbert_data_out_15dot(i) = ~hilbert_data_out_15dot(i-1);
        elseif( (-slope_after(i))<12700 && (-slope_after(i))>2100 && slope_forward(i)>2100 && slope_forward(i)<12700 )
            hilbert_data_out_15dot(i) = ~hilbert_data_out_15dot(i-1);
        else
            hilbert_data_out_15dot(i) = hilbert_data_out_15dot(i-1);
        end %if
    else
        hilbert_data_out_15dot(i) = hilbert_data_out_15dot(i-1);
    end %if
end %for


%%算法修改前后求得的拐点对比
hilbert_data_out_15dot = xor([hilbert_data_out_15dot,0],[0,hilbert_data_out_15dot]);
figure(9),
% plot(hilbert_data_out_compare),title('算法修改前后拐点对比（异或结果）');
% hold on,


plot(hilbert_data_out_15dot,'r');

%%画出拐点判定过程中的判决条件
for i= 2:length(abs_reference_denominator)
  if(abs_reference_denominator(i) - abs_reference_numerator(i) > 0 )
      flag(i) = 1*10^5;
      up_freq1(i) = 12700;
      down_freq1(i) = 2100;
      up_freq2(i) = -2100;
      down_freq2(i) = -12700;  
      freq_shift_max(i) = 18943;
      freq_shift_min(i) = -16670;      
  else
      flag(i) = 0; 
      up_freq1(i) = 12700;
      down_freq1(i) = 2100;
      up_freq2(i) = -2100;
      down_freq2(i) = -12700;  
      freq_shift_max(i) = 18943;
      freq_shift_min(i) = -16670;  
  end
end
%8点斜率频偏上下限
figure(18);
subplot(2,1,1),
plot(slope_forward);
hold on,
plot(slope_after,'c');
hold on,
plot(up_freq1,'r');
hold on,
plot(up_freq2,'r');
hold on,
plot(down_freq1,'r');
hold on,
plot(down_freq2,'r');

%正常数据的8点斜率上下限
hold on,
plot(freq_shift_max,'k');
hold on,
plot(freq_shift_min,'k');

%交点分子分母，斜率
hold on,
plot(hilbert_data_out_15dot*8*10^4,'g');
title('拐点，前后8个点拐点斜率'),

hold on,
subplot(2,1,2),
plot(abs_reference_denominator,'k');
hold on,
plot(abs_reference_numerator,'c'),title('交点横坐标点分子分母');
hold on,
plot(flag,'g');







end


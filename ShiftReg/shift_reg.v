module shift_reg
#(
    parameter   taps  = 8,
    parameter   width = 16
)(
    input               clk,
    input               rst_n,
    input   [width-1:0] d_in,
    output  [width-1:0] d_out
);

    //该移位寄存器是并行多位移位寄存器，完成移位操作只需要一个时钟周期
    generate
        genvar  i;
        for(i = 0;i < width;i=i+1)
        begin:shift_bit_reg

            reg [taps-1:0]  r_reg;
            wire[taps-1:0]  r_next;  

            always@(posedge clk or negedge rst_n)begin 
                if(rst_n == 1'b0)
                    r_reg <= 0;
                else
                    r_reg <= r_next;
            end
            //更新移位寄存器状态
            assign  r_next = {d_in[i],r_reg[taps-1:1]};
            assign  d_out[i] = r_reg[taps-1];
        end
    endgenerate

endmodule
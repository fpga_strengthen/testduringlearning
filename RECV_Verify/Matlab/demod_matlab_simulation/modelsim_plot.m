close all;
clear;
clc;
%% 对比modelsim仿真结果
i_data_modelsim = hex2dec(textread('D:\lab_project\BTM\BTM_RCV_3C25\leexiao\BTM_RCV_3C25_defsk_V1.3.6.0\modelsim_simulation_defsk_v1.3.2.4\i_data.txt','%s'))';
% i_data_modelsim = i_data_modelsim(9000:1.2*10^4);
figure(1);
subplot(2,1,1);
plot(i_data_modelsim(1.5*10^6:1.7*10^6)-2048,'r');
legend('modelsim仿真的ADC采样');

% data_add_5_modelsim = textread('D:\lab_project\BTM\BTM_RCV_3C25\leexiao\BTM_RCV_3C25_defsk_V1.3.6.0\modelsim_simulation_defsk_v1.3.2.4\data_add_5.txt','%s');
% data_add_5_modelsim   = hex2decWithSign(data_add_5_modelsim,14)';
% figure(2);
% plot(data_add_5_modelsim(1:end),'r');
% legend('modelsim仿真data_add_5');

% data_H5_r_modelsim = textread('D:\lab_project\BTM\BTM_RCV_3C25\leexiao\BTM_RCV_3C25_defsk_V1.3.6.0\modelsim_simulation_defsk_v1.3.2.4\data_H5_r.txt','%s');
% data_H5_r_modelsim   = hex2decWithSign(data_H5_r_modelsim,32)';
% figure(3);
% plot(data_H5_r_modelsim(1:end),'r');
% legend('modelsim仿真data_H5');

% im_datain_solve_modelsim = textread('D:\lab_project\BTM\BTM_RCV_3C25\leexiao\BTM_RCV_3C25_defsk_V1.3.6.0\modelsim_simulation_defsk_v1.3.2.4\im_datain_solve.txt','%s');
% im_datain_solve_modelsim   = hex2decWithSign(im_datain_solve_modelsim,34)';
% im_datain_solve_modelsim = im_datain_solve_modelsim(9000:1.2*10^4);
% figure(4);
% plot(im_datain_solve_modelsim(1:end),'r');
% legend('modelsim仿真未截短的虚部');

im_datain_modelsim = textread('D:\lab_project\BTM\BTM_RCV_3C25\leexiao\BTM_RCV_3C25_defsk_V1.3.6.0\modelsim_simulation_defsk_v1.3.2.4\im_datain.txt','%s');
im_datain_modelsim   = hex2decWithSign(im_datain_modelsim,13)';
% im_datain_modelsim = im_datain_modelsim(9000:1.2*10^4);
figure(1);
subplot(2,1,2);
plot(im_datain_modelsim(1.5*10^6:1.7*10^6),'r');
legend('modelsim仿真的虚部');

angle_tan_modelsim = textread('D:\lab_project\BTM\BTM_RCV_3C25\leexiao\BTM_RCV_3C25_defsk_V1.3.6.0\modelsim_simulation_defsk_v1.3.2.4\angle_tan.txt','%s');
angle_tan_modelsim   = hex2dec(angle_tan_modelsim)';
% angle_tan_modelsim = angle_tan_modelsim(9000:1.2*10^4);
figure(6);
plot(angle_tan_modelsim(1.5*10^6:1.7*10^6),'r');
legend('modelsim仿真的原始相位');

angle_fit_modelsim = textread('D:\lab_project\BTM\BTM_RCV_3C25\leexiao\BTM_RCV_3C25_defsk_V1.3.6.0\modelsim_simulation_defsk_v1.3.2.4\angle_fit.txt','%s');
angle_fit_modelsim   = hex2decWithSign(angle_fit_modelsim,17)';
% angle_fit_modelsim = angle_fit_modelsim(9000:1.2*10^4);
figure(7);
subplot(2,1,1);
plot(angle_fit_modelsim(1.5*10^6:1.7*10^6),'r');
legend('modelsim仿真的相位折线');

slope_modelsim = textread('D:\lab_project\BTM\BTM_RCV_3C25\leexiao\BTM_RCV_3C25_defsk_V1.3.6.0\modelsim_simulation_defsk_v1.3.2.4\slope.txt','%s');
slope_modelsim   = hex2decWithSign(slope_modelsim,26)';
slope_correction_modelsim = textread('D:\lab_project\BTM\BTM_RCV_3C25\leexiao\BTM_RCV_3C25_defsk_V1.3.6.0\modelsim_simulation_defsk_v1.3.2.4\slope_correction.txt','%s');
slope_correction_modelsim   = hex2decWithSign(slope_correction_modelsim,26)';
% slope_modelsim = slope_modelsim(9000:1.2*10^4);
% slope_correction_modelsim = slope_correction_modelsim(9000:1.2*10^4);
figure(7);
subplot(2,1,2);
plot(slope_modelsim(1.5*10^6:1.7*10^6),'r');
hold on,
plot(slope_correction_modelsim(1.5*10^6:1.7*10^6),'b');
legend('modelsim仿真的斜率曲线','modelsim仿真的斜率修正量');

slope_delay_26_correction_modelsim = textread('D:\lab_project\BTM\BTM_RCV_3C25\leexiao\BTM_RCV_3C25_defsk_V1.3.6.0\modelsim_simulation_defsk_v1.3.2.4\slope_delay_1_n_correction.txt','%s');
slope_delay_26_correction_modelsim   = hex2decWithSign(slope_delay_26_correction_modelsim,27)';

sync_point_modelsim = str2num(char(textread('D:\lab_project\BTM\BTM_RCV_3C25\leexiao\BTM_RCV_3C25_defsk_V1.3.6.0\modelsim_simulation_defsk_v1.3.2.4\o_sync_point.txt','%s')))';
% slope_delay_26_correction_modelsim = slope_delay_26_correction_modelsim(9000:1.2*10^4);
% sync_point_modelsim = sync_point_modelsim(9000:1.2*10^4);
figure(8);
plot(slope_delay_26_correction_modelsim(1.5*10^6:1.7*10^6),'r');
hold on,
plot(sync_point_modelsim(1.5*10^6:1.7*10^6).*5*10^5,'b');
legend('modelsim仿真修正后的斜率曲线','modelsim仿真修正后的同步点');


cnt_48_modelsim = hex2dec(textread('D:\lab_project\BTM\BTM_RCV_3C25\leexiao\BTM_RCV_3C25_defsk_V1.3.6.0\modelsim_simulation_defsk_v1.3.2.4\cnt_48.txt','%s'))';
figure(9);
plot(cnt_48_modelsim(1.5*10^6:1.7*10^6),'r');
hold on,
legend('modelsim仿真修正后的cnt48');

cnt_sync_point_modelsim = hex2dec(textread('D:\lab_project\BTM\BTM_RCV_3C25\leexiao\BTM_RCV_3C25_defsk_V1.3.6.0\modelsim_simulation_defsk_v1.3.2.4\cnt_sync_point.txt','%s'))';
figure(10);
plot(cnt_sync_point_modelsim(1.5*10^6:1.7*10^6),'r');
hold on,
legend('modelsim仿真修正后的cnt_sync_point');

det_sample_modelsim = hex2dec(textread('D:\lab_project\BTM\BTM_RCV_3C25\leexiao\BTM_RCV_3C25_defsk_V1.3.6.0\modelsim_simulation_defsk_v1.3.2.4\det_sample.txt','%s'))';
figure(11);
plot(det_sample_modelsim(1.5*10^6:1.7*10^6),'r');
hold on,
legend('modelsim仿真修正后的det_sample');

sync_state_modelsim = hex2dec(textread('D:\lab_project\BTM\BTM_RCV_3C25\leexiao\BTM_RCV_3C25_defsk_V1.3.6.0\modelsim_simulation_defsk_v1.3.2.4\sync_state.txt','%s'))';
figure(12);
plot(sync_state_modelsim(1.5*10^6:1.7*10^6),'r');
hold on,
legend('modelsim仿真修正后的sync_state');


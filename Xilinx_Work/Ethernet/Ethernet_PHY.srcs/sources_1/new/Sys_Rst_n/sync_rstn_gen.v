`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date: 2024/08/23 10:33:21
// Module Name: sync_rstn_gen
// Tool Versions: 
// Description: 根据系统复位和时钟产生不同时钟域下的同步复位信号.
//////////////////////////////////////////////////////////////////////////////////

module sync_rstn_gen(
	input	wire	i_phy_rx_clk,
	input	wire	i_phy_tx_clk,
	input	wire	i_app_rx_clk,
	input	wire	i_app_tx_clk,
	input	wire	i_rst_n,

	output  reg 	o_phy_rx_rst_n,
	output	reg 	o_phy_tx_rst_n,
	output 	reg 	o_app_rx_rst_n,
	output  reg 	o_app_tx_rst_n
);

/*-----------------------------------------------*\
				phy_rx_rst_n
\*-----------------------------------------------*/	

	reg	[11:0]	phy_rx_rstn_timer;		// 定义一个计数器,将phy_rx_clk和i_rst_n同步,并展宽
	reg			phy_rx_rstn_d0;
	reg			phy_rx_rstn_d1;

	always @(posedge i_phy_rx_clk or negedge i_rst_n) begin
		if(~i_rst_n) 
			phy_rx_rstn_timer <= 'd0;
		else if(phy_rx_rstn_timer <= 'h0FF)
			phy_rx_rstn_timer <= phy_rx_rstn_timer + 1'b1;
		else
			phy_rx_rstn_timer <= phy_rx_rstn_timer;
	end

	always @(posedge i_phy_rx_clk or negedge i_rst_n) begin
		if(~i_rst_n) begin
			phy_rx_rstn_d0 <= 1'b0;
			phy_rx_rstn_d1 <= 1'b0;
			o_phy_rx_rst_n <= 1'b0;
		end 
		else begin
			phy_rx_rstn_d0 <= (phy_rx_rstn_timer > 'h0FF);
			phy_rx_rstn_d1 <= phy_rx_rstn_d0;
			o_phy_rx_rst_n <= phy_rx_rstn_d1;
		end
	end

/*-----------------------------------------------*\
				phy_tx_rst_n
\*-----------------------------------------------*/	

	reg	[11:0]	phy_tx_rstn_timer;		// 定义一个计数器,将phy_tx_clk和i_rst_n同步,并展宽
	reg			phy_tx_rstn_d0;
	reg			phy_tx_rstn_d1;

	always @(posedge i_phy_tx_clk or negedge i_rst_n) begin
		if(~i_rst_n) 
			phy_tx_rstn_timer <= 'd0;
		else if(phy_tx_rstn_timer <= 'h0FF)
			phy_tx_rstn_timer <= phy_tx_rstn_timer + 1'b1;
		else
			phy_tx_rstn_timer <= phy_tx_rstn_timer;
	end

	always @(posedge i_phy_tx_clk or negedge i_rst_n) begin
		if(~i_rst_n) begin
			phy_tx_rstn_d0 <= 1'b0;
			phy_tx_rstn_d1 <= 1'b0;
			o_phy_tx_rst_n <= 1'b0;
		end 
		else begin
			phy_tx_rstn_d0 <= (phy_tx_rstn_timer > 'h0FF);
			phy_tx_rstn_d1 <= phy_tx_rstn_d0;
			o_phy_tx_rst_n <= phy_tx_rstn_d1;
		end
	end

/*-----------------------------------------------*\
				app_tx_rst_n
\*-----------------------------------------------*/	

	reg	[11:0]	app_tx_rstn_timer;		// 定义一个计数器,将phy_tx_clk和i_rst_n同步,并展宽
	reg			app_tx_rstn_d0;
	reg			app_tx_rstn_d1;

	always @(posedge i_app_tx_clk or negedge i_rst_n) begin
		if(~i_rst_n) 
			app_tx_rstn_timer <= 'd0;
		else if(app_tx_rstn_timer <= 'h0FF)
			app_tx_rstn_timer <= app_tx_rstn_timer + 1'b1;
		else
			app_tx_rstn_timer <= app_tx_rstn_timer;
	end

	always @(posedge i_app_tx_clk or negedge i_rst_n) begin
		if(~i_rst_n) begin
			app_tx_rstn_d0 <= 1'b0;
			app_tx_rstn_d1 <= 1'b0;
			o_app_tx_rst_n <= 1'b0;
		end 
		else begin
			app_tx_rstn_d0 <= (app_tx_rstn_timer > 'h0FF);
			app_tx_rstn_d1 <= app_tx_rstn_d0;
			o_app_tx_rst_n <= app_tx_rstn_d1;
		end
	end

/*-----------------------------------------------*\
				app_rx_rst_n
\*-----------------------------------------------*/	

	reg	[11:0]	app_rx_rstn_timer;		// 定义一个计数器,将phy_rx_clk和i_rst_n同步,并展宽
	reg			app_rx_rstn_d0;
	reg			app_rx_rstn_d1;

	always @(posedge i_app_rx_clk or negedge i_rst_n) begin
		if(~i_rst_n) 
			app_rx_rstn_timer <= 'd0;
		else if(app_rx_rstn_timer <= 'h0FF)
			app_rx_rstn_timer <= app_rx_rstn_timer + 1'b1;
		else
			app_rx_rstn_timer <= app_rx_rstn_timer;
	end

	always @(posedge i_app_rx_clk or negedge i_rst_n) begin
		if(~i_rst_n) begin
			app_rx_rstn_d0 <= 1'b0;
			app_rx_rstn_d1 <= 1'b0;
			o_app_rx_rst_n <= 1'b0;
		end 
		else begin
			app_rx_rstn_d0 <= (app_rx_rstn_timer > 'h0FF);
			app_rx_rstn_d1 <= app_rx_rstn_d0;
			o_app_rx_rst_n <= app_rx_rstn_d1;
		end
	end

endmodule

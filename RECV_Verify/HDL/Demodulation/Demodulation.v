// 解调，判决模块
module Demodulation(
    input   wire            i_clk_108M,
    input   wire            i_clk_27M_en,
    input   wire            i_rst_n,

    input   wire    [11:0]  iv_ADC_data,
    input   wire            i_demod_en,

    output  reg             o_demod_result,
    output  reg             o_demod_result_sck
);

    // 通过对cnt_ctrl进行计数的控制,实现解调各步骤的流水线操作
    /*
        在没有使能信号到来时,cnt_ctrl为0,此时数据进来一个存一个、然后读一个出来,并和载波相乘用于解调
        在有使能信号到来时,cnt_ctrl置1,在2-49内完成48点的本地载波乘加,50完成平方相加,51比较,52回到0并保持
    */

    parameter   CNT_CTRL_STOP               = 6'd0,         // 同步脉冲没有来,计数器保持0
                CNT_CTRL_START              = 6'd1,         // 解调同步脉冲到了,开始计数
                CNT_CTRL_SATRT_MULT_CARRY   = 6'd2,         // 开始48点载波乘积
                CNT_CTRL_END_MULT_CARRY     = 6'd49,        // 48点载波乘法结束
                CNT_CTRL_SQUARE_ADD         = 6'd50,        // 计算平方和
                CNT_CTRL_JUDGE_GET_RESULT   = 6'd51,        // 判决,得到解调结果
                CNT_CTRL_FINISH             = 6'd52;        // 判决完成,计数器置0

//---------------------------输入信号缓存--------------------------//

    // cnt_ctrl
    reg [5:0]   cnt_ctrl;
    wire        state_cnt_ctrl_idle;
    wire        state_carry_mult_add;
    // 没有同步脉冲信号到来时的状态
    assign      state_cnt_ctrl_idle  = (cnt_ctrl == CNT_CTRL_STOP);
    // 本地载波和输入信号相乘
    assign      state_carry_mult_add = (cnt_ctrl >= CNT_CTRL_SATRT_MULT_CARRY)&&(cnt_ctrl <= CNT_CTRL_END_MULT_CARRY);

    // 没有同步脉冲时,置0
    // 同步脉冲到来后,在2-49完成48点载波乘加
    always@(posedge i_clk_108M or negedge i_rst_n)
    begin
        if(i_rst_n == 1'b0)
            cnt_ctrl <= CNT_CTRL_START;
        else if((i_demod_en == 1'b1)&&(i_clk_27M_en == 1'b1))
            cnt_ctrl <= CNT_CTRL_START;
        else if(cnt_ctrl == CNT_CTRL_FINISH)
            cnt_ctrl <= CNT_CTRL_STOP;
        else if(state_cnt_ctrl_idle == 1'b0)
            cnt_ctrl <= cnt_ctrl + 6'd1;
        else
            cnt_ctrl <= cnt_ctrl;
    end

    // RAM的写地址,按照27M的ADC采样速率将数据写入RAM
    reg [6:0]   ram_waddr;
    
    always@(posedge i_clk_108M or negedge i_rst_n)
    begin 
        if(i_rst_n == 1'b0)
            ram_waddr <= 7'd0;
        else if(i_clk_27M_en == 1'b1)begin 
            if(ram_waddr == 7'd80)
                ram_waddr <= 7'd0;
            else
                ram_waddr <= ram_waddr + 7'd1;
        end
        else
            ram_waddr <= ram_waddr;
    end
    
    // RAM的读地址
    // 当cnt_ctrl=0时,ram的读地址使用比写地址大1,这样可以读到80个bit之前的数据
    // 当cnt_ctrl不为0时,ram读地址在108M时钟下递增,累计80后再返回到1
    reg [6:0]   ram_raddr;

    // cnt_ctrl=0时,第一轮写满RAM,写到第80个数据时,RAM开始读第一个数据出来
    // 这样前80个数据也可以读出来,具体可以参考框图
    always@(posedge i_clk_108M or negedge i_rst_n)
    begin
        if(i_rst_n == 1'b0)
            ram_raddr <= 7'd0;
        else if(state_cnt_ctrl_idle == 1'b1)begin 
            if(ram_waddr == 7'd80)
                ram_raddr <= 7'd0;      // 读写逻辑从这里注意
            else 
                ram_raddr <= ram_waddr + 7'd1;
        end
        else if(ram_raddr == 7'd80)
            ram_raddr <= 7'd0;
        else
            ram_raddr <= ram_raddr + 7'd1;
    end

    wire    [11:0]  fsk_data_in;
    
    // RAM IP
    ram_adc_buffer U_ram_adc_buffer(
	    .data       (iv_ADC_data),
	    .rdaddress  (ram_raddr),
	    .rdclock    (i_clk_108M),
	    .wraddress  (ram_waddr),
	    .wrclock    (~i_clk_27M_en),
	    .wren       (1'b1),
	    .q          (fsk_data_in)
    );

    // ROM读地址配置,用来读出本地载波信息（48点）
    reg [5:0]   addr_rd_rom;

    // 读ROM的速率要和读RAM数据速率一致,从而保证数据的对齐
    always@(posedge i_clk_108M or negedge i_rst_n)
    begin
        if(i_rst_n == 1'b0)
            addr_rd_rom <= 6'd0;
        else if(state_cnt_ctrl_idle == 1'b1)
            addr_rd_rom <= 6'd0;
        else if(addr_rd_rom == 6'd47)
            addr_rd_rom <= 6'd0;
        else 
            addr_rd_rom <= addr_rd_rom + 6'd1;
    end

//-------------------------本地载波与输入信号相乘--------------------------//  
//----------------------- s(t)*cos(bit0)以及48点求和----------------------//
    
    // sin(bit0),sin(bit1),cos(bit0),cos(bit1)的四个表
    // 四路本地载波同时查表与输入信号相乘
    wire        [18:0]  sin_bit0;
    wire        [18:0]  sin_bit1;
    wire        [18:0]  cos_bit0;
    wire        [18:0]  cos_bit1;

    // 本地载波和输入信号相乘
    wire        [29:0]  mult_sin_bit0_carry;
    wire        [29:0]  mult_sin_bit1_carry;
    wire        [29:0]  mult_cos_bit0_carry;
    wire        [29:0]  mult_cos_bit1_carry;

    // 48点求和
    reg signed  [35:0]  sum_sin_bit1_carry_add;
    reg signed  [35:0]  sum_sin_bit0_carry_add;
    reg signed  [35:0]  sum_cos_bit0_carry_add;
    reg signed  [35:0]  sum_cos_bit1_carry_add;

    // 将求和结果截短到18位
    reg signed  [17:0]  sum_sin_bit0;
    reg signed  [17:0]  sum_sin_bit1;
    reg signed  [17:0]  sum_cos_bit0;
    reg signed  [17:0]  sum_cos_bit1;

    // 截短后计算平方
    wire        [35:0]  sin_bit0_square;
    wire        [35:0]  sin_bit1_square;
    wire        [35:0]  cos_bit0_square;
    wire        [35:0]  cos_bit1_square; 

    //------------------- s(t)*sin(bit0)以及48点求和--------------------//
    rom_sin_bit0 U_rom_sin_bit0(
	    .address(addr_rd_rom),
	    .clock  (i_clk_108M),
	    .q      (sin_bit0)
    );

    mult_fsk_carry U_mult_carry_sin0(
	    .dataa  (sin_bit0[17:0]),
	    .datab  (iv_ADC_data),
	    .result (mult_sin_bit0_carry)
    );

    //------------------- s(t)*sin(bit1)以及48点求和--------------------//
    rom_sin_bit1 U_rom_sin_bit1(
	    .address(addr_rd_rom),
	    .clock  (i_clk_108M),
	    .q      (sin_bit1)
    );

    mult_fsk_carry U_mult_carry_sin1(
	    .dataa  (sin_bit1[17:0]),
	    .datab  (iv_ADC_data),
	    .result (mult_sin_bit1_carry)
    );

    //------------------- s(t)*cos(bit0)以及48点求和--------------------//
    rom_cos_bit0 U_rom_cos_bit0(
	    .address (addr_rd_rom),
	    .clock   (i_clk_108M),
	    .q       (cos_bit0)
    );

    mult_fsk_carry U_mult_carry_cos0(
	    .dataa   (cos_bit0[17:0]),
	    .datab   (iv_ADC_data),
	    .result  (mult_cos_bit0_carry)
    );

    //------------------- s(t)*cos(bit1)以及48点求和--------------------//
    rom_cos_bit1 U_rom_cos_bit1(
	    .address (addr_rd_rom),
	    .clock   (i_clk_108M),
	    .q       (cos_bit1)
    );

    mult_fsk_carry U_mult_carry_cos1(
	    .dataa   (cos_bit1[17:0]),
	    .datab   (iv_ADC_data),
	    .result  (mult_cos_bit1_carry)
    ); 

    //-------------------------------------------------
    // sin(bit0)*fsk的48点求和
    always@(posedge i_clk_108M or negedge i_rst_n)
    begin 
        if(i_rst_n == 1'b0)
            sum_sin_bit0_carry_add <= 36'd0;
        else if(state_carry_mult_add == 1'b1)begin 
            if(sin_bit0[18] == 1'b0)
                sum_sin_bit0_carry_add <= sum_sin_bit0_carry_add + mult_sin_bit0_carry;
            else
                sum_sin_bit0_carry_add <= sum_sin_bit0_carry_add - mult_sin_bit0_carry;
        end
        else
            sum_sin_bit0_carry_add <= sum_sin_bit0_carry_add;
    end

    //求和结果截短
    always@(posedge i_clk_108M or negedge i_rst_n)
    begin 
        if(i_rst_n == 1'b0) 
            sum_sin_bit0 <= 18'd0;
        else
            sum_sin_bit0 <= {sum_sin_bit0_carry_add[35],sum_sin_bit0_carry_add[34:19]};
    end

    // 计算平方
    sum_square U_sin_bit0_square(
	    .dataa  (sum_sin_bit0),
	    .result (sin_bit0_square)
    );

    //-------------------------------------------------
    // sin(bit1)*fsk的48点求和
    always@(posedge i_clk_108M or negedge i_rst_n)
    begin 
        if(i_rst_n == 1'b0)
            sum_sin_bit1_carry_add <= 36'd0;
        else if(state_carry_mult_add == 1'b1)begin 
            if(sin_bit1[18] == 1'b0)
                sum_sin_bit1_carry_add <= sum_sin_bit1_carry_add + mult_sin_bit1_carry;
            else
                sum_sin_bit1_carry_add <= sum_sin_bit1_carry_add - mult_sin_bit1_carry;
        end
        else
            sum_sin_bit1_carry_add <= sum_sin_bit1_carry_add;
    end

    // 求和结果截短
    always@(posedge i_clk_108M or negedge i_rst_n)
    begin 
        if(i_rst_n == 1'b0)
            sum_sin_bit1 <= 18'd0;
        else
            sum_sin_bit1 <= {sum_sin_bit1_carry_add[35],sum_sin_bit1_carry_add[34:19]};
    end

    // 计算平方
    sum_square U_sin_bit1_square(
	    .dataa  (sum_sin_bit1),
	    .result (sin_bit1_square)
    );

    //-------------------------------------------------
    // cos(bit0)*fsk的48点求和
    always@(posedge i_clk_108M or negedge i_rst_n)
    begin 
        if(i_rst_n == 1'b0)
            sum_cos_bit0_carry_add <= 36'd0;
        else if(state_carry_mult_add == 1'b1)begin 
            if(cos_bit0[18] == 1'b0)
                sum_cos_bit0_carry_add <= sum_cos_bit0_carry_add + mult_cos_bit0_carry;
            else
                sum_cos_bit0_carry_add <= sum_cos_bit0_carry_add - mult_cos_bit0_carry;
        end
        else
            sum_cos_bit0_carry_add <= sum_cos_bit0_carry_add;
    end

    always@(posedge i_clk_108M or negedge i_rst_n)
    begin 
        if(i_rst_n == 1'b0)
            sum_cos_bit0 <= 18'd0;
        else    
            sum_cos_bit0 <= {sum_cos_bit0_carry_add[35],sum_cos_bit0_carry_add[34:19]};
    end

    // 计算平方
    sum_square U_cos_bit0_square(
	    .dataa  (sum_cos_bit0),
	    .result (cos_bit0_square)
    );

    //-------------------------------------------------
    // cos(bit1)*fsk的48点求和
    always@(posedge i_clk_108M or negedge i_rst_n)
    begin 
        if(i_rst_n == 1'b0)
            sum_cos_bit1_carry_add <= 36'd0;
        else if(state_carry_mult_add == 1'b1)begin 
            if(cos_bit1[18] == 1'b0)
                sum_cos_bit1_carry_add <= sum_cos_bit1_carry_add + mult_cos_bit1_carry;
            else
                sum_cos_bit1_carry_add <= sum_cos_bit1_carry_add - mult_cos_bit1_carry;
        end
        else
            sum_cos_bit1_carry_add <= sum_cos_bit1_carry_add;
    end

    always@(posedge i_clk_108M or negedge i_rst_n)
    begin 
        if(i_rst_n == 1'b0)
            sum_cos_bit1 <= 18'd0;
        else
            sum_cos_bit1 <= {sum_cos_bit1_carry_add[35],sum_cos_bit1_carry_add[34:19]};
    end

    // 计算平方
    sum_square U_cos_bit1_square(
	    .dataa  (sum_cos_bit1),
	    .result (cos_bit1_square)
    );

//----------------------------- 平方和累加 -----------------------------//

    reg [35:0]  sum_square_bit0;
    reg [35:0]  sum_square_bit1;

    // 因为平方结果是正数,所以最高位一定是0
    always@(posedge i_clk_108M or negedge i_rst_n)
    begin 
        if(i_rst_n == 1'b0)
            sum_square_bit0 <= 36'd0;
        else if(cnt_ctrl == CNT_CTRL_SQUARE_ADD)
            sum_square_bit0 <= cos_bit0_square[34:0] + sin_bit0_square[34:0];
        else
            sum_square_bit0 <= sum_square_bit0;
    end

    // 因为平方结果是正数,所以最高位一定是0
    always@(posedge i_clk_108M or negedge i_rst_n)
    begin 
        if(i_rst_n == 1'b0)
            sum_square_bit1 <= 36'd0;
        else if(cnt_ctrl == CNT_CTRL_SQUARE_ADD)
            sum_square_bit1 <= cos_bit1_square[34:0] + sin_bit1_square[34:0];
        else
            sum_square_bit1 <= sum_square_bit1;
    end

//------------------------------- 解调判决 -----------------------------//

    always@(posedge i_clk_108M or negedge i_rst_n)
    begin 
        if(i_rst_n == 1'b0)begin 
            o_demod_result     <= 1'b0;
            o_demod_result_sck <= 1'b0;
        end
        else if(cnt_ctrl == CNT_CTRL_JUDGE_GET_RESULT)begin 
            o_demod_result_sck <= 1'b1;
            if(sum_square_bit0 > sum_square_bit1)
                o_demod_result <= 1'b0;
            else
                o_demod_result <= 1'b1;
        end
        else begin 
            o_demod_result     <= o_demod_result;
            o_demod_result_sck <= 1'b0;
        end
    end

endmodule
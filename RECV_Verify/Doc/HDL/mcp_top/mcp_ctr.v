//============================================================
//
//--- ALL RIGHTS RESERVED
//--- File		: mcp_ctr.v
//--- Auther	: smartbai
//--- Data		: 2020-11-18
//--- Version	: v1.0.0.0
//--- Abstract	: 
//
//------------------------------------------------------------
//Description	:
//------------------------------------------------------------
//--- Modification History:
//--- Date		By		Version		Change Description
//------------------------------------------------------------
//
//============================================================

module mcp_ctr (
    i_clk,          //27MHz
    i_rst_n,

    i_tx_type_en,
    i_tx_status_en,
    i_tx_msg_en,

    i_frame_type_ok,
    i_frame_msg_ok,
    o_frame_type_en,
    o_frame_msg_en
);
    input i_clk;          //27MHz
    input i_rst_n;

    input i_tx_type_en;
    input i_tx_status_en;
    input i_tx_msg_en;

    input i_frame_type_ok;
    input i_frame_msg_ok;

    output wire o_frame_type_en;
    output wire o_frame_msg_en;
//-------------------------------------------------------------------//

    reg work_busy;      //can发送占用标志
    reg frame_type_en;
    reg frame_msg_en;
//-------------------------------------------------------------------//
    

    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n)
            work_busy <= 1'b0;
        else if((frame_msg_en && i_frame_msg_ok) || (frame_type_en && i_frame_type_ok))
            work_busy <= 1'b0;
        else if(frame_msg_en || frame_type_en)
            work_busy <= 1'b1;
        else
            work_busy <= 1'b0;
    end

    reg tx_msg_en_r;
    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n)
            tx_msg_en_r <= 1'b0;
        else if(frame_msg_en)
            tx_msg_en_r <= 1'b0;
        else if(i_tx_msg_en)
            tx_msg_en_r <= 1'b1;
        else
            tx_msg_en_r <= tx_msg_en_r;
    end

    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n)
            frame_msg_en <= 1'b0;
        else if(i_frame_msg_ok)
            frame_msg_en <= 1'b0;
        else if(tx_msg_en_r && (~work_busy))
            frame_msg_en <= 1'b1;
        else
            frame_msg_en <= frame_msg_en;
    end

    wire tx_type_en;
    assign tx_type_en = i_tx_type_en || i_tx_status_en;

    reg tx_type_en_r;
    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n)
            tx_type_en_r <= 1'b0;
        else if(frame_type_en)
            tx_type_en_r <= 1'b0;
        else if(tx_type_en)
            tx_type_en_r <= 1'b1;
        else
            tx_type_en_r <= tx_type_en_r;
    end

    always @(posedge i_clk or negedge i_rst_n) begin
        if(~i_rst_n)
            frame_type_en <= 1'b0;
        else if(i_frame_type_ok)
            frame_type_en <= 1'b0;
        else if(tx_type_en_r && (~work_busy))
            frame_type_en <= 1'b1;
        else
            frame_type_en <= frame_type_en;
    end

    assign o_frame_type_en = frame_type_en;
    assign o_frame_msg_en = frame_msg_en;
    // 根据调试需要暂时关闭 CAN 发送使能
    // assign o_frame_type_en = 1'b0;
    // assign o_frame_msg_en = 1'b0;
endmodule

close all;
clear;
clc;

%% 理论上解调算法实现
% --------------------------------------------------------

%%%%%%%产生相位连续的FSK调制信号进行ADC采样后作为输入%%%%%%%%%%%%%
src_data =randint(1,200);%以比特向量方式输入的待调制数据
freq_0 = 4.234 - 0.282  ;
freq_1 = 4.234 + 0.282 ; %比特0和比特1的频点，单位MHz
sample_rate = 25;%采样速率，单位MHz
BPS_data = 0.56448;  %数据传输速率，单位MHz
i_data_ADC  = FSK_mod(src_data,freq_0,freq_1,sample_rate,BPS_data)-2048;



%理论上的Hilbert变换
hilbert_fsk = hilbert(i_data_ADC);  %求出理论上的解析信号


%对中心频率和2pi进行修正
%理论实现
arctan_fsk = hilbert_fsk .* exp(-j*[1:length(hilbert_fsk)]*2*pi*4.234/sample_rate);%处理中心频率；
arctan_fsk = angle(arctan_fsk); %求出解析信号所对应的相位
arctan_fsk = unwrap(arctan_fsk,pi);  %对相位进行2pi的修正

% --------------------------------------------------------
%最小二乘法
%用理论计算的相位进行理论上的最小二乘法
[ intersection_slop_decisioned,slope_forward_total_theroy] = Least_Square_Method_theroy_1( arctan_fsk );
%% 相干解调结果
%    [bit0,bit1] = coherent(fsk_data);
pi				=	3.1415926;          %常数PI
sample_rate_1		=	25e6;               %采样速率，单位Hz
sample_period	=	1/sample_rate_1;      %采样速率，单位Hz
fsym            =	0.56448e6;          %数据传送速度，单位Hz  *(1-0.025)
%本地载波
f0_true = 3.951e6;
f1_true = 4.516e6;           %比特0和比特1的频点，单位Hz

S0 = cos( f0_true/sample_rate_1 * (1:30000) * 2*pi);
S1 = cos( f1_true/sample_rate_1 * (1:30000) * 2*pi);
S0_Q = sin(f0_true/sample_rate_1 * (1:30000) * 2*pi);%%移相90°
S1_Q = sin(f1_true/sample_rate_1 * (1:30000) * 2*pi);%%移相90

fsk_data = i_data_ADC;
length_fsk_data = length(fsk_data);
value_cos0 = fsk_data .* S0(1:length_fsk_data);
value_sin0 = fsk_data .* S0_Q(1:length_fsk_data);
value_cos1 = fsk_data .* S1(1:length_fsk_data);
value_sin1 = fsk_data .* S1_Q(1:length_fsk_data);

for i =1:length_fsk_data-44 %相干解调过程,48点求和，将0,1求和结果对应相减得到判决数据
    bit0(i) = ( sum(value_cos0(i:i+44)) )^2 + ( sum(value_sin0(i:i+44)) )^2  ;
    bit1(i) = ( sum(value_cos1(i:i+44)) )^2 + ( sum(value_sin1(i:i+44)) )^2  ;
end
value_x_sum0_minus_sum1 =  bit1 - bit0;
figure(6)
hold off
plot(bit0,'r');
hold on;
plot(bit1,'b');
plot(value_x_sum0_minus_sum1,'m');
plot( intersection_slop_decisioned(13:end)*10e8,'k');
title('相干运算结果');

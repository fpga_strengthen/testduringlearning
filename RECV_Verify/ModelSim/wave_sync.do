onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -label sim:/Sync_tb/U_Sync_top/U_ZCP_judge/Group1 -group {Region: sim:/Sync_tb/U_Sync_top/U_ZCP_judge} /Sync_tb/U_Sync_top/U_ZCP_judge/i_clk_108M
add wave -noupdate -expand -label sim:/Sync_tb/U_Sync_top/U_ZCP_judge/Group1 -group {Region: sim:/Sync_tb/U_Sync_top/U_ZCP_judge} /Sync_tb/U_Sync_top/U_ZCP_judge/i_clk_27M_en
add wave -noupdate -expand -label sim:/Sync_tb/U_Sync_top/U_ZCP_judge/Group1 -group {Region: sim:/Sync_tb/U_Sync_top/U_ZCP_judge} -format Analog-Step -height 74 -max 798808.0 -min -2524130.0 -radix decimal /Sync_tb/U_Sync_top/U_ZCP_judge/iv_slope
add wave -noupdate -expand -label sim:/Sync_tb/U_Sync_top/U_ZCP_judge/Group1 -group {Region: sim:/Sync_tb/U_Sync_top/U_ZCP_judge} -color Cyan -radix unsigned -radixshowbase 0 /Sync_tb/U_Sync_top/U_ZCP_judge/rd_addr_8p
add wave -noupdate -expand -label sim:/Sync_tb/U_Sync_top/U_ZCP_judge/Group1 -group {Region: sim:/Sync_tb/U_Sync_top/U_ZCP_judge} -color Cyan -radix unsigned -radixshowbase 0 /Sync_tb/U_Sync_top/U_ZCP_judge/wr_addr_8p
add wave -noupdate -expand -label sim:/Sync_tb/U_Sync_top/U_ZCP_judge/Group1 -group {Region: sim:/Sync_tb/U_Sync_top/U_ZCP_judge} /Sync_tb/U_Sync_top/U_ZCP_judge/slope_delay_8p
add wave -noupdate -expand -label sim:/Sync_tb/U_Sync_top/U_ZCP_judge/Group1 -group {Region: sim:/Sync_tb/U_Sync_top/U_ZCP_judge} /Sync_tb/U_Sync_top/U_ZCP_judge/sum_slope_8p
add wave -noupdate -expand -label sim:/Sync_tb/U_Sync_top/U_ZCP_judge/Group1 -group {Region: sim:/Sync_tb/U_Sync_top/U_ZCP_judge} -color Violet -radix decimal -radixshowbase 0 /Sync_tb/U_Sync_top/U_ZCP_judge/wr_addr_43p
add wave -noupdate -expand -label sim:/Sync_tb/U_Sync_top/U_ZCP_judge/Group1 -group {Region: sim:/Sync_tb/U_Sync_top/U_ZCP_judge} -color Violet -radix decimal -radixshowbase 0 /Sync_tb/U_Sync_top/U_ZCP_judge/rd_addr_43p
add wave -noupdate -expand -label sim:/Sync_tb/U_Sync_top/U_ZCP_judge/Group1 -group {Region: sim:/Sync_tb/U_Sync_top/U_ZCP_judge} /Sync_tb/U_Sync_top/U_ZCP_judge/slope_sum8p_delay_43p
add wave -noupdate -expand -label sim:/Sync_tb/U_Sync_top/U_ZCP_judge/Group1 -group {Region: sim:/Sync_tb/U_Sync_top/U_ZCP_judge} -color Yellow /Sync_tb/U_Sync_top/U_ZCP_judge/slope_right
add wave -noupdate -expand -label sim:/Sync_tb/U_Sync_top/U_ZCP_judge/Group1 -group {Region: sim:/Sync_tb/U_Sync_top/U_ZCP_judge} -color {Orange Red} -radix unsigned -radixshowbase 0 /Sync_tb/U_Sync_top/U_ZCP_judge/wr_addr_17p
add wave -noupdate -expand -label sim:/Sync_tb/U_Sync_top/U_ZCP_judge/Group1 -group {Region: sim:/Sync_tb/U_Sync_top/U_ZCP_judge} -color {Orange Red} -radix unsigned -radixshowbase 0 /Sync_tb/U_Sync_top/U_ZCP_judge/rd_addr_17p
add wave -noupdate -expand -label sim:/Sync_tb/U_Sync_top/U_ZCP_judge/Group1 -group {Region: sim:/Sync_tb/U_Sync_top/U_ZCP_judge} -color Orange -radix decimal -radixshowbase 0 /Sync_tb/U_Sync_top/U_ZCP_judge/sum_slope_16p
add wave -noupdate -expand -label sim:/Sync_tb/U_Sync_top/U_ZCP_judge/Group1 -group {Region: sim:/Sync_tb/U_Sync_top/U_ZCP_judge} -color Orange -radix decimal -radixshowbase 0 /Sync_tb/U_Sync_top/U_ZCP_judge/mid_freq
add wave -noupdate -expand -label sim:/Sync_tb/U_Sync_top/U_ZCP_judge/Group1 -group {Region: sim:/Sync_tb/U_Sync_top/U_ZCP_judge} -color {Cornflower Blue} -radix decimal -radixshowbase 0 /Sync_tb/U_Sync_top/U_ZCP_judge/slope_delay_25p
add wave -noupdate -expand -label sim:/Sync_tb/U_Sync_top/U_ZCP_judge/Group1 -group {Region: sim:/Sync_tb/U_Sync_top/U_ZCP_judge} -color {Cornflower Blue} -radix decimal -radixshowbase 0 /Sync_tb/U_Sync_top/U_ZCP_judge/slope_delay_26p
add wave -noupdate -expand -label sim:/Sync_tb/U_Sync_top/U_ZCP_judge/Group1 -group {Region: sim:/Sync_tb/U_Sync_top/U_ZCP_judge} -radix decimal -radixshowbase 0 /Sync_tb/U_Sync_top/U_ZCP_judge/slope_correction
add wave -noupdate -expand -label sim:/Sync_tb/U_Sync_top/U_ZCP_judge/Group1 -group {Region: sim:/Sync_tb/U_Sync_top/U_ZCP_judge} -color Salmon -radix decimal -radixshowbase 0 /Sync_tb/U_Sync_top/U_ZCP_judge/slope_delay25p_correction
add wave -noupdate -expand -label sim:/Sync_tb/U_Sync_top/U_ZCP_judge/Group1 -group {Region: sim:/Sync_tb/U_Sync_top/U_ZCP_judge} /Sync_tb/U_Sync_top/U_ZCP_judge/slope_delay25p_r
add wave -noupdate -expand -label sim:/Sync_tb/U_Sync_top/U_ZCP_judge/Group1 -group {Region: sim:/Sync_tb/U_Sync_top/U_ZCP_judge} /Sync_tb/U_Sync_top/U_ZCP_judge/sync_point
add wave -noupdate -expand -label sim:/Sync_tb/U_Sync_top/U_ZCP_judge/Group1 -group {Region: sim:/Sync_tb/U_Sync_top/U_ZCP_judge} /Sync_tb/U_Sync_top/U_ZCP_judge/sync_point_r
add wave -noupdate -expand -label sim:/Sync_tb/U_Sync_top/U_ZCP_judge/Group1 -group {Region: sim:/Sync_tb/U_Sync_top/U_ZCP_judge} /Sync_tb/U_Sync_top/U_ZCP_judge/sync_point_p
add wave -noupdate -expand -label sim:/Sync_tb/U_Sync_top/U_ZCP_judge/Group1 -group {Region: sim:/Sync_tb/U_Sync_top/U_ZCP_judge} /Sync_tb/U_Sync_top/U_ZCP_judge/o_sync_point
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {2120912 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 192
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {1952423 ps} {2393509 ps}

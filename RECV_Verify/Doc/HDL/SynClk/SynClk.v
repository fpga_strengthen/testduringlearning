module SynClk(
        i_clk_27M,  
        i_rst_n,
        
        o_clk_27M,
        o_clk_27M_en,
        o_clk_54M,
        o_clk_81M,
        o_sys_rst_n
        );

input i_clk_27M;
input i_rst_n;

output wire o_clk_27M;
output wire o_clk_27M_en;
output wire o_clk_54M;
output wire o_clk_81M;
output wire o_sys_rst_n;

//------------------------------信号声明----------------------------------//
reg rst_nr1;
reg rst_nr2;
wire pll_rst;
wire locked_27M;
wire sysrst_nr0;
reg sysrst_nr1;
reg sysrst_nr2;
reg clk_27M_r1;
reg clk_27M_r2;
reg clk_27M_p;

//------------------------------------------------------------------------//
//对复位信号进行同步处理，与锁相环输出的复位信号同步
always @( posedge i_clk_27M or negedge i_rst_n )
  begin
    if( !i_rst_n )
      begin
        rst_nr1 <= 1'b1;  //注意
        rst_nr2 <= 1'b1;
      end
    else  begin
        rst_nr1 <= 1'b0;
        rst_nr2 <= rst_nr1 ;
      end
  end

assign  pll_rst = rst_nr2;  

//对输入的27M时钟进行倍频，并对27M本身进行处理，确保时钟相位同步
pll UPll(
    .areset( pll_rst ), //PLL复位信号，高电平复位
    .inclk0( i_clk_27M ),
    .c0( o_clk_27M ),
    .c1( o_clk_54M ),
    .c2( o_clk_81M ),
    .c3(),    //o_clk_3M
    .locked( locked_27M )
    );

assign sysrst_nr0 = i_rst_n & locked_27M ;

always@ ( posedge o_clk_27M or negedge sysrst_nr0 )
  begin
    if( !sysrst_nr0 )   
      begin
        sysrst_nr1 <= 1'b0;
        sysrst_nr2 <= 1'b0;
      end
    else  begin
        sysrst_nr1 <= 1'b1;
        sysrst_nr2 <= sysrst_nr1;
      end
  end

assign o_sys_rst_n = sysrst_nr2 ;

//-------------------------------------------------------------------------------------------------------------------//
//27M使能
always@ ( posedge o_clk_81M or negedge sysrst_nr2 )
begin
  if( !sysrst_nr2 )begin
      clk_27M_r1 <= 1'b0;
      clk_27M_r2 <= 1'b0;
      clk_27M_p  <= 1'b0;
  end
  else begin
      clk_27M_r1 <= o_clk_27M;
      clk_27M_r2 <= clk_27M_r1;
      clk_27M_p  <= clk_27M_r1 & (~clk_27M_r2);
  end  
end

assign o_clk_27M_en = clk_27M_p ;

endmodule



  
      
%% 1.该程序用来对Cordic计算相位的过程进行仿真
% 基本公式如下：
% x(n+1) = x(n) - d(n)*2^(-n)*y(n)
% y(n+1) = y(n) + d(n)*2^(-n)*x(n)
% z(n+1) = z(n) - d(n)*theta(n) = z(n) - d(n)*arctan(2^(-n))

%% 2.初始化
% 不断旋转直到与x轴重合，此时累计角度就是相位，x值就是模值

% 初始化：制作查找表，相位theta_i满足tan(theta_i)=2^(-i)
% 迭代次数
N = 16;
% tan表
tan_table = 2.^(-(0 : N-1));
% 角度查找表
angle_LUT = atan(tan_table);

% 伪旋转系数Kn,可以参考pdf第437页的公式
% K(n+1) = K(n)*1/cos(theta_n)=K(n)*sqrt(1+2^(-2n))
K = 1;
% 伪旋转系数的迭代，用于计算向量的模值
for k=0:N-1
    K = K * (1/sqrt(1+2^(-2*k)));
end

% 初始坐标值
x = -3;
y = sqrt(3);

% 最终的旋转累计角度值就是要求的相位
angle_acumulate = 0;

rad_keep = zeros(1,N);

%% 3.Cordic算法计算
if((x==0) && (y==0))
    radian_out = 0;
    angle_acumulate = 0;
else                % 象限判断，得到相位补偿值
    if(x > 0)       % 一四象限
        phase_shift = 0;
    else            % 二三象限
        phase_shift = pi;
        % 将(x,y)按照原点对称后再进行相位补偿
        x = -x;
        y = -y;
    end

    % 开始迭代
    for k=0 : N-1
        x_temp = x;
        if(y < 0)       % 在x轴下方要逆时针旋转,则d(k)=1
            x = x_temp - y*2^(-k);
            y = y + x_temp*2^(-k);
            angle_acumulate = angle_acumulate - angle_LUT(k+1);
        else            % 在x轴上方要顺时针旋转,则d(k)=-1
            x = x_temp + y*2^(-k);
            y = y - x_temp * 2^(-k);
            angle_acumulate = angle_acumulate + angle_LUT(k+1);
        end
        % 输出弧度
        radian_out = angle_acumulate + phase_shift ;
        rad_keep(k+1) = radian_out;
    end
    % 幅值输出
    amplitude_out = x * K;
end

% 输出相位的角度值
angle_out = radian_out * 180/pi;

%% End of File.
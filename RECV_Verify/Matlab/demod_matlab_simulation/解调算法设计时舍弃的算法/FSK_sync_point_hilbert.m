%FSK解调判决仿真
%目的检测频偏对判决错误的影响
%Hilbert低通滤波法

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%FSK调制%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clc
format long
close all
clear all

%% %标准条件下主要参数
f0_true = 3.951 ;
f1_true = 4.516 ;         % 比特0和比特1的频点，单位MHz
f_mid =(3.951+4.516) / 2; % 中心频率
sample_rate_true = 27.095;% 标准采样速率，单位MHz
BPS_data_true = 0.56448;  % 标准数据传输速率，单位MHz
ws = BPS_data_true*2*pi;
sample_point = ceil(sample_rate_true/BPS_data_true); %一个周期的采样点数 48
sum_bits = 100;           % 比特数
Hd = LP_6M;
N_least = 3; % 2N+1点最小二乘法
coeff_numerator = 3*[-N_least:N_least];           %极值点多项式分子
coeff_denominator = 8 - 2 * [-N_least:N_least].^2;%极值点多项式分母

%%%%%%%%% 产生相位连续的FSK调制信号进行ADC采样后作为输入 %%%%%%%%%%%%%
src_data =randint(1,sum_bits);%以比特向量方式输入的待调制数据，随机产生
static_lenth_x_axis = 48 * sum_bits + 50;

% 构造本地载波
S0 = cos( f0_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);
S1 = cos( f1_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);
S0_Q = sin(f0_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);%%移相90°
S1_Q = sin(f1_true/sample_rate_true * (1:static_lenth_x_axis) * 2*pi);%%移相90°

%% hilbert 参数
%时窗法参数
N = 10;%取值范围在10~30之间
n = [-N:1:N];
window = ones(1,2*N+1); %矩形窗
nfft = 1024;
w_n_2 = 0.56+0.44*cos(pi*n/N);
h_n_2 = 2./(pi.*n).*w_n_2;
h_n_2(1:2:end) = 0;%修正后的Hilbert滤波因子
%画出Hilbert变换滤波因子h_n_2的功率谱
[Pxx2,f2]=periodogram(h_n_2,window,nfft,sample_rate_true); %直接法求功率谱
Pxx2 = 10*log10(Pxx2);
Fl_75k_2 = find( (f2 < 0.08) & (f2 > 0.074) );%找到75kHz的点
Fh_564k_2 = find((f2 < 0.580) & (f2 > 0.550));%找到564kHz的点
figure(1),subplot(2,1,1)
plot(f2,Pxx2);
grid on
text(f2(Fl_75k_2),Pxx2(Fl_75k_2),'o','color','r');%标注200KHz的位置
text(f2(Fl_75k_2+1),Pxx2(Fl_75k_2+1),['(',num2str(f2(Fl_75k_2)),'kHz,',num2str(Pxx2(Fl_75k_2)),'dB)'],'color','r');
text(f2(Fh_564k_2),Pxx2(Fh_564k_2),'^','color','b');%标注200KHz的位置
text(f2(Fh_564k_2+1),Pxx2(Fh_564k_2+1),['(',num2str(f2(Fh_564k_2)),'kHz,',num2str(Pxx2(Fh_564k_2)),'dB)'],'color','k');
title('时窗法2滤波因子h_n_2的功率谱')

% 镶边法
N1 = 30;    %取值范围在[30,60]之间
n1 = [-N1:1:N1];
f1 = 0.2;  %截止频率200K
f2 = 0.3;   %通带300K
f3 = 1.1;
f4 = 1.2;   %单位MHz
delta = 1/sample_rate_true;%抽样时间间隔
g_n = (4/(f4-f3)^2).*sin(pi.*(f3+f4).*n1.*delta).*sin(pi.*(f4-f3)./2.*n1.*delta).^2./(pi.*n1.*delta).^3 - (4/(f2-f1)^2).*sin(pi.*(f1+f2).*n1.*delta).*sin(pi.*(f2-f1)./2.*n1.*delta).^2./(pi.*n1.*delta).^3 ;
%n1=0时g_n无值，所以在该点求极限代替
syms n_syms;
g_n(N1+1)=limit((4/(f4-f3)^2).*sin(pi.*(f3+f4).*n_syms.*delta).*sin(pi.*(f4-f3)./2.*n_syms.*delta).^2./(pi.*n_syms.*delta).^3 - (4/(f2-f1)^2).*sin(pi.*(f1+f2).*n_syms.*delta).*sin(pi.*(f2-f1)./2.*n_syms.*delta).^2./(pi.*n_syms.*delta).^3)
for i =1:1:length(n1)
    if rem(n1(i),2)==0
        n1_odd(i) = Inf;
    else
        n1_odd(i) = n1(i);
    end %if
end %for
hn = 2./(pi.*n1_odd);
h_n_4 = conv(g_n,hn);
h_n_4 = h_n_4((2*N1-9):(2*N1+11));
%画出Hilbert变换滤波因子h_n的功率谱
[Pxx4,f4]=periodogram(h_n_4,window,nfft,sample_rate_true); %直接法求功率谱
Pxx4 = 10*log10(Pxx4);
Fl_75k_4 = find( (f4 < 0.08) & (f4 > 0.07) );%找到75kHz的点
Fh_564k_4 = find((f4 < 0.58) & (f4 > 0.55));%找到564kHz的点
figure(1), subplot(2,1,2)
plot(f4(1:100),Pxx4(1:100));
grid on
text(f4(Fl_75k_4),Pxx4(Fl_75k_4),'o','color','r');%标注200KHz的位置
text(f4(Fl_75k_4+1),Pxx4(Fl_75k_4+1),['(',num2str(f4(Fl_75k_4)),'kHz,',num2str(Pxx4(Fl_75k_4)),'dB)'],'color','r');
text(f4(Fh_564k_4),Pxx4(Fh_564k_4),'^','color','b');%标注200KHz的位置
text(f4(Fh_564k_4+1),Pxx4(Fh_564k_4+1),['(',num2str(f4(Fh_564k_4)),'kHz,',num2str(Pxx4(Fh_564k_4)),'dB)'],'color','k');
title('镶边法滤波因子h_n_4的功率谱')


%% 比较特殊频率处理抵消效果
for f0 = -0.075:0.02:0.075 %频偏f0在-75K~75K之间，间隔10K连续变化
    for f1 = -0.075:0.02:0.075 %%频偏f1在-75K~75K之间，间隔10K连续变化
        %产生FSK调制信号
        BPS_data = BPS_data_true;%数据传输速率，单位MHz
        sample_rate =  sample_rate_true;%采样速率，单位MHz
        FSK_data  = FSK_mod(src_data,(f0_true + f0),(f1_true + f1),sample_rate,BPS_data);%相位连续
        
        %对输入数据进行定标
        FSK_data = FSK_data  - 2048; % FixPointNum(FSK_data  - 2048, 12, 0);%字长12bit，精度0位小数 + FSK_data_noise
        length_fsk_data = length(FSK_data);
        
        %------------混频并滤除高频信号-----------------------
        FSK_data_S0     = filter(Hd,FSK_data .* S0(1:length_fsk_data));
        FSK_data_S0_Q   = filter(Hd,FSK_data .* S0_Q(1:length_fsk_data));
        FSK_data_S1     = filter(Hd,FSK_data .* S1(1:length_fsk_data));
        FSK_data_S1_Q   = filter(Hd,FSK_data .* S1_Q(1:length_fsk_data));
        
        %% 消除Ws的影响 hilbert法
        %求希尔伯特变换
        %% 系统自带Hilbert函数
        hilbert_FSK_data_S0_system     = FSK_data_S0 - imag(hilbert(FSK_data_S0_Q));
        hilbert_FSK_data_S0_Q_system   = FSK_data_S0_Q + imag(hilbert(FSK_data_S0));
        hilbert_FSK_data_S1_system     = FSK_data_S1 - imag(hilbert(FSK_data_S1_Q));
        hilbert_FSK_data_S1_Q_system   = FSK_data_S1_Q + imag(hilbert(FSK_data_S1));
        
        figure(2),
        subplot(3,1,1),
        hold off,
        plot(FSK_data_S0,'r'),
        hold on,
        plot(imag(hilbert(FSK_data_S0_Q)),'g'),
        plot(hilbert_FSK_data_S0_system,'k');
        title(strcat('采用系统Hibert变换S0路抵消结果：FSK信号频偏：w0=' ,num2str(f0*1000),' ,w1= ',num2str(f1*1000), 'Khz'));
        legend('S0路低通滤波','S0_Q路Hilbert变换后的结果','S0路-S0_Q路Hilbert');
        
        %% 时窗法 时窗函数： w_n_2=w（n△）=0.56+0.44*cos(pi*n/N)
        %卷积的到hilbert变换的虚部
        FSK_data_S0_hilbert_window = conv(h_n_2,FSK_data_S0);
        FSK_data_S0_hilbert_window = FSK_data_S0_hilbert_window(10:end);
        FSK_data_S0_Q_hilbert_window = conv(h_n_2,FSK_data_S0_Q);
        FSK_data_S0_Q_hilbert_window = FSK_data_S0_Q_hilbert_window(10:end);
        FSK_data_S1_hilbert_window = conv(h_n_2,FSK_data_S1);
        FSK_data_S1_hilbert_window = FSK_data_S1_hilbert_window(10:end);
        FSK_data_S1_Q_hilbert_window = conv(h_n_2,FSK_data_S1_Q);
        FSK_data_S1_Q_hilbert_window = FSK_data_S1_Q_hilbert_window(10:end);
        %---原始数据和对应希尔伯特变换相加
        hilbert_FSK_data_S0_window     = FSK_data_S0 - FSK_data_S0_Q_hilbert_window(1:end-11);
        hilbert_FSK_data_S0_Q_window   = FSK_data_S0_Q + FSK_data_S0_hilbert_window(1:end-11);
        hilbert_FSK_data_S1_window     = FSK_data_S1 - FSK_data_S1_Q_hilbert_window(1:end-11);
        hilbert_FSK_data_S1_Q_window   = FSK_data_S1_Q + FSK_data_S1_hilbert_window(1:end-11);
        
        figure(2),
        subplot(3,1,2),
        hold off,
        plot(FSK_data_S0,'r'),
        hold on,
        plot(FSK_data_S0_Q_hilbert_window,'g'),
        plot(hilbert_FSK_data_S0_window,'k');
        title(strcat('采用窗函数Hibert变换S0路抵消结果：FSK信号频偏：w0=' ,num2str(f0*1000),',w1= ',num2str(f1*1000), 'Khz'));
        legend('S0路低通滤波','S0_Q路Hilbert变换后的结果','S0路-S0_Q路Hilbert');
        
        %% 镶边法
        %卷积的到hilbert变换的虚部
        FSK_data_S0_hilbert_edging = conv(h_n_4,FSK_data_S0);
        FSK_data_S0_hilbert_edging = FSK_data_S0_hilbert_edging(10:end)./15;%需要./系数结果才正确，原因是由于滤波器衰减还是滤波器系数本身扩大了？？
        FSK_data_S0_Q_hilbert_edging = conv(h_n_4,FSK_data_S0_Q);
        FSK_data_S0_Q_hilbert_edging = FSK_data_S0_Q_hilbert_edging(10:end)./15;
        FSK_data_S1_hilbert_edging = conv(h_n_4,FSK_data_S1);
        FSK_data_S1_hilbert_edging = FSK_data_S1_hilbert_edging(10:end)./15;
        FSK_data_S1_Q_hilbert_edging = conv(h_n_4,FSK_data_S1_Q);
        FSK_data_S1_Q_hilbert_edging = FSK_data_S1_Q_hilbert_edging(10:end)./15;
        %---原始数据和对应希尔伯特变换相加
        hilbert_FSK_data_S0_edging     = FSK_data_S0 - FSK_data_S0_Q_hilbert_edging(1:end-11);
        hilbert_FSK_data_S0_Q_edging   = FSK_data_S0_Q + FSK_data_S0_hilbert_edging(1:end-11);
        hilbert_FSK_data_S1_edging     = FSK_data_S1 + FSK_data_S1_Q_hilbert_edging(1:end-11);
        hilbert_FSK_data_S1_Q_edging   = FSK_data_S1_Q - FSK_data_S1_hilbert_edging(1:end-11);
        
        figure(2),
        subplot(3,1,3),
        hold off,
        plot(FSK_data_S0,'r'),
        hold on,
        plot(FSK_data_S0_Q,'g'),
        plot(FSK_data_S0_Q_hilbert_edging,'b'),
        plot(hilbert_FSK_data_S0_edging,'k');
        title(strcat('采用镶边法Hibert变换S0路抵消结果：FSK信号频偏：w0=' ,num2str(f0*1000),',w1= ',num2str(f1*1000), 'Khz'));
        legend('S0路低通滤波','S0_Q路低通滤波','S0_Q路Hilbert变换后的结果','S0路-S0_Q路Hilbert');
        
        %% 对镶边法进一步分析
        figure(3),
        subplot(2,2,1),
        hold off,
        plot(FSK_data_S0,'r'),
        hold on,
        plot(FSK_data_S0_Q,'g'),
        plot(FSK_data_S0_Q_hilbert_edging,'b'),
        plot(hilbert_FSK_data_S0_edging,'k');
        title(strcat('采用镶边法Hibert变换S0路抵消结果：FSK信号频偏：w0=' ,num2str(f0*1000),',w1= ',num2str(f1*1000), 'Khz'));
        legend('S0路低通滤波','S0_Q路低通滤波','S0_Q路Hilbert变换后的结果','S0路-S0_Q路Hilbert');
        
        figure(3),
        subplot(2,2,2),
        hold off,
        plot(FSK_data_S0,'r'),
        hold on,
        plot(FSK_data_S0_Q,'g'),
        plot(FSK_data_S0_hilbert_edging,'b'),
        plot(hilbert_FSK_data_S0_Q_edging,'k');
        title(strcat('采用镶边法Hibert变换S0_Q路抵消结果：FSK信号频偏：w0=' ,num2str(f0*1000),',w1= ',num2str(f1*1000), 'Khz'));
        legend('S0路低通滤波','S0_Q路低通滤波','S0路Hilbert变换后的结果','S0_Q路+S0路Hilbert');
        
        figure(3),
        subplot(2,2,3),
        hold off,
        plot(FSK_data_S1,'r'),
        hold on,
        plot(FSK_data_S1_Q,'g'),
        plot(FSK_data_S1_Q_hilbert_edging,'b'),
        plot(hilbert_FSK_data_S1_edging,'k');
        title(strcat('采用镶边法Hibert变换S1路抵消结果：FSK信号频偏：w0=' ,num2str(f0*1000),',w1= ',num2str(f1*1000), 'Khz'));
        legend('S1路低通滤波','S1_Q路低通滤波','S1_Q路Hilbert变换后的结果','S1路+S1_Q路Hilbert');
        
        figure(3),
        subplot(2,2,4),
        hold off,
        plot(FSK_data_S1,'r'),
        hold on,
        plot(FSK_data_S1_Q,'g'),
        plot(FSK_data_S1_hilbert_edging,'b'),
        plot(hilbert_FSK_data_S1_Q_edging,'k');
        title(strcat('采用镶边法Hibert变换S1_Q路抵消结果：FSK信号频偏：w0=' ,num2str(f0*1000),',w1= ',num2str(f1*1000), 'Khz'));
        legend('S1路低通滤波','S1_Q路低通滤波','S1路Hilbert变换后的结果','S1_Q路-S1路Hilbert');
        
        figure(4),
        hold off,
        plot(hilbert_FSK_data_S0_edging,'r');
        hold on,
        plot(hilbert_FSK_data_S0_Q_edging,'g');
        plot(hilbert_FSK_data_S1_edging,'b');
        plot(hilbert_FSK_data_S1_Q_edging,'k');
        title(strcat('采用镶边法Hibert变换四路抵消后的结果：FSK信号频偏：w0=' ,num2str(f0*1000),',w1= ',num2str(f1*1000), 'Khz'));
        legend('S0路','S0_Q路','S1路','S1_Q路');
        
        %% 对镶边法得到的的结果进行24点累加后求平方和
        for i =1:length_fsk_data-23 %相干解调过程,24个点求和，再除以24
            sum_bit0_point(i) = ( (sum(hilbert_FSK_data_S0_edging(i:i+23)))^2 + (sum(hilbert_FSK_data_S0_Q_edging(i:i+23)))^2 )./24  ;
            sum_bit1_point(i) = ( (sum(hilbert_FSK_data_S1_edging(i:i+23)))^2 + (sum(hilbert_FSK_data_S1_Q_edging(i:i+23)))^2 )./24  ;
        end
        sum_bit0_mins_sum_bit0 = sum_bit0_point(1:end-24) - sum_bit0_point(25:end);
        sum_bit1_mins_sum_bit1 = sum_bit1_point(1:end-24) - sum_bit1_point(25:end);
        sum_bit0_mins_sum_bit1 = sum_bit0_mins_sum_bit0 - sum_bit1_mins_sum_bit1;
        sum_bit0_mins_sum_bit1 = [zeros(1,12),sum_bit0_mins_sum_bit1(1:end)];%第1个点减第25个点得到的是第12个点的结果，而目前将该结果存在第1个位置，所以需要移位12点才正确
        sum_bit0_mins_sum_bit1 = abs(sum_bit0_mins_sum_bit1);%去掉符号位
        for i = 1:length(sum_bit0_mins_sum_bit1)
                if (sum_bit0_mins_sum_bit1(i) < max(sum_bit0_mins_sum_bit1)*0.8)%下溢清零
                    sum_bit0_mins_sum_bit1(i) = 0;
                else
                    sum_bit0_mins_sum_bit1(i) = sum_bit0_mins_sum_bit1(i) - max(sum_bit0_mins_sum_bit1)*0.8; %减小位数
                end %if
            end %for
        
        figure(5),
        subplot(2,1,1),
        hold off,
        plot(sum_bit0_point,'r');    hold on,
        plot(sum_bit1_point,'g');
        plot(sum_bit0_mins_sum_bit1);
        legend('24点和的平方bit0','24点和的平方bit1','24点和的平方再求差');
        title(strcat('采用镶边法Hibert变换抵消后再累加的结果：FSK信号频偏：w0=' ,num2str(f0*1000),' ,w1= ',num2str(f1*1000), 'Khz'));
        
        % 求极值点坐标处理 最小二乘法的二次曲线拟合
        temp_val = toeplitz(sum_bit0_mins_sum_bit1(1:2*N_least+1),sum_bit0_mins_sum_bit1);
        sync_point = (coeff_numerator * temp_val) ./ ( coeff_denominator * temp_val);%详见文档极值点坐标公式推导
        sync_point(sync_point > 5) = 5;%极值应在[-3,3]之间，上溢处理
        sync_point(sync_point < -5) = -5;
        sync_point = (abs(sync_point)<0.6).* (sum_bit0_mins_sum_bit1>0);%对称轴在[-0.6,0.6]之间且该点不为零时，该坐标轴原点位置为所求极值点
        sync_point = sync_point(4:end); %该算法会让极值点向后移3个点，是否与N有关？
        sync_point = sync_point(1:end).*(~[0,sync_point(1:end-1)])%取上升沿作极值点
        % 判断两相邻极值点是否是48的整数倍
        edge_position = find(sync_point == 1);%极值点的位置
        edge_variance = mod(edge_position(2:length(edge_position)) - edge_position(1:length(edge_position)-1),48);%两极值点之间的间隔对48点取模，得到偏差量
        x_axis = [-4:4];
        y_axis = zeros(1,9);
        y_axis(1) = length (find((edge_variance>=4)&(edge_variance<=23)));%多>3个点
        y_axis(2) = length (find(edge_variance==3));%多3个点
        y_axis(3) = length (find(edge_variance==2));%多2个点
        y_axis(4) = length (find(edge_variance==1));%多1个点
        y_axis(5) = length (find(edge_variance==0));%无偏差
        y_axis(6) = length (find(edge_variance==47));%少1个点
        y_axis(7) = length (find(edge_variance==46));%少2个点
        y_axis(8) = length (find(edge_variance==45));%少3个点
        y_axis(9) = length (find((edge_variance>=24)&(edge_variance<=44)));%少>4个点
        y_axis = y_axis./sum(y_axis);
        
        figure(6),
        subplot(2,1,1),
        bar(x_axis,y_axis);
        xlabel('偏差量');
        ylabel('统计概率值');
        set(gca,'xtick',-23:1:24);
        title(strcat('采用镶边法Hibert变换，当FSK信号频偏:w0=' ,num2str(f0*1000),' ,w1= ',num2str(f1*1000), 'Khz时统计偏移量'));
        
        %% 对低通滤波后得到的的结果直接进行24点累加后求平方和
        for i =1:length_fsk_data-23 %相干解调过程,24个点求和，再除以24
            sum_bit0_point(i) = ( (sum(FSK_data_S0(i:i+23)))^2 + (sum(FSK_data_S0_Q(i:i+23)))^2 )./24  ;
            sum_bit1_point(i) = ( (sum(FSK_data_S1(i:i+23)))^2 + (sum(FSK_data_S1_Q(i:i+23)))^2 )./24  ;
        end
        sum_bit0_mins_sum_bit0 = sum_bit0_point(1:end-24) - sum_bit0_point(25:end);
        sum_bit1_mins_sum_bit1 = sum_bit1_point(1:end-24) - sum_bit1_point(25:end);
        sum_bit0_mins_sum_bit1 = sum_bit0_mins_sum_bit0 - sum_bit1_mins_sum_bit1;
        sum_bit0_mins_sum_bit1 = [zeros(1,12),sum_bit0_mins_sum_bit1(1:end)];%第1个点减第25个点得到的是第12个点的结果，而目前将该结果存在第1个位置，所以需要移位12点才正确
        sum_bit0_mins_sum_bit1 = abs(sum_bit0_mins_sum_bit1);%去掉符号位
        for i = 1:length(sum_bit0_mins_sum_bit1)
            if (sum_bit0_mins_sum_bit1(i) < max(sum_bit0_mins_sum_bit1)*0.8)%下溢清零
                sum_bit0_mins_sum_bit1(i) = 0;
            else
                sum_bit0_mins_sum_bit1(i) = sum_bit0_mins_sum_bit1(i) - max(sum_bit0_mins_sum_bit1)*0.8; %减小位数
            end %if
        end %for
        
        figure(5),
        subplot(2,1,2),
        hold off,
        plot(sum_bit0_point,'r');    hold on,
        plot(sum_bit1_point,'g');
        plot(sum_bit0_mins_sum_bit1);
        legend('24点和的平方bit0','24点和的平方bit1','24点和的平方再求差');
        title(strcat('低通滤波后直接累加的结果：FSK信号频偏：w0=' ,num2str(f0*1000),' ,w1= ',num2str(f1*1000), 'Khz'));
        
        % 求极值点坐标处理 最小二乘法的二次曲线拟合
        temp_val = toeplitz(sum_bit0_mins_sum_bit1(1:2*N_least+1),sum_bit0_mins_sum_bit1);
        sync_point = (coeff_numerator * temp_val) ./ ( coeff_denominator * temp_val);%详见文档极值点坐标公式推导
        sync_point(sync_point > 5) = 5;%极值应在[-3,3]之间，上溢处理
        sync_point(sync_point < -5) = -5;
        sync_point = (abs(sync_point)<0.6).* (sum_bit0_mins_sum_bit1>0);%对称轴在[-0.6,0.6]之间且该点不为零时，该坐标轴原点位置为所求极值点
        sync_point = sync_point(4:end); %该算法会让极值点向后移3个点，是否与N有关？
        sync_point = sync_point(1:end).*(~[0,sync_point(1:end-1)])%取上升沿作极值点
        % 判断两相邻极值点是否是48的整数倍
        edge_position = find(sync_point == 1);%极值点的位置
        edge_variance = mod(edge_position(2:length(edge_position)) - edge_position(1:length(edge_position)-1),48);%两极值点之间的间隔对48点取模，得到偏差量
        x_axis = [-4:4];
        y_axis = zeros(1,9);
        y_axis(1) = length (find((edge_variance>=4)&(edge_variance<=23)));%多>3个点
        y_axis(2) = length (find(edge_variance==3));%多3个点
        y_axis(3) = length (find(edge_variance==2));%多2个点
        y_axis(4) = length (find(edge_variance==1));%多1个点
        y_axis(5) = length (find(edge_variance==0));%无偏差
        y_axis(6) = length (find(edge_variance==47));%少1个点
        y_axis(7) = length (find(edge_variance==46));%少2个点
        y_axis(8) = length (find(edge_variance==45));%少3个点
        y_axis(9) = length (find((edge_variance>=24)&(edge_variance<=44)));%少>4个点
        y_axis = y_axis./sum(y_axis);
        
        figure(6),
        subplot(2,1,2),
        bar(x_axis,y_axis);
        xlabel('偏差量');
        ylabel('统计概率值');
        set(gca,'xtick',-23:1:24);
        title(strcat('直接低通滤波，当FSK信号频偏:w0=' ,num2str(f0*1000),' ,w1= ',num2str(f1*1000), 'Khz时统计偏移量'));
        
        pause;
    end
end

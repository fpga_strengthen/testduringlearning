clear
%% 这个逻辑生成的mif表有点问题，还需要再进一步调整。
%% 查表求arctan值
% 已知theta值和tan(theta)，且0≤theta≤pi/4，theta的定点位数为10位，tan值的定点位数为9位
% 根据9位的tan(theta)值查表得到10位的theta值

% tan值的有效位为9位
N = 9;
% theta的量化
theta = 0 : (pi/4)/(2^N - 1) :pi/4;
% 计算tan_theta值
tan_theta = tan(theta);
% 将tan_theta值量化为9bit
tan_dp = fix(tan_theta * (2^N-1));
% 将theta归一化，作为查表地址，查表得到的theta是10位
theta_dp = fix(theta/(pi/4) * (2^(N+1)-1));
% 绘图
plot(theta_dp,tan_dp);
xlabel('theta');
ylabel('tan_theta');

%% 写入文件
% 文件写入
fid = fopen('rom_tan.mif','w+');
fprintf(fid,'%s\n','WIDTH=10;');
fprintf(fid,'%s\n','DEPTH=512;');

fprintf(fid,'\n%s\n','ADDRESS_RADIX=UNS;');
fprintf(fid,'\n%s\n','DATA_RADIX=UNS;');
fprintf(fid,'\n');
fprintf(fid,'%s\n','CONTENT BEGIN');
for i=1:2^N
    fprintf(fid,'\t\t\t %d%s%d %s \n',tan_dp(i),':',theta_dp(i),';');
end
fprintf(fid,'%s','END;');
fclose(fid);

%%
//+FHEADER //////////////////////////////////////////////////////////////////////
//  版权所有者：                                                                 
//  Copyright 2014. All Rights Reserved.                                         
//  保密级别：                                                                   
//---------------------------------------------------------------------------    
//  文件名 :  can_control.v                                                     
//  原始作者：郭庆阳                                                             
//  联系方式：                                                                   
// --------------------------------------------------------------------------    
// 版本升级历史：                                                                
// 版本生效日期：                                                                
// 1.0 2015-10-25   郭庆阳  第1版 
// 2020.6.8   王雪                                                                                       
//---------------------------------------------------------------------------    
// 关键字 :                                            
//---------------------------------------------------------------------------    
// 模块功能:  完成mcp2515的初始化以及接收帧和发送帧的状态控制和切换。                                 
//                        
// --------------------------------------------------------------------------    
// 参数文件名：                                                                  
// --------------------------------------------------------------------------    
// 重复使用问题：                                                                
// 复位策略:负跳变沿异步复位信号 i_rst_n                                         
// 时钟域 : i_clk_27M                                                            
// 关键时序: N/A                                                                 
// 是否需要实例引用别的模块 : No                                                 
// 可综合否 : Yes                                                                
//-FHEADER //////////////////////////////////////////////////////////////////////
module can_control(
        //time & rst
        i_clk_27M,
        i_rst_n,

        i_can_init_finish, //脉冲信号，mcp1515芯片初始化完成
        i_int,             //电平信号，低有效，代表mcp2515有数据要输入到FPGA       
        i_read_frame_ok,   //脉冲信号，接收1帧完成
        i_send_msg_en,     //脉冲信号，代表上层有数据帧要发送
        i_send_frame_ok,   //脉冲信号，发送1帧完成
        //output control
        o_init_state_en,       //电平信号，用来触发初始化模块
        o_read_frame_en,   //电平信号，触发can_read_frame，开始接收帧
        o_send_frame_en   //电平信号，触发can_send_msg_frame，开始发送帧
        );

    input i_clk_27M;      
    input i_rst_n;
           
    input i_can_init_finish;
    input i_int;
        
    input i_read_frame_ok;
    input i_send_msg_en;  
    input i_send_frame_ok;
    
    output o_init_state_en;    
    output o_read_frame_en;
    output o_send_frame_en;

//////////////////////////////////////////
    reg send_msg_en;     //电平信号，用来触发can_send_msg_frame
    reg o_init_state_en;
    reg o_read_frame_en;
    reg o_send_frame_en;
    //reg[7:0] o_msg_data_in;
////////////////////////////////////////// 
    reg[10:0] cstate;
    reg[10:0] nstate;    
//////////////////////////////////////////
    parameter IDLE          = 11'b00000000001, //wait for read cmd 
              MCP_INIT      = 11'b00000000010, //initialize the mcp2515
              WAIT          = 11'b00000000100, //wait for reading or sending
              READ          = 11'b00000001000, //read frame
              SEND1         = 11'b00000010000; //send frame
              //WAIT1         = 11'b00000100000,
              //SEND2         = 11'b00001000000,
              //WAIT2         = 11'b00010000000,
              //SEND3         = 11'b00100000000,
              //WAIT3         = 11'b01000000000,
              //SEND4         = 11'b10000000000; 
//////////////////////////////////////////
    //发送状态处理
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if(!i_rst_n) 
            send_msg_en <= 1'b0;
        //else if(i_change_addr_en)//该信号此时应该已经拉低,此处冗余进一步确保该信号拉低
            //send_msg_en <= 1'b0; //该信号仅仅持续到第1个字节发送到can_send_msg_frame即拉低
        else if(i_send_msg_en)
            send_msg_en <= 1'b1;
        else
            send_msg_en <= 1'b0;
    end

/////////////////////////////////////////读写调度状态机
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if(!i_rst_n) 
            cstate <= IDLE;
        else 
            cstate <= nstate;
    end
 
    always@(cstate
        or i_can_init_finish
        or i_int
        or i_read_frame_ok
        or send_msg_en
        or i_send_frame_ok
        )
    begin
        case(cstate)
        IDLE: 
            nstate = MCP_INIT;       //reset once power on
        MCP_INIT: 
            if(i_can_init_finish)    //initial ok,wait for interrupt
                nstate = WAIT;
            else
                nstate = MCP_INIT;        
        WAIT:
            if(~i_int)               //int pull down, receive the bus data
                nstate = READ;
            else if(send_msg_en) 
                nstate = SEND1;
            else
                nstate = WAIT;
        READ:
            if(i_read_frame_ok)      //read one frame, wait for the next or prepare to reply
                nstate = WAIT;
            else
                nstate = READ;
        SEND1:
            if(i_send_frame_ok)     //the first state frame is sent, send next frame 
                nstate = WAIT;
            else
                nstate = SEND1;
        default:nstate = IDLE;
        endcase
    end
 //////////////////////////////////
    always@(posedge i_clk_27M or negedge i_rst_n)
    begin
        if(!i_rst_n) begin
            o_init_state_en <= 1'b0;
            o_read_frame_en <= 1'b0;
            o_send_frame_en <= 1'b0;
        end
        else begin
        case(cstate)
        IDLE: begin
            o_init_state_en <= 1'b0;
            o_read_frame_en <= 1'b0;
            o_send_frame_en <= 1'b0;
        end       
        MCP_INIT: begin
            o_init_state_en <= 1'b1;
            o_read_frame_en <= 1'b0;
            o_send_frame_en <= 1'b0;
        end
        WAIT: begin
            o_init_state_en <= 1'b0;
            o_read_frame_en <= 1'b0;
            o_send_frame_en <= 1'b0;
        end
        READ: begin
            o_init_state_en <= 1'b0;
            o_read_frame_en <= 1'b1;
            o_send_frame_en <= 1'b0;
        end
        SEND1: begin
            o_init_state_en <= 1'b0;
            o_read_frame_en <= 1'b0;
            o_send_frame_en <= 1'b1;
        end    
        default: begin
            o_init_state_en <= 1'b0;
            o_read_frame_en <= 1'b0;
            o_send_frame_en <= 1'b0;
        end   
        endcase
        end
    end
        
endmodule
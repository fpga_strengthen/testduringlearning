`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date: 2024/08/22 23:05:46
// Module Name: udp_protocal_stack
// Description: UDP协议栈顶层.
//////////////////////////////////////////////////////////////////////////////////

module udp_protocal_stack #(
	// 源MAC与目的MAC
	parameter   LOCAL_MAC_ADDR  = 48'hFF_FF_FF_FF_FF_FF,     // local MAC address
	parameter   TARGET_MAC_ADDR = 48'hFF_FF_FF_FF_FF_FF,
	// 源IP与目的IP
	parameter   LOCAL_IP_ADDR   = 32'h0,
	parameter   TARGET_IP_ADDR  = 32'h0,
	// 本地端口与目的端口号
	parameter   LOCAL_PORT      = 16'h0,    // 本地端口号
	parameter   TARGET_PORT     = 16'h0     // 目的端口号
	)(
	// PHY接口时钟
	input  wire			i_phy_rx_clk,
	input  wire			i_phy_tx_clk,
	// 系统复位
	input  wire			i_rst_n,

	//----------------- GMII接口信号 ----------------------//
	input  wire			i_gmii_rx_data_vld,		// 数据有效信号
	input  wire	[7:0]	iv_gmii_rx_data,		// MAC数据包数据(单沿)

	output wire 		o_gmii_tx_data_vld,		// 数据有效信号
	output wire	[7:0]	ov_gmii_tx_data,		// MAC数据包数据(单沿)

	//----------------- 用户端接收信号 ----------------------//
	input  wire			i_app_rx_clk,			// 用户端接收时钟

	output wire			o_app_rx_data_vld,
	output wire			o_app_rx_data_last,
	output wire	[7:0]	ov_app_rx_data,
	output wire	[15:0]	ov_app_rx_length,

	//----------------- 用户端发送信号 ----------------------//
	input  wire			i_app_tx_clk,
	input  wire			i_app_tx_data_vld,		// 数据有效标志
	input  wire			i_app_tx_data_last,		// 数据部分最后一个
	input  wire	[7:0]	iv_app_tx_data,			// UDP层数据
	input  wire	[15:0]	iv_app_tx_length,		// UDP层数据包长度

	output wire			o_app_tx_ready			// 用户端准备完成信号
);

/*-----------------------------------------------*\
					模块连线
\*-----------------------------------------------*/	
	// 复位信号
	wire			phy_rx_rst_n;
	wire			phy_tx_rst_n;
	wire			app_rx_rst_n;
	wire			app_tx_rst_n;
	
	// MAC
	wire			mac_rx_data_vld;
	wire			mac_rx_data_last;
	wire	[7:0]	mac_rx_data;
	wire	[15:0]	mac_rx_frame_type;

	// IP
	wire			ip_tx_data_vld;
	wire			ip_tx_data_last;
	wire	[15:0]	ip_tx_length;
	wire	[7:0]	ip_tx_data;

	wire			icmp_rx_vld;
	wire			icmp_rx_data_last;
	wire	[7:0]	icmp_rx_data;
	wire	[15:0]	icmp_rx_length;

	// UDP
	wire			udp_rx_vld;
	wire			udp_rx_data_last;
	wire	[7:0]	udp_rx_data;
	wire	[15:0]	udp_rx_length;

	wire			udp_tx_data_vld;
	wire			udp_tx_data_last;
	wire	[7:0]	udp_tx_data;
	wire	[15:0]	udp_tx_length;

	// MAC to ARP or IP
	wire			ip_rx_vld;
	wire			ip_rx_data_last;
	wire	[7:0]	ip_rx_data;

	wire			arp_rx_vld;
	wire			arp_rx_data_last;
	wire	[7:0]	arp_rx_data;

	wire			app_tx_req;

	wire	[15:0]	arp_tx_length;
	wire			arp_tx_data_vld;
	wire	[7:0]	arp_tx_data;
	wire			arp_tx_data_last;

	wire	[7:0]	icmp_tx_data;
	wire			icmp_tx_vld;
	wire	[15:0]	icmp_tx_length;
	wire			icmp_tx_data_last;

	wire   			send_mac_data_vld;
	wire   			send_mac_data_last;
	wire   	[7:0]	send_mac_data;
	wire   	[15:0]	send_mac_type;
	wire   	[15:0]	send_mac_length;

	wire			send_ip_data_vld;
	wire			send_ip_data_last;
	wire	[7:0]	send_ip_data;
	wire	[15:0]	send_ip_length;
	wire	[15:0]	send_ip_type;

	// ARP动态链表查找MAC地址
	wire			rd_arp_list_en;
	wire	[31:0]	rd_arp_list_ip;
	// ARP动态链表MAC地址查找结果
	wire	[47:0]	rd_arp_list_mac;
	wire			rd_arp_list_mac_vld;

/*-----------------------------------------------*\
					复位信号
\*-----------------------------------------------*/	

	sync_rstn_gen u0(
		.i_phy_rx_clk   (i_phy_rx_clk),
		.i_phy_tx_clk   (i_phy_tx_clk),
		.i_app_rx_clk   (i_app_rx_clk),
		.i_app_tx_clk   (i_app_tx_clk),
		.i_rst_n        (i_rst_n),
		
		.o_phy_rx_rst_n (phy_rx_rst_n),
		.o_phy_tx_rst_n (phy_tx_rst_n),
		.o_app_rx_rst_n (app_rx_rst_n),
		.o_app_tx_rst_n (app_tx_rst_n)
	);

/*-----------------------------------------------*\
					模块例化
\*-----------------------------------------------*/	
	
	// MAC层
	mac_layer_top #(
		.LOCAL_MAC_ADDR  (LOCAL_MAC_ADDR),
		.TARGET_MAC_ADDR (TARGET_MAC_ADDR),
		.CRC_CHECK_EN    (1'b1)		// 打开CRC计算
	) u1 (
		.i_app_tx_clk         (i_app_tx_clk),
		.i_app_rx_clk         (i_app_rx_clk),
		.i_phy_tx_clk         (i_phy_tx_clk),
		.i_phy_rx_clk         (i_phy_rx_clk),

		.i_app_rx_rst_n       (app_rx_rst_n),
		.i_app_tx_rst_n       (app_tx_rst_n),

		.i_phy_tx_rst_n       (phy_tx_rst_n),
		.i_phy_rx_rst_n       (phy_rx_rst_n),

		// 连接到顶层
		.i_gmii_rx_data_vld   (i_gmii_rx_data_vld),
		.iv_gmii_rx_data      (iv_gmii_rx_data),
		
		.o_gmii_tx_data_vld   (o_gmii_tx_data_vld),
		.ov_gmii_tx_data      (ov_gmii_tx_data),

		// MAC层(发送端)的来自IP层的输入
		.i_mac_tx_data_vld    (send_mac_data_vld),
		.i_mac_tx_data_last   (send_mac_data_last),
		.iv_mac_tx_data       (send_mac_data),
		.iv_mac_tx_frame_type (send_mac_type),			// 代表IP层
		.iv_mac_tx_length     (send_mac_length),
		
		// MAC层(接收端)输出给IP层
		.o_mac_rx_data_vld    (mac_rx_data_vld),
		.o_mac_rx_data_last   (mac_rx_data_last),
		.ov_mac_rx_data       (mac_rx_data),
		.ov_mac_rx_frame_type (mac_rx_frame_type),

		// 将ARP动态链表查表结果发送给MAC层用于组包
		.i_rd_arp_list_mac_vld(rd_arp_list_mac_vld),	// 数据有效信号
		.iv_rd_arp_list_mac	  (rd_arp_list_mac)
	);

	// 过渡选择
	mac_to_arp_ip u2(
		.i_clk                (i_app_rx_clk),
		.i_rst_n              (app_rx_rst_n),

		// MAC层接收端的输出,包含帧类型,也交给这个模块作为帧类型的判断
		.i_mac_rx_data_vld    (mac_rx_data_vld),
		.i_mac_rx_data_last   (mac_rx_data_last),
		.iv_mac_rx_data       (mac_rx_data),
		.iv_mac_rx_frame_type (mac_rx_frame_type),

		.o_ip_rx_vld          (ip_rx_vld),
		.o_ip_rx_data_last    (ip_rx_data_last),
		.ov_ip_rx_data        (ip_rx_data),

		.o_arp_rx_vld         (arp_rx_vld),
		.o_arp_rx_data_last   (arp_rx_data_last),
		.ov_arp_rx_data       (arp_rx_data)
	);

	// ARP层
	arp_layer_top #(
		.LOCAL_MAC_ADDR (LOCAL_MAC_ADDR),
		.LOCAL_IP_ADDR  (LOCAL_IP_ADDR)
	) u3 (
		.i_app_rx_clk          (i_app_rx_clk),
		.i_app_tx_clk          (i_app_tx_clk),

		.i_app_rx_rst_n        (app_rx_rst_n),
		.i_app_tx_rst_n        (app_tx_rst_n),

		.i_arp_rx_data_vld     (arp_rx_vld),
		.i_arp_rx_data_last    (arp_rx_data_last),
		.iv_arp_rx_data        (arp_rx_data),

		.ov_arp_tx_length      (arp_tx_length),
		.o_arp_tx_data_vld     (arp_tx_data_vld),
		.ov_arp_tx_data        (arp_tx_data),
		.o_arp_tx_data_last    (arp_tx_data_last),

		.i_rd_arp_list_en      (rd_arp_list_en),
		.iv_rd_arp_list_ip     (rd_arp_list_ip),

		.ov_rd_arp_list_mac    (rd_arp_list_mac),
		.o_rd_arp_list_mac_vld (rd_arp_list_mac_vld)
	);

	// IP层
	ip_layer_top #(
		.LOCAL_IP_ADDR  (LOCAL_IP_ADDR),
		.TARGET_IP_ADDR (TARGET_IP_ADDR)
	) u4 (
		.i_rx_clk            (i_app_rx_clk),
		.i_tx_clk            (i_app_tx_clk),

		.i_rx_rst_n          (app_rx_rst_n),
		.i_tx_rst_n          (app_tx_rst_n),

		// IP层接收端的输入是经过mac_to_arp_ip模块筛选后的.
		.i_ip_rx_vld         (ip_rx_vld),
		.i_ip_rx_data_last   (ip_rx_data_last),
		.iv_ip_rx_data       (ip_rx_data),

		// IP层接收端的输出给到UDP层的接收端的输入
		.o_udp_rx_vld        (udp_rx_vld),
		.o_udp_rx_data_last  (udp_rx_data_last),
		.ov_udp_rx_data      (udp_rx_data),
		.ov_udp_rx_length    (udp_rx_length),

		// ICMP 暂时连接到中间信号
		.o_icmp_rx_vld       (icmp_rx_vld),
		.o_icmp_rx_data_last (icmp_rx_data_last),
		.ov_icmp_rx_data     (icmp_rx_data),
		.ov_icmp_rx_length   (icmp_rx_length),

		// 与UDP层交互(UDP层输出,是IP层发送端的输入)
		.i_udp_tx_data_vld   (send_ip_data_vld),
		.i_udp_tx_data_last  (send_ip_data_last),
		.iv_udp_tx_data      (send_ip_data),
		.iv_udp_tx_length    (send_ip_length),
		.iv_tx_type  		 (send_ip_type[7:0]),

		// 与MAC层交互(输出给MAC层发送模块的输入端)
		.o_ip_tx_data_vld    (ip_tx_data_vld),
		.o_ip_tx_data_last   (ip_tx_data_last),
		.ov_ip_tx_length     (ip_tx_length),
		.ov_ip_tx_data       (ip_tx_data),

		// 发送MAC查找使能信号和对应的IP地址
		.o_mac_list_rden	 (rd_arp_list_en),
		.ov_list_ip_addr	 (rd_arp_list_ip)
	);

	// MAC层仲裁模块
	mux2_arbit u5(
		.i_clk            (i_app_tx_clk),
		.i_rst_n          (app_tx_rst_n),

		.iv_ch0_data      (ip_tx_data),
		.i_ch0_data_last  (ip_tx_data_last),
		.i_ch0_data_vld   (ip_tx_data_vld),
		.iv_ch0_length    (ip_tx_length),
		.iv_ch0_type      (16'h0800),

		.i_ch1_data_vld   (arp_tx_data_vld),
		.i_ch1_data_last  (arp_tx_data_last),
		.iv_ch1_data      (arp_tx_data),
		.iv_ch1_length    (arp_tx_length),
		.iv_ch1_type      (16'h0806),

		.o_send_data_vld  (send_mac_data_vld),
		.o_send_data_last (send_mac_data_last),
		.ov_send_data     (send_mac_data),
		.ov_send_length   (send_mac_length),
		.ov_send_type     (send_mac_type)
	);

	// IP层仲裁模块:对来自UDP层和ICMP层的数据进行仲裁
	mux2_arbit u6(
		.i_clk            (i_app_tx_clk),
		.i_rst_n          (app_tx_rst_n),

		.iv_ch0_data      (udp_tx_data),
		.i_ch0_data_last  (udp_tx_data_last),
		.i_ch0_data_vld   (udp_tx_data_vld),
		.iv_ch0_length    (udp_tx_length),
		.iv_ch0_type      ({8'd0,8'd17}),

		.i_ch1_data_vld   (icmp_tx_vld),
		.i_ch1_data_last  (icmp_tx_data_last),
		.iv_ch1_data      (icmp_tx_data),
		.iv_ch1_length    (icmp_tx_length),
		.iv_ch1_type      ({8'd0,8'd1}),

		.o_send_data_vld  (send_ip_data_vld),
		.o_send_data_last (send_ip_data_last),
		.ov_send_data     (send_ip_data),
		.ov_send_length   (send_ip_length),
		.ov_send_type     (send_ip_type)
	);

	// ICMP层
	icmp_layer_top u7(
		.i_icmp_rx_clk       (i_app_rx_clk),
		.i_icmp_tx_clk       (i_app_tx_clk),

		.i_icmp_tx_rst_n     (app_tx_rst_n),
		.i_icmp_rx_rst_n     (app_rx_rst_n),

		.i_icmp_rx_vld       (icmp_rx_vld),
		.iv_icmp_rx_data     (icmp_rx_data),
		.i_icmp_rx_last      (icmp_rx_data_last),
		.iv_icmp_rx_length   (icmp_rx_length),

		.ov_icmp_tx_data     (icmp_tx_data),
		.o_icmp_tx_vld       (icmp_tx_vld),
		.ov_icmp_tx_length   (icmp_tx_length),
		.o_icmp_tx_data_last (icmp_tx_data_last)
	);

	// UDP层
	udp_layer_top #(
		.LOCAL_PORT  (LOCAL_PORT),
		.TARGET_PORT (TARGET_PORT)
	) u8 (
		.i_app_tx_clk       (i_app_tx_clk),
		.i_app_rx_clk       (i_app_rx_clk),

		.i_app_rx_rst_n     (app_rx_rst_n),
		.i_app_tx_rst_n     (app_tx_rst_n),

		// UDP接收端的输入来自IP层
		.i_udp_rx_vld       (udp_rx_vld),
		.i_udp_rx_data_last (udp_rx_data_last),
		.iv_udp_rx_data     (udp_rx_data),
		.iv_udp_rx_length   (udp_rx_length),

		// 与IP仲裁模块交互(UDP层发送端的输出，是IP层仲裁模块的输入)
		.o_udp_tx_data_vld  (udp_tx_data_vld),
		.o_udp_tx_data_last (udp_tx_data_last),
		.ov_udp_tx_data     (udp_tx_data),
		.ov_udp_tx_length   (udp_tx_length),

		// UDP发送端的输入来自用户端(连接到顶层)
		.i_app_tx_data_vld  (i_app_tx_data_vld),
		.i_app_tx_data_last (i_app_tx_data_last),
		.iv_app_tx_data     (iv_app_tx_data),
		.iv_app_tx_length   (iv_app_tx_length),

		// UDP接收端的输出给用户端(的输入)(连接到顶层)
		.o_app_rx_data_vld  (o_app_rx_data_vld),
		.o_app_rx_data_last (o_app_rx_data_last),
		.ov_app_rx_data     (ov_app_rx_data),
		.ov_app_rx_length   (ov_app_rx_length),

		.o_app_tx_req       (app_tx_req),
		.o_app_tx_ready     (o_app_tx_ready)
	);

endmodule

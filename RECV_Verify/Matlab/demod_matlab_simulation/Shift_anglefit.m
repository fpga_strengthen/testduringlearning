function [ angle_fit_delay ] = Shift_anglefit( angle_fit ,n )
%   将数据进行移位寄存
%   angle_fit为待移位的数据
%   n为移位几位
Length_data = length(angle_fit);
angle_fit_delay = [ zeros(1,n-1),angle_fit(1:Length_data-n+1)];
%定标到21位
angle_fit_delay = FixPointNum( angle_fit_delay, 21, 0);

end


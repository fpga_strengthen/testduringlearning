onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -label sim:/Sync_tb/Group1 -group {Region: sim:/Sync_tb} /Sync_tb/i_clk_108M
add wave -noupdate -expand -label sim:/Sync_tb/Group1 -group {Region: sim:/Sync_tb} /Sync_tb/rst_n
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} /Sync_tb/U_Hilbert/i_clk_27M_en
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} /Sync_tb/i_clk_108M
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/angle_tan
add wave -noupdate -expand -label sim:/Sync_tb/U_cordic/Group2 -group {Region: sim:/Sync_tb/U_cordic} -radix decimal -radixshowbase 0 /Sync_tb/U_cordic/ov_angle_tan
add wave -noupdate -expand -label sim:/Sync_tb/U_slope_calu/Group3 -group {Region: sim:/Sync_tb/U_slope_calu} -radix decimal -radixshowbase 0 /Sync_tb/U_slope_calu/phase_r1
add wave -noupdate -expand -label sim:/Sync_tb/U_slope_calu/Group3 -group {Region: sim:/Sync_tb/U_slope_calu} -radix decimal -radixshowbase 0 /Sync_tb/U_slope_calu/phase_r2
add wave -noupdate -expand -label sim:/Sync_tb/U_slope_calu/Group3 -group {Region: sim:/Sync_tb/U_slope_calu} -format Analog-Step -height 74 -max 16896.0 -min -17152.0 -radix decimal -radixshowbase 0 /Sync_tb/U_slope_calu/phase_corect
add wave -noupdate -expand -label sim:/Sync_tb/U_slope_calu/Group3 -group {Region: sim:/Sync_tb/U_slope_calu} -format Analog-Step -height 74 -max 203701.99999999971 -min -2290688.0 -radix decimal /Sync_tb/U_slope_calu/ov_slope

add wave -noupdate -expand -label sim:/Sync_tb/U_ZCP_judge/Group4 -group {Region: sim:/Sync_tb/U_ZCP_judge} -radix decimal -radixshowbase 0 /Sync_tb/U_ZCP_judge/o_sync_point

TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {865161912 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 180
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ms
update
WaveRestoreZoom {861990397 ps} {944032033 ps}

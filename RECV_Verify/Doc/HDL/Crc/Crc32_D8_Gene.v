`timescale 1ns / 1ps
/*=======================================================================================*\
  Filename    : Crc32_D8_Gene.v
  Author      : 吕旌阳
  time        ：2016.08.12
  Description : 生成8bit的crc32
               
  Called by   : 
  Revision History :
  Email       : 
  Company     :

\*========================================================================================*/
//8比特输入CRC32生成函数
  // polynomial: (0 1 2 4 5 7 8 10 11 12 16 22 23 26 32)
  // data width: 11
  // convention: the first serial bit is D[10]
function [31:0] getCRC32_D8;
    input [7:0] Data;
    input [31:0] crc;
    reg [31:0] c;
    reg [31:0] newcrc;
    begin
        c = crc ^ {Data,24'b0}; //对输入信号先做处理

        newcrc[31] = c[22] ^ c[29];
        newcrc[30] = c[21] ^ c[28] ^ c[31];
        newcrc[29] = c[20] ^ c[27] ^ c[30] ^ c[31];
        newcrc[28] = c[19] ^ c[26] ^ c[29] ^ c[30];
        newcrc[27] = c[18] ^ c[25] ^ c[28] ^ c[29] ^ c[31];
        newcrc[26] = c[17] ^ c[24] ^ c[27] ^ c[28] ^ c[30];
        newcrc[25] = c[16] ^ c[26] ^ c[27];
        newcrc[24] = c[15] ^ c[25] ^ c[26] ^ c[31];
        newcrc[23] = c[14] ^ c[24] ^ c[25] ^ c[30];
        newcrc[22] = c[13] ^ c[24];
        newcrc[21] = c[12] ^ c[29];
        newcrc[20] = c[11] ^ c[28];
        newcrc[19] = c[10] ^ c[27] ^ c[31];
        newcrc[18] = c[ 9] ^ c[26] ^ c[30] ^ c[31];
        newcrc[17] = c[ 8] ^ c[25] ^ c[29] ^ c[30];
        newcrc[16] = c[ 7] ^ c[24] ^ c[28] ^ c[29];
        newcrc[15] = c[ 6] ^ c[27] ^ c[28] ^ c[29] ^ c[31];
        newcrc[14] = c[ 5] ^ c[26] ^ c[27] ^ c[28] ^ c[30] ^ c[31];
        newcrc[13] = c[ 4] ^ c[25] ^ c[26] ^ c[27] ^ c[29] ^ c[30] ^ c[31];
        newcrc[12] = c[ 3] ^ c[24] ^ c[25] ^ c[26] ^ c[28] ^ c[29] ^ c[30];
        newcrc[11] = c[ 2] ^ c[24] ^ c[25] ^ c[27] ^ c[28];
        newcrc[10] = c[ 1] ^ c[24] ^ c[26] ^ c[27] ^ c[29];
        newcrc[ 9] = c[ 0] ^ c[25] ^ c[26] ^ c[28] ^ c[29];
        newcrc[ 8] = c[24] ^ c[25] ^ c[27] ^ c[28];
        newcrc[ 7] = c[24] ^ c[26] ^ c[27] ^ c[29] ^ c[31];
        newcrc[ 6] = c[25] ^ c[26] ^ c[28] ^ c[29] ^ c[30] ^ c[31];
        newcrc[ 5] = c[24] ^ c[25] ^ c[27] ^ c[28] ^ c[29] ^ c[30] ^ c[31];
        newcrc[ 4] = c[24] ^ c[26] ^ c[27] ^ c[28] ^ c[30];
        newcrc[ 3] = c[25] ^ c[26] ^ c[27] ^ c[31];
        newcrc[ 2] = c[24] ^ c[25] ^ c[26] ^ c[30] ^ c[31];
        newcrc[ 1] = c[24] ^ c[25] ^ c[30] ^ c[31];
        newcrc[ 0] = c[24] ^ c[30];


        getCRC32_D8 = newcrc;
    end
endfunction
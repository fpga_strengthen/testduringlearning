: :Create Date : 2023.1.22
:: Author : J.GMson 
:: Function : Help you create a project struct which our lab requires quickly.

:: Create project folder and click to revise its name
md Project_Name

:: Enter root folder and create other folders.
cd  ./Project_Name

:: Save files with format pdf,doc,docx,csv,xlsx,txt,etc.
md Doc

:: Save HDL design files
md HDL

:: Save Quartus Project files
md TD_Prj

:: Save ModelSim Simulation files,incluing run.do,wave.do and others.
md ModelSim

:: Save Testbench file,including Verilog and SystemVerilog files.
md Testbench
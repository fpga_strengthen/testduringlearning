`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date: 2024/08/20 12:19:06
// Module Name: mac_to_arp_ip
// Description: 根据帧类型将数据区分开
// Revision 0.01 - File Created
//////////////////////////////////////////////////////////////////////////////////

module mac_to_arp_ip(
	// 时钟和复位
	input wire			i_clk,
	input wire			i_rst_n,

	/*---------------- mac_to_arp_ip 模块交互信号--------------------*/
	input wire			i_mac_rx_data_vld,		// 数据有效信号
	input wire			i_mac_rx_data_last,		// MAC层数据的最后一个数据的标志
	input wire	[7:0]	iv_mac_rx_data,			// 从MAC数据包解析出MAC层数据
	input wire	[15:0]	iv_mac_rx_frame_type,	// 帧类型

	//------------------ ip_receive 模块交互信号 --------------------//
	output	reg			o_ip_rx_vld,			// 数据有效信号
	output	reg			o_ip_rx_data_last,		// 最后一个数据的标志
	output 	reg	[7:0]	ov_ip_rx_data,			// 数据

	//------------------ arp_receive 模块交互信号 --------------------//
	output	reg			o_arp_rx_vld,			// 数据有效信号
	output	reg			o_arp_rx_data_last,		// 最后一个数据的标志
	output 	reg	[7:0]	ov_arp_rx_data			// 数据
);

	// 帧类型
	localparam	ARP_TYPE = 16'h0806;
	localparam 	IP_TYPE  = 16'h0800;

	// mac_receive模块中,在接收到帧类型字节后就一直会保持,数据会紧跟在此之后.
	// 所以当检测到帧类型之后,就可以紧跟着同步接收MAC层数据了。
	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n) begin
			o_arp_rx_vld       <= 1'b0;
			o_arp_rx_data_last <= 1'b0;
			ov_arp_rx_data     <= 16'b0;
		end 
		else if(iv_mac_rx_frame_type == ARP_TYPE)begin
			o_arp_rx_vld       <= i_mac_rx_data_vld;
			o_arp_rx_data_last <= i_mac_rx_data_last;
			ov_arp_rx_data     <= iv_mac_rx_data;
		end
		else begin 
			o_arp_rx_vld       <= 1'b0;
			o_arp_rx_data_last <= 1'b0;
			ov_arp_rx_data     <= 16'b0;
		end
	end

	always @(posedge i_clk or negedge i_rst_n) begin
		if(~i_rst_n) begin
			o_ip_rx_vld       <= 1'b0;
			o_ip_rx_data_last <= 1'b0;
			ov_ip_rx_data     <= 16'b0;
		end 
		else if(iv_mac_rx_frame_type == IP_TYPE)begin
			o_ip_rx_vld       <= i_mac_rx_data_vld;
			o_ip_rx_data_last <= i_mac_rx_data_last;
			ov_ip_rx_data     <= iv_mac_rx_data;
		end
		else begin 
			o_ip_rx_vld       <= 1'b0;
			o_ip_rx_data_last <= 1'b0;
			ov_ip_rx_data     <= 16'b0;
		end
	end

endmodule

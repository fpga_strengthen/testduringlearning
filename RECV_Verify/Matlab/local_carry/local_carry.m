%% 产生本地载波信号
% 3.951M,4.516M;cos,sin,共四组
clear
t = 0:2*pi/47:2*pi;
fc_1 = 4.516e6;
fc_0 = 3.951e6;
f_sin_0 = sin(2*pi*fc_0*t);
f_sin_1 = sin(2*pi*fc_1*t);
% plot(t,f_sin_0);
% hold on
% plot(t,f_sin_1);
f_cos_1 = cos(2*pi*fc_1*t);
f_cos_0 = cos(2*pi*fc_0*t);
% hold on
% plot(t,f_cos_1);
% plot(t,f_cos_0);
% 
% legend('cos\_1','cos\_0');

%% 定点化
f_sin_1_fp = round(f_sin_1 * 2^18);
f_sin_0_fp = round(f_sin_0 * 2^18);
f_cos_1_fp = round(f_cos_1 * 2^18);
f_cos_0_fp = round(f_cos_0 * 2^18);

%% 将定点数据写入本地MIF文件
fid_sin1 = fopen("rom_sin1.mif",'w');
fprintf(fid_sin1,"%s\n","DEEPTH = 128;");
fprintf(fid_sin1,"%s\n","WIDTH = 19;");
fprintf(fid_sin1,"%s\n","ADDRESS_RADIX=UNSIGNED;");
fprintf(fid_sin1,"%s\n\n","DATA_RADIX=DECIMAL;");
fprintf(fid_sin1,"%s\n","CONTENT");
fprintf(fid_sin1,"\t%s\n","BEGIN");
for i=0:47
    fprintf(fid_sin1,"\t%d : %d; \n",i,f_sin_1_fp(i+1));
end
fprintf(fid_sin1,"%s","END");
fclose(fid_sin1);



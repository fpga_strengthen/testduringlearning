`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Create Date: 2024/08/14 20:48:36
// Design Name: 
// Module Name: rgmii_receive
// Project Name: 
// Description: 
// Transform data which was triggered by double edges to single postive edge.
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
//////////////////////////////////////////////////////////////////////////////////

module rgmii_receive(
	// reset
	// input		wire			i_rst_n,
	// Reference clk for delay
  	input wire        i_delay_refclk,   // 200MHz
  	// signals from PHY
  	input wire        i_phy_rgmii_rx_clk,
  	input wire        i_phy_rgmii_rx_ctl,
  	// Data comes by two parts in both posedge and negedge.
  	input wire  [3:0] iv_phy_rgmii_rx_data,

  	output wire       o_gmii_rx_clk,
  	output wire       o_gmii_rx_vld,
  	output wire       o_gmii_rx_error,
  	// Data outputs by only posedge,so its width turns to be 8 from 4.
  	output wire [7:0] ov_gmii_rx_data
);

	wire	gmii_rx_error_xor;
	assign	o_gmii_rx_error = o_gmii_rx_vld ^ gmii_rx_error_xor;
	
/*-----------------------------------------*\
					IBUF
\*-----------------------------------------*/

	// buffer the PHY input
	wire			phy_rgmii_rx_clk_ibuf;
	wire			phy_rgmii_rx_ctl_ibuf;
	wire	[3:0]	phy_rgmii_rx_data_ibuf;

	// let input signals from PHY pass IBUF.
	// Vivado usually add IBUF for input signals automatically.Here we add it by ourselves.
	IBUF ibuf_rgmiii_rx_clk (
      	.O(phy_rgmii_rx_clk_ibuf),     		// Buffer output
      	.I(i_phy_rgmii_rx_clk)      		// Buffer input (connect directly to top-level port)
   	);

   	IBUF ibuf_rgmiii_rx_ctl (
      	.O(phy_rgmii_rx_ctl_ibuf),     		// Buffer output
      	.I(i_phy_rgmii_rx_ctl)      		// Buffer input (connect directly to top-level port)
   	);

   	// For multi-bit signal,IBUF can't be used directly.
   	// Here uses a generate block to switch.
   	genvar	i_rx;
   	generate
   		for (i_rx = 0; i_rx < 4; i_rx = i_rx + 1) begin : rx_data_ibuf
   			IBUF ibuf_rgmiii_rx_data (
      			.O(phy_rgmii_rx_data_ibuf[i_rx]),     	// Buffer output
      			.I(iv_phy_rgmii_rx_data[i_rx])      	// Buffer input (connect directly to top-level port)
   			);
   		end
   	endgenerate

/*-----------------------------------------*\
				BUFG、BUFIO
\*-----------------------------------------*/

	wire	phy_rgmii_rx_clk_bufio;

	// Buffer the clk of PHY.
   	BUFG rgmii_rx_clk_bufg(
      	.O(o_gmii_rx_clk), 				// 1-bit output: Clock output
      	.I(phy_rgmii_rx_clk_ibuf)  		// 1-bit input: Clock input
   	);
   	
   	// Buffer I/O .
   	BUFIO rgmii_rx_clk_bufio (
      	.O(phy_rgmii_rx_clk_bufio), 	// 1-bit output: Clock output (connect to I/O clock loads).
      	.I(phy_rgmii_rx_clk_ibuf)  		// 1-bit input: Clock input (connect to an IBUF or BUFMR).
   	);

/*-----------------------------------------------------*\
				Delay Control
\*-----------------------------------------------------*/
	
	wire			phy_rgmii_rx_ctl_delay;
	wire	[3:0]	phy_rgmii_rx_data_delay;

	// When using IDELAY, IDELAYCTRL must be used together.
	IDELAYCTRL IDELAYCTRL_inst(
      	.RDY(),       				// 1-bit output: Ready output
      	.REFCLK(i_delay_refclk), 	// 1-bit input: Reference clock input
      	.RST(0)        				// 1-bit input: Active high reset input
   	);

	// Use delay primitive to achieve equal wire length.
	IDELAYE2 #(
    	.CINVCTRL_SEL          ("FALSE"),          // Enable dynamic clock inversion (FALSE, TRUE)
      	.DELAY_SRC             ("IDATAIN"),         // Delay input (IDATAIN, DATAIN)
      	.HIGH_PERFORMANCE_MODE ("FALSE"), 			// Reduced jitter ("TRUE"), Reduced power ("FALSE")
      	.IDELAY_TYPE           ("FIXED"),           // FIXED, VARIABLE, VAR_LOAD, VAR_LOAD_PIPE
      	.IDELAY_VALUE          (0),                	// Input delay tap setting (0-31)
      	.PIPE_SEL              ("FALSE"),           // Select pipelined mode, FALSE, TRUE
      	.REFCLK_FREQUENCY      (200.0),        		// IDELAYCTRL clock input frequency in MHz (190.0-210.0, 290.0-310.0).
      	.SIGNAL_PATTERN        ("DATA")          	// DATA, CLOCK input signal
   	)
   	rgmii_rx_ctl_delay (
      	.IDATAIN     (i_phy_rgmii_rx_ctl),         	// 1-bit input: Data input from the I/O
      	.DATAOUT     (phy_rgmii_rx_ctl_delay),      // 1-bit output: Delayed data output
      	.DATAIN      (0),          	 // 1-bit input: Internal delay data input
      	.C           (0),            // 1-bit input: Clock input
      	.CE          (0),            // 1-bit input: Active high enable increment/decrement input
      	.CINVCTRL    (0),       		 // 1-bit input: Dynamic clock inversion input
      	.CNTVALUEIN  (0),   			   // 5-bit input: Counter value input
      	.INC         (0),            // 1-bit input: Increment / Decrement tap delay input
      	.LD          (0),            // 1-bit input: Load IDELAY_VALUE input
      	.LDPIPEEN    (0),       		 // 1-bit input: Enable PIPELINE register to load data input
      	.REGRST      (0),            // 1-bit input: Active-high reset tap-delay input
      	.CNTVALUEOUT () 				     // 5-bit output: Counter value output
   	);

   	// Buffer data solved by ibuf.
   	genvar	j_rx;
   	generate
   		for (j_rx = 0; j_rx < 4; j_rx = j_rx + 1) begin : deta_delay
   			IDELAYE2 #(
    			.CINVCTRL_SEL          ("FALSE"),          // Enable dynamic clock inversion (FALSE, TRUE)
      			.DELAY_SRC             ("IDATAIN"),         // Delay input (IDATAIN, DATAIN)
      			.HIGH_PERFORMANCE_MODE ("FALSE"), 			// Reduced jitter ("TRUE"), Reduced power ("FALSE")
      			.IDELAY_TYPE           ("FIXED"),           // FIXED, VARIABLE, VAR_LOAD, VAR_LOAD_PIPE
      			.IDELAY_VALUE          (0),                	// Input delay tap setting (0-31)
      			.PIPE_SEL              ("FALSE"),           // Select pipelined mode, FALSE, TRUE
      			.REFCLK_FREQUENCY      (200.0),        		// IDELAYCTRL clock input frequency in MHz (190.0-210.0, 290.0-310.0).
      			.SIGNAL_PATTERN        ("DATA")          	// DATA, CLOCK input signal
   			)
   			rgmii_rx_ctl_delay (
    		  	.IDATAIN     (phy_rgmii_rx_data_ibuf[j_rx]),         	// 1-bit input: Data input from the I/O
    		  	.DATAOUT     (phy_rgmii_rx_data_delay[j_rx]),      // 1-bit output: Delayed data output
    		  	.DATAIN      (0),          		// 1-bit input: Internal delay data input
    		  	.C           (0),               // 1-bit input: Clock input
    		  	.CE          (0),               // 1-bit input: Active high enable increment/decrement input
    		  	.CINVCTRL    (0),       		// 1-bit input: Dynamic clock inversion input
    		  	.CNTVALUEIN  (0),   			// 5-bit input: Counter value input
    		  	.INC         (0),               // 1-bit input: Increment / Decrement tap delay input
    		  	.LD          (0),               // 1-bit input: Load IDELAY_VALUE input
    		  	.LDPIPEEN    (0),       		// 1-bit input: Enable PIPELINE register to load data input
    		  	.REGRST      (0),            	// 1-bit input: Active-high reset tap-delay input
    		  	.CNTVALUEOUT () 				// 5-bit output: Counter value output
   			);
   		end
   	endgenerate

/*-----------------------------------------------------*\
						IDDR
\*-----------------------------------------------------*/
   	// Solve CTL signal.
   	IDDR #(
    	.DDR_CLK_EDGE ("OPPOSITE_EDGE"),// "OPPOSITE_EDGE", "SAME_EDGE" 
                                     	//    or "SAME_EDGE_PIPELINED" 
      	.INIT_Q1      (1'b0), 			// Initial value of Q1: 1'b0 or 1'b1
      	.INIT_Q2      (1'b0), 			// Initial value of Q2: 1'b0 or 1'b1
      	.SRTYPE       ("SYNC") 			// Set/Reset type: "SYNC" or "ASYNC" 
   	) IDDR_rgmii_rx_ctl (
      	.Q1 (o_gmii_rx_vld), // 1-bit output for positive edge of clock
      	.Q2 (gmii_rx_error_xor), // 1-bit output for negative edge of clock
      	.C  (phy_rgmii_rx_clk_bufio),   // 1-bit clock input
      	.CE (1'b1), // 1-bit clock enable input
      	.D  (phy_rgmii_rx_ctl_delay),   // 1-bit DDR data input
      	.R  (1'b0),   // 1-bit reset
      	.S  (1'b0)    // 1-bit set
   	);

 	// Solve Data signal.
   	genvar	k_rx;
   	generate
   		for (k_rx = 0; k_rx < 4; k_rx=k_rx + 1) begin
   			IDDR #(
    			.DDR_CLK_EDGE ("OPPOSITE_EDGE"),// "OPPOSITE_EDGE", "SAME_EDGE" 
                                     			//    or "SAME_EDGE_PIPELINED" 
      			.INIT_Q1      (1'b0), 			// Initial value of Q1: 1'b0 or 1'b1
      			.INIT_Q2      (1'b0), 			// Initial value of Q2: 1'b0 or 1'b1
      			.SRTYPE       ("SYNC") 			// Set/Reset type: "SYNC" or "ASYNC" 
   			) IDDR_rgmii_rx_ctl (
      			.Q1 (ov_gmii_rx_data[k_rx]), 	// 1-bit output for positive edge of clock
      			.Q2 (ov_gmii_rx_data[k_rx+4]), 	// 1-bit output for negative edge of clock
      			.C  (phy_rgmii_rx_clk_bufio),   		// 1-bit clock input
      			.CE (1'b1), 							// 1-bit clock enable input
      			.D  (phy_rgmii_rx_data_delay[k_rx]),   	// 1-bit DDR data input
      			.R  (1'b0),   							// 1-bit reset
      			.S  (1'b0)    							// 1-bit set
   			);
   		end
   	endgenerate

endmodule